/*
 *  Document   : formsValidation.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Forms Validation page
 */

var FormsValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#createEvent').validate({
                errorClass: 'help-block animation-pullUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    'val-hashtag': {
                        required: true,
                        minlength: 3
                    },
                    'val-weddingCode': {
                        required: true,
                        minlength:4,
                        maxlength:6
                       
                    },
                    'val-groomName': {
                        required: true,
                        minlength: 5
                    },
                    'val-bridalName': {
                        required: true,
                        minlength: 5
                    },
                    'val-weddingImage': {
                        required: true,
                        minlength: 5
                    }
                },
                messages: {
                    'val-hashtag': {
                        required: 'Please enter a hash tag',
                        minlength: 'Your hash tag must consist of at least 5 characters'
                    },
                    'val-weddingCode': 'Please enter a valid wedding code',
                    'val-groomName': 'Please enter the bridal name',
                    'val-bridalName': 'Please enter the bridal name',
                    'val-weddingImage': 'Please select wedding image!'
                   
                },
                submitHandler: function() {
					createWedding();
					}

            }); $('#createcuisinecat').validate({
                errorClass: 'help-block animation-pullUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    'val-cuisinecat': {
                        required: true,
                        minlength: 3
                    }
                },
                messages: {
                    'val-hashtag': {
                        required: 'Please enter a cuisine category',
                        //minlength: 'Your hash tag must consist of at least 5 characters'
                    },
                                     
                },
                submitHandler: function() {
                    //createWedding();
                    createcuisinecat();
                    }
            });

            $('#createdish').validate({
                errorClass: 'help-block animation-pullUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                // rules: {
                //     'val-cuisinecat': {
                //         required: true,
                //         minlength: 3
                //     },
                //      'val-weddingImage': {
                //         required: true,
                //         minlength: 5
                //     }
                // },
                // messages: {
                //     'val-hashtag': {
                //         required: 'Please enter a cuisine category',
                //         //minlength: 'Your hash tag must consist of at least 5 characters'
                //     },
                                     
                // },
                submitHandler: function() {
                    //createWedding();
                    adddish();
                    }
            });




        }
    };
}();