<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'head.php';
    ?>
    <title>Cancellation And Refund | Cloudkitch</title>
    <meta name="description" content="The cancellation and refund policy declares our terms of refund in case of cancellation.">
</head>

<body class="servicesPage">
    <?php
    include 'header.php';
    ?>
    <section class="topSection section section-services">
        <h2 class="setionTitle">Cancellations and Refunds</h2>
        <h2>Cancellations and Refunds</h2>
        <ul>
            <li>
                <p><b><u>Cancellation</u></b></p>
            </li>
            <li>
                <p>As a general rule you shall not be entitled to cancel your order once you have received confirmation of the same. If you cancel your order after it has been confirmed, CLOUDKITCH shall have a right to charge you cancellation fee of a minimum INR 75 upto the order value, with a right to either not to refund the order value or recover from your subsequent order, the complete/ deficit cancellation fee, as applicable, to compensate our restaurant and delivery partners. CLOUDKITCH shall also have right to charge you cancellation fee for the orders cancelled by CLOUDKITCH for the reasons specified under clause 1(iii) of this cancellation and refunds policy. In case of cancellations for the reasons attributable to CLOUDKITCH or its restaurant and delivery partners, CLOUDKITCH shall not charge you any cancellation fee.</p>
            </li>
            <li>
                <p>However, in the unlikely event of an item on your order being unavailable, we will contact you on the phone number provided to us at the time of placing the order and inform you of such unavailability. In such an event you will be entitled to cancel the entire order and shall be entitled to a refund in accordance with our refund policy.</p>
            </li>
            <li>
                <p>We reserve the sole right to cancel your order in the following circumstance:</p>
            </li>
            <li>
                <p>In the event of the designated address falls outside the delivery zone offered by us;</p>
            </li>
            <li>
                <p>Failure to contact you by phone or email at the time of confirming the order booking;</p>
            </li>
            <li>
                <p>Failure to deliver your order due to lack of information, direction or authorization from you at the time of delivery; or</p>
            </li>
            <li>
                <p>Unavailability of all the items ordered by you at the time of booking the order; or</p>
            </li>
            <li>
                <p>Unavailability of all the items ordered by you at the time of booking the order; or</p>
            </li>
            <li>
                <p><b><u>Refunds</u></b></p>
            </li>
            <li>
                <p>You shall be entitled to a refund only if you pre-pay for your order at the time of placing your order on the Platform and only in the event of any of the following circumstances:</p>
            </li>
            <li>
                <p>Your order packaging has been tampered or damaged at the time of delivery;</p>
            </li>
            <li>
                <p>Us cancelling your order due to (A) your delivery location following outside our designated delivery zones; (B) failure to contact you by phone or email at the time of confirming the order booking; or (C) failure to contact you by phone or email at the time of confirming the order booking; or</p>
            </li>
            <li>
                <p>You cancelling the order at the time of confirmation due to unavailability of the items you ordered for at the time of booking.</p>
            </li>
            <li>
                <p>Our decision on refunds shall be at our sole discretion and shall be final and binding.</p>
            </li>
            <li>
                <p>All refund amounts shall be credited to your account within 3-4 business days in accordance with the terms that may be stipulated by the bank which has issued the credit / debit card.</p>
            </li>

        </ul>

        <table class="process-table custTable">
            <tr>
                <th>Process</th>
                <th>Payment Method</th>
                <th>Refund Source</th>
                <th>TAT</th>
            </tr>
            <tr>
                <td rowspan="10">
                    <p>Order Edit/Cancellation/Compensation/Payment Failure</p>
                </td>
                <td>
                    <p>Net Banking</p>
                </td>
                <td>
                    <p>Source</p>
                </td>
                <td>
                    <p>5-7 Business Days</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Debit/Credit Cards</p>
                </td>
                <td>
                    <p>Source</p>
                </td>
                <td>
                    <p>4-7 Business Days</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>UPI</p>
                </td>
                <td>
                    <p>Source</p>
                </td>
                <td>
                    <p>2-4 Business Days</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Amazon Pay (Wallet)</p>
                </td>
                <td>
                    <p>Source</p>
                </td>
                <td>
                    <p>5 Business Days</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Amazon Pay (CC/DC/NB)</p>
                </td>
                <td>
                    <p>Source</p>
                </td>
                <td>
                    <p>5 Business Days</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Phone Pe (Wallet)</p>
                </td>
                <td>
                    <p>Source</p>
                </td>
                <td>
                    <p>5 Business Days</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Phone Pe (CC/DC/NB)</p>
                </td>
                <td>
                    <p>Source</p>
                </td>
                <td>
                    <p>5 Business Days</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Wallets-Paytm/Mobikwik/Freecharge</p>
                </td>
                <td>
                    <p>Source</p>
                </td>
                <td>
                    <p>1 Hour</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Lazy Pay</p>
                </td>
                <td>
                    <p>Source</p>
                </td>
                <td>
                    <p>1 Hour</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Sodexo</p>
                </td>
                <td>
                    <p>Source</p>
                </td>
                <td>
                    <p>5 Business Days</p>
                </td>
            </tr>

        </table>

        <ul>
            <li>
                <p>In case of payment at the time of delivery, you will not be required to pay for:</p>
            </li>
            <li>
                <p>Orders where the packaging has been tampered or damaged by us;</p>
            </li>
            <li>
                <p>Wrong order being delivered; or</p>
            </li>
            <li>
                <p>Items missing from your order at the time of delivery.</p>
            </li>

        </ul>
    </section>


    <?php
    include 'footer.php';
    ?>
</body>

</html>