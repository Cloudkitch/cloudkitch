<?php

if (isset($_GET['user_id']) && isset($_GET['order_id'])) {
    $show_review = "1";
    $user_id = $_GET['user_id'];
    $order_id = $_GET['order_id'];
} else {
    $show_review = "0";
    $user_id = "";
    $order_id = "";
}

// <?php header("Location:web/cloudkitchadmin/"); 


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'head.php';
    // echo $baseurl;
    // print_r($_SESSION);
    ?>
    <title>Top Indian Restaurants Food Delivery Near Me| Indian Cuisine | Delivery Only Restaurants Near Me – Cloud Kitch</title>
    <meta name="description" content="When hunger strikes and when in need to get Indian cuisine or Indian food delivery near me from one of the top Indian restaurants or from the delivery only restaurants near me, always choose Cloud Kitch, the best delivery kitchen curating the top Indian restaurants.">
    <meta name="keywords" content="indian food delivery near me, indian cuisine near me, TOP Indian restaurants, delivery only restaurants near me">
    <meta name="google-signin-client_id" content="287145767352-qi8asdmobui20671musqaq0jga2ru2re.apps.googleusercontent.com">
</head>

<body>
    <h1 class="seoTag">Cloud Kitch</h1>
    <?php
    include 'header.php';

    if (isset($_SESSION['userid'])){
        if($_SESSION['isCorporateUser'] == 1){
            echo '<script>window.location = "'.$baseurl.'corporate";</script>';
        }
    }
    ?>
    <div class="popup popup-order addtocartpopup">
        <div class="popup-wrapper">
            <div class="popup-close">
                <img src="<?= $baseurl; ?>images/icons/close.svg" alt="close">
            </div>
            <div class="popup-item">
                <h2 class="cuisineTitle" id="cuisineTitle1">Veg Christmas Buffet B</h2>
                <input type="hidden" id="cuisine_id">
                <input type="hidden" id="kitchen_open">
                <input type="hidden" id="kitchen_close">
                <!-- <p>Nine course mini buffet with 3 salad, 4 main dishes and 2 desserts.</p> -->
                <div class="cuisine-details">
                    <h4>What’s included:</h4>
                    <div id="cuisine-details">
                    </div>
                    <!-- <ul class="menu-list">
                            <li><p>Watermelon & Feta Salad drizzled with black tea vinaigrette (Vegetarian) </p></li>
                            <li><p>Smoked Duck Salad with roasted cashew and citrus vinaigrette </p></li>
                            <li><p>Tamarind Vinaigrette Capellini tossed with edamame and herbed cherry tomatoes (Vegetarian) </p></li>
                            <li><p>Christmas Spiced Rice with thyme roasted pumpkin and pomegranate (Vegetarian) </p></li>
                            <li><p>Roasted Winter Vegetables with charred pineapple and sweet prune sprinkles (Vegetarian) </p></li>
                            <li><p>Cinnamon Sugar Glazed Chicken with ginger flower and almond flakes </p></li>
                            <li><p>Eggless Eggnog Dory with caramelised onions and fresh cherry tomatoes </p></li>
                            <li><p>Holiday Walnut Cake with rhubarb cherry compote (Vegetarian) </p></li>
                            <li><p>Christmas Cookies with a Grain surprise (Vegetarian) </p></li>
                        </ul> -->
                    
                    <h4 id="addonheading">Add on Packages</h4>
                    <!-- Add on Packages (Multipe select) -->
                    <div class="checkboxWrap" id="addonlist">

                    </div>


                    <h4>Special Instructions</h4>
                    <textarea name="special-instrustion" class="custom-textarea" id="special-instrustion" rows="8"></textarea>
                </div>
            </div>
            <div class="popup-item">
                <img src="<?= $baseurl; ?>images/cuisine/corporate/1.jpg" alt="Cuisine" class="order-img" id="order-img_cuisin">
                <div class="cuisineType">
                    <p id="veg_val"><img src="<?= $baseurl; ?>images/icons/veg.svg" alt="Veg">4 course meal</p>
                </div>
                <div class="priceBlock">
                    <p class="price">&#8377; <span id="pop_price">1,550</span> <span class="price-gst">GST</span></p>
                    <p class="label-gray" id="mim_order">Min Order 25pax</p>
                </div>
                <div class="quantityBlock">
                    <div class="timeBlock">
                        <p class="timeStat label-gray"><span id="leadtime"></span> hours lead time</p>
                    </div>
                </div>
                <div class="order-date-time">
                    <div class="order-date">
                        <div class="form-group form-legend">
                            <input type="text" id="datepicker" class="form-input-legend" placeholder="Select Date">
                        </div>
                    </div>
                    <div class="order-time">
                        <div class="form-group form-legend">
                            <input type="text" class="form-input-legend timepicker" id="order_time" placeholder="Select Time" />
                        </div>
                    </div>
                </div>
                <input id="teamMealMinQty1" type="hidden" class="quantity">
                <div class="order-quantity">
                    <a href="#" class="quantity-icon sub">
                        <p>-</p>
                    </a>
                    <input id="teamMealMinQty" type="text" class="quantity-input quantity" readonly>
                    <a href="#" class="quantity-icon add active">
                        <p>+</p>
                    </a>
                </div>
                
                <input type="hidden" id="validateflag" value="1">
                <div class="addBtn">
                    <p class="addToCart" id="addtocartBtn" onclick="corporatemealorder()"><img src="<?= $baseurl; ?>images/icons/addCart.svg" alt="download">ADD to cart</p>
                    <span id="quterror" style="color:red"></span>
                    
                </div>
               
            </div>
        </div>
        <div class="bg-grey">
            <!-- <p>Terms and Conditions para will follow here if any, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p> -->
        </div>
    </div>

    <section class="banner topSection">
        <h2 style="display:none;">Cloudkitch</h2>
        <div class="homeSlider" id="homeSlider" style="display: none">
            <?php
            $query = "SELECT * FROM banners";
            $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
            while($row = mysqli_fetch_assoc($result))
            {
                ?>
                <div class="homeSlide">
                    <img class="lazyload" data-src="<?=$row['image']?>" alt="Banner">
                </div>
                <?php
            }
            ?>
        </div>
    </section>
    <section class="section firstSection" id="ScrollUpToDiv">
        <div class="titleWrap sectionText">
            <h2>Quick Orders</h2>
            <p>Kitchen's to Choose From</p>
        </div>
        <div class="filterWrap">
            <div class="titleWrap">
                <h2><img src="<?= $baseurl; ?>images/icons/cuisine.svg" alt="Course"> Popular Options</h2>
            </div>
            <div class="filterBtn"></div>
            <div class="filterContainer">
                <div class="searchWrap menuSearch">
                    <form>
                        <input type="text" id="search_inner" oninput="getCuisinesFilter()" placeholder="Search Hot Favourites">
                        <input type="hidden" id="kitchen_id">
                    </form>
                </div>
                <div class="buttonWrap">
                    <p class="btn borderBtn offersBtn" onclick="getOfferedCuisines()"><img src="<?= $baseurl; ?>images/icons/discount.svg" alt="offers">All Offers</p>
                    <div class="filter-check">
                        <input class="custom-radio" onchange="getCuisinesFilter()" type="checkbox" id="checkbox_veg" name="checkbox_veg">
                        <label for="checkbox_veg" class="btn radio-label">
                            <img src="<?= $baseurl; ?>images/icons/leaf.svg" alt="Veg">VEG
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="menuWrap autoHeight">
            <div class="cuisineWrap card restrocuisineWrap">
                <span class="cusine-second">
                    <span class="closeFilter">Close X</span>
                </span>
                <div class="checkWrap">
                    <ul id="categories">

                    </ul>
                </div>
            </div>
            <div class="cuisineWrapper cuisineWrappernotflex">
                <div class="restroWrap homerestroSlider" id="kitchens"></div>
                <div class="cuisineContainer heightContainer">
                    <div id="cuisines"></div>
                </div>
                <span data-value="1" id="loadmoredata" class="loadmore" style="border: none;"> </span>
            </div>
        </div>
        <?php
       
        if (isset($_COOKIE['pincode']) && $_COOKIE['pincode']!="") {
            $display = 'none;';
        } else {
            $display = 'block;';
        }
       
        ?>
        <div class="overlay" id="locationss" style="display:<?= $display ?>"></div>
        <div class="popup locationPopup" id="locationpopup" style="display:<?= $display ?>">
            <div class="popup-wrapper">
                <div class="centerText">
                    <img src="<?= $baseurl; ?>images/logo.svg" alt="Cloudkitch" class="popup-logo">
                </div>
                <div class="firstDiv">
                    <form id="pincode_form" name="pincode_form">
                        <h2>Welcome to Cloudkitch!</h2><br>
                        <div class="form-conatiner">
                            <div class="form-group form-float-group">
                                <input type="text" placeholder="" id="pincode" name="pincode" autocomplete="off" class="form-float-control">
                                <label for="pincode" class="form-float-lab">Enter area pincode here</label>
                            </div>
                            <button type="submit" class="btn-gradient">Proceed</button>
                        </div>
                    </form>
                    <p class="skip">Skip</p>
                </div>
                <div class="secDiv centerText" style="display:none;">
                    <h2>Your area is currently not serviceable.</h2>
                </div>
            </div>
        </div>
    </section>
    <?php
    include 'footer.php';
    ?>
    
    <script>
        
        $(document).ready(function() {
            $("#homeSlider").css("display","block");
            getCategories('1');
            getKitchens();
            getCuisines();
            $("#meals_check").val('corporate_meals');
            var s = '<?php echo $_COOKIE['pincode'] ?>';
            if(s != ''){
                $("#locationpopup").css("display","none");
                $("#locationss").css("display","none");
            }else{
                $("#locationpopup").css("display","block");
                $("#locationss").css("display","block");
            }

            var show_review = '<?= $show_review ?>';
            if (show_review == '1') {
                $(".reviewPopup").css({
                    'display': 'block'
                });
                $(".locationPopup").css({
                    'display': 'none'
                });
            }

            $('#search_inner').keypress(function(event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                }
             });
            
           

        });
  
   
        jQuery(
            function($)
            {
                $('.cuisineContainer').bind('scroll', function()
                    {
                        if($(this).scrollTop() + $(this).innerHeight()>=$(this)[0].scrollHeight)
                        {
                            var loadlimit = Number($("#loadmoredata").attr("data-value"));
                                    if ($("#checkbox_veg").prop("checked") == true) {
                                        var type = "veg";
                                    } else {
                                        var type = "";
                                    }
                                    $("#loadmoredata").attr("data-value",loadlimit+1);
                                    var category_id = $('.checkWrap  li.activeTab').attr("data-id");
                                    var pagedata = {
                                        "keyword": "",
                                        "categories": category_id,
                                        "sortbycost": "",
                                        "type": type,
                                        "limit": loadlimit
                                    };
                                    $.ajax({
                                        url: serviceurl + 'cuisines',
                                        type: 'POST',
                                        data: JSON.stringify(pagedata),
                                        datatype: 'JSON',
                                        async: false,
                                        success: function(data) {
                                            var value = JSON.parse(data);
                                            var html = "";
                                            if (value.cuisines.length > 0) {
                                                for (var i = 0; i < value.cuisines.length; i++) {
                                                            if(value.cuisines[i].isCustomisable == '1'){
                                                                html += '<div class="cuisine card" onclick="customizecuisine('+ value.cuisines[i].id +')">';
                                                            }else{
                                                                html += '<div class="cuisine card">';
                                                            }
                                                        
                                                            html += '<div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '</p><p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock" id="cuisineCatDiv-' + value.cuisines[i].id + '"><div class="hurryBlockContainer">';
                                                            if(value.cuisines[i].isCustomisable == '1'){
                                                                html += '<p>Customizable</p>';
                                                            }else{
                                                                html += '<p> </p>';
                                                            }
                                                            html += ' </div><div class="hurryBlockContainer">';
                                                            var cuisintype = value.cuisines[i].type.split(",");
                                                                if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                                                    html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                                                } else {
                                                                    html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                                                }
                                                            
                                                            html += '</div></div>';
                                                            if(value.cuisines[i].isCustomisable != '1'){
                                                                var start = value.cuisines[i].kitchen_workingstart;
                                                                var end = value.cuisines[i].kitchen_workingend;

                                                                var d = new Date();
                                                                        var datesss = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                                                                        var min = new Date(datesss + ' ' + start);
                                                                        var max = new Date(datesss + ' ' + end);

                                                                        if(min.getTime() > d.getTime() || d.getTime() >  max.getTime()){
                                                                            html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '">Kitchen closed</p></div>';
                                                                        }
                                                                        else{
                                                                            html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="0" max="3" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';
                                                                        }

                                                                     //   html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="0" max="3" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';

                                                               
                                                            }
                                                            html += '</div></div></div>';
                                                    }
                                                    if(value.cuisines.length < 30){
                                                        $("#loadmoredata").hide();
                                                    }
                                                    $("#cuisines").html(html);
                                                    addedIncart();
                                                    hoverCuisine();
                                                    addButtonFunction();
                                                    adjustHeight();
                                                    slideCat();
                                            } else {
                                                $("#loadmoredata").hide();
                                            }



                                        }
                                    });
                                }
                            })
                    }
        );

        /*
        By:Jyoti Vishwakarma
        Description: category
        */
        function getCategories(pagetype) {
            var pagedata = {
                "pagetype": pagetype
            };
            $.ajax({
                url: serviceurl + 'homecategories',
                type: 'POST',
                data: JSON.stringify(pagedata),
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";                   
                    for (var i = 0; i < value.categories.length; i++) {
                        var id = value.categories[i].id;
                        var category = value.categories[i].category;
                            if(i == 0){
                                html += '<li class="snacks-wrapper activeTab" id="category-' + id + '" onclick="addActivetab(' + id + ')" data-id="' + id + '"><p><a><span class="text-check">' + category + '</span></span></a></p></li>';
                            }else{
                                html += '<li class="snacks-wrapper" id="category-' + id + '" onclick="addActivetab(' + id + ')" data-id="' + id + '"><p><a><span class="text-check">' + category + '</span></span></a></p></li>';
                            }

                    }
                    $("#categories").html(html);
                }
            });
        }

        /*
        By:Jyoti Vishwakarma
        Description: kitchen list
        */
        function getKitchens() {

            $.ajax({
                url: serviceurl + 'kitchens',
                type: 'POST',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                     var vl = value.kitchens.length;
                    for (var i = 0; i < vl; i++) {
                       html += '<div class="restro card" onclick="getKitchesData(' + value.kitchens[i].id + ')" id="kitchenDiv-' + value.kitchens[i].id + '"><div class="restroWrapContent"><img src="' + value.kitchens[i].image + '" alt="' + value.kitchens[i].name + '" /><div class="context"><h3>' + value.kitchens[i].name + '</h3><a href="javascript:goToKitchen(' + value.kitchens[i].id + ',' + '\'' + value.kitchens[i].name1.replace("&#39;","\\'")  + '\''+')"><h6 class="visitRestro">Visit Restaurant ></h6></a></div></div></div>';
                        
                        $("#kitchens").html(html);
                    }
                }
            });
        }
        /*
        By:Jyoti Vishwakarma
        Description: cuisines list
        */
        function getCuisines() {
            var category_id = $('.checkWrap  li.activeTab').attr("data-id");
            var pagedata = {
                "keyword": "",
                "categories": category_id,
                "sortbycost": "",
                "type": ""
            };
            $.ajax({
                url: serviceurl + 'cuisines',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                    if (value.cuisines.length > 0) {
                        for (var i = 0; i < value.cuisines.length; i++) {

                            if(value.cuisines[i].isCustomisable == '1'){
                                html += '<div class="cuisine card customize_cusine" onclick="customizecuisine('+ value.cuisines[i].id +')">';
                            }else{
                                html += '<div class="cuisine card">';
                            }
                           
                            html += '<div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img class="lazyload" data-src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description + '</p><div class="priceBlock">';
                            if(value.cuisines[i].discountprice != '' && value.cuisines[i].offer != ""){
                                html += '<p class="price"><strike>₹ ' + value.cuisines[i].price + '</strike>  ₹ ' + value.cuisines[i].discountprice + '</p>';
                            }else{
                                html += '<p class="price">₹ ' + value.cuisines[i].price + '</p>';
                            }
                            html +='<p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock" id="cuisineCatDiv-' + value.cuisines[i].id + '"><div class="hurryBlockContainer">';
                            if(value.cuisines[i].isCustomisable == '1'){
                                html += '<p>Customizable</p>';
                            }else{
                                html += '<p> </p>';
                            }
                            html += ' </div><div class="hurryBlockContainer">';
                            var offer = "";
                            if (value.cuisines[i].offer != "") {
                                offer =  '<span>'+value.cuisines[i].offer + '% off</span>';
                            }

                            var cuisintype = value.cuisines[i].type.split(",");
                                if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                    html += '<p class="food-cat">'+offer+'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                } else {
                                    html += '<p class="food-cat nonfood-cat">'+offer+'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                }
                            
                            html += '</div></div>';
                            if(value.cuisines[i].isCustomisable != '1'){
                                //html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="1" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';
                                var start = value.cuisines[i].kitchen_workingstart;
                                var end = value.cuisines[i].kitchen_workingend;

                                var d = new Date();
                                        var datesss = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                                        var min = new Date(datesss + ' ' + start);
                                        var max = new Date(datesss + ' ' + end);

                                        if(min.getTime() > d.getTime() || d.getTime() >  max.getTime()){
                                            html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '">Kitchen closed</p></div>';
                                        }
                                        else{
                                            html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="1" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';
                                        }

                            }
                            html += '</div></div></div>';
                      
                        }
                        if(value.cuisines.length < 30){
                            $("#loadmoredata").hide();
                        }
                        $("#cuisines").html(html);
                        addedIncart();
                        hoverCuisine();
                        adjustHeight();
                        slideCat();
                    } else {
                        $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");
                    }

                }
            });
        }

        /*
        By:Jyoti Vishwakarma
        Description: display meals
        */
        function getMeals() {
            var category_id = "";
            var kid = $("#kitchen_id").val();
            category_id = $('.checkWrap  li.activeTab').attr("data-id");
            if(kid != ""){
                getKitchesData(kid);
                return;
            }
            var mealtype = $("#meals_check").val();
            var pagedata = {
                "keyword": "",
                "categories": category_id,
                "sortbycost": "",
                "type": "",
                "kitchenid":"",
                "mealstype" : mealtype
            };
            $.ajax({
                url: serviceurl + 'meals',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    console.log(value);
                    var html = "";
                    if(value.cuisines.length > 0){
                        for (var i = 0; i < value.cuisines.length; i++) {
                             html += '<div class="cuisine card" onclick="corporate_order(\'' + value.cuisines[i].id + '\')"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description1 + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '<span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[i].minorderquantity + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[i].leadtime + ' hours lead time</p></div><div class="hurryBlockContainer">';
                            var cuisintype = value.cuisines[i].type.split(",");
                            if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                            } else {
                                html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                            }
                            html += '</div></div></div></div></div>';
                      
                        }
                       
                        $("#cuisines").html(html);
                        addedIncart();
                        adjustHeight();
                    }else{
                        $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");  
                    }

                }
            });
        }
            /*
        By:Jyoti Vishwakarma
        Description: Cuisine filter
        */
        function getCuisinesFilter() {
          
            var kitchenid = $("#kitchen_id").val();
            if(kitchenid != ""){
                getKitchesData(kitchenid);
                return;
            }
            var keyword = $("#search_inner").val();
            var sort_by_cost = $("#sort_by_cost").val();
            var category_id = $('.checkWrap  li.activeTab').attr("data-id");
            var keyLength = keyword.length;
            var type = "";
            if ($("#checkbox_veg").prop("checked") == true) {
                var type = "veg";
            } else {
                var type = "";
            }

            var categories = [];
            var pagedata = {
                "kitchenid": kitchenid,
                "keyword": keyword,
                "categories": category_id,
                "sortbycost": sort_by_cost,
                "type": type
            };
            var mealtype = $("#meals_check").val();
            if(mealtype == 'corporate_meals'){
                $.ajax({
                    url: serviceurl + 'cuisines',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: true,
                    success: function(data) {
                        var value = JSON.parse(data);
                        var html = "";
                        $(".loader").hide();
                        if (value.cuisines.length > 0) {
                            for (var i = 0; i < value.cuisines.length; i++) {
                                if(value.cuisines[i].isCustomisable == '1'){
                                    html += '<div class="cuisine card" onclick="customizecuisine('+ value.cuisines[i].id +')">';
                                }else{
                                    html += '<div class="cuisine card">';
                                }
                            
                                html += '<div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img class="lazyload" data-src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description + '</p><div class="priceBlock">';
                                if(value.cuisines[i].discountprice != '' && value.cuisines[i].offer != ""){
                                    html += '<p class="price"><strike>₹ ' + value.cuisines[i].price + '</strike>  ₹ ' + value.cuisines[i].discountprice + '</p>';
                                }else{
                                    html += '<p class="price">₹ ' + value.cuisines[i].price + '</p>';
                                }
                                html += '<p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock" id="cuisineCatDiv-' + value.cuisines[i].id + '"><div class="hurryBlockContainer">';
                                if(value.cuisines[i].isCustomisable == '1'){
                                    html += '<p>Customizable</p>';
                                }else{
                                    html += '<p> </p>';
                                }
                                html += ' </div><div class="hurryBlockContainer">';
                                var offer = "";
                                if (value.cuisines[i].offer != "") {
                                    offer =  '<span>'+value.cuisines[i].offer + '% off</span>';
                                }
                                var cuisintype = value.cuisines[i].type.split(",");
                                    if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                        html += '<p class="food-cat">'+offer+'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                    } else {
                                        html += '<p class="food-cat nonfood-cat">'+offer+'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                    }
                                
                                html += '</div></div>';
                                if(value.cuisines[i].isCustomisable != '1'){
                                   // html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="0" max="3" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';
                                   var start = value.cuisines[i].kitchen_workingstart;
                                    var end = value.cuisines[i].kitchen_workingend;

                                    var d = new Date();
                                            var datesss = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                                            var min = new Date(datesss + ' ' + start);
                                            var max = new Date(datesss + ' ' + end);

                                            if(min.getTime() > d.getTime() || d.getTime() >  max.getTime()){
                                                html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '">Kitchen closed</p></div>';
                                            }
                                            else{
                                                 html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="0" max="3" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';
                                            }

                                }
                                html += '</div></div></div>';
                            }
                            if(value.cuisines.length < 30){
                                $("#loadmoredata").hide();
                            }
                            $("#cuisines").html(html);
                            addedIncart();
                            hoverCuisine();
                            addButtonFunction();
                            adjustHeight();
                            slideCat();
                            
                        } else {
                            $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");
                        }


                    }
                });
            }else if(mealtype == 'society' || mealtype == 'kitty'){
                $.ajax({
                    url: serviceurl + 'meals',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        console.log(value);
                        var html = "";
                        if(value.cuisines.length > 0){
                            for (var i = 0; i < value.cuisines.length; i++) {
                                html += '<div class="cuisine card" onclick="corporate_order(\'' + value.cuisines[i].id + '\')"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description1 + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '<span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[i].minorderquantity + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[i].leadtime + ' hours lead time</p></div><div class="hurryBlockContainer">';
                                var cuisintype = value.cuisines[i].type.split(",");
                                if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                    html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                } else {
                                    html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                }
                                html += '</div></div></div></div></div>';
                        
                            }
                        
                            $("#cuisines").html(html);
                            addedIncart();
                            adjustHeight();
                        }else{
                            $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");  
                        }

                    }
                });
            }

        }

        function addActivetab(catid) {
            var categoryid = catid;
            $('.checkWrap  li.activeTab').removeClass('activeTab');
            $("#category-"+catid).addClass("activeTab");
            getCuisinesFilter();
        }

            /*
        By:Jyoti Vishwakarma
        Description: review form
        */
        $("form[name='review_form']").validate({
            rules: {
                user_id: "required",
                order_id: "required",
                rating: "required",
                comment: "required",
            },
            submitHandler: function(form) {
                var user_id = $("#user_id").val();
                var order_id = $("#order_id").val();
                var comment = $("#comment").val();
                var rating = $(".rating").val();

                var pagedata = {
                    "userId": user_id,
                    "orderid": order_id,
                    "feedback": comment,
                    "rating": rating
                };
                $.ajax({
                    url: serviceurl + 'getRating',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        if (value.status == 'success') {

                            $('#reviewPopup1').hide();
                        }

                    }
                });
            }
        });

        $( "#other" ).click(function() {
            checkLoginState();
        });
            /*
        By:Jyoti Vishwakarma
        Description: Get kitchen data
        */
        function getKitchesData(kitchenid) {
            $("#cuisines").html("");
            var kid = $("#kitchen_id").val();
            $("#kitchenDiv-"+kid).removeClass("activeRestro");
            $("#kitchenDiv-"+kitchenid).addClass("activeRestro");
            $("#kitchen_id").val(kitchenid);
            if ($("#checkbox_veg").prop("checked") == true) {
                var type = "veg";
            } else {
                var type = "";
            }
            var keyword = $("#search_inner").val();
            var mealtype = $("#meals_check").val();
            if(mealtype != 'society' && mealtype != 'kitty'){
                mealtype = 'corporate_meals';
            }
            var pagedata = {
                "kitchenid": kitchenid,
                "keyword": keyword,
                "categories": "",
                "sortbycost": "",
                "type": type,
                "mealtype": mealtype
            };
            $.ajax({
                url: serviceurl + 'kitchen_page',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var categories = "";
                    for (var i = 0; i < value.categories.length; i++) {
                        if (i == 0) {
                            categories += '<li class="snacks-wrapper activeTab"><a href="#category' + value.categories[i].categoryid + '"><p><span>' + value.categories[i].categoryname + '</span></span></p></a></li>';
                        } else {
                            categories += '<li class="snacks-wrapper"><a href="#category' + value.categories[i].categoryid + '"><p><span>' + value.categories[i].categoryname + '</span></span></p></a></li>';
                        }
                    }
                    $("#categories").html(categories);
                    if(value.cuisines.length == 0){
                        $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");
                        return;
                    }
                    if(mealtype == 'corporate_meals'){
                        var cuisinesdata = "";
                        if(value.cuisines.length > 0){
                            for (var j = 0; j < value.cuisines.length; j++) {
                                
                                cuisinesdata += '<div class="cuisine-divider" id="category' + value.cuisines[j].categoryid + '"> <div class="titleBlock"> <h2>' + value.cuisines[j].categoryname + '</h2> <p>' + value.cuisines[j].cuisines.length + ' Items</p> </div><div class="cuisineContainer">';
                            
                                for (var k = 0; k < value.cuisines[j].cuisines.length; k++) {
                                    if(value.cuisines[j].cuisines[k].isCustomisable == '1'){
                                        cuisinesdata += '<div class="cuisine card customize_cusine" onclick="customizecuisine('+ value.cuisines[j].cuisines[k].id +')">';
                                    }else{
                                        cuisinesdata += '<div class="cuisine card">';
                                    }
                                    cuisinesdata += '<div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img class="lazyload" data-src="' + value.cuisines[j].cuisines[k].image + '" alt="' + value.cuisines[j].cuisines[k].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[j].cuisines[k].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[j].cuisines[k].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[j].cuisines[k].description + '</p><div class="priceBlock">';
                                    if(value.cuisines[j].cuisines[k].discountprice != '' && value.cuisines[j].cuisines[k].offer != ""){
                                        cuisinesdata += '<p class="price"><strike>₹ ' + value.cuisines[j].cuisines[k].price + '</strike>  ₹ ' + value.cuisines[j].cuisines[k].discountprice + '</p>';
                                    }else{
                                        cuisinesdata += '<p class="price">₹ ' + value.cuisines[j].cuisines[k].price + '</p>';
                                    } 
                                    cuisinesdata += '<p>Serves ' + value.cuisines[j].cuisines[k].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="hurryBlockContainer">';
                                    if(value.cuisines[j].cuisines[k].isCustomisable == '1'){
                                        cuisinesdata += '<p>Customizable</p>';
                                    }else{
                                        cuisinesdata += '<p> </p>';
                                    }
                                    cuisinesdata += '</div><div class="hurryBlockContainer">';
                                    var offer = "";
                                    if (value.cuisines[j].cuisines[k].offer != "") {
                                        offer =  '<span>'+value.cuisines[j].cuisines[k].offer + '% off</span>';
                                    }

                                    var cuisintype = value.cuisines[j].cuisines[k].type.split(",");
                                    if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                        cuisinesdata += '<p class="food-cat">'+offer+'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                    } else {
                                        cuisinesdata += '<p class="food-cat nonfood-cat">'+offer+'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                    }
                                    cuisinesdata += '</div></div>';
                                    if(value.cuisines[j].cuisines[k].isCustomisable != '1'){
                                        //cuisinesdata += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[j].cuisines[k].id + '" onclick="addToCart(' + value.cuisines[j].cuisines[k].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[j].cuisines[k].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[j].cuisines[k].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[j].cuisines[k].id + '" class="quantity-input" value="1" min="1" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[j].cuisines[k].id + ',\'add\')"><h2>+</h2></a></div>';
                                        var start = value.cuisines[j].cuisines[k].kitchen_workingstart;
                                            var end = value.cuisines[j].cuisines[k].kitchen_workingend;

                                            var d = new Date();
                                            var datesss = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                                            var min = new Date(datesss + ' ' + start);
                                            var max = new Date(datesss + ' ' + end);

                                            if(min.getTime() > d.getTime() || d.getTime() >  max.getTime()){
                                                cuisinesdata += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[j].cuisines[k].id + '">Kitchen closed</p></div>';
                                            }
                                            else{
                                                cuisinesdata += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[j].cuisines[k].id + '" onclick="addToCart(' + value.cuisines[j].cuisines[k].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[j].cuisines[k].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[j].cuisines[k].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[j].cuisines[k].id + '" class="quantity-input" value="1" min="1" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[j].cuisines[k].id + ',\'add\')"><h2>+</h2></a></div>';
                                        }
                                    
                                    }
                                    cuisinesdata += '</div></div></div>';
                                }

                                cuisinesdata += '</div>';
                            }
                        }else{
                            cuisinesdata = "<div class='noCuisine'><p>No cuisine found.</p></div>";
                        }
                        $("#cuisines").html(cuisinesdata);
                        addedIncart();
                        hoverCuisine();
                        addButtonFunction();
                        adjustHeight();
                        slideCat();
                    }else if(mealtype == 'society' || mealtype == 'kitty'){
                        var cuisinesdata ="";
                            for (var j = 0; j < value.cuisines.length; j++) {
                                cuisinesdata += '<div class="cuisine-divider" id="category' + value.cuisines[j].categoryid + '"> <div class="titleBlock"> <h2>' + value.cuisines[j].categoryname + '</h2> <p>' + value.cuisines[j].cuisines.length + ' Items</p> </div><div class="cuisineContainer">';
                            
                                for (var k = 0; k < value.cuisines[j].cuisines.length; k++) {
                                    
                                    cuisinesdata += '<div class="cuisine card" onclick="corporate_order(\'' + value.cuisines[j].cuisines[k].id + '\')"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[j].cuisines[k].image + '" alt="' + value.cuisines[j].cuisines[k].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[j].cuisines[k].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[j].cuisines[k].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[j].cuisines[k].description + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[j].cuisines[k].price + '<span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[j].cuisines[k].minorderquantity + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[j].cuisines[k].leadtime + ' hours lead time</p></div><div class="hurryBlockContainer">';
                                    
                                    var cuisintype = value.cuisines[j].cuisines[k].type.split(",");
                                    if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                        cuisinesdata += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                    } else {
                                        cuisinesdata += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                    }
                                    cuisinesdata += '</div></div></div></div></div>';
                                    
                                }

                                cuisinesdata += '</div>';
                            }
                    
                        $("#cuisines").html(cuisinesdata);
                        addedIncart();
                        adjustHeight();
                    }
                   
                }
            });
        }

        function getOffer(){
            getCategories('1');
            var kitchenid = $("#kitchen_id").val();
            $("#kitchenDiv-"+kitchenid).removeClass("activeRestro");
            $("#kitchen_id").val('');
            getCuisines();
        }
        /*
        By:Jyoti Vishwakarma
        Description: Change party mela selection
        */
        function changePartyMealType(selected) {
            $("#societyli").removeClass("activeSub");
            $("#kittyli").removeClass("activeSub");
            $("#meals_check").val(selected);
            if(selected == "society"){
                // $(".meal-order").text("Personal Meals");
                var elmnt = document.getElementById("ScrollUpToDiv");
                elmnt.scrollIntoView(true); 
                getCategories('7');
                getMeals();
                $("#societyli").addClass("activeSub");
            }else if(selected == "kitty"){
                // $(".meal-order").text("Team Meals");
                var elmnt = document.getElementById("ScrollUpToDiv");
                elmnt.scrollIntoView(true); // Top
                getCategories('8');
                getMeals();
                $("#kittyli").addClass("activeSub");
            }
        }

            /*
        By:Jyoti Vishwakarma
        Description: Meal description dispay in popup
        */
        function corporate_order(id){
            var pagedata = {
                "id": id
            };
            $.ajax({
                url: serviceurl + 'corporatemeals_detail',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    console.log(value.cuisines[0].name);
                    var val = "";
                    $("#cuisine_id").val(value.cuisines[0].id);
                    $("#kitchen_open").val(value.cuisines[0].kitchen_open);
                    $("#kitchen_close").val(value.cuisines[0].kitchen_close);

                    if (value.cuisines[0].type.includes('Non Veg')) {
                        val = '<img src="images/icons/nonveg.svg" alt="Non Veg">' + value.cuisines[0].kitchen_name;
                    } else {
                        val = '<img src="images/icons/veg.svg" alt="Veg">' + value.cuisines[0].kitchen_name;
                    }
                    $("#cuisineTitle1").html(value.cuisines[0].name);
                    $("#order-img_cuisin").attr("src", value.cuisines[0].image);
                    $("#veg_val").html(val);
                    $("#cuisine-details").html(value.cuisines[0].description);
                    $("#pop_price").html(value.cuisines[0].price);
                    $("#mim_order").html("Min Order " + value.cuisines[0].minorderquantity + "pax");
                    $("#leadtime").html(value.cuisines[0].leadtime);
                    $("#teamMealMinQty").val(value.cuisines[0].minorderquantity);
                    $("#teamMealMinQty1").val(value.cuisines[0].minorderquantity);
                    var addon = "";
                    
                        var addonshtml = "";
                    if(value.cuisines[0].sections.length == 0){
                        $("#addonheading").hide();
                    }else{
                        $("#addonheading").show();
                    }    
                    for(var i=0;i<value.cuisines[0].sections.length;i++){
                        console.log(value.cuisines[0].sections[i].title);
                        var section_id = value.cuisines[0].sections[i].section_id;
                        var noofchoice = value.cuisines[0].sections[i].noofchoice;
                        addonshtml += '<h4 >'+value.cuisines[0].sections[i].title+'</h4><input id="da'+section_id+'" type="hidden" value="'+noofchoice+'"><input id="das'+section_id+'" type="hidden" value="'+noofchoice+'"><span id="counterror'+section_id+'" style="color:red;display:none">error</span><div id="prt'+section_id+'">';
                        var addon = value.cuisines[0].sections[i].addons;
                        

                        if(value.cuisines[0].sections[i].choicetype == 1){
                            addonshtml += '<div class="section-div" id="'+section_id+'"></div>';
                            for(var j=0;j<addon.length;j++){

                                addonshtml += '<label class="container">';
                                addonshtml += '<p>' + addon[j].detail + ' &#8377;' + addon[j].price + '</p>';
                                addonshtml += '<input type="checkbox" onclick="checkval('+section_id+','+addon[j].addon_id+')" id="'+addon[j].addon_id+'" class="addons_corporate" name="addon"  data-id="' + addon[j].addon_id + '" value="' + addon[j].addon_id + '">';
                                addonshtml += '<span class="checkmark"></span>';
                                addonshtml += '</label>';
                            }

                        }else{
                            addonshtml += '<div class="section-divs"></div>';
                            for(var j=0;j<addon.length;j++){
                                    var checked = "";
                                    if(j==0){
                                        checked = "checked";
                                    }
                                    addonshtml +=   ' <div class="checkboxWrap">';
                                    addonshtml +=    '<label class="container radioContainer">';
                                    addonshtml +=        '<p>' + addon[j].detail + ' &#8377;' + addon[j].price + '</p>';
                                    addonshtml +=       '<input type="radio" name="addOnPacagesRadio-'+ value.cuisines[0].sections[i].section_id +'" value="' + addon[j].addon_id + '" ' + checked + '>';
                                    addonshtml +=        '<span class="checkmark"></span>';
                                    addonshtml +=    '</label>';
                                    addonshtml +=  '</div>'; 
                            }

                        }
                        addonshtml +=  '</div>'; 
                    }

                    $("#addonlist").html(addonshtml);
                    $("#datepicker").val("");
                    $("#order_time").val("");
                    $("#quterror").hide();
                    $("#quterror").text("");
                    $(".addtocartpopup, .overlay")
                        .delay(400)
                        .fadeIn();

                }
            });

        }
            /*
        By:Jyoti Vishwakarma
        Description: validate requirement to place meal order 
        */
        function corporatemealorder() {

            var cuisine_id = $("#cuisine_id").val();
            var date = $("#datepicker").val();
            var time = $("#order_time").val();           
            var qty = parseInt($("#teamMealMinQty").val());
            var qty1 = parseInt($("#teamMealMinQty1").val());
            var numItems = $('.section-div').length
            if(numItems > 0){
                $('.section-div').each(function(){
                   var section_id = $(this).prop('id');
                   var minreq = $("#da"+section_id).val(); 
                   var qut1 = $("#das"+section_id).val();
                  
                    if(qut1 != 0){
                        $("#counterror"+section_id).css("display","block");
                        $("#counterror"+section_id).text("Minimum selection should be "+minreq+".");
                        $("#validateflag").val(1);
                    }else{
                        $("#counterror"+section_id).css("display","none");
                        $("#validateflag").val(0);
                    }
                   
                });
            }else{
                $("#validateflag").val(0);
            }
            
            var leadtime = parseInt($("#leadtime").html());
            var today = new Date();
            var minord = new Date(today.setHours(today.getHours()+leadtime));
            var orddate = date + ' ' + time;
            orddate = new Date(orddate);
            
            var min = new Date(date + ' ' + $("#kitchen_open").val());
            var max = new Date(date + ' ' + $("#kitchen_close").val());

            if(qty1 > qty || date == "" || time == ""){
                if(date == ""){
                    $("#quterror").show();
                    $("#quterror").text("Please select date.");
                }else if( time == ""){
                    $("#quterror").show();
                    $("#quterror").text("Please select time.");
                }else if(qty1 > qty){
                    $("#quterror").show();
                    $("#quterror").text("Min Order "+qty1+"pax");
                }
                return;
            }else if(minord.getTime() > orddate.getTime()){
                $("#quterror").show();
                $("#quterror").text("Order time should be greater than leadtime");
                return;
            }else if(min.getTime() > orddate.getTime() || orddate.getTime() >  max.getTime()){
                $("#quterror").show();
                $("#quterror").text("Order time should be between "+ $("#kitchen_open").val() +" and "+ $("#kitchen_close").val());
                return;
            }else{
                $("#quterror").hide();
            }

            var special_instruction = $("#special-instrustion").val();

            var items = $(".addons_corporate");
            var selectedaddons = "";
            for (var i = 0; i < items.length; i++) {
                if (items[i].type == 'checkbox' && items[i].checked == true){
                        selectedaddons += items[i].value + ",";
                }
            }

            $('#addonlist input[type=radio]:checked').each(function() {
                 selectedaddons += this.value + ",";
            });

            addToCartCorporate(cuisine_id, date, time, qty, special_instruction, selectedaddons);
            // alert(special_instruction);

        }
            /*
    By:Jyoti Vishwakarma
    Description: Add to cart meal 
    */
        function addToCartCorporate(id, date, time, qty, special_instruction, selectedaddons) {
            var count = 1;
            var data = {
                "id": id,
                "date": date,
                "time": time,
                "qty": qty,
                "special_instruction": special_instruction,
                "selectedaddons": selectedaddons
            };
            if($("#validateflag").val() == 0){
                    $.ajax({
                    url: serviceurl + 'addtocartCorporate',
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        if (value.status == 'success') {
                            var cartCount = $(".notiCount").html();
                            showCartData();
                            $(".popup-order, .overlay")
                            .delay(400)
                            .fadeOut();
                            $(".toast").fadeIn();
                            setTimeout(function(){
                                $(".toast").fadeOut();
                            }, 3000);
                            //$(".notiCount").html(parseInt(cartCount) + 1);
                        } else {

                            $('.overlay,.cartPopup').fadeIn();
                            setTimeout(function() {
                                $('.overlay,.cartPopup').fadeOut();
                            }, 1200);
                        }
                    }
                });
            }
            else{
                // $("#quterror").text("Min Order "+qty1+"pax");
            }
        }
            /*
    By:Jyoti Vishwakarma
    Description: Validation addons selection
    */
        function checkval(s,z){
            qut1 = $("#da"+s).val();
            qut = $("#da"+s).val();
            var q = "#das"+s;
            var qt = $(q).val();   
            var prt = "#prt"+s;   
            var ckId =  "#"+z;
            
                // alert($(q).val());
                if($(ckId).is(":checked")){
                    $(q).val(parseInt(qt) - 1);
                }
                else if($(ckId).is(":not(:checked)")){
                    $(q).val(parseInt(qt) + 1);
                    
                }
                
                if($(q).val() == 0 ){
                    $("#validateflag").val(0);
                    $("#counterror"+s).css("display","none");
                }
                else{
                    $("#counterror"+s).css("display","block");
                    $("#counterror"+s).text("Minimum selection should be "+qut1+".");
                    $("#validateflag").val(1);
                }
                        
        }
            /*
    By:Jyoti Vishwakarma
    Description: display offer cuisine
    */
        function getOfferedCuisines() {
            var keyword = $("#search_inner").val();
            var sort_by_cost = $("#sort_by_cost").val();
            var category_id = $('.checkWrap  li.activeTab').attr("data-id");
            var keyLength = keyword.length;
            var type = "";
            if ($("#checkbox_veg").prop("checked") == true) {
                var type = "veg";
            } else {
                var type = "";
            }

            var pagedata = {
                "keyword": "",
                "categories": category_id,
                "sortbycost": "",
                "type": ""
            };
            $.ajax({
                url: serviceurl + 'offercuisines',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                    if (value.cuisines.length > 0) {
                        for (var i = 0; i < value.cuisines.length; i++) {

                            if(value.cuisines[i].isCustomisable == '1'){
                                html += '<div class="cuisine card customize_cusine" onclick="customizecuisine('+ value.cuisines[i].id +')">';
                            }else{
                                html += '<div class="cuisine card">';
                            }
                           
                            html += '<div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img class="lazyload" data-src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description + '</p><div class="priceBlock">';
                            if(value.cuisines[i].discountprice != '' && value.cuisines[i].offer != ""){
                                html += '<p class="price"><strike>₹ ' + value.cuisines[i].price + '</strike>  ₹ ' + value.cuisines[i].discountprice + '</p>';
                            }else{
                                html += '<p class="price">₹ ' + value.cuisines[i].price + '</p>';
                            }
                            html +='<p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock" id="cuisineCatDiv-' + value.cuisines[i].id + '"><div class="hurryBlockContainer">';
                            if(value.cuisines[i].isCustomisable == '1'){
                                html += '<p>Customizable</p>';
                            }else{
                                html += '<p> </p>';
                            }
                            html += ' </div><div class="hurryBlockContainer">';
                            var offer = "";
                            if (value.cuisines[i].offer != "") {
                                offer =  '<span>'+value.cuisines[i].offer + '% off</span>';
                            }

                            var cuisintype = value.cuisines[i].type.split(",");
                                if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                    html += '<p class="food-cat">'+offer+'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                } else {
                                    html += '<p class="food-cat nonfood-cat">'+offer+'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                }
                            
                            html += '</div></div>';
                            if(value.cuisines[i].isCustomisable != '1'){
                                html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="1" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';
                            }
                            html += '</div></div></div>';
                      
                        }
                        if(value.cuisines.length < 30){
                            $("#loadmoredata").hide();
                        }
                        $("#cuisines").html(html);
                        addedIncart();
                        hoverCuisine();
                        adjustHeight();
                        slideCat();
                    } else {
                        $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");
                    }

                }
            });
        }
   
    </script>
    
</body>

</html>