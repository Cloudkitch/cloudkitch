<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

?>


<div class="preloader"></div>
<div class="loader"></div>

<div class="overlay" style="display:none"></div>


<div class="popup reviewPopup" id="reviewPopup1" style="display:none;">
    <div class="popup-wrapper">
        <div class="centerText">
            <img src="<?= $baseurl; ?>images/logo.svg" alt="CLOUDKITCH" class="popup-logo">
        </div>
        <div class="firstDiv">
            <form id="review_form" name="review_form">
                <input type="hidden" id="user_id" name="user_id" value="<?= $user_id ?>">
                <input type="hidden" id="order_id" name="order_id" value="<?= $order_id ?>">
                <h2>Review your order</h2><br>
                <div class="form-conatiner">
                    <div class="form-group form-float-group ratingGroup">
                        <p class="">Order No. <?= $order_id ?></p>
                        <fieldset class="ratingField">
                            <input type="radio" id="star5" class="rating" name="rating" value="5" /><label for="star5">5 stars</label>
                            <input type="radio" id="star4" class="rating" name="rating" value="4" /><label for="star4">4 stars</label>
                            <input type="radio" id="star3" class="rating" name="rating" value="3" /><label for="star3">3 stars</label>
                            <input type="radio" id="star2" class="rating" name="rating" value="2" /><label for="star2">2 stars</label>
                            <input type="radio" id="star1" class="rating" name="rating" value="1" /><label for="star1">1 star</label>
                        </fieldset>
                    </div>
                    <div class="form-group form-float-group">
                        <input type="text" placeholder="Comment here" id="comment" name="comment" autocomplete="off" class="form-float-control">
                    </div>
                    <button type="submit" class="btn-gradient">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="popup cartPopup">
    <div class="popup-wrapper">
        <h3>Already in cart.</h3>
    </div>
</div>



<div class="popup cartPopups">
    <div class="popup-wrapper">
        <h3>Thank you for your enquiry. We will be in touch shortly.</h3>
    </div>
</div>
<div class="popup cartPincodePopups">
    <div class="popup-wrapper">
        <h3>Your area is currently not serviceable.</h3>
    </div>
</div>

<div class="popup errorOrderTimePopup" style="width:35%">
    <div class="popup-wrapper">
        <h3>Please remove closed kitchen item from cart.</h3>
    </div>
</div>

<div class="popup profileUpdateAlertMsg">
    <div class="popup-wrapper">
        <h3>Profile updated successfully</h3>
    </div>
</div>


<div class="popup popup-login">
    <div class="popup-wrapper">
        <img src="<?= $baseurl; ?>images/logo.svg" alt="CLOUDKITCH" class="popup-logo">
        <div class="first-login" style="display:block;">
            <!-- <fb:login-button scope="public_profile,email"   onlogin="checkLoginState();">
                </fb:login-button> -->
            <a href="#" class="btn-outline btn-outline-img facebook" onclick="facebookLogin()" id="facebook-button">
                <p>Sign in with Facebook</p>
                <!-- <div scope="public_profile,email"  class="fb-login-button" data-width="" data-size="large" data-button-type="login_with" data-auto-logout-link="false" data-use-continue-as="false"></div> -->

            </a>
            <div id="customBtn" onclick="loginClick()">
                <a href="#" class="btn-outline btn-outline-img google">
                    <p>Sign in with Google</p>
                </a>
                <!-- <div id="customBtn" class="customGPlusSignIn" ></div> -->

            </div>
            <!-- <div id="gSignInWrapper">
                    <span class="label">Sign in with:</span>
                    <div id="customBtn" class="customGPlusSignIn">
                    <span class="icon"></span>
                    <span class="buttonText">Google</span>
                    </div>
                </div> -->
            <!-- <p id="socialMediaMsg" style="display:none;">Social media login coming soon.</p> -->
            <p class="login-with">OR</p>
            <a href="#" class="btn-gradient sign-in-manually">Sign In Manually</a>
            <a href="#" class="btn-gradient sign-in-manually-corporate">Sign In Manually</a>
            <!-- <a href="#" class="btn-outline corporate-log-in">Sign in with your Corporate ID</a> -->
            <p class="login-signup">Don't have an account? <a href="#" class="primary-link signUp">Sign Up</a></p>
        </div>
        <div class="sign-in" style="display:none;">
            <form id="login_form" name="login_form" class="signform">
                <h2>Sign In</h2>
                <div class="form-conatiner">
                    <div class="form-group form-float-group">
                        <input type="email" placeholder="" id="sign_in_email" name="sign_in_email" autocomplete="off" class="form-float-control">
                        <label for="sign_in_email" class="form-float-lab">Your Email</label>
                    </div>
                    <div class="form-group form-float-group">
                        <input type="password" autocomplete="off" placeholder="" id="sign_in_password" name="sign_in_password" class="form-float-control">
                        <label for="sign_in_password" class="form-float-lab">Password</label>
                        <a href="#" class="show-password">
                            <img src="<?= $baseurl; ?>images/icons/password.svg" alt="" class="passwordimg">
                        </a>
                    </div>
                    <div id="signin_alret_msg" style="color: red;font-size: 12px;"></div>
                    <a href="#" class="secondary-link forgotpassbtn">Forget Password ?</a>
                    <button type="submit" class="btn-gradient">Sign In</button>
                </div>
            </form>
            <p class="login-signup">Don't have an account? <a href="#" class="primary-link signUp">Sign Up</a></p>
        </div>
        <div class="corporate-sign-in" style="display:none;">
            <form id="corporate_login_form" name="corporate_login_form" class="corporateSignform">
                <h2 class="corporate-title">Corporate Sign In</h2>
                <div class="form-conatiner">
                    <div class="form-group form-float-group">
                        <input type="email" placeholder="" id="corp_sign_in_email" name="corp_sign_in_email" class="form-float-control">
                        <label for="corp_sign_in_email" class="form-float-lab">Your Email</label>
                    </div>
                    <div class="form-group form-float-group">
                        <input type="password" placeholder="" id="corp_sign_in_password" name="corp_sign_in_password" class="form-float-control">
                        <label for="corp_sign_in_password" class="form-float-lab">Password</label>
                        <a href="#" class="show-password">
                            <img src="<?= $baseurl; ?>images/icons/password.svg" alt="" class="passwordimg">
                        </a>
                        <!-- <a href="#" class="secondary-link forgotpassbtn">Forget Password ?</a> -->
                    </div>
                    <!-- <div class="form-group form-float-group">
                        <input type="text" placeholder="" id="corp_sign_in_id" name="corp_sign_in_id" class="form-float-control">
                        <label for="corp_sign_in_id" class="form-float-lab">Enter Your Company ID</label>
                    </div> -->
                    <button type="submit" class="btn-gradient">Sign In</button>
                </div>
                <p class="login-signup">Don't have an account? <a href="#" class="primary-link signUp">Sign Up</a></p>

            </form>
            <!-- <p class="login-signup"><a href="#" class="primary-link corporateSignUp">Link</a></p> -->
        </div>
        <div class="corporate-sign-up" style="display:none;">
            <form class="corporateSetPassform">
                <h2 class="corporate-title">Corporate Sign In</h2>
                <div class="form-group form-float-group">
                    <input type="email" placeholder="" id="corp-sign-in-email" name="corp-sign-in-email" class="form-float-control">
                    <label for="corp-sign-in-email" class="form-float-lab">Your Email or Phone</label>
                </div>
                <div class="form-group form-float-group">
                    <input type="text" placeholder="" id="corp-sign-in-id" name="corp-sign-in-id" class="form-float-control">
                    <label for="corp-sign-in-id" class="form-float-lab">Enter Your Company ID</label>
                </div>
                <a href="javascript:;" class="btn-gradient btnSetCorpPass">Next</a>
            </form>
        </div>
        <div class="corporate-login" style="display:none;">
            <h2 class="corporate-title">Do you have Corporate ID?</h2><br>
            <form class="">
                <div class="form-group form-float-group">
                    <input type="text" placeholder="" id="autologin_corp_code" name="autologin_corp_code" class="form-float-control">
                    <label for="autologin_corp_code" class="form-float-lab">Enter Your Company ID (Optional)</label>
                    <input type="hidden" name="google_id" id="google_id">
                    <input type="hidden" name="google_name" id="google_name">
                    <input type="hidden" name="google_imgurl" id="google_imgurl">
                    <input type="hidden" name="google_email" id="google_email">
                    <input type="hidden" name="social_type" id="social_type">
                </div>
                <button onclick="savegooglelogin()" class="btn-gradient">Next</button>
            </form>
        </div>
        <div class="corporate-set-password" style="display:none;">
        </div>
        <div class="forgot-password" style="display:none;">
            <form class="signform change-password-form">
                <h2>Change Password</h2>
                <div class="form-conatiner change-password-first">
                    <div class="form-group form-float-group">
                        <input type="email" placeholder="" id="forgotpass-email" name="forgotpass-email" class="form-float-control">
                        <label for="forgotpass-email" class="form-float-lab">Your Email</label>
                        <p id="forgotPasswordMsg" style="color: red"></p>
                    </div>
                    <a href="javascript:;" onclick="forgotPasswordEmail()" class="btn-gradient btnPassFirst">Change Password</a>
                </div>
                <div class="form-conatiner change-password-second" style="display:none;">
                    <div class="form-group form-float-group">
                        <input type="number" placeholder="" id="forgotpass-otp" name="forgotpass-otp" class="form-float-control">
                        <label for="forgotpass-otp" class="form-float-lab">Enter Your 6 Digit OTP</label>
                        <p id="forgotPasswordMsg1" style="color: red"></p>
                    </div>
                    <a href="javascript:;" onclick="forgotPasswordOTPVerify()" class="btn-gradient btnPassSecond">Change Password</a>
                </div>
                <div class="form-conatiner change-password-third" style="display:none;">
                    <div class="form-group form-float-group">
                        <input type="password" placeholder="" id="forgotpass-rep-password" name="forgotpass-rep-password" class="form-float-control">
                        <label for="forgotpass-rep-password" class="form-float-lab">New Password</label>
                        <div id="forgotPwdAlertMsg2" style="display:none;color:red;font-size: 12px;">Confirm password is not matched.</div>
                        <a href="#" class="show-password">
                            <img src="<?= $baseurl; ?>images/icons/password.svg" alt="" class="passwordimg">
                        </a>

                    </div>
                    <div class="form-group form-float-group">
                        <input type="password" placeholder="" id="forgotpass-new-password" name="forgotpass-new-password" class="form-float-control">
                        <label for="forgotpass-new-password" class="form-float-lab">Repeat Password</label>
                        <div id="forgotPwdAlertMsg3" style="display:none;color:red;font-size: 12px;">Confirm password is not matched.</div>
                        <a href="#" class="show-password">
                            <img src="<?= $baseurl; ?>images/icons/password.svg" alt="" class="passwordimg">
                        </a>

                    </div>

                    <a href="javascript:;" onclick="forgotPasswordUpdateNewPassword()" class="btn-gradient btnPassthird">Change Password</a>
                </div>
            </form>
            <div class="password-success" style="display:none;">
                <img src="<?= $baseurl; ?>images/icons/modal-check.svg" alt="" class="passwordimg">
                <h3>Password Changed Successfully</h3>
            </div>
            <p class="login-signup">Do have an account? <a href="#" class="primary-link signIn">Sign In</a></p>
        </div>
        <div class="sign-up" style="display:none;">
            <form id="signup_form" name="signup_form" class="signform form-signup">
                <h2>Sign Up</h2>
                <!-- <div id="facebookAlertMsg" style="display: none;color : red">We were unable to fetch your email ID,Please sign up manually.</div> -->
                <div class="form-conatiner">
                    <div class="form-group form-float-group">
                        <input type="text" placeholder="" autocomplete="off" id="sign_up_fullname" name="sign_up_fullname" class="form-float-control">
                        <label for="sign_up_fullname" class="form-float-lab">Your Full Name</label>
                    </div>
                    <div class="form-group form-float-group">
                        <input type="text" placeholder="" autocomplete="off" id="sign_up_phone" name="sign_up_phone" class="form-float-control">
                        <label for="sign_up_phone" class="form-float-lab">Your Phone Number</label>
                    </div>
                    <div class="form-group form-float-group">
                        <input type="email" placeholder="" autocomplete="off" id="sign_up_email" name="sign_up_email" class="form-float-control">
                        <div id="signup_alret_msg" style="color: red;font-size: 12px;"></div>
                        <label for="sign_up_email" class="form-float-lab">Your Email Address</label>
                    </div>
                    <div class="form-group form-float-group">
                        <input type="password" placeholder="" autocomplete="off" id="sign_up_password" name="sign_up_password" class="form-float-control">
                        <label for="sign_up_password" class="form-float-lab">Password</label>
                        <a href="#" class="show-password">
                            <img src="<?= $baseurl; ?>images/icons/password.svg" alt="" class="passwordimg">
                        </a>
                    </div>
                    <div class="form-group form-float-group">
                        <input type="password" placeholder="" autocomplete="off" id="sign_up_confirmpassword" name="sign_up_confirmpassword" class="form-float-control">
                        <label for="sign_up_confirmpassword" class="form-float-lab">Confirm Password</label>
                        <a href="#" class="show-password">
                            <img src="<?= $baseurl; ?>images/icons/password.svg" alt="" class="passwordimg">
                        </a>
                    </div>
                    <div class="form-group form-float-group">
                        <input type="text" placeholder="" id="sign_up_corporate" name="sign_up_corporate" class="form-float-control">
                        <label for="sign_up_corporate" class="form-float-lab"> Corporate Code(optional)</label>
                    </div>
                    <button type="submit" class="btn-gradient btnSignUp">Sign Up</button>
                </div>
            </form>
            <div class="signup-success" style="display:none;">
                <img src="<?= $baseurl; ?>images/icons/modal-check.svg" alt="" class="passwordimg">
                <h3>Account Created Successfully</h3>
            </div>
            <p class="login-signup normal-loginlink">Do have an account? <a href="#" class="primary-link signIn">Sign In</a></p>
            <p class="login-signup corporate-loginlink">Do have an account? <a href="#" class="primary-link signInCorporate">Sign In</a></p>
        </div>
        <div class="facebookAlertMsg" style="display: none;">
            <div class="popup-wrapper">
                <h3>We were unable to fetch your email ID,Please sign up manually.</h3>
            </div>
        </div>

    </div>
</div>

<div class="popup popup-corporate">
    <!-- <div class="popup-wrapper">
        <img src="<?= $baseurl; ?>images/logo.svg" alt="CLOUDKITCH" class="popup-logo">

        <h3>For Registered Corporate Clients</h3>
        <p>Connect us with <a href="tel:917710999666" class="connected-links">+91 771 099 9666</a> or write us at <a href="mailto:orders@cloudkitch.co.in" class="connected-links">orders@cloudkitch.co.in</a> </p>
    </div> -->
    <div class="popup-wrapper">
        <img src="<?= $baseurl; ?>images/logo.svg" alt="CLOUDKITCH" class="popup-logo">
        <form id="specialenquiry" name="specialenquiry" class="signform form-signup">
            <h2>Special Event Enquiry</h2>
            <div class="form-conatiner">
                <h4>Contact Details</h4>
                <div class="form-group form-float-group">
                    <input type="text" placeholder="" autocomplete="off" id="corpFullName" name="corpFullName" class="form-float-control">
                    <label for="corpFullName" class="form-float-lab">Full Name</label>
                </div>
                <div class="form-group form-float-group">
                    <input type="email" placeholder="" autocomplete="off" id="corpFullEmail" name="corpFullEmail" class="form-float-control">
                    <label for="corpFullEmail" class="form-float-lab">Email</label>
                </div>
                <div class="form-group form-float-group">
                    <input type="text" placeholder="" autocomplete="off" id="corpFullPhone" name="corpFullPhone" class="form-float-control">
                    <label for="corpFullPhone" class="form-float-lab">Phone</label>
                </div>
                <div class="form-group form-float-group">
                    <input type="text" placeholder="" autocomplete="off" id="corpFullCompany" name="corpFullCompany" class="form-float-control">
                    <label for="corpFullCompany" class="form-float-lab">Company</label>
                </div>
                <h4>Event Details</h4>
                <div class="form-group form-float-group">
                    <input type="text" placeholder="" autocomplete="off" id="corpEventDesc" name="corpEventDesc" class="form-float-control">
                    <label for="corpEventDesc" class="form-float-lab">Description of Event</label>
                </div>
                <div class="form-group form-float-group">
                    <input type="text" placeholder="" autocomplete="off" id="corpNoGuest" name="corpNoGuest" class="form-float-control">
                    <label for="corpNoGuest" class="form-float-lab">Number of guests</label>
                </div>
                <div class="form-group form-float-group">
                    <input type="text" placeholder="" autocomplete="off" id="corpFullBudget" name="corpFullBudget" class="form-float-control">
                    <label for="corpFullBudget" class="form-float-lab">Budget</label>
                </div>
                <div class="form-group form-float-group">
                    <input type="text" id="corpdatepicker" name="corpdatepicker" class="form-input-legend corp-date-time">
                    <label for="corpdatepicker" class="form-float-lab" style="z-index:1;">Event Date</label>
                </div>
                <div class="form-group form-float-group">
                    <input type="text" id="corptimepicker" name="corptimepicker" class="form-input-legend corptimepicker corp-date-time" />
                    <label for="corptimepicker" class="form-float-lab" style="z-index:1;">Event Time</label>
                </div>
                <!-- <div class="form-group form-legend form-event">
                    <input type="text" id="corptimepicker" name="corptimepicker" class="form-input-legend corptimepicker corp-date-time" placeholder="Event Time" />
                </div> -->
                <div class="form-group form-float-group">
                    <input type="text" placeholder="" autocomplete="off" id="corpeventlocation" name="corpeventlocation" class="form-float-control">
                    <label for="corpeventlocation" class="form-float-lab">Event Location</label>
                </div>
                <div class="form-group form-float-group">
                    <input type="text" placeholder="" autocomplete="off" id="corpCatered" name="corpCatered" class="form-float-control">
                    <label for="corpCatered" class="form-float-lab">What do you need catered?</label>
                </div>
                <div class="form-group form-float-group">
                    <input type="text" placeholder="" autocomplete="off" id="corpRestrictions" name="corpRestrictions" class="form-float-control">
                    <label for="corpRestrictions" class="form-float-lab">Preferred cuisines or any dietary restrictions? </label>
                </div>
                <div class="form-group form-float-group">
                    <input type="text" placeholder="" autocomplete="off" id="servingStyle" name="servingStyle" class="form-float-control">
                    <!-- <select class="form-float-control" id="servingStyle" name="servingStyle">
                        <option value=""></option>
                        <option value="Packed Meals">Packed Meals</option>
                        <option value="Finger Food">Finger Food</option>
                        <option value="Sharing Platters">Sharing Platters</option>
                        <option value="Mini Buffet (no set up)">Mini Buffet (no set up)</option>
                        <option value="Buffet (set up)">Buffet (set up)</option>
                        <option value="Live Station">Live Station</option>
                        <option value="Sit Down Dining">Sit Down Dining</option>
                    </select> -->
                    <label for="servingStyle" class="form-float-lab">Serving Style</label>
                </div>
                <div class="form-group form-float-group">
                    <input type="text" placeholder="" autocomplete="off" id="corpStaff" name="corpStaff" class="form-float-control">
                    <label for="corpStaff" class="form-float-lab">What type of staff do you need? </label>
                </div>
                <div class="form-group form-float-group">
                    <input type="text" placeholder="" autocomplete="off" id="corpRemarks" name="corpRemarks" class="form-float-control">
                    <label for="corpRemarks" class="form-float-lab">Other Remarks </label>
                </div>
            </div>
            <button type="submit" id="enquirySubmitBtn" class="btn-gradient btnSignUp">Submit</button>
        </form>
    </div>
</div>


<div class="popup popup-customize">
    <h2>Customize</h2>
    <p class="cuisineTitle" id="cuisineTitle"><img src="<?= $baseurl; ?>images/icons/veg.svg" alt="" class="customize-cat"> <span>Paneer Makhani Pizza</span> </p>
    <ul class="cusine-custom" id="cuisineCustomHeading">
        <li>
            <a href="#" class="popup-list-1 active">&#x2022; Select Crust</a>
        </li>
        <li>
            <a href="#" class="popup-list-2">&#x2022; Add Toppings</a>
        </li>
        <li>
            <a href="#" class="tab-list-3">&#x2022; Add on</a>
        </li>
        <li>
            <a href="#" class="tab-list-4">&#x2022; Beverages</a>
        </li>
    </ul>

    <div class="cusine-details" id="cuisineCustomDeatail">
        <div class="cusine-list popup-list-1 active">
            <h3>Select Crust</h3>
            <ul class="cusine-custom-list">
                <li>
                    <img src="<?= $baseurl; ?>images/icons/veg.svg" alt="" class="customize-cat">
                    <div class="checkboxWrap">
                        <label class="container">
                            <input type="checkbox">
                            <span class="checkmark"></span>
                            <!-- <p>New Hand Tossed  <span>(+ ₹ 25)</span></p> -->
                            New Hand Tossed <span>(+ ₹ 25)</span>
                        </label>
                    </div>
                </li>
                <li>
                    <img src="<?= $baseurl; ?>images/icons/veg.svg" alt="" class="customize-cat">
                    <div class="checkboxWrap">
                        <label class="container">
                            <input type="checkbox">
                            <span class="checkmark"></span>
                            Wheat Thin Crust <span>(+ ₹ 25)</span>
                        </label>
                    </div>
                </li>
                <li>
                    <img src="<?= $baseurl; ?>images/icons/veg.svg" alt="" class="customize-cat">
                    <div class="checkboxWrap">
                        <label class="container">
                            <input type="checkbox">
                            <span class="checkmark"></span>
                            Cheese Burst
                        </label>
                    </div>
                </li>
                <li>
                    <img src="<?= $baseurl; ?>images/icons/veg.svg" alt="" class="customize-cat">
                    <div class="checkboxWrap">
                        <label class="container">
                            <input type="checkbox">
                            <span class="checkmark"></span>
                            Fresh Pan Pizza
                        </label>
                    </div>
                </li>
            </ul>
        </div>

        <div class="cusine-list popup-list-2">
            <h3>Add Toppings</h3>
            <ul class="cusine-custom-list">
                <li>
                    <img src="<?= $baseurl; ?>images/icons/veg.svg" alt="" class="customize-cat">
                    <div class="checkboxWrap">
                        <label class="container">
                            <input type="checkbox">
                            <span class="checkmark"></span>
                            <!-- <p>Extra Cheese <span>(+ &#8377; 25)</span></p> -->
                            Extra Cheese <span>(+ &#8377; 25)</span>
                        </label>
                    </div>
                </li>
                <li>
                    <img src="<?= $baseurl; ?>images/icons/veg.svg" alt="" class="customize-cat">
                    <div class="checkboxWrap">
                        <label class="container">
                            <input type="checkbox">
                            <span class="checkmark"></span>
                            2x Vegetables <span>(+ &#8377; 25)</span>
                        </label>
                    </div>
                </li>
                <li>
                    <img src="<?= $baseurl; ?>images/icons/veg.svg" alt="" class="customize-cat">
                    <div class="checkboxWrap">
                        <label class="container">
                            <input type="checkbox">
                            <span class="checkmark"></span>
                            Parmesan Cheese <span>(+ &#8377; 25)</span>
                        </label>
                    </div>
                </li>
                <li>
                    <img src="<?= $baseurl; ?>images/icons/veg.svg" alt="" class="customize-cat">
                    <div class="checkboxWrap">
                        <label class="container">
                            <input type="checkbox">
                            <span class="checkmark"></span>
                            Fresh Pan Pizza
                        </label>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="cusine-pop-footer">
        <div class="cusine-pop-total">
            <p>Total <span>&#8377;</span><span id="cuisinetotalamt" style="margin-left: 0px">-</span></p>
            <input type="hidden" id="cuisinebaseamt">
        </div>
        <div class="cusine-pop-add">
            <div class="addBtn btn-add-popup-cart" id="addtocartbtn">
                <p class="addToCart">
                    <img src="<?= $baseurl; ?>images/icons/addCart.svg" alt="download">ADD to cart
                </p>
            </div>
        </div>
    </div>
</div>

<div class="toast">
    <p>Item Added to Cart Successfully</p>
</div>

<header>
    <div class="logo">
        <?php
        if ($_SESSION['isCorporateUser']) {
            echo '<a href="' . $baseurl . 'corporate"><img src="' . $baseurl . 'images/logo.svg" alt="Cloudkitch"></a>
        ';
        } else {
            echo '<a href="' . $baseurl . '"><img src="' . $baseurl . 'images/logo.svg" alt="Cloudkitch"></a>
        ';
        }
        ?>
    </div>
    <div class="searchWrap">
        <input type="text" id="search_main" autocomplete="off" placeholder="Search Hot Favourites">
        <input type="hidden" id="meals_check" value="<?php if (isset($_COOKIE["mealstype"])) {
                                                            echo $_COOKIE["mealstype"];
                                                        } else {
                                                            echo "corporate_meals";
                                                        } ?>">
    </div>
    <div class="linkWrap">
        <ul>
            <?php
            if (!isset($_SESSION['isCorporateUser'])) {
                $mystring = $_SERVER['REQUEST_URI'];
                $word = "party-menu";
                if (strpos($mystring, $word) !== false) {
                    echo '<li><a href="' . $baseurl . '">
                        <p class="btn borderBtn gradientBtn partyBtn">
                        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                            <path id="Icon_color" data-name="Icon color" d="M12.5,25A12.5,12.5,0,1,1,25,12.5,12.514,12.514,0,0,1,12.5,25Zm0-22.5a10,10,0,1,0,10,10A10.011,10.011,0,0,0,12.5,2.5Zm0,16.261A6.878,6.878,0,0,1,6.925,15.9a.613.613,0,0,1-.1-.488.643.643,0,0,1,.3-.412l1.087-.638a.638.638,0,0,1,.813.163,4.35,4.35,0,0,0,6.95,0,.638.638,0,0,1,.813-.163L17.875,15a.643.643,0,0,1,.3.412.616.616,0,0,1-.1.488A6.881,6.881,0,0,1,12.5,18.761Zm5.625-7.511H14.376a.626.626,0,0,1-.625-.625V10a2.5,2.5,0,0,1,5,0v.625A.626.626,0,0,1,18.125,11.25Zm-7.5,0H6.875a.626.626,0,0,1-.625-.625V10a2.5,2.5,0,0,1,5,0v.625A.626.626,0,0,1,10.625,11.25Z" transform="translate(0 0)" fill="#F68F30"/>
                            </svg>
                            Order Now</p></a>
                     </li>';
                } else {
                    echo '<li><a href="' . $baseurl . 'party-menu.php">
                     <p class="btn borderBtn gradientBtn partyBtn">
                     <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                        <path id="Icon_color" data-name="Icon color" d="M12.5,25A12.5,12.5,0,1,1,25,12.5,12.514,12.514,0,0,1,12.5,25Zm0-22.5a10,10,0,1,0,10,10A10.011,10.011,0,0,0,12.5,2.5Zm0,16.261A6.878,6.878,0,0,1,6.925,15.9a.613.613,0,0,1-.1-.488.643.643,0,0,1,.3-.412l1.087-.638a.638.638,0,0,1,.813.163,4.35,4.35,0,0,0,6.95,0,.638.638,0,0,1,.813-.163L17.875,15a.643.643,0,0,1,.3.412.616.616,0,0,1-.1.488A6.881,6.881,0,0,1,12.5,18.761Zm5.625-7.511H14.376a.626.626,0,0,1-.625-.625V10a2.5,2.5,0,0,1,5,0v.625A.626.626,0,0,1,18.125,11.25Zm-7.5,0H6.875a.626.626,0,0,1-.625-.625V10a2.5,2.5,0,0,1,5,0v.625A.626.626,0,0,1,10.625,11.25Z" transform="translate(0 0)" fill="#F68F30"/>
                        </svg>
                        Party Menu</p></a>
                     </li>';
                }

                echo '<li><a href="' . $baseurl . 'corporate-landing-page/">
                    <p class="btn borderBtn gradientBtn partyBtn">
                        For Corporates </p></a>
                </li>';
            }
            ?>

            <?php
            if (!isset($_SESSION['userid'])) {
            ?>

                <li>
                    <a>
                        <p class="btn borderBtn gradientBtn partyBtn linkBtn">

                            <svg xmlns="http://www.w3.org/2000/svg" width="18.765" height="21.05" viewBox="0 0 18.765 21.05">
                                <g id="profile" transform="translate(-1.932 -1)" fill="#fff" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M 11.31480026245117 21.05000114440918 C 8.989130020141602 21.05000114440918 6.746650218963623 20.76614189147949 5.162350177764893 20.27121162414551 C 3.786810159683228 19.84148025512695 2.932300329208374 19.2595100402832 2.932300329208374 18.75240135192871 C 2.932300329208374 17.25766181945801 3.496840238571167 15.83198070526123 4.564900398254395 14.62949085235596 C 5.626940250396729 13.43377113342285 7.145900249481201 12.53536128997803 8.841959953308105 12.09977054595947 L 10.90585041046143 11.56970119476318 L 9.179240226745605 10.32092094421387 C 7.986180305480957 9.458030700683594 7.273900032043457 8.064901351928711 7.273900032043457 6.594300746917725 C 7.273900032043457 4.060941219329834 9.334939956665039 1.999900937080383 11.86830043792725 1.999900937080383 C 14.40165996551514 1.999900937080383 16.46269989013672 4.060941219329834 16.46269989013672 6.594300746917725 C 16.46269989013672 8.200921058654785 15.64577007293701 9.663890838623047 14.27740001678467 10.50774097442627 L 12.47006034851074 11.62229061126709 L 14.48045063018799 12.30569076538086 C 16.02231025695801 12.82982063293457 17.33246040344238 13.71645069122314 18.2692699432373 14.86974143981934 C 19.20350074768066 16.01984024047852 19.69729995727539 17.36244010925293 19.69729995727539 18.75240135192871 C 19.69729995727539 19.25948143005371 18.84267044067383 19.84145164489746 17.4669303894043 20.27120018005371 C 15.88252067565918 20.76614189147949 13.64015007019043 21.05000114440918 11.31480026245117 21.05000114440918 Z" stroke="none" />
                                    <path d="M 11.31480026245117 20.05000114440918 C 13.20357990264893 20.05000114440918 15.03109073638916 19.85884094238281 16.46068000793457 19.51172065734863 C 17.85150337219238 19.17402076721191 18.49068450927734 18.77973175048828 18.6945915222168 18.59077644348145 C 18.65765953063965 17.49023056030273 18.24415397644043 16.42485618591309 17.49308013916016 15.50023078918457 C 16.67509078979492 14.49322128295898 15.52204036712646 13.71596145629883 14.15861034393311 13.25248050689697 C 13.4253101348877 13.00321102142334 12.90185070037842 12.35296058654785 12.8149299621582 11.58335113525391 C 12.72802066802979 10.81373119354248 13.09325981140137 10.06311130523682 13.75250053405762 9.656571388244629 C 14.8233699798584 8.996180534362793 15.46269989013672 7.851410865783691 15.46269989013672 6.594300746917725 C 15.46269989013672 4.612340927124023 13.85026073455811 2.999901056289673 11.86830043792725 2.999901056289673 C 9.886340141296387 2.999901056289673 8.273900032043457 4.612340927124023 8.273900032043457 6.594300746917725 C 8.273900032043457 7.745000839233398 8.831430435180664 8.835221290588379 9.765280723571777 9.510641098022461 C 10.39365005493164 9.965110778808594 10.70266056060791 10.74186134338379 10.55819034576416 11.50378131866455 C 10.41372013092041 12.26570129394531 9.841830253601074 12.87542057037354 9.090710639953613 13.06833076477051 C 7.585520267486572 13.45491123199463 6.243750095367432 14.2451810836792 5.312560081481934 15.29357051849365 C 4.448665142059326 16.26620674133301 3.974408626556396 17.40298843383789 3.934980392456055 18.59083938598633 C 4.13879919052124 18.77976417541504 4.777880668640137 19.17405128479004 6.168590068817139 19.5117301940918 C 7.598110198974609 19.85884094238281 9.425740242004395 20.05000114440918 11.31480026245117 20.05000114440918 M 11.31480026245117 22.05000114440918 C 6.623100280761719 22.05000114440918 1.932300209999084 20.95110130310059 1.932300209999084 18.75240135192871 C 1.932300209999084 15.15870094299316 4.738500118255615 12.12120056152344 8.59320068359375 11.13120079040527 C 7.188300132751465 10.1151008605957 6.273900032043457 8.461800575256348 6.273900032043457 6.594300746917725 C 6.273900032043457 3.504601001739502 8.778600692749023 0.9999009966850281 11.86830043792725 0.9999009966850281 C 14.95800018310547 0.9999009966850281 17.46269989013672 3.504601001739502 17.46269989013672 6.594300746917725 C 17.46269989013672 8.608501434326172 16.39890098571777 10.37430095672607 14.80230045318604 11.35890102386475 C 18.25740051269531 12.5334005355835 20.69729995727539 15.40080070495605 20.69729995727539 18.75240135192871 C 20.69729995727539 20.95110130310059 16.00559997558594 22.05000114440918 11.31480026245117 22.05000114440918 Z" stroke="none" fill="#f68f30" />
                                </g>
                            </svg>

                            Log in / Register</p>
                    </a>
                </li>
                <!-- <li>
                    <p class="btn gradientBtn signUp">NEW TO CLOUD KITCH ?<span>CREATE AN ACCOUNT</span></p>
                </li> -->
                <?php
            }
            if (isset($_SESSION['userid'])) {
                $image = $baseurl . "images/download.jpg";
                if ($_SESSION['userimage'] != "") {
                    $image = $_SESSION['userimage'];
                }
                if (isset($_SESSION['useraddress']['address']) && $_SESSION['useraddress']['address'] != "") {

                ?>

                    <li class="profile profile-add">
                        <div class="button-flex">
                            <p id="userAddTitle"><?= $_SESSION['useraddress']['addresstype'] ?></p>
                            <p class="small" id="userAddress"><?= substr($_SESSION['useraddress']['address'], 0, 22) ?>...</p>
                        </div>
                        <ul class="submenu">
                            <?php
                            error_reporting(0);
                            $userid = $_SESSION['userid'];
                            $query = "SELECT * FROM useraddresses WHERE userid = '$userid' ORDER BY useraddressid ASC";
                            $result = mysqli_query($conn, $query) or die(mysqli_error($conn));
                            while ($row = mysqli_fetch_assoc($result)) {

                                $address = $row['address'] . ', ' . $row['city'] . ', ' . $row['state'] . ', ' . $row['pincode'];
                                $address = substr($address, 0, 38) . "...";
                                echo '<li onclick="changeAddress(' . $row['useraddressid'] . ')">
                                        <a>
                                            <div class="button-flex">
                                                <p>' . $row['addresstitle'] . '</p>
                                                <p class="small">' . $address . '</p>
                                            </div>
                                        </a>
                                    </li>';
                            }

                            ?>

                        </ul>
                    </li>
                <?php } ?>
                <li class="profile">
                    <p><img src="<?= $image; ?>" alt="Profile"></p>
                    <ul class="submenu profileSubmenu">
                        <li>
                            <a href="<?= $baseurl; ?>profile/">
                                <p>My Profile</p>
                            </a>
                        </li>
                        <li>
                            <a href="<?= $baseurl; ?>order-history.php">
                                <p>My Orders</p>
                            </a>
                        </li>
                        <li>
                            <a onclick="logoutWeb()">
                                <p>Logout</p>
                            </a>
                        </li>
                    </ul>
                </li>
            <?php
            }
            ?>
            <li>
                <a href="<?= $baseurl; ?>cart/">
                    <p><img src="<?= $baseurl; ?>images/icons/headCart.svg" alt="Cart"><span class="notiCount">0</span></p>

                </a>
            </li>
            <?php

            $mystring = $_SERVER['REQUEST_URI'];
            if ($baseurl == $mystring ) {

            ?>
            <li class="meal-logo">
                <p><img src="<?= $baseurl; ?>images/food.png" alt="Meal" style="min-height: 30px;max-width: 100px !important;width:60px !important;"></p>
                <ul class="submenu mealSubmenu">
                    <li onclick="changePartyMealType('society')" id="societyli">
                        <a>
                            <p>
                            <svg xmlns="http://www.w3.org/2000/svg" width="80.006" height="56.002" viewBox="0 0 80.006 56.002">
                                <path d="M28.718,56H3.991A4,4,0,0,1,.43,50.2l5.76-11.56A11.939,11.939,0,0,1,16.926,32H39.04a19.745,19.745,0,0,0-5.962,7.038l-3.8,7.56a12.076,12.076,0,0,0-.56,9.4h0Zm47.3,0H40a4,4,0,0,1-3.561-5.8l4-7.6A11.97,11.97,0,0,1,50.965,36H65.049a12,12,0,0,1,10.723,6.6l3.8,7.6A4,4,0,0,1,76.013,56Zm-18-28h-.04a10.021,10.021,0,1,1,.04,0ZM28,24A12,12,0,1,1,40,12,12.018,12.018,0,0,1,28,24Z" transform="translate(0 0)" /></svg>Society
                            </p>
                        </a>
                    </li>
                    <li onclick="changePartyMealType('kitty')" id="kittyli">
                        <a>
                            <p>
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.333" height="54.835" viewBox="0 0 78.333 54.835">
                <path d="M76.376,54.835H1.96A1.962,1.962,0,0,1,0,52.875V48.959A1.962,1.962,0,0,1,1.96,47H3.916V43.083A35.225,35.225,0,0,1,35.25,8.069V1.96A1.962,1.962,0,0,1,37.21,0h3.916a1.96,1.96,0,0,1,1.956,1.96V8.069A35.222,35.222,0,0,1,74.416,43.083V47h1.96a1.96,1.96,0,0,1,1.956,1.96v3.916A1.96,1.96,0,0,1,76.376,54.835Zm-37.21-39.17A27.449,27.449,0,0,0,11.749,43.083V47H66.584V43.083A27.449,27.449,0,0,0,39.166,15.665Z"></path>
            </svg>Kitty Party
                            </p>
                        </a>
                    </li>
                </ul>
            </li>
    <?php }  ?>
            <?php
            if (isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] == '1') {
                if (isset($_SESSION['corporate_logo']) && $_SESSION['corporate_logo'] != "") {
                    $corp_logo = $_SESSION['corporate_logo'];
                } else {
                    $corp_logo = $baseurl . "images/logo.svg";
                }

                $corporateid = $_SESSION['corporateid'];

            ?>

                <li class="profile-logo">
                    <p><img src="<?= $corp_logo; ?>" alt="Corporate" style="min-height: 50px;max-width: 100px !important;"></p>


                    <?php
                    $mystring = $_SERVER['REQUEST_URI'];
                    $word = "corporate";
                    if (strpos($mystring, $word) !== false) {
                        $query = "SELECT meal_type FROM corporate_mealtype WHERE corporate_id = '$corporateid'";
                        $result = mysqli_query($conn, $query) or die(mysqli_error($conn));
                        $solutions = array();
                        if(mysqli_num_rows($result) > 0){
                            while ($row = mysqli_fetch_assoc($result)) {
                                $solutions[] = $row['meal_type'];
                            }

                            echo '<ul class="submenu corporateSubmenu">';

                        if (in_array("personal_meals", $solutions)) {  ?>
                            <li onclick="changeChooseOffering('corporate_meals')" id="corporateli">
                                <a>
                                    <p><svg xmlns="http://www.w3.org/2000/svg" width="50.004" height="50" viewBox="0 0 50.004 50">
                                    <path d="M47.508,50H2.495A2.5,2.5,0,0,1,.269,46.375L5,36.9A12.435,12.435,0,0,1,16.175,30H33.83a12.435,12.435,0,0,1,11.178,6.9l4.725,9.475a2.476,2.476,0,0,1-.1,2.432A2.511,2.511,0,0,1,47.508,50ZM25,25A12.5,12.5,0,1,1,37.5,12.5,12.515,12.515,0,0,1,25,25Z" /></svg>Personal Meals</p>
                                </a>
                            </li>
                <?php   } 
                        if (in_array("team_meals", $solutions)) {   ?>
                            <li onclick="changeChooseOffering('team_meals')" id="teamli">
                                <a>
                                <p><svg xmlns="http://www.w3.org/2000/svg" width="80.006" height="56.002" viewBox="0 0 80.006 56.002">
                                    <path d="M28.718,56H3.991A4,4,0,0,1,.43,50.2l5.76-11.56A11.939,11.939,0,0,1,16.926,32H39.04a19.745,19.745,0,0,0-5.962,7.038l-3.8,7.56a12.076,12.076,0,0,0-.56,9.4h0Zm47.3,0H40a4,4,0,0,1-3.561-5.8l4-7.6A11.97,11.97,0,0,1,50.965,36H65.049a12,12,0,0,1,10.723,6.6l3.8,7.6A4,4,0,0,1,76.013,56Zm-18-28h-.04a10.021,10.021,0,1,1,.04,0ZM28,24A12,12,0,1,1,40,12,12.018,12.018,0,0,1,28,24Z" transform="translate(0 0)" /></svg>Team Meals</p>
                                </a>
                            </li>
                <?php   }
                        if(in_array("event_meals", $solutions)) {       ?>
                            <li onclick="changeChooseOffering('event_meals')" id="eventli">
                                <a>
                                    <p><svg xmlns="http://www.w3.org/2000/svg" width="78.333" height="54.835" viewBox="0 0 78.333 54.835">
                                    <path d="M76.376,54.835H1.96A1.962,1.962,0,0,1,0,52.875V48.959A1.962,1.962,0,0,1,1.96,47H3.916V43.083A35.225,35.225,0,0,1,35.25,8.069V1.96A1.962,1.962,0,0,1,37.21,0h3.916a1.96,1.96,0,0,1,1.956,1.96V8.069A35.222,35.222,0,0,1,74.416,43.083V47h1.96a1.96,1.96,0,0,1,1.956,1.96v3.916A1.96,1.96,0,0,1,76.376,54.835Zm-37.21-39.17A27.449,27.449,0,0,0,11.749,43.083V47H66.584V43.083A27.449,27.449,0,0,0,39.166,15.665Z"/></svg>Corporate Events</p>
                                </a>
                            </li>
                <?php   } 
                        if (in_array("cafeteria", $solutions)) {    ?>
                            <li onclick="changeChooseOffering('cafeteria')" id="cafeteriali">
                                <a>
                                    <p><svg xmlns="http://www.w3.org/2000/svg" width="79.999" height="64.001" viewBox="0 0 79.999 64.001">
                                    <path d="M46.361,64H33.638a3.973,3.973,0,0,1-2.8-1.159L29.16,61.16A3.977,3.977,0,0,0,26.359,60H4a4,4,0,0,1-4-4V4A4,4,0,0,1,4,0H28A15.991,15.991,0,0,1,40,5.479,16.006,16.006,0,0,1,52,0H76a4,4,0,0,1,4,4V56a4,4,0,0,1-4,4H53.64a4.032,4.032,0,0,0-2.8,1.159l-1.678,1.681A4.032,4.032,0,0,1,46.361,64ZM52,8a8.008,8.008,0,0,0-8,8v34.16A16.068,16.068,0,0,1,52,48H72V8ZM8,8V48H28a16.044,16.044,0,0,1,8,2.16V16a8.008,8.008,0,0,0-8-8Z"/></svg>Cafeteria Menu</p>
                                </a>
                            </li>
            <?php            } 
                            echo ' </ul>';
                        }
                    }
        ?>



        </li>
    <?php }      ?>
        </ul>
    </div>
</header>

<div class="mobHeader">
    <div class="logo">
        <!-- <a href="<?= $baseurl; ?>"><img src="<?= $baseurl; ?>images/logo.svg" alt="Cloudkitch"></a> -->
        <?php
        if ($_SESSION['isCorporateUser']) {
            echo '<a href="' . $baseurl . 'corporate"><img src="' . $baseurl . 'images/logo.svg" alt="Cloudkitch"></a>
                ';
        } else {
            echo '<a href="' . $baseurl . '"><img src="' . $baseurl . 'images/logo.svg" alt="Cloudkitch"></a>
                ';
        }
        ?>
    </div>
    <div class="mobCart">
        <a href="<?= $baseurl; ?>cart/">
            <p><img src="<?= $baseurl; ?>images/icons/headCart.svg" alt="Cart"><span class="notiCount">10</span></p>
        </a>
    </div>
    <li class="meal-logo">
                <p><img src="<?= $baseurl; ?>images/food.png" alt="Meal" style="min-height: 30px;max-width: 100px !important;width:60px !important;"></p>
                <ul class="submenu mealSubmenu">
                    <li><a><p>Society</p></a></li>
                    <li><a><p>Kitty Party</p></a></li>
                </ul>
            </li>
    <?php

    if (isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] == '1') {
        if (isset($_SESSION['corporate_logo']) && $_SESSION['corporate_logo'] != "") {
            $corp_logo = $_SESSION['corporate_logo'];
        } else {
            $corp_logo = $baseurl . "images/logo.svg";
        }

    ?>
        <div class="profile-logo">
            <!-- <p><img src="<= $baseurl; ?>images/restro/Market-Bistro.svg" alt="Corporate"></p> -->
            <p><img src="<?= $corp_logo; ?>" alt="Corporate"></p>


            <?php
            $mystring = $_SERVER['REQUEST_URI'];
            $word = "corporate";
            if (strpos($mystring, $word) !== false) {
                // $corporateid = $_SESSION['corporateid'];
                $query = "SELECT meal_type FROM corporate_mealtype WHERE corporate_id = '$corporateid'";
                $result = mysqli_query($conn, $query) or die(mysqli_error($conn));
                $solutions = array();
                if(mysqli_num_rows($result) > 0){
                    while ($row = mysqli_fetch_assoc($result)) {
                        $solutions[] = $row['meal_type'];
                    }
                    echo '<ul class="submenu corporateSubmenu">';
                    if (in_array("personal_meals", $solutions)) {  ?>
                        <li onclick="changeChooseOffering('corporate_meals')" id="corporateli2">
                            <a>
                                <p>Personal Meals</p>
                            </a>
                        </li>
        <?php    }
                    if (in_array("team_meals", $solutions)) {   ?>
                        <li onclick="changeChooseOffering('team_meals')" id="teamli2"><a>
                                <p>Team Meals</p>
                            </a></li>
        <?php     }
                    if (in_array("event_meals", $solutions)) {  ?>
                        <li onclick="changeChooseOffering('event_meals')" id="eventli2">
                            <a>
                                <p>Corporate Events</p>
                            </a>
                        </li>
        <?php     } 
                    if (in_array("cafeteria", $solutions)) {    ?>
                        <li onclick="changeChooseOffering('cafeteria')" id="cafeteriali2">
                            <a>
                                <p>Cafeteria Menu</p>
                            </a
                        ></li>
        <?php     } 
                    echo '</ul>';
                }
            }
            ?>

        <?php   }   ?>
        </div>

        <div class="menuBtn">
            <div class="line l1"></div>
            <div class="line l2"></div>
            <div class="line l3"></div>
        </div>
</div>

<div class="mobNavbar">
    <ul>
        <?php
        if (!isset($_SESSION['userid'])) {
        ?>
            <li><a href="/party-menu.php" class="linkBtn">
                    <p>Party Menu</p>
                </a></li>
            <li><a href="/corporate-landing-page/" class="linkBtn">
                    <p>For Corporates</p>
                </a></li>
            <li><a href="javascript:;" class="linkBtn">
                    <p>Login</p>
                </a></li>
            <li><a class="signUp">
                    <p>Sign Up</p>
                </a></li>
        <?php
        }
        ?>
        <?php
        if (isset($_SESSION['userid'])) {
        ?>
            <li>
                <a href="<?= $baseurl; ?>profile/">
                    <p>My Profile</p>
                </a>
            </li>
            <li>
                <a href="<?= $baseurl; ?>order-history.php">
                    <p>My Orders</p>
                </a>
            </li>
            <li>
                <a onclick="logoutWeb()">
                    <p>Logout</p>
                </a>
            </li>
        <?php
        }
        ?>
        <!-- <li><a><p>Get Our App</p></a></li> -->
        <!-- <li><a href="javascript:;" class="corporate-login-link">
                <p>Corporate Login</p>
            </a></li> -->
        <li>
            <div class="searchWrap-mob">
                <form>
                    <input type="text" placeholder="Search Hot Favourites" id="mobSearch">
                    <button onClick="searchCusine();"><img src="<?= $baseurl; ?>images/icons/search.svg" alt="Search"></button>
                </form>
            </div>
        </li>
    </ul>
</div>