<!DOCTYPE html> 
<html>
<head>
    <?php
    include 'head.php';
    
    ?>
    <title>Order Deatils - orderid | Cloudkitch</title>
    <meta name="description" content="CloudKitch introduces smart kitchens that are connected with innovative technologies which are quintessential to bring success to any restaurant.">
</head>
<body class="order-details">
    <?php
    include 'header.php';
    ?>
    <?php
    if ($_GET['status']=='success') {
        ?>
        <section class="section topSection message-order">
            <div class="message-container">
                <div class="message-check">
                    <img src="<?=$baseurl;?>images/icons/white-check.svg" alt="check">
                </div>
                <div class="message-details">
                    <div class="meassage-wrapper">
                        <h2 id="deliveryMsg">Thanks,Your order has been placed successfully</h2>
                        <p>Order ID : <span class="order-id"><?= $_GET['orderId'] ?></span></p>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="cart-wrapper">
                <div class="cart-item cart-profile">
                    <h2>Order ID: <span class="order-id" id="order-id" ><?= $_GET['orderId'] ?></span></h2>
                    <p class="order-placed">Placed on : <span class="order-date" id="order-date">08 Nov 2019</span></p>
                </div>
                <div class="cart-item ">
                    <div class="button-wrapper">
                        <a  class="btn-gray order-cancel" onclick="cancelorder()"><img src="<?=$baseurl;?>images/icons/order-cancel.svg" alt="cancel"> Cancel</a>
                        <a href="<?=$baseurl?>order-history.php"  class="btn-green manage-order">Manage your order</a>
                    </div>
                </div>
            </div>

        </section>
        <section class="section">
            <div class="cart-wrapper">
                <div class="cart-item cart-profile">
                    <div class="address-block">
                        <h3 class="address-title">Shipping Information</h3>
                        <p class="address-details" id="address-details"></p>
                    </div>
                    <div class="address-block">
                        <h3 class="address-title">Customer Information</h3>
                        <p class="address-details" id="address-detailsC"></p>
                    </div>
                    <div class="address-location">
                        <ul>
                            <li>
                                <div class="address-icon">
                                    <img id="placed-icon" src="<?=$baseurl;?>images/icons/order-check.svg" id="placed-icon" alt="Placed">
                                </div>
                                <h2>Placed</h2>
                                <div class="address-time"><p id="ordTime">12:45 pm</p></div>
                                <div class="address-date"><p id="ordDate">08 Nov 2019</p></div>
                            </li>
                            <li>
                                <div class="address-icon">
                                    <img id="accepted-icon" src="<?=$baseurl;?>images/icons/accepted.svg" alt="Accepted">
                                </div>
                                <h2>Accepted</h2>
                                <!-- <div class="address-date"><p id="acceptedDate">08 Nov 2019</p></div> -->
                            </li>
                            <li>
                                <div class="address-icon">
                                    <img id="preparing-icon" src="<?=$baseurl;?>images/icons/preparing.svg" alt="Preparing">
                                </div>
                                <h2>Preparing</h2>
                                <!-- <div class="address-date"><p id="preparingDate">08 Nov 2019</p></div> -->
                            </li>
                            <li>
                                <div class="address-icon">
                                    <img id="dispatch-icon" src="<?=$baseurl;?>images/icons/truck.svg" alt="On the way">
                                </div>
                                <h2>On the way</h2>
                                <!-- <div class="address-date"><p id="dispatchDate">08 Nov 2019</p></div> -->
                            </li>
                            <li>
                                <div class="address-icon">
                                    <img src="<?=$baseurl;?>images/icons/delivery.svg" alt="Delivered" id="delivery-icon">
                                </div>
                                <h2 id="deliveryMsg1">Delivery by</h2>
                                <div class="address-time"><p id="delTime">01:25 pm</p></div>
                                <div class="address-date"><p id="delDate">08 Nov 2019</p></div>
                            </li>
                        </ul>
                        <div class="location-line"></div>
                        <?php
                        if (isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] == 1) {}else{
                        ?>
                        <div class="repeat-order">
                            <p>You will receive next update when the item in your order is packed/ shipped by the seller.</p>
                            <a href="#" class="btn-green order-repeat" onclick="repeatOrder()">Repeat Order</a>
                        </div>
                        <?php    }   ?>
                    </div>
                </div>
                <div class="cart-item  ">
                    <div class="cart-order order-details-list">
                        <h2 class="bill-title">Bill details</h2>
                        <div class="order-details-item order-details-title order-context-title">
                            <div class="order-detail-content order-deatils-item"><p>Item details</p></div>
                            <div class="order-detail-content order-deatils-price"><p>Quantity</p></div>
                            <div class="order-detail-content order-deatils-price"><p>Price</p></div>
                            <div class="order-detail-content order-deatils-subtotal"><p>Subtotal</p></div>
                        </div>
                        <div id="listdata">

<!-- Add ons UI -->
<!-- <div class="order-details-item">
    <p><b>Add ons</b></p>
</div>
<div class="chipsWrap orderDetailsAddOn">
    <div class="chip">Cheez (+30)</div>
    <div class="chip">Kimchi Salad (+50)</div>
    <div class="chip">Ice Tea (+50)</div>
</div> -->
                        </div>
                        <div class="order-details-item order-details-title">
                            <div class="order-detail-content order-deatils-item" style="visibility:hidden"><p>Item details</p></div>
                            <div class="order-detail-content order-deatils-price" style="visibility:hidden"><p>Quantity</p></div>
                            <div class="order-detail-content order-deatils-price" ><p>2.5% SGST</p></div>
                            <div class="order-detail-content order-deatils-subtotal"><p>&#8377; <span id="sgstamount"></span></p></div>
                        </div>
                        <div class="order-details-item order-details-title">
                            <div class="order-detail-content order-deatils-item" style="visibility:hidden"><p>Item details</p></div>
                            <div class="order-detail-content order-deatils-price" style="visibility:hidden"><p>Quantity</p></div>
                            <div class="order-detail-content order-deatils-price"><p>2.5% CGST</p></div>
                            <div class="order-detail-content order-deatils-subtotal"><p>&#8377; <span id="cgstamount"></span></p></div>
                        </div>
                        <div class="order-details-item order-details-title" id="promocode1" style="display:none;">
                            <div class="order-detail-content order-deatils-item" style="visibility:hidden"><p>Item details</p></div>
                            <div class="order-detail-content order-deatils-price" style="visibility:hidden"><p>Quantity</p></div>
                            <div class="order-detail-content order-deatils-price"><p>Discount</p></div>
                            <div class="order-detail-content order-deatils-subtotal"><p>- &#8377; <span id="discountamount"></span></p></div>
                        </div>

                        <div class="order-details-item order-details-title" id="walletpointsdiv" style="display:none;">
                            <div class="order-detail-content order-deatils-item" style="visibility:hidden"><p>Item details</p></div>
                            <div class="order-detail-content order-deatils-price" style="visibility:hidden"><p>Quantity</p></div>
                            <div class="order-detail-content order-deatils-price"><p>Wallet Point</p></div>
                            <div class="order-detail-content order-deatils-subtotal"><p>- <span id="walletamount"></span></p></div>
                        </div>

                        <div class="order-details-item order-details-title">
                            <div class="order-detail-content order-deatils-item" style="visibility:hidden"><p>Item details</p></div>
                            <div class="order-detail-content order-deatils-price" style="visibility:hidden"><p>Quantity</p></div>
                            <div class="order-detail-content order-deatils-price"><p>Total</p></div>
                            <div class="order-detail-content order-deatils-subtotal"><p>&#8377; <span id="totalamount"></span></p></div>
                        </div>
                    </div>
                    <a href="<?=$baseurl;?>order-history.php"><p class="btn borderBtn viewBtn"><img src="<?=$baseurl;?>images/icons/clock.svg" alt="download">View Order History</p></a>
                </div>
            </div>
        </section>
        <?php
    }
    else
    {
        ?>
        <section class="section topSection message-order">
            <div class="message-container">
                <div class="message-check">
                    <img src="<?=$baseurl;?>images/icons/white-check.svg" alt="check">
                </div>
                <div class="message-details">
                    <div class="meassage-wrapper">
                        <h2>Transaction has been tampered. Please try again</h2>
                        <!-- <p>Order ID : <span class="order-id"><?= $_GET['orderId'] ?></span></p> -->
                    </div>
                </div>
            </div>
        </section>
        <?php
    }
    include 'footer.php';
    ?>
</body>
<script>
    $(document).ready(function(){
        $("#promocode1").hide();
        getOrderHistory();
        activeChoosedMealType(); 
    });
        /*
    By:Jyoti Vishwakarma
    Description: Order details
    */
    function getOrderHistory(){
        var invc = '';

        invc = '1111';  
        var orderIds = '<?=  $_GET['orderId'] ?>';
        var cfo = {"invitecode":invc,"orderid":orderIds};

        $.ajax({
            url: 'cloudkitchadmin/service.php?servicename=chefordersDetail',
            type: 'POST',
            datatype: 'JSON',
            data: JSON.stringify(cfo),
            async: false,
            success: function(data)
            {
                var html="";
                var rscol = JSON.parse(data);

                var co = new Array();
                

                const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
                for(var oc=0;oc<rscol.cord.length;oc++)
                {
                            // alert(rscol.cord[oc].orderid);
                            if(rscol.cord[oc].orderid ==  orderIds){
                                var orderId = rscol.cord[oc].orderid;
                                var quantity = rscol.cord[oc].quantity;
                                var cname = rscol.cord[oc].cuisinename;
                                var discountamount = rscol.cord[oc].discountamount;
                                var couponapplied = rscol.cord[oc].couponapplied;
                                var kName = rscol.cord[oc].chefname;
                                var deliveryaddress = rscol.cord[oc].delivery_address;
                                var chefaddress = rscol.cord[oc].chefaddress;
                                var cloudkitchpoints = rscol.cord[oc].cloudkitchpoints;
                                var totalamount = rscol.cord[oc].totalamount;
                                
                                let current_datetime = new Date(rscol.cord[oc].orderdate)
                                let d = current_datetime.getDate() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getFullYear()
                                var date = d;
                                var amount = rscol.cord[oc].price;
                                var total = rscol.cord[oc].total;

                                var hours = current_datetime.getHours();
                                var minutes = current_datetime.getMinutes();
                                var ampm = hours >= 12 ? 'pm' : 'am';
                                hours = hours % 12;
                                hours = hours ? hours : 12; // the hour '0' should be '12'
                                minutes = minutes < 10 ? '0'+minutes : minutes;
                                var ordTime = hours + ':' + minutes + ' ' + ampm;


                                let delivery_datetime = new Date(rscol.cord[oc].delivery_time)
                                let d1 = delivery_datetime.getDate() + "-" + months[delivery_datetime.getMonth()] + "-" + delivery_datetime.getFullYear()
                                var deliverydate = d1;

                                var hours = delivery_datetime.getHours();
                                var minutes = delivery_datetime.getMinutes();
                                var ampm = hours >= 12 ? 'pm' : 'am';
                                hours = hours % 12;
                                hours = hours ? hours : 12; // the hour '0' should be '12'
                                minutes = minutes < 10 ? '0'+minutes : minutes;
                                var deleveryTime = hours + ':' + minutes + ' ' + ampm;
                                var current = new Date();
                                //console.log(rscol.cord[oc].delivery_time);
                                var orderstatus = rscol.cord[oc].orderstatus;
                                
                                if(orderstatus == '1'){
                                    $("#accepted-icon").attr("src", "<?=$baseurl;?>images/icons/order-check.svg");
                                    $("#preparing-icon").attr("src", "<?=$baseurl;?>images/icons/order-check.svg");
                                }else if(orderstatus == '3'){
                                    $("#accepted-icon").attr("src", "<?=$baseurl;?>images/icons/order-check.svg");
                                    $("#preparing-icon").attr("src", "<?=$baseurl;?>images/icons/order-check.svg");
                                    $("#dispatch-icon").attr("src", "<?=$baseurl;?>images/icons/order-check.svg");
                                    $(".order-cancel").attr("disabled",true);
                                    $('.order-cancel').css("pointer-events", "none");
                                }
                               
                                if(delivery_datetime.getTime() <= current.getTime() && orderstatus != '4' && orderstatus == '3' ){
                                    $("#delivery-icon").attr("src", "<?=$baseurl;?>images/icons/delivered.svg");
                                    console.log("true");
                                    $("#deliveryMsg1").html("Delivered on");
                                    $("#deliveryMsg").html("Thanks,Your order has been delivered successfully.");
                                    $(".order-cancel").attr("disabled",true);
                                    $('.order-cancel').css("pointer-events", "none");
                                }
                                if(orderstatus == '4'){
                                    $("#deliveryMsg").html("Thanks,Your order has been cancelled successfully.");
                                    deleveryTime = "<strong>-</strong>";
                                    deliverydate = "";
                                    $("#placed-icon").attr("src", "<?=$baseurl;?>images/icons/accepted.svg");
                                    $(".order-cancel").attr("disabled",true);
                                    $('.order-cancel').css("pointer-events", "none");
                                }

                                
                                var actualamount = rscol.cord[oc].actualamount;
                                var gst = 2.5 / 100 * actualamount;
                                gst = Math.round(gst*100)/100
                                //console.log($gst);
                                if(couponapplied != 0 || discountamount!=0){
                                    $("#promocode1").show();
                                    $("#discountamount").html(discountamount);
                                }else{
                                    $("#promocode1").hide();
                                }
                                
                                if(cloudkitchpoints != "0" && cloudkitchpoints!=""){
                                    $("#walletpointsdiv").show();
                                    $("#walletamount").html(cloudkitchpoints);
                                }else{
                                    $("#walletpointsdiv").hide();
                                }

                                $("#order-date").html(date);
                                $("#address-details").html(chefaddress);
                                $("#totalamount").html(totalamount);
                                $("#sgstamount").html(gst);
                                $("#cgstamount").html(gst);
                                $("#ordTime").html(ordTime);
                                $("#ordDate").html(date);
                                $("#delTime").html(deleveryTime);
                                $("#delDate").html(deliverydate);
                                $("#address-detailsC").html(deliveryaddress);
                                html +='<div class="order-details-item"> <p>'+kName+'</p> </div> <div class="order-details-item order-detail-context"> <div class="order-detail-content order-deatils-item"><p><span>'+cname+'</span></p></div><div class="order-detail-content order-deatils-price"><p>'+quantity+'</p></div><div class="order-detail-content order-deatils-price"><p>&#8377; <span>'+amount+'</span></p></div> <div class="order-detail-content order-deatils-subtotal"><p>&#8377; <span>'+total+'</span></p></div> </div>';
                                if(rscol.cord[oc].sections.length > 0){
                                    html += '<div class="order-details-item"><p><b>Add ons</b></p></div><div class="chipsWrap orderDetailsAddOn">';
                                    for(var j=0;j<rscol.cord[oc].sections.length;j++){
                                        html += '<div class="chip">'+ rscol.cord[oc].sections[j].detail +' (+ '+ rscol.cord[oc].sections[j].price +')</div>';
                                    }
                                    html += '</div><br>';
                                }
                                
                                $("#listdata").html(html);


                            }
                            else{
                                console.log("no");
                            }
                        }
                    }
                });
    }
        /*
    By:Jyoti Vishwakarma
    Description: repeat order 
    */
    function repeatOrder(){
        var pagedata = {
            "orderid": <?=  $_GET['orderId'] ?>
        };
        $.ajax({
            url: serviceurl + 'repeatOrder',
            type: 'POST',
            data: JSON.stringify(pagedata),
            async: false,
            success: function(data) {
                var value = JSON.parse(data);
                if (value.status == 'success') {
                    var cartcount = value.cartcount;
                    $(".notiCount").html(cartcount);
                    $(".toast").fadeIn();
                    setTimeout(function(){
                        $(".toast").fadeOut();
                      }, 3000);
                      window.location.href = "<?=$baseurl?>cart.php";
                } else {
                    $('.overlay,.cartPopup').fadeIn();
                    setTimeout(function() {
                        $('.overlay,.cartPopup').fadeOut();
                    }, 1200);
                }
            }
        });

    }
        /*
    By:Jyoti Vishwakarma
    Description: cancel order
    */
    function cancelorder(){
        // alert("here");
        var pagedata = {
            "orderid": <?=  $_GET['orderId'] ?>
        };
        $.ajax({
            url: serviceurl + 'cancelOrder',
            type: 'POST',
            data: JSON.stringify(pagedata),
            async: false,
            success: function(data) {
                var value = JSON.parse(data);
                if (value.status == 'success') {
                    $("#deliveryMsg").html("Thanks,Your order has been cancelled successfully.");
                } else {
                   
                }
            }
        });
    }
</script>
</html>


