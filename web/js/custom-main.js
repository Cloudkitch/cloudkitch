var baseurl = '/cloudkitch/web/';
// var baseurl = "/";

$(document).ready(function() {
 
  // set focus on floating label
  $("input").focus(function() {
    $(this)
      .parent()
      .addClass("form-setfocus");
  });
  // if empty input set focus on floating label
  $("input").blur(function() {
    if ($(this).val() == "") {
      $(this)
        .parent()
        .removeClass("form-setfocus");
    }
  });

  // $(".corporate-card").click(function() {
  //   $(".popup-order, .overlay")
  //     .delay(400)
  //     .fadeIn();
  // });

  $(".homeSlider").slick({
    dots: true,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    centerMode: true,
    centerPadding: "100px",
    slidesToShow: 1,
    speed: 800,
    responsive: [
      {
        breakpoint: 700,
        settings: {
          centerPadding: "20px"
        }
      },
      {
        breakpoint: 1000,
        settings: {
          centerPadding: "50px"
        }
      }
    ]
  });

  $(".homerestroSlider").slick({
    slidesToShow: 5,
    slideToScroll: 1,
    infinite: true,
    arrow: true,
    dots: false,
    responsive: [
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1
        }
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 2
        }
      }
    ]
  });



  function adjustHeight() {
    $(".cuisineTitle").matchHeight();
    $(".cuisineImgWrap").matchHeight();  
    $('.restroWrapContent').matchHeight();
    $('.cuisineDesc').matchHeight();
    $(".cuisineRestroTitle").matchHeight();
    $(".cuisineData").matchHeight();
  }

  adjustHeight();

  $(".menuBtn").on("click", function() {
    $(this).toggleClass("actMenuBtn");
    $(".mobNavbar").slideToggle();
  });

  $(".filterBtn").on("click", function() {
    $(".cuisineWrap").addClass("activeFilter");
  });

  $(".closeFilter").on("click", function() {
    $(".cuisineWrap").removeClass("activeFilter");
  });


  $(".addBtn").on("click", function() {
    // $(this).find("p").text("Added To Cart.");
    $(this).parent().find('.hurryBlock').fadeOut();
  });


  $(".linkBtn,.signIn,.normal-loginlink").click(function() {
    $(".sign-up,.forgot-password,.sign-in-manually-corporate,.corporate-loginlink").fadeOut();
    $(".overlay,.popup-login,.first-login,.sign-in-manually,.normal-loginlink")
      .delay(400)
      .fadeIn();
    $(".menuBtn").removeClass("actMenuBtn");
    $(".mobNavbar").slideUp();
  });

  $(".linkCorporate").click(function() {
    $(".popup-corporate, .overlay")
      .delay(400)
      .fadeIn();
    $(".menuBtn").removeClass("actMenuBtn");
    $(".mobNavbar").slideUp();
  });

  $(".sign-in-manually").click(function() {
    $(".first-login").fadeOut();
    $(".sign-in")
      .delay(400)
      .fadeIn();
  });

  $('.corporate-login-link,.corporate-loginlink').on('click',function(){
    $(".sign-up,.forgot-password,.sign-in-manually,.normal-loginlink").fadeOut();
    $(".overlay,.popup-login,.first-login,.sign-in-manually-corporate,.corporate-loginlink")
      .delay(400)
      .fadeIn();
    $(".menuBtn").removeClass("actMenuBtn");
    $(".mobNavbar").slideUp();
  });

  $('.sign-in-manually-corporate').on('click',function(){
    $(".first-login").fadeOut();
    $(".corporate-sign-in")
      .delay(400)
      .fadeIn();
  });

  // $(".cuisine.corporate-card").on("mouseenter", function() {
  //   $(this)
  //     .find(".hurryBlock")
  //     .fadeIn();
  // });

  $(".quantityBlock").on("click", function() {
    $(this)
      .find(".addBtn")
      // .fadeOut();
    $(this)
      .find(".cuisineQty")
      .css("display", "flex");
  });



  $('.quantityBlock').on('click',function(){
        $(this).find('.addBtn').fadeOut();
        $(this).find('.cuisineQty').css('display','flex');
    });

    $('.cuisineQty .sub').click(function () {
        if ($(this).next().val() > 0) $(this).next().val($(this).next().val() - 1);
        if ($(this).next().val() == 0) {
            $('.cuisineQty').fadeOut();
            $(this).parent().parent().find('.addBtn').fadeIn();
        }
    });
    
  $(".cuisineQty .sub").click(function() {
    if (
      $(this)
        .next()
        .val() > 0
    )
      $(this)
        .next()
        .val(
          +$(this)
            .next()
            .val() - 1
        );
    if (
      $(this)
        .next()
        .val() == 0
    ) {
      $(".cuisineQty").fadeOut();
      $(this)
        .parent()
        .parent()
        .find(".addBtn")
        .fadeIn();
    }
  });
  $(".show-password").click(function() {
    if (
      $(this)
        .parent()
        .find("input")
        .attr("type") == "password"
    ) {
      $(
        $(this)
          .parent()
          .find("input")
      ).attr("type", "text");
      $(this)
        .find("img")
        .attr("src", baseurl + "images/icons/hide-password.svg");
    } else {
      $(
        $(this)
          .parent()
          .find("input")
      ).attr("type", "password");
      $(this)
        .find("img")
        .attr("src", baseurl + "images/icons/password.svg");
    }

    // $("#sign-in-password").attr('type','text');
  });

  $(".google").on("click", function() {
    // $('#socialMediaMsg').show();
    $(".first-login").fadeOut();
    $(".corporate-login")
      .delay(400)
      .fadeIn();
  });

  $(".facebook").on("click", function() {
    $("#socialMediaMsg").show();
    // $('.first-login').fadeOut();
    // $('.corporate-login').delay(400).fadeIn();
  });

  $(".overlay").click(function() {
    $(".change-password-first, .change-password-form").fadeIn();
    $(
      ".overlay,.popup-customize,.popup-offerings,.locationPopup,.reviewPopup,.corporate-set-password,.corporate-login,.corporate-sign-up,.corporate-sign-in,.popup-login,.sign-in,.sign-up,.first-login,.forgot-password, .change-password-second, .change-password-third, .password-success, .popup-corporate, .popup-order"
    ).fadeOut();
    $(".popup-card-details").fadeOut();
    // $('.first-login').delay(1000).fadeIn();
    // $('.sign-in').delay(1000).fadeOut();
    // $('#sign-in-email, #sign-in-password').val('');
    // $('.form-float-group').removeClass('form-setfocus');
  });

  $('.proceed-btn').on("click", function() {
    $(".popup-offerings,.overlay").fadeOut();
  });

  $(".skip").on("click", function() {
    $(".locationPopup,.overlay").fadeOut();
  });

  $(".signUp").click(function() {
    $(".first-login,.sign-in,.corporate-sign-in,.corporate-loginlink").fadeOut();
    $(".overlay,.popup-login,.sign-up")
      .delay(400)
      .fadeIn();
    $(".menuBtn").removeClass("actMenuBtn");
    $(".mobNavbar").slideUp();
  });

  $(".forgotpassbtn").click(function() {
    $(".sign-in,.corporate-sign-in").fadeOut();
    $(".forgot-password")
      .delay(400)
      .fadeIn();
  });

  $(".corporateSignUp").on("click", function() {
    $(".corporate-sign-in").fadeOut();
    $(".corporate-sign-up")
      .delay(400)
      .fadeIn();
  });
  $(".btnSetCorpPass").on("click", function() {
    $(".corporate-sign-up").fadeOut();
    $(".corporate-set-password")
      .delay(400)
      .fadeIn();
  });

  $(".cart-title").click(function() {
    var cart_id = this.id;
    if ($(this).hasClass("open")) {
    } else {
      $(".open")
        .parent()
        .find(".cart-profile-info")
        .slideUp();
      $(".open")
        .parent()
        .find(".faq_btn")
        .removeClass("actBtn");
      $(".cart-title").removeClass("open");
    }
    $("." + cart_id).slideToggle("slow");
    $(this).toggleClass("open");
    $(this)
      .find(".faq_btn")
      .toggleClass("actBtn");
    if ($(".cart-title").hasClass("completed")) {
      $(".cart-title.completed img").attr(
        "src",
        baseurl + "images/icons/completed.svg"
      );
    }
  });

  if ($(".cart-title").hasClass("completed")) {
    $(".cart-title.completed img").attr(
      "src",
      baseurl + "images/icons/completed.svg"
    );
  }

  $(".account-login, .account-signup").click(function() {
    $(".cart-first-login").fadeOut();
    $(".cart-second-login").fadeIn();
  });

  $(".account-accept").click(function() {
    $("#account").toggleClass("open");
    $(".cart-profile-info.account").toggleClass("open");
    $("#delivery-info").toggleClass("open");
    $(".cart-profile-info.delivery-info").slideToggle("open");
    $(".cart-first-login").fadeIn();
    $(".cart-second-login").fadeOut();
  });

  // $(".btn-continue").click(function() {
    // $("#delivery-info").toggleClass("open");
    // $(".cart-profile-info.delivery-info").slideToggle("open");
    // $("#delivery-details").toggleClass("open");
    // $(".cart-profile-info.delivery-details").slideToggle("open");
  // });

  $(".btn-save-order").click(function() {
    $("#paymnet-details").toggleClass("open");
    $(".cart-profile-info.paymnet-details").slideToggle("open");
    $("#delivery-info").toggleClass("open");
    $(".cart-profile-info.delivery-info").slideToggle("open");
  });

  // $(".partyBtn").on("click", function() {
  //   $(".overlay, .popup-customize").fadeIn();
  // });

  $(".btn-add-popup-cart").on("click", function() {
    $(".overlay, .popup-customize").delay(400).fadeOut();
  });
  // $(".btnPassFirst").click(function() {
  //   $(".change-password-first").fadeOut();
  //   $(".change-password-second")
  //     .delay(400)
  //     .fadeIn();
  // });

  // $(".btnPassSecond").click(function() {
  //   $(".change-password-second").fadeOut();
  //   $(".change-password-third")
  //     .delay(400)
  //     .fadeIn();
  // });

  // $(".btnPassthird").click(function() {
  //   $(".change-password-form").fadeOut();
  //   $(".password-success")
  //     .delay(400)
  //     .fadeIn();
  //   $(".overlay,.popup-login")
  //     .delay(3000)
  //     .fadeOut();
  //   $(".password-success, .change-password-third")
  //     .delay(3500)
  //     .fadeOut();
  //   $(".change-password-form, .change-password-first")
  //     .delay(3500)
  //     .fadeIn();
  // });

  // $(".btnSignUp").click(function() {
  //   $(".form-signup").fadeOut();
  //   $(".signup-success")
  //     .delay(400)
  //     .fadeIn();
  //   $(".overlay,.popup-login")
  //     .delay(3000)
  //     .fadeOut();
  //   $(".signup-success")
  //     .delay(3500)
  //     .fadeOut();
  //   $(".form-signup")
  //     .delay(3500)
  //     .fadeIn();
  // });

  $(".add").click(function() {
    if (
      $(this)
        .prev()
        .val() < 50
    ) {
      $(this)
        .prev()
        .val(
          +$(this)
            .prev()
            .val() + 1
        );
      $(this).removeClass("active");
    }
  });
  $(".sub").click(function() {
    if (
      $(this)
        .next()
        .val() > 1
    ) {
      if (
        $(this)
          .next()
          .val() > 1
      )
        $(this)
          .next()
          .val(
            +$(this)
              .next()
              .val() - 1
          );
    }
  });

  $("input[name$='payment-method']").click(function() {
    var test = $(this).val();
    if (test === "debit-card") {
      $(".card-paymnet").slideDown();
    } else {
      $(".card-paymnet").slideUp();
    }
    // $("div.desc").hide();
    // $("#" + test).show();
  });

  $(".corporate-log-in").click(function() {
    $(".first-login").fadeOut();
    $(".corporate-sign-in")
      .delay(400)
      .fadeIn();
  });

  $("input:radio[name=meals_check]").change(function() {
    if (this.value == "corporate_meals") {
      $(".meal-order").text("Personal Meals");
      $(".meal-text").text("Courses to Choose From");
    } else if (this.value == "team_meals") {
      $(".meal-order").text("Team Meals");
      $(".meal-text").text("Courses to Choose From");
    }else if (this.value == "event_meals") {
      $(".meal-order").text("Event Meals");
      $(".meal-text").text("Courses to Choose From");
    }
  });

  $(".popup-close").click(function() {
    $(".popup-order, .overlay").fadeOut();
  });
  $(".btnCorpSignUp").on("click", function() {
    $(".overlay,.popup,.corporate-set-password").fadeOut();
  });

  $(".meals-select").click(function() {
    if ($(this).hasClass("meal-selected")) {

    } else {
    $(".meals-select").removeClass("meal-selected");
    }
    $(this).addClass("meal-selected");
  });


    $('.promoText').on('click',function(){
      $('.promoCode').slideToggle();
    });

    // $(".cuisineWrap ul li").on("click", function() {
    //   $(".cuisineWrap ul li").removeClass("activeTab");
    //   $(this).addClass("activeTab");
    // });

});

// $(window).scroll(function() {
//     var scrollDistance = $(window).scrollTop();

//        $('.cuisine-divider').each(function(i) {
//         if ($(this).position().top <= scrollDistance) {
//                 $('.cuisineWrap ul li.activeTab').removeClass('activeTab');
//                 $('.cuisineWrap ul li').eq(i).addClass('activeTab');
//         }
//     });
// }).scroll();

$("input:radio[name=orderPreference]").change(function() {
  if (this.value == "delivery-now") {
    $(".scheduleDetails").slideUp();
  } else if (this.value == "schedule-now") {
    $(".scheduleDetails").slideDown();
  }
});

// function onScroll(event) {
//   var scrollPos = $(document).scrollTop();
//   console.log("new");
//   $(".cuisineWrap ul li").each(function() {
//     var currLink = $(this);
//     var refElement = $(currLink.attr("href"));
//     if (
//       refElement.position().top <= scrollPos &&
//       refElement.position().top + refElement.height() > scrollPos
//     ) {
//       $(".cuisineWrap ul li").removeClass("activeTab");
//       currLink.find(".cuisineWrap ul li").addClass("activeTab");
//       console.log("positiion");
//     } else {
//       currLink.find(".cuisineWrap ul li").removeClass("activeTab");
//       console.log("new positiion");
//     }
//   });
// }

// $(document).ready(function() {
//   $('#categories li a[href*=#]').bind('click', function(e) {
//       e.preventDefault(); // prevent hard jump, the default behavior

//       var target = $(this).attr("href"); // Set the target as variable

//       // perform animated scrolling by getting top-position of target-element and set it as scroll target
//       $('html, body').stop().animate({
//           scrollTop: $(target).offset().top + 120
//       }, 600, function() {
//           location.hash = target; //attach the hash (#jumptarget) to the pageurl
//       });

//       return false;
//   });
// });

$(window).scroll(function() {
  var scrollDistance = $(window).scrollTop() + 120;
  $('.cuisine-divider').each(function(i) {
    if ($(this).position().top <= scrollDistance) {
     
      $('#categories li').removeClass('activeTab');
      $('#categories li').eq(i).addClass('activeTab');
    }
  });
}).scroll();
