<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'head.php';
    ?>

    <title>Corporate | Cloudkitch</title>
    <meta name="description" content="">
    <meta name="google-signin-client_id" content="287145767352-qi8asdmobui20671musqaq0jga2ru2re.apps.googleusercontent.com">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="<?= $baseurl; ?>css/timepicki.css">
</head>

<body class="corporatePage">
    <h1 class="seoTag">Cloud Kitch</h1>
    <?php
    include 'header.php';
    ?>

    <div class="popup popup-order">
        <div class="popup-wrapper">
            <div class="popup-close">
                <img src="<?= $baseurl; ?>images/icons/close.svg" alt="close">
            </div>
            <div class="popup-item">
                <h2 class="cuisineTitle" id="cuisineTitle1">Veg Christmas Buffet B</h2>
                <input type="hidden" id="cuisine_id">
                <input type="hidden" id="kitchen_open">
                <input type="hidden" id="kitchen_close">
                <!-- <p>Nine course mini buffet with 3 salad, 4 main dishes and 2 desserts.</p> -->
                <div class="cuisine-details">
                    <h4>What’s included:</h4>
                    <div id="cuisine-details">
                    </div>
                    <!-- <ul class="menu-list">
                            <li><p>Watermelon & Feta Salad drizzled with black tea vinaigrette (Vegetarian) </p></li>
                            <li><p>Smoked Duck Salad with roasted cashew and citrus vinaigrette </p></li>
                            <li><p>Tamarind Vinaigrette Capellini tossed with edamame and herbed cherry tomatoes (Vegetarian) </p></li>
                            <li><p>Christmas Spiced Rice with thyme roasted pumpkin and pomegranate (Vegetarian) </p></li>
                            <li><p>Roasted Winter Vegetables with charred pineapple and sweet prune sprinkles (Vegetarian) </p></li>
                            <li><p>Cinnamon Sugar Glazed Chicken with ginger flower and almond flakes </p></li>
                            <li><p>Eggless Eggnog Dory with caramelised onions and fresh cherry tomatoes </p></li>
                            <li><p>Holiday Walnut Cake with rhubarb cherry compote (Vegetarian) </p></li>
                            <li><p>Christmas Cookies with a Grain surprise (Vegetarian) </p></li>
                        </ul> -->
                    
                    <h4 id="addonheading">Add on Packages</h4>
                    <!-- Add on Packages (Multipe select) -->
                    <div class="checkboxWrap" id="addonlist">

                    </div>


                    <h4>Special Instructions</h4>
                    <textarea name="special-instrustion" class="custom-textarea" id="special-instrustion" rows="8"></textarea>
                </div>
            </div>
            <div class="popup-item">
                <img src="<?= $baseurl; ?>images/cuisine/corporate/1.jpg" alt="Cuisine" class="order-img" id="order-img_cuisin">
                <div class="cuisineType">
                    <p id="veg_val"><img src="<?= $baseurl; ?>images/icons/veg.svg" alt="Veg">4 course meal</p>
                </div>
                <div class="priceBlock">
                    <p class="price">&#8377; <span id="pop_price">1,550</span> <span class="price-gst">+ GST</span></p>
                    <p class="label-gray" id="mim_order">Min Order 25pax</p>
                </div>
                <div class="quantityBlock">
                    <div class="timeBlock">
                        <p class="timeStat label-gray"><span id="leadtime"></span> hours lead time</p>
                    </div>
                </div>
                <div class="order-date-time">
                    <div class="order-date">
                        <div class="form-group form-legend">
                            <input type="text" id="datepicker" class="form-input-legend" placeholder="Select Date">
                        </div>
                    </div>
                    <div class="order-time">
                        <div class="form-group form-legend">
                            <input type="text" class="form-input-legend timepicker" id="order_time" placeholder="Select Time" />
                        </div>
                    </div>
                </div>
                <input id="teamMealMinQty1" type="hidden" class="quantity">
                <div class="order-quantity">
                    <a href="#" class="quantity-icon sub">
                        <p>-</p>
                    </a>
                    <!-- <input id="teamMealMinQty" type="text" class="quantity-input" readonly="readonly"> -->
                    
                    <input id="teamMealMinQty" type="text" class="quantity-input quantity" readonly>
                    <a href="#" class="quantity-icon add active">
                        <p>+</p>
                    </a>
                </div>
                <input type="hidden" id="validateflag" value="1">
                <div class="addBtn">
                    <p class="addToCart" id="addtocartBtn" onclick="corporatemealorder()"><img src="<?= $baseurl; ?>images/icons/addCart.svg" alt="download">ADD to cart</p>
                    <span id="quterror" style="color:red"></span>
                </div>
               
            </div>
        </div>
        <div class="bg-grey">
            <!-- <p>Terms and Conditions para will follow here if any, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p> -->
        </div>
    </div>

    <section class="banner topSection">
        <h2 style="display:none;">Cloudkitch</h2>
        <div class="homeSlider" id="homeSlider" style="display: none">
        <?php
            $query = "SELECT * FROM banners";
            $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
            while($row = mysqli_fetch_assoc($result))
            {
                ?>
                <div class="homeSlide">
                    <img src="<?=$row['image']?>" alt="Banner">
                </div>
                <?php
            }
            ?>
        </div>
    </section>
   
    <section class="section">
        
        <div class="filterWrap">
            <div class="titleWrap">
                <h2 class="meal-order">Party Meals</h2>
                <p class="meal-text">Courses to Choose From</p>
            </div>
            <div class="filterBtn"></div>
            <div class="filterContainer">
                <div class="searchWrap menuSearch">
                    <form>
                        <input type="text" id="search_inner" onkeyup="getCuisinesFilter()" placeholder="Search Hot Favourites">
                        <input type="hidden" id="kitchen_id">
                    </form>
                </div>
                <div class="buttonWrap">
                <p class="btn borderBtn offersBtn" onclick="getOffer()"><img src="<?= $baseurl; ?>images/icons/discount.svg" alt="offers">All Offers</p>
                    <div class="filter-check">
                        <input class="custom-radio" type="checkbox" onchange="getCuisinesFilter()" id="checkbox_veg" name="pure_veg" value="PURE VEG">
                        <label for="checkbox_veg" class="btn radio-label">
                            <img src="<?= $baseurl; ?>images/icons/leaf.svg" alt="Veg">VEG
                        </label>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="menuWrap autoHeight">
            <div class="cuisineWrap card restrocuisineWrap">
                <h3 class="cusineTitle">
                    <!-- <span class="cusine-first">
                        <img src="<?= $baseurl; ?>images/icons/cuisine.svg" alt="Course">Course
                    </span> -->
                    <span class="cusine-second">
                        <!-- <p class="filterConatiner btn-gradient btn-mob-filter">Apply </p> -->
                        <span class="closeFilter">Close X</span>
                    </span>
                </h3>
                <div class="checkWrap">
                    <ul id="categories">

                    </ul>
                </div>
            </div>
            <div class="cuisineWrapper">
                <div class="restroWrap homerestroSlider" id="kitchens"></div>
                <div class="cuisineContainer" id="cuisines">  </div>
                <span data-value="1" id="loadmoredata" class="loadmore" style="border: none;"> </span>
            </div>
        </div>
    </section>
    <?php
    include 'footer.php';
    ?>

    <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?= $baseurl; ?>js/timepicki.js"></script> -->
    <script>
        $(function() {
            $('#datepicker').datepicker();
            $(".timepicker").timepicki();
        });
        $(document).ready(function() {
            $("#homeSlider").css("display","block");
            getCategories('5');
            getKitchens();
            getPartyMeals();
            $("#meals_check").val('party_meals');
            // var d = new Date();
            // d.setTime(d.getTime() + (1*24*60*60*1000));
            // var expires = "expires="+ d.toUTCString();
            // document.cookie = "mealstype" + "=" + "partymeals" + ";" + expires + ";path=/";

        });

        function adjustHeight() {
            $(".cuisineTitle").matchHeight();
            $(".cuisineImgWrap").matchHeight();  
            $('.restroWrapContent').matchHeight();
            $(".cuisineRestroTitle").matchHeight();
            $(".cuisineData").matchHeight();
        }


        jQuery(
            function($)
            {
                $('.cuisineContainer').bind('scroll', function()
                    {
                        if($(this).scrollTop() + $(this).innerHeight()>=$(this)[0].scrollHeight)
                        {
                         
                            var loadlimit = Number($("#loadmoredata").attr("data-value"));
                                    if ($("#checkbox_veg").prop("checked") == true) {
                                        var type = "veg";
                                    } else {
                                        var type = "";
                                    }
                                    var keyword = $("#search_inner").val();
                                    var category_id = $('.checkWrap  li.activeTab').attr("data-id");
                                    $("#loadmoredata").attr("data-value",loadlimit+1);
                                    var mealtype = $('input[name=meals_check]:checked').val();
                                    var pagedata = {
                                        "keyword": keyword,
                                        "categories": category_id,
                                        "sortbycost": "",
                                        "type": type,
                                        "limit": loadlimit
                                    };
                                   
                                        $.ajax({
                                            url: serviceurl + 'partymeals',
                                            type: 'POST',
                                            data: JSON.stringify(pagedata),
                                            datatype: 'JSON',
                                            async: false,
                                            success: function(data) {
                                                var value = JSON.parse(data);
                                                var html = "";
                                                if (value.cuisines.length > 0) {
                                                    for (var i = 0; i < value.cuisines.length; i++) {
                                                            html += '<div class="cuisine card"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '</p><p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="hurryBlockContainer"><p>Customisable</p></div><div class="hurryBlockContainer">';
                                                            var cuisintype = value.cuisines[i].type.split(",");
                                                            if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                                                html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                                            } else {
                                                                html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                                            }
                                                            html += '</div></div><div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div></div></div></div>';
                                                    
                                                        }
                                                        if(value.cuisines.length < 30){
                                                            $("#loadmoredata").hide();
                                                        }
                                                        $("#cuisines").html(html);
                                                        addedIncart();
                                                        hoverCuisine();
                                                        adjustHeight();
                                                } else {
                                                
                                                    $("#loadmoredata").hide();
                                                }

                                            }
                                        });
                                  
                              
                                }
                            })
                    }
        );
 /*
        By:Jyoti Vishwakarma
        Description: kitchen list
        */
        function getKitchens() {

            $.ajax({
                url: serviceurl + 'kitchens',
                type: 'POST',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                    var vl = value.kitchens.length;
                    for (var i = 0; i < vl; i++) {
                        // html += '<div class="restro card"><a href="kitchen/' + value.kitchens[i].id + '/' + value.kitchens[i].name1 + '"><div class="restroWrapContent"><img src="' + value.kitchens[i].image + '" alt="' + value.kitchens[i].name + '"><div class="context"><h3>' + value.kitchens[i].name + '</h3><p>' + value.kitchens[i].tagline + '</p><div class="ratingWrap"><ul><li><img src="<?= $baseurl; ?>images/icons/star.svg" alt="Star"></li><li><img src="<?= $baseurl; ?>images/icons/star.svg" alt="Star"></li><li><img src="<?= $baseurl; ?>images/icons/star.svg" alt="Star"></li><li><img src="<?= $baseurl; ?>images/icons/star.svg" alt="Star"></li><li><img src="<?= $baseurl; ?>images/icons/blank-star.svg" alt="Star"></li></ul></div></div></div></a></div>';
                        html += '<div class="restro card" onclick="getKitchesData(' + value.kitchens[i].id + ')" id="kitchenDiv-' + value.kitchens[i].id + '"><div class="restroWrapContent"><img src="' + value.kitchens[i].image + '" alt="' + value.kitchens[i].name + '" /><div class="context"><h3>' + value.kitchens[i].name + '</h3><a href="javascript:goToKitchenParty(' + value.kitchens[i].id + ',' + '\'' + value.kitchens[i].name1.replace("&#39;","\\'") + '\''+')"><h6 class="visitRestro">Visit Restaurant ></h6></a></div></div></div>';
                        $("#kitchens").html(html);
                    }
                }
            });
        }
         /*
        By:Jyoti Vishwakarma
        Description: redirect to particular kitchen page
        */
        function goToKitchenParty(kitchenid, kitchenname){
            // alert(kitchenid);
            $('<form action="<?=$baseurl?>party-kitchen/' + kitchenname + '" method="POST">' +
            '<input type="hidden" name="kitchenid" value="' + kitchenid + '" />' +
            '</form>').appendTo('body').submit();
        }
 /*
        By:Jyoti Vishwakarma
        Description: display party meals
        */
        function getPartyMeals() {
            var kid = $("#kitchen_id").val();
            var category_id = $('.checkWrap  li.activeTab').attr("data-id");
            if(kid != ""){
                getKitchesData(kid);
                return;
            }
            var pagedata = {
                "keyword": "",
                "categories": category_id,
                "sortbycost": "",
                "type": "",
                "kitchen_id":""
            };
            $.ajax({
                url: serviceurl + 'partymeals',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    console.log(value);
                    var html = "";
                    if(value.cuisines.length > 0){
                        for (var i = 0; i < value.cuisines.length; i++) {
                             html += '<div class="cuisine card" onclick="corporate_order(\'' + value.cuisines[i].id + '\')"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description1 + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '<span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[i].minorderquantity + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[i].leadtime + ' hours lead time</p></div><div class="hurryBlockContainer">';
                            var cuisintype = value.cuisines[i].type.split(",");
                            if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                            } else {
                                html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                            }
                            html += '</div></div></div></div></div>';
                      
                        }
                        if(value.cuisines.length < 30){
                            $("#loadmoredata").hide();
                        }
                        $("#cuisines").html(html);
                        addedIncart();
                        adjustHeight();
                    }else{
                        $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");  
                    }

                }
            });
        }
  /*
        By:Jyoti Vishwakarma
        Description: category
        */
        function getCategories(pagetype) {
            var pagedata = {
                "pagetype": pagetype
            };
            $.ajax({
                url: serviceurl + 'homecategories',
                type: 'POST',
                data: JSON.stringify(pagedata),
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";                   
                    for (var i = 0; i < value.categories.length; i++) {
                        var id = value.categories[i].id;
                        var category = value.categories[i].category;
                            // html += '<div class="checkboxWrap"><label class="container">' + category + '<input type="checkbox" id="category" name="category[]" onchange="getCuisinesFilter()" value="' + id + '"><span class="checkmark"></span></label></div>';
                            if(i == 0){
                                html += '<li class="snacks-wrapper activeTab" id="category-' + id + '" onclick="addActivetab(' + id + ')" data-id="' + id + '"><a><p><span>' + category + '</span></span></p></a></li>';
                            }else{
                                html += '<li class="snacks-wrapper" id="category-' + id + '" onclick="addActivetab(' + id + ')" data-id="' + id + '"><a><p><span>' + category + '</span></span></p></a></li>';
                            }

                    }
                    $("#categories").html(html);
                }
            });
        }

        function addActivetab(catid) {
            var categoryid = catid;
            $('.checkWrap  li.activeTab').removeClass('activeTab');
            $("#category-"+catid).addClass("activeTab");
            getCuisinesFilter();
        }
 /*
        By:Jyoti Vishwakarma
        Description: filter meals
        */
        function getCuisinesFilter() {
            var kitchenid = $("#kitchen_id").val();
            if(kitchenid != ""){
                getKitchesData(kitchenid);
                return;
            }
            
            var keyword = $("#search_inner").val();
            var sort_by_cost = $("#sort_by_cost").val();
            var keyLength = keyword.length;
            var type = "";
            if ($("#checkbox_veg").prop("checked") == true) {
                var type = "Veg";
            } else {
                var type = "";
            }

            var category_id = $('.checkWrap  li.activeTab').attr("data-id");

            var pagedata = {
                "keyword": keyword,
                "categories": category_id,
                "sortbycost": sort_by_cost,
                "type": type
            };
            var mealtype = $('input[name=meals_check]:checked').val();
            $.ajax({
                    url: serviceurl + 'partymeals',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        var html = "";
                       // $(".loader").hide();
                        if (value.cuisines.length > 0) {
                            for (var i = 0; i < value.cuisines.length; i++) {
                                html += '<div class="cuisine card" onclick="corporate_order(\'' + value.cuisines[i].id + '\')"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description1 + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '<span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[i].minorderquantity + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[i].leadtime + ' hours lead time</p></div><div class="hurryBlockContainer">';
                                var cuisintype = value.cuisines[i].type.split(",");
                                if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                    html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                } else {
                                    html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                }
                                html += '</div></div></div></div></div>';
                        
                            }
                            // if(value.cuisines.length < 30){
                            //     $("#loadmoredata").hide();
                            // }
                            $("#cuisines").html(html);
                            addedIncart();
                            adjustHeight();
                        } else {
                            $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");
                        }
                    }
                });
        }
        /*
        By:Jyoti Vishwakarma
        Description: Meal description dispay in popup
        */
        function corporate_order(id){
            var pagedata = {
                "id": id
            };
            $.ajax({
                url: serviceurl + 'corporatemeals_detail',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    console.log(value.cuisines[0].name);
                    var val = "";
                    $("#cuisine_id").val(value.cuisines[0].id);
                    $("#kitchen_open").val(value.cuisines[0].kitchen_open);
                    $("#kitchen_close").val(value.cuisines[0].kitchen_close);
                    if (value.cuisines[0].type.includes('Non Veg')) {
                        val = '<img src="images/icons/nonveg.svg" alt="Non Veg">' + value.cuisines[0].kitchen_name;
                    } else {
                        val = '<img src="images/icons/veg.svg" alt="Veg">' + value.cuisines[0].kitchen_name;
                    }
                    $("#cuisineTitle1").html(value.cuisines[0].name);
                    $("#order-img_cuisin").attr("src", value.cuisines[0].image);
                    $("#veg_val").html(val);
                    $("#cuisine-details").html(value.cuisines[0].description);
                    $("#pop_price").html(value.cuisines[0].price);
                    $("#mim_order").html("Min Order " + value.cuisines[0].minorderquantity + "pax");
                    $("#leadtime").html(value.cuisines[0].leadtime);
                    $("#teamMealMinQty").val(value.cuisines[0].minorderquantity);
                    $("#teamMealMinQty1").val(value.cuisines[0].minorderquantity);
                    var addon = "";
                    var addonshtml = "";
                    if(value.cuisines[0].sections.length == 0){
                        $("#addonheading").hide();
                    }else{
                        $("#addonheading").show();
                    }    
                    for(var i=0;i<value.cuisines[0].sections.length;i++){
                        console.log(value.cuisines[0].sections[i].title);
                        var section_id = value.cuisines[0].sections[i].section_id;
                        var noofchoice = value.cuisines[0].sections[i].noofchoice;
                        addonshtml += '<h4 >'+value.cuisines[0].sections[i].title+'</h4><input id="da'+section_id+'" type="hidden" value="'+noofchoice+'"><input id="das'+section_id+'" type="hidden" value="'+noofchoice+'"><span id="counterror'+section_id+'" style="color:red;display:none">error</span><div id="prt'+section_id+'">';
                        var addon = value.cuisines[0].sections[i].addons;
                        

                        if(value.cuisines[0].sections[i].choicetype == 1){
                            addonshtml += '<div class="section-div" id="'+section_id+'"></div>';
                            for(var j=0;j<addon.length;j++){

                                addonshtml += '<label class="container">';
                                addonshtml += '<p>' + addon[j].detail + ' &#8377;' + addon[j].price + '</p>';
                                addonshtml += '<input type="checkbox" onclick="checkval('+section_id+','+addon[j].addon_id+')" id="'+addon[j].addon_id+'" class="addons_corporate" name="addon"  data-id="' + addon[j].addon_id + '" value="' + addon[j].addon_id + '">';
                                addonshtml += '<span class="checkmark"></span>';
                                addonshtml += '</label>';
                            }

                        }else{
                            addonshtml += '<div class="section-divs"></div>';
                            for(var j=0;j<addon.length;j++){
                                    var checked = "";
                                    if(j==0){
                                        checked = "checked";
                                    }
                                    addonshtml +=   ' <div class="checkboxWrap">';
                                    addonshtml +=    '<label class="container radioContainer">';
                                    addonshtml +=        '<p>' + addon[j].detail + ' &#8377;' + addon[j].price + '</p>';
                                    addonshtml +=       '<input type="radio" name="addOnPacagesRadio-'+ value.cuisines[0].sections[i].section_id +'" value="' + addon[j].addon_id + '" ' + checked + '>';
                                    addonshtml +=        '<span class="checkmark"></span>';
                                    addonshtml +=    '</label>';
                                    addonshtml +=  '</div>'; 
                            }

                        }
                        addonshtml +=  '</div>'; 
                    }
                    $("#datepicker").val("");
                    $("#order_time").val("");
                    $("#quterror").hide();
                    $("#quterror").text("");
                    $("#addonlist").html(addonshtml); 
                    $(".popup-order, .overlay")
                        .delay(400)
                        .fadeIn();

                }
            });

        }
        /*
        By:Jyoti Vishwakarma
        Description: Meal description dispay in popup
        */
        function corporatemealorder() {
            
            var cuisine_id = $("#cuisine_id").val();
            var date = $("#datepicker").val();
            var time = $("#order_time").val();
            var qty = parseInt($("#teamMealMinQty").val());
            var qty1 = parseInt($("#teamMealMinQty1").val());
            var numItems = $('.section-div').length
           
            if(numItems > 0){
                $('.section-div').each(function(){
                   var section_id = $(this).prop('id');
                   var minreq = $("#da"+section_id).val(); 
                   var qut1 = $("#das"+section_id).val();
                  
                    if(qut1 != 0){
                        $("#counterror"+section_id).css("display","block");
                        $("#counterror"+section_id).text("Minimum selection should be "+minreq+".");
                        $("#validateflag").val(1);
                    }else{
                        $("#counterror"+section_id).css("display","none");
                        $("#validateflag").val(0);
                    }
                   
                });
            }else{
                $("#validateflag").val(0);
            }

            var leadtime = parseInt($("#leadtime").html());
            var today = new Date();
            var minord = new Date(today.setHours(today.getHours()+leadtime));
            var orddate = date + ' ' + time;
            orddate = new Date(orddate);

            var min = new Date(date + ' ' + $("#kitchen_open").val());
            var max = new Date(date + ' ' + $("#kitchen_close").val());

            if(qty1 > qty || date == "" || time == ""){
                if(date == ""){
                    $("#quterror").show();
                    $("#quterror").text("Please select date.");
                }else if( time == ""){
                    $("#quterror").show();
                    $("#quterror").text("Please select time.");
                }else if(qty1 > qty){
                    $("#quterror").show();
                    $("#quterror").text("Min Order "+qty1+"pax");
                }
                return;
            }else if(minord.getTime() > orddate.getTime()){
                $("#quterror").show();
                $("#quterror").text("Order time should be greater than leadtime");
                return;
            }else if(min.getTime() > orddate.getTime() || orddate.getTime() >  max.getTime()){
                $("#quterror").show();
                $("#quterror").text("Order time should be between "+ $("#kitchen_open").val() +" and "+ $("#kitchen_close").val());
                return;
            }else{
                $("#quterror").hide();
            }

            var special_instruction = $("#special-instrustion").val();

            var items = $(".addons_corporate");
            var selectedaddons = "";
            for (var i = 0; i < items.length; i++) {
                if (items[i].type == 'checkbox' && items[i].checked == true){
                        selectedaddons += items[i].value + ",";
                }
            }

            $('#addonlist input[type=radio]:checked').each(function() {
                 selectedaddons += this.value + ",";
            });

            addToCartCorporate(cuisine_id, date, time, qty, special_instruction, selectedaddons);
            // alert(special_instruction);

        }
        /*
        By:Jyoti Vishwakarma
        Description:validate requirement to place meal order
        */
        function addToCartCorporate(id, date, time, qty, special_instruction, selectedaddons) {
            var count = 1;
            var data = {
                "id": id,
                "date": date,
                "time": time,
                "qty": qty,
                "special_instruction": special_instruction,
                "selectedaddons": selectedaddons
            };
            if($("#validateflag").val() == 0){
                $.ajax({
                    url: serviceurl + 'addtocartCorporate',
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        if (value.status == 'success') {
                            var cartCount = $(".notiCount").html();
                            $(".notiCount").html(parseInt(cartCount) + 1);
                            $(".popup-order, .overlay").delay(400).fadeOut();
                            $(".toast").fadeIn();
                                setTimeout(function(){
                                    $(".toast").fadeOut();
                                }, 3000);
                        } else {

                            $('.overlay,.cartPopup').fadeIn();
                            setTimeout(function() {
                                $('.overlay,.cartPopup').fadeOut();
                            }, 1200);
                        }
                    }
                });
            }
        }
        /*
        By:Jyoti Vishwakarma
        Description:display selected kitchen party meals
        */
        function getKitchesData(kitchenid) {    
            var kid = $("#kitchen_id").val();
            $("#kitchenDiv-"+kid).removeClass("activeRestro");
            $("#kitchenDiv-"+kitchenid).addClass("activeRestro");
            $("#kitchen_id").val(kitchenid);
            var keyword = $("#search_inner").val();
            var type = "";
            if ($("#checkbox_veg").prop("checked") == true) {
                var type = "Veg";
            } else {
                var type = "";
            }

            var pagedata = {
                "kitchenid": kitchenid,
                "keyword": keyword,
                "categories": "",
                "sortbycost": "",
                "type": type,
                "mealtype":"party_meals"
            };
            var mealtype = $('input[name=meals_check]:checked').val();
               $.ajax({
                    url: serviceurl + 'kitchen_page_party',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        var categories = "";
                        for (var i = 0; i < value.categories.length; i++) {
                            if (i == 0) {
                                categories += '<li class="snacks-wrapper activeTab"><a href="#category' + value.categories[i].categoryid + '"><p><span>' + value.categories[i].categoryname + '</span></span></p></a></li>';
                            } else {
                                categories += '<li class="snacks-wrapper"><a href="#category' + value.categories[i].categoryid + '"><p><span>' + value.categories[i].categoryname + '</span></span></p></a></li>';
                            }
                        }
                        $("#categories").html(categories);
                        
                            var cuisinesdata = "";
                            if(value.cuisines.length > 0){
                                for (var j = 0; j < value.cuisines.length; j++) {
                                    cuisinesdata += '<div class="cuisine-divider" id="category' + value.cuisines[j].categoryid + '"> <div class="titleBlock"> <h2>' + value.cuisines[j].categoryname + '</h2> <p>' + value.cuisines[j].cuisines.length + ' Items</p> </div><div class="cuisineContainer">';
                                
                                    for (var k = 0; k < value.cuisines[j].cuisines.length; k++) {
                                        cuisinesdata += '<div class="cuisine card" onclick="corporate_order(\'' + value.cuisines[j].cuisines[k].id + '\')"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[j].cuisines[k].image + '" alt="' + value.cuisines[j].cuisines[k].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[j].cuisines[k].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[j].cuisines[k].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[j].cuisines[k].description + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[j].cuisines[k].price + '<span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[j].cuisines[k].minorderquantity + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[j].cuisines[k].leadtime + ' hours lead time</p></div><div class="hurryBlockContainer">';
                                        var cuisintype = value.cuisines[j].cuisines[k].type.split(",");
                                        if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                            cuisinesdata += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                        } else {
                                        cuisinesdata += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                    }
                                    cuisinesdata += '</div></div></div></div></div>';
                            
                                }
                            }

                                cuisinesdata += '</div>';
                            
                            $("#cuisines").html(cuisinesdata);
                            addedIncart();
                            hoverCuisine();
                            adjustHeight();
                            }else{
                                $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");
                            }
                      
                    }
                });
            

        }

        function getOffer(){
            var kitchenid = $("#kitchen_id").val();
            $("#kitchenDiv-"+kitchenid).removeClass("activeRestro");
            $("#kitchen_id").val('');
            // var mealtype = $("#meals_check").val();
            // changeChooseOffering(mealtype);
        }
        /*
        By:Jyoti Vishwakarma
        Description:validate validate addons selection
        */
        function checkval(s,z){
            qut1 = $("#da"+s).val();
            qut = $("#da"+s).val();
            var q = "#das"+s;
            var qt = $(q).val();   
            var prt = "#prt"+s;   
            var ckId =  "#"+z;
            
                // alert($(q).val());
                if($(ckId).is(":checked")){
                    $(q).val(parseInt(qt) - 1);
                }
                else if($(ckId).is(":not(:checked)")){
                    $(q).val(parseInt(qt) + 1);
                    
                }
                
                if($(q).val() == 0 ){
                    $("#validateflag").val(0);
                    $("#counterror"+s).css("display","none");
                }
                else{
                    $("#counterror"+s).css("display","block");
                    $("#counterror"+s).text("Minimum selection should be "+qut1+".");
                    $("#validateflag").val(1);
                }
                      
        }

    </script>
</body>

</html>