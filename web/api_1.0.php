<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
error_reporting(1);
header('Access-Control-Allow-Origin: *');

include 'inc/databaseConfig.php';			//include database configuration
date_default_timezone_set('Asia/Kolkata');	//setting default time zone of india

if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
require 'aws-autoloader.php';

use Aws\S3\S3Client;
use Aws\Api\ApiProvider;
use Aws\Api\DocModel;
use Aws\Api\Service;
use Aws\AwsClient;
use Aws\ClientResolver;
use Aws\Command;
use Aws\Exception\AwsException;
use Aws\HandlerList;
use Aws\Middleware;
use Aws\RetryMiddleware;
use Aws\S3\Exception\S3Exception;
use Aws\ResultInterface;
use Aws\CommandInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\StreamInterface;
use Aws\S3\MultipartUploader;
use Aws\Credentials\Credentials;

/*
By: Dilip Kumawat
Created at: 18 Nov, 2019
Created at: 18 Nov, 2019
Description: Upload data in S3 bucket 
*/

function s3upload($base64){
	if(((strpos($base64, 'data:image/jpeg') !== false) == 1) or ((strpos($base64, 'data:image/png') !== false) == 1) or ((strpos($base64, 'data:application/pdf') !== false) == 1)){
		if(((strpos($base64, 'data:image/jpeg') !== false) == 1))
		{
			$keyname = ''.uniqid().".jpg";
		}
		else if(((strpos($base64, 'data:application/pdf') !== false) == 1))
		{
			$keyname = ''.uniqid().".pdf";
		} 
		else
		{
			$keyname = ''.uniqid().".png";
		}    
		$bucket = 'cloudkitch';

		$s3 = S3Client::factory(array(
			'credentials' => array(
				'key'    => 'AKIAQ62UWJYQODVJT3XY',
				'secret' => '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',
			),
			'region' => 'ap-south-1',
			'version' => '2006-03-01',
			'http'    => [
				'verify' => false
			]
		));
		$result = $s3->putObject(array(
			'Bucket'       => $bucket,
			'Key'          => "appictest/".$keyname,
			'SourceFile'   => $base64,
			'ACL'          => 'public-read'
		));
		return $imagePath = $result['ObjectURL'];
	}
	else
	{
		return $imagePath = "";
	}
}

/*
By: Dilip Kumawat
Created at: 18 Nov, 2019
Created at: 18 Nov, 2019
Description: Generate Random Nimber 
*/

function generate_random_referral($length = 7) {
	$numbers = range('0','9');   
	$alphabets = range('A','Z');
	$final_array = array_merge($alphabets,$numbers);

	$referralno = '';

	while($length--) {
		$key = array_rand($final_array);
		$referralno .= $final_array[$key];
	}

	return $referralno;
}

/*
By: 
Created at: 18 Nov, 2019
Created at: 18 Nov, 2019
Description: Registration
*/  

if($_GET['service_name'] == 'registration')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);

	$name = $json['name'];
	$email = $json['email'];
	$phone = $json['phone'];
	$password = $json['password'];
	
	$profpic = $json['profpic'];
	$logintype = $json['signuptype'];
	$socialtype = $json['socialtype'];
	$authid = $json['authid'];
	$devicetype = $json['devicetype'];
	$devicetoken = $json['devicetoken'];

	if(isset($json['sign_up_corporate']) && $json['sign_up_corporate']!=""){
		$corporatecode = $json['sign_up_corporate'];
		$iscorporateuser = '1';
		$quesp1 = "SELECT * FROM corporate WHERE code='$corporatecode'";	
		$excph1 = mysqli_query($conn,$quesp1)or die(mysqli_error($conn));
		if(mysqli_num_rows($excph1) > 0)
		{
			$rowse = mysqli_fetch_assoc($excph1);
			$corporateid = $rowse['id'];
		}else{
			$response['status'] = 'failed';
			$response['msg'] = 'Corporate ID is wrong!';
			print_r(json_encode($response));
			exit;
		}
	}else{
		
		$corporatecode = "";
		$iscorporateuser = '0';
		$corporateid = "";
	}

	$referralcode = generate_random_referral();

	

	if ($socialtype=='facebook') {
		$quesp = "SELECT * FROM user WHERE email='".$email."'";	
		$excph = mysqli_query($conn,$quesp)or die(mysqli_error($conn));
		if(mysqli_num_rows($excph) > 0)
		{
			$rowse = mysqli_fetch_assoc($excph);
			$_SESSION['userid'] = $rowse['userid'];
			$_SESSION['useremail'] = $rowse['email'];
			$_SESSION['username'] = $rowse['name'];
			$_SESSION['userimage'] = $rowse['profilepic'];
			$response['status'] = 'success';
			$response['msg'] = 'Login success';
		}
		else
		{
			$referralcode = generate_random_referral();
		 	$insf = "INSERT INTO user SET name='".$name."',email='".$email."',profilepic='".$profpic."',facebookid='".$authid."',socialtype='".$socialtype."',devicetype='".$devicetype."',phone='".$phone."',referralcode='".$referralcode."',isNewUser='Y',invitecode='',corporateusercode='$corporatecode',corporate_id='$corporateid',isCorporateUser='$iscorporateuser',createdate=NOW()";
			$excf = mysqli_query($conn,$insf) or die(mysqli_error($conn));
			$userid = mysqli_insert_id($conn);
			if ($excf) {
				$_SESSION['userid'] = $userid;
				$_SESSION['useremail'] = $email;
				$_SESSION['username'] = $name;
				$_SESSION['userimage'] = $profpic;
				$response['status'] = 'success';
				$response['msg'] = 'Login success';
			}
		}
	}
	else if($socialtype=='google')
	{
		$quesp = "SELECT * FROM user WHERE email='".$email."'";	
		$excph = mysqli_query($conn,$quesp)or die(mysqli_error($conn));
		if(mysqli_num_rows($excph) > 0)
		{
			$rowse = mysqli_fetch_assoc($excph);
			$_SESSION['userid'] = $rowse['userid'];
			$_SESSION['useremail'] = $rowse['email'];
			$_SESSION['username'] = $rowse['name'];
			$_SESSION['userimage'] = $rowse['profilepic'];
			$response['status'] = 'success';
			$response['msg'] = 'Login success';
		}
		else
		{
			$referralcode = generate_random_referral();
			$insf = "INSERT INTO user SET name='".$name."',email='".$email."',profilepic='".$profpic."',gmailid='".$authid."',socialtype='".$socialtype."',devicetype='".$devicetype."',phone='".$phone."',referralcode='".$referralcode."',isNewUser='Y',invitecode='',corporateusercode='$corporatecode',corporate_id='$corporateid',isCorporateUser='$iscorporateuser',createdate=NOW()";
			$excf = mysqli_query($conn,$insf) or die(mysqli_error($conn));
			$userid = mysqli_insert_id($conn);
			if ($excf) {
				$_SESSION['userid'] = $userid;
				$_SESSION['useremail'] = $email;
				$_SESSION['username'] = $name;
				$_SESSION['userimage'] = $profpic;
				$response['status'] = 'success';
				$response['msg'] = 'Login success';
			}
		}
	}
	else
	{
		$quesp = "SELECT * FROM user WHERE email='".$email."'";	
		$excph = mysqli_query($conn,$quesp)or die(mysqli_error($conn));
		if(mysqli_num_rows($excph) > 0)
		{
			$response['status'] = 'exist';
			$response['msg'] = 'Email Id already Registered.';
		}
		else
		{

			$referralcode = generate_random_referral();
			$insf = "INSERT INTO user SET name='".$name."',email='".$email."',password='".$password."',profilepic='".$prof."',gmailid='".$authid."',socialtype='".$socialtype."',devicetype='".$devicetype."',phone='".$phone."',referralcode='".$referralcode."',isNewUser='Y',invitecode='',corporateusercode='$corporatecode',corporate_id='$corporateid',isCorporateUser='$iscorporateuser',createdate=NOW()";
			$excf = mysqli_query($conn,$insf) or die(mysqli_error($conn));
			if ($excf) {
				$lastid = mysqli_insert_id($conn);
				$_SESSION['userid'] = $lastid;
				$_SESSION['useremail'] = $email;
				$_SESSION['username'] = $name;
				$_SESSION['userimage'] = $prof;

				if($iscorporateuser == '1'){
					$_SESSION['corporateid'] = $corporateid;
					$_SESSION['isCorporateUser'] = $iscorporateuser;
					$response['isCorporateUser'] = $iscorporateuser;
				}else{
					$response['isCorporateUser'] = "0";
				}
				$queryK = "SELECT corporate_logo FROM corporate WHERE id = '$corporateid'";	
				$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
				$rowK = mysqli_fetch_assoc($resultK);
				$_SESSION['corporate_logo'] = $rowK['corporate_logo'];

				$quewallet = "SELECT * FROM walletmaster WHERE userid='".$lastid."'";	
				$excwallet = mysqli_query($conn,$quewallet)or die(mysqli_error($conn));
				if(mysqli_num_rows($excwallet) > 0)
				{
					
				}else{
					$insf = "INSERT INTO walletmaster SET userid='$lastid',total='0',cloudkitchValue='0',invitecode='1111'";
					$excf = mysqli_query($conn,$insf) or die(mysqli_error($conn));
				}

				$response['status'] = 'success';
				$response['msg'] = 'Registration Success.';
			}
			else
			{
				$response['status'] = 'failed';
				$response['msg'] = 'Something went wrong!';
			}
		}
	}
	print_r(json_encode($response));
}


/*
By: Jyoti Vishwakarma
Description: To check user existance
*/

if($_GET['service_name'] == 'isUserExist')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);

	$email = $json['email'];
	
	$quesp = "SELECT * FROM user WHERE email='".$email."'";	
	$excph = mysqli_query($conn,$quesp)or die(mysqli_error($conn));
	if(mysqli_num_rows($excph) > 0)
	{
		$rowse = mysqli_fetch_assoc($excph);
		$_SESSION['userid'] = $rowse['userid'];
		$_SESSION['useremail'] = $rowse['email'];
		$_SESSION['username'] = $rowse['name'];
		$_SESSION['userimage'] = $rowse['profilepic'];
		if($rowse['isCorporateUser']=='1'){
			$_SESSION['corporateid'] = $rowse['corporate_id'];
			$_SESSION['isCorporateUser'] = $rowse['isCorporateUser'];
			$response['isCorporateUser'] = $rowse['isCorporateUser'];
		}else{
			$response['isCorporateUser'] = "0";
		}
		
		$response['status'] = 'success';
		$response['msg'] = 'Login success';
	}
	else
	{
		$response['status'] = 'new';
		$response['msg'] = 'User Not Registered';
	}
	
	print_r(json_encode($response));
}
/*
By: Jyoti Vishwakarma
Description: Set pincode for restro and cuisines
*/

if($_GET['service_name'] == 'pincode')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);

	$pincode = $json['pincode'];

	$quesp = "SELECT * FROM user WHERE FIND_IN_SET($pincode, pin_code)";	
	$excph = mysqli_query($conn,$quesp)or die(mysqli_error($conn));
	
	if(mysqli_num_rows($excph) > 0)
	{
		setcookie("pincode", $pincode, time() + (86400 * 30), "/");
		$response['status'] = 'success';
		$response['msg'] = 'Found.';
	}else{
		$response['status'] = 'failed';
		$response['msg'] = 'Not Found.';
	}
	print_r(json_encode($response));
}

/*
By: Jyoti Vishwakarma
Description: delivery availability based on pincode
*/

if($_GET['service_name'] == 'pincode_checkout')
{
	$response = array();
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$flag = 0;
	$pincode = $json['pincode'];
	$userid = $_SESSION['userid'];
	$query = "SELECT * FROM cart WHERE userid='$userid'";
	$result1 = mysqli_query($conn,$query)or die(mysqli_error($conn));
	while ($row = mysqli_fetch_assoc($result1)) {
		$productid = $row['product_id']; 
		$queryP = "SELECT userid FROM cuisine WHERE cuisineid='$productid'";
		$resultP = mysqli_query($conn,$queryP)or die(mysqli_error($conn));
		$rowP = mysqli_fetch_assoc($resultP);
		$kitchenid = $rowP['userid'];
		$quesp = "SELECT * FROM user WHERE userid = '$kitchenid' AND FIND_IN_SET($pincode, pin_code)";	
		$excph = mysqli_query($conn,$quesp)or die(mysqli_error($conn));
		if(mysqli_num_rows($excph) > 0)
		{
			$flag = 1;
		}else{
			$flag = 0;
			break;
		}
	}

	if($flag == 1)
	{
		setcookie("pincode", $pincode, time() + (86400 * 30), "/");
		$response['status'] = 'success';
		$response['msg'] = 'Found.';
	}else{
		$response['status'] = 'failed';
		$response['msg'] = 'Not Found.';
	}
	print_r(json_encode($response));
}

/*
By: Jyoti Vishwakarma
Description: Login - manual, google and facebook login
*/
if($_GET['service_name'] == 'login')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);

	$email = $json['email'];
	$authid = $json['authid'];
	$phone = $json['phone'];
	$password = $json['password'];
	$logintype = $json['logintype'];
	if ($logintype=='facebook') {
		$quesp = "SELECT * FROM user WHERE facebookid='".$authid."'";	
		$excph = mysqli_query($conn,$quesp)or die(mysqli_error($conn));
		if(mysqli_num_rows($excph) > 0)
		{
			$response['status'] = 'success';
			$response['msg'] = 'Login success.';
		}
		else
		{
			$response['status'] = 'failure';
			$response['msg'] = 'You are not registered.';
		}
	}
	else if ($logintype=='google') {
		$quesp = "SELECT * FROM user WHERE gmailid ='".$authid."'";	
		$excph = mysqli_query($conn,$quesp)or die(mysqli_error($conn));
		if(mysqli_num_rows($excph) > 0)
		{
			$response['status'] = 'success';
			$response['msg'] = 'Login success.';
		}
		else
		{
			$response['status'] = 'failure';
			$response['msg'] = 'You are not registered.';
		}
	}
	else
	{
		$quesp = "SELECT * FROM user WHERE email='$email'";	
		$excph = mysqli_query($conn,$quesp)or die(mysqli_error($conn));
		
		if(mysqli_num_rows($excph) > 0)
		{
			$quesp1 = "SELECT * FROM user WHERE email='$email' AND password = '$password'";	
			$excph1 = mysqli_query($conn,$quesp1)or die(mysqli_error($conn));
			if(mysqli_num_rows($excph1) > 0)
			{
				$rowse = mysqli_fetch_assoc($excph1);
				$_SESSION['userid'] = $rowse['userid'];
				$_SESSION['useremail'] = $rowse['email'];
				$_SESSION['username'] = $rowse['name'];
				$_SESSION['userimage'] = $rowse['profilepic'];
				if($rowse['isCorporateUser']=='1'){
					$_SESSION['corporateid'] = $rowse['corporate_id'];
					$_SESSION['isCorporateUser'] = $rowse['isCorporateUser'];
					$response['isCorporateUser'] = $rowse['isCorporateUser'];
					$cropid = $rowse['corporate_id'];
					$queryK = "SELECT corporate_logo FROM corporate WHERE id = '$cropid'";	
					$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
					$rowK = mysqli_fetch_assoc($resultK);
					$_SESSION['corporate_logo'] = $rowK['corporate_logo'];

				}else{
					$response['isCorporateUser'] = "0";
				}
				$_SESSION['useraddress']=array();
				$userid= $rowse['userid'];
				$query = "SELECT * FROM useraddresses WHERE userid = '$userid' ORDER BY useraddressid ASC";
				$result = mysqli_query($conn,$query) or die(mysqli_error($conn));
				if(mysqli_num_rows($result) > 0)
				{
					$row = mysqli_fetch_assoc($result);
					$_SESSION['useraddress']['id'] = $row['useraddressid'];
					$_SESSION['useraddress']['addresstype'] = $row['addresstitle'];
					$_SESSION['useraddress']['address'] = $row['address'].', '.$row['city'].', '.$row['state'].', '.$row['pincode'];
				}
				$response['status'] = 'success';
				$response['msg'] = 'Login success';
			}
			else
			{
				$response['status'] = 'failure';
				$response['msg'] = 'Invalid passsword';
			}
		}
		else
		{
			$response['status'] = 'failure';
			$response['msg'] = 'Your email is not registered.';
		}
	}
	print_r(json_encode($response));
}


/*
By: Jyoti Vishwakarma
Description: logout
*/

if($_GET['service_name'] == 'logout')
{
	// session_destroy();
	
	setcookie('mealstype', "corporate_meals",  time() - 3600, "/");
	session_unset();
	session_destroy();
	session_write_close();
	setcookie(session_name(),'',0,'/');
	session_regenerate_id(true);
	header("Refresh:0");
}

/*
By: Dilip Kumawat
Created at: 18 Nov, 2019
Created at: 18 Nov, 2019
Description: Kitchen List 
*/

if($_GET['service_name'] == 'kitchens')
{
	if(isset($_COOKIE['pincode'])){
		$pincode = $_COOKIE['pincode'];
		$location = "AND pin_code LIKE '%$pincode%'";
	}else{
		$location = "";
	}


	$query = "SELECT * FROM user WHERE status='1' AND ischef = 1 $location ORDER BY userid DESC";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	
	$data = array();
	$data['kitchens'] = array();
	while($row = mysqli_fetch_assoc($result))
	{
		$kitchens = array();
		$id = $row['userid'];
		$kitchens['id'] = $row['userid'];
		$kitchens['name'] = $row['name'];

		$newTitle = $row['name'];
		$newTitle1 = trim($newTitle);
		$newTitle1= preg_replace('/\s+/', '-', $newTitle1);
		$kitchens['name1'] = $newTitle1;

		$kitchens['tagline'] = $row['tagline'];
		if ($row['profilepic']!="") {
			$image_name = explode("/",$row['profilepic']);
			$image_name = array_pop($image_name);
			$kitchens['image'] = S3URL.$image_name;
		}
		else
		{
			$kitchens['image'] = "images/food.png";
		}
		$queryR = "SELECT count(id) as count, rating FROM chefrating WHERE chefId = '$id' GROUP BY rating";	
		$resultR = mysqli_query($conn,$queryR)or die(mysqli_error($conn));
		$rating1 = 0;
		$rating2 = 0;
		$rating3 = 0;
		$rating4 = 0;
		$rating5 = 0;
		while ($rowR = mysqli_fetch_assoc($resultR)) {
			$rating = $rowR['rating'];
			if ($rating==1) {
				$rating1 = (int)$rowR['count'];
			}
			if ($rating==2) {
				$rating2 = (int)$rowR['count'];
			}
			if ($rating==3) {
				$rating3 = (int)$rowR['count'];
			}
			if ($rating==4) {
				$rating4 = (int)$rowR['count'];
			}
			if ($rating==5) {
				$rating5 = (int)$rowR['count'];
			}
		}
		$ratingTotal = $rating5+$rating4+$rating3+$rating2+$rating1;
		if ($ratingTotal>0) {
			$rating = (5*$rating5 + 4*$rating4 + 3*$rating3 + 2*$rating2 + 1*$rating1) / ($rating5+$rating4+$rating3+$rating2+$rating1);
		}
		else
		{
			$rating = 0;
		}
		$kitchens['rating'] = round($rating);
		array_push($data['kitchens'], $kitchens);
	}
	print_r(json_encode($data));
}

/*
By: Dilip Kumawat
Created at: 18 Nov, 2019
Created at: 18 Nov, 2019
Description: Category list
*/

if($_GET['service_name'] == 'categories')
{
	$data = array();
	$data['categories'] = array();

	$querySq = "SELECT * FROM `kitchen_cuisinetype_sequence` ORDER BY sequence ASC";	
	$resultsq = mysqli_query($conn,$querySq)or die(mysqli_error($conn));
	$seq = array();
	while($rowSq = mysqli_fetch_assoc($resultsq)) {
		$seq[count($seq)] = $rowSq['cuisinetypeid'];
	}
	$catseq = implode(",",$seq);

	$queryC = "SELECT cuisinetypeId FROM cuisine WHERE isActive = 'Y' AND isCorporateMeal = '0' AND isEventMeal = '0' GROUP BY cuisinetypeId ORDER BY FIELD(cuisinetypeId, $catseq)";	
	$resultC = mysqli_query($conn,$queryC)or die(mysqli_error($conn));
	while ($rowC = mysqli_fetch_assoc($resultC)) {

	$cuisinetypeid = $rowC['cuisinetypeId'];
	$query = "SELECT * FROM cuisinetypemaster WHERE cuisinetypeid = '$cuisinetypeid' ";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	
	
	while ($row = mysqli_fetch_assoc($result)) {
		$categories = array();
		$categories['id'] = $row['cuisinetypeid'];
		$categories['category'] = $row['cuisinetype'];
		array_push($data['categories'], $categories);
	}	
	}
	print_r(json_encode($data));
}

/*
By: Jyoti Vishwakarma
Description: Team meals category
*/
if($_GET['service_name'] == 'teamcategories')
{

	
	$querySq = "SELECT * FROM `kitchen_cuisinetype_sequence` WHERE kitchen_id = '$id' ORDER BY sequence ASC";	
	$resultsq = mysqli_query($conn,$querySq)or die(mysqli_error($conn));
	$seq = array();
	while($rowSq = mysqli_fetch_assoc($resultsq)) {
		$seq[count($seq)] = $rowSq['cuisinetypeid'];
	}
	$catseq = implode(",",$seq);

	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$kitchenid = $json['kitchenid'];
	if($kitchenid != ""){
		$condition = "userid = '$kitchenid' AND";
	}else{
		$condition = "";
	}

	$data = array();
	$data['categories'] = array();

	$queryC = "SELECT cuisinetypeId FROM cuisine WHERE $condition isActive = 'Y' AND isCorporateMeal = '1' AND isEventMeal = '0' GROUP BY cuisinetypeId  ORDER BY FIELD(cuisinetypeId, $catseq)";	
	$resultC = mysqli_query($conn,$queryC)or die(mysqli_error($conn));
	while ($rowC = mysqli_fetch_assoc($resultC)) {

	$cuisinetypeid = $rowC['cuisinetypeId'];
	$query = "SELECT * FROM cuisinetypemaster WHERE cuisinetypeid = '$cuisinetypeid' ";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	
	
	while ($row = mysqli_fetch_assoc($result)) {
		$categories = array();
		$categories['id'] = $row['cuisinetypeid'];
		$categories['category'] = $row['cuisinetype'];
		array_push($data['categories'], $categories);
	}	
	}
	print_r(json_encode($data));
}

/*
By: Jyoti Vishwakarma
Description: Event meals Category
*/

if($_GET['service_name'] == 'eventcategories')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$kitchenid = $json['kitchenid'];
	if($kitchenid != ""){
		$condition = "userid = '$kitchenid' AND";
	}else{
		$condition = "";
	}

	$querySq = "SELECT * FROM `kitchen_cuisinetype_sequence` WHERE kitchen_id = '$id' ORDER BY sequence ASC";	
	$resultsq = mysqli_query($conn,$querySq)or die(mysqli_error($conn));
	$seq = array();
	while($rowSq = mysqli_fetch_assoc($resultsq)) {
		$seq[count($seq)] = $rowSq['cuisinetypeid'];
	}
	$catseq = implode(",",$seq);

	$data = array();
	$data['categories'] = array();

	$queryC = "SELECT cuisinetypeId FROM cuisine WHERE $condition isActive = 'Y' AND isCorporateMeal = '0' AND isEventMeal = '1' GROUP BY cuisinetypeId ORDER BY FIELD(cuisinetypeId, $catseq)";	
	$resultC = mysqli_query($conn,$queryC)or die(mysqli_error($conn));
	while ($rowC = mysqli_fetch_assoc($resultC)) {

	$cuisinetypeid = $rowC['cuisinetypeId'];
	$query = "SELECT * FROM cuisinetypemaster WHERE cuisinetypeid = '$cuisinetypeid' ";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	
	
	while ($row = mysqli_fetch_assoc($result)) {
		$categories = array();
		$categories['id'] = $row['cuisinetypeid'];
		$categories['category'] = $row['cuisinetype'];
		array_push($data['categories'], $categories);
	}	
}
	print_r(json_encode($data));
}

/*
By: Jyoti Vishwakarma
Description: Cuisine lists
*/

if($_GET['service_name'] == 'cuisines')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$keyword = $json['keyword'];
	$categories = $json['categories'];
	$sortbycost = $json['sortbycost'];
	$kitchenid = $json['kitchenid'];
	$limit = "";
	if(isset($json['limit'])){
		$limit = $json['limit'];
	}
	$type = $json['type'];
	$orderby = "ORDER BY cuisineid DESC";
	$limitss = "limit 0,30";

	$condition = "";
	if ($keyword!="") {
		$condition .= " AND (cuisinename LIKE '%$keyword%' OR tags LIKE '%$keyword%')";
	}
	if($categories!="")
	{
		$condition .= " AND homepagecatid = $categories";
	}
	if($type!="")
	{
		$condition .= " AND type LIKE '$type%'";
	}

	if($sortbycost!="")
	{
		$orderby = "ORDER BY price $sortbycost";
	}

	if($limit!="")
	{
		$d = $limit * 30;
		$limitss = "limit  $d, 30";
	}

	$kitchen = "";
	if ($kitchenid!="") {
		$condition .= " AND userid = '$kitchenid'";
	}

	if(isset($_COOKIE['pincode'])){
		$pincode = $_COOKIE['pincode'];
		$location = "AND cuisine.userid in (SELECT userid FROM `user` WHERE pin_code LIKE '%$pincode%')";
	}else{
		$location = "";
	}

	$corporateid = '';
	if(isset($_SESSION['corporateid'])){
		$corporateid = $_SESSION['corporateid'];

	}
	$corporate_condition = "";
	if(isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] =='1'){
		$corporate_id = $_SESSION['corporateid'];
		$corporate_condition =  "AND FIND_IN_SET($corporate_id, invitecode)";
	}

	$query = "SELECT * FROM cuisine WHERE isActive = 'Y' AND status='1' AND isCorporateMeal = '0' AND isEventMeal = '0' $corporate_condition $location $condition $orderby";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));


	
	$data = array();
	$data['cuisines'] = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$cuisine = array();
		$id = $row['cuisineid'];
		$cuisine['id'] = $row['cuisineid'];
		$cuisine['name'] = $row['cuisinename'];
		$cuisine['description'] = $row['cuisinedescription'];
		if($row['cuisinedescription'] != ""){
			$desc1 = strip_tags($row['cuisinedescription']);
			if(strlen($desc1) > 100){
				$cuisine['description'] = substr($desc1,0,100)."...";
			}else{
				$cuisine['description'] = substr($desc1,0,100);
			}
		}
		
		$cuisine['price'] = $row['price'];
		$cuisine['serves'] = $row['servingdetail'];
		$cuisine['type'] = $row['type'];

		$userid = $row['userid'];

		$queryK = "SELECT * FROM user WHERE userid = '$userid'";	
		$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
		$rowK = mysqli_fetch_assoc($resultK);
		$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
		$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";
		$cuisine['kitchen_workingstart'] = isset($rowK['workingstart']) ? $rowK['workingstart'] : "";
		$cuisine['kitchen_workingend'] = isset($rowK['workingend']) ? $rowK['workingend'] : "";

		if ($row['cuisinepic']!="") {
			$image_name = explode("/",$row['cuisinepic']);
			$image_name = array_pop($image_name);
			$cuisine['image'] = S3URL.$image_name;
		}
		else
		{
			$cuisine['image'] = "images/food.png";
		}
		$cuisine['offer']='';
		$cuisine['discountprice'] = '';
		$offer = 0;
		if($corporateid != ''){
			$offer = 0;
			$queryO = "SELECT offer FROM corporateoffermapping WHERE status='1' AND restaurantid = '$userid' AND corporateid='$corporateid'";	
			$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
			$rowO = mysqli_fetch_assoc($resultO);
			$cuisine['offer'] = isset($rowO['offer']) ? $rowO['offer'] : "";
			$offer = $rowO['offer'];
			$discountprice = round($row['price'] - ( ( $row['price'] * $offer ) / 100 ));
			$cuisine['discountprice'] = $discountprice;

		}else{
			$offer = 0;
			$queryO = "SELECT offer FROM offeroncuisine WHERE status='1' AND kitchen_id = '$userid'";	
			$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
			$rowO = mysqli_fetch_assoc($resultO);
			$cuisine['offer'] = isset($rowO['offer']) ? $rowO['offer'] : "";
			$offer = $rowO['offer'];
			$discountprice = round($row['price'] - ( ( $row['price'] * $offer ) / 100 ));
			$cuisine['discountprice'] = $discountprice;
		}
		$cuisine['isCustomisable']='0';
		$query2 = "SELECT * FROM corporate_cuisine_sections WHERE cuisine_id='$id'";
		$result2 = mysqli_query($conn,$query2) or die(mysqli_error($conn));
		if(mysqli_num_rows($result2) > 0 ){
			$cuisine['isCustomisable']='1';
		}
		
		array_push($data['cuisines'], $cuisine);
	}	
	print_r(json_encode($data));
}

/*
By: Jyoti Vishwakarma
Description: Cuisines list based on offer
*/
if($_GET['service_name'] == 'offercuisines')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$keyword = $json['keyword'];
	$categories = $json['categories'];
	$sortbycost = $json['sortbycost'];
	$kitchenid = $json['kitchenid'];
	$limit = "";
	if(isset($json['limit'])){
		$limit = $json['limit'];
	}
	$type = $json['type'];
	$orderby = "ORDER BY cuisineid DESC";
	$limitss = "limit 0,30";

	$condition = "";
	if ($keyword!="") {
		$condition .= " AND (cuisinename LIKE '%$keyword%' OR tags LIKE '%$keyword%')";
	}
	if($categories!="")
	{
		$condition .= " AND homepagecatid = $categories";
	}
	if($type!="")
	{
		$condition .= " AND type LIKE '$type%'";
	}

	if($sortbycost!="")
	{
		$orderby = "ORDER BY price $sortbycost";
	}

	if($limit!="")
	{
		$d = $limit * 30;
		$limitss = "limit  $d, 30";
	}

	$kitchen = "";
	if ($kitchenid!="") {
		$condition .= " AND userid = '$kitchenid'";
	}

	if(isset($_COOKIE['pincode'])){
		$pincode = $_COOKIE['pincode'];
		$location = "AND cuisine.userid in (SELECT userid FROM `user` WHERE pin_code LIKE '%$pincode%')";
	}else{
		$location = "";
	}

	$corporateid = '';
	if(isset($_SESSION['corporateid'])){
		$corporateid = $_SESSION['corporateid'];

	}
	$corporate_condition = "";
	if(isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] =='1'){
		$corporate_id = $_SESSION['corporateid'];
		$corporate_condition =  "AND FIND_IN_SET($corporate_id, invitecode)";
	}



	$query = "SELECT * FROM cuisine WHERE isActive = 'Y' AND status='1' AND isCorporateMeal = '0' AND isEventMeal = '0' $corporate_condition $location $condition $orderby $limitss";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));


	
	$data = array();
	$data['cuisines'] = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$cuisine = array();
		$id = $row['cuisineid'];
		$cuisine['id'] = $row['cuisineid'];
		$cuisine['name'] = $row['cuisinename'];
		$cuisine['description'] = $row['cuisinedescription'];
		if($row['cuisinedescription'] != ""){
			$desc1 = strip_tags($row['cuisinedescription']);
			if(strlen($desc1) > 100){
				$cuisine['description'] = substr($desc1,0,100)."...";
			}else{
				$cuisine['description'] = substr($desc1,0,100);
			}
		}
		
		$cuisine['price'] = $row['price'];
		$cuisine['serves'] = $row['servingdetail'];
		$cuisine['type'] = $row['type'];

		$userid = $row['userid'];

		$queryK = "SELECT * FROM user WHERE userid = '$userid'";	
		$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
		$rowK = mysqli_fetch_assoc($resultK);
		$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
		$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";

		if ($row['cuisinepic']!="") {
			$image_name = explode("/",$row['cuisinepic']);
			$image_name = array_pop($image_name);
			$cuisine['image'] = S3URL.$image_name;
		}
		else
		{
			$cuisine['image'] = "images/food.png";
		}
		$cuisine['offer']='';
		$cuisine['discountprice'] = '';
		if($corporateid != ''){
			$offer = 0;
			$queryO = "SELECT offer FROM corporateoffermapping WHERE status='1' AND restaurantid = '$userid' AND corporateid='$corporateid'";	
			$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
			$rowO = mysqli_fetch_assoc($resultO);
			$cuisine['offer'] = isset($rowO['offer']) ? $rowO['offer'] : "";
			$offer = $rowO['offer'];
			$discountprice = round($row['price'] - ( ( $row['price'] * $offer ) / 100 ));
			$cuisine['discountprice'] = $discountprice;

		}else{
			$offer = 0;
			$queryO = "SELECT offer FROM offeroncuisine WHERE status='1' AND kitchen_id = '$userid'";	
			$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
			$rowO = mysqli_fetch_assoc($resultO);
			$cuisine['offer'] = isset($rowO['offer']) ? $rowO['offer'] : "";
			$offer = $rowO['offer'];
			$discountprice = round($row['price'] - ( ( $row['price'] * $offer ) / 100 ));
			$cuisine['discountprice'] = $discountprice;
		}
		$cuisine['isCustomisable']='0';
		$query2 = "SELECT * FROM corporate_cuisine_sections WHERE cuisine_id='$id'";
		$result2 = mysqli_query($conn,$query2) or die(mysqli_error($conn));
		if(mysqli_num_rows($result2) > 0 ){
			$cuisine['isCustomisable']='1';
		}

		
		array_push($data['cuisines'], $cuisine);
	}

	$keys = array_column($data['cuisines'], 'discountprice');
	array_multisort($keys, SORT_ASC, $data['cuisines']);

	print_r(json_encode($data));
}

/*
By: Jyoti Vishwakarma
Description: Cafeteria cuisine list
*/
if($_GET['service_name'] == 'cafeteria')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$keyword = $json['keyword'];
	$categories = $json['categories'];
	$sortbycost = $json['sortbycost'];
	$kitchenid = $json['kitchenid'];
	$limit = "";
	if(isset($json['limit'])){
		$limit = $json['limit'];
	}
	$corporate_id = $_SESSION['corporateid'];
	$type = $json['type'];
	$orderby = "ORDER BY cuisineid DESC";
	$limitss = "limit 0,30";

	$condition = "";
	if ($keyword!="") {
		$condition .= " AND (cuisinename LIKE '%$keyword%' OR tags LIKE '%$keyword%')";
	}
	if($categories!="")
	{
		$condition .= " AND homepagecatid = $categories";
	}
	if($type!="")
	{
		$condition .= " AND type LIKE '$type%'";
	}

	if($sortbycost!="")
	{
		$orderby = "ORDER BY price $sortbycost";
	}

	// if($limit!="")
	// {
	// 	$d = $limit * 30;
	// 	$limitss = "limit  $d, 30";
	// }

	$kitchen = "";
	if ($kitchenid!="") {
		$condition .= " AND userid = '$kitchenid'";
	}

	if(isset($_COOKIE['pincode'])){
		$pincode = $_COOKIE['pincode'];
		$location = "AND cuisine.userid in (SELECT userid FROM `user` WHERE pin_code LIKE '%$pincode%')";
	}else{
		$location = "";
	}

	$corporateid = '';
	if(isset($_SESSION['corporateid'])){
		$corporateid = $_SESSION['corporateid'];

	}

	$query = "SELECT * FROM cuisine WHERE isActive = 'Y' AND status='1' AND isCorporateMeal = '4' AND isEventMeal = '0' AND FIND_IN_SET($corporate_id, invitecode) $location $condition $orderby";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));


	
	$data = array();
	$data['cuisines'] = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$cuisine = array();
		$id = $row['cuisineid'];
		$cuisine['id'] = $row['cuisineid'];
		$cuisine['name'] = $row['cuisinename'];
		$cuisine['description'] = $row['cuisinedescription'];
		if($row['cuisinedescription'] != ""){
			$desc1 = strip_tags($row['cuisinedescription']);
			if(strlen($desc1) > 100){
				$cuisine['description1'] = substr($desc1,0,100)."...";
			}else{
				$cuisine['description1'] = substr($desc1,0,100);
			}
			
			
		}
		$cuisine['price'] = $row['price'];
		$cuisine['serves'] = $row['servingdetail'];
		$cuisine['type'] = $row['type'];

		$userid = $row['userid'];

		$queryK = "SELECT * FROM user WHERE userid = '$userid'";	
		$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
		$rowK = mysqli_fetch_assoc($resultK);
		$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
		$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";

		if ($row['cuisinepic']!="") {
			$image_name = explode("/",$row['cuisinepic']);
			$image_name = array_pop($image_name);
			$cuisine['image'] = S3URL.$image_name;
		}
		else
		{
			$cuisine['image'] = "images/food.png";
		}

		if($corporateid != '' && $userid != ''){
			$queryO = "SELECT offer FROM corporateoffermapping WHERE restaurantid = '$userid' AND corporateid='$corporateid'";	
			$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
			$rowO = mysqli_fetch_assoc($resultO);
			$cuisine['offer'] = isset($rowO['offer']) ? $rowO['offer'] : "";
		}else{
			$cuisine['offer']='';
		}



		array_push($data['cuisines'], $cuisine);
	}	
	print_r(json_encode($data));
}

/*
By: Jyoti Vishwakarma
Description: Team meals list
*/
if($_GET['service_name'] == 'corporatemeals')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$keyword = $json['keyword'];
	$categories = $json['categories'];
	$sortbycost = $json['sortbycost'];
	$type = $json['type'];
	$kitchenid = $json['kitchenid'];
	if($kitchenid != ""){
		$restocondition = "userid = '$kitchenid' AND";
	}else{
		$restocondition = "";
	}

	$orderby = "ORDER BY cuisineid DESC";
	$condition = "";
	if ($keyword!="") {
		$condition .= " AND (cuisinename LIKE '%$keyword%' OR tags LIKE '%$keyword%')";
	}
	if($categories!="")
	{
		$condition .= " AND homepagecatid IN ($categories)";
	}
	if($type!="")
	{
		$condition .= " AND type LIKE '$type%'";
	}

	if($sortbycost!="")
	{
		$orderby = "ORDER BY price $sortbycost";
	}
	$corporate_id = $_SESSION['corporateid'];

	
	
	$query = "SELECT * FROM cuisine WHERE $restocondition isActive = 'Y' AND status='1' AND isCorporateMeal='1' AND FIND_IN_SET($corporate_id, invitecode) $condition $orderby";	
	//$query = "SELECT * FROM cuisine WHERE isActive = 'Y' AND isCorporateMeal='1' ORDER BY price";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	
	$data = array();
	$data['cuisines'] = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$cuisine = array();
		$id = $row['cuisineid'];
		$cuisine['id'] = $row['cuisineid'];
		$cuisine['name'] = $row['cuisinename'];
		$cuisine['description'] = $row['cuisinedescription'];
		if($row['cuisinedescription'] != ""){
			$desc1 = strip_tags($row['cuisinedescription']);
			if(strlen($desc1) > 99){
				$cuisine['description1'] = substr($desc1,0,100)."...";
			}else{
				$cuisine['description1'] = substr($desc1,0,100);
			}
		}
		$cuisine['price'] = $row['price'];
		$cuisine['leadtime'] = $row['leadtime'];
		$cuisine['minorderquantity'] = $row['minorderquantity'];
		$cuisine['type'] = $row['type'];
		$userid = $row['userid'];

		$queryK = "SELECT * FROM user WHERE userid = '$userid'";	
		$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
		$rowK = mysqli_fetch_assoc($resultK);
		$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
		$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";

		if ($row['cuisinepic']!="") {
			$image_name = explode("/",$row['cuisinepic']);
			$image_name = array_pop($image_name);
			$cuisine['image'] = S3URL.$image_name;
		}
		else
		{
			$cuisine['image'] = "https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png";
		}
		array_push($data['cuisines'], $cuisine);
	}	
	print_r(json_encode($data));
}

/*
By: Jyoti Vishwakarma
Description: Event meals list
*/
if($_GET['service_name'] == 'eventmeals')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$keyword = $json['keyword'];
	$categories = $json['categories'];
	$sortbycost = $json['sortbycost'];
	$type = $json['type'];
	$kitchenid = $json['kitchenid'];
	if($kitchenid != ""){
		$restocondition = "userid = '$kitchenid' AND";
	}else{
		$restocondition = "";
	}

	$orderby = "ORDER BY cuisineid DESC";
	$condition = "";
	if ($keyword!="") {
		$condition .= " AND (cuisinename LIKE '%$keyword%' OR tags LIKE '%$keyword%')";
	}
	if($categories!="")
	{
		$condition .= " AND homepagecatid IN ($categories)";
	}
	if($type!="")
	{
		$condition .= " AND type LIKE '$type%'";
	}

	if($sortbycost!="")
	{
		$orderby = "ORDER BY price $sortbycost";
	}

	$corporate_id = $_SESSION['corporateid'];

	$query = "SELECT * FROM cuisine WHERE $restocondition isActive = 'Y' AND status='1' AND isEventMeal= 1 AND FIND_IN_SET($corporate_id, invitecode) $condition $orderby";
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	
	$data = array();
	$data['cuisines'] = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$cuisine = array();
		$id = $row['cuisineid'];
		$cuisine['id'] = $row['cuisineid'];
		$cuisine['name'] = $row['cuisinename'];
		$cuisine['description'] = $row['cuisinedescription'];
		if($row['cuisinedescription'] != ""){
			$desc1 = strip_tags($row['cuisinedescription']);
			if(strlen($desc1) > 99){
				$cuisine['description1'] = substr($desc1,0,100)."...";
			}else{
				$cuisine['description1'] = substr($desc1,0,100);
			}
		}
		$cuisine['price'] = $row['price'];
		$cuisine['leadtime'] = $row['leadtime'];
		$cuisine['minorderquantity'] = $row['minorderquantity'];
		$cuisine['type'] = $row['type'];
		$userid = $row['userid'];

		$queryK = "SELECT * FROM user WHERE userid = '$userid'";	
		$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
		$rowK = mysqli_fetch_assoc($resultK);
		$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
		$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";

		if ($row['cuisinepic']!="") {
			$image_name = explode("/",$row['cuisinepic']);
			$image_name = array_pop($image_name);
			$cuisine['image'] = S3URL.$image_name;
		}
		else
		{
			$cuisine['image'] = "https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png";
		}
		array_push($data['cuisines'], $cuisine);
	}	
	print_r(json_encode($data));
}

/*
By: Jyoti Vishwakarma
Description: Society and kitty party meals list
*/
if($_GET['service_name'] == 'meals')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$keyword = $json['keyword'];
	$categories = $json['categories'];
	$sortbycost = $json['sortbycost'];
	$type = $json['type'];
	$kitchenid = $json['kitchenid'];
	if($kitchenid != ""){
		$restocondition = "userid = '$kitchenid' AND";
	}else{
		$restocondition = "";
	}
	$mealstype = $json['mealstype'];
	if($mealstype == 'society'){
		$mealcondition = "AND isCorporateMeal='5'";
	}else if($mealstype == 'kitty'){
		$mealcondition = "AND isCorporateMeal='6'";
	}
	
	

	$orderby = "ORDER BY cuisineid DESC";
	$condition = "";
	if ($keyword!="") {
		$condition .= " AND (cuisinename LIKE '%$keyword%' OR tags LIKE '%$keyword%')";
	}
	if($categories!="")
	{
		$condition .= " AND homepagecatid IN ($categories)";
	}
	if($type!="")
	{
		$condition .= " AND type LIKE '$type%'";
	}

	if($sortbycost!="")
	{
		$orderby = "ORDER BY price $sortbycost";
	}
	$corporate_id = $_SESSION['corporateid'];

	
	
	$query = "SELECT * FROM cuisine WHERE $restocondition isActive = 'Y' AND status='1' $mealcondition $condition $orderby";	
	//$query = "SELECT * FROM cuisine WHERE isActive = 'Y' AND isCorporateMeal='1' ORDER BY price";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	
	$data = array();
	$data['cuisines'] = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$cuisine = array();
		$id = $row['cuisineid'];
		$cuisine['id'] = $row['cuisineid'];
		$cuisine['name'] = $row['cuisinename'];
		$cuisine['description'] = $row['cuisinedescription'];
		if($row['cuisinedescription'] != ""){
			$desc1 = strip_tags($row['cuisinedescription']);
			if(strlen($desc1) > 99){
				$cuisine['description1'] = substr($desc1,0,100)."...";
			}else{
				$cuisine['description1'] = substr($desc1,0,100);
			}
		}
		$cuisine['price'] = $row['price'];
		$cuisine['leadtime'] = $row['leadtime'];
		$cuisine['minorderquantity'] = $row['minorderquantity'];
		$cuisine['type'] = $row['type'];
		$userid = $row['userid'];

		$queryK = "SELECT * FROM user WHERE userid = '$userid'";	
		$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
		$rowK = mysqli_fetch_assoc($resultK);
		$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
		$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";

		if ($row['cuisinepic']!="") {
			$image_name = explode("/",$row['cuisinepic']);
			$image_name = array_pop($image_name);
			$cuisine['image'] = S3URL.$image_name;
		}
		else
		{
			$cuisine['image'] = "https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png";
		}
		array_push($data['cuisines'], $cuisine);
	}	
	print_r(json_encode($data));
}

/*
By: Jyoti Vishwakarma
Description: Meals details
*/
if($_GET['service_name'] == 'corporatemeals_detail')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$id = $json['id'];
	
	$query = "SELECT * FROM cuisine WHERE cuisineid = $id";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	
	$data = array();
	$data['cuisines'] = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$cuisine = array();
		$id = $row['cuisineid'];
		$cuisine['id'] = $row['cuisineid'];
		$cuisine['name'] = $row['cuisinename'];
		$cuisine['image'] = $row['cuisinepic'];
		$cuisine['description'] = $row['cuisinedescription'];
		$cuisine['price'] = $row['price'];
		$cuisine['leadtime'] = $row['leadtime'];
		$cuisine['minorderquantity'] = $row['minorderquantity'];
		$cuisine['type'] = $row['type'];
		$userid = $row['userid'];
		$offer = 0;
		if($row['isCorporateMeal'] =='0' && $row['isEventMeal'] == '0'){
			if(isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] =='1'){
				$corporateid = '';
				if(isset($_SESSION['corporateid'])){
					$corporateid = $_SESSION['corporateid'];
				}
				$offer = 0;
				$queryO = "SELECT offer FROM corporateoffermapping WHERE status='1' AND restaurantid = '$userid' AND corporateid='$corporateid'";	
				$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
				$rowO = mysqli_fetch_assoc($resultO);
				$cuisine['offer'] = isset($rowO['offer']) ? $rowO['offer'] : "";
				$offer = $rowO['offer'];
				$discountprice = round($row['price'] - ( ( $row['price'] * $offer ) / 100 ));
				$cuisine['discountprice'] = $discountprice;
			}else{
				$offer = 0;
				$queryO = "SELECT offer FROM offeroncuisine WHERE status='1' AND kitchen_id = '$userid'";	
				$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
				$rowO = mysqli_fetch_assoc($resultO);
				$cuisine['offer'] = isset($rowO['offer']) ? $rowO['offer'] : "";
				$offer = $rowO['offer'];
				$discountprice = round($row['price'] - ( ( $row['price'] * $offer ) / 100 ));
				$cuisine['discountprice'] = $discountprice;
			}
			
		}

		//Aradhana
		
		$queryK = "SELECT * FROM user WHERE userid = '$userid'";	
		$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
		$rowK = mysqli_fetch_assoc($resultK);
		$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
		$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";
		$cuisine['kitchen_open'] = isset($rowK['workingstart']) ? $rowK['workingstart'] : "";
		$cuisine['kitchen_close'] = isset($rowK['workingend']) ? $rowK['workingend'] : "";

		if ($row['cuisinepic']!="") {
			$image_name = explode("/",$row['cuisinepic']);
			$image_name = array_pop($image_name);
			$cuisine['image'] = S3URL.$image_name;
		}
		else
		{
			$cuisine['image'] = "images/food.png";
		}
		//Sections
		$cuisine['sections'] = array();
		$query2 = "SELECT * FROM corporate_cuisine_sections WHERE cuisine_id='$id'";
		$result2 = mysqli_query($conn,$query2) or die(mysqli_error($conn));
		while($row2 = mysqli_fetch_assoc($result2))
		{    
				$sections=array();
				$secid = $row2['ID'];
				$heading = $row2['heading'];
				$sections['title'] =  $heading;
				$sections['section_id'] =  $secid;
				$sections['choicetype'] = $row2['choice_type'];
				$sections['noofchoice'] = $row2['no_of_choice'];
				$sections['addons'] = array();

				$query3 = "SELECT * FROM corporate_cuisine_sections_addons WHERE section_id='$secid'";
				$result3 = mysqli_query($conn,$query3) or die(mysqli_error($conn));
				while($row3 = mysqli_fetch_assoc($result3))
				{
					$addon = array();
					$detail = $row3['addon'];
					$price = $row3['price'];
					$ctype = $row3['cuisinetype'];
					$addon['addon_id'] = $row3['ID'];
					$addon['detail'] = $detail;
					$addon['price'] = $price;
					$addon['strikeprice'] = "";
					if($row['isCorporateMeal'] =='0' && $row['isEventMeal'] == '0' && $offer != 0){
						$discountprice = round($price - ( ( $price * $offer ) / 100 ));
						$addon['price'] = $discountprice;
						$addon['strikeprice'] = $price;
					}

					

					$addon['cuisinetype'] = $ctype ;
					array_push($sections['addons'], $addon);
				}
				array_push($cuisine['sections'], $sections);
		}
		
			// $query1 = "SELECT * FROM `corporate_cuisine_sections_addons` where section_id IN (SELECT ID FROM `corporate_cuisine_sections` where cuisine_id = $id)";	
			// //$query = "SELECT * FROM cuisine WHERE isActive = 'Y' AND isCorporateMeal='1' ORDER BY price";	
			// $result1 = mysqli_query($conn,$query1)or die(mysqli_error($conn));
			
			// $data1 = array();
			// while ($row1 = mysqli_fetch_assoc($result1)) {
			// 	$addon = array();
			// 	$addon["add_id"] = $row1["ID"];
			// 	$addon["addon"] = $row1["addon"];
			// 	$addon["price"] = $row1["price"];
			// 	array_push($data1, $addon);
			// }

			$cuisine['selectedaddons'] = "";
			if (isset($_SESSION['userid'])) {
				$userid = $_SESSION['userid'];
				$query = "SELECT * FROM cart WHERE userid='$userid' AND product_id='$id'";
				$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
				while ($row = mysqli_fetch_assoc($result)) {
					
					if($row['selectedaddons'] != ""){
						$selectedaddons = array_filter(explode(",",$row['selectedaddons'])); 
						$selectedaddons = implode(",",$selectedaddons);
					}else{
						$selectedaddons = "''"; 
					}
					$cuisine['selectedaddons'] = $selectedaddons;
				}
			}else{
				foreach ($_COOKIE as $key=>$val) {
					if (substr($key, 0, 10) == "cartcookie") {
						if($val == $id){
							$selectedaddons = $_COOKIE['cookieselectedaddons'.$val];
							if($selectedaddons != ""){
								$selectedaddons = array_filter(explode(",",$selectedaddons)); 
								$selectedaddons = implode(",",$selectedaddons);
							}else{
								$selectedaddons = "''"; 
							}
							$cuisine['selectedaddons'] = $selectedaddons;
						}						
					}
				}
			}

		array_push($data['cuisines'], $cuisine);
	}	
	print_r(json_encode($data));
}

/*
By: Jyoti Vishwakarma
Description: Cafeteria cuisine description
*/
if($_GET['service_name'] == 'cafeteria_cuisine_desc')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$id = $json['id'];
	
	$query = "SELECT * FROM cuisine WHERE cuisineid = $id";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	
	$data = array();
	$data['cuisines'] = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$cuisine = array();
		$id = $row['cuisineid'];
		$cuisine['id'] = $row['cuisineid'];
		$cuisine['name'] = $row['cuisinename'];
		$cuisine['image'] = $row['cuisinepic'];
		$cuisine['description'] = $row['cuisinedescription'];
		$cuisine['price'] = $row['price'];
		$cuisine['serve'] = $row['servingdetail'];
		$cuisine['type'] = $row['type'];
		$userid = $row['userid'];

		$queryK = "SELECT * FROM user WHERE userid = '$userid'";	
		$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
		$rowK = mysqli_fetch_assoc($resultK);
		$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
		$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";

		if ($row['cuisinepic']!="") {
			$image_name = explode("/",$row['cuisinepic']);
			$image_name = array_pop($image_name);
			$cuisine['image'] = S3URL.$image_name;
		}
		else
		{
			$cuisine['image'] = "images/food.png";
		}

		array_push($data['cuisines'], $cuisine);
	}	
	print_r(json_encode($data));
}

/*
By: Jyoti Vishwakarma
Description: Party Meals list
*/
if($_GET['service_name'] == 'partymeals')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$keyword = $json['keyword'];
	$categories = $json['categories'];
	$sortbycost = $json['sortbycost'];
	$type = $json['type'];
	$limit = "";
	if(isset($json['limit'])){
		$limit = $json['limit'];
	}
	$kitchenid = $json['kitchen_id'];
	if($kitchenid != ""){
		$restocondition = "userid = '$kitchenid' AND";
	}else{
		$restocondition = "";
	}

	$orderby = "ORDER BY cuisineid DESC";
	$condition = "";
	if ($keyword!="") {
		$condition .= " AND (cuisinename LIKE '%$keyword%' OR tags LIKE '%$keyword%')";
	}
	if($categories!="")
	{
		$condition .= " AND homepagecatid IN ($categories)";
	}
	if($type!="")
	{
		$condition .= " AND type LIKE '$type%'";
	}

	if($limit!="")
	{
		$d = $limit * 30;
		$limitss = "limit  $d, 30";
	}


	if($sortbycost!="")
	{
		$orderby = "ORDER BY price $sortbycost";
	}

	$query = "SELECT * FROM cuisine WHERE $restocondition isActive = 'Y' AND status='1' AND isCorporateMeal='3' $condition $orderby $limitss";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	
	$data = array();
	$data['cuisines'] = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$cuisine = array();
		$id = $row['cuisineid'];
		$cuisine['id'] = $row['cuisineid'];
		$cuisine['name'] = $row['cuisinename'];
		$cuisine['description'] = $row['cuisinedescription'];
		if($row['cuisinedescription'] != ""){
			$desc1 = strip_tags($row['cuisinedescription']);
			if(strlen($desc1) > 99){
				$cuisine['description1'] = substr($desc1,0,100)."...";
			}else{
				$cuisine['description1'] = substr($desc1,0,100);
			}
		}
		$cuisine['price'] = $row['price'];
		$cuisine['leadtime'] = $row['leadtime'];
		$cuisine['minorderquantity'] = $row['minorderquantity'];
		$cuisine['type'] = $row['type'];
		$userid = $row['userid'];

		$queryK = "SELECT * FROM user WHERE userid = '$userid'";	
		$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
		$rowK = mysqli_fetch_assoc($resultK);
		$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
		$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";

		if ($row['cuisinepic']!="") {
			$image_name = explode("/",$row['cuisinepic']);
			$image_name = array_pop($image_name);
			$cuisine['image'] = S3URL.$image_name;
		}
		else
		{
			$cuisine['image'] = "images/food.png";
		}
		array_push($data['cuisines'], $cuisine);
	}	
	print_r(json_encode($data));
}
/*
By: Dilip Kumawat
Created at: 19 Nov, 2019
Updated at: 19 Nov, 2019
Description: kitchen_page
*/

if($_GET['service_name'] == 'kitchen_page')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$kitchenid = $json['kitchenid'];
	$keyword = $json['keyword'];
	$categories = $json['categories'];
	$sortbycost = $json['sortbycost'];
	$type = $json['type'];
	$mealtype = $json['mealtype'];

	$queryK = "SELECT * FROM user WHERE userid = '$kitchenid' ORDER BY userid DESC";	
	$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
	$rowK = mysqli_fetch_assoc($resultK);
	$data = array();
	$data['kitchen'] = array();
	$data['categories'] = array();
	$kitchens = array();
	$id = $rowK['userid'];
	$kitchens['id'] = $rowK['userid'];
	$kitchens['name'] = $rowK['name'];
	$kitchens['tagline'] = $rowK['tagline'];
	$kitchens['type'] = $rowK['kitchen_type'];
	$kitchens['delivery_time'] = $rowK['delivery_time'];
	
	if ($rowK['profilepic']!="") {
		$image_name = explode("/",$rowK['profilepic']);
		$image_name = array_pop($image_name);
		$kitchens['image'] = S3URL.$image_name;
	}
	else
	{
		$kitchens['image'] = "https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png";
	}
	
	if($rowK['category'] !=""){
		$category = $rowK['category'];
	}else{
		$category='';
	}

	$corporateid = '';
	if(isset($_SESSION['corporateid'])){
		$corporateid = $_SESSION['corporateid'];

	}

	$querySq = "SELECT * FROM `kitchen_cuisinetype_sequence` WHERE kitchen_id = '$id' ORDER BY sequence ASC";	
	$resultsq = mysqli_query($conn,$querySq)or die(mysqli_error($conn));
	$seq = array();
	while($rowSq = mysqli_fetch_assoc($resultsq)) {
		$seq[count($seq)] = $rowSq['cuisinetypeid'];
	}
	$catseq = implode(",",$seq);


	$queryR = "SELECT count(id) as count, rating FROM chefrating WHERE chefId = '$id' GROUP BY rating";	
	$resultR = mysqli_query($conn,$queryR)or die(mysqli_error($conn));
	$rating1 = 0;
	$rating2 = 0;
	$rating3 = 0;
	$rating4 = 0;
	$rating5 = 0;
	while ($rowR = mysqli_fetch_assoc($resultR)) {
		$rating = $rowR['rating'];
		if ($rating==1) {
			$rating1 = (int)$rowR['count'];
		}
		if ($rating==2) {
			$rating2 = (int)$rowR['count'];
		}
		if ($rating==3) {
			$rating3 = (int)$rowR['count'];
		}
		if ($rating==4) {
			$rating4 = (int)$rowR['count'];
		}
		if ($rating==5) {
			$rating5 = (int)$rowR['count'];
		}
	}
	$ratingTotal = $rating5+$rating4+$rating3+$rating2+$rating1;
	if ($ratingTotal>0) {
		$rating = (5*$rating5 + 4*$rating4 + 3*$rating3 + 2*$rating2 + 1*$rating1) / ($rating5+$rating4+$rating3+$rating2+$rating1);
	}
	else
	{
		$rating = 0;
	}
	$kitchens['ratingcount'] = mysqli_num_rows($resultR);		
	$kitchens['rating'] = round($rating);		
	array_push($data['kitchen'], $kitchens);

	$kitchens['category'] = array();
	if($mealtype == "corporate_meals"){
		$mtcondition = "AND isCorporateMeal='0' AND isEventMeal='0'";
		$pagetype ='2';
	}else if($mealtype == "team_meals"){
		$mtcondition = "AND isCorporateMeal='1' AND isEventMeal='0'";
		$pagetype ='3';
	}else if($mealtype == "event_meals"){
		$mtcondition = "AND isCorporateMeal='0' AND isEventMeal='1'";
		$pagetype ='4';
	}else if($mealtype == "party_meals"){
		$mtcondition = "AND isCorporateMeal='3' AND isEventMeal='0'";
		$pagetype ='5';
	}else if($mealtype == "cafeteria"){
		$mtcondition = "AND isCorporateMeal='4' AND isEventMeal='0'";
		$pagetype ='6';
	}else if($mealtype == "society"){
		$mtcondition = "AND isCorporateMeal='5' AND isEventMeal='0'";
		$pagetype ='7';
	}else if($mealtype == "kitty"){
		$mtcondition = "AND isCorporateMeal='6' AND isEventMeal='0'";
		$pagetype ='8';
	}

	
	
	
	$corporate_condition1 = "";
	if(isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] =='1'){
		$corporate_id = $_SESSION['corporateid'];
		$corporate_condition1 =  "page_type = '$pagetype' AND FIND_IN_SET($corporate_id, corporateid) AND FIND_IN_SET($kitchenid, kitchensid)";
	}else if($mealtype == "society"){
		$corporate_condition1 =  "page_type = '$pagetype'";
	}else if($mealtype == "kitty"){
		$corporate_condition1 =  "page_type = '$pagetype'";
	}else{
		$corporate_condition1 ="cuisinetypeid IN ($category) ORDER BY FIELD(cuisinetypeid, $catseq)";
	}

	if($category != "" and $catseq != ""){
		$queryC = "SELECT * FROM cuisinetypemaster WHERE $corporate_condition1";
		$resultC = mysqli_query($conn,$queryC)or die(mysqli_error($conn));
	
	$data['cuisines'] = array();
		while($rowC = mysqli_fetch_assoc($resultC)) {
			
			$cattemp = array();
			$cattemp['categoryid'] = $rowC['cuisinetypeid'];
			$cattemp['categoryname'] = $rowC['cuisinetype'];

			array_push($data['categories'], $cattemp);


			$catgorytemp = $rowC['cuisinetypeid'];
			
			// echo "<br>";
			$orderby = "ORDER BY cuisineid DESC";
			$condition = "";
			if ($keyword!="") {
				$condition .= "AND ( cuisinename LIKE '%$keyword%' OR tags LIKE '%$keyword%' )";
			}
			if($type!="")
			{
				$condition .= "AND type LIKE '$type%'";
			}
			if($sortbycost!="")
			{
				$orderby = "ORDER BY price $sortbycost";
			}
			$corporate_condition = "";
			if(isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] =='1'){
				$corporate_id = $_SESSION['corporateid'];
				$corporate_condition =  "AND homepagecatid = '$catgorytemp' AND FIND_IN_SET($corporate_id, invitecode)";
				if($mealtype == "corporate_meals"){
					$corporate_condition =  "AND cuisinetypeId = '$catgorytemp' AND FIND_IN_SET($corporate_id, invitecode)";
				}
			}else if($mealtype == "society"){
				$corporate_condition =  "AND homepagecatid = '$catgorytemp'";
			}else if($mealtype == "kitty"){
				$corporate_condition =  "AND homepagecatid = '$catgorytemp'";
			}else{
				$corporate_condition =  "AND cuisinetypeId = '$catgorytemp'";
			}
			$query1 = "SELECT * FROM cuisine WHERE isActive = 'Y' AND status='1' AND userid='$kitchenid'  $corporate_condition $mtcondition $condition $orderby";	 
			$result1 = mysqli_query($conn,$query1)or die(mysqli_error($conn));
			$count = mysqli_num_rows($result1);
			if ($count==0) {
				continue;
			}	
			$cattemp['cuisines'] = array();
			while ($row = mysqli_fetch_assoc($result1)) {
				$cuisine = array();
				$id = $row['cuisineid'];
				$cuisine['id'] = $row['cuisineid'];
				$cuisine['name'] = $row['cuisinename'];
				// $cuisine['description'] = $row['cuisinedescription'];
				$cuisine['description'] = $row['cuisinedescription'];
				if($row['cuisinedescription'] != ""){
					$desc1 = strip_tags($row['cuisinedescription']);
					if(strlen($desc1) > 99){
						$cuisine['description'] = substr($desc1,0,100)."...";
					}else{
						$cuisine['description'] = substr($desc1,0,100);
					}
					
				}
				$cuisine['price'] = $row['price'];
				$cuisine['serves'] = $row['servingdetail'];
				$cuisine['type'] = $row['type'];
				$cuisine['minorderquantity'] = $row['minorderquantity'];
				$cuisine['leadtime'] = $row['leadtime'];

				$userid = $row['userid'];

				$queryK = "SELECT * FROM user WHERE userid = '$userid'";	
				$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
				$rowK = mysqli_fetch_assoc($resultK);
				$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
				$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";
				$cuisine['kitchen_workingstart'] = isset($rowK['workingstart']) ? $rowK['workingstart'] : "";
				$cuisine['kitchen_workingend'] = isset($rowK['workingend']) ? $rowK['workingend'] : "";

				if ($row['cuisinepic']!="") {
					$image_name = explode("/",$row['cuisinepic']);
					$image_name = array_pop($image_name);
					$cuisine['image'] = S3URL.$image_name;
				}
				else
				{
					$cuisine['image'] = "https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png";
				}
				
				$cuisine['isCustomisable']='0';
				$query2 = "SELECT * FROM corporate_cuisine_sections WHERE cuisine_id='$id'";
				$result2 = mysqli_query($conn,$query2) or die(mysqli_error($conn));
				if(mysqli_num_rows($result2) > 0 ){
					$cuisine['isCustomisable']='1';
				}

				$cuisine['offer']='';
				$cuisine['discountprice'] = '';
				if($row['isCorporateMeal'] =='0' && $row['isEventMeal'] == '0'){
					if($corporateid != ''){
						$offer = 0;
						$queryO = "SELECT offer FROM corporateoffermapping WHERE status='1' AND restaurantid = '$userid' AND corporateid='$corporateid'";	
						$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
						$rowO = mysqli_fetch_assoc($resultO);
						$cuisine['offer'] = isset($rowO['offer']) ? $rowO['offer'] : "";
						$offer = $rowO['offer'];
						$discountprice = round($row['price'] - ( ( $row['price'] * $offer ) / 100 ));
						$cuisine['discountprice'] = $discountprice;

					}else{
						$offer = 0;
						$queryO = "SELECT offer FROM offeroncuisine WHERE status='1' AND kitchen_id = '$userid'";	
						$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
						$rowO = mysqli_fetch_assoc($resultO);
						$cuisine['offer'] = isset($rowO['offer']) ? $rowO['offer'] : "";
						$offer = $rowO['offer'];
						$discountprice = round($row['price'] - ( ( $row['price'] * $offer ) / 100 ));
						$cuisine['discountprice'] = $discountprice;
					}
				}

				// if($corporateid != ''){
				// 	$queryO = "SELECT offer FROM corporateoffermapping WHERE restaurantid = '$userid' AND corporateid='$corporateid'";	
				// 	$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
				// 	$rowO = mysqli_fetch_assoc($resultO);
				// 	$cuisine['offer'] = isset($rowO['offer']) ? $rowO['offer'] : "";
				// }else{
				// 	$cuisine['offer']='';
				// }
				
				array_push($cattemp['cuisines'], $cuisine);
			}
			array_push($data['cuisines'], $cattemp);
		}	
	}
	print_r(json_encode($data));
}

/*
By: Jyoti Vishwakarma
Description: Party kitchen page
*/

if($_GET['service_name'] == 'kitchen_page_party')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$kitchenid = $json['kitchenid'];
	$keyword = $json['keyword'];
	$categories = $json['categories'];
	$sortbycost = $json['sortbycost'];
	$type = $json['type'];
	$mealtype = $json['mealtype'];

	$queryK = "SELECT * FROM user WHERE userid = '$kitchenid' ORDER BY userid DESC";	
	$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
	$rowK = mysqli_fetch_assoc($resultK);
	$data = array();
	$data['kitchen'] = array();
	$data['categories'] = array();
	$kitchens = array();
	$id = $rowK['userid'];
	$kitchens['id'] = $rowK['userid'];
	$kitchens['name'] = $rowK['name'];
	$kitchens['tagline'] = $rowK['tagline'];
	$kitchens['type'] = $rowK['kitchen_type'];
	$kitchens['delivery_time'] = $rowK['delivery_time'];
	if ($rowK['profilepic']!="") {
		$image_name = explode("/",$rowK['profilepic']);
		$image_name = array_pop($image_name);
		$kitchens['image'] = S3URL.$image_name;
	}
	else
	{
		$kitchens['image'] = "https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png";
	}
	
	if($rowK['category'] !=""){
		$category = $rowK['category'];
	}else{
		$category='';
	}

	$corporateid = '';
	if(isset($_SESSION['corporateid'])){
		$corporateid = $_SESSION['corporateid'];

	}

	$querySq = "SELECT * FROM `kitchen_cuisinetype_sequence` WHERE kitchen_id = '$id' ORDER BY sequence ASC";	
	$resultsq = mysqli_query($conn,$querySq)or die(mysqli_error($conn));
	$seq = array();
	while($rowSq = mysqli_fetch_assoc($resultsq)) {
		$seq[count($seq)] = $rowSq['cuisinetypeid'];
	}
	$catseq = implode(",",$seq);


	$queryR = "SELECT count(id) as count, rating FROM chefrating WHERE chefId = '$id' GROUP BY rating";	
	$resultR = mysqli_query($conn,$queryR)or die(mysqli_error($conn));
	$rating1 = 0;
	$rating2 = 0;
	$rating3 = 0;
	$rating4 = 0;
	$rating5 = 0;
	while ($rowR = mysqli_fetch_assoc($resultR)) {
		$rating = $rowR['rating'];
		if ($rating==1) {
			$rating1 = (int)$rowR['count'];
		}
		if ($rating==2) {
			$rating2 = (int)$rowR['count'];
		}
		if ($rating==3) {
			$rating3 = (int)$rowR['count'];
		}
		if ($rating==4) {
			$rating4 = (int)$rowR['count'];
		}
		if ($rating==5) {
			$rating5 = (int)$rowR['count'];
		}
	}
	$ratingTotal = $rating5+$rating4+$rating3+$rating2+$rating1;
	if ($ratingTotal>0) {
		$rating = (5*$rating5 + 4*$rating4 + 3*$rating3 + 2*$rating2 + 1*$rating1) / ($rating5+$rating4+$rating3+$rating2+$rating1);
	}
	else
	{
		$rating = 0;
	}
	$kitchens['ratingcount'] = mysqli_num_rows($resultR);		
	$kitchens['rating'] = round($rating);		
	array_push($data['kitchen'], $kitchens);

	$kitchens['category'] = array();

	if($category != "" and $catseq != ""){
		$queryC = "SELECT * FROM `cuisinetypemaster` WHERE page_type = '5'";
		$resultC = mysqli_query($conn,$queryC)or die(mysqli_error($conn));
	
	$data['cuisines'] = array();
		while($rowC = mysqli_fetch_assoc($resultC)) {
			
			$cattemp = array();
			$cattemp['categoryid'] = $rowC['cuisinetypeid'];
			$cattemp['categoryname'] = $rowC['cuisinetype'];

			array_push($data['categories'], $cattemp);


			$catgorytemp = $rowC['cuisinetypeid'];
			
			// echo "<br>";
			$orderby = "ORDER BY cuisineid DESC";
			$condition = "";
			if ($keyword!="") {
				$condition .= "AND (cuisinename LIKE '%$keyword%' OR tags LIKE '%$keyword%')";
			}
			if($type!="")
			{
				$condition .= "AND type LIKE '$type%'";
			}
			if($sortbycost!="")
			{
				$orderby = "ORDER BY price $sortbycost";
			}
			

			$query1 = "SELECT * FROM cuisine WHERE isActive = 'Y' AND userid='$kitchenid' AND isCorporateMeal='3' AND isEventMeal='0' AND homepagecatid = '$catgorytemp' $condition $orderby";	 
			$result1 = mysqli_query($conn,$query1)or die(mysqli_error($conn));
			$count = mysqli_num_rows($result1);
			if ($count==0) {
				continue;
			}	
			$cattemp['cuisines'] = array();
			while ($row = mysqli_fetch_assoc($result1)) {
				$cuisine = array();
				$id = $row['cuisineid'];
				$cuisine['id'] = $row['cuisineid'];
				$cuisine['name'] = $row['cuisinename'];
				// $cuisine['description'] = $row['cuisinedescription'];
				$cuisine['description'] = $row['cuisinedescription'];
				if($row['cuisinedescription'] != ""){
					$desc1 = strip_tags($row['cuisinedescription']);
					if(strlen($desc1) > 99){
						$cuisine['description'] = substr($desc1,0,100)."...";
					}else{
						$cuisine['description'] = substr($desc1,0,100);
					}
					
					
				}
				$cuisine['price'] = $row['price'];
				$cuisine['serves'] = $row['servingdetail'];
				$cuisine['type'] = $row['type'];
				$cuisine['minorderquantity'] = $row['minorderquantity'];
				$cuisine['leadtime'] = $row['leadtime'];

				$userid = $row['userid'];

				$queryK = "SELECT * FROM user WHERE userid = '$userid'";	
				$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
				$rowK = mysqli_fetch_assoc($resultK);
				$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
				$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";

				if ($row['cuisinepic']!="") {
					$image_name = explode("/",$row['cuisinepic']);
					$image_name = array_pop($image_name);
					$cuisine['image'] = S3URL.$image_name;
				}
				else
				{
					$cuisine['image'] = "https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png";
				}
				
				$cuisine['isCustomisable']='0';
				$query2 = "SELECT * FROM corporate_cuisine_sections WHERE cuisine_id='$id'";
				$result2 = mysqli_query($conn,$query2) or die(mysqli_error($conn));
				if(mysqli_num_rows($result2) > 0 ){
					$cuisine['isCustomisable']='1';
				}


				if($corporateid != ''){
					$queryO = "SELECT offer FROM corporateoffermapping WHERE restaurantid = '$userid' AND corporateid='$corporateid'";	
					$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
					$rowO = mysqli_fetch_assoc($resultO);
					$cuisine['offer'] = isset($rowO['offer']) ? $rowO['offer'] : "";
				}else{
					$cuisine['offer']='';
				}
				
				array_push($cattemp['cuisines'], $cuisine);
			}
			array_push($data['cuisines'], $cattemp);
		}	
	}
	print_r(json_encode($data));
}

/*
By: Jyoti Vishwakarma
Description: Fetch kitchen detail 
*/
if($_GET['service_name'] == 'kitchen_details')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);

	$kitchenid = $json['kitchenid'];

	$queryK = "SELECT * FROM user WHERE userid = '$kitchenid' ORDER BY userid DESC";	
	$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
	$rowK = mysqli_fetch_assoc($resultK);
	$data = array();
	$data['kitchen'] = array();
	$kitchens = array();
	$id = $rowK['userid'];
	$kitchens['id'] = $rowK['userid'];
	$kitchens['name'] = $rowK['name'];
	$kitchens['tagline'] = $rowK['tagline'];
	$kitchens['type'] = $rowK['kitchen_type'];
	$kitchens['delivery_time'] = $rowK['delivery_time'];
	if ($rowK['profilepic']!="") {
		$image_name = explode("/",$rowK['profilepic']);
		$image_name = array_pop($image_name);
		$kitchens['image'] = S3URL.$image_name;
	}
	else
	{
		$kitchens['image'] = "https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png";
	}
	
	array_push($data['kitchen'], $kitchens);



	print_r(json_encode($data));
}

/*
By: Dilip Kumawat
Created at: 19 Nov, 2019
Updated at: 19 Nov, 2019
Description: search
*/

if($_GET['service_name'] == 'search')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$keyword = $json['keyword'];
	$sort_by_cost = $json['sort_by_cost'];
	$mealtype = $json['ctype'];	
	$orderby = "ORDER BY cuisineid DESC";
	$condition = "";
	if ($sort_by_cost!="") {
		$orderby = "ORDER BY price $sort_by_cost";
	}

	$corporateid = '';
	if(isset($_SESSION['corporateid'])){
		$corporateid = $_SESSION['corporateid'];

	}
	$mtcondition = "";
	if($mealtype == "corporate_meals"){
		$mtcondition = "AND isCorporateMeal='0' AND isEventMeal='0'";
	}else if($mealtype == "team_meals"){
		$mtcondition = "AND isCorporateMeal='1' AND isEventMeal='0'";
	}else if($mealtype == "event_meals"){
		$mtcondition = "AND isCorporateMeal='0' AND isEventMeal='1'";
	}else if($mealtype == "party_meals"){
		$mtcondition = "AND isCorporateMeal='3' AND isEventMeal='0'";
	}else if($mealtype == "cafeteria"){
		$mtcondition = "AND isCorporateMeal='4' AND isEventMeal='0'";
	}else if($mealtype == "society"){
		$mtcondition = "AND isCorporateMeal='5' AND isEventMeal='0'";
	}else if($mealtype == "kitty"){
		$mtcondition = "AND isCorporateMeal='6' AND isEventMeal='0'";
	}

	$corporate_condition = "";
	if(isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] =='1'){
		$corporate_id = $_SESSION['corporateid'];
		$corporate_condition =  "AND FIND_IN_SET($corporate_id, cuisine.invitecode)";
	}
	$query = "SELECT DISTINCT cuisine.*,user.userid AS kitchenid, user.name AS Kitchenname FROM `cuisinetypemaster`,`cuisine` LEFT JOIN `user` ON user.userid = cuisine.userid WHERE cuisine.status='1' AND cuisine.isActive = 'Y' $mtcondition $corporate_condition  AND  (cuisine.cuisinename LIKE '%$keyword%' OR user.name LIKE '%$keyword%' OR (cuisinetypemaster.cuisinetype LIKE '%$keyword%' AND  (cuisinetypemaster.cuisinetypeid = cuisine.cuisinetypeId OR cuisine.homepagecatid =cuisinetypemaster.cuisinetypeid )) OR cuisine.tags LIKE '%$keyword%') $orderby";
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	$data = array();
	$data['cuisines'] = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$cuisine = array();
		$id = $row['cuisineid'];
		$cuisine['id'] = $row['cuisineid'];
		$cuisine['name'] = $row['cuisinename'];
		$cuisine['description'] = $row['cuisinedescription'];
		if($row['cuisinedescription'] != ""){
			$desc1 = strip_tags($row['cuisinedescription']);
			if(strlen($desc1) > 100){
				$cuisine['description'] = substr($desc1,0,100)."...";
			}else{
				$cuisine['description'] = substr($desc1,0,100);
			}
			
			
		}
		$cuisine['price'] = $row['price'];
		$cuisine['serves'] = $row['servingdetail'];
		$cuisine['type'] = $row['type'];
		$cuisine['leadtime'] = $row['leadtime'];
		$cuisine['minorderquantity'] = $row['minorderquantity'];
		$userid = $row['userid'];

		$queryK = "SELECT * FROM user WHERE userid = '$userid'";	
		$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
		$rowK = mysqli_fetch_assoc($resultK);
		$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
		$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";
		$cuisine['kitchen_workingstart'] = isset($rowK['workingstart']) ? $rowK['workingstart'] : "";
		$cuisine['kitchen_workingend'] = isset($rowK['workingend']) ? $rowK['workingend'] : "";

		if ($row['cuisinepic']!="") {
			$image_name = explode("/",$row['cuisinepic']);
			$image_name = array_pop($image_name);
			$cuisine['image'] = S3URL.$image_name;
		}
		else
		{
			$cuisine['image'] = "images/food.png";
		}

	

		if($corporateid != '' && $userid != ''){
			$offer = 0;
			$queryO = "SELECT offer FROM corporateoffermapping WHERE status='1' AND restaurantid = '$userid' AND corporateid='$corporateid'";	
			$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
			$rowO = mysqli_fetch_assoc($resultO);
			$cuisine['offer'] = isset($rowO['offer']) ? $rowO['offer'] : "";
			$offer = $rowO['offer'];
			$discountprice = round($row['price'] - ( ( $row['price'] * $offer ) / 100 ));
			$cuisine['discountprice'] = $discountprice;

		}else{
			$offer = 0;
			$queryO = "SELECT offer FROM offeroncuisine WHERE status='1' AND kitchen_id = '$userid'";	
			$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
			$rowO = mysqli_fetch_assoc($resultO);
			$cuisine['offer'] = isset($rowO['offer']) ? $rowO['offer'] : "";
			$offer = $rowO['offer'];
			$discountprice = round($row['price'] - ( ( $row['price'] * $offer ) / 100 ));
			$cuisine['discountprice'] = $discountprice;
		}

		$cuisine['isCustomisable']='0';
		$query2 = "SELECT * FROM corporate_cuisine_sections WHERE cuisine_id='$id'";
		$result2 = mysqli_query($conn,$query2) or die(mysqli_error($conn));
		if(mysqli_num_rows($result2) > 0 ){
			$cuisine['isCustomisable']='1';
		}

		array_push($data['cuisines'], $cuisine);
	}	
	print_r(json_encode($data));
}

/*
By:Jyoti Vishwakarma
Description: Add to cart normal cuisine and customize cuisine
*/
if($_GET['service_name'] == 'addtocart')
{
	$date = date('Y-m-d H:i:s');
	$data = file_get_contents('php://input');
	$json = json_decode($data, true);
	$id= $json['id'];
	$selectedaddons = $json['selectedaddons'];
	$count = 1;
	$querySt = "SELECT * FROM cuisine WHERE status='1'";
	$resultSt = mysqli_query($conn,$querySt)or die(mysqli_error($conn));
	if(mysqli_num_rows($resultSt) > 0){
		if(isset($_SESSION['userid']))
		{
			$userid = $_SESSION['userid'];

			$query = "SELECT * FROM cart WHERE product_id='$id' AND userid='$userid'";
			$result = mysqli_query($conn,$query)or die(mysqli_error($conn));

			$catcount = mysqli_num_rows($result);
			$status = array();
			if($catcount>0){
				$status['status']='exist';
				if($selectedaddons != ""){
					$query = "UPDATE cart SET selectedaddons = '$selectedaddons' WHERE product_id='$id' AND userid='$userid'";	
					$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
					$status['status']='success';
				}
				

			} else {
				$data = array(
					'product_id' => $id,
					'quantity' => $count,
					'userid' => $_SESSION['userid'],
				);
				$queryadd = "INSERT INTO cart SET userid = '$userid',product_id = '$id', quantity = '$count',selectedaddons='$selectedaddons',created_at=NOW()";
				$resultadd = mysqli_query($conn,$queryadd)or die(mysqli_error($conn));
				if($resultadd)
				{
					$status["status"]="success";
				}
				else{
					$status["status"]="failed";
				}
			}
			print_r(json_encode($status));
		}
		else
		{
			$status = array();
			$name = "cartcookie".$id;
			$countname = "cookiecount".$id;
			$countselectedaddons = "cookieselectedaddons".$id;
			if(isset($_COOKIE[$name]))
			{
				$status['status']="exist";
				if($selectedaddons != ""){
					$countselectedaddons = "cookieselectedaddons".$id;
					setcookie($countselectedaddons, $selectedaddons, time() + (86400 * 30), "/"); // 86400 = 1 day
					$status['status']="success";
				}
				
			}
			else
			{
				$status['status']="success";
				setcookie($name, $id, time() + (86400 * 30), "/"); // 86400 = 1 day
				setcookie($countname, $count, time() + (86400 * 30), "/"); // 86400 = 1 day
				setcookie($countselectedaddons, $selectedaddons, time() + (86400 * 30), "/"); // 86400 = 1 day
			}
			print_r(json_encode($status));
		}
	}	
}

/*
By:Jyoti Vishwakarma
Description: Add to cart all type of meals
*/
if($_GET['service_name'] == 'addtocartCorporate')
{
	$date = date('Y-m-d H:i:s');
	$data = file_get_contents('php://input');
	$json = json_decode($data, true);
	$id= $json['id'];
	$date = $json['date'];
	$time = $json['time'];
	$qty = $json['qty'];
	$special_instruction = $json['special_instruction'];
	$selectedaddons = $json['selectedaddons'];
	$querySt = "SELECT * FROM cuisine WHERE status='1'";
	$resultSt = mysqli_query($conn,$querySt)or die(mysqli_error($conn));
	if(mysqli_num_rows($resultSt) > 0){
		if(isset($_SESSION['userid']))
		{
			$userid = $_SESSION['userid'];

			if(isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] == '1'){
				$queryCheck = "SELECT * FROM cart WHERE userid='$userid'";
				$resultcheck = mysqli_query($conn,$queryCheck)or die(mysqli_error($conn));
				$checkcount = mysqli_num_rows($resultcheck);
				if($checkcount > 0){
					$querydel = "DELETE FROM cart WHERE userid='$userid'";
					$resultdel = mysqli_query($conn,$querydel)or die(mysqli_error($conn));
				}
			}
			
			$query = "SELECT * FROM cart WHERE product_id='$id' AND userid='$userid'";
			$result = mysqli_query($conn,$query)or die(mysqli_error($conn));

			$catcount = mysqli_num_rows($result);
			$status = array();
			if($catcount>0){
				$status['status']='exist';
			} else {
				$data = array(
					'product_id' => $id,
					'quantity' => $qty,
					'userid' => $_SESSION['userid'],
				);
				$queryadd = "INSERT INTO cart SET userid = '$userid',product_id = '$id', quantity = '$qty',date='$date',time='$time',special_instruction='$special_instruction',selectedaddons='$selectedaddons',created_at=NOW()";
				$resultadd = mysqli_query($conn,$queryadd)or die(mysqli_error($conn));
				if($resultadd)
				{
					$status["status"]="success";
				}
				else{
					$status["status"]="failed";
				}
			}
			print_r(json_encode($status));
		}
		else
		{
			$status = array();
			$name = "cartcookie".$id;
			$countname = "cookiecount".$id;
			$countdate = "cookiedate".$id;
			$counttime = "cookietime".$id;
			$countspecialinstruction = "cookiespecialinstruction".$id;
			$countselectedaddons = "cookieselectedaddons".$id;

			if(isset($_COOKIE[$name]))
			{
				$status['status']="exist";
			}
			else
			{
				$status['status']="success";
				setcookie($name, $id, time() + (86400 * 30), "/"); // 86400 = 1 day
				setcookie($countname, $qty, time() + (86400 * 30), "/"); // 86400 = 1 day
				setcookie($countdate, $date, time() + (86400 * 30), "/"); // 86400 = 1 day
				setcookie($counttime, $time, time() + (86400 * 30), "/"); // 86400 = 1 day
				setcookie($countspecialinstruction, $special_instruction, time() + (86400 * 30), "/"); // 86400 = 1 day
				setcookie($countselectedaddons, $selectedaddons, time() + (86400 * 30), "/"); // 86400 = 1 day
			}
			print_r(json_encode($status));
		}
	}
}


/*
By: Dilip Kumawat
Created at: 21 Nov, 2019
Updated at: 21 Nov, 2019
Description: Cart count in header and add cookies data to database on login
*/
if($_GET['service_name'] == 'showcartdata')
{	
	$totalQuantity = 0;
	$countdata = 0;
	if (isset($_SESSION['userid'])) {
		$userid = $_SESSION['userid'];
		foreach ($_COOKIE as $key=>$val) {
			if (substr($key, 0, 10) == "cartcookie") {
				$totalQuantity++;
				$countdata++;
				$quantity =  $_COOKIE['cookiecount'.$val];
				$date = $_COOKIE['cookiedate'.$val]; 
				$time = $_COOKIE['cookietime'.$val]; 
				$special_instruction = $_COOKIE['cookiespecialinstruction'.$val]; 
				$selectedaddons = $_COOKIE['cookieselectedaddons'.$val]; 

				$productid = $val;

				$querySt = "SELECT * FROM cuisine WHERE cuisineid='$productid' and status = '1'";
				$resultSt = mysqli_query($conn,$querySt)or die(mysqli_error($conn));
				if(mysqli_num_rows($resultSt) > 0){
					$query = "SELECT * FROM cart WHERE product_id='$productid' AND userid='$userid'";
					$result = mysqli_query($conn,$query)or die(mysqli_error($conn));

					$catcount = mysqli_num_rows($result);
					$status = array();
					if($catcount>0){
						$status['status']='exist';
						setcookie('cartcookie'.$val, $val,  time() - 3600, "/");
						setcookie('cookiecount'.$val, $val, time() - 3600, "/");
						setcookie('cookiedate'.$val, $val, time() - 3600, "/");
						setcookie('cookietime'.$val, $val, time() - 3600, "/");
						setcookie('cookiespecialinstruction'.$val, $val, time() - 3600, "/");
						setcookie('cookieselectedaddons'.$val, $val, time() - 3600, "/");
					} else {
						$data = array(
							'product_id' => $productid,
							'quantity' => $quantity,
							'userid' => $userid,
						);

						$queryadd = "INSERT INTO cart SET userid = '$userid',product_id = '$productid', quantity = '$quantity',date='$date',time='$time',special_instruction='$special_instruction',selectedaddons='$selectedaddons',created_at=NOW()";
						// $queryadd = "INSERT INTO cart SET userid = '$userid',product_id = '$productid', quantity = '$quantity',created_at=NOW()";
						$resultadd = mysqli_query($conn,$queryadd)or die(mysqli_error($conn));
						setcookie('cartcookie'.$val, $val,  time() - 3600, "/");
						setcookie('cookiecount'.$val, $val, time() - 3600, "/");
						setcookie('cookiedate'.$val, $val, time() - 3600, "/");
						setcookie('cookietime'.$val, $val, time() - 3600, "/");
						setcookie('cookiespecialinstruction'.$val, $val, time() - 3600, "/");
						setcookie('cookieselectedaddons'.$val, $val, time() - 3600, "/");

					}
				}else{
					$queryadd = "DELETE FROM cart WHERE product_id='$productid' AND userid='$userid'";
					$resultadd = mysqli_query($conn,$queryadd)or die(mysqli_error($conn));
				}
				
			}
		}
		$query = "SELECT * FROM cart WHERE userid='$userid'";
		$result1 = mysqli_query($conn,$query)or die(mysqli_error($conn));
		while ($row = mysqli_fetch_assoc($result1)) {
			$productid = $row['product_id']; 
			$querySt = "SELECT * FROM cuisine WHERE cuisineid='$productid' and status = '0'";
			$resultSt = mysqli_query($conn,$querySt)or die(mysqli_error($conn));
			if(mysqli_num_rows($resultSt) > 0){
				$queryadd = "DELETE FROM cart WHERE product_id='$productid' AND userid='$userid'";
				$resultadd = mysqli_query($conn,$queryadd)or die(mysqli_error($conn));
				setcookie('cartcookie'.$val, $val,  time() - 3600, "/");
				setcookie('cookiecount'.$val, $val, time() - 3600, "/");
				setcookie('cookiedate'.$val, $val, time() - 3600, "/");
				setcookie('cookietime'.$val, $val, time() - 3600, "/");
				setcookie('cookiespecialinstruction'.$val, $val, time() - 3600, "/");
				setcookie('cookieselectedaddons'.$val, $val, time() - 3600, "/");
			}
		}
		
		$querycount = "SELECT * FROM cart WHERE userid='$userid'";
		$resultcount = mysqli_query($conn,$querycount)or die(mysqli_error($conn));
		$cartcount = mysqli_num_rows($resultcount);

		$product = array();
		$product['count'] = array();
		$product['count'] = $cartcount;
	}
	else
	{
		foreach ($_COOKIE as $key=>$val) {
			if (substr($key, 0, 10) == "cartcookie") {
				$countdata++;
				$productid = $val;
				$querySt = "SELECT * FROM cuisine WHERE cuisineid='$productid' and status = '0'";
				$resultSt = mysqli_query($conn,$querySt)or die(mysqli_error($conn));
				if(mysqli_num_rows($resultSt) > 0){
					$queryadd = "DELETE FROM cart WHERE product_id='$productid' AND userid='$userid'";
					$resultadd = mysqli_query($conn,$queryadd)or die(mysqli_error($conn));
					setcookie('cartcookie'.$val, $val,  time() - 3600, "/");
					setcookie('cookiecount'.$val, $val, time() - 3600, "/");
				}

			}
		}
		$product = array();
		$product['count'] = array();
		$product['count'] = $countdata;
	}
	print_r(json_encode($product));
}

/*
By:Jyoti Vishwakarma
Description: Fetch user Wallet amount
*/
if($_GET['service_name'] == 'walletAmount')
{	
	$userid= $_SESSION['userid'];
    $query = "SELECT * FROM walletmaster WHERE userid='$userid' ";
    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
    $count = mysqli_num_rows($result);
    $data = array();
    while ($row = mysqli_fetch_assoc($result)) {
        array_push($data, $row);
    }
    print_r(json_encode($data,JSON_UNESCAPED_SLASHES));
    exit;
	
}

/*
By: Dilip Kumawat
Created at: 21 Nov, 2019
Updated at: 21 Nov, 2019
Description: Remove item from cart 
*/
if($_GET['service_name'] == 'removecart')
{	
	$data = file_get_contents('php://input');
	$json = json_decode($data, true);
	$id= $json['id'];
	if (isset($_SESSION['userid'])) {
		$userid = $_SESSION['userid'];
		//$delete = $this->Common_m->deleteData("cart","product_id='$id' AND userid='$userid'",$optionid);

		$queryadd = "DELETE FROM cart WHERE product_id='$id' AND userid='$userid'";
		$resultadd = mysqli_query($conn,$queryadd)or die(mysqli_error($conn));
	}
	else
	{
		setcookie('cartcookie'.$id, $id,  time() - 3600, "/");
		setcookie('cookiecount'.$id, $id, time() - 3600, "/");
	}
}

/*
By:Jyoti Vishwakarma
Description: Removed select addons from cart
*/
if($_GET['service_name'] == 'removeaddon')
{	
	$data = file_get_contents('php://input');
	$json = json_decode($data, true);
	$addonid= $json['addonid'].",";
	$id= $json['cuisineid'];

	if (isset($_SESSION['userid'])) {
		$userid = $_SESSION['userid'];
		
		$query = "SELECT selectedaddons FROM cart WHERE product_id='$id' AND userid='$userid'";
		$result1 = mysqli_query($conn,$query)or die(mysqli_error($conn));
	    $row = mysqli_fetch_assoc($result1);
		$selectedaddons = str_replace($addonid,"",$row['selectedaddons']); 
		$query = "UPDATE cart SET selectedaddons = '$selectedaddons' WHERE product_id='$id' AND userid='$userid'";	
		$result = mysqli_query($conn,$query)or die(mysqli_error($conn));

	}
	else
	{
		$countselectedaddons = "cookieselectedaddons".$id;
		$selectedaddons = $_COOKIE[$countselectedaddons];
		$selectedaddons = str_replace($addonid,"",$selectedaddons);
		setcookie($countselectedaddons, $selectedaddons, time() + (86400 * 30), "/"); // 86400 = 1 day
	}
}

/*
By: Jyoti Vishwakarma
Description: Cart details
*/

if($_GET['service_name'] == 'cartdetail'){
	$data = file_get_contents('php://input');
	$json = json_decode($data, true);
	$datecount = $json['datecount'];

	if (isset($_SESSION['userid'])) {
		$userid = $_SESSION['userid'];

		$cartitem = array();
		$queryRecommand = "SELECT * FROM cart WHERE userid='$userid'";
		$resultRecommand = mysqli_query($conn,$queryRecommand) or die(mysqli_error($conn));
		while($rowRecommand = mysqli_fetch_assoc($resultRecommand)){
			$cartitem[count($cartitem)] = $rowRecommand['product_id'];
		
		}

		$query = "SELECT * FROM cart WHERE userid='$userid'";
		$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
		$cuisinedata = array();
		$cuisinedata['cuisines'] = array();
		$cuisinedata['discountprice']=0;
		$tprice = 0;
		$corporateid = '';
		if(isset($_SESSION['corporateid'])){
			$corporateid = $_SESSION['corporateid'];
		}
		while ($row = mysqli_fetch_assoc($result)) {
			$productid = $row['product_id'];
			$quantity = $row['quantity'];
			if($row['selectedaddons'] != ""){
				$selectedaddons = array_filter(explode(",",$row['selectedaddons'])); 
				$selectedaddons = implode(",",$selectedaddons);
			}else{
				$selectedaddons = "''"; 
			}
			
			$queryP = "SELECT * FROM cuisine WHERE status='1' and cuisineid='$productid'";
			$resultP = mysqli_query($conn,$queryP)or die(mysqli_error($conn));
			while ($rowP = mysqli_fetch_assoc($resultP)){
				$id = $rowP['cuisineid'];
				$cuisine = array();
				$cuisine['id'] = $rowP['cuisineid'];
				$cuisine['name'] = $rowP['cuisinename'];
				$cuisine['description'] = $rowP['cuisinedescription'];
				$cuisine['quantity'] = $quantity;
				$cuisine['minqty'] = $rowP['minorderquantity'];
				$cuisine['serves'] = $rowP['servingdetail'];
				$cuisine['type'] = $rowP['type'];
				$kitchenid = $rowP['userid'];
				$cuisine['isNormalCuisine']='0';
				$offer = 0;
				if($rowP['isCorporateMeal'] =='0' && $rowP['isEventMeal'] == '0'){
					$cuisine['isNormalCuisine']='1';
					if($corporateid != ''){
						$queryO = "SELECT offer FROM corporateoffermapping WHERE status='1' AND restaurantid = '$kitchenid' AND corporateid='$corporateid'";	
						$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
						$rowO = mysqli_fetch_assoc($resultO);
						$offer = $rowO['offer'];
						$discountprice = round($rowP['price'] - ( ( $rowP['price'] * $offer ) / 100 ));
						$rowP['price'] = $discountprice;
					}else{
						$queryO = "SELECT offer FROM offeroncuisine WHERE status='1' AND kitchen_id = '$kitchenid'";	
						$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
						$rowO = mysqli_fetch_assoc($resultO);
						$offer = $rowO['offer'];
						$discountprice = round($rowP['price'] - ( ( $rowP['price'] * $offer ) / 100 ));
						$rowP['price'] = $discountprice;
					}
				}
				$cuisine['isPartyMeal']='0';
				if($rowP['isCorporateMeal'] =='3' && $rowP['isEventMeal'] == '0'){
					$cuisine['isPartyMeal']='1';
				}

			
				$cuisine['price'] = $rowP['price']*$quantity;
				$cuisine['priceact'] = $rowP['price'];
				$tprice = $tprice+($rowP['price']*$quantity);
				$cuisine['totalprice'] = $tprice;
				

				$queryK = "SELECT * FROM user WHERE userid = '$kitchenid'";	
				$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
				$rowK = mysqli_fetch_assoc($resultK);
				$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
				$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";
				$cuisine['kitchen_tagline'] = isset($rowK['tagline']) ? $rowK['tagline'] : "";
				$cuisine['kitchen_image'] = isset($rowK['profilepic']) ? $rowK['profilepic'] : "";
				$cuisine['kitchen_open'] = isset($rowK['workingstart']) ? $rowK['workingstart'] : "";
				$cuisine['kitchen_close'] = isset($rowK['workingend']) ? $rowK['workingend'] : "";
				if ($row['cuisinepic']!="") {
					$image_name = explode("/",$row['cuisinepic']);
					$image_name = array_pop($image_name);
					$cuisine['image'] = S3URL.$image_name;
				}
				else
				{
					$cuisine['image'] = "https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png";
				}

				$cuisine['date'] = $row['date'];
				$cuisine['time'] = $row['time'];
				if($cuisine['date'] !="NULL" && $cuisine['date'] !=""){
					$cuisinedata['isTeamMeal'] = '1';
				}else{
					$cuisinedata['isTeamMeal'] = '0';
				}

				$cuisine['isCustomisable']='0';
				$cuisine['sections'] = array();
				$addon_total = 0;

				if($selectedaddons != "''" && $selectedaddons != ""){
					if($rowP['isCorporateMeal'] =='0' && $rowP['isEventMeal'] == '0'){
						$cuisine['isCustomisable']='1';
						$sections=array();
						$query3 = "SELECT * FROM corporate_cuisine_sections_addons WHERE ID IN ($selectedaddons)";
						$result3 = mysqli_query($conn,$query3) or die(mysqli_error($conn));
						if(mysqli_num_rows($result3) > 0 ){
							while($row3 = mysqli_fetch_assoc($result3))
							{
								
								$addon = array();
								$detail = $row3['addon'];
								$price = $row3['price'];
								$addon['addon_id'] = $row3['ID'];
								$addon['detail'] = $detail;
								$addon['price'] = $price;
								if($rowP['isCorporateMeal'] =='0' && $rowP['isEventMeal'] == '0' && $offer != 0){
									$addon['strikeprice'] = $price;
									$discountprice = round($price - ( ( $price * $offer ) / 100 ));
									$addon['price'] = $discountprice;
									$price =  $discountprice;
									
								}
								$addon_total = $addon_total + $price * $quantity;
								array_push($cuisine['sections'], $addon);
							}
						}

						$querySC = "SELECT COUNT(corporate_cuisine_sections_addons.ID) as toatladdon FROM corporate_cuisine_sections,corporate_cuisine_sections_addons WHERE corporate_cuisine_sections.cuisine_id='$id' and corporate_cuisine_sections.ID = corporate_cuisine_sections_addons.section_id";
						$resultSC = mysqli_query($conn,$querySC) or die(mysqli_error($conn));	
						$rowSC = mysqli_fetch_assoc($resultSC);
						$cuisine['totaladdons']=$rowSC['toatladdon'];
					}else{

						$query2 = "SELECT * FROM corporate_cuisine_sections WHERE cuisine_id='$id'";
						$result2 = mysqli_query($conn,$query2) or die(mysqli_error($conn));
						while($row2 = mysqli_fetch_assoc($result2))
						{    

							if($rowP['isCorporateMeal'] =='0' && $rowP['isEventMeal'] == '0'){
								$cuisine['isCustomisable']='1';
							}
							
							$sections=array();
							$secid = $row2['ID'];
							$sections['addons'] = array();
							
							$query3 = "SELECT * FROM corporate_cuisine_sections_addons WHERE section_id='$secid' AND ID IN ($selectedaddons)";
							$result3 = mysqli_query($conn,$query3) or die(mysqli_error($conn));
							if(mysqli_num_rows($result3) > 0 ){
								while($row3 = mysqli_fetch_assoc($result3))
								{
									$sections['title'] =  $row2['heading'];
									$addon = array();
									$detail = $row3['addon'];
									$price = $row3['price'];
									$addon_total = $addon_total + $price * $quantity;
									$addon['addon_id'] = $row3['ID'];
									$addon['detail'] = $detail;
									$addon['price'] = $price;
									array_push($sections['addons'], $addon);
								}
							}
							array_push($cuisine['sections'], $sections);
						}
					}
				}

				
				
				$tprice = $tprice + $addon_total;
				$cuisine['recommanded'] = array();
				$recommandedfor = $rowP['recommandedfor'];
				$recommandedfor = explode(",",$recommandedfor);

				foreach ($cartitem as $val)
				{
						$productid = $val; 
						$pos = array_search( $productid, $recommandedfor );
						
						if($pos !== false){
							unset( $recommandedfor[array_search( $productid, $recommandedfor )] );
						}
					
				}
				
				foreach($recommandedfor as $vals){
					$query2 = "SELECT * FROM cuisine WHERE cuisineid = '$vals'";
					$result2 = mysqli_query($conn,$query2) or die(mysqli_error($conn));
					while($row2 = mysqli_fetch_assoc($result2))
					{ 	
						$recommand=array();
						$recommand['ID'] = $row2['cuisineid'];
						$recommand['cuisine_name'] = $row2['cuisinename'];
						if($row2['cuisinepic'] != ''){
							$recommand['image'] = $row2['cuisinepic'];
						}else{
							$recommand['image'] = "https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png";
						}
						$recommand['price'] = $row2['price'];				
						array_push($cuisine['recommanded'], $recommand);
					}
				}

				array_push($cuisinedata['cuisines'], $cuisine);		
			}	
		}
				
				$offer = 0;
				if($datecount != ""){
					$tprice = $tprice * $datecount;
					$queryO = "SELECT offer FROM subscriptionoffer WHERE noofdays<='$datecount' and status='1' ORDER BY noofdays DESC";	
					$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
					$rowO = mysqli_fetch_assoc($resultO);
					$offer = $rowO['offer'];
				}

				$gst = round(2.5 / 100 * $tprice ,2);
				$cuisinedata['sgst'] = $gst;
				$cuisinedata['cgst'] = $gst;

				$tpricewithgst = $tprice + ($gst * 2);

				$discountprice = round($tpricewithgst - ( ( $tpricewithgst * $offer ) / 100 ));
				$cuisinedata['discountprice'] = round(( $tpricewithgst * $offer ) / 100);
				$cuisinedata['totalitemprice'] = $tprice;
				$cuisinedata['totalprice'] = round($discountprice , 2);
			
		
	}
	else
	{
		$cuisinedata = array();
		$cuisinedata['cuisines'] = array();
		$cuisinedata['discountprice']= '0';
		$tprice = 0;
		foreach ($_COOKIE as $key=>$val) {
			if (substr($key, 0, 10) == "cartcookie") {
				$quantity =  $_COOKIE['cookiecount'.$val];
				$productid = $val;
				$selectedaddons = $_COOKIE['cookieselectedaddons'.$val];
				$queryP = "SELECT * FROM cuisine WHERE status='1' and cuisineid='$productid'";
				$resultP = mysqli_query($conn,$queryP)or die(mysqli_error($conn));
				while ($rowP = mysqli_fetch_assoc($resultP)){
					$cuisine = array();
					$id = $rowP['cuisineid'];
					$cuisine['id'] = $rowP['cuisineid'];
					$cuisine['name'] = $rowP['cuisinename'];
					$cuisine['description'] = $rowP['cuisinedescription'];
					$cuisine['quantity'] = $quantity;
					$cuisine['minqty'] = $rowP['minorderquantity'];
					
					$cuisine['serves'] = $rowP['servingdetail'];
					$cuisine['type'] = $rowP['type'];
					$cuisine['quantity'] = $quantity;
					$kitchenid = $rowP['userid'];
					$cuisine['isPartyMeal']='0';
					if($rowP['isCorporateMeal'] =='3' && $rowP['isEventMeal'] == '0'){
						$cuisine['isPartyMeal']='1';
					}
					$offer = 0;
					if($rowP['isCorporateMeal'] =='0' && $rowP['isEventMeal'] == '0'){

						$queryO = "SELECT offer FROM offeroncuisine WHERE status='1' AND kitchen_id = '$kitchenid'";	
						$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
						$rowO = mysqli_fetch_assoc($resultO);
						$offer = $rowO['offer'];
						$discountprice = round($rowP['price'] - ( ( $rowP['price'] * $offer ) / 100 ));
						$rowP['price'] = $discountprice;
					}

					$cuisine['price'] = $rowP['price']*$quantity;
					$cuisine['priceact'] = $rowP['price'];
					$tprice = $tprice+($rowP['price']*$quantity);
					$cuisine['totalprice'] = $tprice;
					

					if($selectedaddons != ""){
						$selectedaddons = array_filter(explode(",",$selectedaddons)); 
						$selectedaddons = implode(",",$selectedaddons);
					}else{
						$selectedaddons = "''"; 
					}

					$queryK = "SELECT * FROM user WHERE userid = '$kitchenid'";	
					$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
					$rowK = mysqli_fetch_assoc($resultK);
					$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
					$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";
					$cuisine['kitchen_image'] = isset($rowK['profilepic']) ? $rowK['profilepic'] : "";
					$cuisine['kitchen_tagline'] = isset($rowK['tagline']) ? $rowK['tagline'] : "";

					if ($rowP['cuisinepic']!="") {
						$image_name = explode("/",$rowP['cuisinepic']);
						$image_name = array_pop($image_name);
						$cuisine['image'] = S3URL.$image_name;
					}
					else
					{
						$cuisine['image'] = "https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png";
					}

					$cuisine['isCustomisable']='0';
					$cuisine['sections'] = array();
					$addon_total = 0;

					if($selectedaddons != "''" && $selectedaddons != ""){
						if($rowP['isCorporateMeal'] =='0' && $rowP['isEventMeal'] == '0'){
							$cuisine['isCustomisable']='1';
							$sections=array();
							$query3 = "SELECT * FROM corporate_cuisine_sections_addons WHERE ID IN ($selectedaddons)";
							$result3 = mysqli_query($conn,$query3) or die(mysqli_error($conn));
							if(mysqli_num_rows($result3) > 0 ){
								while($row3 = mysqli_fetch_assoc($result3))
								{
								
									$addon = array();
									$detail = $row3['addon'];
									$price = $row3['price'];
									$addon['addon_id'] = $row3['ID'];
									$addon['detail'] = $detail;
									$addon['price'] = $price;
									if($rowP['isCorporateMeal'] =='0' && $rowP['isEventMeal'] == '0' && $offer != 0){
										$addon['strikeprice'] = $price;
										$discountprice = round($price - ( ( $price * $offer ) / 100 ));
										$addon['price'] = $discountprice;
										$price = $discountprice;
									}
									$addon_total = $addon_total + $price * $quantity;
									array_push($cuisine['sections'], $addon);
								}
							}
	
							$querySC = "SELECT COUNT(corporate_cuisine_sections_addons.ID) as toatladdon FROM corporate_cuisine_sections,corporate_cuisine_sections_addons WHERE corporate_cuisine_sections.cuisine_id='$id' and corporate_cuisine_sections.ID = corporate_cuisine_sections_addons.section_id";
							$resultSC = mysqli_query($conn,$querySC) or die(mysqli_error($conn));	
							$rowSC = mysqli_fetch_assoc($resultSC);
							$cuisine['totaladdons']=$rowSC['toatladdon'];
						}else{
	
						
	
							$query2 = "SELECT * FROM corporate_cuisine_sections WHERE cuisine_id='$id'";
							$result2 = mysqli_query($conn,$query2) or die(mysqli_error($conn));
							while($row2 = mysqli_fetch_assoc($result2))
							{    
	
								if($rowP['isCorporateMeal'] =='0' && $rowP['isEventMeal'] == '0'){
									$cuisine['isCustomisable']='1';
								}
								
								$sections=array();
								$secid = $row2['ID'];
								$sections['addons'] = array();
								
								$query3 = "SELECT * FROM corporate_cuisine_sections_addons WHERE section_id='$secid' AND ID IN ($selectedaddons)";
								$result3 = mysqli_query($conn,$query3) or die(mysqli_error($conn));
								if(mysqli_num_rows($result3) > 0 ){
									while($row3 = mysqli_fetch_assoc($result3))
									{
										$sections['title'] =  $row2['heading'];
										$addon = array();
										$detail = $row3['addon'];
										$price = $row3['price'];
										$addon_total = $addon_total + $price * $quantity;
										$addon['addon_id'] = $row3['ID'];
										$addon['detail'] = $detail;
										$addon['price'] = $price;
										
										array_push($sections['addons'], $addon);
									}
								}
								array_push($cuisine['sections'], $sections);
							}
						}
					}
				

					$cuisine['recommanded'] = array();
					$recommandedfor = $rowP['recommandedfor'];
					$recommandedfor = explode(",",$recommandedfor);
					
					foreach ($_COOKIE as $key=>$val) {
						if (substr($key, 0, 10) == "cartcookie") {
							$productid = $val;
							$pos = array_search( $productid, $recommandedfor );
							
							if($pos !== false){
								unset( $recommandedfor[array_search( $productid, $recommandedfor )] );
							}
						}
					}
					
					foreach($recommandedfor as $vals){
						$query2 = "SELECT * FROM cuisine WHERE cuisineid = '$vals'";
						$result2 = mysqli_query($conn,$query2) or die(mysqli_error($conn));
						while($row2 = mysqli_fetch_assoc($result2))
						{ 	
							$recommand=array();
							$recommand['ID'] = $row2['cuisineid'];
							$recommand['cuisine_name'] = $row2['cuisinename'];
							if($row2['cuisinepic'] != ''){
								$recommand['image'] = $row2['cuisinepic'];
							}else{
								$recommand['image'] = "https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png";
							}
							$recommand['price'] = $row2['price'];				
							array_push($cuisine['recommanded'], $recommand);
						}
					}

					if(($rowP['isCorporateMeal'] == '0' || $rowP['isCorporateMeal'] == '3' || $rowP['isCorporateMeal'] == '5' || $rowP['isCorporateMeal'] == '6' ) && $rowP['isEventMeal'] == '0'){
						// $cuisine['price'] = $cuisine['price'] + $addon_total;
						$tprice = $tprice + $addon_total;
						$cuisine['totalprice'] = $tprice;
					}

					array_push($cuisinedata['cuisines'], $cuisine);
				}
			}

		}
			$gst = round(2.5 / 100 * $tprice , 2 );
			$cuisinedata['sgst'] = $gst;
			$cuisinedata['cgst'] = $gst;
			$cuisinedata['totalitemprice'] = $tprice;
			$cuisinedata['totalprice'] = round( $tprice + ($gst * 2), 2);
		
	}
	print_r(json_encode($cuisinedata));
}

/*
By: Dilip Kumawat
Created at: 21 Nov, 2019
Updated at: 21 Nov, 2019
Description: Update cart data
*/

if($_GET['service_name'] == 'cartupdate'){
	$data = file_get_contents('php://input');
	$json = json_decode($data, true);
	$userid= $_SESSION['userid'];
	$id= $json['id'];
	$quantity = $json['count'];
	if (isset($_SESSION['userid'])) {
		$userid = $_SESSION['userid'];
		$quantity = $quantity;
		$query = "UPDATE cart SET quantity = '$quantity' WHERE product_id='$id' AND userid='$userid'";	
		$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	}
	else
	{
		$quantity = $quantity;
		$countname = "cookiecount".$id;
		setcookie($countname, $quantity, time() + (86400 * 30), "/");
	}
}

/*
By: Jyoti Vishwakarma 
Description: Checkout process
*/
if($_GET['service_name'] == 'checkout'){
	require_once "partners/PHPMailer/PHPMailerAutoload.php";
	$data = file_get_contents('php://input');
	$json = json_decode($data, true);
	$corporateid = '';
	if(isset($_SESSION['corporateid'])){
		$corporateid = $_SESSION['corporateid'];

	}
	if (isset($_POST["status"])) { 
		$userid= $_SESSION['userid'];
		$status=$_POST["status"];
		$firstname=$_POST["firstname"];
		$total=$amount=$_POST["amount"]; 
		$txnid=$_POST["txnid"];
		$posted_hash=$_POST["hash"];
		$key=$_POST["key"];
		$productinfo=$_POST["productinfo"];
		$email= $_POST["email"];
		$mobile= $_POST["phone"];
		$delivery_address= $_POST["address1"];

		$actualamount= $_POST["itemcosttotal"];
		$combine_pay= $_POST["is_combine_pay"];
		$cart_total_amount= $_POST["cart_total_amount"];
		$wallt_amount= $_POST["walletamount"];

		$isPromoApply= $_POST["ispromoapply"];
		$discountamount= $_POST["discountamount"];

		$startdate = $_POST['startdate']; 
		$enddate = $_POST['enddate']; 
		$time = $_POST['time']; 
		
		$actualamount = $total;
		$paymentType = 'online';
		$salt="ttBAqyWz"; 
		if (isset($_POST["additionalCharges"])) {
			$additionalCharges=$_POST["additionalCharges"];
			$retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		}
		else {	  
			$retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		}
		$hash = hash("sha512", $retHashSeq);
		if ($hash != $posted_hash) {
			header('Location:  '.  'order-details.php?status=failed&orderId=');
			exit;
		}
		// else {
			// echo "<h3>Thank You, " . $firstname .".Your order status is ". $status .".</h3>";
			// echo "<h4>Your Transaction ID for this transaction is ".$txnid.".</h4>";

		// } 
	}
	else
	{
		$userid= $_SESSION['userid'];
		$paymentType = $json['paymenttype'];
		$delivery_address = $json['delivery_address'];
		$mobile = $json['mobile'];
		$total = $json['total'];
		$actualamount = $json['totalitemprice']; 
		$cart_total_amount = $json['cart_total_amount']; 
		$combine_pay = $json['combine_pay']; 
		$wallt_amount = $json['wallet_amount'];
		$startdate = $json['startdate']; 
		$enddate = $json['enddate']; 
		$time = $json['order_time']; 

		$isPromoApply = $json['isPromoApply']; 
		$discountamount = $json['discountamount']; 
		
	}
	$iswalletused = '0';
	$datecount= 0;
	if($startdate != ""){
		$datecount = explode(",",$startdate);	
		$datecount = count($datecount);
	}
	$offer = 0;
	if($datecount != ""){
		$gst1 = round(5 / 100 * $actualamount,2);
		$tpricewithgst =  $gst1 + $actualamount;
		$queryO = "SELECT offer FROM subscriptionoffer WHERE noofdays<='$datecount' and status='1' ORDER BY noofdays DESC";	
		$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
		$rowO = mysqli_fetch_assoc($resultO);
		$offer = $rowO['offer'];
	    $discountamount = round(( $tpricewithgst * $offer ) / 100);
	}
	
	if($isPromoApply == '1'){
		$cart_total_amount = $cart_total_amount - $discountamount;
	}
	$usedwallamount = 0;
	if($wallt_amount!=""){
		if($wallt_amount >= $cart_total_amount){
			$cart_total_amount = 0;
			$usedwallamount = $cart_total_amount;
			$paymentType = "WP";
		}else{
			$cart_total_amount = $cart_total_amount - $wallt_amount;
			$usedwallamount = $wallt_amount;
			if ($paymentType == "online"){
				$paymentType = "online + WP";
			}else if($paymentType == "COD"){
				$paymentType = "COD + WP";
			}
		}

	}
	$discounthtml = "";
	if($discountamount != "" && $discountamount != 0){
		$discounthtml = '<tr>
							<td colspan="2"></td>
							<td><p>Discount</p></td>
							<td><p>&#8377; <span>'.$discountamount.'</span></p></td>
						</tr>';
	}
	$usedwallethtml = "";
	if($usedwallamount != "" && $usedwallamount != ""){
		$usedwallethtml = '<tr>
							   <td colspan="2"></td>
							   <td><p>Used Wallet Points</p></td>
							   <td><p>&#8377; <span>'.$usedwallamount.'</span></p></td>
						   </tr>';
	}	

	$orderdate = $date = date('Y-m-d H:i:s');
	$quecsord = "INSERT INTO casseroleorder SET userid='".$userid."',total='$cart_total_amount',orderdate='$orderdate',paymentType = '$paymentType', transId = '',actualamount='".$actualamount."',discountamount='".$discountamount."',couponapplied='".$isPromoApply."',cloudkitchpoints='".$usedwallamount."',delivery_address='$delivery_address',mobile='$mobile',invitecode='1111',startdate='$startdate',enddate='$enddate',order_time='$time',offerapplied='$offer'";
	$result = mysqli_query($conn,$quecsord) or die(mysqli_error($conn));
	$casordid = mysqli_insert_id($conn);
	$status = array();
	if ($result) {

		$closedate = date("Y-m-d H:i:s", strtotime('+2 hours', strtotime($orderdate)));
		$quecsit = "INSERT INTO pushnotiorder SET orderid='$casordid',user_id='$userid',closedate='$closedate'";
		$resultorder = mysqli_query($conn,$quecsit) or die(mysqli_error($conn));

		$status['status'] = "success";
		$status['orderid'] = $casordid;
		$ordered = "";
		$query = "SELECT * FROM cart WHERE userid='$userid'";
		$result1 = mysqli_query($conn,$query)or die(mysqli_error($conn));
		while ($row = mysqli_fetch_assoc($result1)) {
			$productid = $row['product_id']; 
			$quantity = $row['quantity']; 
			$date = $row['date'];
			$time = $row['time']; 
			$special_instruction = $row['special_instruction']; 
			if($row['selectedaddons'] != ""){
				$selectedaddons = array_filter(explode(",",$row['selectedaddons'])); 
				$selectedaddons = implode(",",$selectedaddons);
			}else{
				$selectedaddons = "''"; 
			}
			$queryP = "SELECT * FROM cuisine WHERE status = '1' and cuisineid='$productid'";
			$resultP = mysqli_query($conn,$queryP)or die(mysqli_error($conn));
			while ($rowP = mysqli_fetch_assoc($resultP)){
					$kitchenid = $rowP['userid'];
					$price = $rowP['price'];
					$cuisinename = $rowP['cuisinename'];
					$offer = 0;
					if($rowP['isCorporateMeal'] =='0' && $rowP['isEventMeal'] == '0'){
						if($corporateid != ''){
							$queryO = "SELECT offer FROM corporateoffermapping WHERE status='1' AND restaurantid = '$kitchenid' AND corporateid='$corporateid'";	
							$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
							$rowO = mysqli_fetch_assoc($resultO);
							$offer = $rowO['offer'];
							$discountprice = round($rowP['price'] - ( ( $rowP['price'] * $offer ) / 100 ));
							$rowP['price'] = $discountprice;
						}else{
							$queryO = "SELECT offer FROM offeroncuisine WHERE status='1' AND kitchen_id = '$kitchenid'";	
							$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
							$rowO = mysqli_fetch_assoc($resultO);
							$offer = $rowO['offer'];
							$discountprice = round($rowP['price'] - ( ( $rowP['price'] * $offer ) / 100 ));
							$rowP['price'] = $discountprice;
						}
					}
					$price = $rowP['price'];
					$totalprice = $rowP['price']*$quantity;
					$chefid = $rowP['userid'];
					$MTP = 0;

					$addon_total = 0;
					$selectedaddonswithprice = '';
					$addon = "";
					$query3 = "SELECT * FROM corporate_cuisine_sections_addons WHERE ID IN ($selectedaddons)";
					$result3 = mysqli_query($conn,$query3) or die(mysqli_error($conn));
					if(mysqli_num_rows($result3) > 0){
						while($row3 = mysqli_fetch_assoc($result3))
						{ 
							$priceaddon = $row3['price'];
							if($rowP['isCorporateMeal'] =='0' && $rowP['isEventMeal'] == '0' && $offer != 0){
								
								$priceaddon = round( $row3['price'] - ( (  $row3['price'] * $offer ) / 100 ));
								
							}
							
							$addonpriceqty = $priceaddon * $quantity;
							$addon .= '<div style="background-color: #fff;font-size: 12px;margin: 10px 10px 0 0;padding: 2px 12px;border-radius: 10px;position: relative;">'. $row3['addon'] .' (+&#8377; '. $addonpriceqty .')</div>';
							$addon_total = $addon_total + $addonpriceqty;
							$selectedaddonswithprice .= $row3['ID'] .'-'. $addonpriceqty.',';

						
						}

					}
								
						$totalprice =  $totalprice + $addon_total ;
						$gst = round(2.5 / 100 * $totalprice,2);
						$totalprice = $totalprice + $gst +$gst;
						$cash_price= $totalprice;
							
					if($wallt_amount > 0){
						$iswalletused = '1';
						if($wallt_amount > $totalprice){
							$wallt_amount = $wallt_amount - $totalprice;
							$cash_price = "0";
							$MTP = $totalprice;
							$payment_type = "WP";
							$remain_wallet = $wallt_amount;
						}else if($totalprice > $wallt_amount){
							$remain_price = $totalprice - $wallt_amount;
							$cash_price = $remain_price;
							$MTP = $wallt_amount;
							$payment_type =$paymentType;
							$remain_wallet = 0;
							
						}
						
					}
					
					$queryrestro = "SELECT * FROM user WHERE userid = '$kitchenid'";	
					$resultrestro = mysqli_query($conn,$queryrestro)or die(mysqli_error($conn));
					$rowrestro = mysqli_fetch_assoc($resultrestro);
					$kitchen_name = isset($rowrestro['name']) ? $rowrestro['name'] : "";
					
					$ordered .= '<tr>
									<td><p><span>'.$kitchen_name.'</span><br> '.$cuisinename.''.$addon.'</p></td>
									<td><p><span>'.$quantity.'</span> </p></td>
									<td><p>&#8377; <span>'.$price.'</span></p></td>
									<td><p>&#8377; <span>'.($quantity * $price).'</span></p></td>
								</tr>';
					
					$quecsit = "INSERT INTO casseroleorderitem SET cuisineid='$productid',cassordid='".$casordid."',price='".$price."',quantity='".$quantity."',type='trending',code='',chefid='$chefid',usrwntDelivery='',cash='$cash_price',MTP='$MTP',PaymentType='$payment_type', total = '$totalprice',sgst='$gst',cgst='$gst',selected_addons='$selectedaddonswithprice',special_instructions='$special_instruction'";
					$resultorder = mysqli_query($conn,$quecsit) or die(mysqli_error($conn));
					if ($resultorder) {

						if($iswalletused == '1'){
							$queryWallet = "UPDATE walletmaster SET cloudkitchValue = '$remain_wallet' WHERE  userid='$userid'";	
							$resultwallet = mysqli_query($conn,$queryWallet)or die(mysqli_error($conn));
						}
						

						$querydel = "DELETE FROM cart WHERE product_id='$productid' AND userid='$userid'";
						$resultdel = mysqli_query($conn,$querydel)or die(mysqli_error($conn));

						if($date != "" && $time!=""){

							$queryWallet = "UPDATE casseroleorder SET startdate='$date',enddate='$date',order_time='$time' WHERE  cassordid='$casordid'";	
							$resultwallet = mysqli_query($conn,$queryWallet)or die(mysqli_error($conn));
						}
					}
					else
					{
						$status['status'] = "failed";
					}
			}
		}
		$querydel = "DELETE FROM cart WHERE userid='$userid'";
		$resultdel = mysqli_query($conn,$querydel)or die(mysqli_error($conn));

		$billsgst = round(2.5 / 100 * $actualamount,2); 
		$queryrestro = "SELECT email FROM user WHERE userid = '$userid'";	
		$resultrestro = mysqli_query($conn,$queryrestro)or die(mysqli_error($conn));
		$rowrestro = mysqli_fetch_assoc($resultrestro);
		$email = isset($rowrestro['email']) ? $rowrestro['email'] : "";

		$mail = new PHPMailer;
		$mail->SMTPDebug = false;
		$mail->isSMTP();
		$mail->Host = "smtp.gmail.com";
		$mail->SMTPAuth = true;
		$mail->Username = "cloudkitchservice@gmail.com";
		$mail->Password = "Cloudkitch@12345";
		$mail->SMTPSecure = "ssl";

		//Set TCP port to connect to 
		$mail->Port = 465;

		$mail->From = "donotreply@cloudkitch.in";
		$mail->FromName = "Cloudkitch";

		//To address and name
		// echo $email;
		$mail->addAddress($email, "order");
		$mail->addAddress("cloudkitchprivatelimited@gmail.com", "New order");
		//CC and BCC
		$mail->isHTML(true);
		$htmlmail = '';
		$htmlmail = '<!DOCTYPE html>
						<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
						<head>
							<meta charset="utf-8" />
							<meta name="viewport" content="width=device-width" />
							<meta http-equiv="X-UA-Compatible" content="IE=edge" />
							<meta name="x-apple-disable-message-reformatting" />
							<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet"/>
							<title></title>
							<style>
							html,
							body {
								margin: 0 auto !important;
								height: 100% !important;
								width: 100% !important;
							}
							* {
								-ms-text-size-adjust: 100%;
								-webkit-text-size-adjust: 100%;
							}
							div[style*="margin: 16px 0"] {
								margin: 0 !important;
							}
							table table table {
								table-layout: auto;
							}
							img {
								-ms-interpolation-mode: bicubic;
							}
							*[x-apple-data-detectors],
							.x-gmail-data-detectors,
							.x-gmail-data-detectors *,
							.aBn {
								border-bottom: 0 !important;
								cursor: default !important;
								color: inherit !important;
								text-decoration: none !important;
								font-size: inherit !important;
								font-family: inherit !important;
								font-weight: inherit !important;
								line-height: inherit !important;
							}
							.a6S {
								display: none !important;
								opacity: 0.01 !important;
							}
							img.g-img + div {
								display: none !important;
							}
							.button-link {
								text-decoration: none !important;
							}
							.email-container {
								width: 31.33% !important;
								margin: 0 1% !important;
								display: inline-block;
								vertical-align: top;
							}
							.button-td,
							.button-a {
								transition: all 100ms ease-in;
							}
							.button-td:hover,
							.button-a:hover {
								background: #555555 !important;
								border-color: #555555 !important;
							}
							.table-header tr:first-child th{border-bottom: 1px solid #c4c4c4;padding-bottom: 20px;}
							.table-header tr:last-child td{border-top: 1px solid #c4c4c4;}
							.table-header tr:nth-last-child(2) td{border-top: 1px solid #c4c4c4;}
							.table-header tr:nth-last-child(3) td{border-top: 1px solid #c4c4c4;}
							.table-header td p span{font-weight: 700;}
							.table-header tr th, .table-header tr td{padding: 10px;}
							@media screen and (min-width: 200px) and (max-width: 600px) {
								.email-container {
								width: 100% !important;
								margin: 10px auto !important;
								display: block !important;
								}
								.fluid {
								max-width: 100% !important;
								height: auto !important;
								margin-left: auto !important;
								margin-right: auto !important;
								}
								.stack-column,
								.stack-column-center {
								display: block !important;
								width: 100% !important;
								max-width: 100% !important;
								direction: ltr !important;
								}
								.stack-column-center {
								text-align: center !important;
								}
								.center-on-narrow {
								text-align: center !important;
								display: block !important;
								margin-left: auto !important;
								margin-right: auto !important;
								float: none !important;
								}
								table.center-on-narrow {
								display: inline-block !important;
								}
								.email-container p {
								font-size: 17px !important;
								line-height: 22px !important;
								}
							}
							</style>
						</head>
						<body	style="margin:0px auto; font-family: Open Sans, sans-serif;;box-sizing:border-box; color: #5A5A5A;">
							<table style="width: 100%;max-width:800px; margin:0 auto;border-spacing: 0; border-collapse: collapse; border: solid 1px #eceaea;">
							<tr>
								<td style="padding: 0;">
								<table style="width: 100%;border-spacing: 0; border-collapse: collapse;">
									<tr>
									<td style="padding: 0;border-bottom:1px solid #eceaea;">
										<a href="#" target="_blank">
										<img src="https://cloudkitch.co.in/images/mail/cloudkitch-logo.png" style="width:auto;height:80px;margin:15px auto;display:block;" />
										</a>
									</td>
									</tr>
								</table>
						
								<table style="width: 100%; padding: 20px; border-spacing: 0;text-align: center;">
									<tr>
									<td style="text-align: center;">
										<h2 style="color: #A88883;">Bill details</h2>
										<table class="table-header" style="width: 100%; padding: 20px; border-spacing: 0;text-align: left;background: #e5e5e557;">
											<tr>
												<th>Item details</th>
												<th>Quantity</th>
												<th>Price</th>
												<th>Subtotal</th>
											</tr>
											'.$ordered.'
											<tr>
												<td colspan="2"></td>
												<td><p>2.5% SGST</p></td>
												<td><p>&#8377; <span>'.$billsgst.'</span></p></td>
											</tr>
											<tr>
												<td colspan="2"></td>
												<td><p>2.5% CGST</p></td>
												<td><p>&#8377; <span>'.$billsgst.'</span></p></td>
											</tr>
											'.$discounthtml.'
											'.$usedwallethtml.'
											<tr>
												<td colspan="2"></td>
												<td><p>Total</p></td>
												<td><p>&#8377; <span>'.$cart_total_amount.'</span></p></td>
											</tr>
										</table>
						
									</td>
									</tr>
								</table>
						
								<table style="width: 100%; border-spacing: 0;text-align: center;border-top: 1px solid #eceaea;background: #dfdfdf;">
									<tr>
									<td style="text-align: center;padding-left: 40px;">
										<p>
										<a href="https://twitter.com/CloudKitch" target="_blank" style="text-decoration: none; margin-right: 10px;">
											<img src="https://cloudkitch.co.in/images/mail/twitter.png" alt="Twitter"/>
										</a>
										<a href="https://www.instagram.com/cloudkitch/" target="_blank" style="text-decoration: none; margin-right: 10px;">
											<img src="https://cloudkitch.co.in/images/mail/instagram.png" alt="Instagram"/>
										</a>
										<a href="https://www.linkedin.com/company/cloudkitch-private-limited" target="_blank" style="text-decoration: none; margin-right: 10px;">
											<img  src="https://cloudkitch.co.in/images/mail/linkedin.png" alt="Linkedin"/>
										</a>
										</p>
									</td>
									</tr>
								</table>
								</td>
							</tr>
							</table>
						</body>
						</html>
						';
				// Set email format to HTML
				$mail->isHTML(true);
				$mail->Subject = 'Cloudkitch Order Status';
				$mail->Body    = $htmlmail;
	
				if($mail->send()){
					$msg = 'Order Accepted.And Mail is send.';
				}else{
					$msg = 'Order Accepted.But Mail is not send.';
				}
		

	}	
	if (isset($_POST["status"])) {
		header("Location:  order-details.php?status=success&orderId=$casordid");
	}
	else
	{
		print_r(json_encode($status));
	}
}


/*
By: Dilip Kumawat
Created at: 28 Nov, 2019
Updated at: 28 Nov, 2019
Description: State list
*/
if($_GET['service_name'] == 'states'){
	$status = array();
	$status['states'] = array();
	$query = "SELECT * FROM states WHERE country_id='101'";
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	while ($row = mysqli_fetch_assoc($result)) {
		$states = array();
		$states['id'] = $row['id'];
		$states['name'] = $row['name'];
		array_push($status['states'], $states);
	}
	print_r(json_encode($status));
}

/*
By:Jyoti Vishwakarma
Description: Fetch cuisine present in cart
*/
if($_GET['service_name'] == 'addedincart'){
	$item = array();
	$cartitem = array();
	 $_SESSION['userid'];
	 if(isset($_SESSION['userid'])){
		$userid = $_SESSION['userid'];
		$query = "SELECT product_id,quantity FROM cart WHERE userid='$userid'";
		$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
		//print_r($result);
		$item = array();
		while ($row = mysqli_fetch_assoc($result)) {
			$ct = array();
			$productid = $row['product_id'];
			$quantity =  $row['quantity'];
			$ct['id'] = $productid;
			$ct['qty'] = $quantity;
			array_push($item,$ct);
		}
	 }else{	
		foreach($_COOKIE as $key => $val) {
			if (substr($key, 0, 10) == "cartcookie") {
				 $ct = array();
				 $quantity =  $_COOKIE['cookiecount'.$val];
				 $ct['id'] = $val;
				 $ct['qty'] = $quantity;
				 array_push($item,$ct);

			}
	    }
	
	}
	$cartitem['cartitem'] = $item;
	print_r(json_encode($cartitem));
}

/*
By:Jyoti Vishwakarma
Description: Store user delivery address
*/
if($_GET['service_name'] == 'createaddress')
{
	$loc_full_name = $_POST['loc_full_name'];
    $loc_mobile_number = $_POST['loc_mobile_number'];
    $loc_state = $_POST['loc_state'];
    $loc_city = $_POST['loc_city'];
    $loc_pincode = $_POST['loc_pincode'];
    $loc_address = $_POST['loc_address'];
    $loc_address_title = $_POST['loc_address_title'];
    $loc_landmark = $_POST['loc_landmark'];
	$userid= $_SESSION['userid'];

	$query = "SELECT * FROM useraddresses WHERE userid='$userid' AND addresstitle='$loc_address_title'";
    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
    $rowcount=mysqli_num_rows($result);
    if ($rowcount>0) {
		$query = "UPDATE useraddresses SET fullname='$loc_full_name',mobile='$loc_mobile_number',state='$loc_state',city='$loc_city',landmark='$loc_landmark',pincode='$loc_pincode',address='$loc_address' WHERE userid='$userid' AND addresstitle='$loc_address_title'";	
		$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
        $status['status'] = 'success';
        $status['msg'] = 'Updated successfully.';
        print_r(json_encode($status,JSON_UNESCAPED_SLASHES));
        exit;
    }else{
		$queadd = "INSERT INTO useraddresses SET addresstitle='$loc_address_title',fullname='$loc_full_name',mobile='$loc_mobile_number',state='$loc_state',city='$loc_city',landmark='$loc_landmark',pincode='$loc_pincode',address='$loc_address',userid='$userid'";
		$excadd = mysqli_query($conn,$queadd) or die(mysqli_error($conn));
			if ($excadd) {
				$status['status'] = 'success';
				$status['msg'] = 'Added successfully.';
			}
			else
			{
				$status['status'] = 'failed';
				$status['msg'] = 'Something went wrong!';
			}
		print_r(json_encode($status,JSON_UNESCAPED_SLASHES));
		exit;
	}
    
}

/*
By:Jyoti Vishwakarma
Description: Fetch user delivery address
*/
if($_GET['service_name'] == 'getUserAddress')
{
	
    $json = file_get_contents('php://input');
    $json = json_decode($json,true);
    $type = $json['type'];  
	$userid= $_SESSION['userid'];
    $query = "SELECT * FROM useraddresses WHERE userid='$userid' AND addresstitle='$type'";
    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
    $count = mysqli_num_rows($result);
    $data = array();
    while ($row = mysqli_fetch_assoc($result)) {
        array_push($data, $row);
    }
    print_r(json_encode($data,JSON_UNESCAPED_SLASHES));
    exit;
}

/*
By:Jyoti Vishwakarma
Description: Corporate login
*/
if($_GET['service_name'] == 'corporatelogin')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);

	$email = $json['email'];
	$password = $json['password'];
	$logintype = $json['logintype'];
	
		$quesp = "SELECT * FROM user WHERE email='$email' AND  isCorporateUser ='1'";	
		$excph = mysqli_query($conn,$quesp)or die(mysqli_error($conn));
		
		if(mysqli_num_rows($excph) > 0)
		{
			$quesp1 = "SELECT * FROM user WHERE (email='$email' AND  isCorporateUser ='1') AND password = '$password'";	
			$excph1 = mysqli_query($conn,$quesp1)or die(mysqli_error($conn));
			if(mysqli_num_rows($excph) > 0)
			{
				$rowse = mysqli_fetch_assoc($excph1);
				$_SESSION['userid'] = $rowse['userid'];
				$_SESSION['useremail'] = $rowse['email'];
				$_SESSION['username'] = $rowse['name'];
				$_SESSION['userimage'] = $rowse['profilepic'];
				$_SESSION['corporateid'] = $rowse['corporate_id'];
				$_SESSION['isCorporateUser'] = $rowse['isCorporateUser'];
				$response['status'] = 'success';
				$response['msg'] = 'Login success';
			}
			else
			{
				$response['status'] = 'failure';
				$response['msg'] = 'Invalid passsword';
			}
		}
		else
		{
			$response['status'] = 'failure';
			$response['msg'] = 'You are not registered as corporate user.';
		}
	
	print_r(json_encode($response));
}

/*
By:Jyoti Vishwakarma
Description: Fetch user details
*/
if($_GET['service_name'] == 'getUserDetails')
{
	$userid = $_SESSION['userid'];
	$query = "SELECT * FROM user WHERE userid = '$userid' ";	
	$result = mysqli_query($conn,$query) or die(mysqli_error($conn));
    $row = mysqli_fetch_assoc($result);
    $data = array();
    $data['id'] = isset($row['id']) ? $row['id'] : ''; 
	$data['name'] = isset($row['name']) ? $row['name'] : '';
	$data['email'] = isset($row['email']) ? $row['email'] : '';
	$data['phone'] = isset($row['phone']) ? $row['phone'] : '';

	if(isset($row['profilepic']) && $row['profilepic']!=""){
		$data['profilepic'] = $row['profilepic'];
	}else{
		$data['profilepic'] = "";
	}
	
    print_r(json_encode($data));
	
}

/*
By:Jyoti Vishwakarma
Description: Save special enquiry form deatils
*/
if($_GET['service_name'] == 'specialenquiry')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);

	$corpFullEmail = $json['corpFullEmail'];
	$corpFullName = $json['corpFullName'];
	$corpFullCompany = $json['corpFullCompany'];
	$corpFullPhone = $json['corpFullPhone'];
	$corpEventDesc = $json['corpEventDesc'];
	$corpNoGuest = $json['corpNoGuest'];
	$corpFullBudget = $json['corpFullBudget'];
	$corpdatepicker = $json['corpdatepicker'];
	$corptimepicker = $json['corptimepicker'];
	$corpCatered = $json['corpCatered'];
	$corpRestrictions = $json['corpRestrictions'];
	$corpRemarks = $json['corpRemarks'];
	
	$insf = "INSERT INTO specialenquiry SET corpFullName='".$corpFullName."',corpFullEmail='".$corpFullEmail."',corpFullPhone='".$corpFullPhone."',corpFullCompany='".$corpFullCompany."',corpEventDesc='".$corpEventDesc."',corpNoGuest='".$corpNoGuest."',corpFullBudget='".$corpFullBudget."',corpdatepicker='".$corpdatepicker."',corptimepicker='".$corptimepicker."',corpCatered='".$corpCatered."',corpRestrictions='".$corpRestrictions."',corpRemarks='".$corpRemarks."'";
	$excf = mysqli_query($conn,$insf) or die(mysqli_error($conn));
	if ($excf){
		$response['status'] = 'success';
		$response['msg'] = 'Login success';
	}
	else
	{
		$response['status'] = 'new';
		$response['msg'] = 'User Not Registered';
	}
	
	print_r(json_encode($response));
}

/*
By:Jyoti Vishwakarma
Description: Corporate restaurant cuisines lsit
*/
if($_GET['service_name'] == 'corporate_restro_cuisines')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$keyword = $json['keyword'];
	$categories = $json['categories'];
	$sortbycost = $json['sortbycost'];
	$kitchen_id = $json['kitchen_id'];
	$limit = "";
	if(isset($json['limit'])){
		$limit = $json['limit'];
	}
	$type = $json['type'];
	$orderby = "ORDER BY cuisineid DESC";
	$limitss = "limit 0,30";

	$condition = "";
	if ($keyword!="") {
		$condition .= " AND cuisinename LIKE '%$keyword%'";
	}
	if($categories!="")
	{
		$condition .= " AND cuisinetypeId IN ($categories)";
	}
	if($type!="")
	{
		$condition .= " AND type LIKE '$type%'";
	}

	if($sortbycost!="")
	{
		$orderby = "ORDER BY price $sortbycost";
	}

	if($limit!="")
	{
		$d = $limit * 30;
		$limitss = "limit  $d, 30";
	}

	if(isset($_COOKIE['pincode'])){
		$pincode = $_COOKIE['pincode'];
		$location = "AND cuisine.userid in (SELECT userid FROM `user` WHERE pin_code LIKE '%$pincode%')";
	}else{
		$location = "";
	}

	$corporateid = '';
	if(isset($_SESSION['corporateid'])){
		$corporateid = $_SESSION['corporateid'];

	}

	$query = "SELECT * FROM cuisine WHERE userid ='$kitchen_id' AND isActive = 'Y' AND isCorporateMeal = '0' AND isEventMeal = '0' $location $condition $orderby $limitss";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));


	
	$data = array();
	$data['cuisines'] = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$cuisine = array();
		$id = $row['cuisineid'];
		$cuisine['id'] = $row['cuisineid'];
		$cuisine['name'] = $row['cuisinename'];
		$cuisine['description'] = $row['cuisinedescription'];
		$cuisine['price'] = $row['price'];
		$cuisine['serves'] = $row['servingdetail'];
		$cuisine['type'] = $row['type'];

		$userid = $row['userid'];

		$queryK = "SELECT * FROM user WHERE userid = '$userid'";	
		$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
		$rowK = mysqli_fetch_assoc($resultK);
		$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
		$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";

		if ($row['cuisinepic']!="") {
			$image_name = explode("/",$row['cuisinepic']);
			$image_name = array_pop($image_name);
			$cuisine['image'] = S3URL.$image_name;
		}
		else
		{
			$cuisine['image'] = "https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png";
		}

		if($corporateid != '' && $userid != ''){
			$queryO = "SELECT offer FROM corporateoffermapping WHERE restaurantid = '$userid' AND corporateid='$corporateid'";	
			$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
			$rowO = mysqli_fetch_assoc($resultO);
			$cuisine['offer'] = isset($rowO['offer']) ? $rowO['offer'] : "";
		}else{
			$cuisine['offer']='';
		}



		array_push($data['cuisines'], $cuisine);
	}	
	print_r(json_encode($data));
}

/*
By:Jyoti Vishwakarma
Description: Restaurant category
*/
if($_GET['service_name'] == 'restro_categories')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$kitchenid = $json['kitchenid'];

	$data = array();
	$data['categories'] = array();

	$queryC = "SELECT cuisinetypeId FROM cuisine WHERE userid= '$kitchenid' AND  isActive = 'Y' AND isCorporateMeal = '0' AND isEventMeal = '0' GROUP BY cuisinetypeId";	
	$resultC = mysqli_query($conn,$queryC)or die(mysqli_error($conn));
	while ($rowC = mysqli_fetch_assoc($resultC)) {

	$cuisinetypeid = $rowC['cuisinetypeId'];
	$query = "SELECT * FROM cuisinetypemaster WHERE cuisinetypeid = '$cuisinetypeid' ";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	
	
		while ($row = mysqli_fetch_assoc($result)) {
			$categories = array();
			$categories['id'] = $row['cuisinetypeid'];
			$categories['category'] = $row['cuisinetype'];
			array_push($data['categories'], $categories);
		}	
    }
	print_r(json_encode($data));
}

/*
By:Jyoti Vishwakarma
Description: Upload user profile Picture
*/
if($_GET['service_name'] == 'updateProfilePic')
{
	
	$data = file_get_contents('php://input');
	$json = json_decode($data, true);
	$image= trim($json['image']);
	$userid = $_SESSION['userid'];

	$propic = s3upload($image);
	$updatepic = "UPDATE `user` SET `profilepic` = '$propic' WHERE userid = '$userid'";
	$excpic = mysqli_query($conn,$updatepic) or die(mysqli_error($conn));
	if($excpic)
	{
		$status["status"]="success";
		$status["propic"]=$propic;
		$_SESSION['userimage'] = $propic;
	}
	else{
		$status["status"]="failed";
	}
	
	print_r(json_encode($status));
	
}

/*
By:Jyoti Vishwakarma
Description: Update user profile details
*/
if($_GET['service_name'] == 'updateProfileDetails')
{
	
	$data = file_get_contents('php://input');
	$json = json_decode($data, true);
	$fullname = $json['fullname'];
	$email= $json['email'];
	$phone= $json['phone'];
	$userid = $_SESSION['userid'];

	$propic = s3upload($image);
	$update = "UPDATE `user` SET name = '$fullname',email='$email',phone='$phone' WHERE userid = '$userid'";
	$excpic = mysqli_query($conn,$update) or die(mysqli_error($conn));
	if($excpic)
	{
		$status["status"]="success";
	}
	else{
		$status["status"]="failed";
	}
	
	print_r(json_encode($status));
	
}

/*
By:Jyoti Vishwakarma
Description: Save user ratings
*/
if($_GET['service_name'] == 'getRating')
 {
 	
     $json = file_get_contents('php://input');
 	$json = json_decode($json,true);
 	
 	$userId = $json['userId'];  
 	$orderid = $json['orderid'];  
 	$rating = $json['rating'];  
 	$feedback = $json['feedback'];  
 	$insf = "INSERT INTO order_rating SET userId='".$userId."',orderid='".$orderid."',rating='".$rating."',feedback='".$feedback."'";
 	$excf = mysqli_query($conn,$insf) or die(mysqli_error($conn));
 	if ($excf) {
 		$response['status'] = 'success';
		$response['msg'] = 'Registration Success.';
		$response['msg'] = 'Review submitted successfully.';
 	}
 
     print_r(json_encode($response,JSON_UNESCAPED_SLASHES));
 }    

 /*
By:Jyoti Vishwakarma
Description: Send otp email when user fill forgot password form  
*/
 if($_GET['service_name'] == 'forgotPasswordEmail')
 {
	require_once "partners/PHPMailer/PHPMailerAutoload.php";
    $json = file_get_contents('php://input');
 	$json = json_decode($json,true);
 	$response = array();
	$email = $json['email']; 


	$queryuser = "SELECT * FROM user WHERE email='$email'";	
	$excuser = mysqli_query($conn,$queryuser)or die(mysqli_error($conn));
	if(mysqli_num_rows($excuser) > 0)
	{
		$otp = rand(000000,999999);
		//PHPMailer Object
		$mail = new PHPMailer;

		//$mail->SMTPDebug = 3;
		$mail->SMTPDebug = false;
		$mail->isSMTP();
		$mail->Host = "smtp.gmail.com";
		$mail->SMTPAuth = true;
		$mail->Username = "aradhana@appicmobile.com";
		$mail->Password = "Aradhana24011990";
		$mail->SMTPSecure = "ssl";

		//Set TCP port to connect to 
		$mail->Port = 465;

		$mail->From = "donotreply@cloudkitch.in";
		$mail->FromName = "Cloudkitch";

		//To address and name
		$mail->addAddress("$email", "OTP");

		//CC and BCC
		$mail->isHTML(true);
		
		$text .= '<!DOCTYPE html>
					<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" >
					<head>
						<meta charset="utf-8" />
						<meta name="viewport" content="width=device-width" />
						<meta http-equiv="X-UA-Compatible" content="IE=edge" />
						<meta name="x-apple-disable-message-reformatting" />
						<link
						href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap"
						rel="stylesheet"
						/>
						<title></title>
						<style>
						html,
						body {
							margin: 0 auto !important;
							height: 100% !important;
							width: 100% !important;
						}
						* {
							-ms-text-size-adjust: 100%;
							-webkit-text-size-adjust: 100%;
						}
						div[style*="margin: 16px 0"] {
							margin: 0 !important;
						}
						table table table {
							table-layout: auto;
						}
						img {
							-ms-interpolation-mode: bicubic;
						}
						*[x-apple-data-detectors],
						.x-gmail-data-detectors,
						.x-gmail-data-detectors *,
						.aBn {
							border-bottom: 0 !important;
							cursor: default !important;
							color: inherit !important;
							text-decoration: none !important;
							font-size: inherit !important;
							font-family: inherit !important;
							font-weight: inherit !important;
							line-height: inherit !important;
						}
						.a6S {
							display: none !important;
							opacity: 0.01 !important;
						}
						img.g-img + div {
							display: none !important;
						}
						.button-link {
							text-decoration: none !important;
						}
						.email-container {
							width: 31.33% !important;
							margin: 0 1% !important;
							display: inline-block;
							vertical-align: top;
						}
						.button-td,
						.button-a {
							transition: all 100ms ease-in;
						}
						.button-td:hover,
						.button-a:hover {
							background: #555555 !important;
							border-color: #555555 !important;
						}
						@media screen and (min-width: 200px) and (max-width: 600px) {
							.email-container {
							width: 100% !important;
							margin: 10px auto !important;
							display: block !important;
							}
							.fluid {
							max-width: 100% !important;
							height: auto !important;
							margin-left: auto !important;
							margin-right: auto !important;
							}
							.stack-column,
							.stack-column-center {
							display: block !important;
							width: 100% !important;
							max-width: 100% !important;
							direction: ltr !important;
							}
							.stack-column-center {
							text-align: center !important;
							}
							.center-on-narrow {
							text-align: center !important;
							display: block !important;
							margin-left: auto !important;
							margin-right: auto !important;
							float: none !important;
							}
							table.center-on-narrow {
							display: inline-block !important;
							}
							.email-container p {
							font-size: 17px !important;
							line-height: 22px !important;
							}
						}
						</style>
					</head>
					<body style="margin:0px auto; font-family: Open Sans, sans-serif;;box-sizing:border-box; color: #5A5A5A;">
						<table style="width: 100%;max-width:800px; margin:0 auto;border-spacing: 0; border-collapse: collapse; border: solid 1px #eceaea;" >
						<tr>
							<td style="padding: 0;">
							<table style="width: 100%;border-spacing: 0; border-collapse: collapse;" >
								<tr>
								<td style="padding: 0;border-bottom:1px solid #eceaea;">
									<a href="#" target="_blank">
									<img src="http://cloudkitch.co.in/images/logo.svg" style="width:auto;height:80px;margin:15px auto;display:block;" />
									</a>
								</td>
								</tr>
							</table>
					
							<table style="width: 100%; padding: 20px; border-spacing: 0;text-align: center;" >
								<tr>
								<td style="text-align: center;padding-left: 40px;">
									
									<p style="color: #707070;">
									Your OTP is '.$otp.' .
									</p>
									
								</td>
								</tr>
							</table>
					
							<table style="width: 100%; border-spacing: 0;text-align: center;border-top: 1px solid #eceaea;background: #dfdfdf;" >
								<tr>
								<td style="text-align: center;padding-left: 40px;">
									<p>
									<a href="https://twitter.com/CloudKitch" target="_blank" style="text-decoration: none;margin-right: 10px;" >
										<img src="http://cloudkitch.co.in/images/icons/icon-twitter.svg" alt="Twitter" />
									</a>
									<a href="https://www.instagram.com/cloudkitch/" target="_blank" style="text-decoration: none; margin-right: 10px;" >
										<img src="http://cloudkitch.co.in/images/icons/icon-instagram.svg" alt="Instagram" />
									</a>
									<a href="https://www.linkedin.com/company/cloudkitch-private-limited" target="_blank" style="text-decoration: none;margin-right: 10px;">
										<img src="http://cloudkitch.co.in/images/icons/icon-linkedin.svg" alt="Linkedin"/>
									</a>
									</p>
								</td>
								</tr>
							</table>
							</td>
						</tr>
						</table>
					</body>
					</html>';
	
		
			// Content
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = 'Cloudkitch OTP';
		$mail->Body    = $text;

		if($mail->send()){
			$update = "UPDATE `user` SET otp_code = '$otp' WHERE email='$email'";
			$excpic = mysqli_query($conn,$update) or die(mysqli_error($conn));
			
			$response['status'] = 'success';
			$response['msg'] = 'OTP is send.';
		}else{
			$response['status'] = 'failed';
			$response['msg'] = 'Something went wrong.';
		}

	}else{
		$response['status'] = 'failed';
		$response['msg'] = 'Email ID doest not exist.';
	}
     print_r(json_encode($response,JSON_UNESCAPED_SLASHES));
 }    

 /*
By:Jyoti Vishwakarma
Description: OTP verification for forgot password 
*/
if($_GET['service_name'] == 'forgotPasswordOTPVerify')
{
 	
    $json = file_get_contents('php://input');
 	$json = json_decode($json,true);
 	$response = array();
 	$email = $json['email'];  
	$otp = $json['otp'];  
	 
	$queryuser = "SELECT * FROM user WHERE email='$email' AND otp_code='$otp'";	
	$excuser = mysqli_query($conn,$queryuser)or die(mysqli_error($conn));
	if(mysqli_num_rows($excuser) > 0)
	{
		$response['status'] = 'success';
		$response['msg'] = 'OTP Verified.';
	}else{
		$response['status'] = 'failed';
		$response['msg'] = 'OTP is Wrong.';
	}
		
     print_r(json_encode($response,JSON_UNESCAPED_SLASHES));
}

/*
By:Jyoti Vishwakarma
Description: Update New password
*/
if($_GET['service_name'] == 'UpdateNewPassword')
{
 	
    $json = file_get_contents('php://input');
 	$json = json_decode($json,true);
 	$response = array();
 	$email = $json['email'];  
	$pwd = $json['pwd'];  
	 
	$queryuser = "SELECT * FROM user WHERE email='$email'";	
	$excuser = mysqli_query($conn,$queryuser)or die(mysqli_error($conn));
	if(mysqli_num_rows($excuser) > 0)
	{

		$update = "UPDATE `user` SET password = '$pwd' WHERE email='$email'";
		$excupdate = mysqli_query($conn,$update) or die(mysqli_error($conn));

		$response['status'] = 'success';
		$response['msg'] = 'Password Updated.';
	}else{
		$response['status'] = 'failed';
		$response['msg'] = 'OTP is Wrong.';
	}
		
     print_r(json_encode($response,JSON_UNESCAPED_SLASHES));
}

/*
By:Jyoti Vishwakarma
Description:Home category list
*/
if($_GET['service_name'] == 'homecategories')
{
	$json = file_get_contents('php://input');
	$json = json_decode($json,true);
	 
	$pagetype = $json['pagetype'];
	$corporateid = '';

	if(isset($_SESSION['corporateid'])){
		$corporateid = $_SESSION['corporateid'];
	}
	$corporate_condition = "";
	if(isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] =='1'){
		$corporate_id = $_SESSION['corporateid'];
		$corporate_condition =  "AND FIND_IN_SET($corporate_id, corporateid)";
	}else{
		$corporate_condition ="";
	}
	$querySq = "SELECT * FROM `kitchen_cuisinetype_sequence` WHERE kitchen_id = '0' ORDER BY sequence ASC";	
	$resultsq = mysqli_query($conn,$querySq)or die(mysqli_error($conn));
	$seq = array();
	while($rowSq = mysqli_fetch_assoc($resultsq)) {
		$seq[count($seq)] = $rowSq['cuisinetypeid'];
	}
	$catseq = implode(",",$seq);
	$data = array();
	$data['categories'] = array();
	$query = "SELECT * FROM cuisinetypemaster WHERE page_type = '$pagetype' $corporate_condition ORDER BY FIELD(cuisinetypeid, $catseq)";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	
	
	while ($row = mysqli_fetch_assoc($result)) {
		$categories = array();
		$categories['id'] = $row['cuisinetypeid'];
		$categories['category'] = $row['cuisinetype'];
		array_push($data['categories'], $categories);
	}

	print_r(json_encode($data));
}

/*
By:Jyoti Vishwakarma
Description: Change user address
*/
if($_GET['service_name'] == 'changedUserAddress')
{
 	
    $json = file_get_contents('php://input');
 	$json = json_decode($json,true);
 	$response = array();
	$addressid = $json['addressid'];  
	$_SESSION['useraddress']=array();
	$query = "SELECT * FROM useraddresses WHERE useraddressid = '$addressid' ORDER BY useraddressid ASC";
	$result = mysqli_query($conn,$query) or die(mysqli_error($conn));
	if(mysqli_num_rows($result) > 0)
	{
		$row = mysqli_fetch_assoc($result);
		$_SESSION['useraddress']['id'] = $row['useraddressid'];
		$_SESSION['useraddress']['addresstype'] = $row['addresstitle'];
		$_SESSION['useraddress']['address'] = $row['address'].', '.$row['city'].', '.$row['state'].', '.$row['pincode'];
		$response['addresstype'] = $row['addresstitle'];
		$response['address'] = $row['address'].', '.$row['city'].', '.$row['state'].', '.$row['pincode'];
		$response['status'] = 'success';
		$response['msg'] = 'Login success';
	}else{
		$response['status'] = 'failed';
		$response['msg'] = 'Failed to chnaged.';
	}
	
     print_r(json_encode($response,JSON_UNESCAPED_SLASHES));
}

/*
By:Jyoti Vishwakarma
Description: Promotional banner list based on kitchen
*/
if($_GET['service_name'] == 'promotionbanner')
{
	$json = file_get_contents('php://input');
 	$json = json_decode($json,true);
 	$response = array();
	$kitchenid = $json['kitchenid'];  

	$data = array();
	$data['banner'] = array();
	$query = "SELECT * FROM promotionbanner WHERE promoted_by = '$kitchenid' ";	
	$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
	
	while ($row = mysqli_fetch_assoc($result)) {
		$categories = array();
		$categories['id'] = $row['id'];
		$categories['promoted_to'] = $row['promoted_to'];
		$categories['image'] = $row['img'];
		$promotedto = $row['promoted_to'];
		$queryK = "SELECT * FROM user WHERE userid = '$promotedto' ORDER BY userid DESC";	
		$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
		$rowK = mysqli_fetch_assoc($resultK);
		$categories['name'] = $rowK['name'];
		$categories['tagline'] = $rowK['tagline'];

		$newTitle = $rowK['name'];
		$newTitle1 = trim($newTitle);
		$newTitle1= preg_replace('/\s+/', '-', $newTitle1);
		$categories['name1'] = $newTitle1;

		array_push($data['banner'], $categories);
	}	
	
	print_r(json_encode($data));
}

/*
By:Jyoti Vishwakarma
Description: Repeat order of cuisines
*/
if($_GET['service_name'] == 'repeatOrder')
{
 	
    $json = file_get_contents('php://input');
 	$json = json_decode($json,true);
 	$response = array();
 	$orderid = $json['orderid'];  
	$userid = $_SESSION['userid'];
	$queryuser = "SELECT cuisineid,quantity,selected_addons FROM casseroleorderitem WHERE cassordid='$orderid'";	
	$excuser = mysqli_query($conn,$queryuser)or die(mysqli_error($conn));
	$itemcount = mysqli_num_rows($excuser);
	if(mysqli_num_rows($excuser) > 0)
	{
		while ($row = mysqli_fetch_assoc($excuser)) {
			
			$productid = $row['cuisineid'];
			$quantity = $row['quantity'];
			$addon = "";
			if($row['selected_addons'] != ""){
				$addon = '';
				$selectedaddons = array_filter(explode(",",$row['selected_addons']));
				foreach ($selectedaddons as $value) {
				$split = explode("-",$value);
				$addonsid = $split[0];
				$addon .= $addonsid .",";
				}
			}else{
				$addon = "";
			}

			    $query = "SELECT * FROM cart WHERE product_id='$productid' AND userid='$userid'";
				$result = mysqli_query($conn,$query)or die(mysqli_error($conn));
				$catcount = mysqli_num_rows($result);
				if($catcount>0){

				} else {
					
					$queryadd = "INSERT INTO cart SET userid = '$userid',product_id = '$productid', quantity = '$quantity',selectedaddons='$addon',created_at=NOW()";
					$resultadd = mysqli_query($conn,$queryadd)or die(mysqli_error($conn));
					setcookie('cartcookie'.$val, $val,  time() - 3600, "/");
					setcookie('cookiecount'.$val, $val, time() - 3600, "/");					
				}

		}	
		$response['status'] = 'success';
		$response['cartcount'] = $itemcount;
		$response['msg'] = 'Order repeated.';
	}else{
		$response['status'] = 'failed';
		$response['msg'] = 'Order repeated failed.';
	}
		
     print_r(json_encode($response,JSON_UNESCAPED_SLASHES));
}

/*
By:Jyoti Vishwakarma
Description: Kitchen page  details
*/
if($_GET['service_name'] == 'kitchen_page1')
{
	$req = file_get_contents('php://input');
	$json = json_decode($req,true);
	$kitchenid = $json['kitchenid'];
	$keyword = $json['keyword'];
	$categories = $json['categories'];
	$sortbycost = $json['sortbycost'];
	$type = $json['type'];
	$mealtype = $json['mealtype'];

	$queryK = "SELECT * FROM user WHERE userid = '$kitchenid' ORDER BY userid DESC";	
	$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
	$rowK = mysqli_fetch_assoc($resultK);
	$data = array();
	$data['kitchen'] = array();
	$data['categories'] = array();
	$kitchens = array();
	$id = $rowK['userid'];
	$kitchens['id'] = $rowK['userid'];
	$kitchens['name'] = $rowK['name'];
	$kitchens['tagline'] = $rowK['tagline'];
	$kitchens['type'] = $rowK['kitchen_type'];
	$kitchens['delivery_time'] = $rowK['delivery_time'];
	if ($rowK['profilepic']!="") {
		$image_name = explode("/",$rowK['profilepic']);
		$image_name = array_pop($image_name);
		$kitchens['image'] = S3URL.$image_name;
	}
	else
	{
		$kitchens['image'] = "https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png";
	}
	
	if($rowK['category'] !=""){
		$category = $rowK['category'];
	}else{
		$category='';
	}

	$corporateid = '';
	if(isset($_SESSION['corporateid'])){
		$corporateid = $_SESSION['corporateid'];

	}

	$querySq = "SELECT * FROM `kitchen_cuisinetype_sequence` WHERE kitchen_id = '$id' ORDER BY sequence ASC";	
	$resultsq = mysqli_query($conn,$querySq)or die(mysqli_error($conn));
	$seq = array();
	while($rowSq = mysqli_fetch_assoc($resultsq)) {
		$seq[count($seq)] = $rowSq['cuisinetypeid'];
	}
	$catseq = implode(",",$seq);


	$queryR = "SELECT count(id) as count, rating FROM chefrating WHERE chefId = '$id' GROUP BY rating";	
	$resultR = mysqli_query($conn,$queryR)or die(mysqli_error($conn));
	$rating1 = 0;
	$rating2 = 0;
	$rating3 = 0;
	$rating4 = 0;
	$rating5 = 0;
	while ($rowR = mysqli_fetch_assoc($resultR)) {
		$rating = $rowR['rating'];
		if ($rating==1) {
			$rating1 = (int)$rowR['count'];
		}
		if ($rating==2) {
			$rating2 = (int)$rowR['count'];
		}
		if ($rating==3) {
			$rating3 = (int)$rowR['count'];
		}
		if ($rating==4) {
			$rating4 = (int)$rowR['count'];
		}
		if ($rating==5) {
			$rating5 = (int)$rowR['count'];
		}
	}
	$ratingTotal = $rating5+$rating4+$rating3+$rating2+$rating1;
	if ($ratingTotal>0) {
		$rating = (5*$rating5 + 4*$rating4 + 3*$rating3 + 2*$rating2 + 1*$rating1) / ($rating5+$rating4+$rating3+$rating2+$rating1);
	}
	else
	{
		$rating = 0;
	}
	$kitchens['ratingcount'] = mysqli_num_rows($resultR);		
	$kitchens['rating'] = round($rating);		
	array_push($data['kitchen'], $kitchens);

	$kitchens['category'] = array();

	if($mealtype == "corporate_meals"){
		$mtcondition = "AND isCorporateMeal='0' AND isEventMeal='0'";
		$pagetype ='2';
	}else if($mealtype == "team_meals"){
		$mtcondition = "AND isCorporateMeal='1' AND isEventMeal='0'";
		$pagetype ='3';
	}else if($mealtype == "event_meals"){
		$mtcondition = "AND isCorporateMeal='0' AND isEventMeal='1'";
		$pagetype ='4';
	}else if($mealtype == "party_meals"){
		$mtcondition = "AND isCorporateMeal='3' AND isEventMeal='0'";
		 $pagetype ='5';
	}else if($mealtype == "cafeteria"){
		$mtcondition = "AND isCorporateMeal='4' AND isEventMeal='0'";
		$pagetype ='6';
	}
	
	$corporate_condition = "";
	if(isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] =='0'){
		$corporate_id = $_SESSION['corporateid'];
		$corporate_condition =  " page_type = '$pagetype' AND FIND_IN_SET($corporate_id, corporateid)";
	}else if($pagetype == 5){
		$corporate_condition =  " page_type = '$pagetype'";
	}else{
		$corporate_condition ="cuisinetypeid IN ($category) ORDER BY FIELD(cuisinetypeid, $catseq)";
	}

	echo $corporate_condition;
	if($category != "" and $catseq != ""){
	 $queryC = "SELECT * FROM `cuisinetypemaster` WHERE $corporate_condition ";
		$resultC = mysqli_query($conn,$queryC)or die(mysqli_error($conn));
	
	$data['cuisines'] = array();
		while($rowC = mysqli_fetch_assoc($resultC)) {
			
			$cattemp = array();
			$cattemp['categoryid'] = $rowC['cuisinetypeid'];
			$cattemp['categoryname'] = $rowC['cuisinetype'];

			array_push($data['categories'], $cattemp);


			$catgorytemp = $rowC['cuisinetypeid'];
			
			// echo "<br>";
			$orderby = "ORDER BY cuisineid DESC";
			$condition = "";
			if ($keyword!="") {
				$condition .= "AND cuisinename LIKE '%$keyword%'";
			}
			if($type!="")
			{
				$condition .= "AND type LIKE '$type%'";
			}
			if($sortbycost!="")
			{
				$orderby = "ORDER BY price $sortbycost";
			}
			

			$corporate_condition = "";
			if(isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] =='1'){
				$corporate_id = $_SESSION['corporateid'];
				$corporate_condition =  "AND homepagecatid = '$catgorytemp' AND FIND_IN_SET($corporate_id, invitecode)";
			}else{
				$corporate_condition =  "AND cuisinetypeId = '$catgorytemp'";
			}

			$query1 = "SELECT * FROM cuisine WHERE isActive = 'Y' AND userid='$kitchenid'  $corporate_condition $mtcondition $condition $orderby";	 
			$result1 = mysqli_query($conn,$query1)or die(mysqli_error($conn));
			$count = mysqli_num_rows($result1);
			if ($count==0) {
				continue;
			}	
			$cattemp['cuisines'] = array();
			while ($row = mysqli_fetch_assoc($result1)) {
				$cuisine = array();
				$id = $row['cuisineid'];
				$cuisine['id'] = $row['cuisineid'];
				$cuisine['name'] = $row['cuisinename'];
				// $cuisine['description'] = $row['cuisinedescription'];
				$cuisine['description'] = $row['cuisinedescription'];
				if($row['cuisinedescription'] != ""){
					$desc1 = strip_tags($row['cuisinedescription']);
					if(strlen($desc1) > 99){
						$cuisine['description'] = substr($desc1,0,100)."...";
					}else{
						$cuisine['description'] = substr($desc1,0,100);
					}
					
					
				}
				$cuisine['price'] = $row['price'];
				$cuisine['serves'] = $row['servingdetail'];
				$cuisine['type'] = $row['type'];
				$cuisine['minorderquantity'] = $row['minorderquantity'];
				$cuisine['leadtime'] = $row['leadtime'];

				$userid = $row['userid'];

				$queryK = "SELECT * FROM user WHERE userid = '$userid'";	
				$resultK = mysqli_query($conn,$queryK)or die(mysqli_error($conn));
				$rowK = mysqli_fetch_assoc($resultK);
				$cuisine['kitchen_id'] = isset($rowK['userid']) ? $rowK['userid'] : "";
				$cuisine['kitchen_name'] = isset($rowK['name']) ? $rowK['name'] : "";

				if ($row['cuisinepic']!="") {
					$image_name = explode("/",$row['cuisinepic']);
					$image_name = array_pop($image_name);
					$cuisine['image'] = S3URL.$image_name;
				}
				else
				{
					$cuisine['image'] = "https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png";
				}
				
				$cuisine['isCustomisable']='0';
				$query2 = "SELECT * FROM corporate_cuisine_sections WHERE cuisine_id='$id'";
				$result2 = mysqli_query($conn,$query2) or die(mysqli_error($conn));
				if(mysqli_num_rows($result2) > 0 ){
					$cuisine['isCustomisable']='1';
				}


				if($corporateid != ''){
					$queryO = "SELECT offer FROM corporateoffermapping WHERE restaurantid = '$userid' AND corporateid='$corporateid'";	
					$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
					$rowO = mysqli_fetch_assoc($resultO);
					$cuisine['offer'] = isset($rowO['offer']) ? $rowO['offer'] : "";
				}else{
					$cuisine['offer']='';
				}
				
				array_push($cattemp['cuisines'], $cuisine);
			}
			array_push($data['cuisines'], $cattemp);
		}	
	}
	print_r(json_encode($data));
}

/*
By:Jyoti Vishwakarma
Description: Add to cart recommanded cuisines
*/
if($_GET['service_name'] == 'addtocartrecommanded')
{
	$date = date('Y-m-d H:i:s');
	$data = file_get_contents('php://input');
	$json = json_decode($data, true);
	$recommandedid = $json['recommandedid'];

	if(isset($_SESSION['userid']))
	{
		$userid = $_SESSION['userid'];

		$query = "SELECT * FROM cart WHERE product_id='$id' AND userid='$userid'";
		$result = mysqli_query($conn,$query)or die(mysqli_error($conn));

		$catcount = mysqli_num_rows($result);
		$status = array();
		if($catcount>0){
			$status['status']='exist';
		} else {
			$data = array(
				'product_id' => $id,
				'quantity' => $count,
				'userid' => $_SESSION['userid'],
			);
			$queryadd = "INSERT INTO cart SET userid = '$userid',product_id = '$id', quantity = '$count',selectedaddons='$selectedaddons',created_at=NOW()";
			$resultadd = mysqli_query($conn,$queryadd)or die(mysqli_error($conn));
			if($resultadd)
			{
				$status["status"]="success";
			}
			else{
				$status["status"]="failed";
			}
		}
		print_r(json_encode($status));
	}
	else
	{
		$status = array();
		$cookierecommanded = "cookierecommanded".$id;
		if(isset($_COOKIE[$cookierecommanded]))
		{
			$status['status']="exist";
		}
		else
		{
			$status['status']="success";
			setcookie($cookierecommanded, $cookierecommanded, time() + (86400 * 30), "/"); // 86400 = 1 day
		}
		print_r(json_encode($status));
	}
}

/*
By:Jyoti Vishwakarma
Description: Cancel order
*/
if($_GET['service_name'] == 'cancelOrder')
{
 	
    $json = file_get_contents('php://input');
 	$json = json_decode($json,true);
 	$response = array();
 	$orderid = $json['orderid'];  
	 
	$queryuser = "SELECT * FROM casseroleorder WHERE orderstatus < '3' and cassordid='$orderid'";	
	$excuser = mysqli_query($conn,$queryuser)or die(mysqli_error($conn));
	if(mysqli_num_rows($excuser) > 0)
	{

		$update = "UPDATE `casseroleorder` SET orderstatus = '4' WHERE cassordid='$orderid'";
		$excupdate = mysqli_query($conn,$update) or die(mysqli_error($conn));

		$update = "UPDATE `pushnotiorder` SET status = '1' WHERE orderid='$orderid'";
		$excupdate = mysqli_query($conn,$update) or die(mysqli_error($conn));

		$response['status'] = 'success';
		$response['msg'] = 'Order cancelled.';
	}else{
		$response['status'] = 'failed';
		$response['msg'] = 'Order cannot cancelled.';
	}
		
     print_r(json_encode($response,JSON_UNESCAPED_SLASHES));
}

/*
By:Jyoti Vishwakarma
Description: Save details of get in touch form
*/
if($_GET['service_name'] == 'getInTouch')
{
 	
    $json = file_get_contents('php://input');
 	$json = json_decode($json,true);
 	
 	$name = $json['name'];  
 	$email = $json['email'];  
 	$phone = $json['phone'];  
	$message = $json['message'];  
	$today = date("Y-m-d H:i:s"); 

 	$insf = "INSERT INTO getintouch SET name='".$name."',email='".$email."',phone='".$phone."',message='".$message."',createdat='$today'";
 	$excf = mysqli_query($conn,$insf) or die(mysqli_error($conn));
 	if ($excf) {
 		$response['status'] = 'success';
		$response['msg'] = 'Form submitted successfully.';
 	}else{
		$response['status'] = 'failure';
		$response['msg'] = 'Something went wrong.';
	 }
 
     print_r(json_encode($response,JSON_UNESCAPED_SLASHES));
}  

/*
By:Jyoti Vishwakarma
Description: Verify coupon code
*/

if($_GET['service_name'] == 'verifcoupon')
{
	$reqvc = file_get_contents('php://input');

	$resvc = json_decode($reqvc,true);

	$userid = '';
	$couponcode = '';
	$invitecode = '';

	$userid = $resvc['userId'];
	$couponcode = $resvc['couponcode'];
	$invitecode = 9999;//$resvc['invitecode'];
	$date = date('Y-m-d');


	$vc = array();

	$quecc = "SELECT * FROM coupon WHERE couponcode='".$couponcode."' and enddate >= '$date'";
	$exchc = mysqli_query($conn,$quecc) or die(mysqli_error($conn));
	$rschk = mysqli_fetch_assoc($exchc);

	if(mysqli_num_rows($exchc) > 0)
	{
		$queucm = "SELECT * FROM mapcouponuser WHERE couponid='".$rschk['couponid']."' AND userid='".$userid."' AND invitecode='".$invitecode."'";

		$excucm = mysqli_query($conn,$queucm) or die(mysqli_error($conn));
		//mysqli_num_rows($excucm) > 0
		if(0)
		{
			$vc['status'] = 'failure';
			$vc['msg'] = 'Coupon is Already Used';
		}
		else
		{
			$insuc = "INSERT INTO mapcouponuser SET couponid='".$rschk['couponid']."',userid='".$userid."',invitecode='".$invitecode."'";

			$exmcu = mysqli_query($conn,$insuc) or die(mysqli_error($conn));

			$rsmcu = mysqli_insert_id($conn);

			if(count($exmcu) != 0) //
			{
				//select * from mapcouponuser as mc,coupon as c where c.couponid=mc.couponid
				$queguc = "SELECT mc.couponid,c.discount,mc.couponuserid FROM mapcouponuser as mc,coupon as c where c.couponid=mc.couponid and mc.couponuserid='".$rsmcu."'";
				$excguc = mysqli_query($conn,$queguc) or die(mysqli_error($conn));
				$rsgcu = mysqli_fetch_assoc($excguc);

				$vc['couponid'] = $rsgcu['couponid'];
				$vc['discount'] = $rsgcu['discount'];
				$vc['couponuserid'] = $rsgcu['couponuserid'];
				$vc['msg'] = 'Coupon Code Applied with Discount '.$rsgcu['discount'].'%';
				$vc['status'] = 'success';
			}
			else
			{
				$vc['status'] = 'failure';
				$vc['msg'] = 'Not Able To Apply Coupon Code';
			}	
		}	

	}
	else
	{
		$vc['status'] = 'failure';
		$vc['msg'] = 'Coupon Code is Wrong';
	}	

	print_r(json_encode($vc,JSON_UNESCAPED_SLASHES));
	exit;

}