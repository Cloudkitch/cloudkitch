<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
			include 'head.php';
		?>
        <title>Sitemap | Cloudkitch</title>
        <meta name="description" content="">
        <style>
            footer{display:none;}
        </style>
    </head>
<body class="servicesPage">
    <?php
        include 'header.php';
    ?>
    <section class="topSection section section-services">
        <h2 class="setionTitle">Sitemap</h2>
        <div class="flexDiv">
            <div class="leftLinks">
                <ul>
                    <li>
                        <h2>General Pages</h2>
                    </li>
                    <li>
                        <a href="<?= $baseurl; ?>partners/"><p>About Us</p></a>
                    </li>
                    <li>
                        <a href="<?= $baseurl; ?>partners/"><p>Partner with us</p></a>
                    </li>
                    <li>
                        <a href="<?= $baseurl; ?>partners/#footer"><p>Contact Us</p></a>
                    </li>
                </ul>
            </div>
            <div class="rightLinks">
                <ul id="siteMapResto">
                    <li>
                        <h2>Your Favourite Restaurants</h2>
                    </li>
                   
                    </ul>
                <ul>
                    <li>
                        <h2>Other Pages</h2>
                    </li>
                    <li>
                        <a href="<?= $baseurl; ?>terms-and-conditions/"><p>Terms & Conditions</p></a>
                    </li>
                    <li>
                        <a href="<?= $baseurl; ?>grievance-policy/"><p>Grievance Policy</p></a>
                    </li>
                    <li>
                        <a href="<?= $baseurl; ?>cookie-policy/"><p>Cookie Policy</p></a>
                    </li>
                    <li>
                        <a href="<?= $baseurl; ?>cancellation-and-refund/"><p>Cancellation & Refund</p></a>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <?php
        include 'footer.php';
    ?>
</body>
</html>