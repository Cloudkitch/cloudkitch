<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
			include 'head.php';
		?>
        <title>Cloudkitch | Profile</title>
        <meta name="description" content="CloudKitch introduces smart kitchens that are connected with innovative technologies which are quintessential to bring success to any restaurant.">
    </head>
    <body class="servicesPage">
        <?php
            include 'header.php';
        ?>
        <section class="topSection profileSection">
            <div class="userInfo">
                <div class="card">
                    <div class="upload-btn-wrapper" id="user_pic" style="background-image:url('<?=$baseurl?>images/download.jpg')">
                        <input type="file" name="myfile">
                        <div class="editProfileImg">
                           <input type="file" id="profilePicture" onchange="updateProflePic(this)" style="display: none" />
                           <p  onclick="$('#profilePicture').click();">Edit </p>
                        </div>
                    </div>
                    <h2 id="user_name">John Smith</h2>
                    <p id="user_email">john@gmail.com</p>
                    <p id="user_phone">+91 8974674670</p>
                </div>
            </div>
            <div class="userDetails">
                <div class="card">
                    <form id="profileForm" class="signform form-signup">
                        <div class="flexBlock">
                            <h2>Personal Details</h2><p onclick="makeFormEditable()"><img src="<?=$baseurl;?>images/icons/edit.svg" alt="edit">Edit</p>
                        </div>
                        <div class="form-conatiner">
                            <div class="form-group form-float-group">
                                <input type="text"  id="profile-fullname" name="profile-fullname" class="form-float-control">
                                <label for="profile-fullname" class="form-float-lab">Your Full Name</label>
                            </div>
                            <div class="form-group form-float-group">
                                <input type="text" placeholder="" id="profile-phone" name="profile-phone" class="form-float-control" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" onpast="return false">
                                <label for="profile-phone" class="form-float-lab">Your Phone Number</label>
                            </div>
                            <div class="form-group form-float-group">
                                <input type="email" placeholder="" id="profile-email" name="profile-email" class="form-float-control">
                                <label for="profile-email" class="form-float-lab">Your Email Address</label>
                            </div>
                           
                            <a href="javascript:;" class="btn-gradient btnSignUp" onclick="updateProfileDetails()" disabled="true">Submit</a>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <?php
            include 'footer.php';
        ?>
        <script>
            $(document).ready(function(){
                getUserDetails();
                activeChoosedMealType(); 
                $('#profileForm input').attr('readonly', 'readonly');
                $(".btnSignUp").attr("disabled",true);
                $('.btnSignUp').css("pointer-events", "none");
            });

    /*
    By:Jyoti Vishwakarma
    Description: get user details
    */
            function getUserDetails(){
               
                $.ajax({
                    url: serviceurl + 'getUserDetails',
                    type: 'POST',
                    async: false,
                    success: function(data)
                    {
                        var result = JSON.parse(data);
                        $("#id").val(result.id);
                        $("#user_name").html(result.name);
                        $("#user_email").html(result.email);
                        if(result.phone != ""){
                            $("#user_phone").html("+91 "+result.phone);
                        }else{
                            $("#user_phone").html("");
                        }
                        $(".form-group").addClass("form-setfocus");
                        $("#profile-fullname").val(result.name);
                        $("#profile-phone").val(result.phone);
                        $("#profile-email").val(result.email);
                        // $("#user_pic").attr("src", imgurl+result.profilepic);
                        if(result.profilepic != ""){
                            $('#user_pic').css('background-image', 'url("' + result.profilepic + '")');
                        }else{
                            $('#user_pic').css('background-image', 'url(<?=$baseurl?>"images/download.jpg)');
                            
                        }
                        
                    }
                });
            }
                /*
    By:Jyoti Vishwakarma
    Description: Make user details form editable
    */
            function makeFormEditable(){
                $('.form-group input').css('background-color','#f5f5f5');
                $('#profileForm input').removeAttr('readonly');
                $(".btnSignUp").attr("disabled",false);
                $('.btnSignUp').removeAttr("style");
            }
    /*
    By:Jyoti Vishwakarma
    Description: Upload profile picture
    */
            function updateProflePic(input){
                // alert("here");
                // var image = $("#profilePicture").attr("src");


                if (input.files) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        var image = event.target.result;
                        var pagedata = {
                            "image": image
                        };
                        $.ajax({
                            url: serviceurl + 'updateProfilePic',
                            type: 'POST',
                            data: JSON.stringify(pagedata),
                            datatype: 'JSON',
                            async: false,
                            success: function(data) {
                                var value = JSON.parse(data);
                                if (value.status == 'success') {
                                    $('#user_pic').css('background-image', 'url("' + value.propic + '")');

                                } else {
                                    alert("not update successfull");
                                }
                            }
                        });
                    }
                    reader.readAsDataURL(input.files[0]);
                
                }
            }
    /*
    By:Jyoti Vishwakarma
    Description: Update user details
    */
            function updateProfileDetails(){
                
                var fullname = $("#profile-fullname").val();
                var email = $("#profile-email").val();
                var phone = $("#profile-phone").val();  
                var pagedata = {
                    "fullname": fullname,
                    "email":email,
                    "phone":phone
                };
                $.ajax({
                    url: serviceurl + 'updateProfileDetails',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        if (value.status == 'success') {
                            $('.overlay,.profileUpdateAlertMsg').fadeIn();
                            setTimeout(function() {
                                $('.overlay,.profileUpdateAlertMsg').fadeOut();
                            }, 2000);
                           
                            getUserDetails();
                            $('#profileForm input').attr('readonly', 'readonly');
                            $('#profileForm input').attr('style','');
 
                        } else {
                            alert("not update successfull");
                        }
                    }
                });
               
            }

          

        </script>
    </body>
</html>
