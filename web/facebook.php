<?php
include 'inc/databaseConfig.php';
if (isset($_GET['user_id']) && isset($_GET['order_id'])) {
    $show_review = "1";
    $user_id = $_GET['user_id'];
    $order_id = $_GET['order_id'];
} else {
    $show_review = "0";
    $user_id = "";
    $order_id = "";
}

// <?php header("Location:web/cloudkitchadmin/"); 


?>
<!DOCTYPE html>
<html>

<head>
    <?php
    include 'head.php';
    // echo $baseurl;

    ?>
    <title>Cloudkitch</title>
    <meta name="description" content="CloudKitch introduces smart kitchens that are connected with innovative technologies which are quintessential to bring success to any restaurant.">
    <meta name="google-signin-client_id" content="479429176998-7mpkolsspal42s254devm757gmkk59r2.apps.googleusercontent.com">
</head>

<body>
<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>
<script>

        function statusChangeCallback(response) {  // Called with the results from FB.getLoginStatus().
        console.log('statusChangeCallback');
        console.log(response);                   // The current login status of the person.
        if (response.status === 'connected') {   // Logged into your webpage and Facebook.
            testAPI();  
        } else {                                 // Not logged into your webpage or we are unable to tell.
            // document.getElementById('status').innerHTML = 'Please log ' +
            // 'into this webpage.';
        }
        }


        function checkLoginState() {               // Called when a person is finished with the Login Button.
        FB.getLoginStatus(function(response) {   // See the onlogin handler
            statusChangeCallback(response);
        });
        }


        window.fbAsyncInit = function() {
        FB.init({
            appId      : '2646941222205273',
            xfbml      : true,
            version    : 'v5.0'        // Use this Graph API version for this call.
        });


        FB.getLoginStatus(function(response) {   // Called after the JS SDK has been initialized.
            statusChangeCallback(response);        // Returns the login status.
        });
        };


        (function(d, s, id) {                      // Load the SDK asynchronously
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));


        function testAPI() {                      // Testing Graph API after login.  See statusChangeCallback() for when this call is made.
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me?fields=id,name,email,picture{url}', function(response) {
            console.log('Successful login for: ' + response.name);
            console.log('Successful login for: ' + response.email);
            var picurl = "";
            var picres = response.picture;
            // console.log(picres);

            var data = picres.data;
            // console.log(data);

            picurl = data.url;
            console.log(picurl);

           
            // document.getElementById('status').innerHTML =
            // 'Thanks for logging in, ' + response.name + '!';
            var email  = response.email;
            var pagedata = {
                "email": email
            };
            var userid = '<?php if (isset($_SESSION['userid'])) {  echo $_SESSION['userid'];    } ?>';
            if (userid == "") {
                $.ajax({
                    url: serviceurl + 'isUserExist',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        if (value.status == 'new') {
                            $('.first-login').fadeOut();
                            $('.corporate-login').fadeIn();
                            $("#google_id").val('');
                            $("#google_name").val(response.name);
                            $("#google_imgurl").val(picurl);
                            $("#google_email").val(email);
                            $("#social_type").val('facebook');
                        } else {
                            if (value.isCorporateUser == '1') {
                                window.location.href = "corporate.php";
                            } else {
                                location.reload();
                            }
                        }
                    }
                });
            }






        });
        }

</script>

<script>



window.fbAsyncInit = function() {
      FB.init({
          appId      : '2646941222205273',
          xfbml      : true,
          version    : 'v2.2'
      });
      FB.getLoginStatus(function(response){
          if(response.status === 'connected'){
              document.getElementById('status').innerHTML = 'we are connected';
              getInfo()
          } else if(response.status === 'not_authorized') {
               document.getElementById('status').innerHTML = 'we are not logged in.'
          } else {
              document.getElementById('status').innerHTML = 'you are not logged in to Facebook';
          }
      });
  // FB.AppEvents.logPageView();
  };

  (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  function login(){
      FB.login(function(response){
          console.log(response);
          if(response.status === 'connected'){
              document.getElementById('status').innerHTML = 'we are connected';
              getInfo();
          } else if(response.status === 'not_authorized') {
               document.getElementById('status').innerHTML = 'we are not logged in.'
          } else {
              document.getElementById('status').innerHTML = 'you are not logged in to Facebook';
          }

      });
  }
  // get user basic info

  function getInfo() {
    //   FB.api('/me', 'GET', {fields: 'first_name,last_name,name,id,email'}, function(response) {
    //     console.log(response);  
    //     console.log('Successful login for: ' + response.name);
    //         console.log('Successful login for: ' + response.email);
    //     document.getElementById('status').innerHTML = response;
    //   });
    // console.log('Welcome!  Fetching your information.... ');
    //     FB.api('/me?fields=id,name,email,picture{url}', function(response) {
    //         console.log('Successful login for: ' + response.name);
    //         console.log('Successful login for: ' + response.email);
    //         var picurl = "";
    //         var picres = response.picture;
    //         // console.log(picres);

    //         var data = picres.data;
    //         // console.log(data);

    //         picurl = data.url;
    //         console.log(picurl);

           
    //         // document.getElementById('status').innerHTML =
    //         // 'Thanks for logging in, ' + response.name + '!';
    //         var email  = response.email;
    //         var pagedata = {
    //             "email": email
    //         };
    //         var userid = '<?php if (isset($_SESSION['userid'])) {  echo $_SESSION['userid'];    } ?>';
    //         if (userid == "") {
    //             $.ajax({
    //                 url: serviceurl + 'isUserExist',
    //                 type: 'POST',
    //                 data: JSON.stringify(pagedata),
    //                 datatype: 'JSON',
    //                 async: false,
    //                 success: function(data) {
    //                     var value = JSON.parse(data);
    //                     if (value.status == 'new') {
    //                         $('.first-login').fadeOut();
    //                         $('.corporate-login').fadeIn();
    //                         $("#google_id").val('');
    //                         $("#google_name").val(response.name);
    //                         $("#google_imgurl").val(picurl);
    //                         $("#google_email").val(email);
    //                         $("#social_type").val('facebook');
    //                     } else {
    //                         if (value.isCorporateUser == '1') {
    //                             window.location.href = "corporate.php";
    //                         } else {
    //                             location.reload();
    //                         }
    //                     }
    //                 }
    //             });
    //         }
    //     });

    FB.api('/me', {locale: 'en_US', fields: 'id,name,first_name,last_name,email,link,gender,locale,picture'},
        function (response) {
            console.log('Successful login for: ' + response.name);
            console.log('Successful login for: ' + response.email);
    }
    );
  }

  function logout(){
      FB.logout(function(response) {
          document.location.reload();
      });
  }


</script>
    <?php
    include 'header.php';
    ?>
    <div id="status"></div>

    <section class="banner topSection">
        <div class="homeSlider">
            <?php
            $query = "SELECT * FROM banners";
            $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
            while($row = mysqli_fetch_assoc($result))
            {
                ?>
                <div class="homeSlide">
                    <img src="<?=$row['image']?>" alt="">
                </div>
                <?php
            }
            ?>
        </div>
    </section>
    <section class="section">

        <h2>Restaurants</h2>
        <p>Restaurants to Choose From</p>
        <div class="restroWrap homerestroSlider" id="kitchens">
        </div>
    </section>
    <!-- <section class="orderSection">
            <div class="orderWrap">
                <div class="orderBlock card">
                    <div class="orderDetails">
                        <h2>Repeat</h2>
                        <p>My Last Order</p>
                    </div>
                    <div class="orderedItems">
                        <ul>
                            <li><p>Chole Kulcha  X2</p></li>
                            <li><p>Rumali Roti x4</p></li>
                        </ul>
                    </div>
                    <div class="itemCount">
                        <p>+4 items</p>
                    </div>
                </div>
                <div class="orderBlock card">
                    <div class="orderDetails">
                        <h2>Connect with us</h2>
                        <p>+91 771 099 9666</p>
                    </div>
                    <div class="orderedItems getApp">
                        <p>GET OUR <br><span>APP</span> today</p>
                        <p><a href="tel:917710999666">Call<br>Now</a></p>
                    </div>
                </div>
            </div>
        </section> -->
    <section class="section">
        <div class="filterWrap">
            <div class="titleWrap">
                <h2>Popular Cuisines</h2>
                <p>Courses to Choose From</p>
            </div>
            <div class="filterBtn"></div>
            <div class="filterContainer">
                <div class="searchWrap menuSearch">
                    <form>
                        <input type="text" id="search_inner" onkeyup="getCuisinesFilter()" placeholder="Search Hot Favourites">
                    </form>
                </div>
                <!-- <div class="buttonWrap">
                        <p class="btn greyBorderBtn"><img src="<?= $baseurl; ?>images/icons/download-arrow.svg" alt="download">SORT BY COST</p>
                        <p class="btn greyBorderBtn"><img src="<?= $baseurl; ?>images/icons/leaf.svg" alt="download">PURE VEG</p>
                        <p class="btn borderBtn"><img src="<?= $baseurl; ?>images/icons/discount.svg" alt="download">DELIGHTFUL OFFERS</p>
                    </div> -->
                <div class="buttonWrap">
                    <!-- <div class="buttonWrap btn greyBorderBtn">
                            <select class="custSelect costselect" id="sort_by_cost" name="sort_by_cost" onchange="getCuisinesFilter()">
                                <option class="sort by cost" value="">SORT BY COST</option>
                                <option class="High To Low" value="DESC">High To Low</option>
                                <option class="Low To High" value="ASC">Low To High</option>
                            </select>
                        </div> -->
                    <div class="filter-check">
                        <input class="custom-radio" onchange="getCuisinesFilter()" type="checkbox" id="checkbox_veg" name="checkbox_veg">
                        <label for="checkbox_veg" class="btn radio-label">
                            <img src="<?= $baseurl; ?>images/icons/leaf.svg" alt="download">VEG
                        </label>
                    </div>
                    <!-- <p class="btn borderBtn"><img src="<?= $baseurl; ?>images/icons/discount.svg" alt="download">DELIGHTFUL OFFERS</p> -->
                </div>
            </div>
        </div>
        <div class="menuWrap">
            <div class="cuisineWrap card">
                <p class="closeFilter">Close X</p>
                <h3><img src="<?= $baseurl; ?>images/icons/cuisine.svg" alt="Course">Course</h3>
                <div class="checkWrap" id="categories">
                    <!-- <div class="checkboxWrap">
                            <label class="container">American
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>
                        </div> -->
                </div>
            </div>
            <div class="cuisineWrapper">
                <div class="cuisineContainer">
                    <div id="cuisines">
                    <!-- <div class="cuisine card">
                            <div class="cuisineImgWrap">
                                <img src="?=$baseurl;?>images/cuisine/1.jpg" alt="Cuisine">
                            </div>
                            <div class="cuisineData">
                                <h2 class="cuisineTitle">Indori Poha</h2>
                                <div class="cuisineType">
                                    <p><img src="?=$baseurl;?>images/icons/veg.svg" alt="Veg">Market Bistro Kitchen</p>
                                </div>
                                <div class="priceBlock">
                                    <p class="price">&#8377; 35</p>
                                    <p>Serves 2</p>
                                </div>
                                // Latest UI
                                <div class="quantityBlock">
                                    <div class="hurryBlock">
                                        <p class="disc">10% off</p>
                                        <p class="state">New</p>
                                    </div>
                                    <div class="addBtn">
                                        <p class="addToCart"><img src="<?= $baseurl; ?>images/icons/addCart.svg" alt="download">ADD to cart</p>
                                    </div>
                                    <div class="order-quantity cuisineQty">
                                        <a class="quantity-icon sub"><h2>-</h2></a>
                                        <input type="text" class="quantity-input" value="1" min="0" max="3" readonly>
                                        <a class="quantity-icon add active"><h2>+</h2></a>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                <span data-value="1" id="loadmoredata" class="loadmore" style="border: none;"> </span>
                </div>
            </div>
        </div>
        <?php
        if (isset($_COOKIE['pincode'])) {
            $display = 'none;';
        } else {
            $display = 'block;';
        }
        ?>
        <div class="overlay" style="display:<?= $display ?>"></div>
        <div class="popup locationPopup" style="display:<?= $display ?>">
            <div class="popup-wrapper">
                <div class="centerText">
                    <img src="<?= $baseurl; ?>images/logo.svg" alt="CLOUDKITCH" class="popup-logo">
                </div>
                <div class="firstDiv">
                    <form id="pincode_form" name="pincode_form">
                        <h2>Welcome to Cloudkitch!</h2><br>
                        <div class="form-conatiner">
                            <div class="form-group form-float-group">
                                <input type="text" placeholder="" id="pincode" name="pincode" autocomplete="off" class="form-float-control">
                                <label for="pincode" class="form-float-lab">Enter area pincode here</label>
                            </div>
                            <button type="submit" class="btn-gradient">Proceed</button>
                        </div>
                    </form>
                    <p class="skip">Skip</p>
                </div>
                <div class="secDiv centerText" style="display:none;">
                    <h2>Your area is currently not serviceable.</h2>
                </div>
            </div>
        </div>
    </section>
    <?php
    include 'footer.php';
    ?>

    <script>


    </script>
    <script>
        $(document).ready(function() {
            getCategories();
            getKitchens();
            getCuisines();

            var show_review = '<?= $show_review ?>';
            if (show_review == '1') {
                $(".reviewPopup").css({
                    'display': 'block'
                });
                $(".locationPopup").css({
                    'display': 'none'
                });
            }

        });

        jQuery(
            function($)
            {
                $('.cuisineContainer').bind('scroll', function()
                    {
                        if($(this).scrollTop() + $(this).innerHeight()>=$(this)[0].scrollHeight)
                        {
                            var loadlimit = Number($("#loadmoredata").attr("data-value"));
                                    if ($("#checkbox_veg").prop("checked") == true) {
                                        var type = "veg";
                                    } else {
                                        var type = "";
                                    }
                                    $("#loadmoredata").attr("data-value",loadlimit+1);
                                    var pagedata = {
                                        "keyword": "",
                                        "categories": "",
                                        "sortbycost": "",
                                        "type": type,
                                        "limit": loadlimit
                                    };
                                    $.ajax({
                                        url: serviceurl + 'cuisines',
                                        type: 'POST',
                                        data: JSON.stringify(pagedata),
                                        datatype: 'JSON',
                                        async: false,
                                        success: function(data) {
                                            var value = JSON.parse(data);
                                            var html = "";
                                            if (value.cuisines.length > 0) {
                                                for (var i = 0; i < value.cuisines.length; i++) {
                                                    html += '<div class="cuisine card"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt=""></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><div class="cuisineType"><p>';
                                                    var cuisintype = value.cuisines[i].type.split(",");
                                                    if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                                        html += '<img src="images/icons/veg.svg" alt="Veg">';
                                                    } else {
                                                        html += '<img src="images/icons/nonveg.svg" alt="Veg">';
                                                    }
                                                    html += value.cuisines[i].kitchen_name + '</p></div><div class="priceBlock"><p class="price">&#8377; ' + value.cuisines[i].price + '</p><p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock"></div><div class="addBtn"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="' + imgurl + 'images/icons/addCart.svg" alt="download">ADD to cart</p></div></div></div></div>';
                                                    
                                                }
                                                if(value.cuisines.length < 30){
                                                    $("#loadmoredata").hide();
                                                }
                                                $("#cuisines").append(html);
                                                addedIncart();
                                                hoverCuisine();
                                            } else {
                                                $("#cuisines").addend("<p>No Cuisine Found.</p>");
                                                $("#loadmoredata").hide();
                                            }



                                        }
                                    });
                                }
                            })
                    }
            );

        $('#loadmoredata').click(function(){
            //do something
            //
            
        });

        // function getCategories(){
        //     $.ajax({
        //         url: serviceurl + 'kitchens',
        //         type: 'POST',
        //         async: false,
        //         success: function(data)
        //         {
        //             var value = JSON.parse(data);
        //             var html="";
        //             for(var i = 0; i < value.kitchens.length; i++){
        //                 var category = value.kitchens[i].name;
        //                 html += '<div class="checkboxWrap"><label class="container">'+ category +'<input type="checkbox" id="category" name="category[]" onchange="getCuisinesFilter()" value="'+ value.kitchens[i].id +'"><span class="checkmark"></span></label></div>';
        //             }
        //             $("#categories").html(html);
        //         }
        //     });
        // }
        function getCategories() {
            $.ajax({
                url: serviceurl + 'categories',
                type: 'POST',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";

                   
                    for (var i = 0; i < value.categories.length; i++) {
                        var id = value.categories[i].id;
                        var category = value.categories[i].category;
                            html += '<div class="checkboxWrap"><label class="container">' + category + '<input type="checkbox" id="category" name="category[]" onchange="getCuisinesFilter()" value="' + id + '"><span class="checkmark"></span></label></div>';
                        

                    }
                    $("#categories").html(html);
                }
            });
        }

        // function getKitchens(){
        //     $.ajax({
        //         url: serviceurl + 'categories',
        //         type: 'POST',
        //         async: false,
        //         success: function(data)
        //         {
        //             var value = JSON.parse(data);
        //             var html="";
        //             for(var i = 0; i < 6; i++){
        //                 html += '<div class="restro card"><a onclick="getCuisinesFilter()" href="javascript:void(0);"><div class="restroImgWrap"><img src="images/temp/'+(i+1)+'.jpg" alt="'+ value.categories[i].category +'"></div><h3>'+ value.categories[i].category +'</h3></a></div>';
        //             }
        //             $("#kitchens").html(html);
        //         }
        //     });
        // }
        function getKitchens() {

            $.ajax({
                url: serviceurl + 'kitchens',
                type: 'POST',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                    // var vl = value.kitchen.length;
                    for (var i = 0; i < 3; i++) {
                        html += '<div class="restro card"><a href="restro.php?id=' + value.kitchens[i].id + '"><div class="restroWrapContent"><img src="' + value.kitchens[i].image + '" alt="' + value.kitchens[i].name + '"><div class="context"><h3>' + value.kitchens[i].name + '</h3><p>' + value.kitchens[i].tagline + '</p><div class="ratingWrap"><ul><li><img src="<?= $baseurl; ?>images/icons/star.svg" alt=""></li><li><img src="<?= $baseurl; ?>images/icons/star.svg" alt=""></li><li><img src="<?= $baseurl; ?>images/icons/star.svg" alt=""></li><li><img src="<?= $baseurl; ?>images/icons/star.svg" alt=""></li><li><img src="<?= $baseurl; ?>images/icons/blank-star.svg" alt=""></li></ul></div></div></div></a></div>';
                        $("#kitchens").html(html);
                    }
                }
            });
        }

        function getCuisines() {
            var pagedata = {
                "keyword": "",
                "categories": "",
                "sortbycost": "",
                "type": ""
            };
            $.ajax({
                url: serviceurl + 'cuisines',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                    if (value.cuisines.length > 0) {
                        for (var i = 0; i < value.cuisines.length; i++) {
                            html += '<div class="cuisine card"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt=""></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><div class="cuisineType"><p>';
                            var cuisintype = value.cuisines[i].type.split(",");
                            if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                html += '<img src="images/icons/veg.svg" alt="Veg">';
                            } else {
                                html += '<img src="images/icons/nonveg.svg" alt="Veg">';
                            }
                            html += value.cuisines[i].kitchen_name + '</p></div><div class="priceBlock"><p class="price">&#8377; ' + value.cuisines[i].price + '</p><p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock"></div><div class="addBtn"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="' + imgurl + 'images/icons/addCart.svg" alt="download">ADD to cart</p></div></div></div></div>';
                        }
                        if(value.cuisines.length < 30){
                            $("#loadmoredata").hide();
                        }
                        $("#cuisines").html(html);
                        addedIncart();
                        hoverCuisine();
                    } else {
                        $("#cuisines").html("<p>No Cuisine Found.</p>");
                    }



                }
            });
        }


        function getCuisinesFilter() {
            var keyword = $("#search_inner").val();
            var sort_by_cost = $("#sort_by_cost").val();
            var keyLength = keyword.length;
            var type = "";
            if ($("#checkbox_veg").prop("checked") == true) {
                var type = "veg";
            } else {
                var type = "";
            }

            var categories = [];
            $.each($("input[name='category[]']:checked"), function() {
                categories.push("'" + $(this).val() + "'");
            });
            var categorydata = categories.join(",");

            var pagedata = {
                "keyword": keyword,
                "categories": categorydata,
                "sortbycost": sort_by_cost,
                "type": type
            };
            $(".loader").show();
            $.ajax({
                url: serviceurl + 'cuisines',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: true,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                    $(".loader").hide();
                    if (value.cuisines.length > 0) {
                        for (var i = 0; i < value.cuisines.length; i++) {
                            html += '<div class="cuisine card"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt=""></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><div class="cuisineType"><p>';
                            var cuisintype = value.cuisines[i].type.split(",");
                            
                            if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                html += '<img src="images/icons/veg.svg" alt="Veg">';
                            } else {
                                html += '<img src="images/icons/nonveg.svg" alt="Veg">';
                            }
                            html += value.cuisines[i].kitchen_name + '</p></div><div class="priceBlock"><p class="price">&#8377; ' + value.cuisines[i].price + '</p><p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock"></div><div class="addBtn"><p class="addToCart" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="' + imgurl + 'images/icons/addCart.svg" alt="download">ADD to cart</p></div></div></div></div>';
                        }
                        if(value.cuisines.length < 30){
                            $("#loadmoredata").hide();
                        }
                        $("#cuisines").html(html);
                        addedIncart();
                        hoverCuisine();
                    } else {
                        $("#cuisines").html("<p>No Cuisine Found.</p>");
                    }


                }
            });
        }

        $("form[name='review_form']").validate({
            rules: {
                user_id: "required",
                order_id: "required",
                rating: "required",
                comment: "required",
            },
            submitHandler: function(form) {
                var user_id = $("#user_id").val();
                var order_id = $("#order_id").val();
                var comment = $("#comment").val();
                var rating = $(".rating").val();

                var pagedata = {
                    "userId": user_id,
                    "orderid": order_id,
                    "feedback": comment,
                    "rating": rating
                };
                $.ajax({
                    url: serviceurl + 'getRating',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        if (value.status == 'success') {

                            $('#reviewPopup1').hide();
                        }

                    }
                });
            }
        });

        $( "#other" ).click(function() {
          //  alert("hello")
            //  $( "#target" ).click();
            checkLoginState();
        });
    </script>
    
</body>

</html>