<!DOCTYPE html>
<html>

<head>
    <?php
    include 'head.php';
    ?>
    <title>Search | Cloudkitch</title>
    <meta name="description" content="">
    <meta name="google-signin-client_id" content="287145767352-qi8asdmobui20671musqaq0jga2ru2re.apps.googleusercontent.com">

</head>

<body>
    <?php
    include 'header.php';
    ?>

<div class="popup popup-order addtocartpopup">
        <div class="popup-wrapper">
            <div class="popup-close">
                <img src="<?= $baseurl; ?>images/icons/close.svg" alt="close">
            </div>
            <div class="popup-item">
                <h2 class="cuisineTitle" id="cuisineTitle1">Veg Christmas Buffet B</h2>
                <input type="hidden" id="cuisine_id">
                <input type="hidden" id="kitchen_open">
                <input type="hidden" id="kitchen_close">
                <!-- <p>Nine course mini buffet with 3 salad, 4 main dishes and 2 desserts.</p> -->
                <div class="cuisine-details">
                    <h4>What’s included:</h4>
                    <div id="cuisine-details">
                    </div>
                    <!-- <ul class="menu-list">
                            <li><p>Watermelon & Feta Salad drizzled with black tea vinaigrette (Vegetarian) </p></li>
                            <li><p>Smoked Duck Salad with roasted cashew and citrus vinaigrette </p></li>
                            <li><p>Tamarind Vinaigrette Capellini tossed with edamame and herbed cherry tomatoes (Vegetarian) </p></li>
                            <li><p>Christmas Spiced Rice with thyme roasted pumpkin and pomegranate (Vegetarian) </p></li>
                            <li><p>Roasted Winter Vegetables with charred pineapple and sweet prune sprinkles (Vegetarian) </p></li>
                            <li><p>Cinnamon Sugar Glazed Chicken with ginger flower and almond flakes </p></li>
                            <li><p>Eggless Eggnog Dory with caramelised onions and fresh cherry tomatoes </p></li>
                            <li><p>Holiday Walnut Cake with rhubarb cherry compote (Vegetarian) </p></li>
                            <li><p>Christmas Cookies with a Grain surprise (Vegetarian) </p></li>
                        </ul> -->
                    
                    <h4 id="addonheading">Add on Packages</h4>
                    <!-- Add on Packages (Multipe select) -->
                    <div class="checkboxWrap" id="addonlist">

                    </div>


                    <h4>Special Instructions</h4>
                    <textarea name="special-instrustion" class="custom-textarea" id="special-instrustion" rows="8"></textarea>
                </div>
            </div>
            <div class="popup-item">
                <img src="<?= $baseurl; ?>images/cuisine/corporate/1.jpg" alt="Cuisine" class="order-img" id="order-img_cuisin">
                <div class="cuisineType">
                    <p id="veg_val"><img src="<?= $baseurl; ?>images/icons/veg.svg" alt="Veg">4 course meal</p>
                </div>
                <div class="priceBlock">
                    <p class="price">&#8377; <span id="pop_price">1,550</span> <span class="price-gst">GST</span></p>
                    <p class="label-gray" id="mim_order">Min Order 25pax</p>
                </div>
                <div class="quantityBlock">
                    <div class="timeBlock">
                        <p class="timeStat label-gray"><span id="leadtime"></span> hours lead time</p>
                    </div>
                </div>
                <div class="order-date-time">
                    <div class="order-date">
                        <div class="form-group form-legend">
                            <input type="text" id="datepicker" class="form-input-legend" placeholder="Select Date">
                        </div>
                    </div>
                    <div class="order-time">
                        <div class="form-group form-legend">
                            <input type="text" class="form-input-legend timepicker" id="order_time" placeholder="Select Time" />
                        </div>
                    </div>
                </div>
                <input id="teamMealMinQty1" type="hidden" class="quantity">
                <div class="order-quantity">
                    <a href="#" class="quantity-icon sub">
                        <p>-</p>
                    </a>
                    <input id="teamMealMinQty" type="text" class="quantity-input quantity" readonly>
                    <a href="#" class="quantity-icon add active">
                        <p>+</p>
                    </a>
                </div>
                
                <input type="hidden" id="validateflag" value="1">
                <div class="addBtn">
                    <p class="addToCart" id="addtocartBtn" onclick="corporatemealorder()"><img src="<?= $baseurl; ?>images/icons/addCart.svg" alt="download">ADD to cart</p>
                    <span id="quterror" style="color:red"></span>
                    
                </div>
               
            </div>
        </div>
        <div class="bg-grey">
            <!-- <p>Terms and Conditions para will follow here if any, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p> -->
        </div>
    </div>
    <section class="section topSection">
        <div class="filterWrap">
            <div class="titleWrap">
                <h3>Your Favourite Restaurants</h3>
                <p>Kitchen's to Choose From</p>
            </div>
            <div class="filterBtn"></div>
            <div class="filterContainer">
                <div class="searchWrap menuSearch">
                    <!-- <h3>Showing 1 – 12 of 10,825 results for "pizza"</h3> -->
                </div>
               
            </div>
        </div>
        <div class="menuWrap restroConatiner autoHeight">
            <div class="cuisineRestroWrapper cuisineWrap">
                <span class="cusine-second">
                    <!-- <p class="filterConatiner btn-gradient btn-mob-filter">Apply </p> -->
                    <p class="closeFilter">Close X</p>
                </span>
                <ul id="kitchens">

                </ul>
            </div>
            <div class="cuisineWrapper">
                <div class="cuisineContainer heightContainer">

                </div>
            </div>
        </div>
    </section>
    <?php
    include 'footer.php';
    ?>
    <script type="text/javascript">
        $(document).ready(function() {
            getKitchens();
            getCuisines();
            $("#meals_check").val('<?= $_REQUEST['mealtype'] ?>');
        });
        /*
        By:Jyoti Vishwakarma
        Description:Kitchen list
        */
        function getKitchens() {
            $.ajax({
                url: serviceurl + 'kitchens',
                type: 'POST',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                    for (var i = 0; i < value.kitchens.length; i++) {
                        <?php if(isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] == '1'){  ?>
                                 html += '<li> <a href="javascript:goToKitchenCorporate(' + value.kitchens[i].id + ',' + '\'' + value.kitchens[i].name1.replace("&#39;","\\'") + '\''+')"> <div class="restroWrapperImg"> <img src="' + value.kitchens[i].image + '" alt="' + value.kitchens[i].name + '"> </div> <div class="restroWrapperDetails"> <h3>' + value.kitchens[i].name + '</h3> <p>' + value.kitchens[i].tagline + '</p> <div class="ratingWrap"> <ul>';
                        <?php    }else{     ?>
                            html += '<li> <a href="javascript:goToKitchen(' + value.kitchens[i].id + ',' + '\'' + value.kitchens[i].name1.replace("&#39;","\\'") + '\''+')"> <div class="restroWrapperImg"> <img src="' + value.kitchens[i].image + '" alt="' + value.kitchens[i].name + '"> </div> <div class="restroWrapperDetails"> <h3>' + value.kitchens[i].name + '</h3> <p>' + value.kitchens[i].tagline + '</p> <div class="ratingWrap"> <ul>';
                        <?php    }  ?>
                        
                        html += '</ul></div></div></a></li>';
                    }
                    $("#kitchens").html(html);
                }
            });
        }
        /*
        By:Jyoti Vishwakarma
        Description: cuisine based search keyword
        */
        function getCuisines() {
            var keyword = "<?= $_REQUEST['keyword'] ?>";
            var sort_by_cost = $("#sort_by_cost").val();
            var ctype =  "<?= $_REQUEST['mealtype'] ?>";    
            var pagedata = {
                "keyword": keyword,
                "sort_by_cost": sort_by_cost,
                "ctype" : ctype
            };
            $(".loader").show();
            $.ajax({
                url: serviceurl + 'search',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: true,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                    var total = value.cuisines.length;
                    if (total <= 12) {
                        var totalsowing = total;
                    } else {
                        var totalsowing = 12;
                    }
                    $(".menuSearch").html('<h3>Showing 1 – ' + totalsowing + ' of ' + total + ' results for "' + keyword + '"</h3>');

                    if(value.cuisines.length > 0){
                        if(ctype == 'corporate_meals'){
                            for (var i = 0; i < value.cuisines.length; i++) {
                           
                                if(value.cuisines[i].isCustomisable == '1'){
                                        html += '<div class="cuisine card customize_cusine" onclick="customizecuisine('+ value.cuisines[i].id +')">';
                                    }else{
                                        html += '<div class="cuisine card">';
                                    }
                                    
                                    html += '<div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description + '</p><div class="priceBlock">';
                                    if(value.cuisines[i].discountprice != '' && value.cuisines[i].offer != ""){
                                            html += '<p class="price"><strike>₹ ' + value.cuisines[i].price + '</strike>  ₹ ' + value.cuisines[i].discountprice + '</p>';
                                        }else{
                                            html += '<p class="price">₹ ' + value.cuisines[i].price + '</p>';
                                        }
                                    html += '<p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock" id="cuisineCatDiv-' + value.cuisines[i].id + '"><div class="hurryBlockContainer">';
                                    if(value.cuisines[i].isCustomisable == '1'){
                                        html += '<p>Customizable</p>';
                                    }else{
                                        html += '<p> </p>';
                                    }
                                    html += ' </div><div class="hurryBlockContainer">';
                                    var offer = "";
                                        if (value.cuisines[i].offer != "") {
                                            offer =  '<span>'+value.cuisines[i].offer + '% off</span>';
                                        }
                                    var cuisintype = value.cuisines[i].type.split(",");
                                        if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                            html += '<p class="food-cat">'+offer+'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                        } else {
                                            html += '<p class="food-cat nonfood-cat">'+offer+'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                        }
                                    
                                    html += '</div></div>';
                                    if(value.cuisines[i].isCustomisable != '1'){
                                        //html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="0" max="3" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';
                                        var start = value.cuisines[i].kitchen_workingstart;
                                    var end = value.cuisines[i].kitchen_workingend;

                                    var d = new Date();
                                            var datesss = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                                            var min = new Date(datesss + ' ' + start);
                                            var max = new Date(datesss + ' ' + end);

                                            if(min.getTime() > d.getTime() || d.getTime() >  max.getTime()){
                                                html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '">Kitchen closed</p></div>';
                                            }
                                            else{
                                                html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="0" max="3" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';
                                    }
                                    }
                                    html += '</div></div></div>';
                                    
                            }
                            $(".cuisineContainer").html(html);
                            $(".loader").hide();
                            addedIncart();
                            hoverCuisine();
                            addButtonFunction();
                            adjustHeight();
                            slideCat();
                        }else{
                            for (var i = 0; i < value.cuisines.length; i++) {
                                html += '<div class="cuisine card" onclick="corporate_order(\'' + value.cuisines[i].id + '\')"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '<span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[i].minorderquantity + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[i].leadtime + ' hours lead time</p></div><div class="hurryBlockContainer">';
                                var cuisintype = value.cuisines[i].type.split(",");
                                if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                    html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                } else {
                                    html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                }
                                html += '</div></div></div></div></div>';
                        
                            }
                            $(".loader").hide();
                            $(".cuisineContainer").html(html);
                            addedIncart();
                            adjustHeight();
                        }
                       
                    }else{
                        $(".loader").hide();
                        $(".menuSearch").html('<h3>0 results for "' + serchKeyword + '"</h3>');
                        html = "<div class='noCuisine'><p>No cuisine found.</p></div>";
                    }
                    
                   
                }
            });
        }
        
        function changeChooseOffering(selected){

            var d = new Date();
            d.setTime(d.getTime() + (1*24*60*60*1000));
            var expires = "expires="+ d.toUTCString();
            document.cookie = "mealstype" + "=" + selected + ";" + expires + ";path=/";

            if(selected == "corporate_meals"){
                $("#meals_check").val('corporate_meals');
                
            }else if(selected == "team_meals"){
               
                $("#meals_check").val('team_meals');
                
            }else if(selected == "event_meals"){
               
                $("#meals_check").val('event_meals');
               
            }else if(selected == "cafeteria"){
               
                $("#meals_check").val('cafeteria');
                
                }
            }

            function corporate_order(id){
            var pagedata = {
                "id": id
            };
            $.ajax({
                url: serviceurl + 'corporatemeals_detail',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    console.log(value.cuisines[0].name);
                    var val = "";
                    $("#cuisine_id").val(value.cuisines[0].id);
                    $("#kitchen_open").val(value.cuisines[0].kitchen_open);
                    $("#kitchen_close").val(value.cuisines[0].kitchen_close);

                    if (value.cuisines[0].type.includes('Non Veg')) {
                        val = '<img src="images/icons/nonveg.svg" alt="Non Veg">' + value.cuisines[0].kitchen_name;
                    } else {
                        val = '<img src="images/icons/veg.svg" alt="Veg">' + value.cuisines[0].kitchen_name;
                    }
                    $("#cuisineTitle1").html(value.cuisines[0].name);
                    $("#order-img_cuisin").attr("src", value.cuisines[0].image);
                    $("#veg_val").html(val);
                    $("#cuisine-details").html(value.cuisines[0].description);
                    $("#pop_price").html(value.cuisines[0].price);
                    $("#mim_order").html("Min Order " + value.cuisines[0].minorderquantity + "pax");
                    $("#leadtime").html(value.cuisines[0].leadtime);
                    $("#teamMealMinQty").val(value.cuisines[0].minorderquantity);
                    $("#teamMealMinQty1").val(value.cuisines[0].minorderquantity);
                    var addon = "";
                    
                        var addonshtml = "";
                    if(value.cuisines[0].sections.length == 0){
                        $("#addonheading").hide();
                    }else{
                        $("#addonheading").show();
                    }    
                    for(var i=0;i<value.cuisines[0].sections.length;i++){
                        console.log(value.cuisines[0].sections[i].title);
                        var section_id = value.cuisines[0].sections[i].section_id;
                        var noofchoice = value.cuisines[0].sections[i].noofchoice;
                        addonshtml += '<h4 >'+value.cuisines[0].sections[i].title+'</h4><input id="da'+section_id+'" type="hidden" value="'+noofchoice+'"><input id="das'+section_id+'" type="hidden" value="'+noofchoice+'"><span id="counterror'+section_id+'" style="color:red;display:none">error</span><div id="prt'+section_id+'">';
                        var addon = value.cuisines[0].sections[i].addons;
                        

                        if(value.cuisines[0].sections[i].choicetype == 1){
                            addonshtml += '<div class="section-div" id="'+section_id+'"></div>';
                            for(var j=0;j<addon.length;j++){

                                addonshtml += '<label class="container">';
                                addonshtml += '<p>' + addon[j].detail + ' &#8377;' + addon[j].price + '</p>';
                                addonshtml += '<input type="checkbox" onclick="checkval('+section_id+','+addon[j].addon_id+')" id="'+addon[j].addon_id+'" class="addons_corporate" name="addon"  data-id="' + addon[j].addon_id + '" value="' + addon[j].addon_id + '">';
                                addonshtml += '<span class="checkmark"></span>';
                                addonshtml += '</label>';
                            }

                        }else{
                            addonshtml += '<div class="section-divs"></div>';
                            for(var j=0;j<addon.length;j++){
                                    var checked = "";
                                    if(j==0){
                                        checked = "checked";
                                    }
                                    addonshtml +=   ' <div class="checkboxWrap">';
                                    addonshtml +=    '<label class="container radioContainer">';
                                    addonshtml +=        '<p>' + addon[j].detail + ' &#8377;' + addon[j].price + '</p>';
                                    addonshtml +=       '<input type="radio" name="addOnPacagesRadio-'+ value.cuisines[0].sections[i].section_id +'" value="' + addon[j].addon_id + '" ' + checked + '>';
                                    addonshtml +=        '<span class="checkmark"></span>';
                                    addonshtml +=    '</label>';
                                    addonshtml +=  '</div>'; 
                            }

                        }
                        addonshtml +=  '</div>'; 
                    }

                    $("#addonlist").html(addonshtml);
                    $("#datepicker").val("");
                    $("#order_time").val("");
                    $("#quterror").hide();
                    $("#quterror").text("");
                    $(".addtocartpopup, .overlay")
                        .delay(400)
                        .fadeIn();

                }
            });

        }

        function corporatemealorder() {

            var cuisine_id = $("#cuisine_id").val();
            var date = $("#datepicker").val();
            var time = $("#order_time").val();           
            var qty = parseInt($("#teamMealMinQty").val());
            var qty1 = parseInt($("#teamMealMinQty1").val());
            var numItems = $('.section-div').length
            if(numItems > 0){
                $('.section-div').each(function(){
                   var section_id = $(this).prop('id');
                   var minreq = $("#da"+section_id).val(); 
                   var qut1 = $("#das"+section_id).val();
                  
                    if(qut1 != 0){
                        $("#counterror"+section_id).css("display","block");
                        $("#counterror"+section_id).text("Minimum selection should be "+minreq+".");
                        $("#validateflag").val(1);
                    }else{
                        $("#counterror"+section_id).css("display","none");
                        $("#validateflag").val(0);
                    }
                   
                });
            }else{
                $("#validateflag").val(0);
            }
            
            var leadtime = parseInt($("#leadtime").html());
            var today = new Date();
            var minord = new Date(today.setHours(today.getHours()+leadtime));
            var orddate = date + ' ' + time;
            orddate = new Date(orddate);

            var min = new Date(date + ' ' + $("#kitchen_open").val());
            var max = new Date(date + ' ' + $("#kitchen_close").val());

            if(qty1 > qty || date == "" || time == ""){
                if(date == ""){
                    $("#quterror").show();
                    $("#quterror").text("Please select date.");
                }else if( time == ""){
                    $("#quterror").show();
                    $("#quterror").text("Please select time.");
                }else if(qty1 > qty){
                    $("#quterror").show();
                    $("#quterror").text("Min Order "+qty1+"pax");
                }
                return;
            }else if(minord.getTime() > orddate.getTime()){
                $("#quterror").show();
                $("#quterror").text("Order time should be greater than leadtime");
                return;
            }else if(min.getTime() > orddate.getTime() || orddate.getTime() >  max.getTime()){
                $("#quterror").show();
                $("#quterror").text("Order time should be between "+ $("#kitchen_open").val() +" and "+ $("#kitchen_close").val());
                return;
            }else{
                $("#quterror").hide();
            }

            var special_instruction = $("#special-instrustion").val();

            var items = $(".addons_corporate");
            var selectedaddons = "";
            for (var i = 0; i < items.length; i++) {
                if (items[i].type == 'checkbox' && items[i].checked == true){
                        selectedaddons += items[i].value + ",";
                }
            }

            $('#addonlist input[type=radio]:checked').each(function() {
                 selectedaddons += this.value + ",";
            });

            addToCartCorporate(cuisine_id, date, time, qty, special_instruction, selectedaddons);
            // alert(special_instruction);

        }

        function addToCartCorporate(id, date, time, qty, special_instruction, selectedaddons) {
            var count = 1;
            var data = {
                "id": id,
                "date": date,
                "time": time,
                "qty": qty,
                "special_instruction": special_instruction,
                "selectedaddons": selectedaddons
            };
            if($("#validateflag").val() == 0){
                    $.ajax({
                    url: serviceurl + 'addtocartCorporate',
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        if (value.status == 'success') {
                            var cartCount = $(".notiCount").html();
                            showCartData();
                            $(".popup-order, .overlay")
                            .delay(400)
                            .fadeOut();
                            $(".toast").fadeIn();
                            setTimeout(function(){
                                $(".toast").fadeOut();
                            }, 3000);
                            //$(".notiCount").html(parseInt(cartCount) + 1);
                        } else {

                            $('.overlay,.cartPopup').fadeIn();
                            setTimeout(function() {
                                $('.overlay,.cartPopup').fadeOut();
                            }, 1200);
                        }
                    }
                });
            }
            else{
                // $("#quterror").text("Min Order "+qty1+"pax");
            }
        }

        function checkval(s,z){
            qut1 = $("#da"+s).val();
            qut = $("#da"+s).val();
            var q = "#das"+s;
            var qt = $(q).val();   
            var prt = "#prt"+s;   
            var ckId =  "#"+z;
            
                // alert($(q).val());
                if($(ckId).is(":checked")){
                    $(q).val(parseInt(qt) - 1);
                }
                else if($(ckId).is(":not(:checked)")){
                    $(q).val(parseInt(qt) + 1);
                    
                }
                
                if($(q).val() == 0 ){
                    $("#validateflag").val(0);
                    $("#counterror"+s).css("display","none");
                }
                else{
                    $("#counterror"+s).css("display","block");
                    $("#counterror"+s).text("Minimum selection should be "+qut1+".");
                    $("#validateflag").val(1);
                }
                        
        }
   

    </script>
</body>

</html>