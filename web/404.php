<!DOCTYPE html> 
<html>
    <head>
        <?php
			include 'head.php';
		?>
        <title>Cloudkitch</title>
        <meta name="description" content="">
        <style>
            footer,header{display:none;}
            .notlogo{position: fixed;left: 20px;top: 10px;height: 65px;}
            @media screen and (max-width: 800px) and (min-width: 200px){
                .pageNotFound{width: 100%}
                .mobHeader{display: none}
            }
        </style>
    </head>
    <body class="notFound">
        <?php
			include 'header.php';
        ?>

        <section class="section pageNotFound">
            <img src="<?=$baseurl;?>images/logo.svg" alt="" class="notlogo">
            <img src="<?=$baseurl;?>images/page-not-found.svg" alt="">
            <h2>Oops !</h2>
            <p>Sorry! Something went wrong. Please retry.</p>
            <a href="<?=$baseurl;?>" class="btn-gradient">Go Home</a>
        </section>
        <?php
			include 'footer.php';
        ?>
    </body>
</html>