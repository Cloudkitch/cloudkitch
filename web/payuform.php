<?php

    /*
    By:Jyoti Vishwakarma
    Description: For online payment
    */
error_reporting(1);
session_start();
include 'inc/databaseConfig.php';
if(empty($posted['txnid'])) {
  $paymentType = isset($_POST['payment_paymenttype']) ? $_POST['payment_paymenttype']: "";
  $firstname = isset($_POST['payment_firstname']) ? $_POST['payment_firstname'] : "";
  $delivery_address = isset($_POST['payment_delivery_address']) ? $_POST['payment_delivery_address'] : "";
  $stateinfo = isset($_POST['payment_stateinfo']) ? $_POST['payment_stateinfo'] : "";
  $cityinfo = isset($_POST['payment_cityinfo']) ? $_POST['payment_cityinfo'] : "";
  $zip = isset($_POST['payment_zip']) ? $_POST['payment_zip'] : "";
  $phone = isset($_POST['payment_phone']) ? $_POST['payment_phone'] : "";
  
  $itemcosttotal = isset($_POST['itemcosttotal']) ? $_POST['itemcosttotal'] : "";
  $is_combine_pay = isset($_POST['is_combine_pay']) ? $_POST['is_combine_pay'] : "";
  $cart_total_amount = isset($_POST['cart_total_amount']) ? $_POST['cart_total_amount'] : "";
  $walletamount = isset($_POST['walletamount']) ? $_POST['walletamount'] : "";

  $isPromoApply = isset($_POST['isPromoApply']) ? $_POST['isPromoApply'] : "";
  $discountAmount = isset($_POST['discountAmount']) ? $_POST['discountAmount'] : "";

  $startdate = isset($_POST['startdatecu']) ? $_POST['startdatecu'] : "";
  $timecu = isset($_POST['timecu']) ? $_POST['timecu'] : "";


  $productinfo = "cloudkitch";
  $userid= $_SESSION['userid'];
  $useremail= $_SESSION['useremail'];
  $corporateid = '';
  if(isset($_SESSION['corporateid'])){
    $corporateid = $_SESSION['corporateid'];

  }

  $query = "SELECT * FROM cart WHERE userid='$userid'";
  $result = mysqli_query($conn,$query) or die(mysqli_error($conn));

  $grand_total = 0;
  while ($row = mysqli_fetch_assoc($result)) {
    $product_id = $row['product_id'];
    $quantity = $row['quantity'];

    $queryProduct = "SELECT * FROM cuisine WHERE cuisineid='$product_id'";
    $resultProduct = mysqli_query($conn,$queryProduct) or die(mysqli_error($conn));
    $rowProduct = mysqli_fetch_assoc($resultProduct);
    $price = isset($rowProduct['price']) ? $rowProduct['price'] : 0;
    $kitchenid = $rowProduct['userid'];
    $offer = 0;
    if($rowProduct['isCorporateMeal'] =='0' && $rowProduct['isEventMeal'] == '0'){
      if($corporateid != ''){
        $queryO = "SELECT offer FROM corporateoffermapping WHERE status='1' AND restaurantid = '$kitchenid' AND corporateid='$corporateid'";	
        $resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
        $rowO = mysqli_fetch_assoc($resultO);
        $offer = $rowO['offer'];
        $price = round($rowProduct['price'] - ( ( $rowProduct['price'] * $offer ) / 100 ));
        
      }else{
        $queryO = "SELECT offer FROM offeroncuisine WHERE status='1' AND kitchen_id = '$kitchenid'";	
        $resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
        $rowO = mysqli_fetch_assoc($resultO);
        $offer = $rowO['offer'];
        $price = round($rowProduct['price'] - ( ( $rowProduct['price'] * $offer ) / 100 ));
      }
    }



    if($row['selectedaddons'] != ""){
      $selectedaddons = array_filter(explode(",",$row['selectedaddons'])); 
      $selectedaddons = implode(",",$selectedaddons);
    }else{
      $selectedaddons = "''"; 
    }
    $addon_total = 0;
    $query3 = "SELECT * FROM corporate_cuisine_sections_addons WHERE ID IN ($selectedaddons)";
    $result3 = mysqli_query($conn,$query3) or die(mysqli_error($conn));
    while($row3 = mysqli_fetch_assoc($result3))
    {
      $addonprice = $row3['price'];
      if($rowProduct['isCorporateMeal'] =='0' && $rowProduct['isEventMeal'] == '0' && $offer != 0){
								
        $addonprice = round( $row3['price'] - ( (  $row3['price'] * $offer ) / 100 ));
        
      }
      $addon_total = $addon_total + ($addonprice * $quantity);
    }

    $orderid = isset($rowProduct['cassordid']) ? $rowProduct['cassordid'] : 0;
    $total = $price*$quantity;
    $total = $total +  $addon_total;
    $grand_total+=$total;
  }

  $tprice =  $grand_total;
 
 
  $datecount= 0;
	if($startdate != ""){
		$datecount = explode(",",$startdate);	
		$datecount = count($datecount);
	}
	$offer = 0;
	if($datecount != ""){
    $grand_total = $grand_total * $datecount;
		$queryO = "SELECT offer FROM subscriptionoffer WHERE noofdays<='$datecount' and status='1' ORDER BY noofdays DESC";	
		$resultO = mysqli_query($conn,$queryO)or die(mysqli_error($conn));
		$rowO = mysqli_fetch_assoc($resultO);
		$offer = $rowO['offer'];
  }
  $gst = round( 5 / 100 * $grand_total,2);
  $grand_total = $grand_total + $gst;
  $grand_total = round($grand_total - ( ( $grand_total * $offer ) / 100 ));

  if($walletamount >= $grand_total ){
    $grand_total = $grand_total - $walletamount;
  }

  if($isPromoApply == 1 && $discountAmount != 0){
    $grand_total = $grand_total - $discountAmount;
  }

}


// Merchant key here as provided by Payu
// $MERCHANT_KEY = "gtKFFx"; //Please change this value with live key for production
$MERCHANT_KEY = "xTisbg";
$hash_string = '';
// Merchant Salt as provided by Payu
// $SALT = "eCwWELxi"; //Please change this value with live salt for production
$SALT = "ttBAqyWz";
// End point - change to https://secure.payu.in for LIVE mode
// $PAYU_BASE_URL = "https://test.payu.in";  // Test url
$PAYU_BASE_URL = "https://secure.payu.in";
$action = '';
$posted = array();
if(!empty($_POST)) {
    //print_r($_POST);
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
  }
}

$formError = 0;
if(empty($posted['txnid'])) {
   // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
    empty($posted['key'])
    || empty($posted['txnid'])
    || empty($posted['amount'])
    || empty($posted['firstname'])
    || empty($posted['email'])
    || empty($posted['phone'])
    || empty($posted['productinfo'])

  ) {
    $formError = 1;
  } else {

   $hashVarsSeq = explode('|', $hashSequence);
   foreach($hashVarsSeq as $hash_var) {
    $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
    $hash_string .= '|';
  }

  $hash_string .= $SALT;
  $hash = strtolower(hash('sha512', $hash_string));
  $action = $PAYU_BASE_URL . '/_payment';
}
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
?>
<html>
<head>
  <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
  <?php
    include 'head.php';
    ?>
</head>

<style type="text/css">
  .processingSection .overlay, .popup-card-details{display:block;}
  header, footer{display:none;}
</style>
<body class="processingSection" onload="submitPayuForm()">
  <div class="popup popup-card-details">
    <div class="popup-wrapper">
      <div class="popup-image-wrapper">
        <img src="<?=$baseurl;?>images/icons/popup-loader.svg" alt="">
      </div>
      <p class="popup-first">You will be redirected to your bank's website. <br> it might take few seconds</p>
      <p class="popup-second">Please do not refresh the page or click the back close button of your computer</p>
    </div>
  </div>
  <!-- <h2>PayU Form</h2> -->
  <br/>
  <?php if($formError) { ?>
    <!-- <span style="color:red">Please fill all mandatory fields.</span> -->
    <br/>
    <br/>
  <?php } ?>
  <form action="<?php echo $action; ?>" method="post" name="payuForm" id="payuForm" style="display: none;">
    <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
    <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
    <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />

    <input type="hidden" name="itemcosttotal" value="<?php echo $itemcosttotal ?>" />
    <input type="hidden" name="is_combine_pay" value="<?php echo $is_combine_pay ?>" />
    <input type="hidden" name="cart_total_amount" value="<?php echo $cart_total_amount ?>" />
    <input type="hidden" name="walletamount" value="<?php echo $walletamount ?>" />

    <input type="hidden" name="ispromoapply" value="<?php echo $isPromoApply ?>" />
    <input type="hidden" name="discountamount" value="<?php echo $discountAmount ?>" />

    <input type="hidden" name="startdate" value="<?php echo $startdate ?>" />
    <input type="hidden" name="order_time" value="<?php echo $timecu ?>" />

    <input type="hidden" name="surl" value="http://cloudkitch.co.in/api_1.0.php?service_name=checkout" />   <!-- Live -->
    <input type="hidden" name="furl" value="http://cloudkitch.co.in/cart.php" /><!-- Live -->
     <table>
        <tr>
          <td><b>Mandatory Parameters</b></td>
        </tr>
        <tr>
          <td>Amount: </td>
          <td><input name="amount" value="<?php echo (empty($posted['amount'])) ? $grand_total : $posted['amount'] ?>" /></td>
          <td>First Name: </td>
          <td><input name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? $firstname : $posted['firstname']; ?>" /></td>
        </tr>
        <tr>
          <td>Email: </td>
          <td><input name="email" id="email" value="<?php echo (empty($posted['email'])) ? $useremail : $posted['email']; ?>" /></td>
          <td>Phone: </td>
          <td><input name="phone" value="<?php echo (empty($posted['phone'])) ? $phone : $posted['phone']; ?>" /></td>
        </tr>
        <tr>
          <td>Product Info: </td>
          <td colspan="3"><textarea name="productinfo"><?php echo (empty($posted['productinfo'])) ? $productinfo : $posted['productinfo'] ?></textarea></td>
        </tr>
      <tr>
        <td><b>Optional Parameters</b></td>
      </tr>
      <tr>
        <td>Last Name: </td>
        <td><input name="lastname" id="lastname" value="<?php echo (empty($posted['lastname'])) ? '' : $posted['lastname']; ?>" /></td>
        <td>Cancel URI: </td>
        <td><input name="curl" value="" /></td>
      </tr>
      <tr>
        <td>Address1: </td>
        <td><input name="address1" value="<?php echo (empty($posted['address1'])) ? $delivery_address : $posted['address1']; ?>" /></td>
        <td>Address2: </td>
        <td><input name="address2" value="<?php echo (empty($posted['address2'])) ? '' : $posted['address2']; ?>" /></td>
      </tr>
      <tr>
        <td>City: </td>
        <td><input name="city" value="<?php echo (empty($posted['city'])) ? $stateinfo : $posted['city']; ?>" /></td>
        <td>State: </td>
        <td><input name="state" value="<?php echo (empty($posted['state'])) ? $cityinfo : $posted['state']; ?>" /></td>
      </tr>
      <tr>
        <td>Country: </td>
        <td><input name="country" value="<?php echo (empty($posted['country'])) ? 'INDIA' : $posted['country']; ?>" /></td>
        <td>Zipcode: </td>
        <td><input name="zipcode" value="<?php echo (empty($posted['zipcode'])) ? $zip : $posted['zipcode']; ?>" /></td>
      </tr>
      <tr>
        <td>UDF1: </td>
        <td><input name="udf1" value="<?php echo (empty($posted['udf1'])) ? '' : $posted['udf1']; ?>" /></td>
        <td>UDF2: </td>
        <td><input name="udf2" value="<?php echo (empty($posted['udf2'])) ? '' : $posted['udf2']; ?>" /></td>
      </tr>
      <tr>
        <td>UDF3: </td>
        <td><input name="udf3" value="<?php echo (empty($posted['udf3'])) ? '' : $posted['udf3']; ?>" /></td>
        <td>UDF4: </td>
        <td><input name="udf4" value="<?php echo (empty($posted['udf4'])) ? '' : $posted['udf4']; ?>" /></td>
      </tr>
      <tr>
        <td>UDF5: </td>
        <td><input name="udf5" value="<?php echo (empty($posted['udf5'])) ? '' : $posted['udf5']; ?>" /></td>
        <td>PG: </td>
        <td><input name="pg" value="<?php echo (empty($posted['pg'])) ? '' : $posted['pg']; ?>" /></td>
      </tr>
      <tr>
        <?php if(!$hash) { ?>
          <td colspan="4"><input type="submit" value="Submit" /></td>
        <?php } ?>
      </tr>
    </table>
  </form>
  <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <script type="text/javascript" defer> 
    $(document).ready(function(){
       var payuForm = document.forms.payuForm;
        payuForm.submit();
    });
  </script>
</body>
</html>
