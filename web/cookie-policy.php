<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'head.php';
    ?>
    <title>Cookie Policy | Cloudkitch</title>
    <meta name="description" content="The cookie policy informs our online visitors of our policy regarding the same.">
</head>

<body class="servicesPage">
    <?php
    include 'header.php';
    ?>
    <section class="topSection section section-services">
        <h2 class="setionTitle">Cookie Policy</h2>
        <p>This Cookie Policy <b>(“Cookie Policy”)</b> is a part of and incorporated within and is to be read along with the Privacy Policy <b>(“Policy”)</b> and Term of Use. The capitalized terms used in this Cookie Policy, but not defined herein, shall have the meaning given to such terms in the Privacy Policy.</p>

        <h2>What are cookies and local storage?</h2>

        <p>Cookies are small text files that are placed on your browser or device by websites, apps, online media, and advertisements. There are different types of cookies. Cookies served by the entity that operates the domain you are visiting are called “first party cookies.” So, cookies served by Cloudkitch while you are on the Cloudkitch Platform are first party cookies. Cookies served by companies that are not operating the domain you are visiting are called “third party cookies.” For eg., we may allow Google to set a cookie on your browser while you visit the Cloudkitch Platform, and that would be a third party cookie. Cookies may also endure for different periods of time. “Session Cookies” only last only as long as your browser is open. These are deleted automatically once you close your browser. Other cookies are “persistent cookies” meaning that they survive after your browser is closed. For example, they may recognize your device when you re-open your browser and browse the internet again.</p>
        <p>“Pixel tags” (also called beacons or pixels) are small blocks of code installed on (or called by) a webpage, app, or advertisement which can retrieve certain information about your device and browser, including for example: device type, operating system, browser type and version, website visited, time of visit, referring website, IP address, and other similar information, including the small text file (the cookie) that uniquely identifies the device. Pixels provide the means by which third parties can set and read browser cookies from a domain that they do not themselves operate and collect information about visitors to that domain, typically with the permission of the domain owner.</p>
        <p>Local storage is a technology that allows a website or application to store information locally on your device. “Software Development Kits” (also called SDKs) function like pixels and cookies, but operate in the mobile app context where pixels and cookies cannot always function. The primary app developer can install pieces of code (the SDK) from partners in the app, and thereby allow the partner to collect certain information about user interaction with the app and information about the user device and network information.</p>

        <h2>Why do we use these technologies?</h2>
        <p>We use cookies and other identification technologies for various purposes, including: authenticating users,store information about you (including on your device or in your browser cache) and your use of our Services and the Cloudkitch Platform, remembering user preferences and settings, determining the popularity of content, delivering and measuring the effectiveness of advertising campaigns, analyzing site traffic and trends, and generally understanding the online behaviors and interests of people who interact with our Services.</p>

        <h2>Cookies used by us</h2>

        <table border="1" class="custTable">
            <tr>
                <th>
                    <p><b>Type of Cookie</b></p>
                </th>
                <th>
                    <p><b>Purpose</b></p>
                </th>
                <th>
                    <p><b>Host</b></p>
                </th>
            </tr>
            <tr>
                <td>
                    <p><b>Authentication Cookies</b></p>
                </td>
                <td>
                    <p>These cookies (including local storage and similar technologies) tell us when you’re logged in, so we can show you the appropriate experience and features such as your account information, order history and to edit your account settings.</p>
                </td>
                <td>
                    <p>Cloudkitch</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><b>Security and site integrity cookies</b></p>
                </td>
                <td>
                    <p>We use these cookies to support or enable security features to help keep Cloudkitch safe and secure. For example, they enable us to remember when you are logged into a secure area of the Services and help protect your account from being accessed by anyone other than you.</p>
                </td>
                <td>
                    <p>Cloudkitch</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><b>Site features and Services</b></p>
                </td>
                <td>
                    <p>These provide functionality that help us deliver products and Services. For example, cookies help you log in by pre-filling fields. We may also use cookies and similar technologies to help us provide you and others with social plugins and other customized content and experiences, such as making suggestions to you and others.</p>
                </td>
                <td>
                    <ul>
                        <li>
                            <p>Cloudkitch</p>
                        </li>
                        <li>
                            <p>Facebook</p>
                        </li>
                        <li>
                            <p>Google</p>
                        </li>
                    </ul>
                </td>
            </tr>

            <tr>
                <td>
                    <p><b>Analytics and research</b></p>
                </td>
                <td>
                    <p>These are used to understand, improve, and research products and Services, including when you access the Cloudkitch website and related websites and apps from a computer or mobile device. For example, we may use cookies to understand how you are using site features, and segmenting audiences for feature testing. We and our partners may use these technologies and the information we receive to improve and understand how you use websites, apps, products, services and ads.</p>
                </td>
                <td>
                    <p>Google</p>
                </td>
            </tr>

            <tr>
                <td>
                    <p><b>Advertising</b></p>
                </td>
                <td>
                    <p>Things like cookies and pixels are used to deliver relevant ads, track ad campaign performance and efficiency. For example, we and our ad partners may rely on information gleaned through these cookies to serve you ads that may be interesting to you on other websites. Similarly, our partners may use a cookie, attribution service or another similar technology to determine whether we’ve served an ad and how it performed or provide us with information about how you interact with them.</p>
                </td>
                <td>
                    <ul>
                        <li>
                            <p>Cloudkitch</p>
                        </li>
                        <li>
                            <p>Facebook</p>
                        </li>
                        <li>
                            <p>Google</p>
                        </li>
                    </ul>
                </td>
            </tr>
        </table>

        <h2>Do we use Third Party Cookies?</h2>
        <p>We use a number of suppliers that may also set cookies on your device on our behalf when you visit the Cloudkitch Platform to allow them to deliver the services they are providing.</p>
        <p>When you visit the Cloudkitch Platform you may receive cookies from third party websites or domains. More information about these cookies may be available on the relevant third party's website.</p>

        <h2>Additional Information About Third Party Analytics in use on the Cloudkitch Platform :</h2>

        <ul>
            <li>
                <p><b>Facebook Connect:</b> For more information about what Facebook collects when you use Facebook buttons on the Cloudkitch Platform, please see: Data Policy.</p>
            </li>

            <li>
                <p><b>Twitter:</b> For more information about what Twitter collects when you use the Cloudkitch Platform, please see: <a href="https://twitter.com/en/privacy" target="_blank">https://twitter.com/en/privacy</a>.</p>
            </li>
            <li>
                <p><b>Google Analytics:</b> For more information about Google Analytics cookies, please see Google's help pages and privacy policy:</p>
                <ul>
                    <li><a href="https://policies.google.com/privacy" target="_blank">Google's Privacy Policy</a></li>
                    <li><a href="https://support.google.com/analytics/#topic=3544906" target="_blank">Google Analytics Help pages</a></li>
                </ul>
            </li>
        </ul>

        <p>Please note that this is not an exhaustive list of all third party analytics providers that we work with.</p>

        <h2>How can I control Cookies?</h2>

        <p>Most internet browsers are initially set up to automatically accept cookies. You can change the settings to block cookies or to alert you when cookies are being sent to your device. There are a number of ways to manage cookies. Please refer to your browser instructions or help screen to learn more about how to adjust or modify your browser settings.</p>
        <p>If you disable the cookies that we use, this may impact your experience while on the Cloudkitch Platform, for example you may not be able to visit certain areas of the Cloudkitch Platform or you may not receive personalised information when you visit the Cloudkitch Platform or you may also be unable to login to services or programs, such as logging into forums or accounts.</p>
        <p>If you use different devices to view and access the Cloudkitch Platform(e.g. your computer, smartphone, tablet etc) you will need to ensure that each browser on each device is adjusted to suit your cookie preferences.</p>
        <p><b>Changing your Cookie Settings.</b> The browser settings for changing your cookies settings are usually found in the 'options' or 'preferences' menu of your internet browser. In order to understand these settings, the following links may be helpful. Otherwise you should use the 'Help' option in your internet browser for more details.</p>
        <ul>
            <li>
                <p><a href="https://support.microsoft.com/en-us/help/17442/windows-internet-explorer-delete-manage-cookies" target="_blank">Cookie settings in Internet Explorer</a></p>
            </li>
            <li>
                <p><a href="https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences" target="_blank">Cookie settings in Firefox</a></p>
            </li>
            <li>
                <p><a href="https://support.google.com/chrome/answer/95647" target="_blank">Cookie settings in Chrome</a></p>
            </li>
            <li>
                <p><a href="https://support.apple.com/" target="_blank">Cookie settings in Safari</a></p>
            </li>
        </ul>

        <p>More information. To find out more about cookies, including how to see what cookies have been set and how to manage and delete them, visit <a href="https://www.aboutcookies.org/" target="_blank">About Cookies</a>  or <a href="http://www.allaboutcookies.org/" target="_blank">www.allaboutcookies.org</a>.</p>

    </section>

    <?php
    include 'footer.php';
    ?>
</body>

</html>