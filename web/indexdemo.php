<?php

if (isset($_GET['user_id']) && isset($_GET['order_id'])) {
    $show_review = "1";
    $user_id = $_GET['user_id'];
    $order_id = $_GET['order_id'];
} else {
    $show_review = "0";
    $user_id = "";
    $order_id = "";
}

// <?php header("Location:web/cloudkitchadmin/"); 


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'head.php';
    // echo $baseurl;
    // print_r($_SESSION);
    ?>
    <title>Top Indian Restaurants Food Delivery Near Me| Indian Cuisine | Delivery Only Restaurants Near Me â€“ Cloud Kitch</title>
    <meta name="description" content="When hunger strikes and when in need to get Indian cuisine or Indian food delivery near me from one of the top Indian restaurants or from the delivery only restaurants near me, always choose Cloud Kitch, the best delivery kitchen curating the top Indian restaurants.">
    <meta name="keywords" content="indian food delivery near me, indian cuisine near me, TOP Indian restaurants, delivery only restaurants near me">
    <meta name="google-signin-client_id" content="287145767352-qi8asdmobui20671musqaq0jga2ru2re.apps.googleusercontent.com">
</head>

<body>
    <h1 class="seoTag">Cloud Kitch</h1>
    <?php
    include 'header.php';
    if (isset($_SESSION['userid'])){
        if($_SESSION['isCorporateUser'] == 1){
            echo '<script>window.location = "'.$baseurl.'corporate";</script>';
        }
    }
    ?>
   

    <section class="banner topSection">
        <h2 style="display:none;">Cloudkitch</h2>
        <div class="homeSlider" id="homeSlider" style="display: none">
            <?php
            $query = "SELECT * FROM banners";
            $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
            while($row = mysqli_fetch_assoc($result))
            {
                ?>
                <div class="homeSlide">
                    <img class="lazyload" data-src="<?=$row['image']?>" alt="Banner">
                </div>
                <?php
            }
            ?>
        </div>
    </section>
         <a href="#" class="btn-outline btn-outline-img facebook" onclick="facebookLogin()" id="facebook-button">
                <p>Sign in with Facebook</p>
                <!-- <div scope="public_profile,email"  class="fb-login-button" data-width="" data-size="large" data-button-type="login_with" data-auto-logout-link="false" data-use-continue-as="false"></div> -->
               
            </a>
    <section class="section firstSection">
        <div class="titleWrap sectionText">
            <h2>Quick Orders</h2>
            <p>Kitchen's to Choose From</p>
        </div>
        <div class="filterWrap">
            <div class="titleWrap">
                <h2><img src="<?= $baseurl; ?>images/icons/cuisine.svg" alt="Course"> Popular Options</h2>
            </div>
            <div class="filterBtn"></div>
            <div class="filterContainer">
                <div class="searchWrap menuSearch">
                    <form>
                        <input type="text" id="search_inner" oninput="getCuisinesFilter()" placeholder="Search Hot Favourites">
                        <input type="hidden" id="kitchen_id">
                    </form>
                </div>
                <div class="buttonWrap">
                    <p class="btn borderBtn offersBtn" onclick="getOffer()"><img src="<?= $baseurl; ?>images/icons/discount.svg" alt="offers">All Offers</p>
                    <div class="filter-check">
                        <input class="custom-radio" onchange="getCuisinesFilter()" type="checkbox" id="checkbox_veg" name="checkbox_veg">
                        <label for="checkbox_veg" class="btn radio-label">
                            <img src="<?= $baseurl; ?>images/icons/leaf.svg" alt="Veg">VEG
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="menuWrap autoHeight">
            <div class="cuisineWrap card restrocuisineWrap">
                <span class="cusine-second">
                    <span class="closeFilter">Close X</span>
                </span>
                <div class="checkWrap">
                    <ul id="categories">

                    </ul>
                </div>
            </div>
            <div class="cuisineWrapper cuisineWrappernotflex">
                <div class="restroWrap homerestroSlider" id="kitchens"></div>
                <div class="cuisineContainer heightContainer">
                    <div id="cuisines"></div>
                </div>
                <span data-value="1" id="loadmoredata" class="loadmore" style="border: none;"> </span>
            </div>
        </div>
        <?php
       
        if (isset($_COOKIE['pincode']) && $_COOKIE['pincode']!="") {
            $display = 'none;';
        } else {
            $display = 'block;';
        }
       
        ?>
        <div class="overlay" id="locationss" style="display:<?= $display ?>"></div>
        <div class="popup locationPopup" id="locationpopup" style="display:<?= $display ?>">
            <div class="popup-wrapper">
                <div class="centerText">
                    <img src="<?= $baseurl; ?>images/logo.svg" alt="Cloudkitch" class="popup-logo">
                </div>
                <div class="firstDiv">
                    <form id="pincode_form" name="pincode_form">
                        <h2>Welcome to Cloudkitch!</h2><br>
                        <div class="form-conatiner">
                            <div class="form-group form-float-group">
                                <input type="text" placeholder="" id="pincode" name="pincode" autocomplete="off" class="form-float-control">
                                <label for="pincode" class="form-float-lab">Enter area pincode here</label>
                            </div>
                            <button type="submit" class="btn-gradient">Proceed</button>
                        </div>
                    </form>
                    <p class="skip">Skip</p>
                </div>
                <div class="secDiv centerText" style="display:none;">
                    <h2>Your area is currently not serviceable.</h2>
                </div>
            </div>
        </div>
    </section>
    <?php
    include 'footer.php';
    ?>
    
    <script>
        
        $(document).ready(function() {
            $("#homeSlider").css("display","block");
            getCategories('1');
            getKitchens();
            getCuisines();
            var show_review = '<?= $show_review ?>';
            if (show_review == '1') {
                $(".reviewPopup").css({
                    'display': 'block'
                });
                $(".locationPopup").css({
                    'display': 'none'
                });
            }

            $('#search_inner').keypress(function(event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                }
             });
            
            var s = '<?php echo $_COOKIE['pincode'] ?>';
            if(s != ''){
                $("#locationpopup").css("display","none");
                $("#locationss").css("display","none");
            }else{
                $("#locationpopup").css("display","block");
                $("#locationss").css("display","block");
            }

        });
  
   

        jQuery(
            function($)
            {
                $('.cuisineContainer').bind('scroll', function()
                    {
                        if($(this).scrollTop() + $(this).innerHeight()>=$(this)[0].scrollHeight)
                        {
                            var loadlimit = Number($("#loadmoredata").attr("data-value"));
                                    if ($("#checkbox_veg").prop("checked") == true) {
                                        var type = "veg";
                                    } else {
                                        var type = "";
                                    }
                                    $("#loadmoredata").attr("data-value",loadlimit+1);
                                    var category_id = $('.checkWrap  li.activeTab').attr("data-id");
                                    var pagedata = {
                                        "keyword": "",
                                        "categories": category_id,
                                        "sortbycost": "",
                                        "type": type,
                                        "limit": loadlimit
                                    };
                                    $.ajax({
                                        url: serviceurl + 'cuisines',
                                        type: 'POST',
                                        data: JSON.stringify(pagedata),
                                        datatype: 'JSON',
                                        async: false,
                                        success: function(data) {
                                            var value = JSON.parse(data);
                                            var html = "";
                                            if (value.cuisines.length > 0) {
                                                for (var i = 0; i < value.cuisines.length; i++) {
                                                            if(value.cuisines[i].isCustomisable == '1'){
                                                                html += '<div class="cuisine card" onclick="customizecuisine('+ value.cuisines[i].id +')">';
                                                            }else{
                                                                html += '<div class="cuisine card">';
                                                            }
                                                        
                                                            html += '<div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description + '</p><div class="priceBlock"><p class="price">â‚¹ ' + value.cuisines[i].price + '</p><p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock" id="cuisineCatDiv-' + value.cuisines[i].id + '"><div class="hurryBlockContainer">';
                                                            if(value.cuisines[i].isCustomisable == '1'){
                                                                html += '<p>Customizable</p>';
                                                            }else{
                                                                html += '<p> </p>';
                                                            }
                                                            html += ' </div><div class="hurryBlockContainer">';
                                                            var cuisintype = value.cuisines[i].type.split(",");
                                                                if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                                                    html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                                                } else {
                                                                    html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                                                }
                                                            
                                                            html += '</div></div>';
                                                            if(value.cuisines[i].isCustomisable != '1'){
                                                                html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="0" max="3" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';
                                                            }
                                                            html += '</div></div></div>';
                                                    }
                                                    if(value.cuisines.length < 30){
                                                        $("#loadmoredata").hide();
                                                    }
                                                    $("#cuisines").html(html);
                                                    addedIncart();
                                                    hoverCuisine();
                                                    addButtonFunction();
                                                    adjustHeight();
                                                    slideCat();
                                            } else {
                                                $("#loadmoredata").hide();
                                            }



                                        }
                                    });
                                }
                            })
                    }
        );

      
        function getCategories(pagetype) {
            var pagedata = {
                "pagetype": pagetype
            };
            $.ajax({
                url: serviceurl + 'homecategories',
                type: 'POST',
                data: JSON.stringify(pagedata),
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";                   
                    for (var i = 0; i < value.categories.length; i++) {
                        var id = value.categories[i].id;
                        var category = value.categories[i].category;
                            if(i == 0){
                                html += '<li class="snacks-wrapper activeTab" id="category-' + id + '" onclick="addActivetab(' + id + ')" data-id="' + id + '"><p><a><span class="text-check">' + category + '</span></span></a></p></li>';
                            }else{
                                html += '<li class="snacks-wrapper" id="category-' + id + '" onclick="addActivetab(' + id + ')" data-id="' + id + '"><p><a><span class="text-check">' + category + '</span></span></a></p></li>';
                            }

                    }
                    $("#categories").html(html);
                }
            });
        }

        function getKitchens() {

            $.ajax({
                url: serviceurl + 'kitchens',
                type: 'POST',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                     var vl = value.kitchens.length;
                    for (var i = 0; i < vl; i++) {
                       html += '<div class="restro card" onclick="getKitchesData(' + value.kitchens[i].id + ')" id="kitchenDiv-' + value.kitchens[i].id + '"><div class="restroWrapContent"><img src="' + value.kitchens[i].image + '" alt="' + value.kitchens[i].name + '" /><div class="context"><h3>' + value.kitchens[i].name + '</h3><a href="javascript:goToKitchen(' + value.kitchens[i].id + ',' + '\'' + value.kitchens[i].name1.replace("&#39;","\\'")  + '\''+')"><h6 class="visitRestro">Visit Restaurant ></h6></a></div></div></div>';
                        
                        $("#kitchens").html(html);
                    }
                }
            });
        }


        function getCuisines() {
            var category_id = $('.checkWrap  li.activeTab').attr("data-id");
            var pagedata = {
                "keyword": "",
                "categories": category_id,
                "sortbycost": "",
                "type": ""
            };
            $.ajax({
                url: serviceurl + 'cuisines',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                    if (value.cuisines.length > 0) {
                        for (var i = 0; i < value.cuisines.length; i++) {

                            if(value.cuisines[i].isCustomisable == '1'){
                                html += '<div class="cuisine card customize_cusine" onclick="customizecuisine('+ value.cuisines[i].id +')">';
                            }else{
                                html += '<div class="cuisine card">';
                            }
                           
                            html += '<div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img class="lazyload" data-src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description + '</p><div class="priceBlock"><p class="price">â‚¹ ' + value.cuisines[i].price + '</p><p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock" id="cuisineCatDiv-' + value.cuisines[i].id + '"><div class="hurryBlockContainer">';
                            if(value.cuisines[i].isCustomisable == '1'){
                                html += '<p>Customizable</p>';
                            }else{
                                html += '<p> </p>';
                            }
                            html += ' </div><div class="hurryBlockContainer">';
                            var cuisintype = value.cuisines[i].type.split(",");
                                if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                    html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                } else {
                                    html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                }
                            
                            html += '</div></div>';
                            if(value.cuisines[i].isCustomisable != '1'){
                                html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="1" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';
                            }
                            html += '</div></div></div>';
                      
                        }
                        if(value.cuisines.length < 30){
                            $("#loadmoredata").hide();
                        }
                        $("#cuisines").html(html);
                        addedIncart();
                        hoverCuisine();
                        adjustHeight();
                        slideCat();
                    } else {
                        $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");
                    }

                }
            });
        }


        function getCuisinesFilter() {
          
            var kitchenid = $("#kitchen_id").val();
            if(kitchenid != ""){
                getKitchesData(kitchenid);
                return;
            }
            var keyword = $("#search_inner").val();
            var sort_by_cost = $("#sort_by_cost").val();
            var category_id = $('.checkWrap  li.activeTab').attr("data-id");
            var keyLength = keyword.length;
            var type = "";
            if ($("#checkbox_veg").prop("checked") == true) {
                var type = "veg";
            } else {
                var type = "";
            }

            var categories = [];
            var pagedata = {
                "kitchenid": kitchenid,
                "keyword": keyword,
                "categories": category_id,
                "sortbycost": sort_by_cost,
                "type": type
            };
            $.ajax({
                url: serviceurl + 'cuisines',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: true,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                    $(".loader").hide();
                    if (value.cuisines.length > 0) {
                        for (var i = 0; i < value.cuisines.length; i++) {
                            if(value.cuisines[i].isCustomisable == '1'){
                                html += '<div class="cuisine card" onclick="customizecuisine('+ value.cuisines[i].id +')">';
                            }else{
                                html += '<div class="cuisine card">';
                            }
                           
                            html += '<div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img class="lazyload" data-src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description + '</p><div class="priceBlock"><p class="price">â‚¹ ' + value.cuisines[i].price + '</p><p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock" id="cuisineCatDiv-' + value.cuisines[i].id + '"><div class="hurryBlockContainer">';
                            if(value.cuisines[i].isCustomisable == '1'){
                                html += '<p>Customizable</p>';
                            }else{
                                html += '<p> </p>';
                            }
                            html += ' </div><div class="hurryBlockContainer">';
                            var cuisintype = value.cuisines[i].type.split(",");
                                if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                    html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                } else {
                                    html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                }
                            
                            html += '</div></div>';
                            if(value.cuisines[i].isCustomisable != '1'){
                                html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="0" max="3" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';
                            }
                            html += '</div></div></div>';
                        }
                        if(value.cuisines.length < 30){
                            $("#loadmoredata").hide();
                        }
                        $("#cuisines").html(html);
                        addedIncart();
                        hoverCuisine();
                        addButtonFunction();
                        adjustHeight();
                        slideCat();
                        
                    } else {
                        $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");
                    }


                }
            });
        }

        function addActivetab(catid) {
            var categoryid = catid;
            $('.checkWrap  li.activeTab').removeClass('activeTab');
            $("#category-"+catid).addClass("activeTab");
            getCuisinesFilter();
        }

        $("form[name='review_form']").validate({
            rules: {
                user_id: "required",
                order_id: "required",
                rating: "required",
                comment: "required",
            },
            submitHandler: function(form) {
                var user_id = $("#user_id").val();
                var order_id = $("#order_id").val();
                var comment = $("#comment").val();
                var rating = $(".rating").val();

                var pagedata = {
                    "userId": user_id,
                    "orderid": order_id,
                    "feedback": comment,
                    "rating": rating
                };
                $.ajax({
                    url: serviceurl + 'getRating',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        if (value.status == 'success') {

                            $('#reviewPopup1').hide();
                        }

                    }
                });
            }
        });

        $( "#other" ).click(function() {
            checkLoginState();
        });
        
        function getKitchesData(kitchenid) {
            $("#cuisines").html("");
            var kid = $("#kitchen_id").val();
            $("#kitchenDiv-"+kid).removeClass("activeRestro");
            $("#kitchenDiv-"+kitchenid).addClass("activeRestro");
            $("#kitchen_id").val(kitchenid);
            if ($("#checkbox_veg").prop("checked") == true) {
                var type = "veg";
            } else {
                var type = "";
            }
            var pagedata = {
                "kitchenid": kitchenid,
                "keyword": "",
                "categories": "",
                "sortbycost": "",
                "type": type,
                "mealtype": "corporate_meals"
            };
            $.ajax({
                url: serviceurl + 'kitchen_page',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var categories = "";
                    for (var i = 0; i < value.categories.length; i++) {
                        if (i == 0) {
                            categories += '<li class="snacks-wrapper activeTab"><a href="#category' + value.categories[i].categoryid + '"><p><span>' + value.categories[i].categoryname + '</span></span></p></a></li>';
                        } else {
                            categories += '<li class="snacks-wrapper"><a href="#category' + value.categories[i].categoryid + '"><p><span>' + value.categories[i].categoryname + '</span></span></p></a></li>';
                        }
                    }
                    $("#categories").html(categories);

                    var cuisinesdata = "";
                    if(value.cuisines.length > 0){
                        for (var j = 0; j < value.cuisines.length; j++) {
                            
                            cuisinesdata += '<div class="cuisine-divider" id="category' + value.cuisines[j].categoryid + '"> <div class="titleBlock"> <h2>' + value.cuisines[j].categoryname + '</h2> <p>' + value.cuisines[j].cuisines.length + ' Items</p> </div><div class="cuisineContainer">';
                        
                            for (var k = 0; k < value.cuisines[j].cuisines.length; k++) {
                                if(value.cuisines[j].cuisines[k].isCustomisable == '1'){
                                    cuisinesdata += '<div class="cuisine card customize_cusine" onclick="customizecuisine('+ value.cuisines[j].cuisines[k].id +')">';
                                }else{
                                    cuisinesdata += '<div class="cuisine card">';
                                }
                                cuisinesdata += '<div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img class="lazyload" data-src="' + value.cuisines[j].cuisines[k].image + '" alt="' + value.cuisines[j].cuisines[k].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[j].cuisines[k].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[j].cuisines[k].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[j].cuisines[k].description + '</p><div class="priceBlock"><p class="price">â‚¹ ' + value.cuisines[j].cuisines[k].price + '</p><p>Serves ' + value.cuisines[j].cuisines[k].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="hurryBlockContainer">';
                                if(value.cuisines[j].cuisines[k].isCustomisable == '1'){
                                    cuisinesdata += '<p>Customizable</p>';
                                }else{
                                    cuisinesdata += '<p> </p>';
                                }
                                cuisinesdata += '</div><div class="hurryBlockContainer">';
                                var cuisintype = value.cuisines[j].cuisines[k].type.split(",");
                                if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                    cuisinesdata += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                } else {
                                    cuisinesdata += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                }
                                cuisinesdata += '</div></div>';
                                if(value.cuisines[j].cuisines[k].isCustomisable != '1'){
                                    cuisinesdata += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[j].cuisines[k].id + '" onclick="addToCart(' + value.cuisines[j].cuisines[k].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[j].cuisines[k].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[j].cuisines[k].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[j].cuisines[k].id + '" class="quantity-input" value="1" min="1" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[j].cuisines[k].id + ',\'add\')"><h2>+</h2></a></div>';
                                }
                                cuisinesdata += '</div></div></div>';
                            }

                            cuisinesdata += '</div>';
                        }
                    }else{
                        cuisinesdata = "<div class='noCuisine'><p>No cuisine found.</p></div>";
                    }
                    $("#cuisines").html(cuisinesdata);
                    addedIncart();
                    hoverCuisine();
                    addButtonFunction();
                    adjustHeight();
                    slideCat();
                }
            });
        }

        function getOffer(){
            getCategories('1');
            var kitchenid = $("#kitchen_id").val();
            $("#kitchenDiv-"+kitchenid).removeClass("activeRestro");
            $("#kitchen_id").val('');
            getCuisines();
        }


       //Aradhana facebook login
       
       function facebookLogin()
          {
              console.log("facebook login");
           fbLogin();
       }
//=============================
//**FACEBOOK LOGIN WITH JS*
//=============================
window.fbAsyncInit = function() {
    // FB JavaScript SDK configuration and setup
    FB.init({
      appId      : '2646941222205273', // FB App ID
      cookie     : true,  // enable cookies to allow the server to access the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.11' // use graph api version 2.8
  });
   
};

// Load the JavaScript SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "/js/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Facebook login with JavaScript SDK
function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getFbUserData();
        } else {
            document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, {scope: 'email'});
}

// Fetch the user profile data from facebook
function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,name,first_name,last_name,email,link,gender,locale,picture'},
        function (response) {
            console.log(response);
            // id:response.id,
            //  name:response.name,
            //  email:response.email,
            //  fname:response.first_name,
            //  lname:response.last_name,
            //  gender:response.gender,
            //  picture:response.picture
    }
    );
}

    </script>
    
</body>

</html>