<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'head.php';
    $restroid = $_REQUEST['kitchenid']; 
    if($restroid == ""){
        echo '<script>window.location.href = "'.$baseurl.'corporate"</script>';
    }
    $query = "SELECT * FROM restro_meta_details WHERE kitchen_id = '$restroid'";
    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
    if(mysqli_num_rows($result) > 0){
        $row = mysqli_fetch_assoc($result);
         echo '<title>'.$row['title'].'</title>
                <meta name="description" content="'.$row['description'].'">
                <meta name="keywords" content="'.$row['keywords'].'">';
    }else{
        $query = "SELECT name,tagline FROM user WHERE userid = '$restroid'";
        $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
        $row = mysqli_fetch_assoc($result);
        echo '<title>'.$row['name'].'</title>
              <meta name="description" content="'.$row['tagline'].'">
              <meta name="keywords" content="'.$row['name'].'">';
    }
    ?>

    <title>Cloudkitch</title>
    <meta name="description" content="">
    <meta name="google-signin-client_id" content="287145767352-qi8asdmobui20671musqaq0jga2ru2re.apps.googleusercontent.com">
    <link rel="stylesheet" href="<?= $baseurl; ?>css/jquery-ui.css">
    <link rel="stylesheet" href="<?= $baseurl; ?>css/timepicki.css">
</head>

<body class="corporatePage">
    <h1 class="seoTag">Cloud Kitch</h1>
    <?php
        include 'header.php';
    ?>

<div class="popup popup-order">
        <div class="popup-wrapper">
            <div class="popup-close">
                <img src="<?= $baseurl; ?>images/icons/close.svg" alt="close">
            </div>
            <div class="popup-item">
                <h2 class="cuisineTitle" id="cuisineTitle1">Veg Christmas Buffet B</h2>
                <input type="hidden" id="cuisine_id">
                <input type="hidden" id="kitchen_open">
                <input type="hidden" id="kitchen_close">
                <!-- <p>Nine course mini buffet with 3 salad, 4 main dishes and 2 desserts.</p> -->
                <div class="cuisine-details">
                    <h4>What’s included:</h4>
                    <div id="cuisine-details">
                    </div>
                    <!-- <ul class="menu-list">
                            <li><p>Watermelon & Feta Salad drizzled with black tea vinaigrette (Vegetarian) </p></li>
                            <li><p>Smoked Duck Salad with roasted cashew and citrus vinaigrette </p></li>
                            <li><p>Tamarind Vinaigrette Capellini tossed with edamame and herbed cherry tomatoes (Vegetarian) </p></li>
                            <li><p>Christmas Spiced Rice with thyme roasted pumpkin and pomegranate (Vegetarian) </p></li>
                            <li><p>Roasted Winter Vegetables with charred pineapple and sweet prune sprinkles (Vegetarian) </p></li>
                            <li><p>Cinnamon Sugar Glazed Chicken with ginger flower and almond flakes </p></li>
                            <li><p>Eggless Eggnog Dory with caramelised onions and fresh cherry tomatoes </p></li>
                            <li><p>Holiday Walnut Cake with rhubarb cherry compote (Vegetarian) </p></li>
                            <li><p>Christmas Cookies with a Grain surprise (Vegetarian) </p></li>
                        </ul> -->
                    
                    <h4>Add on Packages</h4>
                    <!-- Add on Packages (Multipe select) -->
                    <div class="checkboxWrap" id="addonlist">

                    </div>


                    <h4 id="addonheading">Special Instructions</h4>
                    <textarea name="special-instrustion" class="custom-textarea" id="special-instrustion" rows="8"></textarea>
                </div>
            </div>
            <div class="popup-item">
                <img src="<?= $baseurl; ?>images/cuisine/corporate/1.jpg" alt="Cuisine" class="order-img" id="order-img_cuisin">
                <div class="cuisineType">
                    <p id="veg_val"><img src="<?= $baseurl; ?>images/icons/veg.svg" alt="Veg">4 course meal</p>
                </div>
                <div class="priceBlock">
                    <p class="price">&#8377; <span id="pop_price">1,550</span> <span class="price-gst">GST</span></p>
                    <p class="label-gray" id="mim_order">Min Order 25pax</p>
                </div>
                <div class="quantityBlock">
                    <div class="timeBlock">
                        <p class="timeStat label-gray"><span id="leadtime"></span> hours lead time</p>
                    </div>
                </div>   
                <div class="order-date-time">
                    <div class="order-date">
                        <div class="form-group form-legend">
                            <input type="text" id="datepicker" class="form-input-legend" placeholder="Select Date">
                        </div>
                    </div>
                    <div class="order-time">
                        <div class="form-group form-legend">
                            <input type="text" class="form-input-legend timepicker" id="order_time" placeholder="Select Time" />
                        </div>
                    </div>
                </div>
                <input id="teamMealMinQty1" type="hidden" class="quantity">
                <div class="order-quantity">
                    <a href="#" class="quantity-icon sub">
                        <p>-</p>
                    </a>
                    <!-- <input id="teamMealMinQty" type="text" class="quantity-input" readonly> -->
                   
                    <input id="teamMealMinQty" type="text" class="quantity-input quantity" readonly>
                    <a href="#" class="quantity-icon add active">
                        <p>+</p>
                    </a>
                </div>   
                <input type="hidden" id="validateflag" value="1">
                <div class="addBtn">
                    <p class="addToCart" id="addtocartBtn" onclick="corporatemealorder()"><img src="<?= $baseurl; ?>images/icons/addCart.svg" alt="download">ADD to cart</p>
                    <span id="quterror" style="color:red"></span>
                </div>
               
            </div>
        </div>
        <div class="bg-grey">
            <!-- <p>Terms and Conditions para will follow here if any, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p> -->
        </div>
    </div>
    <section class="section restroBanner topSection">
        <div class="restroSlider">
            <div class="restroBlock">
                <div class="restroLogo">
                    <img src="https://cloudkitch.s3.ap-south-1.amazonaws.com/food.png" alt="Market Bistro">
                </div>
                <div class="restroData">
                    <h2 id="kitchen_name">Restaurant Name</h2>
                    <p class="restroQuote" id="kitchen_tagline">Restaurant's tagline</p>
                    <div class="restroSpec">
                        <div id="veg_nonveg">
                            <p><img src="<?= $baseurl; ?>images/icons/vegi.svg" alt="Veg"> Veg</p>
                            <p><img src="<?= $baseurl; ?>images/icons/non-veg.svg" alt="Non Veg">Non Veg</p>
                        </div>
                        <div class="smText">
                            <p><img src="<?= $baseurl; ?>images/icons/star.svg" alt="star"><span>4</span> / 5</p>
                            <p>100+ Ratings</p>
                        </div>
                        <div class="smText deliverytime">
                            <p><span>46</span> mins</p>
                            <p>Delivery Time</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <!-- <div class="corporate-control-wrap">
            <div class="corporate-filter">
            <div class="corporate-filter-wrapper">
                    <input type="radio" class="filter-radio" id="corporate_meals" value="corporate_meals" name="meals_check" onchange="getCuisines();" checked>
                    <label for="corporate_meals">
                        <p>Personal Meals</p>
                    </label>
                </div>
                <div class="corporate-filter-wrapper">
                    <input type="radio" class="filter-radio" id="team_meals" value="team_meals" name="meals_check" onchange="getTeamMeals();">
                    <label for="team_meals">
                        <p>Team Meals</p>
                    </label>
                </div>
                <div class="corporate-filter-wrapper">
                    <input type="radio" class="filter-radio" id="event_meals" value="event_meals" name="meals_check" onchange="getEventMeals();">
                    <label for="event_meals">
                        <p>Event Meals</p>
                    </label>
                </div>
            </div>
        </div> -->
        <!-- <input type="hidden" id="meals_check" value="corporate_meals"> -->
        <div class="filterWrap">
            <div class="titleWrap">
                <h2 class="meal-order">Personal Orders</h2>
                <p class="meal-text">Kitchen's to Choose From</p>
            </div>
            <div class="filterBtn"></div>
            <div class="filterContainer">
                <div class="searchWrap menuSearch">
                    <form>
                        <input type="text" id="search_inner" onkeyup="getKitchesData()" placeholder="Search Hot Favourites">
                    </form>
                </div>
                <div class="buttonWrap">
                    <div class="filter-check">
                        <input class="custom-radio" type="checkbox" id="checkbox_veg" onchange="getKitchesData()" name="pure_veg" value="PURE VEG">
                        <label for="checkbox_veg" class="btn radio-label">
                            <img src="<?= $baseurl; ?>images/icons/leaf.svg" alt="download">VEG
                        </label>
                    </div>
                    <!-- <p class="btn borderBtn"><img src="<?= $baseurl; ?>images/icons/discount.svg" alt="download"><span>DELIGHTFUL OFFERS</span></p> -->
                </div>
            </div>
        </div>
        <div class="menuWrap autoHeight">
            <div class="cuisineWrap card">
                <p class="closeFilter">Close X</p>
                <h3><img src="<?= $baseurl; ?>images/icons/cuisine.svg" alt="cuisine">Cuisines</h3>
                <div class="checkWrap">
                    <ul id="categories">

                    </ul>
                </div>
            </div>
            <div class="cuisineWrapper cuisineWrappernotflex">
                <div class="cuisineContainer heightContainer">
                    <div id="cuisines"></div>
                </div>
            </div>
        </div>
        <div class="restroBar" id="promotionBannerDiv">
            <div class="cuisine-divider"> 
                <div class="titleBlock"> 
                    <h2>Need more options?</h2> 
                    <p>Try our other restaurants.</p> 
                </div>
            </div> 
            <div class="restroBox restroBoxSlider">
                <div class="restroInnerSlider" id="promotionbanner"> 
                    
                </div>
            </div>
        </div>
    </section>
    <?php
            include 'footer.php';
    ?>

    <script src="<?= $baseurl; ?>js/jquery-ui.js"></script>
    <script src="<?= $baseurl; ?>js/timepicki.js"></script>
    <script>
        $(function() {
            $('#datepicker').datepicker();
            $(".timepicker").timepicki();
          
        });
        $(document).ready(function() {
            getPromotionBanner();
            getKitchesData();
           
        });

        function getKitchesData() {
            
            var kitchenid = "<?=$restroid?>";
            var mealtype = $("#meals_check").val();
            if ($("#checkbox_veg").prop("checked") == true) {
                var type = "veg";
            } else {
                var type = "";
            }
            keyword = "";
            var keyword = $("#search_inner").val();
            var pagedata = {
                "kitchenid": kitchenid,
                "keyword": keyword,
                "categories": "",
                "sortbycost": "",
                "type": type,
                "mealtype":mealtype
            };
            $.ajax({
                    url: serviceurl + 'kitchen_page_party',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        if(value.categories.length == 0){
                        $("#search_inner").css("display","none");
                        $(".filter-check").css("display","none");
                        $(".cuisineWrap").css("display","none");
                        $(".titleWrap").css("display","none");
                        $("#cuisines").html("");
                        $(".menuSearch").html('<span class="infoText"><img src="<?= $baseurl; ?>images/icons/missing.svg" alt="missing"> No cuisines have been added yet.</span>');
                        return ;
                     }
                     $(".cuisineWrap").css("display","block");
                        $(".restroLogo").html('<img src="' + value.kitchen[0].image + '" alt="'+value.kitchen[0].name+'">');
                        $("#kitchen_name").html(value.kitchen[0].name);
                        $("#kitchen_tagline").html(value.kitchen[0].tagline);
                        if ((value.kitchen[0].type) == 1) {
                            $("#veg_nonveg").html('<p><img src="<?= $baseurl; ?>images/icons/vegi.svg" alt="Veg">Veg</p>');
                        } else if ((value.kitchen[0].type) == 2) {
                            $("#veg_nonveg").html('<p><img src="<?= $baseurl; ?>images/icons/non-veg.svg" alt="Non Veg">Non Veg</p>');
                        } else {
                            $("#veg_nonveg").html('<p><img src="<?= $baseurl; ?>images/icons/vegi.svg" alt="Veg">Veg</p><p><img src="<?= $baseurl; ?>images/icons/non-veg.svg" alt="Non Veg">Non Veg</p>');
                        }

                        var ratingimg = "";
                        for (var i = 0; i < value.kitchen[0].rating; i++) {
                            ratingimg += '<img src="<?= $baseurl; ?>images/icons/star.svg" alt="star">';
                        }
                        $(".rating").html('<p>' + ratingimg + '<span>' + value.kitchen[0].rating + '</span> / 5</p><p>' + value.kitchen[0].ratingcount + '+ Ratings</p>');
                        $(".deliverytime").html('<p><span>' + value.kitchen[0].delivery_time + '</span>&nbsp; mins</p><p>Delivery Time</p>');
                        
                        var categories = "";
                        for (var i = 0; i < value.categories.length; i++) {
                            if (i == 0) {
                                categories += '<li class="snacks-wrapper activeTab"><a href="#category' + value.categories[i].categoryid + '"><p><span>' + value.categories[i].categoryname + '</span></span></p></a></li>';
                            } else {
                                categories += '<li class="snacks-wrapper"><a href="#category' + value.categories[i].categoryid + '"><p><span>' + value.categories[i].categoryname + '</span></span></p></a></li>';
                            }
                        }
                        $("#categories").html(categories);
                        console.log(mealtype);
                        
                            var cuisinesdata ="";
                            if(value.cuisines.length >0){
                                
                                    for (var j = 0; j < value.cuisines.length; j++) {
                                        cuisinesdata += '<div class="cuisine-divider" id="category' + value.cuisines[j].categoryid + '"> <div class="titleBlock"> <h2>' + value.cuisines[j].categoryname + '</h2> <p>' + value.cuisines[j].cuisines.length + ' Items</p> </div><div class="cuisineContainer">';
                                    
                                        for (var k = 0; k < value.cuisines[j].cuisines.length; k++) {
                                            
                                            cuisinesdata += '<div class="cuisine card" onclick="corporate_order(\'' + value.cuisines[j].cuisines[k].id + '\')"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[j].cuisines[k].image + '" alt="' + value.cuisines[j].cuisines[k].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[j].cuisines[k].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[j].cuisines[k].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[j].cuisines[k].description + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[j].cuisines[k].price + '<span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[j].cuisines[k].minorderquantity + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[j].cuisines[k].leadtime + ' hours lead time</p></div><div class="hurryBlockContainer">';
                                            
                                            var cuisintype = value.cuisines[j].cuisines[k].type.split(",");
                                            if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                                cuisinesdata += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                            } else {
                                                cuisinesdata += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                            }
                                            cuisinesdata += '</div></div></div></div></div>';
                                        
                                        }

                                        cuisinesdata += '</div>';
                                    }
                                                           
                                $("#cuisines").html(cuisinesdata);
                                addedIncart();
                                adjustHeight();
                            }else{
                                $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");
                            }    
                       
                    }
                });
            

        }

        function getPromotionBanner(){
            var kitchenid = "<?= $restroid ?>";
            var pagedata = {
                "kitchenid": kitchenid
            };
            $.ajax({
                url: serviceurl + 'promotionbanner',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";        
                    if(value.banner.length > 0){
                        for (var i = 0; i < value.banner.length; i++) {
                            html += '<div class="restro card"><a href="javascript:goToKitchenParty(' + value.banner[i].promoted_to + ',' + '\'' + value.banner[i].name1.replace("&#39;","\\'") + '\''+')" tabindex="0"><div class="restroWrapContent"><img src="' + value.banner[i].image + '" alt=""><div class="context"><h3>' + value.banner[i].name + '</h3><div class="ratingWrap"><ul><li><img src="/cloudkitch-web/images/icons/star.svg" alt=""></li><li><img src="/cloudkitch-web/images/icons/star.svg" alt=""></li><li><img src="/cloudkitch-web/images/icons/star.svg" alt=""></li><li><img src="/cloudkitch-web/images/icons/star.svg" alt=""></li><li><img src="/cloudkitch-web/images/icons/blank-star.svg" alt=""></li></ul></div><p>' + value.banner[i].tagline + '</p></div></div></a></div>';
                        }
                        $("#promotionbanner").html(html);
                    }else{
                        $("#promotionBannerDiv").hide();
                    }        
                    
                }
            });
        
        }

        function goToKitchenParty(kitchenid, kitchenname){
            // alert(kitchenid);
            $('<form action="<?=$baseurl?>party-kitchen/' + kitchenname + '" method="POST">' +
            '<input type="hidden" name="kitchenid" value="' + kitchenid + '" />' +
            '</form>').appendTo('body').submit();
        }
        
        function corporate_order(id){
            var pagedata = {
                "id": id
            };
            $.ajax({
                url: serviceurl + 'corporatemeals_detail',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    console.log(value.cuisines[0].name);
                    var val = "";
                    $("#cuisine_id").val(value.cuisines[0].id);
                    $("#kitchen_open").val(value.cuisines[0].kitchen_open);
                    $("#kitchen_close").val(value.cuisines[0].kitchen_close);

                    if (value.cuisines[0].type.includes('Non Veg')) {
                        val = '<img src="images/icons/nonveg.svg" alt="Non Veg">' + value.cuisines[0].kitchen_name;
                    } else {
                        val = '<img src="images/icons/veg.svg" alt="Veg">' + value.cuisines[0].kitchen_name;
                    }
                    $("#cuisineTitle1").html(value.cuisines[0].name);
                    $("#order-img_cuisin").attr("src", value.cuisines[0].image);
                    $("#veg_val").html(val);
                    $("#cuisine-details").html(value.cuisines[0].description);
                    $("#pop_price").html(value.cuisines[0].price);
                    $("#mim_order").html("Min Order " + value.cuisines[0].minorderquantity + "pax");
                    $("#leadtime").html(value.cuisines[0].leadtime);
                    $("#teamMealMinQty").val(value.cuisines[0].minorderquantity);
                    $("#teamMealMinQty1").val(value.cuisines[0].minorderquantity);
                    // $("#teamMealMinQty").min(value.cuisines[0].minorderquantity );
                    var addon = "";
                    // for (var i = 0; i < value.cuisines[0].addon.length; i++) {
                    //     addon += '<label class="container">';
                    //     addon += '<p>' + value.cuisines[0].addon[i].addon + ' &#8377;' + value.cuisines[0].addon[i].price + '</p>';
                    //     addon += '<input type="checkbox" class="addons_corporate" name="addon" data-id="' + value.cuisines[0].addon[i].add_id + '" value="' + value.cuisines[0].addon[i].add_id + '">';
                    //     addon += '<span class="checkmark"></span>';
                    //     addon += '</label>';
                    // }
                        var addonshtml = "";
                    if(value.cuisines[0].sections.length == 0){
                        $("#addonheading").hide();
                    }else{
                        $("#addonheading").show();
                    }    
                    for(var i=0;i<value.cuisines[0].sections.length;i++){
                        console.log(value.cuisines[0].sections[i].title);
                        var section_id = value.cuisines[0].sections[i].section_id;
                        var noofchoice = value.cuisines[0].sections[i].noofchoice;
                        addonshtml += '<h4 >'+value.cuisines[0].sections[i].title+'</h4><input id="da'+section_id+'" type="hidden" value="'+noofchoice+'"><input id="das'+section_id+'" type="hidden" value="'+noofchoice+'"><span id="counterror'+section_id+'" style="color:red;display:none">error</span><div id="prt'+section_id+'">';
                        var addon = value.cuisines[0].sections[i].addons;
                        

                        if(value.cuisines[0].sections[i].choicetype == 1){
                            addonshtml += '<div class="section-div" id="'+section_id+'"></div>';
                            for(var j=0;j<addon.length;j++){

                                addonshtml += '<label class="container">';
                                addonshtml += '<p>' + addon[j].detail + ' &#8377;' + addon[j].price + '</p>';
                                addonshtml += '<input type="checkbox" onclick="checkval('+section_id+','+addon[j].addon_id+')" id="'+addon[j].addon_id+'" class="addons_corporate" name="addon"  data-id="' + addon[j].addon_id + '" value="' + addon[j].addon_id + '">';
                                addonshtml += '<span class="checkmark"></span>';
                                addonshtml += '</label>';
                            }

                        }else{
                            addonshtml += '<div class="section-divs"></div>';
                            for(var j=0;j<addon.length;j++){
                                    var checked = "";
                                    if(j==0){
                                        checked = "checked";
                                    }
                                    addonshtml +=   ' <div class="checkboxWrap">';
                                    addonshtml +=    '<label class="container radioContainer">';
                                    addonshtml +=        '<p>' + addon[j].detail + ' &#8377;' + addon[j].price + '</p>';
                                    addonshtml +=       '<input type="radio" name="addOnPacagesRadio-'+ value.cuisines[0].sections[i].section_id +'" value="' + addon[j].addon_id + '" ' + checked + '>';
                                    addonshtml +=        '<span class="checkmark"></span>';
                                    addonshtml +=    '</label>';
                                    addonshtml +=  '</div>'; 
                            }

                        }
                        addonshtml +=  '</div>'; 
                    }

                    $("#addonlist").html(addonshtml);
                    $("#datepicker").val("");
                    $("#order_time").val("");
                    $("#quterror").hide();
                    $("#quterror").text("");
                    $(".popup-order, .overlay")
                        .delay(400)
                        .fadeIn();

                }
            });

        }

        function corporatemealorder() {
            
            var cuisine_id = $("#cuisine_id").val();
            var date = $("#datepicker").val();
            var time = $("#order_time").val();
            var qty = parseInt($("#teamMealMinQty").val());
            var qty1 = parseInt($("#teamMealMinQty1").val());
            var numItems = $('.section-div').length
           
            if(numItems > 0){
                $('.section-div').each(function(){
                   var section_id = $(this).prop('id');
                   var minreq = $("#da"+section_id).val(); 
                   var qut1 = $("#das"+section_id).val();
                  
                    if(qut1 != 0){
                        $("#counterror"+section_id).css("display","block");
                        $("#counterror"+section_id).text("Minimum selection should be "+minreq+".");
                        $("#validateflag").val(1);
                    }else{
                        $("#counterror"+section_id).css("display","none");
                        $("#validateflag").val(0);
                    }
                   
                });
            }else{
                $("#validateflag").val(0);
            }

            var leadtime = parseInt($("#leadtime").html());
            var today = new Date();
            var minord = new Date(today.setHours(today.getHours()+leadtime));
            var orddate = date + ' ' + time;
            orddate = new Date(orddate);

            var min = new Date(date + ' ' + $("#kitchen_open").val());
            var max = new Date(date + ' ' + $("#kitchen_close").val());

            if(qty1 > qty || date == "" || time == ""){
                if(date == ""){
                    $("#quterror").show();
                    $("#quterror").text("Please select date.");
                }else if( time == ""){
                    $("#quterror").show();
                    $("#quterror").text("Please select time.");
                }else if(qty1 > qty){
                    $("#quterror").show();
                    $("#quterror").text("Min Order "+qty1+"pax");
                }
                return;
            }else if(minord.getTime() > orddate.getTime()){
                $("#quterror").show();
                $("#quterror").text("Order time should be greater than leadtime");
                return;
            }else  if(min.getTime() > orddate.getTime() || orddate.getTime() >  max.getTime()){
                $("#quterror").show();
                $("#quterror").text("Order time should be between "+ $("#kitchen_open").val() +" and "+ $("#kitchen_close").val());
                return;
            }else{
                $("#quterror").hide();
            }

            var special_instruction = $("#special-instrustion").val();

            var items = $(".addons_corporate");
            var selectedaddons = "";
            for (var i = 0; i < items.length; i++) {
                if (items[i].type == 'checkbox' && items[i].checked == true){
                        selectedaddons += items[i].value + ",";
                }
            }

            $('#addonlist input[type=radio]:checked').each(function() {
                 selectedaddons += this.value + ",";
            });

            addToCartCorporate(cuisine_id, date, time, qty, special_instruction, selectedaddons);
            // alert(special_instruction);

        }

        function addToCartCorporate(id, date, time, qty, special_instruction, selectedaddons) {
            var count = 1;
            var data = {
                "id": id,
                "date": date,
                "time": time,
                "qty": qty,
                "special_instruction": special_instruction,
                "selectedaddons": selectedaddons
            };
            if($("#validateflag").val() == 0){
                    $.ajax({
                    url: serviceurl + 'addtocartCorporate',
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        if (value.status == 'success') {
                            var cartCount = $(".notiCount").html();
                            showCartData();
                            $(".popup-order, .overlay")
                            .delay(400)
                            .fadeOut();
                            //$(".notiCount").html(parseInt(cartCount) + 1);
                            $(".toast").fadeIn();
                            setTimeout(function(){
                                $(".toast").fadeOut();
                            }, 3000);
                        } else {

                            $('.overlay,.cartPopup').fadeIn();
                            setTimeout(function() {
                                $('.overlay,.cartPopup').fadeOut();
                            }, 1200);
                        }
                    }
                });
            }
            else{
                // $("#quterror").text("Min Order "+qty1+"pax");
            }
        }

        function checkval(s,z){
            qut1 = $("#da"+s).val();
            qut = $("#da"+s).val();
            var q = "#das"+s;
            var qt = $(q).val();   
            var prt = "#prt"+s;   
            var ckId =  "#"+z;
            
                // alert($(q).val());
                if($(ckId).is(":checked")){
                    $(q).val(parseInt(qt) - 1);
                }
                else if($(ckId).is(":not(:checked)")){
                    $(q).val(parseInt(qt) + 1);
                    
                }
                
                if($(q).val() == 0 ){
                    $("#validateflag").val(0);
                    $("#counterror"+s).css("display","none");
                }
                else{
                    $("#counterror"+s).css("display","block");
                    $("#counterror"+s).text("Minimum selection should be "+qut1+".");
                    $("#validateflag").val(1);
                }
                        
        }

    </script>

</body>

</html>