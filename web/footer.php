<footer class="footer">
    <div class="footerLogoWrap">
        <img src="<?= $baseurl; ?>images/logo.png" alt="Cloudkitch" class="logo">
        <p>Great food experience is about thoughtful preparation. CloudKitch makes that happen for you.</p>
    </div>
    <div class="footer-wrapper">
        <div class="footerCol">
            <ul class="footer-link">
                <li>
                    <a href="<?= $baseurl; ?>partners/" target="_blank">
                        <p>About Us</p>
                    </a>
                </li>
                <li>
                    <a href="<?= $baseurl; ?>partners/" target="_blank">
                        <p>Partner with us</p>
                    </a>
                </li>
                <li>
                    <a href="<?= $baseurl; ?>partners/#footer" target="_blank">
                        <p>Contact Us</p>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="linkCorporate">
                        <p>Special Events Enquiry</p>
                    </a>
                </li>
            </ul>
            <ul class="social-link">
                <li>
                    <a href="https://www.facebook.com/officialcloudkitch/" target="_blank">
                        <p><img src="<?= $baseurl; ?>images/icons/fb-icon.svg" alt="facebook"></p>
                    </a>
                </li>
                <!-- <li>
                    <a href="https://twitter.com/CloudKitch" target="_blank">
                        <p><img src="<?= $baseurl; ?>images/icons/icon-twitter.svg" alt="twitter"></p>
                    </a>
                </li> -->
                <li>
                    <a href="https://www.instagram.com/cloudkitch/" target="_blank">
                        <p><img src="<?= $baseurl; ?>images/icons/icon-instagram.svg" alt="instagram"></p>
                    </a>
                </li>
                <li>
                    <a href="https://www.linkedin.com/company/cloudkitch-private-limited" target="_blank">
                        <p><img src="<?= $baseurl; ?>images/icons/icon-linkedin.svg" alt="linkedin"></p>
                    </a>
                </li>
            </ul>
        </div>
        <div class="footerCol">
            <ul class="footer-link" id="favouriteResto">
                <li>
                    <p class="subtitle">Your Favourite Restaurants</p>
                </li>
                <li>
                    <a href="#">
                        <p>Sun Moon & Potatoes</p>
                    </a>
                </li>
                <li>
                    <p class="subtitle">Serving Soon</p>
                </li>
                <li>
                    <a href="#">
                        <p>India Bistro Express</p>
                    </a>
                </li>
            </ul>
        </div>
        <div class="footerCol">
            <ul class="footer-link">
                <li>
                    <p class="subtitle">To Place Your Order Call</p>
                </li>
                <li>
                    <a href="tel:917710999666">
                        <p>+91 771 099 9666</p>
                    </a>
                </li>
                <li>
                    <p class="subtitle">Take away</p>
                </li>
                <li>
                    <a href="mailto:enquiry@cloudkitch.co.in">
                        <p>enquiry@cloudkitch.co.in</p>
                    </a>
                </li>
            </ul>
        </div>
        <div class="footerCol">
            <ul>
                <li>
                    <a href="<?= $baseurl; ?>terms-and-conditions/">
                        <p>Terms & Conditions</p>
                    </a>
                </li>
                <li>
                    <a href="<?= $baseurl; ?>grievance-policy/">
                        <p>Grievance Policy</p>
                    </a>
                </li>
                <li>
                    <a href="<?= $baseurl; ?>cookie-policy/">
                        <p>Cookie Policy</p>
                    </a>
                </li>
                <li>
                    <a href="<?= $baseurl; ?>cancellation-and-refund/">
                        <p>Cancellation & Refund</p>
                    </a>
                </li>
                <li>
                    <a href="<?= $baseurl; ?>sitemap/">
                        <p>Sitemap</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</footer>



<script src="<?= $baseurl; ?>js/jquery.js"></script>
<script defer src="<?= $baseurl; ?>js/slick.js"></script>
<script defer src="<?= $baseurl; ?>js/matchHeight.js"></script>
<script  defer src="<?= $baseurl; ?>js/custom.js" ></script>
<script src="<?= $baseurl; ?>js/jquery.validate.min.js"></script>
<script defer src="<?= $baseurl; ?>js/platform.js"  ></script>
<script  src="<?= $baseurl; ?>js/lazysizes.min.js" ></script>
<script>startApp();</script>      
<script src="https://connect.facebook.net/en_US/sdk.js"></script> 
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?= $baseurl; ?>js/timepicki.js"></script>

<script>
     $(function() {
            $('#datepicker').datepicker({
                minDate: 0
            });
            $(".timepicker").timepicki();
        });
    function hoverCuisine(){
        $('.cuisine').on('mouseenter', function() {
            $(this).find('.hurryBlock').fadeOut();

            var d = new Date();
            var datesss = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
            var min = new Date(datesss + ' ' + '10:45 AM');
            var max = new Date(datesss + ' ' + '11:30 PM');

            if(0){
                $(this).find('.addBtn').delay(200).fadeIn();
                $(this).find('.addToCart').html("Kitchen closed");
                $("#customizecuisinebtntitle").html("Kitchen closed");
                $(this).find('.addToCart').removeAttr('onclick');
                $(this).find('.addBtn').unbind();
                $(this).find('.cuisineQty').fadeOut();
                $(this).find(".cuisineQty").remove();
                // $('.cuisineQty').hide();
                
                // $("#addtocartbtn").hide();
            }else{ 
                if($(this).find('.cuisineQty').is(':visible')){
                    $(this).find('.addBtn').delay(200).fadeOut();
                }else{
                    $(this).find('.addBtn').delay(200).fadeIn();
                }
            }


        });

        $('.cuisine').on('mouseleave', function() {
            $(this).find('.addBtn').fadeOut();
            if($(this).find('.cuisineQty').is(':visible')){

            }else{
                $(this).find('.hurryBlock').delay(200).fadeIn();
            }
            

        });

        
    }


    function addButtonFunction()
    {
        $(".addBtn").on("click", function() {
            // $(this).find("p").text("Added To Cart.");
            $(this).parent().find('.hurryBlock').fadeOut();
        });

        $(".quantityBlock").on("click", function() {
            $(this)
            .find(".addBtn")
            $(this)
            .find(".cuisineQty")
            .css("display", "flex");
        });

    }

    $(document).ready(function() {
        showCartData();
        showFavouriteResto();
        var aradhana = 0;
    });

   /*
        By:Jyoti Vishwakarma
        Description: js for search from header
        */
    $("#search_main").keypress(function(event) { 
       if (event.which == 13) {
            var serchKeyword = $("#search_main").val();
            if (serchKeyword != "") {
                var mealtype = $("#meals_check").val();
                $('<form action="<?=$baseurl?>search-result.php" method="POST">' +
                '<input type="hidden" name="keyword" value="' + serchKeyword + '" />' +
                '<input type="hidden" name="mealtype" value="' + mealtype + '" />' +
                '</form>').appendTo('body').submit();
            }
       }
    });

    function searchCusine() {
        var serchKeyword = $.trim($("#mobSearch").val());
        serchKeyword = serchKeyword.replace(/\s+/g, '-');
        if (serchKeyword != "") {
            window.location.href = 'search-result.php?keyword=' + serchKeyword;
        }
    }

    /*
    By:Jyoti Vishwakarma
    Description: Pincode form
    */
    $("form[name='pincode_form']").validate({
        rules: {
            pincode: "required"
        },
        submitHandler: function(form) {
            var pincode = $("#pincode").val();
            var pagedata = {
                "pincode": pincode
            };
            $.ajax({
                url: serviceurl + 'pincode',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    if (value.status == 'success') {
                        location.reload();
                    } else {
                        $(".firstDiv").fadeOut();
                        $(".secDiv").fadeIn();
                        // alert(value.msg);
                    }
                }
            });
        }
    });

    /*
    By:Jyoti Vishwakarma
    Description: Signup
    */
    $("form[name='signup_form']").validate({
        rules: {
            sign_up_fullname: "required",
            sign_up_phone: "required",
            sign_up_email: "required",
            sign_up_password: "required",
            sign_up_confirmpassword: {
                required: true,
                equalTo: '[name="sign_up_password"]'
            }
        },
        submitHandler: function(form) {
            var name = $("#sign_up_fullname").val();
            var email = $("#sign_up_email").val();
            var phone = $("#sign_up_phone").val();
            var password = $("#sign_up_password").val();
            var corporatecode = $("#sign_up_corporate").val();

            var pagedata = {
                "name": name,
                "email": email,
                "phone": phone,
                "password": password,
                "profpic": "",
                "signuptype": "web",
                "socialtype": "",
                "authid": "",
                "devicetype": "",
                "devicetoken": "",
                "sign_up_corporate": corporatecode
            };
            $.ajax({
                url: serviceurl + 'registration',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    if (value.status == 'success') {
                        $(".form-signup").fadeOut();
                        $(".signup-success")
                        .delay(400)
                        .fadeIn();
                        $(".overlay,.popup-login")
                        .delay(3000)
                        .fadeOut();
                        $(".signup-success")
                        .delay(3500)
                        .fadeOut();
                        $(".form-signup")
                        .delay(3500)
                        .fadeIn();
                        $(".normal-loginlink").fadeOut();
                        setTimeout(function(){ 
                            if (value.isCorporateUser == '1') {
                                window.location.href = "<?=$baseurl?>corporate.php";
                            } else {
                                location.reload();
                            }
                         }, 2000);
                        $("#signup_alret_msg").hide();
                        $("#sign_up_fullname").val('');
                        $("#sign_up_email").val('');
                        $("#sign_up_phone").val('');
                        $("#sign_up_password").val('');
                        $("#sign_up_corporate").val('');
                        $("#sign_up_confirmpassword").val('');
                    } else {
                        $("#signup_alret_msg").html(value.msg);
                        $("#signup_alret_msg").show();
                        // $(".sign-up").hide();
                        // $('.overlay, .loginFormPopups').fadeIn();
                        // setTimeout(function() {
                        //     $('.overlay, .loginFormPopups').fadeOut();
                        // }, 1200);
                        // alert(value.msg);
                    }

                    
                }
            });
        }
    });

        /*
    By:Jyoti Vishwakarma
    Description: manual login
    */
    $("form[name='login_form']").validate({
        rules: {
            sign_in_email: "required",
            sign_in_password: "required"
        },
        submitHandler: function(form) {
            var emailorphone = $("#sign_in_email").val();
            var password = $("#sign_in_password").val();
            var pagedata = {
                "email": emailorphone,
                "authid": "",
                "phone": "",
                "password": password,
                "logintype": "web"
            };
            $.ajax({
                url: serviceurl + 'login',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    if (value.status == 'success') {
                        if (value.isCorporateUser == '1') {
                            // window.location.href = "<?=$baseurl?>corporate.php";
                            var currenturl = window.location.href;
                            if(currenturl.includes("cart")){
                                location.reload();
                            }else{
                                window.location.href = "<?=$baseurl?>corporate.php";
                            }
                            
                        } else {
                            location.reload();
                        }

                    } else {
                        $("#signin_alret_msg").html(value.msg);
                        $("#signin_alret_msg").show();
                        // alert(value.msg);
                    }
                }
            });
        }
    });

    $("form[name='corporate_login_form']").validate({
        rules: {
            corp_sign_in_email: "required",
            corp_sign_in_password: "required"
        },
        submitHandler: function(form) {

            var email = $("#corp_sign_in_email").val();
            var password = $("#corp_sign_in_password").val();
            var pagedata = {
                "email": email,
                "password": password,
                "logintype": "web"
            };
            $.ajax({
                url: serviceurl + 'corporatelogin',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    if (value.status == 'success') {
                        //location.reload();
                        window.location.href = "corporate.php";
                    } else {
                        alert(value.msg);
                    }
                }
            });
        }
    });

    /*
    By:Jyoti Vishwakarma
    Description: logout
    */
    function logoutWeb() {
        $.ajax({
            url: serviceurl + 'logout',
            type: 'POST',
            datatype: 'JSON',
            async: false,
            success: function(data) {
                var auth2 = gapi.auth2.getAuthInstance();
                auth2.signOut().then(function () {   
                console.log('User signed out.');   
                auth2.disconnect();   
                }); 
                auth2.disconnect();
                window.location.href = "<?= $baseurl ?>";
            }
        });
    }

    /*
    By:Jyoti Vishwakarma
    Description: add to cart cuisine
    */
    function addToCart(id) {
        // var count = $("#itemcount"+id).val();
        var count = 1;
        var data = {
            "id": id,
            "count": count,
            "selectedaddons": ""
        };
        $.ajax({
            url: serviceurl + 'addtocart',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            datatype: 'JSON',
            async: false,
            success: function(data) {
                var value = JSON.parse(data);
                if (value.status == 'success') {
                    var cartCount = $(".notiCount").html();
                    $(".notiCount").html(parseInt(cartCount) + 1);
                    $(".toast").fadeIn();
                    setTimeout(function(){
                        $(".toast").fadeOut();
                      }, 3000);
                      $("#addedTocart-"+id).parent('.addBtn').delay(200).fadeOut();
                } else {
                   
                    $('.overlay,.cartPopup').fadeIn();
                    setTimeout(function() {
                        $('.overlay,.cartPopup').fadeOut();
                    }, 1200);
                }
            }
        });
    }
        /*
    By:Jyoti Vishwakarma
    Description: add to cart customize cuisine
    */
    function addToCartCustomize(id){
        var items = $(".addons_corporate");
        var selectedaddons = "";
        for (var i = 0; i < items.length; i++) {
            if (items[i].type == 'checkbox' && items[i].checked == true)
                    selectedaddons += items[i].value + ",";
        }

        $('#cuisineCustomDeatail input[type=radio]:checked').each(function() {
                selectedaddons += this.value + ",";
        });

        // alert(selectedaddons);
        var count = 1;
        var data = {
            "id": id,
            "count": count,
            "selectedaddons":selectedaddons
        };
        $.ajax({
            url: serviceurl + 'addtocart',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            datatype: 'JSON',
            async: false,
            success: function(data) {
                var value = JSON.parse(data);
                if (value.status == 'success') {
                    var cartCount = $(".notiCount").html();
                    // $(".notiCount").html(parseInt(cartCount) + 1);
                    showCartData();
                    $(".toast").fadeIn();
                    setTimeout(function(){
                        $(".toast").fadeOut();
                      }, 3000);
                    var currenturl = window.location.href;
                        if(currenturl.includes("cart")){
                            getCuisines();
                        }

                } else {
                    $('.overlay,.cartPopup').fadeIn();
                    setTimeout(function() {
                        $('.overlay,.cartPopup').fadeOut();
                    }, 1200);
                }
            }
        });
    }
        /*
    By:Jyoti Vishwakarma
    Description: cart count
    */
    function showCartData()
    {
        $.ajax({
            url: serviceurl + 'showcartdata',
            type: 'POST',
            async: false,
            success: function(data) {
                var value = JSON.parse(data);
                $(".notiCount").html(value.count);
            }
        });
    }
        /*
    By:Jyoti Vishwakarma
    Description: favourite restaurant list
    */
    function showFavouriteResto(){
        $.ajax({
            url: serviceurl + 'kitchens',
            type: 'POST',
            async: false,
            success: function(data) {
                var value = JSON.parse(data);
                var html = "";
                <?php if(isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] == '1'){  ?>
                    html += '<li><p class="subtitle">Your Favourite Restaurants</p></li>';
                    for (var i = 0; i < 6; i++) {
                        html += '<li><a href="javascript:goToKitchenCorporate(' + value.kitchens[i].id + ',' + '\'' + value.kitchens[i].name1.replace("&#39;","\\'") + '\''+')"><p>' + value.kitchens[i].name + '</p></a></li>';
                        $("#favouriteResto").html(html);
                    }
                <?php    }else{     ?>
                    html += '<li><p class="subtitle">Your Favourite Restaurants</p></li>';
                    for (var i = 0; i < 6; i++) {
                        html += '<li><a href="javascript:goToKitchen(' + value.kitchens[i].id + ',' + '\'' + value.kitchens[i].name1.replace("&#39;","\\'") + '\''+')"><p>' + value.kitchens[i].name + '</p></a></li>';
                        $("#favouriteResto").html(html);
                    }
                <?php    }  ?>
                
                var html1 = "";
                html1 += '<li><p class="subtitle">Your Favourite Restaurants</p></li>';
                for (var i = 0; i < value.kitchens.length; i++) {
                    html1 += '<li><a href="javascript:goToKitchen(' + value.kitchens[i].id + ',' + '\'' + value.kitchens[i].name1.replace("&#39;","\\'") + '\''+')"><p>' + value.kitchens[i].name + '</p></a></li>';
                    $("#siteMapResto").html(html1);
                }
            }
        });
    }

        /*
    By:Jyoti Vishwakarma
    Description: redirect to kitchen
    */
    function goToKitchen(kitchenid, kitchenname){
        // alert(kitchenid);
        var mealtype = $("#meals_check").val(); 
        $('<form action="<?=$baseurl?>kitchen/' + kitchenname + '" method="POST">' +
        '<input type="hidden" name="kitchenid" value="' + kitchenid + '" />' +
        '<input type="hidden" name="mealtype" value="' + mealtype + '" />' +
        '</form>').appendTo('body').submit();
    }

    function goToKitchenCorporate(kitchenid, kitchenname){
        // alert(kitchenid);
        $('<form action="<?=$baseurl?>corporate-kitchen/' + kitchenname + '" method="POST">' +
        '<input type="hidden" name="kitchenid" value="' + kitchenid + '" />' +
        '</form>').appendTo('body').submit();
    }

    /*
    By:Jyoti Vishwakarma
    Description: display quantity bar on cuisine present in cart
    */
    function addedIncart() {

        $.ajax({
            url: serviceurl + 'addedincart',
            type: 'POST',
            async: false,
            success: function(data) {
                // $("#addedTocart-45").html("Added To Cart.")
                var response = JSON.parse(data);
                for (var i = 0; i < response.cartitem.length; i++) {

                    // $("#addedTocart-" + response.cartitem[i]).text("Added To Cart.");
                    $("#addedTocart-" + response.cartitem[i].id).hide();
                    $("#upadteQuantity-" + response.cartitem[i].id).css("display", "flex");
                    $("#cuisineCatDiv-"+ response.cartitem[i].id).hide();
                    $("#cuisineQty-"+ response.cartitem[i].id).val(response.cartitem[i].qty);
                }
            }
        });
    }

    /*
    By:Jyoti Vishwakarma
    Description: google login
    */
    function savegooglelogin() {

        var corporatecode = $("#autologin_corp_code").val();
        console.log(corporatecode);
        var name = $("#google_name").val();
        var email = $("#google_email").val();
        if(email == ""){
            return false;
        }
        var phone = "";
        var password = "";
        var profilepic = $("#google_imgurl").val();
        var googleid = $("#google_id").val();
        var social_type =  $("#social_type").val();
        console.log(profilepic);
        var pagedata = {
            "name": name,
            "email": email,
            "phone": phone,
            "password": password,
            "profpic": encodeURI(profilepic),
            "signuptype": "web",
            "socialtype": social_type,
            "authid": googleid,
            "devicetype": "",
            "devicetoken": "",
            "sign_up_corporate": corporatecode
        };
        var pagedata1 = {
            "name": name,
            "email": email,
            "phone": phone,
            "password": password,
            "profpic": encodeURI(profilepic),
            "signuptype": "web",
            "socialtype": social_type,
            "authid": googleid,
            "devicetype": "",
            "devicetoken": "",
            "sign_up_corporate": corporatecode
        };
        var userid = '<?php if (isset($_SESSION['userid'])) {   echo $_SESSION['userid'];  } ?>';
        if (userid == "") {
            //alert("herte");
            $.ajax({
                url: serviceurl + 'registration',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    if (value.status == 'success') {
                        location.reload();
                        // window.location.href = "<?= $baseurl ?>";
                    } else {
                        alert(value.msg);
                    }
                }
            });
        }
    }

    $("form[name='specialenquiry']").validate({
        rules: {
            corpFullName: "required",
            corpFullEmail: "required",
            corpFullPhone: "required",
            corpFullCompany: "required",
            corpEventDesc: "required",
            corpNoGuest: "required",
            corpFullBudget: "required",
            corpdatepicker: "required",
            corptimepicker: "required",
            corpCatered: "required",
            corpRestrictions: "required",
            corpRemarks: "required",
            
        },
        submitHandler: function(form){
            $("#enquirySubmitBtn").prop("disabled", true);
            
            var corpFullName = $("#corpFullName").val();
            var corpFullEmail = $("#corpFullEmail").val();
            var corpFullPhone = $("#corpFullPhone").val();
            var corpFullCompany = $("#corpFullCompany").val();
            var corpEventDesc = $("#corpEventDesc").val();
            var corpNoGuest = $("#corpNoGuest").val();
            var corpFullBudget = $("#corpFullBudget").val();
            var corpdatepicker = $("#corpdatepicker").val();
            var corptimepicker = $("#corptimepicker").val();
            var corpCatered = $("#corpCatered").val();
            var corpRestrictions = $("#corpRestrictions").val();
            var corpRemarks = $("#corpRemarks").val();
            var pagedata = {
                "corpFullName": corpFullName,
                "corpFullEmail": corpFullEmail,
                "corpFullPhone": corpFullPhone,
                "corpFullCompany": corpFullCompany,
                "corpEventDesc": corpEventDesc,
                "corpNoGuest": corpNoGuest,
                "corpFullBudget": corpFullBudget,
                "corpdatepicker": corpdatepicker,
                "corptimepicker": corptimepicker,
                "corpCatered": corpCatered,
                "corpRestrictions": corpRestrictions,
                "corpRemarks": corpRemarks
            };
            $.ajax({
                url: serviceurl + 'specialenquiry',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    if (value.status == 'success') {
                        $(".popup-corporate, .overlay").fadeOut();
                        $("#enquirySubmitBtn").prop("disabled", false);
                    } else {
                        //alert(value.msg);
                    }
                }
            });
        }
    });

        /*
    By:Jyoti Vishwakarma
    Description: send otp on mail
    */
    function forgotPasswordEmail() {
        var email = $("#forgotpass-email").val();
        if(email == ""){
            return false;
        }
        var data = {
            "email": email
        };
        $.ajax({
            url: serviceurl + 'forgotPasswordEmail',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            datatype: 'JSON',
            async: false,
            success: function(data) {
                var value = JSON.parse(data);
                if (value.status == 'success') {
                    $(".change-password-first").fadeOut();
                    $(".change-password-second")
                    .delay(100)
                    .fadeIn();
                } else {
                    $("#forgotPasswordMsg").html(value.msg);
                //    alert(value.msg);
                }
            }
        });
    }
        /*
    By:Jyoti Vishwakarma
    Description: verify otp
    */
    function forgotPasswordOTPVerify() {
        var email = $("#forgotpass-email").val();
        var otp = $("#forgotpass-otp").val();
        if(email == "" && otp== ""){
            return false;
        }
        var data = {
            "email": email,
            "otp": otp
        };
        $.ajax({
            url: serviceurl + 'forgotPasswordOTPVerify',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            datatype: 'JSON',
            async: false,
            success: function(data) {
                var value = JSON.parse(data);
                if (value.status == 'success') {
                    $(".change-password-second").fadeOut();
                    $(".change-password-third")
                    .delay(400)
                    .fadeIn();
                } else {
                    $("#forgotPasswordMsg1").html(value.msg);
                //    alert(value.msg);
                }
            }
        });
    }
    /*
    By:Jyoti Vishwakarma
    Description: Update new password
    */
    function forgotPasswordUpdateNewPassword() {
        var email = $("#forgotpass-email").val();
        var otp = $("#forgotpass-otp").val();
        var pwd = $("#forgotpass-rep-password").val();
        var cnfpwd = $("#forgotpass-new-password").val();
        $("#forgotPwdAlertMsg2").hide();
        $("#forgotPwdAlertMsg3").hide();
        if(pwd == ""){
            $("#forgotPwdAlertMsg2").show();
            $("#forgotPwdAlertMsg2").html("Field can not be empty.");
            return false;
        }

        if(cnfpwd == ""){
            $("#forgotPwdAlertMsg3").show();
            $("#forgotPwdAlertMsg3").html("Field can not be empty.");
            return false;
        }

        if(pwd != cnfpwd ){
            $("#forgotPwdAlertMsg3").show();
            $("#forgotPwdAlertMsg3").html("Confirm password is not matched.");
            return false;
        }

        
        // $("#forgotPwdAlertMsg").hide();
        
        
        var data = {
            "email": email,
            "pwd": pwd
        };
        $.ajax({
            url: serviceurl + 'UpdateNewPassword',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            datatype: 'JSON',
            async: false,
            success: function(data) {
                var value = JSON.parse(data);
                if (value.status == 'success') {
                    $(".change-password-form").fadeOut();
                    $(".password-success")
                    .delay(400)
                    .fadeIn();
                    $(".overlay,.popup-login")
                    .delay(3000)
                    .fadeOut();
                    $(".password-success, .change-password-third")
                    .delay(3500)
                    .fadeOut();
                    $(".change-password-form, .change-password-first")
                    .delay(3500)
                    .fadeIn();
                } else {
                   alert(value.msg);
                }
            }
        });
    }

    /*
    By:Jyoti Vishwakarma
    Description: change address
    */
    function changeAddress(addressid){
        var data = {
            "addressid": addressid
        };
        $.ajax({
            url: serviceurl + 'changedUserAddress',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            datatype: 'JSON',
            async: false,
            success: function(data) {
                var value = JSON.parse(data);
                if (value.status == 'success') {
                    $("#userAddTitle").html(value.addresstype);
                    $("#userAddress").html(value.address.substr(0, 22)+'...');
                    if(window.location.href.includes("cart")){
                        $('input:radio[name=loc_address_title][value='+value.addresstype+']').prop('checked', true);
                        getUserAddress(value.addresstype);
                    }
                    

                } else {
                   alert(value.msg);
                }
            }
        });
    } 
        /*
    By:Jyoti Vishwakarma
    Description: display addons of customize cuisine
    */
    function customizecuisine(id){
            var pagedata = {
                "id": id
            };
            $.ajax({
                url: serviceurl + 'corporatemeals_detail',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var val = "";

                    if (value.cuisines[0].type.includes('Non Veg')) {
                        val = '<img src="<?= $baseurl; ?>images/icons/nonveg.svg" alt="Non Veg" class="customize-cat"> <span>' + value.cuisines[0].name +'</span>';
                    } else {
                        val = '<img src="<?= $baseurl; ?>images/icons/veg.svg" alt="Non Veg" class="customize-cat"> <span>' + value.cuisines[0].name +'</span>';
                    }
                    $("#cuisineTitle").html(val);
                    var selectedaddons = value.cuisines[0].selectedaddons;
                    selectedaddons = selectedaddons.split(',');
                    var addonshtml = "";
                    var sectionheading = "";
                    var addtocartbtn = "";
                    var addtocartflag = 0;
                    for(var i=0;i<value.cuisines[0].sections.length;i++){
                        var addon = value.cuisines[0].sections[i].addons;    
                        sectionheading += '<li><a href="#category'+value.cuisines[0].sections[i].section_id+'" class="popup-list-1">&#x2022; '+value.cuisines[0].sections[i].title+'</a></li>';

                        addonshtml += '<div class="cusine-list popup-list-1 active" id="category'+value.cuisines[0].sections[i].section_id+'"><h3> '+value.cuisines[0].sections[i].title+' </h3><ul class="cusine-custom-list">';

                        if(value.cuisines[0].sections[i].choicetype == 1){
                            for(var j=0;j<addon.length;j++){
                                var checked = "";
                                if(selectedaddons.length>0){ 
                                    if(selectedaddons.includes(addon[j].addon_id)){
                                        checked = "checked";
                                        addtocartflag = 1;
                                    }
                                }
                           
                                if(addon[j].cuisinetype == '1'){
                                    addonshtml += '<li><img src="<?= $baseurl; ?>images/icons/veg.svg" alt="" class="customize-cat">';
                                }else{
                                    addonshtml += '<li><img src="<?= $baseurl; ?>images/icons/nonveg.svg" alt="" class="customize-cat">';
                                }
                                var  strikeprice ="";
                                
                                if(addon[j].strikeprice != "" && addon[j].strikeprice != 0){
                                   
                                    strikeprice = '<strike> ₹'+ addon[j].strikeprice +'</strike>  ';
                                }

                                addonshtml +='<div class="checkboxWrap" onclick="addCustomize()"><label class="container"><input type="checkbox" class="addons_corporate" name="addon" data-id="' + addon[j].addon_id + '" value="' + addon[j].addon_id + '" '+checked+'><span class="checkmark"></span><p>' + addon[j].detail + '  <span>(+ ' + strikeprice +' ₹ <span id="addonprice' + addon[j].addon_id + '">' + addon[j].price + '</span>)</span></p></label></div></li>';
                            }

                        }else{
                         
                               
                           
                            for(var j=0;j<addon.length;j++){
                                var checked = "";
                                // checked = 'checked="checked"';
                                var selectedaddons1 = selectedaddons;
                                if(selectedaddons.length>0){  
                                    if(selectedaddons.includes(addon[j].addon_id)){
                                        checked = 'checked="checked"';
                                        addtocartflag = 1;
                                    }
                                }

                                if(addon[j].cuisinetype == '1'){
                                    addonshtml += '<li><img src="<?= $baseurl; ?>images/icons/veg.svg" alt="" class="customize-cat">';
                                }else{
                                    addonshtml += '<li><img src="<?= $baseurl; ?>images/icons/nonveg.svg" alt="" class="customize-cat">';
                                }
                                var  strikeprice ="";
                                
                                if(addon[j].strikeprice != "" && addon[j].strikeprice != 0){
                                   
                                    strikeprice = '<strike> ₹'+ addon[j].strikeprice +'</strike>  ';
                                }
                                addonshtml +='<div class="checkboxWrap" onclick="addCustomize()"><label class="container"><input type="radio" name="addOnPacagesRadio-'+ value.cuisines[0].sections[i].section_id +'" value="' + addon[j].addon_id + '" '+checked+'><span class="checkmark"></span><p>' + addon[j].detail + '  <span>(+ ' + strikeprice +' ₹ <span id="addonprice' + addon[j].addon_id + '">' + addon[j].price + '</span>)</span></p></label></div></li>';

                            }

                    }
                        addonshtml += '</ul></div>';


                    }
                    
                    addtocartbtn = '<p class="addToCart" id="addedTocart-' + value.cuisines[0].id + '" onclick="addToCartCustomize(' + value.cuisines[0].id + ')"><img src="<?= $baseurl; ?>images/icons/addCart.svg" alt="download"><span id="customizecuisinebtntitle">ADD to cart</span></p>';

                    $("#cuisineCustomHeading").html(sectionheading);
                    $("#cuisineCustomDeatail").html(addonshtml);
                    $("#cuisinetotalamt").html(value.cuisines[0].discountprice);
                    $("#cuisinebaseamt").val(value.cuisines[0].discountprice);
                    $("#addtocartbtn").html(addtocartbtn);
                    $(".popup-customize, .overlay")
                        .delay(400)
                        .fadeIn();
                   
                        addCustomize();
                    if(addtocartflag == 0){
                        $("#customizecuisinebtntitle").html("ADD to cart");
                    }else{
                        $("#customizecuisinebtntitle").html("Added In Cart");
                    }  

                    var start = value.cuisines[0].kitchen_open;
                    var end = value.cuisines[0].kitchen_close;

                    var d = new Date();
                    var datesss = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                    var min = new Date(datesss + ' ' + start);
                    var max = new Date(datesss + ' ' + end);

                    if(min.getTime() > d.getTime() || d.getTime() >  max.getTime()){
                       $(".addToCart").html("Kitchen closed");
                       $(".addToCart").removeAttr('onclick');
                    }else{
                        
                        $("#addtocartbtn").show();
                    }

                }
            });
        }

        function addCustomize(){
            var cp =parseInt($("#cuisinebaseamt").val());
            $("#customizecuisinebtntitle").html("ADD to cart");
            var items = $(".addons_corporate");
            var selectedaddons = "";
            var total = cp;
            for (var i = 0; i < items.length; i++) {
                if (items[i].type == 'checkbox' && items[i].checked == true){
                        selectedaddons = items[i].value;
                        var price = parseInt($("#addonprice"+selectedaddons).html());
                        total = price + total;
                }
                        
            }

            $('#cuisineCustomDeatail input[type=radio]:checked').each(function() {
                 selectedaddons = this.value;
                 var price = parseInt($("#addonprice"+selectedaddons).html());
                 total = price + total;
               
            });
          $("#cuisinetotalamt").html(total);
        }

        function updateCuisineQuantity(id, type){
            var countex = $("#cuisineQty-" + id).val();
            if (type == 'add') {
                var count1 = parseInt(countex) + 1;
               
            } else {
                var minqty = $("#teamMealMinQty").val();
                if(countex => minqty){
                    var count1 = parseInt(countex) - 1;
                }
                else{
                    alert("Qty can not be less then "+ minqty);
                }
                
            }
            if (count1 < 1) {
                count1 = 1;
                $("#cuisineQty-" + id).val('1');
            }
            $("#cuisineQty-" + id).val(count1);
            var count12 = count1;
            var cartdata = {
                "id": id,
                "count": count1
            };
            $.ajax({
                url: serviceurl + 'cartupdate',
                type: 'POST',
                data: JSON.stringify(cartdata),
                contentType: 'application/json; charset=utf-8',
                datatype: 'JSON',
                async: true,
                success: function(data) {
                   
                    // var total = 0;
                    // $(".quantity" + id).val(count12);
                    // var act = parseInt($("#priceAct" + id).val());
                    // $("#itemPrice" + id).html(act * count12);
                    // $(".itemprice").each(function() {
                    //     total += parseInt($(this).html());
                    // });
                    // $("#totalcostitem").html(total);
                    // $("#totalcost").html(total);
                    // $("#btn_total").html(total);
                }
            });
        }

        function adjustHeight() {
            $(".cuisineTitle").matchHeight();
            $(".cuisineImgWrap").matchHeight();  
            $('.restroWrapContent').matchHeight();
            $('.cuisineDesc').matchHeight();
            $(".cuisineRestroTitle").matchHeight();
            $(".cuisineData").matchHeight();
        }

        function slideCat(){
            $(".cuisineWrap ul li").on("click", function() {
                $(".cuisineWrap ul li").removeClass("activeTab");
                $(this).addClass("activeTab");
            });
    }
    /*
    By:Jyoti Vishwakarma
    Description: change meal type
    */
    function activeChoosedMealType(){
            $("#corporateli").removeClass("activeSub");
            $("#teamli").removeClass("activeSub");
            $("#eventli").removeClass("activeSub");
            $("#cafeteriali").removeClass("activeSub");
                var selected = $("#meals_check").val();
                if(selected == "corporate_meals"){
                    $("#corporateli").addClass("activeSub");
                }else if(selected == "team_meals"){
                    $("#teamli").addClass("activeSub");
                }else if(selected == "event_meals"){
                    $("#eventli").addClass("activeSub");
                }else if(selected == "cafeteria"){
                    $("#cafeteriali").addClass("activeSub");
                }else{
                    return false;
                }
    } 

    
        $(".preloader").fadeOut();



</script>

<script>
    /*
    By:Jyoti Vishwakarma
    Description: facebook login
    */
    function facebookLogin()
    {
            console.log("facebook login");
        fbLogin();
    }
    
    window.fbAsyncInit = function() {
        // FB JavaScript SDK configuration and setup
        FB.init({
            appId      : '2646941222205273', // FB App ID
            cookie     : true,  // enable cookies to allow the server to access the session
            xfbml      : true,  // parse social plugins on this page
            version    : 'v2.11' // use graph api version 2.8
        });
    };

    // Load the JavaScript SDK asynchronously
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // Facebook login with JavaScript SDK
    function fbLogin() {
        FB.login(function (response) {
            if (response.authResponse) {
                // Get and display the user profile data
                getFbUserData();
            } else {
                document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
            }
        }, {scope: 'email'});
    }

    // Fetch the user profile data from facebook
    function getFbUserData(){
        FB.api('/me', {locale: 'en_US', fields: 'id,name,first_name,last_name,email,link,gender,locale,picture.type(large)'},
            function (response) {
                console.log(response);
                // id:response.id,
                //  name:response.name,
                //  email:response.email,
                //  fname:response.first_name,
                //  lname:response.last_name,
                //  gender:response.gender,
                //  picture:response.picture
                var picurl = "";
                var picres = response.picture;
                // console.log(picres);

                var data = picres.data;
                // console.log(data);

                picurl = data.url;
                console.log(picurl);

            
                // document.getElementById('status').innerHTML =
                // 'Thanks for logging in, ' + response.name + '!';
                var email  = response.email;
                console.log(email);
                if(email != "" && typeof email !== "undefined"){
                    var pagedata = {
                        "email": email
                    };
                    var userid = '<?php if (isset($_SESSION['userid'])) {  echo $_SESSION['userid'];    } ?>';
                    console.log(userid);
                    if (userid == "") {
                        $.ajax({
                            url: serviceurl + 'isUserExist',
                            type: 'POST',
                            data: JSON.stringify(pagedata),
                            datatype: 'JSON',
                            async: false,
                            success: function(data) {
                                var value = JSON.parse(data);
                                if (value.status == 'new') {
                                    $('.first-login').fadeOut();
                                    $('.corporate-login').delay(400).fadeIn();   //$(".google").on("click",function(){$(".first-login").fadeOut();$(".corporate-login").delay(400).fadeIn();});
                                    $("#google_id").val('');
                                    $("#google_name").val(response.name);
                                    $("#google_imgurl").val(picurl);
                                    $("#google_email").val(email);
                                    $("#social_type").val('facebook');
                                } else {
                                    var currenturl = window.location.href;
                                    // if (value.isCorporateUser == '1') {
                                    //     // window.location.href = "?=$baseurl?>corporate.php";
                                    //     var currenturl = window.location.href;
                                    //     if(currenturl.includes("cart")){
                                    //         location.reload();
                                    //     }else{
                                    //         window.location.href = "<?=$baseurl?>corporate.php";
                                    //     }
                                    // } else {
                                    //     window.location.href = "<?=$baseurl?>index.php";
                                    // }

                                    if(currenturl.includes("cart")){
                                            location.reload();
                                        }else{
                                            if (value.isCorporateUser == '1') {
                                            window.location.href = "<?=$baseurl?>corporate.php";
                                            }else {
                                                window.location.href = "<?=$baseurl?>index.php";
                                            }
                                        }


                                }
                            }
                        });
                    }
                }else{
                        // alert("Not able get email Id");
                        $("#sign_up_fullname").val(response.name);
                        // $(".first-login").fadeOut();
                        $(".first-login").hide();
                        $('.facebookAlertMsg').fadeIn();
                            setTimeout(function() {
                                $('.facebookAlertMsg').fadeOut();
                                $(".first-login,.sign-in,.corporate-sign-in,.corporate-loginlink").fadeOut();
                                $("overlay,.popup-login,.sign-up")
                                .delay(400)
                                .fadeIn();
                                $(".menuBtn").removeClass("actMenuBtn");
                                $(".mobNavbar").slideUp();
                            }, 3000);
                            
                    
                }
           }
        );
    }

</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "Cloudkitch",
  "alternateName": "Cloudkitch",
  "url": "https://cloudkitch.co.in/",
  "logo": "https://cloudkitch.co.in/images/logo.svg",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "Unit No. G4-A, Trade Centre,",
    "addressLocality": "Bandra Kurla Complex,",
    "addressRegion": "Bandra East, Mumbai",
    "postalCode": "400051",
    "addressCountry": "IN",
    "telephone": "+91 771 099 9666",
    "email": "enquiry@cloudkitch.co.in"
  }
}

</script>