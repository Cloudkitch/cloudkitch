<!DOCTYPE html> 
<html>
    <head>
        <?php
            include 'head.php';
            
		?>
        <title>Order History | Cloudkitch</title>
        <meta name="description" content="">
    </head>
    <body>
        <?php
            include 'header.php';
        ?>
        <section class="section topSection message-order">
            <div class="message-container">
                <!-- <div class="message-check">
                    <img src="<?=$baseurl;?>images/icons/white-check.svg" alt="">
                </div> -->
                <div class="message-details">
                    <div class="meassage-wrapper">
                        <h2>Order History</h2>
                        <!-- <p>Order ID : <span class="order-id">CFDGDTHG85FGH5H012329</span></p> -->
                    </div>
                </div>
            </div>
        </section>
        <section class="orderHistory" id="orderHistory">
            <div class="cart-wrapper">
                <h2>No Order Found</h2>
            </div>
        </section>
        <?php
			include 'footer.php';
		?>
    </body>
    <script>
            $(document).ready(function(){
                getOrderHistory();
                activeChoosedMealType(); 
            });
                /*
    By:Jyoti Vishwakarma
    Description: get order history list
    */
            function getOrderHistory(){
                var invc = '';

                invc = '1111';  

                var cfo = {"invitecode":invc};

                $.ajax({
                    url: 'cloudkitchadmin/service.php?servicename=cheforders&for=user',
                    type: 'POST',
                    datatype: 'JSON',
                    data: JSON.stringify(cfo),
                    async: false,
                    success: function(data)
                    {
                        var html="";
                        var rscol = JSON.parse(data);

                        var co = new Array();
                        const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
                        for(var oc=0;oc<rscol.cord.length;oc++)
                        {
                            var orderId = rscol.cord[oc].orderid;
                            let current_datetime = rscol.cord[oc].orderdate;
                            // let d = current_datetime.getDate() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getFullYear();
                            var date = current_datetime.split(" ");
                            date = date[0].split('/').join('-'); 
                            var amount = rscol.cord[oc].price;
                            html += '<div class="cart-wrapper"> <div class="cart-item cart-profile"> <h2>Order ID: <span class="order-id">'+ orderId +'</span></h2> <p class="order-placed">Placed on : <span class="order-date">'+ date +'</span></p> <h2>&#8377; '+ amount +'</h2> </div> <div class="cart-btn-item"> <div class="button-wrapper"> <a href="<?=$baseurl;?>order-details.php?status=success&orderId='+orderId+'" class="btn-green manage-order">View Details</a> </div> </div> </div>';
                            $("#orderHistory").html(html);
                        }
                    }
                });
            }
</script>
</html>

