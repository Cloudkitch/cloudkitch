<!DOCTYPE html>
<html>
<head>
    <?php
    include 'head.php';
    include 'inc/databaseConfig.php';
    ?>
    <title>Cart | Cloudkitch</title>
    <meta name="description" content="">
    <meta name="google-signin-client_id" content="287145767352-qi8asdmobui20671musqaq0jga2ru2re.apps.googleusercontent.com">
    <link rel="stylesheet" href="<?= $baseurl; ?>/css/jquery-ui.css">
    <link rel="stylesheet" href="<?= $baseurl; ?>css/jquery-ui.multidatespicker.css">
    <link rel="stylesheet" href="<?= $baseurl; ?>css/timepicki.css">
    <style>
        .cart-title.completed img {
            content: url('<?= $baseurl; ?>images/icons/completed.svg');
        }

        .form-legend label.error {
            bottom: -20px;
            top: unset;
            left: -5px;
            background: transparent;
            color: red;
        }
        .corporate-loginlink{
            display:none !important
        }
    </style>
</head>

<body class="cartSection">
    <?php
        include 'header.php';
    ?>

    <div class="popup popup-card-details">
        <div class="popup-wrapper">
            <div class="popup-image-wrapper">
                <img src="<?= $baseurl; ?>images/icons/popup-loader.svg" alt="loader">
            </div>
            <p class="popup-first">You will be redirected to your bank's website. <br> it might take few seconds</p>
            <p class="popup-second">Please do not refresh the page or click the back close button of your computer</p>
        </div>
    </div>

    <section class="section topSection">
        <div class="cart-wrapper">
            <div class="cart-item cart-profile">
                <div class="cart-profile-details ">
                    <?php
                                    if (isset($_SESSION['userid'])) {
                    ?>
                        <div class="cart-title completed open" id="account">
                            <img src="<?= $baseurl; ?>images/icons/account-profile.svg" alt="profile">
                            <h3>Account</h3>
                        </div>
                    <?php
                                    } else {
                    ?>
                        <div class="cart-title open" id="account">
                            <img src="<?= $baseurl; ?>images/icons/account-profile.svg" alt="profile">
                            <h3>Account</h3>
                        </div>
                    <?php
                                    }
                                    if (!isset($_SESSION['userid'])) {
                    ?>
                        <div class="cart-profile-info account open">
                            <div class="cart-first-login">
                                <p>To place your order now, log in to your existing account or sign up.</p>
                                <div class="button-wrapper">
                                    <a href="#" class="btn-outline-green" onclick="cartLogin()" style="width:50%">Log in / Register</a>
                                    <!-- <a href="#" class="btn-green" onclick="cartsignUp()">Sign up</a> -->
                                </div>
                                <!-- <div class="button-wrapper">
                                <p>Have an account</p>
                                <p>Don't have an account? Create account </p>
                            </div> -->
                            </div>
                            <div class="cart-second-login">
                                <p>To place your order now, to create a new account or <span class="highlight-text">log in</span></p>
                                <form class="accountForm">
                                    <div class="form-group form-legend">
                                        <input type="number" class="form-input-legend" id="account-phone" name="account-phone">
                                        <label for="account-phone">Phone Number</label>
                                    </div>
                                    <div class="form-group form-legend">
                                        <input type="text" class="form-input-legend" id="account-name" name="account-name">
                                        <label for="account-name">Name</label>
                                    </div>
                                    <div class="form-group form-legend">
                                        <input type="email" class="form-input-legend" id="account-email" name="account-email">
                                        <label for="account-email">Email id </label>
                                    </div>
                                    <div class="form-group form-legend">
                                        <input type="password" class="form-input-legend" id="account-password" name="account-password">
                                        <label for="account-password">Password </label>
                                    </div>
                                    <div class="form-group">
                                        <div class="checkboxWrap">
                                            <label class="container"> I accept the Terms & Conditions
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group form-legend">
                                        <a href="#" class="btn-green account-accept">Continue</a>
                                    </div>
                                </form>
                            </div>

                            <div class="cart-third-login">

                            </div>

                        </div>
                    <?php
                                    }
                    ?>
                </div>
                <?php
                
                    if(isset($_SESSION['isCorporateUser']) == 1) {
                ?>
                    <div class="cart-profile-details">
                        <div class="cart-title" id="delivery-details">
                            <img src="<?= $baseurl; ?>images/icons/deliveryDetails.svg" alt="Delivery Details">
                            <h3>Delivery Details</h3>
                        </div>
                        <div class="cart-profile-info delivery-details">
                            <p>Delivery your order.</p>
                            <form action="" class="deliveryForm">
                                <div class="radio-wrapper">
                                    <label class="radio-inline">
                                        <input type="radio" class="form-button-radio" name="orderPreference" checked id="delivery-now" value="delivery-now">
                                        <label class="form-radio-button" for="delivery-now">Delivery Now</label>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" class="form-button-radio" name="orderPreference" id="schedule-now" value="schedule-now">
                                        <label class="form-radio-button" for="schedule-now">Schedule</label>
                                    </label>
                                </div>
                                <div class="scheduleDetails">
                                    <div class="form-wrapper">
                                        <div class="form-group form-legend">
                                            <input type="text" id="from-datepicker" placeholder="DD/MM/YYYY" class="form-input-legend schedule-date" name="from-date" >
                                            <label for="from-date">Select Date</label>
                                        </div>
                                        <div class="form-group form-legend form-to-date">
                                            <input type="text" placeholder="HH:MM" class="form-input-legend timepicker schedule-time" id="delivery-time" name="delivery-time">
                                            <label for="delivery-time">Time of Delivery </label>
                                        </div>

                                        <!-- <div class="form-group form-legend">
                                            <input type="text" id="to-datepicker" placeholder="DD/MM/YYYY" class="form-input-legend schedule-date" id="to-date" name="to-date">
                                            <label for="to-date">To Date </label>
                                        </div> -->
                                        
                                    </div>
                                    <div class="form-wrapper">

                                    <span id="errorOrderTime" style="display:none;color:red">Order time should be between 10:45 AM and 11:30 PM</span>

                                    </div>
                                </div>
                                <div class="form-group form-legend">
                                    
                                    <a href="#" class="btn-green btn-continue" id="countyss">Continue</a>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php
                                    }
                ?>

                <?php
                    if (isset($_SESSION['userid'])) {
                        $open = "open";
                        $pointer = 'style=""';
                    } else {
                        $open = "";
                        $pointer = 'style="pointer-events: none;cursor:pointer"';
                    }
                ?>
                <div class="cart-profile-details" <?= $pointer ?>>
                    <div class="cart-title <?= $open ?>" id="delivery-info">
                        <img src="<?= $baseurl; ?>images/icons/location.svg" alt="Delivery Information">
                        <h3>Delivery Information</h3>
                    </div>
                    <?php
                         if (isset($_SESSION['userid'])) {
                    ?>
                        <div class="cart-profile-info delivery-info <?= $open ?>">
                            <form name="location_form" id="location_form" class="loactionForm">
                            <input type="hidden" id="isNormalCuisine" value="0">
                            <input type="hidden" id="iskitchenClosed" value="0">
                                <!-- <div class="form-group form-legend">
                    <form name="location_form" id="location_form" class="loactionForm">
                        <div class="form-group form-legend">
                            <a href="#" class="btn btn-form borderBtn">
                                <img src="<?= $baseurl; ?>images/icons/detect-location.svg" alt=""> Use my location
                            </a>
                        </div> -->
                                <div class="addressWrap">
                                    <!-- <p class="editAddress"><img src="?=$baseurl;?>images/icons/edit.svg" alt="">Edit</p> -->
                                    <div class="checkboxWrap">
                                        <label class="container radioContainer">Home
                                            <input type="radio" name="loc_address_title" value="Home" onchange="getUserAddress('Home');" checked>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="checkboxWrap">
                                        <label class="container radioContainer">Office
                                            <input type="radio" name="loc_address_title" onchange="getUserAddress('Office');" value="Office">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="checkboxWrap">
                                        <label class="container radioContainer">Other
                                            <input type="radio" name="loc_address_title" onchange="getUserAddress('Other');" value="Other">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <!-- <div class="form-wrapper">
                            <div class="form-group form-legend">
                                <input type="text" class="form-input-legend" id="loc_address_title" name="loc_address_title">
                                <label for="loc_address_title">Address Title</label>
                                
                            </div>
                            <div class="form-group form-legend">
                                <select name="loc_address_type" id="loc_address_type" class="form-input-legend">
                                    <option value="create">Create New Address</option>
                                    ?php
                                        error_reporting(0);
                                        $userid= $_SESSION['userid'];
                                        $query = "SELECT * FROM useraddresses WHERE userid = '$userid' ORDER BY useraddressid ASC";
                                        $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                                        while($row = mysqli_fetch_assoc($result))
                                        {
                                            echo '<option value="'.$row['addresstitle'].'">'.$row['addresstitle'].'</option>';
                                        }   
                                    ?>
                                </select>
                                <label for="loc_address_type">Address Type</label>
                            </div>
                        </div> -->
                                <div class="form-wrapper">
                                    <div class="form-group form-legend">
                                        <input type="text" class="form-input-legend" id="loc_full_name" name="loc_full_name">
                                        <label for="loc_full_name">Full Name</label>
                                        <input type="hidden" id="location_flag">
                                    </div>
                                    <div class="form-group form-legend">
                                        <input type="number" class="form-input-legend" id="loc_mobile_number" name="loc_mobile_number">
                                        <label for="loc_mobile_number">10 digit Mobile number </label>
                                    </div>
                                </div>
                                <div class="form-wrapper">
                                    <div class="form-group form-legend">
                                        <!-- <input type="text" class="form-input-legend" id="loc-state" name="loc-state"> -->
                                        <select name="loc_state" id="loc_state" class="form-input-legend">
                                            <option value="">Select State</option>
                                            <option value="Maharashtra">Maharashtra</option>
                                            <option value="Gujarat">Gujarat</option>
                                        </select>
                                        <label for="loc_state">State</label>
                                    </div>
                                    <div class="form-group form-legend">
                                        <input type="text" class="form-input-legend" id="loc_city" name="loc_city">
                                        <label for="loc_city">City / District / Town</label>
                                    </div>
                                </div>
                                <div class="form-wrapper">
                                    <div class="form-group form-legend">
                                        <input type="text" class="form-input-legend" id="loc_landmark" name="loc_landmark">
                                        <label for="loc_landmark">Land mark (Optional)</label>
                                    </div>
                                    <div class="form-group form-legend">
                                        <input type="number" class="form-input-legend" id="loc_pincode" name="loc_pincode">
                                        <label for="loc_pincode">Pin code</label>
                                    </div>
                                </div>
                                <div class="form-group form-legend">
                                    <textarea class="form-textarea-legend" name="loc_address" id="loc_address" cols="30" rows="4"></textarea>
                                    <label for="loc_address">Address</label>
                                </div>
                                <div class="form-group form-legend">
                                    <button type="submit" class="btn-green">Save and deliver here</button>
                                </div>
                            </form>
                        </div>
                    <?php
                        }
                    ?>
                </div>

                <div class="cart-profile-details" <?= $pointer ?>>
                    <div class="cart-title" style="pointer-events: none;">
                        <img src="<?= $baseurl; ?>images/icons/payment.svg" alt="Payment Details">
                        <h3>Payment Details</h3>
                    </div>
                    <?php
                      if (isset($_SESSION['userid'])) {
                    ?>
                        <div class="cart-profile-info payment-method">
                            <p>If you have a <span class="">corporate coupons</span></p>
                            <form action="" class="paymentForm">
                                <div class="form-group form-legend">
                                    <input type="test" class="form-input-legend" id="paymnet-gift-number" name="paymnet-gift-number">
                                    <label for="paymnet-gift-number">Enter Gift Card Number</label>
                                </div>
                                <?php
                        if (isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser'] == 1) {
                            echo '<div class="walletBox">
                                    <div class="checkboxWrap">
                                        <label class="container">Pay using wallet (&#8377; <span id="walletamount">45</span>)
                                            <input type="checkbox" id="checkbox_wallet" name="wallet" value="wallet" onchange="payUsingWallet()">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>';
                                      }
                                ?>

                                <div class="form-group form-legend">
                                    <h2>Or Select one of the below payment methods.</h2>
                                </div>
                                <div class="radio-container">
                                    <div class="form-group form-group-radio">
                                        <input class="custom-radio" type="radio" id="radio_3" name="payment_method" value="online">
                                        <label for="radio_3" class="radio-label">
                                            <svg id="ico_24_map_places_2" data-name="ico / 24 / map &amp; places / 2" xmlns="http://www.w3.org/2000/svg" width="38" height="38" viewBox="0 0 38 38">
                                                <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                                                    <stop offset="0" stop-color="#d48d1f" />
                                                    <stop offset="1" stop-color="#fcbc57" />
                                                </linearGradient>
                                                <rect id="Bounding_box" data-name="Bounding box" width="38" height="38" fill="rgba(255,255,255,0)" />
                                                <path id="Icon_color" data-name="Icon color" d="M16,32A16,16,0,1,1,32,16,16.018,16.018,0,0,1,16,32ZM11.4,17.778c.242,5.588,2.1,9.6,3.8,10.667h1.6c1.654-1.073,3.493-5.082,3.8-10.667Zm12.338,0a24.991,24.991,0,0,1-2.1,9.3,12.459,12.459,0,0,0,6.667-9.3Zm-20.035,0a12.456,12.456,0,0,0,6.667,9.3,25,25,0,0,1-2.1-9.3H3.7ZM21.635,4.925a24.966,24.966,0,0,1,2.1,9.3H28.3A12.459,12.459,0,0,0,21.635,4.925ZM15.27,3.555c-1.654,1.073-3.491,5.082-3.8,10.667H20.6c-.242-5.589-2.1-9.6-3.8-10.667H15.27Zm-4.906,1.37a12.455,12.455,0,0,0-6.667,9.3h4.57A24.97,24.97,0,0,1,10.365,4.925Z" transform="translate(3 3)" />
                                            </svg>
                                            <p>Online Payment</p>
                                        </label>
                                    </div>
                                    <div class="form-group form-group-radio">
                                        <input class="custom-radio" type="radio" id="radio_4" name="payment_method" value="cash" checked>
                                        <label for="radio_4" class="radio-label">
                                            <svg id="ico_24_payment_money" data-name="ico / 24 / payment / money" xmlns="http://www.w3.org/2000/svg" width="51" height="38" viewBox="0 0 51 38">
                                                <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                                                    <stop offset="0" stop-color="#d48d1f" />
                                                    <stop offset="1" stop-color="#fcbc57" />
                                                </linearGradient>
                                                <rect id="Bounding_box" data-name="Bounding box" width="51" height="38" fill="rgba(255,255,255,0)" />
                                                <path id="Icon_color" data-name="Icon color" d="M44.65,30H2.35A2.155,2.155,0,0,1,0,28.125V1.875A2.155,2.155,0,0,1,2.35,0h42.3A2.155,2.155,0,0,1,47,1.875v26.25A2.155,2.155,0,0,1,44.65,30ZM14.1,3.75c0,4.136-4.217,7.5-9.4,7.5v7.5c5.183,0,9.4,3.366,9.4,7.5H32.9c0-4.135,4.217-7.5,9.4-7.5v-7.5c-5.182,0-9.4-3.366-9.4-7.5Zm9.4,16.875c-3.887,0-7.049-2.523-7.049-5.624S19.613,9.376,23.5,9.376,30.549,11.9,30.549,15,27.387,20.624,23.5,20.624Z" transform="translate(2 4)" />
                                            </svg>
                                            <p>Cash On Delivery</p>
                                        </label>
                                    </div>
                                </div>

                                <div class="card-paymnet">
                                    <div class="form-group">
                                        <input type="text" class="form-input-legend form-input" id="paymnet-card" name="paymnet-card" placeholder="Card Number">
                                    </div>
                                    <div class="form-group form-controller">
                                        <div class="form-expiry">
                                            <input type="text" class="form-input-legend form-input form-input-expiry" placeholder="Expiry" readonly>
                                            <div class="form-group form-legend form-payment-select">
                                                <!-- <input type="text" class="form-input-legend" id="loc-state" name="loc-state"> -->
                                                <select name="paymnet-month" id="paymnet-month" class="form-input-legend select-month">
                                                    <option value="-1"> MM</option>
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8">08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                            <div class="form-group form-legend form-payment-select">
                                                <!-- <input type="text" class="form-input-legend" id="loc-state" name="loc-state"> -->
                                                <select name="paymnet-year" id="paymnet-year" class="form-input-legend select-year">
                                                    <option value="-1">YYYY</option>
                                                    <option value="1">2019</option>
                                                    <option value="2">2020</option>
                                                    <option value="3">2021</option>
                                                    <option value="4">2022</option>
                                                    <option value="5">2023</option>
                                                    <option value="6">2024</option>
                                                    <option value="7">2025</option>
                                                    <option value="8">2026</option>
                                                    <option value="9">2027</option>
                                                    <option value="10">2028</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-cvv">
                                            <input type="number" class="form-input-legend form-input" id="paymnet-cvv" name="paymnet-cvv" placeholder="CVV">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-legend">
                                    <a href="javascript:void(0);" onclick="checkoutOrder()" class="btn-green btn-block btn-payment emptycart" id="payBtn">Pay ₹ <span id="btn_total"></span> </a>
                                    <!-- <a href="#" class="btn-green btn-block btn-disable btn-payment">Pay ₹ <span id="btn_total"></span> </a> -->
                                </div>

                            </form>
                        </div>
                    <?php
                                                                }
                    ?>
                </div>
            </div>
            <div class="cart-item cart-order">
                <div class="itemWrapper">
                    <div id="orderdata">
                    </div>
                </div>
                <div class="order-item-wrapper" id="cartdetails_nodata">
                    <h2 class="bill-title">Cart is empty</h2>
                </div>

               <div class="order-item-wrapper" id="cartdetails1">
                    <!-- <h2 class="bill-title">Bill Details</h2> -->
                    <div class="order-item-conatiner bill-details order-font">
                        <div class="order-list-qty">
                            <p>Item Total</p>
                        </div>
                        <div class="order-list-cost">&#8377; <span id="totalcostitem"></span></div>
                    </div>
                    <!-- <div class="order-item-conatiner order-saving order-font">
                        <div class="order-list-qty">
                            <p>Save <span class="saving-price">&#8377; 25</span> for this item </p>
                        </div>
                        <div class="order-list-cost"><a href="#" class="offer-highlight">Get offer</a> </div>
                    </div> -->
                    <h4 class="promoText">Have a Coupon Code ?</h4>
                    <div class="promoCode">
                        <form>
                            <div class="promoWrap">
                                <input type="text" id="applypromocode">
                                <button type="button" id="applypromocodebutton">Apply</button>
                            </div>
                            <div class="promoErr">
                                <span class="errorText" style="display:none"></span>
                                <span class="successText" style="display:none"></span>
                            </div>
                        </form>
                    </div> 
                    <div class="cartdetail order-item-conatiner order-charge finalcharge" id="usedpromocode" style="display: none;margin-right: 0px;">
                        <div class="order-list-qty">
                            <p>Promo Code</p>
                        </div>
                        <div class="order-list-cost"> &#8377;<span id="discount_amount_pay">0.00</span></div>
                        <input type="hidden" id="promoapplied" value="0">
                    </div>              
                    <div class="order-item-conatiner">
                        <div class="order-list-qty">
                            <p>SGST (2.5%)</p>
                        </div>
                        <div class="order-list-cost">&#8377; <span id="sgst">0.00</span></div>
                    </div>
                    <div class="order-item-conatiner">
                        <div class="order-list-qty">
                            <p>CGST (2.5%)</p>
                        </div>
                        <div class="order-list-cost">&#8377; <span id="cgst">0.00</span></div>
                    </div>
                </div>
                <div class="order-item-conatiner finalcharge cartdetail" id="cartdetails3">
                    <div class="order-list-qty">
                        <p>Restaurant Charges</p>
                    </div>
                    <div class="order-list-cost">&#8377; 0.00</div>
                </div>
                <div class="order-item-conatiner order-charge  finalcharge delivery cartdetail" id="cartdetails4">
                    <div class="order-list-qty">
                        <p>Delivery Charges</p>
                    </div>
                    <div class="order-list-cost">&#8377; 0.00</div>
                </div>
                <div class="cartdetail order-item-conatiner order-charge finalcharge" id="subscriptionoffer" style="display: none;">
                    <div class="order-list-qty">
                        <p>Subscription Offer</p>
                    </div>
                    <div class="order-list-cost"> &#8377;<span id="subsoffer">0.00</span></div>
                </div>
                
                <div class="cartdetail order-item-conatiner order-charge finalcharge" id="usedWalletDiv" style="display: none;">
                    <div class="order-list-qty">
                        <p>Wallet Point</p>
                    </div>
                    <div class="order-list-cost"> <span id="wallet_amount_pay">0.00</span> Point</div>
                </div>
                
                <div class="order-item-conatiner order-charge order-font finalcharge total cartdetail" id="cartdetails5">
                    <div class="order-list-qty">
                        <p>Total</p>
                    </div>
                    <div class="order-list-cost">&#8377; <span id="totalcost"></span></div>

                </div>
            </div>
        </div>
        </div>
    </section>
    <form id="checkout" name="checkout" action="<?=$baseurl?>payuform.php" method="POST">
        <input type="hidden" id="payment_paymenttype" name="payment_paymenttype" value="online">
        <input type="hidden" id="payment_firstname" name="payment_firstname">
        <input type="hidden" id="payment_delivery_address" name="payment_delivery_address">
        <input type="hidden" id="payment_stateinfo" name="payment_stateinfo">
        <input type="hidden" id="payment_cityinfo" name="payment_cityinfo">
        <input type="hidden" id="payment_zip" name="payment_zip">
        <input type="hidden" id="payment_phone" name="payment_phone">
        <input type="hidden" id="total" name="total">
        <input type="hidden" id="itemcosttotal" name="itemcosttotal">
        <input type="hidden" id="walletamount" name="walletamount">
        <input type="hidden" id="is_combine_pay" name="is_combine_pay">
        <input type="hidden" id="cart_total_amount" name="cart_total_amount">

        <input type="hidden" id="isPromoApply" name="isPromoApply">
        <input type="hidden" id="discountAmount" name="discountAmount">

        <input type="hidden" id="startdatecu" name="startdatecu">
        <input type="hidden" id="enddate" name="enddate">
        <input type="hidden" id="timecu" name="timecu">
    </form>
    <?php
        include 'footer.php';
    ?>
    <script src="<?= $baseurl; ?>js/jquery-ui.min.js"></script>
    <script src="<?= $baseurl; ?>js/jquery-ui.multidatespicker.js"></script>
    <script src="<?= $baseurl; ?>js/timepicki.js"></script>
    <script type="text/javascript">
        $(function() {
            $('#from-datepicker').multiDatesPicker({
                minDate: 0,
                onSelect: function() {
                    $(this).data('datepicker').inline = true;                               
                },
                

            });

            $('#to-datepicker').datepicker({
                dateFormat: 'dd-mm-yy'
                
            });
            
            $(".timepicker").timepicki();
        });
            
        $(document).on('click touch', function(event) {
            if (!$(event.target).parents().addBack().is('.ui-datepicker,#from-datepicker')) {
                $('.ui-datepicker').css('display','none');
            }
        });

        $('#from-datepicker').on('click',function(){
            $('.ui-datepicker').css('display','block');
        });

/*
By:Jyoti Vishwakarma
Description: On page ready called required function
*/

        $(document).ready(function() {

            
            getCuisines();  // For cart 
            getStates();    // state list
            getWalletAmount();  // wallet amount
            activeChoosedMealType(); // Meal Type
            //For auto fill delivery address
            <?php
                    if(isset($_SESSION['userid'])){  ?>
                        var delivery = $('input[name=loc_address_title]:checked').val();
                        // console.log(delivery);
                        if (delivery == 'Home') {
                            getUserAddress(delivery);
                        }
            <?php   }    ?>
           

            getUserDetails(); // fetch user details
         
        });

/*
By:Jyoti Vishwakarma
Description: Apply coupon code on cart
*/
        $('#applypromocodebutton').click(function(){
            applypromocode();
        });

        /*
        By:Jyoti Vishwakarma
        Description: Fetch state list in dropdown
        */
    
        function getStates(){
            $.ajax({
                url: serviceurl + 'states',
                type: 'POST',
                datatype: 'JSON',
                async: true,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = '<option value="">Select State</option>';
                    for (var i = 0; i < value.states.length; i++) {
                        html += '<option value="' + value.states[i].name + '">' + value.states[i].name + '</option>';
                    }
                    $("#loc_state").html(html);
                }
            });
        }
        /*
        By:Jyoti Vishwakarma
        Description: Fetch and dispaly cart details
        */
        function getCuisines() {
            var datecount = "";
            // if($('.ui-state-highlight').length > 0){
            //     datecount = $('.ui-state-highlight').length;
            // }
            var datecount = $('#from-datepicker').multiDatesPicker('getDates').length;
            var cartdata = {
                "datecount": datecount
            };
            $.ajax({
                url: serviceurl + 'cartdetail',
                type: 'POST',
                data: JSON.stringify(cartdata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                    var addonprice = 0;
                    // console.log(value.cuisines.length);
                    if(value.cuisines.length == 0){
                        $(".cart-wrapper").css("align-items","flex-start");
                        $("#cartdetails").hide();
                        $("#cartdetails_nodata").show();
                        $("#cartdetails1").hide();
                        $("#cartdetail2").hide();
                        $("#cartdetails3").hide();
                        $("#cartdetails4").hide();
                        $("#cartdetails5").hide();
                        $(".order-title-wrapper").hide();
                        $(".cart-profile").hide();
                        $("#orderdata").hide();
                        $("#emptycart").removeAttr( "Onclick" );
                        $("#applypromocode").val("");
                        $("#usedpromocode").hide();
                        $("#subscriptionoffer").hide();
                        $("#subsoffer").html(0);
                    }
                    else{
                        $("#orderdata").show();
                        $("#cartdetails_nodata").hide();
                        $(".cart-profile").show();
                        var isNormalCuisine = '0';
                        var kitchens_timing =[];
                        for (var i = 0; i < value.cuisines.length; i++) {
                            var customizable = "";
                            var kitchenopen = value.cuisines[i].kitchen_open;
                            var kitchenclose = value.cuisines[i].kitchen_close;
                            var d = new Date();
                            var datesss = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                            var min = new Date(datesss + ' ' + kitchenopen);
                            var max = new Date(datesss + ' ' + kitchenclose);
                            var kitchenalert = '';
                            if ($('#from-datepicker').length > 0 && $('#delivery-time').length > 0) {
                                if ($('#from-datepicker').val() != "" && $('#delivery-time').val() != "") {
                                    var d = new Date();
                                    var datesss = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                                    var s = new Date($('#from-datepicker').val() + ' ' + $('#delivery-time').val());


                                    var min = new Date($('#from-datepicker').val() + ' ' + kitchenopen);
                                     var max = new Date($('#from-datepicker').val() + ' ' + kitchenclose);
                                    console.log(min +"-------"+ max);
                                    console.log(s);
                                    if(min.getTime() > s.getTime() || s.getTime() >  max.getTime()){
                                        kitchenalert = '   <p style="color:red;font-size: 12px;">KITCHEN CLOSED</p>';
                                        $("#iskitchenClosed").val('1');
                                    }
                                    else{
                                        $("#iskitchenClosed").val('0');
                                    }
                                }
                                else{
                                    if(value.cuisines[i].isNormalCuisine == '0'){
                                        kitchenalert = '';
                                        $("#iskitchenClosed").val('0');
                                    }else{
                                        if(min.getTime() > d.getTime() || d.getTime() >  max.getTime()){
                                            kitchenalert = '   <p style="color:red;font-size: 12px;">KITCHEN CLOSED</p>';
                                            $("#iskitchenClosed").val('1');
                                        }
                                    }
                                    
                                }
                            }
                            else{
                                if(value.cuisines[i].isNormalCuisine == '0'){
                                    kitchenalert = '';
                                    $("#iskitchenClosed").val('0');
                                }else{
                                    if(min.getTime() > d.getTime() || d.getTime() >  max.getTime()){
                                            kitchenalert = '   <p style="color:red;font-size: 12px;">KITCHEN CLOSED</p>';
                                            $("#iskitchenClosed").val('1');
                                        }
                                }
                            }
                            // if(value.cuisines[i].isCustomisable == '1' ){
                            //     customizable = '<p class="menu-custom" onclick="customizecuisine('+ value.cuisines[i].id +')">customise ></p>';
                            // }else{
                            //     customizable = '<p class="menu-custom"></p>';
                            // }
                            html += '<div class="order-item-context"><div class="order-title-wrapper"><div class="removeItem" onclick="removeCart(' + value.cuisines[i].id + ')"></div><div class="order-title-details"><h2>' + value.cuisines[i].name + '</h2><p>' + value.cuisines[i].kitchen_name + kitchenalert +'</p><p class="menu-custom"></p></div><input type="hidden" id="minqty-' + value.cuisines[i].id + '" value="' + value.cuisines[i].minqty + '"><div class="order-quantity"><a href="javascript:;" onclick="updateCart(' + value.cuisines[i].id + ',\'less\')" class="quantity-icon sub1"><h2>-</h2></a><input type="text" class="quantity-input quantity' + value.cuisines[i].id + '" value="' + value.cuisines[i].quantity + '" readonly><a href="javascript:;" onclick="updateCart(' + value.cuisines[i].id + ',\'add\')" class="quantity-icon add1 active"><h2>+</h2></a></div><div class="order-title-price"><p>&#8377; <span class="itemprice" id="itemPrice' + value.cuisines[i].id + '">' + value.cuisines[i].price + '</span><input value="' + value.cuisines[i].priceact + '" type="hidden" id="priceAct' + value.cuisines[i].id + '"></p></div></div></div>';

                            if(value.cuisines[i].recommanded.length > 0){
                                
                                html += '<p class="text-muted">Also Check this from ' + value.cuisines[i].kitchen_name + '</p><div class="addon-wrapper">';
                                for(var j=0;j<value.cuisines[i].recommanded.length;j++){
                                    
                                    html += '<div class="addon-items"><div class="addon-main"><img src="'+value.cuisines[i].recommanded[j].image+'" alt="" ></div><div class="addon-container"><h3>'+value.cuisines[i].recommanded[j].cuisine_name+'</h3><div class="addon-context"><p>&#8377; '+value.cuisines[i].recommanded[j].price+'</p><a href="javascript:;" class="link-green" onclick="addToCartRecommand('+ value.cuisines[i].recommanded[j].ID +')">+ Add</a></div></div></div>';
                                }
                                html += '</div>';
                            }

                            if(value.cuisines[i].isNormalCuisine == '1'){
                                isNormalCuisine = '1';
                            }
                           

                            // Add on packages

                            // <div class="chipsWrap">
                            //     <div class="chip">Cheez (+30) <span>x</span></div>
                            //     <div class="chip">Kimchi Salad (+50) <span>x</span></div>
                            //     <div class="chip">Ice Tea (+50) <span>x</span></div>
                            //     <div class="chip">Dimsum (+100) <span>x</span></div>
                            //     <div class="chip">+5 More <span>x</span></div>
                            // </div>


                            // if(value.isTeamMeal == "1"){
                            if(value.cuisines[i].sections.length > 0){
                               
                                if(value.cuisines[i].isCustomisable == '1'){
                                    html += '<div class="addon" id="cartdetails"><h2 class="bill-title" >Add on Packages</h2><div class="chipsWrap">';
                                    var looplimit = value.cuisines[i].sections.length;
                                    var more = 0;
                                    if(value.cuisines[i].sections.length > 5){
                                        looplimit = 5;
                                        more = value.cuisines[i].sections.length - 5;
                                    }
                                    for(var j=0;j<looplimit;j++){
                                        html += '<div class="chip">'+ value.cuisines[i].sections[j].detail+' (+'+ value.cuisines[i].sections[j].price * value.cuisines[i].quantity +') <span onclick="removeAddons('+value.cuisines[i].sections[j].addon_id+','+ value.cuisines[i].id +')">x</span></div>';
                                         addonprice = addonprice + (value.cuisines[i].sections[j].price * value.cuisines[i].quantity);
                                    } 
                                    if(value.cuisines[i].sections.length != value.cuisines[i].totaladdons || more != 0){
                                        if(more != 0){
                                            html +=  '<div class="chip" style="cursor:pointer;" onclick="customizecuisine('+ value.cuisines[i].id +')">+'+more+' More <span>x</span></div>';
                                        }else{
                                            html +=  '<div class="chip" style="cursor:pointer;" onclick="customizecuisine('+ value.cuisines[i].id +')"> Add More <span>x</span></div>';
                                        }
                                    }
                                    
                                    html += '</div></div><br>';


                                }else{
                                    html += '<div class="addon" id="cartdetails"><h2 class="bill-title" >Add on Packages</h2>';
                                    for(var j=0;j<value.cuisines[i].sections.length;j++){
                                        if(value.cuisines[i].sections[j].addons.length > 0){
                                            html += '<h2 class="bill-title">'+value.cuisines[i].sections[j].title+'</h2>';
                                            var addon = value.cuisines[i].sections[j].addons;
                                            if(addon.length > 0){
                                                for(var k=0;k<addon.length;k++){
                                                    html +=   '<div class="order-item-conatiner"><div class="order-list-qty"><p>'+ addon[k].detail +'</p></div><div class="order-list-cost">&#8377;'+ addon[k].price * value.cuisines[i].quantity +'</div></div>';
                                                        addonprice = addonprice + (addon[k].price * value.cuisines[i].quantity);
                                                }
                                            }
                                        }
                                            
                                    }
                                    html += '</div><br>';
                                }
                                    
                                

                            }
                            if(value.cuisines[i].isPartyMeal == '1'){
                                var ispartymeal = 1;
                                $(".promoText").hide();
                                $(".promoCode").hide();
                            }
                           
                        }
                    
                        $("#orderdata").html(html);
                        $("#totalcostitem").html(value.totalitemprice);
                        $("#totalcost").html(value.totalprice);
                        $("#sgst").html(value.sgst);
                        $("#cgst").html(value.cgst);
                        $("#btn_total").html(value.totalprice);
                        $("#cart_total_amount").val(value.totalprice);
                        if(value.discountprice != '0'){
                            $("#subscriptionoffer").show();
                            $("#subsoffer").html(value.discountprice);
                            $("#usedpromocode").hide();
                            $("#promoapplied").val(0);
                            $("#applypromocode").val('');
                            $(".successText").css("display","none");
                            $(".errorText").css("display","none");
                        }
                        else{
                            $("#subscriptionoffer").hide();
                            $("#subsoffer").html(0);
                            if(ispartymeal != 1){
                                <?php 
                                        if(isset($_COOKIE["couponcode"]) && $_COOKIE["couponcode"]!=""){  ?>
                                        $("#applypromocode").val('<?=$_COOKIE["couponcode"]?>');
                                            applypromocode();
                                            // $("#usedpromocode").show();
                                            // $("#promoapplied").val(1);
                                <?php   }   ?>
                            }
                            

                        }
                        if(value.isTeamMeal != "0"){
                            $("#delivery-details").hide();
                        }

                        if(isNormalCuisine == '1'){
                            $("#isNormalCuisine").val('1');
                        }
                    }
                }
            });
        }
        /*
        By:Jyoti Vishwakarma
        Description: Update quantity of cuisines
        */
        function updateCart(id, type) {
           
            var countex = $(".quantity" + id).val();
            var minqty = parseInt($("#minqty-"+id).val());
            if (type == 'add') {
                var count1 = parseInt(countex) + 1;
               
            } else {
                var count1 = parseInt(countex) - 1;
            }
            $(".quantity" + id).val(countex);
            if(count1 < minqty){
                $(".quantity" + id).val(minqty);
                count1 = minqty;
                return false;
            }
            if (count1 < 1) {
                count1 = 1;
                $(".quantity" + id).val('1');
            }

            if (count1 < 1) {
                return;
            }

            var count12 = count1;
            var cartdata = {
                "id": id,
                "count": count1
            };
            $.ajax({
                url: serviceurl + 'cartupdate',
                type: 'POST',
                data: JSON.stringify(cartdata),
                contentType: 'application/json; charset=utf-8',
                datatype: 'JSON',
                async: true,
                success: function(data) {
                    getCuisines();
                    applypromocode();
                }
            });
        }

        /*
        By:Jyoti Vishwakarma
        Description: Remove cuisines fom cart
        */      

        function removeCart(id) {

            var cartdata = {
                "id": id
            };
            $.ajax({
                url: serviceurl + 'removecart',
                type: 'POST',
                data: JSON.stringify(cartdata),
                contentType: 'application/json; charset=utf-8',
                datatype: 'JSON',
                async: true,
                success: function(data) {

                    getCuisines();
                    showCartData();
                    applypromocode();
                    $("#addonListdata").hide();
                }
            });
        }

        /*
        By:Jyoti Vishwakarma
        Description: Login from cart
        */

        function cartLogin() {
            $(".linkBtn").click();
        }

        function cartsignUp() {
            $(".signUp").click();
        }
        
        $('#countyss').click(function(){
            chackdate();
        });

        function chackdate(){
            
            $("#errorOrderTime").hide();
            $("#delivery-info").toggleClass("open");
            $(".cart-profile-info.delivery-info").slideToggle("open");
            $("#delivery-details").toggleClass("open");
            $(".cart-profile-info.delivery-details").slideToggle("open");

            getCuisines();

        }

        /*
        By:Jyoti Vishwakarma
        Description: Checkout process
        */
        function checkoutOrder() {
            
            $(".loader").show();
            var del_pincode = $('#loc_pincode').val();
            var pagedata = {
                "pincode": del_pincode
            };
            $.ajax({
                url: serviceurl + 'pincode_checkout',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    if (value.status == 'success') {

                            var wallet_amount = 0;
                            var combine_pay = 0;
                            if ($("#checkbox_wallet").prop("checked") == true) {
                                wallet_amount = parseInt($('#walletamount').html());
                                combine_pay = 1;
                            }

                            var payment_type = $("input[name='payment_method']:checked").val();
                            if (payment_type == 'online') {
                                var name = $('#loc_full_name').val();
                                var mobile = $('#loc_mobile_number').val();
                                var state = $('#loc_state').val();
                                var city = $('#loc_city').val();
                                var landmark = $('#loc_landmark').val();
                                var pincode = $('#loc_pincode').val();
                                var address = $('#loc_address').val();
                                var total = $('#totalcost').html();
                                var itemcosttotal = $('#totalcostitem').html();

                                var startdate = "";
                                var enddate = "";
                                var time = "";
                                if ($('#from-datepicker').length > 0 && $('#delivery-time').length > 0) {
                                    startdate = $('#from-datepicker').val();
                                    time = $('#delivery-time').val();
                                    // alert(startdate);
                                    // return;
                                }

                                var fulladdress = name + ", " + address + ", " + landmark + ", " + city + ", " + state + ", " + pincode;

                                $("#payment_firstname").val(name);
                                $("#payment_delivery_address").val(fulladdress);
                                $("#payment_stateinfo").val(state);
                                $("#payment_cityinfo").val(city);
                                $("#payment_zip").val(pincode);
                                $("#payment_phone").val(mobile);
                                $("#total").val(total);
                                $("#itemcosttotal").val(itemcosttotal);
                                $("#walletamount").val(wallet_amount);
                                $("#is_combine_pay").val(combine_pay);

                                $("#isPromoApply").val($("#promoapplied").val());
                                $("#discountAmount").val(parseInt($('#discount_amount_pay').html()));

                                $("#startdatecu").val(startdate);
                                $("#enddatecu").val(enddate);
                                $("#timecu").val(time);

                                $("#checkout").submit();
                            } else {
                                var name = $('#loc_full_name').val();
                                var mobile = $('#loc_mobile_number').val();
                                var state = $('#loc_state').val();
                                var city = $('#loc_city').val();
                                var landmark = $('#loc_landmark').val();
                                var pincode = $('#loc_pincode').val();
                                var address = $('#loc_address').val();
                                var total = $('#totalcost').html();
                                var totalitemprice = $('#totalcostitem').html();
                                var fulladdress = name + ", " + address + ", " + landmark + ", " + city + ", " + state + ", " + pincode;
                                var userid = "<?= isset($_SESSION['userid']) ? $_SESSION['userid'] : '' ?>";
                                var cart_total_amount = $('#cart_total_amount').val();

                                var startdate = "";
                                var enddate = "";
                                var time = "";
                                if ($('#from-datepicker').length > 0 && $('#delivery-time').length > 0) {
                                    startdate = $('#from-datepicker').val();
                                    time = $('#delivery-time').val();
                                }


                                var ispromoapply = $("#promoapplied").val();
                                var discountamount = parseInt($('#discount_amount_pay').html()); 


                                var cartdata = {
                                    "userid": userid,
                                    "mobile": mobile,
                                    "delivery_address": fulladdress,
                                    "paymenttype": "COD",
                                    "total": total,
                                    "totalitemprice": totalitemprice,
                                    "combine_pay": combine_pay,
                                    "cart_total_amount": cart_total_amount,
                                    "wallet_amount": wallet_amount,
                                    "startdate": startdate,
                                    "order_time": time,
                                    "isPromoApply" :ispromoapply,
                                    "discountamount" : discountamount
                                };
                                
                                $.ajax({
                                    url: serviceurl + 'checkout',
                                    type: 'POST',
                                    data: JSON.stringify(cartdata),
                                    contentType: 'application/json; charset=utf-8',
                                    datatype: 'JSON',
                                    async: true,
                                    success: function(data) {
                                        var value = JSON.parse(data);
                                        if (value.status) {
                                            window.location.href = '<?= $baseurl; ?>order-details.php?status=success&orderId=' + value.orderid;
                                        } else {
                                            alert("something went wrong!");
                                        }
                                    }
                                });
                            }
      

                       
                    } else {
                        $(".loader").hide();
                        $('.overlay,.cartPincodePopups').fadeIn();
                            setTimeout(function() {
                                $('.overlay,.cartPincodePopups').fadeOut();
                            }, 2000);
                            }
                }
            });

      
        }
        /*
        By:Jyoti Vishwakarma
        Description: get User address based on type
        */
        function getUserAddress(type) {
            $("#loc_full_name").val('');
            $("#loc_mobile_number").val('');
            $("#loc_state").val('');
            $("#loc_city").val('');
            $("#loc_landmark").val('');
            $("#loc_pincode").val('');
            $("#loc_address").val('');
            var addtype = {
                "type": type
            };
            $.ajax({
                url: serviceurl + 'getUserAddress',
                type: 'POST',
                data: JSON.stringify(addtype),
                contentType: 'application/json; charset=utf-8',
                datatype: 'JSON',
                async: true,
                success: function(data) {
                    var result = JSON.parse(data);

                    $("#loc_full_name").val(result[0]['fullname']);
                    $("#loc_mobile_number").val(result[0]['mobile']);
                    $("#loc_state").val(result[0]['state']);
                    $("#loc_city").val(result[0]['city']);
                    $("#loc_landmark").val(result[0]['landmark']);
                    $("#loc_pincode").val(result[0]['pincode']);
                    $("#loc_address").val(result[0]['address']);
                }
            });
        }

        $("#delivery-info").click(function() {
            $("#paymnet-details").slideUp();
            $(".cart-profile-info.payment-method").slideUp();
            $("#delivery-info").removeClass('completed');
        });

        /*
        By:Jyoti Vishwakarma
        Description: Save user address
        */
        $("form[name='location_form']").validate({
            rules: {
                loc_full_name: "required",
                loc_mobile_number: {
                    required: true,
                    minlength: 10,
                    maxlength: 10,
                    number: true
                },
                loc_state: "required",
                loc_city: "required",
                loc_pincode: {
                    required: true,
                    minlength: 6,
                    maxlength: 6,
                    number: true
                },
                loc_address: "required",
                loc_address_title: "required"
            },
            submitHandler: function(form) {
                var data = new FormData($('#location_form')[0]);
                $.ajax({
                    url: serviceurl + 'createaddress',
                    type: 'POST',
                    mimeType: "multipart/form-data",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data: data,
                    success: function(data) {

                        var del_pincode = $('#loc_pincode').val();
                        var pagedata = {
                            "pincode": del_pincode
                        };        
                        $.ajax({
                            url: serviceurl + 'pincode_checkout',
                            type: 'POST',
                            data: JSON.stringify(pagedata),
                            datatype: 'JSON',
                            async: false,
                            success: function(data) {
                                var value = JSON.parse(data);
                                if (value.status == 'success') {
                                    // $("#location_flag").val('1');
                                    // $("#paymnet-details").slideDown();
                                    // $(".cart-profile-info.payment-method").slideDown();
                                    // $(".cart-profile-info.delivery-info").slideUp();
                                    // $("#delivery-info").addClass('completed');
                                    // $("#delivery-info").removeClass('open');
                                    // $(".delivery-info").removeClass('open');
                                    var iskitchenclosed = $("#iskitchenClosed").val();
                                    if($('#from-datepicker').length == 0 && $('#delivery-time').length == 0) {
                                            var d = new Date();
                                            var datesss = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                                            var min = new Date(datesss + ' ' + '10:45 AM');
                                            var max = new Date(datesss + ' ' + '11:30 PM');

                                            var isNormalcuisine = $("#isNormalCuisine").val();

                                            if(isNormalcuisine == '0'){
                                                $("#location_flag").val('1');
                                                $("#paymnet-details").slideDown();
                                                $(".cart-profile-info.payment-method").slideDown();
                                                $(".cart-profile-info.delivery-info").slideUp();
                                                $("#delivery-info").addClass('completed');
                                                $("#delivery-info").removeClass('open');
                                                
                                                $(".delivery-info").removeClass('open');
                                            }else{
                                                // if(min.getTime() > d.getTime() || d.getTime() >  max.getTime()){

                                                if(iskitchenclosed == '1'){
                                                    $(".loader").hide();
                                                    $('.overlay,.errorOrderTimePopup').fadeIn();
                                                    setTimeout(function() {
                                                        $('.overlay,.errorOrderTimePopup').fadeOut();
                                                    }, 2000);
                                                }
                                                else{
                                                    $("#location_flag").val('1');
                                                    $("#paymnet-details").slideDown();
                                                    $(".cart-profile-info.payment-method").slideDown();
                                                    $(".cart-profile-info.delivery-info").slideUp();
                                                    $("#delivery-info").addClass('completed');
                                                    $("#delivery-info").removeClass('open');
                                                    $(".delivery-info").removeClass('open');
                                                }
                                            }
                                            
                                    }else{

                                        if($('#from-datepicker').length > 0 && $('#delivery-time').length > 0) {
                                            dd = $('#from-datepicker').val();
                                            ss = $('#delivery-time').val();
                                            if (dd == "" && ss == "") {
                                                var d = new Date();
                                                var datesss = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                                                var min = new Date(datesss + ' ' + '10:45 AM');
                                                var max = new Date(datesss + ' ' + '11:30 PM');
                                                var isNormalcuisine = $("#isNormalCuisine").val();
                                                if(isNormalcuisine == '0'){
                                                    $("#location_flag").val('1');
                                                    $("#paymnet-details").slideDown();
                                                    $(".cart-profile-info.payment-method").slideDown();
                                                    $(".cart-profile-info.delivery-info").slideUp();
                                                    $("#delivery-info").addClass('completed');
                                                    $("#delivery-info").removeClass('open');
                                                    $(".delivery-info").removeClass('open');
                                                }else{
                                                    // if(min.getTime() > d.getTime() || d.getTime() >  max.getTime()){
                                                    if(iskitchenclosed == '1'){
                                                        $(".loader").hide();
                                                        $('.overlay,.errorOrderTimePopup').fadeIn();
                                                        setTimeout(function() {
                                                            $('.overlay,.errorOrderTimePopup').fadeOut();
                                                        }, 2000);
                                                    }
                                                    else{
                                                        $("#location_flag").val('1');
                                                        $("#paymnet-details").slideDown();
                                                        $(".cart-profile-info.payment-method").slideDown();
                                                        $(".cart-profile-info.delivery-info").slideUp();
                                                        $("#delivery-info").addClass('completed');
                                                        $("#delivery-info").removeClass('open');
                                                        $(".delivery-info").removeClass('open');
                                                        }
                                                }
                                            
                                            }else{
                                                startdate = $('#from-datepicker').val();
                                                time = $('#delivery-time').val();

                                                var ar = startdate.split(', '); // split string on comma space

                                                var datesss = ar[0];

                                                var orddate = datesss + ' ' + time;
                                                orddate = new Date(orddate);

                                                var min = new Date(datesss + ' ' + '10:45 AM');
                                                var max = new Date(datesss + ' ' + '11:30 PM');

                                                // if(min.getTime() > orddate.getTime() || orddate.getTime() >  max.getTime()){
                                                if(iskitchenclosed == '1'){
                                                    $(".loader").hide();
                                                    $('.overlay,.errorOrderTimePopup').fadeIn();
                                                    setTimeout(function() {
                                                        $('.overlay,.errorOrderTimePopup').fadeOut();
                                                    }, 2000);
                                                
                                                }
                                                else{
                                                    
                                                    $("#location_flag").val('1');
                                                    $("#paymnet-details").slideDown();
                                                    $(".cart-profile-info.payment-method").slideDown();
                                                    $(".cart-profile-info.delivery-info").slideUp();
                                                    $("#delivery-info").addClass('completed');
                                                    $("#delivery-info").removeClass('open');
                                                    
                                                    $(".delivery-info").removeClass('open');
                                                }
                                            }
                                         }
                                    }
                                }else {
                                    $(".loader").hide();
                                    $('.overlay,.cartPincodePopups').fadeIn();
                                    setTimeout(function() {
                                        $('.overlay,.cartPincodePopups').fadeOut();
                                    }, 2000);
                                }
                            }
                        });
                                  
                            

                    }
                });

            }
        });

        function getWalletAmount() {
            $.ajax({
                url: serviceurl + 'walletAmount',
                type: 'POST',
                async: false,
                success: function(data) {
                    var result = JSON.parse(data);
                    if (result.length > 0) {
                        $("#walletamount").html(result[0].cloudkitchValue);
                    }
                }
            });
        }
        /*
        By:Jyoti Vishwakarma
        Description: Pay Using wallet amount
        */
        function payUsingWallet() {
            if ($("#checkbox_wallet").prop("checked") == true) {
                $("#usedWalletDiv").show();
                var pay = 0;
                var wallet_amount = parseInt($('#walletamount').html());
                var totalpay = parseInt($('#totalcost').html());
                if (wallet_amount >= totalpay) {
                    balwallet = wallet_amount - totalpay;
                    $("#is_wallet_pay").val(1); // 1 for is wallte pay true and 0 for false
                    $("#is_combine_pay").val(0);
                    $("#wallet_amount_pay").html(totalpay);
                    $("#totalcost").html(0);
                    $("#btn_total").html(0);
                } else {
                    pay = totalpay - wallet_amount;
                    $("#is_wallet_pay").val(1); // 1 for is wallte pay true and 0 for false
                    $("#is_combine_pay").val(1);
                    $("#wallet_amount_pay").html(wallet_amount);
                    $("#totalcost").html(pay);
                    $("#btn_total").html(pay);
                    // $('#walletamount').html(0)
                }
            } else {
                $("#usedWalletDiv").hide();
                var paid_wallet = parseInt($("#wallet_amount_pay").html());
                applypromocode();
                var total_wallet = wallet_amount + paid_wallet;
                $("#totalcost").html($("#cart_total_amount").val());
                $("#btn_total").html($("#cart_total_amount").val());
                
            }
        }
        
        function applypromocode1(){
            var promocode = $("#applypromocode").val();
            if(promocode == "RD20"){
                $(".successText").html("Success");
                $(".successText").css("display","block");
                $(".errorText").css("display","none");
            }
            else{
                $(".errorText").html("Invalid Code");
                $(".errorText").css("display","block");
                
            }
        }
        /*
        By:Jyoti Vishwakarma
        Description: Apply coupon code
        */

        function applypromocode(){
        
            var promocode = $("#applypromocode").val();
            if(promocode  != ''){
                
                var userid = "<?= isset($_SESSION['userid']) ? $_SESSION['userid'] : '' ?>";
                var addtype = {
                    "userId": userid,
                    "couponcode":promocode

                };
                $.ajax({
                    url: serviceurl + 'verifcoupon',
                    type: 'POST',
                    data: JSON.stringify(addtype),
                    contentType: 'application/json; charset=utf-8',
                    datatype: 'JSON',
                    async: true,
                    success: function(data) {
                        var result = JSON.parse(data);
                        console.log(result.msg);
                            var discount = result.discount;
                            
                            var totalpay = parseInt($('#totalcostitem').html());
                            var sgst = parseFloat($("#sgst").html());
                            var csgt = parseFloat($("#cgst").html());
                        if (result.status == 'success') {
                           
                            if(totalpay >= 350){
                                $("#usedpromocode").show();
                                var x = totalpay + sgst + csgt;
                                var per =Math.round((discount/100)*x);
                                finalamount = totalpay + sgst + csgt - per;
                                $("#discount_amount_pay").html(per);
                                $("#totalcost").html(finalamount.toFixed(2));
                                $("#btn_total").html(finalamount.toFixed(2));
                                
                                $("#promoapplied").val(1);
                                $(".successText").html(result.msg);
                                $(".successText").css("display","block");
                                $(".errorText").css("display","none");
                                $("#promoapplied").val(1);
                                var d = new Date();
                                d.setTime(d.getTime() + (1*24*60*60*1000));
                                var expires = "expires="+ d.toUTCString();
                                document.cookie = "couponcode" + "=" + promocode + ";" + expires + ";path=/";
                            }else{
                                $("#usedpromocode").hide();
                                $("#promoapplied").val(0);
                                var x = totalpay + sgst + csgt;
                                var per = 0;//Math.round((discount/100)*x);
                                finalamount = totalpay + sgst + csgt - per;
                                $("#discount_amount_pay").html(per);
                                $("#totalcost").html(finalamount.toFixed(2));
                                $("#btn_total").html(finalamount.toFixed(2));
                                $(".successText").css("display","none");
                                $(".errorText").html("Minimum order amount should be 350 Rs.");
                                $(".errorText").css("display","block");
                                promocode = '';
                                var d = new Date();
                                d.setTime(d.getTime() + (1*24*60*60*1000));
                                var expires = "expires="+ d.toUTCString();
                                document.cookie = "couponcode=;" + expires + ";path=/";
                                document.cookie = "couponcode=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                            }

                        }else{
                            $("#usedpromocode").hide();
                            $("#promoapplied").val(0);
                            var x = totalpay + sgst + csgt;
                            var per = 0;//Math.round((discount/100)*x);
                            finalamount = totalpay + sgst + csgt - per;
                            $("#discount_amount_pay").html(per);
                            $("#totalcost").html(finalamount.toFixed(2));
                            $("#btn_total").html(finalamount.toFixed(2));
                            
                            $(".successText").css("display","none");
                            $(".errorText").html(result.msg);
                            $(".errorText").css("display","block");
                            promocode = '';
                            var d = new Date();
                            d.setTime(d.getTime() + (1*24*60*60*1000));
                            var expires = "expires="+ d.toUTCString();
                            // document.cookie = "couponcode=;" + expires + ";path=/";
                            document.cookie = "couponcode=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                        }
                    }
                });
            }
        }

        /*
        By:Jyoti Vishwakarma
        Description: recommand cuisine add to cart
        */
        function addToCartRecommand(id){
            // var count = $("#itemcount"+id).val();
            var count = 1;
            var data = {
                "id": id,
                "count": count,
                "selectedaddons": ""
            };
            $.ajax({
                url: serviceurl + 'addtocart',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    if (value.status == 'success') {
                        getCuisines();
                        var cartCount = $(".notiCount").html();
                        $(".notiCount").html(parseInt(cartCount) + 1);
                        $(".toast").fadeIn();
                        setTimeout(function(){
                            $(".toast").fadeOut();
                        }, 3000);
                        $("#addedTocart-"+id).parent('.addBtn').delay(200).fadeOut();
                        
                    } else {
                    
                        $('.overlay,.cartPopup').fadeIn();
                        setTimeout(function() {
                            $('.overlay,.cartPopup').fadeOut();
                        }, 1200);
                    }
                }
            });
        }

        function getUserDetails(){
               
               $.ajax({
                   url: serviceurl + 'getUserDetails',
                   type: 'POST',
                   async: false,
                   success: function(data)
                   {
                       var result = JSON.parse(data);
                    //    $("#id").val(result.id);
                       $("#loc_full_name").val(result.name);
                       var pin = "";
                       var pincookie = '<?= $_COOKIE['pincode'] ?>';
                       if(pincookie != ""){
                            pin = pincookie; 
                       }
                       $("#loc_pincode").val(pin);
                       if(result.phone != ""){
                           $("#loc_mobile_number").val(result.phone);
                       }
                   }
               });
        }

        /*
        By:Jyoti Vishwakarma
        Description: Remove addons from cart
        */
        function removeAddons(addonid,cid){
            $(".loader").show();
            var cartdata = {
                "addonid": addonid,
                "cuisineid" : cid
            };
            $.ajax({
                url: serviceurl + 'removeaddon',
                type: 'POST',
                data: JSON.stringify(cartdata),
                contentType: 'application/json; charset=utf-8',
                datatype: 'JSON',
                async: true,
                success: function(data) {
                    getCuisines();
                    $(".loader").hide();
                }
            });
        }
      
    </script>
</body>

</html>