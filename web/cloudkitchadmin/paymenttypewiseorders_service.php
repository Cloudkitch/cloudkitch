<?php

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['servicename'] == 'PTW-ORDERS')
{
	$reqpto = file_get_contents('php://input');

	$respto = json_decode($reqpto,true);

	$month = '';

	$yr = '';

	$st = '';

	$invitecode = '';

	$month = $respto['month'];

	$yr = $respto['year'];

	$invitecode = $respto['invitecode'];

	$st = $respto['status'];

	$pto = array();

	$quepto = "SELECT SUM((SELECT count(*) FROM casseroleorderitem where cassitemid=coi.cassitemid AND 
				PaymentType='COD' AND cassordid = coi.cassordid group by PaymentType))as COD,
				SUM((SELECT count(*) FROM casseroleorderitem where cassitemid=coi.cassitemid AND 
				PaymentType='OLP' AND cassordid = coi.cassordid group by PaymentType)) as OLP,
				SUM((SELECT count(*) FROM casseroleorderitem where cassitemid=coi.cassitemid AND 
				PaymentType='MW' AND cassordid = coi.cassordid group by PaymentType))as MTP,
				SUM((SELECT count(*) FROM casseroleorderitem where cassitemid=coi.cassitemid AND 
				PaymentType='COD' AND MTP != 0 AND cassordid = coi.cassordid group by PaymentType))as CODMTP,
				SUM((SELECT count(*) FROM casseroleorderitem where cassitemid=coi.cassitemid AND 
				PaymentType='OLP' AND MTP != 0 AND cassordid = coi.cassordid group by PaymentType)) as OLPMTP
 				FROM casseroleorder as co,casseroleorderitem as coi 
				WHERE coi.cassordid=co.cassordid AND co.invitecode='".$invitecode."' 
				AND YEAR(co.orderdate)='".$yr."' 
				AND MONTH(co.orderdate)='".$month."' AND coi.status = '".$st."'";

	$excpto = mysqli_query($conn,$quepto) or die(mysqli_error($conn));

	if(mysqli_num_rows($excpto) > 0)
	{	
		$pto['pto'] = array();

		while($rspto = mysqli_fetch_assoc($excpto))
		{
			$po = array();

			if($rspto['COD'] != null)
			{
				$po['cod'] = $rspto['COD'];	
			}
			else
			{
				$po['cod'] = '0';
			}	

			if($rspto['OLP'] != null)
			{
				$po['olp'] = $rspto['OLP'];
			}
			else
			{
				$po['olp'] = '0';
			}

			if($rspto['MTP'] != null)
			{
				$po['mtp'] = $rspto['MTP'];
			}
			else
			{
				$po['mtp'] = '0';
			}

			if($rspto['CODMTP'] != null)
			{
				$po['codmtp'] = $rspto['CODMTP'];
			}
			else
			{
				$po['codmtp'] = '0';
			}

			if($rspto['OLPMTP'] != null)
			{
				$po['olpmtp'] = $rspto['OLPMTP'];
			}
			else
			{
				$po['olpmtp'] = '0';
			}

			array_push($pto['pto'], $po);
		}	
		

		$pto['status'] =  'success';

		$pto['msg'] = 'data available';
	}
	else
	{
		$pto['status'] = 'failure';
		$pto['msg'] = 'not available';
	}

	print_r(json_encode($pto));
	exit;				
}

?>	