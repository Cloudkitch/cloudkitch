<?php

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['service_name'] == 'paidtochefs')
{
	$reqptc = file_get_contents('php://input');

	$resptc = json_decode($reqptc,true);

	$invitecode = '';

	$invitecode = $resptc['invitecode'];

	$itemids = '';

	$ptc = array();

	$queptc = "SELECT cop.ccopid,cop.chefid,u.name as chefname,cop.total,cop.paymenttype,cop.itemid,cop.createdate,cop.paidstatus FROM chefcancelorderpayout as cop,user as u WHERE u.userid=cop.chefid AND cop.invitecode='".$invitecode."' ORDER BY cop.ccopid DESC";

	$excptc = mysqli_query($conn,$queptc) or die(mysqli_error($conn));

	if(mysqli_num_rows($excptc) > 0)
	{
		$ptc['chefs'] = array();

		while($rowptc = mysqli_fetch_assoc($excptc))
		{
			$c = array();

			$c['ccopid'] = $rowptc['ccopid'];

			$c['chefid'] = $rowptc['chefid'];

			$c['chefname'] = $rowptc['chefname'];

			$c['total'] = $rowptc['total'];

			$c['paymenttype'] = $rowptc['paymenttype'];

			$c['itemid'] = $rowptc['itemid'];

			$c['paymentdate'] = $rowptc['createdate'];

			$c['paidstatus'] = $rowptc['paidstatus'];

			$itemids = explode(',', $rowptc['itemid']);

			$quecfc = "SELECT count(c.cuisineid) as cus FROM casseroleorderitem as
					   coi,cuisine as c WHERE c.cuisineid=coi.cuisineid and coi.chefid='".$rowptc['chefid']."' and coi.cassitemid IN  ('".implode("','", $itemids)."')";

			//echo $quecfc;		   
			$excfc = mysqli_query($conn,$quecfc) or die(mysqli_error($conn));
			
			$rscfc = mysqli_fetch_assoc($excfc);

			$c['cuisines'] = $rscfc['cus'];

			array_push($ptc['chefs'], $c);
		}

		$ptc['status'] = 'success';
		$ptc['msg'] = 'Chef lists available';

	}
	else
	{
		$ptc['status'] = 'failure';
		$ptc['msg'] = 'No data available';
	}

	print_r(json_encode($ptc));
	exit;	
}

if($_GET['service_name'] == 'cuisine-detail')
{
    $reqcd = file_get_contents('php://input');

	$rescd = json_decode($reqcd,true);

	$itemid = '';
	$cfid = '';

	$itemid = $rescd['itemid'];

	$cfid = $rescd['chefid'];

	$quecd =  "SELECT coi.cassitemid,c.cuisineid,c.cuisinename FROM codcancelorders as coi,cuisine as c WHERE cassitemid in (".$itemid.") and c.cuisineid=coi.cuisineid and coi.chefid='".$cfid."' group by c.cuisineid";

	//echo $quecd;	

	$exccd = mysqli_query($conn,$quecd) or die(mysqli_error($conn));

	$cd = array();

	if(mysqli_num_rows($exccd) > 0)
	{
		$cd['cu'] = array();

		while($rowc = mysqli_fetch_assoc($exccd))
		{
			$c = array();

			$c['iid'] = $rowc['cassitemid'];

			$c['cuisinename'] = $rowc['cuisinename'];

			$quedt = "SELECT createdate FROM chefcancelorderpayout WHERE find_in_set('".$rowc['cassitemid']."',itemid)";
			$excdt = mysqli_query($conn,$quedt) or die(mysqli_error($conn));

			$rsdt = mysqli_fetch_assoc($excdt);

			$c['date'] = $rsdt['createdate'];

			array_push($cd['cu'], $c);
		}

		$cd['status'] = 'success';
		$cd['msg'] = 'Data available';
	}
	else
	{
		$cd['status'] = 'failure';
		$cd['msg'] = 'not available';
	}

	print_r(json_encode($cd));
	exit;
}

if($_GET['service_name'] == 'closepayout-cancelorder')
{
	$reqcop = file_get_contents('php://input');

	$rescop = json_decode($reqcop,true);

	$chefpid= '';

	$chefpid = $rescop['chefpid'];

	$cpco =  array();

	$updco = "UPDATE chefcancelorderpayout SET paidstatus='DONE' WHERE ccopid='".$chefpid."'";
	$excco = mysqli_query($conn,$updco) or die(mysqli_error($conn));

	if($excco)
	{
		$cpco['status'] = 'success';
		$cpco['msg'] = 'Paid successfully';
	}
	else
	{
		$cpco['status'] = 'failure';
		$cpco['msg'] = 'Failed to paid';
	}

	print_r(json_encode($cpco));
	exit;	
}

?>