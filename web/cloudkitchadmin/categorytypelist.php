<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: https://coludkitch.co.in/cloudkitchadmin/index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Cuisine Type List</h1>
                </div>
            </div>
            
            <div class="col-sm-6">
              <div class="header-section" style="float:right;margin-bottom: 3px;">
              <a href="addcategorytype.php"><button class="btn btn-rounded btn-primary"><i class="fa fa-plus"></i> Add Category Type</button></a>
              </div>
            </div>  
        </div>
    </div>

    <!-- <div class="row" style="float:right;margin-bottom: 3px;">
            
            <a href="addcategorytype.php"><button class="btn btn-rounded btn-primary"><i class="fa fa-plus"></i> Add Category Type</button></a>

        </div> -->


    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tablecategorytypeList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Category Type</th>
                                                 <th>Action</th>
                                                 </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script src="js/CasseroleService.js"></script>



<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    // var type = '?php 
    //echo $_SESSION['info']['type']; ?>';
    // var wedcode = '';
    // wedcode = '2616';
    // console.log("My Type Is :"+type);
    // if(type == "admin")
    // {
    //     getWeddingList();
    // }

    //invitationlist();
    //dishlist();
    categorytypelist();
    // if(type == "subadmin") 
    // {
    //    getWeddingListBySubadmin(wedcode); 
    // }   
    });
    
    function deletedata(id)
            {
    
              swal({
                title: "Are You sure want to delete this?",
                //text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                  var request = {"id":id};
                  $.ajax({
                    url: 'service.php?servicename=deletecategorytype',
                    type: 'POST',
                    data: JSON.stringify(request),
                    contentType: 'application/json; charset=utf-8',
                    datatype: 'JSON',
                    async: true,
                    success: function(data)
                    {
                      var result = JSON.parse(data);
                      if(result.status == 'success')
                      {
                        categorytypelist();
                        $("#toast-error").html(result.msg);
                        $("#toasterError").fadeIn();
                      }
                      else
                      {
                        $("#toast-error").html(result.msg);
                        $("#toasterError").fadeIn();
                      }
                      setTimeout(function(){
                        $("#toaster").fadeOut();
                        $("#toasterError").fadeOut();
                      }, 3000);
                    }
                  });
                } 
              });

            }
    </script>