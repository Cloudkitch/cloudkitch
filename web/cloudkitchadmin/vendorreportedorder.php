<?php 

session_start();


//print_r($_SESSION);

if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/Casseroleadmin/index.php");
    header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
// include 'inc/config.php'; $template['header_link'] = 'WELCOME ';
 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Validation Header -->
    <!-- <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Create Invitation Code</h1>
                </div>
            </div>
           
        </div>
    </div> -->

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- END Validation Header -->

    <div class="row" id="col">

    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">

            <div class="col-sm-10 col-md-12 col-lg-12">
            
                <div class="block">

    
                <div class="block-title">
                    <h2>Reported Orders</h2>
                    <button type="button" id="exptexcel" class="btn btn-rounded btn-success" style="float:right">Export</button>
                </div>
           

      
                <table id ="tablccList" class="table table-vcenter table-striped table-hover table-borderless">

                                                <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Reported By</th>
                                                 <th>Reported Against</th>
                                                 <th>Orderid</th>
                                                 <th>itemname</th>
                                                 <th>ReportedDateTime</th>
                                                 <th>Reason</th>
                                                 <th>Action</th>
                                                 <th>User Comments</th>
                                                 <th>Vendor Comments</th>
                                                 </tr> 
                                                </thead>  
                  
                </table>
            

                </div>

            </div>
    </div>
</div>


      <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" >
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">                            
                                <h5 class="modal-title" id="myModalLabel"  style="font-weight: bold">Reported Details</h5>
                            </div>
                            <div class="modal-body">
                               <div id="bodyShowInfo" >
                                <div class="col-md-12">
                                   <label >Add Narration Here</label> </div>
                                   <div class="col-md-12">
                                   <textarea rows="4" cols="100" maxlength="150" id ="textarea" >
                                  
                                    </textarea>
                                  <div id="textarea_feedback"></div></div>
                               </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal"  id="ok" onclick="resolvevendor();">OK</button>
                                
                            </div>
                        </div>
                    </div>
                </div>


 <?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">

$(document).ready(function() {
    $("#loading").hide();
    $("#cht").hide();


    var text_max = 150;
    $('#textarea_feedback').html(text_max + ' characters remaining');

    $('#textarea').keyup(function() {
        var text_length = $('#textarea').val().length;
        var text_remaining = text_max - text_length;

        $('#textarea_feedback').html(text_remaining + ' characters remaining');
    });
   // $("#col").hide();
    
    // users();

    // chapters();

  //    $("#exptexcel").click(function(e) {
  //   e.preventDefault();

  //   //getting data from our table
  //   var data_type = 'data:application/vnd.ms-excel';
  //   var table_div = document.getElementById('tablccList');
  //   var table_html = table_div.outerHTML.replace(/ /g, '%20');

  //   var a = document.createElement('a');

  //   a.href = data_type + ', ' + table_html;
  //        var dt = new Date();
  //       var day = dt.getDate();
  //       var month = dt.getMonth() + 1;
  //       var year = dt.getFullYear();
  //       var hour = dt.getHours();
  //       var mins = dt.getMinutes();
  //       var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
  //   a.download = 'MyTree_Reported_Orders' + postfix + '.xls';
  //   a.click();
  // }); click function commented by priya added new functionjust below

 function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;
    // CSV FILE
    csvFile = new Blob([csv], {type: "text/csv"});
    // Download link
    downloadLink = document.createElement("a");
    // File name
    downloadLink.download = filename;
    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);
    // Make sure that the link is not displayed
    downloadLink.style.display = "none";
    // Add the link to your DOM
    document.body.appendChild(downloadLink);
    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("table tr");
    
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText);        
        csv.push(row.join(","));       
    }
    // Download CSV
    download_csv(csv.join("\n"), filename);
}

document.querySelector("#exptexcel").addEventListener("click", function () {
    var html = document.querySelector("table").outerHTML;
    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    filename = 'MyTree_Reported_Orders' + postfix + '.csv';
    export_table_to_csv(html, filename);
});
    


});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    
    $('#tablccList').dataTable().fnClearTable();
        $('#tablccList').dataTable().fnDraw();
        $('#tablccList').dataTable().fnDestroy();

        reportedorders();
       
      });</script>


<script type="text/javascript">

function reportedorders()
{

    var ic = '';

    ic = $("#invitecode").val()

    var reqro = {"invitecode":ic}; 


    $.ajax({
             url: 'vendorpanicmode_service.php?servicename=Reported-Orders',
             type: 'POST',
             datatype: 'JSON',
             data: JSON.stringify(reqro),
             async: false,
             success: function(rro)
             {
               console.log("CC"+JSON.stringify(rro));

               var rescc = JSON.parse(rro);

                var co = new Array();

               if(rescc.status == 'success')
               {
                   for(var ch=0;ch<rescc.reportedo.length;ch++)
                   {

                      co[ch] = new Array();

                      co[ch][0] = ch+1;

                      co[ch][1] = rescc.reportedo[ch].user1;

                      co[ch][2] = rescc.reportedo[ch].user2;

                      co[ch][3] = rescc.reportedo[ch].orderid;

                      co[ch][4] = rescc.reportedo[ch].dishname;

                      co[ch][5] = rescc.reportedo[ch].panicdatetime;

                      co[ch][6] = rescc.reportedo[ch].reason;

                      co[ch][7] = '<button type="button" data-toggle="modal"  data-itemid='+rescc.reportedo[ch].itemid+' id="res" data-target=".bs-example-modal-lg" class="btn btn-effect-ripple btn-primary">Reported </button>';  

                      co[ch][8] = rescc.reportedo[ch].uop;                

                       co[ch][9] = rescc.reportedo[ch].cop;                

               
                   }
               }
               // else
               // {
               //      co[] = '<p>Not Available</p>';
               // } 

               

                $('#tablccList').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true,
                        "order": [[ 5, 'desc' ]]                                              
                       });




             }

    });
}

</script>

<script type="text/javascript">

    function resolvevendor()
    {
       var cmd = $('#res').attr('data-itemid');
       var nar = $.trim($('#textarea').val());   

         var d = new Date();
         var n = d.toISOString();
       
          var itid = {"itemid":cmd,"Narration":nar,"Narrationdt":n};

           console.log(itid);
          // return;

       
        $.ajax({
                 url:'vendorpanicmode_service.php?servicename=ResolvedVendor',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(itid),                 
                 success: function(rsres)
                 {
                    console.log("Resolved"+JSON.stringify(rsres));

                    var rsrs = JSON.parse(rsres);
                   
                    if(rsrs.status == 'success')
                    {
                        location.reload();
                    }
                    else
                    {
                        alert('Something went wrong..!!');
                    }    
                 }


        });
    }

</script>





