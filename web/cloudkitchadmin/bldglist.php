<?php 
session_start();

//print_r($_SESSION);
//print_r($_SESSION);

if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: https://coludkitch.co.in/cloudkitchadmin/index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Building Detail</h1>
                    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
                </div>
            </div>
           
        </div>
    </div>

    <div class="row" style="float:right;margin-bottom: 3px;">
            
            <a href="addbldg.php"><button class="btn btn-rounded btn-primary"><i class="fa fa-plus"></i> Add Building</button></a>

        </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tablebldgList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Building Name</th>
                                                 <th>Invitecode</th>
                                                 <!-- <th>Create Date</th> -->
                                                 </tr> 
                                            </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script src="js/CasseroleService.js"></script>



<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    // var type = '?php 
    //echo $_SESSION['info']['type']; ?>';
    // var wedcode = '';
    // wedcode = '2616';
    // console.log("My Type Is :"+type);
    // if(type == "admin")
    // {
    //     getWeddingList();
    // }

    //invitationlist();
    //dishlist();
    //cuisinetypelist();
    //rechargelist();
    builinglist();
    // if(type == "subadmin") 
    // {
    //    getWeddingListBySubadmin(wedcode); 
    // }   
    });
 function builinglist()
 {
    var invitecode = '';

    invitecode = $("#invitecode").val();

    var reqcl = {"invitecode":invitecode};
 
    $('#loading').show();
      $.ajax({

              type: "POST",
              url: 'service.php?servicename=buildingdetail',
              datatype: "JSON",
              data: JSON.stringify(reqcl),
              success:function(bl)
              {
                  $('#loading').hide();

                  console.log("Building List Are"+JSON.stringify(bl));

                  var bld = JSON.parse(bl);

                  var b = new Array();

                  for(var bl=0;bl<bld.bldgs.length;bl++)
                  {
                     b[bl] = new Array();

                     b[bl][0] = bl+1;

                     b[bl][1] = bld.bldgs[bl].building;

                     b[bl][2] = bld.bldgs[bl].invitationcode;
                  }  

                   $('#tablebldgList').dataTable({
                        "aaData": b,
                        "bDestroy": true
                    });


              }

    })
 }


</script>