<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <!-- <a href="#myModal" class="btn btn-lg btn-primary" data-toggle="modal">Launch
      Demo Modal</a> -->
    <!-- Modal HTML -->
    <div id="myModalcuisines" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Cuisine Detail</h4>
          </div>
          <div class="modal-body">

          <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tablecuList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Cuisine Name</th>
                                                 <th>Paid Date</th>
                                                                                                
                                                                                              
                                                </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>

           

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>
        </div>
      </div>
    </div>

    
  </div>

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Chef Payout Cancellation History</h1>
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
         <button type="button" id="exptexcel" class="btn btn-rounded btn-success" style="float:right">Export</button>
    <br><br>
        <div class="col-sm-10 col-md-12 col-lg-12">
        <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
            <!-- Form Validation Block -->
            <table id ="tableChefphListcancel" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Chef Name</th>
                                                 <th>Cuisines</th>
                                                 <!-- <th>Payment Breakdown</th> -->
                                                 <th>Payment Mode</th>
                                                 <th>Payment Date</th>
                                                 <th>Total</th>
                                                 <th>Status</th>
                                                 </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->



<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<!-- <script src="js/CasseroleService.js"></script> -->

<!-- <script src="js/order.js"></script> -->


<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    $('#tableChefphListcancel').dataTable().fnClearTable();
        $('#tableChefphListcancel').dataTable().fnDraw();
        $('#tableChefphListcancel').dataTable().fnDestroy();

        // $('#tableOrderItemList').dataTable().fnClearTable();
        // $('#tableOrderItemList').dataTable().fnDraw();
        // $('#tableOrderItemList').dataTable().fnDestroy();

        
    // var type = '<?php 
    //echo $_SESSION['info']['type']; ?>';
    // var wedcode = '';
    // wedcode = '2616';
    // console.log("My Type Is :"+type);
    // if(type == "admin")
    // {
    //     getWeddingList();
    // }

    //invitationlist();
    //customerlist();
    chefpayhistorycancel();
    // if(type == "subadmin") 
    // {
    //    getWeddingListBySubadmin(wedcode); 
    // }   
    });</script>

    <script type="text/javascript">
        
    function chefpayhistorycancel()
    {
       var invitecode = '';

       invitecode = $("#invitecode").val();

       var reqhc = {"invitecode":invitecode};

        $("#loading").show();
        $.ajax({
                 url: 'chef-cancel-order-payout-history.php?service_name=paidtochefs',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(reqhc),
                 async: false,
                 success: function (col)
                 {
                    $("#loading").hide();
                     console.log("Cheforders Response"+JSON.stringify(col));

                     var rscol = JSON.parse(col);

                     var co = new Array();

                     for(var oc=0;oc<rscol.chefs.length;oc++)
                     {
                        co[oc] = new Array();

                        co[oc][0] = oc+1;

                        co[oc][1] = rscol.chefs[oc].chefname;

                        co[oc][2] = '<a href="#myModalcuisines" onclick="opencuisinedetail('+rscol.chefs[oc].chefid+',\''+rscol.chefs[oc].itemid+'\');" data-toggle="modal">'+rscol.chefs[oc].cuisines+'</a>';

                        // co[oc][3] = 'MTP : ' + rscol.chefs[oc].MTP + ' | CASH : '+ rscol.chefs[oc].CASH;

                        //<button type="button" class="btn btn-rounded btn-success">Success</button>

                         //co[oc][3] = rscol.chefs[oc].pmtmode;

                         if(rscol.chefs[oc].paymenttype == 'CASH')
                         {
                            co[oc][3] = '<button style="pointer-events: none;" type="button" class="btn btn-rounded btn-success">'+rscol.chefs[oc].paymenttype+'</button>';
                         }
                         else if(rscol.chefs[oc].paymenttype == 'MTP')
                         {
                            co[oc][3] = '<button style="pointer-events: none;" type="button" class="btn btn-rounded btn-danger">'+rscol.chefs[oc].paymenttype+'</button>';
                         }   

                        co[oc][4] = rscol.chefs[oc].paymentdate;

                        co[oc][5] = rscol.chefs[oc].total;

                        if(rscol.chefs[oc].paidstatus == 'DONE')
                        {
                           co[oc][6] = '<button style="pointer-events: none;" type="button" class="btn btn-square btn-primary">'+rscol.chefs[oc].paidstatus+'</button>'; 
                        }
                        else if(rscol.chefs[oc].paidstatus == 'PENDING')
                        {
                            co[oc][6] = '<button id="'+rscol.chefs[oc].cfpid+'" onclick="setpaidstatus(\''+rscol.chefs[oc].ccopid+'\')" type="button" class="btn btn-square btn-warning">'+rscol.chefs[oc].paidstatus+'</button>';
                        }    

                                                                                                                              
                    }

                    $('#tableChefphListcancel').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true
                //         dom: '<"html5buttons"B>lTfgitp',
                // buttons: [
                //     { extend: 'copy'},
                //     {extend: 'csv'},
                //     {extend: 'excel', title: 'ExampleFile'},
                //     {extend: 'pdf', title: 'ExampleFile'},

                //     {extend: 'print',
                //      customize: function (win){
                //             $(win.document.body).addClass('white-bg');
                //             $(win.document.body).css('font-size', '10px');

                //             $(win.document.body).find('table')
                //                     .addClass('compact')
                //                     .css('font-size', 'inherit');
                //         }
                //     }
                // ]

                    });   
                 }    
        });
    }

    </script>

    <script type="text/javascript">
      
    function opencuisinedetail(cfid,itmid)
    {
        console.log("Cfid"+cfid+"Itmid"+itmid);

        var reqcd =  {"itemid":itmid,"chefid":cfid};

        $.ajax({
                 url: 'chef-cancel-order-payout-history.php?service_name=cuisine-detail',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(reqcd),
                 async: false,
                 success: function(rcd)
                 {
                   console.log("Rs"+JSON.stringify(rcd));

                   var pimg = JSON.parse(rcd);

                  var pm = new Array();

                  for(var po=0;po<pimg.cu.length;po++)
                  {
                     pm[po] = new Array();

                     pm[po][0] = po+1;

                     pm[po][1] = pimg.cu[po].cuisinename;

                     pm[po][2] = pimg.cu[po].date;
                  }

                   $('#tablecuList').dataTable({
                        "aaData": pm,
                        "bDestroy": true
                    });
                 }



        });
    }

    </script>

    <script type="text/javascript">
        
        function exporttoexcel()
        {
           // alert("Hii");
        //     $('#tableChefOrderList').tableExport({type:'excel',escape:'false'});

        //     var dt = new Date();
        // var day = dt.getDate();
        // var month = dt.getMonth() + 1;
        // var year = dt.getFullYear();
        // var hour = dt.getHours();
        // var mins = dt.getMinutes();
        // var postfix = day + "." + month + "." + year + "_" + hour;

        //     var a = document.createElement('a');
        // //getting data from our div that contains the HTML table
        // var data_type = 'data:application/vnd.ms-excel';
        // var table_div = document.getElementById('tableChefOrderList');
        // var table_html = table_div.outerHTML.replace(/ /g, '%20');
        // a.href = data_type + ', ' + table_html;
        // //setting the file name
        // a.download = 'Mytree_' + postfix + '.xls';
        // //triggering the function
        // a.click();
        // //just in case, prevent default behaviour
        // e.preventDefault();
        }
    </script>

    <script type="text/javascript">
        
        $(document).ready(function() {

          $("#frmtm").val('');
          $("#totm").val('');

         function download_csv(csv, filename) {
            var csvFile;
            var downloadLink;
            // CSV FILE
            csvFile = new Blob([csv], {type: "text/csv"});
            // Download link
            downloadLink = document.createElement("a");
            // File name
            downloadLink.download = filename;
            // We have to create a link to the file
            downloadLink.href = window.URL.createObjectURL(csvFile);
            // Make sure that the link is not displayed
            downloadLink.style.display = "none";
            // Add the link to your DOM
            document.body.appendChild(downloadLink);
            // Lanzamos
            downloadLink.click();
        }

        function export_table_to_csv(html, filename) {
            var csv = [];
            var rows = document.querySelectorAll("table tr");
            
            for (var i = 0; i < rows.length; i++) {
                var row = [], cols = rows[i].querySelectorAll("td, th");
                
                for (var j = 0; j < cols.length; j++) 
                    row.push(cols[j].innerText);        
                csv.push(row.join(","));       
            }
            // Download CSV
            download_csv(csv.join("\n"), filename);
        }

        document.querySelector("#exptexcel").addEventListener("click", function () {
            var html = document.querySelector("table").outerHTML;
            var dt = new Date();
            var day = dt.getDate();
            var month = dt.getMonth() + 1;
            var year = dt.getFullYear();
            var hour = dt.getHours();
            var mins = dt.getMinutes();
            var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
            filename = 'MyTree_CancelOrder_Paidhistory' + postfix + '.csv';
            export_table_to_csv(html, filename);
        });

});


    </script>

    
    <!-- <script type="text/javascript">
        
    function reset()
    {
        $("#frmdt").val('');

        $("#todt").val('');

        cheforderlist();

   }

    </script> -->

    <script type="text/javascript">
        
        function setpaidstatus(cfpid)
        {
            console.log("Chefpid"+cfpid);

            var reqcf = {"chefpid":cfpid};

            $.ajax({
                     url:'chef-cancel-order-payout-history.php?service_name=closepayout-cancelorder',
                     type: 'POST',
                     datatype: 'JSON',
                     data: JSON.stringify(reqcf),
                     async:false,
                     success: function(rscf)
                     {
                        console.log("CP"+JSON.stringify(rscf));

                        var rcp = JSON.parse(rscf);

                        if(rcp.status == 'success')
                        {
                            location.reload();
                        }
                        else
                        {
                            alert('Failed tchefpido update');
                        }    


                     }
            })
        }

    </script>