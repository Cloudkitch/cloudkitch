<?php
    include 'inc/databaseConfig.php';
    $id = $_GET['itemid']; 
    // $id = base64_decode($idencoded); 
    $quecasseroleorder = "SELECT co.cassordid as orderid,co.total as totalamount,co.orderdate,co.orderstatus,co.PaymentType,co.actualamount,co.discountamount,
    co.couponapplied,(select couponcode FROM coupon WHERE couponid=co.couponid) as couponcode,co.cloudkitchpoints,co.delivery_address,co.mobile,co.startdate,
    co.order_time,co.offerapplied,u.name as customer,u.phone as customerphone,u.isCorporateUser,u.corporateusercode FROM casseroleorder as co,user as u,user as r,
    casseroleorderitem as coi,cuisine as c WHERE co.userid=u.userid and coi.cassordid=co.cassordid AND co.cassordid = '$id' AND coi.chefid = r.userid AND c.userid = r.userid AND c.userid = coi.chefid and c.cuisineid=coi.cuisineid  GROUP BY co.cassordid";
    $exccasseroleorder = mysqli_query($conn,$quecasseroleorder) or die(mysqli_error($conn));
    $rowcord = mysqli_fetch_assoc($exccasseroleorder);

    $orderid = $rowcord['orderid'];
    $orderdate = date('d/m/Y h:i A', strtotime($rowcord['orderdate']));
    $paymenttype = $rowcord['PaymentType'];
    $customer = $rowcord['customer'];
    $customerphone = $rowcord['customerphone'];
    $delivery_address = $rowcord['delivery_address'];
    $totalamount = $rowcord['totalamount'];

    $actualamount = $rowcord['actualamount'];
    $gst = round(2.5 / 100 * $actualamount,2);
    $discount = "";
    if( $rowcord['discountamount'] != "" && $rowcord['discountamount']!=0){
        $discount = '<tr>
                        <td colspan="3">
                            <p>Discount</p>
                        </td>
                        <td>
                            <p>₹ '.$rowcord['discountamount'].'</p>
                        </td>
                    </tr>';
    }

    if( $rowcord['cloudkitchpoints'] != "" && $rowcord['cloudkitchpoints']!=0){
        $discount = '<tr>
                        <td colspan="3">
                            <p>Wallet Points</p>
                        </td>
                        <td>
                            <p>₹ '.$rowcord['cloudkitchpoints'].'</p>
                        </td>
                    </tr>'; 
    }
   


    $queitems = "SELECT (SELECT name FROM user as u1 WHERE u1.userid=coi.chefid ) as chefname, 
    (SELECT phone FROM user as u1 WHERE u1.userid=coi.chefid ) as chefphone,
    (SELECT address FROM user as u1 WHERE u1.userid=coi.chefid ) as chefaddress, 
    (SELECT flatno FROM user as u1 WHERE u1.userid=coi.chefid ) as cheffaltno, 
    (SELECT building FROM user as u1 WHERE u1.userid=coi.chefid ) as chefbuilding,
    c.cuisinename,c.cuisinetype,coi.quantity,coi.price,coi.quantity * coi.price as total, coi.selected_addons,coi.special_instructions 
    FROM casseroleorderitem as coi,cuisine as c WHERE coi.cassordid='$orderid' and coi.cuisineid = c.cuisineid";
    $excitems = mysqli_query($conn,$queitems) or die(mysqli_error($conn));
    $ordercuisines = "";
    while($rowitems = mysqli_fetch_assoc($excitems)){
           
        $rowitems['selected_addons'] = str_replace("'","",$rowitems['selected_addons']);
        $addon = "";
        if($rowitems['selected_addons'] != ""){
            
            $selectedaddons = array_filter(explode(",",$rowitems['selected_addons']));
           
            foreach ($selectedaddons as $value) {
            $split = explode("-",$value);
            $addonsid = $split[0];
            $addonprice = @$split[1];
            $query3 = "SELECT * FROM corporate_cuisine_sections_addons WHERE ID = '$addonsid'";
            $result3 = mysqli_query($conn,$query3) or die(mysqli_error($conn));
                if(mysqli_num_rows($result3) > 0){
                    while($row3 = mysqli_fetch_assoc($result3))
                    { 
                        $addon .= '<div style="background-color: #fff;font-size: 12px;margin: 10px 10px 0 0;padding: 2px 12px;border-radius: 10px;position: relative;">'. $row3['addon'] .' (+₹ '. $addonprice .')</div>';
                    
                    }
                }

            }
            
        }

        $ordercuisines .= '<tr>
                            <td>
                                <div style="display: flex;"> <p>'.$rowitems['chefname'].'</p> </div>
                                <p style="font-family: OpenSans-Bold;">'.$rowitems['cuisinename'].'</p>
                                '.$addon.'
                            </td>
                            <td>
                                <p>'.$rowitems['quantity'].'</p>
                            </td>
                            <td>
                                <p>'.$rowitems['price'].'</p>
                            </td>
                            <td>
                                <p>₹ '.$rowitems['total'].'</p>
                            </td>
                        </tr>';

    }

 

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Reciept</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
        <style>
            body{width:70%;margin:0 auto;border:1px solid #5E6A6C;}
            *{padding: 0;margin:0;font-family: 'Open Sans', sans-serif;box-sizing: border-box;}
            p{font-size:14px;line-height:22px;}
            table{width: 100%;padding: 20px;border-collapse: collapse;margin:25px 0;}
            .centerText{text-align: center;}
            tr td,tr th{padding: 5px 20px;}
            .orderTable tr:first-child{background-color: #a38379;text-align: left;}
            .orderTable tr:first-child h3{color: #fff;}
            .orderTable tr td:last-child,table tr th:last-child,.addessTable tr td:last-child{text-align: right;}
        </style>
    </head>
    <body>
        <table>
            <tr class="centerText">
                <td>
                    <img src="http://cloudkitch.co.in/images/logo.png">
                </td>
            </tr>
            <!-- <tr class="centerText">
                <td>
                    <h3><?=$chefname?></h3>
                    <p><?=$tagline?></p>
                </td>
            </tr> -->
        </table>
        <table class="addessTable">
            <tr>
                <td>
                    <h3>Delivery Address</h3>
                    <p style="max-width: 400px;"><?=$delivery_address?><br>Phone: <?=$customerphone?></p>
                </td>
                <td>
                    <h3>Order Details</h3>
                    <p>Order ID: <?=$orderid?><br>Date: <?=$orderdate?></p>
                </td>
            </tr>
        </table>
        <table class="orderTable">
            <tr>
                <th>
                    <h3>Description</h3>
                </th>
                <th>
                    <h3>Quantity</h3>
                </th>
                <th>
                    <h3>Price</h3>
                </th>
                <th>
                    <h3>Amount</h3>
                </th>
            </tr>
            <?=$ordercuisines?>
           <!--  <tr style="border-top: 1px solid #d8d8d8;">
                <td colspan="3">
                    <p>Total</p>
                </td>
                <td>
                    <p><?=$total1?></p>
                </td>
            </tr> -->
            <tr>
                <td colspan="3">
                    <p>2.5% SGST</p>
                </td>
                <td>
                    <p>₹ <?=$gst?></p>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <p>2.5% CGST</p>
                </td>
                <td>
                    <p>₹ <?=$gst?></p>
                </td>
            </tr>
            <?=$discount?>
            <tr style="border-top: 1px solid #d8d8d8;">
                <td colspan="3">
                    <h3>Grant Total</h3>
                </td>
                <td>
                    <h3>₹ <?=$totalamount?></h3>
                </td>
            </tr>
        </table>
    </body>
</html>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function(){
      window.print();
  });
</script>