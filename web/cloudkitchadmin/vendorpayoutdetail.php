
<?php //include 'inc/config.php'; $template['header_link'] = 'FORMS'; 

if(!isset($_POST['itmids'])){

    header('Location: vendorpayout.php');
}

session_start();
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
     header("Location: index.php");
}



 include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user']; 
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">

  <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Vendor Payout Detail</h1>
                </div>
            </div>
           
        </div>
    </div>

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />

              
        </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <div class="block">
<div class="block-title">
<h2>Vendor Payout Detail</h2>
</div>
<form id="validation-wizard" action="" method="post" class="form-horizontal form-bordered ui-formwizard" novalidate="novalidate">

              <input type="hidden" name="itmids" id="itmids" value="<?php echo $_POST['itmids'];?>">

              <input type="hidden" name="olpv" id="olpv" value="<?php echo $_POST['olpv'];?>">

              <input type="hidden" name="codv" id="codv" value="<?php echo $_POST['vcodv'];?>">
              <input type="hidden" name="ptmv" id="ptmv" value="<?php echo  $_POST['ptmv'];?>">

              <input type="hidden" name="cfid" id="cfid" value="<?php echo $_POST['cfid'];?>">

              <input type="hidden" name="cuid" id="cuid" value="<?php echo $_POST['cuid'];?>">


              <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">

<div id="validation-first" class="step ui-formwizard-content" style="display: block;">
<div class="form-group">
<div class="col-xs-12">
<ul class="nav nav-pills nav-justified">
<li class="active disabled"><a href="javascript:void(0)" class="text-muted"> <i class="fa fa-user"></i> <strong>Step1</strong></a></li>
<li class="disabled"><a href="javascript:void(0)"><i class="fa fa-info-circle"></i> <strong>Step2</strong></a></li>
</ul>
</div>
</div>

<div class="row">
        <div class="col-sm-4" id="olpbox">
        <div class="widget">
            <div class="widget-content themed-background-dark text-light-op text-center">
                <i class="fa fa-money"></i> <strong>OLP</strong>
            </div>
            <div class="widget-content themed-background-muted text-center">
                <i class="fa fa-rupee fa-3x text-success"></i><h2 class="widget-heading text-dark" id="olprs"></h2>
            </div>
            <div class="widget-content">
                <div class="row text-center">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="radio">
                                <label for="example-radio1">
                                <input type="radio" id="cash" name="radolp" value="CASH"> Transfer amount to account
                                </label>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4" id="codbox">
        <div class="widget">
            <div class="widget-content themed-background-dark text-light-op text-center">
                <i class="fa fa-money"></i> <strong>COD</strong>
            </div>
            <div class="widget-content themed-background-muted text-center">
                <i class="fa fa-rupee fa-3x text-success"></i><h2 class="widget-heading text-dark" id="codrs"></h2>
            </div>
            <div class="widget-content">
                <div class="row text-center">
                    <div class="col-xs-12">
                        <!-- <div class="form-group">
                            <div class="radio">
                                <label for="example-radio1">
                                <input type="radio" id="cash" name="radolp" value="CASH"> Transfer amount to account
                                </label>
                            </div>
                        </div> -->
                    </div>
                
                </div>
            </div>
        </div>
    </div>


    <div class="col-sm-4" id="ptmbox">
        <div class="widget">
            <div class="widget-content themed-background-dark text-light-op text-center">
                <i class="fa fa-money"></i> <strong>PTM</strong>
            </div>
            <div class="widget-content themed-background-muted text-center">
                <i class="fa fa-rupee fa-3x text-success"></i><h2 class="widget-heading text-dark" id="ptmrs"></h2>
            </div>
            <div class="widget-content">
                <div class="row text-center">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="radio">
                                <label for="example-radio1">
                                <input type="radio" id="cash" name="radptm" value="CASH"> Transfer amount to account
                                </label>
                            </div>
                        </div>
                    </div>
        
                </div>
            </div>
        </div>
    </div>

</div>

<!-- <div class="form-group">
<label class="col-md-4 control-label">MTP</label>
<div class="col-md-6">
<p class="form-control-static">This is static text</p>
</div>
</div>

<div class="form-group">
<div class="col-md-4">
</div>
<div class="col-md-6">
<div class="radio">
<label for="example-radio1">
<input type="radio" id="cash" name="example-radios" value="CASH"> Would you like to use cash
</label>
</div>
<div class="radio">
<label for="example-radio2">
<input type="radio" id="mtp" name="example-radios" value="MTP"> Would you like to use MTP
</label>
</div>
</div>
</div>

<div class="form-group">
<label class="col-md-4 control-label">OLP</label>
<div class="col-md-6">
<p class="form-control-static">This is static text</p>
</div>
</div>

<div class="form-group">
<div class="col-md-4">
</div>
<div class="col-md-6">
<div class="radio">
<label for="example-radio1">
<input type="radio" id="cash" name="example-radios" value="CASH"> Would you like to use cash
</label>
</div>
<div class="radio">
<label for="example-radio2">
<input type="radio" id="mtp" name="example-radios" value="MTP"> Would you like to use MTP
</label>
</div>
</div>
</div> -->





</div>
<div id="validation-second" class="step ui-formwizard-content" style="display: none;">
<div class="form-group">
<div class="col-xs-12">
<ul class="nav nav-pills nav-justified">
<li class="disabled"><a href="javascript:void(0)" class="text-muted"><del><i class="fa fa-user"></i> <strong>Step1</strong></del></a></li>
<li class="active disabled"><a href="javascript:void(0)"><i class="fa fa-info-circle"></i> <strong>Step2</strong></a></li>
</ul>
</div>
</div>
<!-- <div class="form-group">
<label class="col-md-4 control-label" for="example-validation-suggestions">Suggestions</label>
<div class="col-md-8">
<textarea id="example-validation-suggestions" name="example-validation-suggestions" rows="5" class="form-control ui-wizard-content" placeholder="Share your ideas with us.." disabled="disabled"></textarea>
</div>
</div> -->
<!-- <div class="form-group">
<label class="col-md-4 control-label"><a href="#modal-terms" data-toggle="modal">Terms</a> <span class="text-danger">*</span></label>
<div class="col-md-6">
<label class="switch switch-primary" for="example-validation-terms">
<input type="checkbox" id="example-validation-terms" name="example-validation-terms" value="1" disabled="disabled" class="ui-wizard-content">
<span data-toggle="tooltip" title="" data-original-title="I agree to the terms!"></span>
</label>
</div>
</div> -->
<div class="col-sm-12">
<div class="widget">
<div class="widget-image widget-image-xs">
<div class="widget-image-content">
<h2 class="widget-heading text-light"><strong>CHEF PAYOUT</strong></h2>
<!-- <h3 class="widget-heading text-light-op h4">Favorite games you have to play</h3> -->
</div>
<i class="fa fa-money"></i>
</div>
<div class="widget-content widget-content-full">
<table class="table table-striped table-borderless remove-margin">
<tbody>
<tr id="olptr">
<td><a href="javascript:void(0)" class="text-black">OLP</a></td>
<td><a href="javascript:void(0)" class="text-black" id="olppt"></a></td>
<td class="text-center" style="width: 80px;"><span class="text-muted" id="olppv"></span></td>
</tr>
<tr id="ptmtr">
<td><a href="javascript:void(0)" class="text-black">PTM</a></td>
<td><a href="javascript:void(0)" class="text-black" id="ptmppt"></a></td>
<td class="text-center" style="width: 80px;"><span class="text-muted" id="ptmppv"></span></td>
</tr>

<tr>
<td><a href="javascript:void(0)" class="text-black"><b>Total</b></a></td>
<td><a href="javascript:void(0)" class="text-black"></a></td>
<td class="text-center" style="width: 80px;"><b><span class="text-muted" id="tot"></span></b></td>
</tr>

<tr id="codtr">
<td><a href="javascript:void(0)" class="text-black">COD</a></td>
<td><a href="javascript:void(0)" class="text-black" id="codpt"></a></td>
<td class="text-center" style="width: 80px;"><span class="text-muted" id="codpv"></span></td>
</tr>


</tbody>
</table>

<button  type="button" onclick="vpayNow();" class="btn btn-effect-ripple btn-primary" style="overflow: hidden; position: relative;margin-left: 530px;">Paynow</button>


</div>
</div>
</div>
</div>
<div class="form-group form-actions">
<div class="col-md-8 col-md-offset-4">
<input type="reset" class="btn btn-sm btn-warning" data-toback="redirect" id="back3" value="Back" onclick="redirect(this);">
<input type="submit" onclick="validate(this.value);" class="btn btn-sm btn-primary ui-wizard-content ui-formwizard-button" id="next3" value="Next">
</div>
</div>
</form>
</div>
            <!-- END Form Validation Block -->
        </div>
    </div>

    

    <!-- Cuisine Details -->

    


    <!-- END Form Validation Content -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script type="text/javascript" src="js/CasseroleService.js"></script>
 <script src="js/pages/formsWizard.js"></script> -->
<script>$(function(){ FormsWizard.init(); });</script>

<script type="text/javascript">
$(document).ready(function() {
    $("#loading").hide();
    $("#olpbox").hide();
   
    $("#ptmbox").hide();
    $("#codbox").hide();
    $("#olptr").hide();
   
    $("#ptmtr").hide();
    $('#back3').removeAttr('disabled');


    var itmid = '';

    itmid = $("#itmids").val();

    var olp = '';

    olp = $("#olpv").val();

    var cod = '';

    cod = $("#codv").val();



    var ptm = '';

    ptm = $("#ptmv").val();

    

    var cefid = '';

    cefid = $("#cfid").val();

    var cid = '';

    cid = $("#cuid").val();  

    var olptyp = '';

  

    var ptmtyp ='';

    console.log("Itmid :"+itmid+"ptm :"+ptm+"Olp :"+olp+"Cod :"+cod+"Chefid :"+cefid+" Cuid  :"+cid);

    // MTP
  
    // OLP
    if(olp != 0){
        $("#olpbox").show();
        $("#olprs").text(olp);
        olptyp = $("input[name='radolp']:checked").val();
    }

    // PAYTM
    if(ptm != 0 && ptm != null){
        $("#ptmbox").show();
        $("#ptmrs").text(ptm);
        ptmtyp = $("input[name='radptm']:checked").val();
    }

    if(cod != 0){
        $("#codbox").show();
        $("#codrs").text(cod);
    }


});
</script>

<script type="text/javascript">
    
    function validate(btn)
    {
        
        if(btn == 'Next')
        {
            var sbtbtntxt = $('#next3').val();
            if(sbtbtntxt=='Submit')
            {
                $('#next3').hide();
            } else {
                $('#next3').show();
                $('#back3').attr('data-toback','')
            }

            console.log("Entered Over Here"+btn);

            var itmid = ''; 

            itmid = $("#itmids").val();

            // var mtp = '';

            // mtp = $("#mtpv").val();

            // var olp = '';

            // olp = $("#olpv").val();

            var olp = '';

            olp = $("#olpv").val();

            var cod = '';

            cod = $("#codv").val();

           


            var ptm = '';

            ptm = $("#ptmv").val();

            var cefid = '';

            cefid = $("#cfid").val();

            var cid = '';

            cid = $("#cuid").val();  

            var olptyp = '';

          
            var ptmtyp = '';

            var folp = '';

            var tot = '';

            console.log("Itmid :"+itmid+"Olp :"+olp+"Cod :"+cod+"ptm :"+ptm+"Chefid :"+cefid+" Cuid  :"+cid);

            // MTP
  

            // OLP
            if(olp != 0){
                    $("#olptr").show();
                olptyp = $("input[name='radolp']:checked").val();

                // if(olptyp == 'CASH')
                // {
                //  folp = (70 * olp) / 100;
                // }
                // else if(olptyp == 'MTP')
                // {
                //  folp = olp;
                // }
                $("#olppv").text(olp);
                $("#olppt").text(olptyp);

            //      $("#mtppt").text(mtptyp);

            //      $("#ptmppt").text(ptmtyp);
                tot +=  parseFloat(olp);
                
            }

            // PAYTM
            if(ptm != 0){
                $("#ptmtr").show();
                
                ptmtyp = $("input[name='radptm']:checked").val();
                $("#ptmppt").text(ptmtyp)
                
                    $("#ptmppv").text(ptm);
                        tot+= parseFloat(ptm);
                        
            }

            if(cod != 0){
                $("#codtr").show();
                $("#codpv").text(cod);

            }
                
          
                $("#tot").text(tot);  

                
        }

        if(btn == 'Submit')
        {
            $("#next3").hide();
        }
    }

</script>

<script type="text/javascript">

function redirect(dis)
{
    // window.location.href='ChefPayout.php';
    var redirectto = $(dis).data('toback');
    if(redirectto == 'redirect'){
        window.location.href='vendorpayout.php';
    }

}
    
    function vpayNow()
    {
        
        console.log("hii");

      
        var olppm = '';

        var itmary = '';

      

        var olpvl = '';

        var ptmpm='';

        var ptmvl='';

        var codvl =''


        itmary = $("#itmids").val();

        var chefid = '';

        chefid = $("#cfid").val();

        var cuid = '';

        cuid = $("#cuid").val();

        var ic = '';

        ic = $("#invitecode").val();

        if($("#ptmppt").text() != '')
        {
            ptmpm = $("#ptmppt").text();    

            ptmvl = $("#ptmppv").text();
        }

        if($("#olppt").text() != '')
        {
            olppm = $("#olppt").text(); 

            olpvl = $("#olppv").text();
        }

        if($("#codpv").text() != '')
        {
            //codpm = $("#codpt").text();   

            codvl = $("#codpv").text();
        }   



        console.log("Olp paymode :"+olppm+  "PTM paymode :"+ptmpm+"Items :"+itmary+"Chefid :"+chefid+" Cuid :"+cuid +  "Olp Rs :"+olpvl+ "PTM Rs :"+ptmvl +"COD Rs :"+codvl);

        var reqchefp = {"olppm":olppm,"ptmpm":ptmpm,"itmary":itmary,"chefid":chefid,"cuid":cuid,"olp":olpvl,"ptm":ptmvl,"cod":codvl,"invitecode":ic}

        console.log("Req Paynow"+JSON.stringify(reqchefp));

        $.ajax({
                url: 'vendorpayoutservice.php?servicename=vfinalpaynew',
                type: 'POST',
                datatype: 'JSON',
                data: JSON.stringify(reqchefp),
                async: false,
                success: function(rsfp)
                {
                   console.log("Response"+JSON.stringify(rsfp));

                   var rsp = JSON.parse(rsfp);

                   if(rsp.status == 'success')
                   {
                        var n = noty({
                     layout: 'bottomRight',
                     text: 'Paid to Vendor Successfully',
                     theme:'relax',type: 'success',
                     timeout : '3000',
                     animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // document.getElementById("divcreateexp").style.display="none" ;
                                //document.getElementById("confirmView").style.display="block" ;
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });

                    setTimeout(redirect,1000);
                
                   }
                   else
                   {
                      var n = noty({
                         layout: 'bottomRight',
                         text: 'Failed to paid',
                         theme:'relax',type: 'error',
                         timeout : '3000',
                        animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });
                   } 
                }

      });
    }

</script>



<?php include 'inc/template_end.php'; ?>