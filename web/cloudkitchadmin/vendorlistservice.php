<?php

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';


if($_GET['servicename'] == 'VendorList')
{

	$reqcl = file_get_contents('php://input');

	$rescl = json_decode($reqcl,true);

	$invc = '';

	$invc = $rescl['invitecode'];

	// $quecl = "SELECT userid,name,email,phone,address,flatno,building FROM user WHERE ischef='1' ORDER BY userid DESC";

	$quecl = "SELECT * FROM user as u,invitationdetail as ivd,invitationusermap as ium WHERE ivd.id=ium.invitationid and ium.userid=u.userid and ivd.invitationcode='".$invc."' and u.isvendor='1'";
	$exccl = mysqli_query($conn,$quecl) or die(mysqli_error($conn));

	// echo $quecl.' hi';
	// exit;

	$cl = array();

	if(mysqli_num_rows($exccl) > 0)
	{
		$cl['vendors'] = array();

		while($rowcl = mysqli_fetch_assoc($exccl))
		{
			$v = array();

			$v['vendorid'] = $rowcl['userid'];

			$v['vendorname'] = $rowcl['name'];

			$v['email'] = $rowcl['email'];

			$v['phone'] = $rowcl['phone'];

			if($rowcl['address'] != '')
			{
				$v['address'] = $rowcl['address'];
			}
			else
			{
				$v['address'] = $rowcl['flatno'].'-'.$rowcl['building'];
			}

			$v['vendorverified'] = $rowcl['vendorverified'];

			array_push($cl['vendors'], $v);	
		}

		$cl['status'] = 'success';
		$cl['msg'] = 'Vendor list available';

	}
	else
	{
		$cl['status'] = 'failure';
		$cl['msg'] = 'Vendor list not found';
	}

	print_r(json_encode($cl));
	exit;	
}

?>