<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/Casseroleadmin/index.php");
	header("Location: index.php");
}

include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Get In Touch List</h1>
                    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tablecul" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                    <th>Sr</th>
                                                    <th>Name</th>     
                                                    <th>Email</th>                                            
                                                    <th>Phone</th>
                                                    <th>Message</th>
                                                    <th>Created At</th>
                                                </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
   
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script src="js/CasseroleService.js"></script>



<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide(); 
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ 
    UiTables.init(); 

    feedbacklist();
    });
</script>
<script type="text/javascript">
      
    function feedbacklist()
    {
        var reqc = {"id":"1"};
       $.ajax({
                url: 'service.php?servicename=getintouchlist',  
                type: 'POST',
                datatype: 'JSON',
                data: JSON.stringify(reqc),
                async: false,
                success: function(cul)
                {
                
                    var cr = JSON.parse(cul);

                    var ca = new Array();

                    for(var c=0;c<cr.feedback.length;c++)
                    {
                        ca[c] = new Array();

                        ca[c][0] = c+1;

                        ca[c][1] = cr.feedback[c].name	;

                        ca[c][2] = cr.feedback[c].email;

                        ca[c][3] = cr.feedback[c].phone;

                        ca[c][4] = cr.feedback[c].message;

                        ca[c][5] = cr.feedback[c].createdat;
                    }

                    var table = $('#tablecul').DataTable({
                        "aaData": ca,
                        "bDestroy": true,
                        "scrollX": true,
                        "autoWidth": true
                    }); 

                }

       });
    }

    </script>
