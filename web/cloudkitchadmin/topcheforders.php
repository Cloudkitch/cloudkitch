<?php 

session_start();

//print_r($_SESSION);

if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/Casseroleadmin/index.php");
    header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
// include 'inc/config.php'; $template['header_link'] = 'WELCOME ';
 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Validation Header -->
    <!-- <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Create Invitation Code</h1>
                </div>
            </div>
           
        </div>
    </div> -->

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <div class="block">
                <!-- Form Validation Title -->
                <div class="block-title">
                    <h2>Top Chefs</h2>
                </div>
                <!-- END Form Validation Title -->

                <!-- Form Validation Form -->
                
                <!--event_start_time,event_end_time,address,info-->
                <form id="topchefs" action="topchefsorders.php"  method="post" class="form-horizontal form-bordered">

                <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">

                <div class="form-group" id="ost">
                        <label class="col-md-3 control-label" for="val-mt">Select Top Chef Type<span class="text-danger">*</span></label>
                        <div class="col-md-6" >
                           <select id="val-typ" name="val-typ" class="form-control">
                               <option value="">Select Type</option>
                               <option value="DS">Dishes Sold</option>
                               <option value="ES">Earnings</option>
                           </select>
                        </div>
                </div>

                    <div class="form-group" id="year">
                        <label class="col-md-3 control-label" for="val-mt">Select Year<span class="text-danger">*</span></label>
                        <div class="col-md-6" >
                           <select id="val-yr" name="val-yr" class="form-control">
                               <option value="">Select Year</option>
                               <option value="2017">2017</option>
                               <option value="2018">2018</option>
                               <option value="2019">2019</option>
                               <option value="2020">2020</option>
                           </select>
                        </div>
                    </div>

                    

                    <div class="form-group" id="selectInvite">
                        <label class="col-md-3 control-label" for="val-mt">Select Month<span class="text-danger">*</span></label>
                        <div class="col-md-6" >
                           <select id="val-mt" name="val-mt" class="form-control">
                               <option value="">Select Month</option>
                               <option value="1">January</option>
                               <option value="2">February</option>
                               <option value="3">March</option>
                               <option value="4">April</option>
                               <option value="5">May</option>
                               <option value="6">June</option>
                               <option value="7">July</option>
                               <option value="8">August</option>
                               <option value="9">September</option>
                               <option value="10">October</option>
                               <option value="11">November</option>
                               <option value="12">December</option>
                            </select>
                        </div>
                    </div>

                                        
                    <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-3">
                            <!-- <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button> -->
                            <button type="button" onclick="ptworders();" class="btn btn-effect-ripple btn-primary">Submit</button>
                            <button type="button" onclick="reset();"  class="btn btn-danger">Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Form Validation Form -->

                
            </div>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->

    

    <div class="row" id="colds">

            <div class="col-sm-10 col-md-12 col-lg-12">
            
                <div class="block">

    
                <div class="block-title">
                    <h2>Dishes Sold</h2>
                    <button type="button" id="exptexcel" class="btn btn-rounded btn-success" style="float:right">Export</button>
                </div>
           

      
                <table id ="dsold" class="table table-vcenter table-striped table-hover table-borderless">

                                                <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Chef</th>
                                                 <th>Sold</th>
                                                 </tr> 
                                                </thead>  
                  
                </table>
            

                </div>

            </div>
    </div>
    <div class="row" id="coles">

            <div class="col-sm-10 col-md-12 col-lg-12">
            
                <div class="block">

    
                <div class="block-title">
                    <h2>Earnings</h2>
                    <button type="button" id="exptexceler" class="btn btn-rounded btn-success" style="float:right">Export</button>
                </div>
           

      
                <table id ="erng" class="table table-vcenter table-striped table-hover table-borderless">

                                                <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Chef</th>
                                                 <th>Earning</th>
                                                 </tr> 
                                                </thead>  
                  
                </table>
            

                </div>

            </div>
    </div>
</div>


 <?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function() {
    $("#loading").hide();
    $("#cht").hide();
    $("#colds").hide();
    $("#coles").hide();
   // chefdetail();

    function download_csv(csv, filename) {
        var csvFile;
        var downloadLink;
        // CSV FILE
        csvFile = new Blob([csv], {type: "text/csv"});
        // Download link
        downloadLink = document.createElement("a");
        // File name
        downloadLink.download = filename;
        // We have to create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);
        // Make sure that the link is not displayed
        downloadLink.style.display = "none";
        // Add the link to your DOM
        document.body.appendChild(downloadLink);
        // Lanzamos
        downloadLink.click();
    }

    function export_table_to_csv(html, filename) {
        var csv = [];
        var rows = document.querySelectorAll("table tr");
        
        for (var i = 0; i < rows.length; i++) {
            var row = [], cols = rows[i].querySelectorAll("td, th");
            
            for (var j = 0; j < cols.length; j++) 
                row.push(cols[j].innerText);        
            csv.push(row.join(","));       
        }
        // Download CSV
        download_csv(csv.join("\n"), filename);
    }

    document.querySelector("#exptexcel").addEventListener("click", function () {
    var html = document.querySelector("table").outerHTML;
    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    filename = 'MyTree_TopChefSold_' + postfix + '.csv';
    export_table_to_csv(html, filename);

 });
    document.querySelector("#exptexceler").addEventListener("click", function () {
    var html = document.querySelector("table").outerHTML;
    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    filename = 'MyTree_TopChefEarning_' + postfix + '.csv';
    export_table_to_csv(html, filename);
   });


});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

        $('#dsold').dataTable().fnClearTable();
        $('#dsold').dataTable().fnDraw();
        $('#dsold').dataTable().fnDestroy();
//erng

        $('#erng').dataTable().fnClearTable();
        $('#erng').dataTable().fnDraw();
        $('#erng').dataTable().fnDestroy();
       
      });</script>



<script type="text/javascript">

    function ptworders()
    {

        console.log("Hii");

        var mt = '';
         var yr = '';
         var t = '';


        mt = $("#val-mt").val();
        
        yr = $("#val-yr").val();

        t = $("#val-typ").val();

        if(mt == '' && yr == '' && t == '')
        {
            alert('Please select year,month and type');

            return false;
        }

        var ic =  $("#invitecode").val();

        
        var reqsu = {"month":mt,"invitecode":ic,"year":yr,"type":t};        
   

        $.ajax({
                 url: 'topchefs_service.php?servicename=TC-ORDERS',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(reqsu),
                 async: false,
                 success: function(rsch)
                 {
                     console.log("Json"+JSON.stringify(rsch));

                     var rsuch = JSON.parse(rsch);

                     if(rsuch.status == 'success')
                     {
                        var tp = $("#val-typ").val();

                        if(tp == 'DS')
                        {
                            $("#colds").show();

                             $("#coles").hide();
                        }
                        else if(tp == 'ES')
                        {
                            $("#coles").show();

                            $("#colds").hide();
                        }
                        else 
                        {
                            $("#colds").hide();
                            $("#coles").hide();
                        }    
                     

                        var co = new Array();

                                              

                        for(var ch=0;ch<rsuch.tc.length;ch++)
                        {

                            co[ch] = new Array();

                            co[ch][0] = ch+1;

                            co[ch][1] = rsuch.tc[ch].chefname;

                            co[ch][2] = rsuch.tc[ch].amt;
                 
                        }

                        if(tp == 'DS')
                        {
                                $('#dsold').dataTable({
                            "aaData": co,
                            "bDestroy": true,
                            "scrollX": true,
                            "autoWidth": true,
                            //"order": [[ 2, "asc" ]]
                            "aoColumns": [{ "orderSequence": [ "desc", "asc" ] },
                                { "orderSequence": [ "desc", "asc" ] },
                                { "orderSequence": [ "desc", "asc" ] }
                                
                            ]
                           });
                        }
                        else if(tp == 'ES')
                        {
                            $('#erng').dataTable({
                            "aaData": co,
                            "bDestroy": true,
                            "scrollX": true,
                            "autoWidth": true,
                            //"order": [[ 2, "asc" ]]
                            "aoColumns": [{ "orderSequence": [ "desc", "asc" ] },
                                { "orderSequence": [ "desc", "asc" ] },
                                { "orderSequence": [ "desc", "asc" ] }
                            ]
                           });
                        }

                        

                        
                            
                     }
                     else
                     {
                        $("#colds").hide();
                        $("#coles").hide();
                     }   
                 }    
        });
    }

    function reset()
    {
       location.reload();
    }

</script>





