<?php 

session_start();
include 'inc/databaseConfig.php';
include 'inc/config.php'; 
if(!isset($_SESSION['info']['user']))
{
	header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
include 'inc/template_start.php';
include 'inc/page_head.php';
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<style type="text/css">
    .working-days .custom-checkbox
    {
        display: inline-block;
        margin-left: 10px;
    }
</style>
<div id="page-content">
  <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
  <img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1>Add Corporate Meal Type</h1>
    </div>
</div>
</div>
</div>
<div class="row">
    <div class="col-sm-10 col-md-12 col-lg-12">
        <div class="block">
            <form id="createcorporatemealtype" name="createcorporatemealtype" class="form-horizontal form-bordered">

                <div class="form-group">
                    <label class="col-md-3 control-label" for="corporate">Corporate <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                    
                    <select class="form-control selectpicker" id="corporate" data-live-search="true"  name="val_corporate[]">
                            
                        <?php
                            $query = "SELECT * FROM corporate WHERE status = '1' ORDER BY id ASC";
                            $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                            while($row = mysqli_fetch_assoc($result))
                            {
                                echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
                            }   
                        ?> 
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="val_restaurant">Meal Type <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                    
                    <select class="form-control selectpicker" id="val_restaurant" data-live-search="true" multiple="multiple" name="val_restaurant[]">
                            <option value="personal_meals">Personal Meals</option>
                            <option value="team_meals">Team Meals</option>
                            <option value="event_meals">Event Meals</option>
                            <option value="cafeteria">Cafeteria</option> 

                        </select>
                    </div>
                </div>

                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-3">
                        <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<!-- END Page Content -->
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<!-- Load and execute javascript code used only in this page -->
<script src="js/CasseroleService.js"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
<!-- <script>
    $(function() { FormsValidation.init();
        invitecodedetail();
    });
</script> -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#loading").hide();
    });
    $("form[name='createcorporatemealtype']").validate({
        rules: {
            val_corporate : "required",
            val_restaurant : "required"
           
        },
        submitHandler: function(form) {
            $(this).find('button[type=submit]').prop('disabled', true);
            $(".preloader").show();
            var data = new FormData($('#createcorporatemealtype')[0]);
            $.ajax({
                url: 'service.php?servicename=addcorporatmealtype',
                type: 'POST',
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                data: data, 
                success: function(data)
                {
                   $(".preloader").hide();
                   var result = JSON.parse(data);
                   if(result.status == 'success')
                   {
                    window.location.href = 'corporatMealType.php';
                }
                else
                {
                    $("#toast-error").html(result.msg);
                    $("#toasterError").fadeIn();
                }
                setTimeout(function(){
                    $("#toasterError").fadeOut();
                }, 3000);
            }
        });
        }

    });


    // $('#val_restaurant').change(function(e) {
    //         var selected = $(this).val();
    //         //alert(selected);
    //         console.dir(selected);
    //         var reqcl = {"selected":selected};
    //         $.ajax({
    //             type: "POST",
    //             url : "service.php?servicename=corporatecuisinelist",
    //             datatype: "JSON",
    //             data: JSON.stringify(reqcl),
    //             success: function(data)
    //             {
    //                 $('#cuisinelistDiv').show();
    //                 var cl = '';

    //                 cl = JSON.parse(data);

    //                 var culi = new Array();
    //                 $grid ="";
    //                 for(var d=0;d<cl.cuisines.length;d++)
    //                 {
    //                 culi[d] = new Array();

    //                 culi[d][0] = d+1;
    //                 culi[d][1] = cl.cuisines[d].cuisinename;
    //                 culi[d][2] = '<input type="text" name="cuisineOffer">'; 
    //                 $grid = $grid.concat('<tr><td>'+cl.cuisines[d].cuisinename+'</td><td><input type="text" name="cuisineOffer"></td></tr>');
    //             }  

    //             // $('#tablecuisineslist').dataTable({
    //             //     "aaData": culi,
    //             //     "bDestroy": true
    //             // });
                
    //             $('#tablecuisineslist').html($grid);

    //             }
    //             });

    //     });
</script>
<?php include 'inc/template_end.php'; ?>

