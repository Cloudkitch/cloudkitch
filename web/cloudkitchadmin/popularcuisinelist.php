<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: https://coludkitch.co.in/cloudkitchadmin/index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <!-- <a href="#myModal" class="btn btn-lg btn-primary" data-toggle="modal">Launch
      Demo Modal</a> -->
    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Popular Cuisine images</h4>
          </div>
          <div class="modal-body">

          <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tablecuisinepopimg" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                    <th>Sr</th>
                                                    <th>Images</th>
                                                 </tr> 
                                            </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>

           

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>
        </div>
      </div>
    </div>

    
  </div>

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Popular Cuisines</h1>
                </div>
            </div>
           
        </div>
    </div>

    <div class="row" style="float:right;margin-bottom: 3px;">
            
            <a href="addCuisine.php"><button class="btn btn-rounded btn-primary"><i class="fa fa-plus"></i> Add Popular Cuisine</button></a>

        </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tablepopcimgList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Cuisine Name</th>
                                                 <th>View Cuisine Images</th>
                                                 <!-- <th>Cuisine Pic</th>
                                                 <th>Description</th>
                                                 <th>Price</th>
                                                 <th>Start Date</th>
                                                 <th>End Date</th>
                                                 <th>Pikup Time</th>
                                                 <th>More Details</th> -->
                                                 <!-- <th></th> -->
                                                
                                                </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script src="js/CasseroleService.js"></script>
<!-- <script src="js/cuisine.js"></script>
 -->


<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    // var type = '<?php 
    //echo $_SESSION['info']['type']; ?>';
    // var wedcode = '';
    // wedcode = '2616';
    // console.log("My Type Is :"+type);
    // if(type == "admin")
    // {
    //     getWeddingList();
    // }

    //invitationlist();
   // customerlist();
    //cuisinelist();
    popularcuisinelist();
    // if(type == "subadmin") 
    // {
    //    getWeddingListBySubadmin(wedcode); 
    // }   
    });</script>