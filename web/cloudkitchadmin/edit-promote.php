<?php 
session_start();

include 'inc/databaseConfig.php';
//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/Casseroleadmin/index.php");
	header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Add Promotional  Banner</h1>
                    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <form id="createcuisines11" name='createcuisines11' action="" class="form-horizontal form-bordered">
     <div class="row">
     <input type="hidden" id="id" name="id">
     <div class="form-group">
                    <label class="col-md-3 control-label" for="example-select">Promoted By</label>
                    <div class="col-md-6">
                    <select id="kitchen1" name="kitchen1" class="form-control" size="1">
                    <?php
                     $query = "SELECT * FROM user WHERE ischef='1' and status='1' ORDER BY userid ASC";
                    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                    echo '<option value="">Select kitchen</option>';
                    while($row = mysqli_fetch_assoc($result))
                    {
                        echo '<option value="'.$row['userid'].'">'.$row['name'].'</option>';
                    }   
                    ?>
                    <option value="0">Home</option>
                    </select>
                    </div>
                </div>      
     <div class="form-group">
                    <label class="col-md-3 control-label" for="example-select">Promoted To</label>
                    <div class="col-md-6">
                    <select id="kitchen2" name="kitchen2" class="form-control" size="1">
                    <?php
                     $query = "SELECT * FROM user WHERE ischef='1' and status='1'  ORDER BY userid ASC";
                    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                    echo '<option value="">Select kitchen</option>';
                    while($row = mysqli_fetch_assoc($result))
                    {
                        echo '<option value="'.$row['userid'].'">'.$row['name'].'</option>';
                    }   
                    ?>
                    <option value="0">Home</option>
                    </select>
                    </div>
                </div>
                
     <div class="form-group">
                <label class="col-md-3 control-label" for="image">Cuisines Images<span class="text-danger"></span></label>
                <div class="col-md-6">
                    <input type="hidden" id="base64" name="base64">
                    <input type="file" id="image" name="image" onchange="previewImage(this)" multiple>
                    <div id="cuisinesImg"></div>
                </div>
    </div>

  
    <div class="form-group form-actions">
                <div class="col-md-8 col-md-offset-3">
                    <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button>
                    <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                </div>
            </div>
   
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
</form>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script src="js/CasseroleService.js"></script>



<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
    var id = "<?=$_GET['id']?>";
            var request = {"id":id};
            $.ajax({
                url: 'service.php?servicename=edit-promote',
                type: 'POST',
                data: JSON.stringify(request),
                contentType: 'application/json; charset=utf-8',
                datatype: 'JSON',
                async: true,
                success: function(data)
                {
                   var result = JSON.parse(data);
                   $("#id").val(result[0]['id']); 
                   $("#kitchen1").val(result[0]['promoted_by']); 
                   $("#kitchen2").val(result[0]['promoted_to']); 
                //    $("#img").val(result[0]['password']); 
                $("#cuisinesImg").append('<div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;padding-left:5px;" onclick = "imageClose(this)">X</div><img  src="'+result[0]['img']+'" style="height:100px;margin-right:5px;margin-top:5px;" class="img-thumbnail cuisine-img"></div>');

               }
           });

 
});

function previewImage(input) {
            if (input.files) {
                var filesAmount = input.files.length;
                for (var i = 0; i < filesAmount; i++)   {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        var src = event.target.result;
                        $("#cuisinesImg").append('<div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;padding-left:5px;" onclick = "imageClose(this)">X</div><img  src="'+src+'" style="height:100px;margin-right:5px;margin-top:5px;" class="img-thumbnail cuisine-img"></div>');
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }
        function imageClose(a)
        {
            $(a).parent().remove();
        }
        $("form[name='createcuisines11']").validate({
        rules: {
            kitchen1:"required",
            kitchen2:"required",
          
        },
        submitHandler: function(form) {
            $(this).find('button[type=submit]').prop('disabled', true);
            $(".preloader").show();
            var images = "";
            $( ".cuisine-img" ).each(function() {
                var image = $(this).attr("src");
                images += "###"+image;
            });
            $("#base64").val(images);

            var kitchen1_to = $( "#kitchen1 option:selected" ).text();
            var kitchen2_by = $( "#kitchen2 option:selected" ).text();
            var data = new FormData($('#createcuisines11')[0]);
            $.ajax({
                url: 'service.php?servicename=updatepromote',
                type: 'POST',
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                data: data, 
                success: function(data)
                {
                     $(".preloader").hide();
                     var result = JSON.parse(data);
                     if(result.status == 'success')
                     {
                        window.location.href = 'promotelist.php';
                     }
                     else
                     {
                        $("#toast-error").html(result.msg);
                        $("#toasterError").fadeIn();
                     }
                      setTimeout(function(){
                        $("#toasterError").fadeOut();
                  }, 3000);
                }
            });
        }
    });
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

     
    });</script>
