<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
//include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
if($_SESSION['info']['isAdmin'] == 1)
    {
       include 'inc/configAdmin.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

    }
    else
    {
       include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
    }

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <!-- <a href="#myModal" class="btn btn-lg btn-primary" data-toggle="modal">Launch
      Demo Modal</a> -->
    <!-- Modal HTML -->
    <div id="myModalcuisines" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Cuisine Detail</h4>
          </div>
          <div class="modal-body">

          <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tablecuList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                
                                                 <th>Cuisine Name</th>
                                                 <th>Sold Qty</th>
                                                 <th>Paid Date</th>
                                                                                                
                                                                                              
                                                </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>

           

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>
        </div>
      </div>
    </div>

    
  </div>

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Vendor Payout History</h1>
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">

    <!-- <div class="form-group">

<div class="col-md-6">
<label class="col-md-3 control-label" for="example-daterange1">Pickup Date :</label>
<div class="input-group input-daterange" data-date-format="yyyy/mm/dd">
<input type="text" id="frmdt" name="frmdt" class="form-control" placeholder="From">
<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
<input type="text" id="todt" name="todt" class="form-control" placeholder="To">


</div>

<div style="display: flex;">

<div class="input-group bootstrap-timepicker" style="width: 34%;
    margin-left: 132px;"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a href="#" data-action="incrementHour"><i class="fa fa-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><i class="fa fa-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementSecond"><i class="fa fa-chevron-up"></i></a></td></tr><tr><td><input type="text" class="form-control bootstrap-timepicker-hour" maxlength="2"></td> <td class="separator">:</td><td><input type="text" class="form-control bootstrap-timepicker-minute" maxlength="2"></td> <td class="separator">:</td><td><input type="text" class="form-control bootstrap-timepicker-second" maxlength="2"></td></tr><tr><td><a href="#" data-action="decrementHour"><i class="fa fa-chevron-down"></i></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><i class="fa fa-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="decrementSecond"><i class="fa fa-chevron-down"></i></a></td></tr></tbody></table></div>
<input type="text" id="frmtm" name="frmtm" placeholder="From Time" class="form-control input-timepicker24">
<span class="input-group-btn">
<a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary" style="overflow: hidden; position: relative;"><span class="btn-ripple animate" style="height: 38px; width: 38px; top: 3px; left: 12.625px;"></span><i class="fa fa-clock-o"></i></a>
</span>
</div>

<div class="input-group bootstrap-timepicker" style="width: 34%;
    margin-left: 39px;"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a href="#" data-action="incrementHour"><i class="fa fa-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><i class="fa fa-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementSecond"><i class="fa fa-chevron-up"></i></a></td></tr><tr><td><input type="text" class="form-control bootstrap-timepicker-hour" maxlength="2"></td> <td class="separator">:</td><td><input type="text" class="form-control bootstrap-timepicker-minute" maxlength="2"></td> <td class="separator">:</td><td><input type="text" class="form-control bootstrap-timepicker-second" maxlength="2"></td></tr><tr><td><a href="#" data-action="decrementHour"><i class="fa fa-chevron-down"></i></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><i class="fa fa-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="decrementSecond"><i class="fa fa-chevron-down"></i></a></td></tr></tbody></table></div>
<input type="text" id="totm" name="totm" placeholder="To Time" class="form-control input-timepicker24">
<span class="input-group-btn">
<a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary" style="overflow: hidden; position: relative;"><span class="btn-ripple animate" style="height: 38px; width: 38px; top: 3px; left: 12.625px;"></span><i class="fa fa-clock-o"></i></a>
</span>
</div>
  

</div>

</div>
<div class="col-md-3" >
   <button type="button" onclick="searchbypickup();" class="btn btn-effect-ripple btn-info" style="overflow: hidden; position: relative;">Find</button>
   <button type="button" onclick="reset();" class="btn btn-effect-ripple btn-success" style="overflow: hidden; position: relative;">Reset</button> 
</div>

</div> -->

<!-- <br><br> -->
    <!-- <button type="button" onclick="exporttoexcel()" class="btn btn-rounded btn-success" style="float:right">Export</button> -->
    <button type="button" id="exptexcel" class="btn btn-rounded btn-success" style="float:right">Export</button>
    <br><br>
        <div class="col-sm-10 col-md-12 col-lg-12">
        <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
            <!-- Form Validation Block -->
            <table id ="tableVendorphList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Vendor Name</th>
                                                 <th>Items</th>
                                                 <!-- <th>Payment Breakdown</th> -->
                                                 <th>Payment Mode</th>
                                                 <th>Payment Date</th>
                                                 <th>Total</th>
                                                 <th>Status</th>
                                                 </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->



<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script src="js/CasseroleService.js"></script>

<!-- <script src="js/order.js"></script> -->


<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    $('#tableChefOrderList').dataTable().fnClearTable();
        $('#tableChefOrderList').dataTable().fnDraw();
        $('#tableChefOrderList').dataTable().fnDestroy();

        // $('#tableOrderItemList').dataTable().fnClearTable();
        // $('#tableOrderItemList').dataTable().fnDraw();
        // $('#tableOrderItemList').dataTable().fnDestroy();

        
    // var type = '<?php 
    //echo $_SESSION['info']['type']; ?>';
    // var wedcode = '';
    // wedcode = '2616';
    // console.log("My Type Is :"+type);
    // if(type == "admin")
    // {
    //     getWeddingList();
    // }

    //invitationlist();
    //customerlist();
    chefpayhistory();
    // if(type == "subadmin") 
    // {
    //    getWeddingListBySubadmin(wedcode); 
    // }   
    });</script>

    <script type="text/javascript">
        
    function chefpayhistory()
    {
       var invitecode = '';

       invitecode = $("#invitecode").val();

       var reqhc = {"invitecode":invitecode};

        $("#loading").show();
        $.ajax({
                 url: 'vendorpayouthistoryservice.php?service_name=paidvendors',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(reqhc),
                 async: false,
                 success: function (col)
                 {
                    $("#loading").hide();
                     console.log("Vendororders Response"+JSON.stringify(col));

                     var rscol = JSON.parse(col);

                     var co = new Array();

                     for(var oc=0;oc<rscol.vendors.length;oc++)
                     {
                        co[oc] = new Array();

                        co[oc][0] = oc+1;

                        co[oc][1] = rscol.vendors[oc].vendorname;

                        co[oc][2] = '<a href="#myModalcuisines" onclick="opencuisinedetail('+rscol.vendors[oc].chefid+',\''+rscol.vendors[oc].itemid+'\');" data-toggle="modal">'+rscol.vendors[oc].cuisines+'</a>';

                                                if(rscol.vendors[oc].pmtmode == 'CASH')
                         {
                            co[oc][3] = '<button style="pointer-events: none;" type="button" class="btn btn-rounded btn-success">'+rscol.vendors[oc].pmtmode+'</button>';
                         }
                         else if(rscol.vendors[oc].pmtmode == 'MTP')
                         {
                            co[oc][3] = '<button style="pointer-events: none;" type="button" class="btn btn-rounded btn-danger">'+rscol.vendors[oc].pmtmode+'</button>';
                         }   

                        co[oc][4] = rscol.vendors[oc].pmtdate;

                        co[oc][5] = rscol.vendors[oc].total;

                        if(rscol.vendors[oc].paidstatus == 'DONE')
                        {
                           co[oc][6] = '<button style="pointer-events: none;" type="button" class="btn btn-square btn-primary">'+rscol.vendors[oc].paidstatus+'</button>'; 
                        }
                        else if(rscol.vendors[oc].paidstatus == 'PENDING')
                        {
                            co[oc][6] = '<button id="'+rscol.vendors[oc].vnpid+'" onclick="vsetpaidstatus(\''+rscol.vendors[oc].vnpid+'\')" type="button" class="btn btn-square btn-danger">'+rscol.vendors[oc].paidstatus+'</button>';
                        }    

                        // if(rscol.cord[oc].chefaddress != '')
                        // {
                        //     co[oc][4] = rscol.cord[oc].chefaddress;
                        // }
                        // else
                        // {
                        //     co[oc][4] = rscol.cord[oc].cheffaltno + ' - ' + rscol.cord[oc].chefbuilding;
                        // }

                        // co[oc][5] = rscol.cord[oc].customer;

                        // co[oc][6] = rscol.cord[oc].customerphone;

                        // if(rscol.cord[oc].customeraddress != '')
                        // {
                        //      co[oc][7] = rscol.cord[oc].customeraddress;
                        // }
                        // else
                        // {
                        //      co[oc][7] = rscol.cord[oc].customerflatno + ' - ' + rscol.cord[oc].customerbuilding;
                        // }

                        // co[oc][8] = rscol.cord[oc].cuisinename;

                        // co[oc][9] = rscol.cord[oc].cuisinetype;

                        // co[oc][10] = rscol.cord[oc].quantity;

                        // co[oc][11] =  rscol.cord[oc].price;

                        // co[oc][12] = rscol.cord[oc].total;

                        // co[oc][13] = rscol.cord[oc].paymenttype; 
                        
                        // co[oc][14] = rscol.cord[oc].pickupdate;

                        // co[oc][15] = 'NA';                                                                                                      
                    }

                    $('#tableVendorphList').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true
                //         dom: '<"html5buttons"B>lTfgitp',
                // buttons: [
                //     { extend: 'copy'},
                //     {extend: 'csv'},
                //     {extend: 'excel', title: 'ExampleFile'},
                //     {extend: 'pdf', title: 'ExampleFile'},

                //     {extend: 'print',
                //      customize: function (win){
                //             $(win.document.body).addClass('white-bg');
                //             $(win.document.body).css('font-size', '10px');

                //             $(win.document.body).find('table')
                //                     .addClass('compact')
                //                     .css('font-size', 'inherit');
                //         }
                //     }
                // ]

                    });   
                 }    
        });
    }

    </script>

    <script type="text/javascript">
      
    function opencuisinedetail(cfid,itmid)
    {
        console.log("Cfid"+cfid+"Itmid"+itmid);

        var reqcd =  {"itemid":itmid,"chefid":cfid};

        $.ajax({
                 url: 'chefpayouthistoryService.php?service_name=cuisinedetail',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(reqcd),
                 async: false,
                 success: function(rcd)
                 {
                   console.log("Rs"+JSON.stringify(rcd));

                   var pimg = JSON.parse(rcd);

                  var pm = new Array();

                  for(var po=0;po<pimg.cu.length;po++)
                  {
                     pm[po] = new Array();

                     pm[po][0] = po+1;

                   // pm[po][1] = pimg.cu[po].cassitemid;

                     pm[po][1] = pimg.cu[po].cuisinename;

                     pm[po][2] = pimg.cu[po].soldqty;

                     pm[po][3] = pimg.cu[po].date;
                  }

                   $('#tablecuList').dataTable({
                        "aaData": pm,
                        "bDestroy": true
                    });
                 }



        });
    }

    </script>

    <script type="text/javascript">
        
        function exporttoexcel()
        {
           // alert("Hii");
        //     $('#tableChefOrderList').tableExport({type:'excel',escape:'false'});

        //     var dt = new Date();
        // var day = dt.getDate();
        // var month = dt.getMonth() + 1;
        // var year = dt.getFullYear();
        // var hour = dt.getHours();
        // var mins = dt.getMinutes();
        // var postfix = day + "." + month + "." + year + "_" + hour;

        //     var a = document.createElement('a');
        // //getting data from our div that contains the HTML table
        // var data_type = 'data:application/vnd.ms-excel';
        // var table_div = document.getElementById('tableChefOrderList');
        // var table_html = table_div.outerHTML.replace(/ /g, '%20');
        // a.href = data_type + ', ' + table_html;
        // //setting the file name
        // a.download = 'Mytree_' + postfix + '.xls';
        // //triggering the function
        // a.click();
        // //just in case, prevent default behaviour
        // e.preventDefault();
        }
    </script>

    <script type="text/javascript">
        
        $(document).ready(function() {

          $("#frmtm").val('');
          $("#totm").val('');
  $("#exptexcel").click(function(e) {
    e.preventDefault();

    //getting data from our table
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('tableChefphList');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');

    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
         var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    a.download = 'MyTree_' + postfix + '.xls';
    a.click();
  });
});


    </script>

    <script type="text/javascript">
        
    function searchbypickup()
    {
        $("#loading").show();
        var frmdate = '';
        var todate = '';
        var totm = '';
        var frmtm = '';
        var ftm = '';
        var fttm = '';

        frmdate = $("#frmdt").val();
        todate = $("#todt").val();
        
        frmtm = $("#frmtm").val();

        if(frmtm != '')
        {
            frmtm = frmtm.split(":");
             ftm = frmtm[0]+':'+frmtm[1];  
        }
        else
        {
           ftm = ''; 
        }  

        totm = $("#totm").val();

        if(totm != '')
        {
            totm = totm.split(":");
            fttm = totm[0]+':'+totm[1];  
        }
        else
        {
          fttm = '';
        }  
        

        // if(typeof ftm === 'undefined')
        // {
        //    ftm = '';
        // }

        // if(typeof fttm === 'undefined')
        // {
        //   fttm = '';
        // }  

        console.log("From Date"+frmdate+"to date"+todate+"From Time"+ftm+"To Time"+fttm);

        var reqpdt = {"fromdate":frmdate,"todate":todate,"fromtime":ftm,"totime":fttm};

        $.ajax({
                  url: 'service.php?servicename=SearchByPickupDate',
                  type: 'POST',
                  datatype: 'JSON',
                  contentType: 'application/json',
                  data: JSON.stringify(reqpdt),
                  async: false,
                  success: function(rspd)
                  {
                     console.log("search response"+JSON.stringify(rspd));

                     $('#tableChefOrderList').dataTable().fnClearTable();
                    $('#tableChefOrderList').dataTable().fnDraw();
                    $('#tableChefOrderList').dataTable().fnDestroy();

                    $("#loading").hide();
                     //console.log("Cheforders Response"+JSON.stringify(col));

                     var rscol = JSON.parse(rspd);

                     var co = new Array();

                     for(var oc=0;oc<rscol.cord.length;oc++)
                     {
                        co[oc] = new Array();

                        co[oc][0] = oc+1;

                        co[oc][1] = rscol.cord[oc].orderid;

                        co[oc][2] = rscol.cord[oc].chefname;

                        co[oc][3] = rscol.cord[oc].chefphone;

                        if(rscol.cord[oc].chefaddress != '')
                        {
                            co[oc][4] = rscol.cord[oc].chefaddress;
                        }
                        else
                        {
                            co[oc][4] = rscol.cord[oc].cheffaltno + ' - ' + rscol.cord[oc].chefbuilding;
                        }

                        co[oc][5] = rscol.cord[oc].customer;

                        co[oc][6] = rscol.cord[oc].customerphone;

                        if(rscol.cord[oc].customeraddress != '')
                        {
                             co[oc][7] = rscol.cord[oc].customeraddress;
                        }
                        else
                        {
                             co[oc][7] = rscol.cord[oc].customerflatno + ' - ' + rscol.cord[oc].customerbuilding;
                        }

                        co[oc][8] = rscol.cord[oc].cuisinename;

                        co[oc][9] = rscol.cord[oc].cuisinetype;

                        co[oc][10] = rscol.cord[oc].quantity;

                        co[oc][11] =  rscol.cord[oc].price;

                        co[oc][12] = rscol.cord[oc].total;

                        co[oc][13] = rscol.cord[oc].paymenttype; 
                        
                        co[oc][14] = rscol.cord[oc].pickupdate;

                        co[oc][15] = 'NA';                                                                                                      
                    }

                    $('#tableChefOrderList').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true
                

                    }); 
                  }    
        });
    }

    </script>

    <script type="text/javascript">
        
    function reset()
    {
        $("#frmdt").val('');

        $("#todt").val('');

        cheforderlist();

   }

    </script>

    <script type="text/javascript">
        
        function vsetpaidstatus(vpid)
        {


            console.log("Vendorpid"+vpid);

            var reqcf = {"Vendorpid":vpid};

            $.ajax({
                     url:'vendorpayouthistoryservice.php?service_name=vclosepayout',
                     type: 'POST',
                     datatype: 'JSON',
                     data: JSON.stringify(reqcf),
                     async:false,
                     success: function(rscf)
                     {
                        console.log("VP"+JSON.stringify(rscf));

                        var rcp = JSON.parse(rscf);

                        if(rcp.status == 'success')
                        {
                            location.reload();
                        }
                        else
                        {
                            alert('Failed to update');
                        }    


                     }
            })
        }

    </script>