<?php

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['servicename'] == 'UserList')
{
	$ul = array();

	$queul = "SELECT u.userid,u.name,ivd.groupname,ivd.invitationcode FROM user as u,invitationdetail as ivd,invitationusermap as ium WHERE ivd.id=ium.invitationid and ium.userid=u.userid";

	$excul = mysqli_query($conn,$queul) or die(mysqli_error($conn));

	if(mysqli_num_rows($excul) > 0)
	{
		$ul['users'] = array();

		while($rowul = mysqli_fetch_assoc($excul))
		{
			$u = array();

			$u['userid'] = $rowul['userid'];

			$u['name'] = $rowul['name'];

			$u['groupname'] = $rowul['groupname'];

			$u['invitecode'] = $rowul['invitationcode'];

			array_push($ul['users'], $u);
		}

		$ul['status'] = 'success';
		$ul['msg'] = 'Data available';	
	}
	else
	{
		$ul['status'] = 'failure';
		$ul['msg'] = 'Data not available';
	}

	print_r(json_encode($ul));
	exit;	
}

if($_GET['servicename'] == 'Chapters')
{
	$quechp = "SELECT groupname,invitationcode FROM invitationdetail";
	$excchp = mysqli_query($conn,$quechp) or die(mysqli_error($conn));

	$chp = array();

	if(mysqli_num_rows($excchp) > 0)
	{
		$chp['chapters'] = array();

		while ($rowchp = mysqli_fetch_assoc($excchp)) {
			
			$cp = array();

			$cp['groupname'] = $rowchp['groupname'];

			$cp['invitecode'] = $rowchp['invitationcode'];

			array_push($chp['chapters'], $cp);
		}

		$chp['status'] = 'success';
		$chp['msg'] = 'Data available';
	}
	else
	{
		$chp['status'] = 'failure';
		$chp['msg'] =  'Data not available';
	}

	print_r(json_encode($chp));
	exit;	
}

if($_GET['servicename'] == 'AssignChapterChampion')
{
	$reqacc = file_get_contents('php://input');

	$resacc = json_decode($reqacc,true);

	$userid = '';

	$ivc =  '';

	$email = '';

	$pwd = '';

	$userid = $resacc['userid'];

	$ivc = $resacc['invitecode'];

	$email = $resacc['email'];

	$pwd = $resacc['pwd'];

	$acc = array();

	$queun = "SELECT name FROM user WHERE userid='".$userid."'";
	$excun = mysqli_query($conn,$queun) or die(mysqli_error($conn));
	$rsun = mysqli_fetch_assoc($excun);


	$quead = "SELECT * FROM admindetail WHERE invitecode='".$ivc."'";
	$excad = mysqli_query($conn,$quead) or die(mysqli_error($conn));

	if(mysqli_num_rows($excad) > 0)
	{
		$updad = "UPDATE admindetail SET email='".$email."',password='".$pwd."',active='Yes',name='Admin',chefid='".$userid."' WHERE invitecode='".$ivc."'";
		$excad = mysqli_query($conn,$updad) or die(mysqli_error($conn));

		if($excad)
		{
			$acc['status'] = 'success';
			$acc['msg'] = 'Updated chapter champion details';
		}	
		else
		{
			$acc['status'] = 'failure';
			$acc['msg'] = 'failed to assign chapter champion';
		}	
	}
	else
	{
		$insad = "INSERT INTO admindetail SET email='".$email."',password='".$pwd."',active='Yes',name='Admin',invitecode='".$ivc."',chefid='".$userid."'";
		$exciad = mysqli_query($conn,$insad) or die(mysqli_error($conn));

		if($exciad)
		{
			$acc['status'] = 'success';
			$acc['msg'] = 'Assigned successfully chapter champion';
		}
		else
		{
			$acc['status'] = 'failure';
			$acc['msg'] = 'failed to assign chapter champion';
		}	
	}

	print_r(json_encode($acc));
	exit;	

}

if($_GET['servicename'] == 'Chapter-Champions')
{
	$quecc = "SELECT * FROM admindetail";
	$excacc = mysqli_query($conn,$quecc) or die(mysqli_error($conn));

	$cc = array();

	if(mysqli_num_rows($excacc) > 0)
	{
		$cc['chapchm'] = array();

		while ($rowcc = mysqli_fetch_assoc($excacc)) {
			
			$cm = array();

			$cm['id'] = $rowcc['chefid'];

			$cm['invitecode'] = $rowcc['invitecode'];

			$quechpnm =  "SELECT groupname FROM invitationdetail WHERE invitationcode='".$rowcc['invitecode']."'";
			$excchpnm = mysqli_query($conn,$quechpnm) or die(mysqli_error($conn));
			$rschpnm = mysqli_fetch_assoc($excchpnm);

			$cm['chaptername'] = $rschpnm['groupname'];

			$cm['email'] = $rowcc['email'];

			$cm['password'] = $rowcc['password'];

			$queud = "SELECT name FROM user WHERE userid='".$rowcc['chefid']."'";
			$excud = mysqli_query($conn,$queud) or die(mysqli_error($conn));
			$rsud = mysqli_fetch_assoc($excud);

			$cm['username'] = $rsud['name'];

			array_push($cc['chapchm'], $cm);

		}

		$cc['status'] = 'success';
		$cc['msg'] = 'Chapter champions available';
	}	
	else
	{
		$cc['status'] = 'failure';
		$cc['msg'] = 'Not available';
	}

	print_r(json_encode($cc));
	exit;	
}


?>