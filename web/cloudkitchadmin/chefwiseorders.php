<style>
    .blink{
	    animation:blink 0.8s infinite;
        background: linear-gradient(to right, #ffb800 0%, #ffb800 13.05%, #ffb200 26.06%, #f9941d 54.28%, #ee5e56 100%);
        font-size:14px;
        padding: 5px 10px;
        border-radius:5px;
    }
    @keyframes blink{
        0%{color: #000;}
        50%{color: transparent;}
        100%{color: #000;}
    }
  

</style>

<?php 
session_start();
include 'inc/config.php'; 
include 'inc/databaseConfig.php';
if(!isset($_SESSION['info']['user']))
{
    header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content"> 

  <div id="loading" style="position:fixed;left: 50%;top: 50%;">
  <img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<!-- Validation Header -->
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <!-- <h1>Orders <span id="neworder" style="display:none;" class="blink">New Order!</span></h1> -->
        <input type="hidden" name="invitecode" id="invitecode" value="<?php echo '1111'; ?>">
      </div>
    </div>
    <div class="col-sm-6">
     <div class="header-section">
      <a href="service.php?servicename=exportorder&usertype=0" style="width: 150px;float: right;" class="btn btn-block btn-primary" id="exportOrderA">
        <i class="fa fa-download">&nbsp;&nbsp;</i> Export Orders
      </a>
    </div>
  </div>
</div>
</div>
<!-- END Validation Header -->
<div class="row">
    <div class="col-sm-10 col-md-12 col-lg-12">
        <div class="block">
            <form id="ordercuisines" name="ordercuisines" class="form-horizontal form-bordered">

            <div class="form-group">
                <label class="col-md-3 control-label" for="val_usertype">User Type<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <select class="form-control" id="val_usertype" name="val_usertype">
                        <option value="0" selected>Normal User</option>
                        <option value="1">Corporate User</option>
                    </select>
                </div>
            </div>
            <div class="form-group" id="corporatediv" style="display: none">
                <label class="col-md-3 control-label" for="val_corporate">Corporate<span class="text-danger">*</span></label>
                <div class="col-md-6">
                
                    <select class="form-control" id="val_corporate" name="val_corporate">
                    <?php
                    $query = "SELECT * FROM corporate ORDER BY name ASC";
                    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                    echo '<option value="">Select Corporate</option><option value="All">All</option>';
                    while($row = mysqli_fetch_assoc($result))
                    {
                        echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
                    }   
                    ?>
                    </select>
                </div>
            </div>
          
           
        </form>
    </div>
</div>
<!-- Form Validation Content -->
<div class="row">
  <div class="col-sm-12 col-md-12 col-lg-12">
    <!-- Form Validation Block -->
    <table id ="tableChefOrderList" class="table table-vcenter table-striped table-hover table-borderless">
        <thead>
            <tr>
                <th>Sr</th>
                <th>Order Id</th>
                <th>Restaurant Name</th>
                <th>Restaurant Number</th>
                <th>Restaurant Address</th>
                <th>Customer Name</th>
                <th>Customer Number</th>
                <th>Customer Address</th>
                <th>Cuisine Name</th>
                <th>Selected addons</th>
                <th>Special Instruction</th>
                <th>Cuisine Type</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Total</th>
                <th>Payment Mode</th>
                <th>Order Placed Date</th>
                <th>Order Date</th>
                <th>Order Time</th>
                <th>Corporate User</th>
                <th>Corporate Name</th>
                <th>Action</th>
                <th>Order Action</th>
            </tr> 
        </thead>
    </table>
   <!-- END Form Validation Block -->
 </div>
</div>

</div>




<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script src="js/CasseroleService.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $("#loading").hide();

        // // Code to flash new order if any new order is placed
        //         $.ajax({
        //         url: 'service.php?servicename=isneworderupdate',
        //         type: 'POST',
        //         success: function(data)
        //         {
        //             var result = JSON.parse(data);
        //             var status = result.status;
        //             if(status == "success"){
        //                 //$("#neworder").show();
        //             }
        //             else{
        //                 //$("#neworder").hide();
        //             }
        //         }
            
        //         });

      
    }); 
</script>
<script src="js/pages/uiTables.js"></script>
<script>
    $(function(){ UiTables.init(); 

    $('#tableChefOrderList').dataTable().fnClearTable();
    $('#tableChefOrderList').dataTable().fnDraw();
    $('#tableChefOrderList').dataTable().fnDestroy();

        // $('#tableOrderItemList').dataTable().fnClearTable();
        // $('#tableOrderItemList').dataTable().fnDraw();
        // $('#tableOrderItemList').dataTable().fnDestroy();

        
    // var type = '?php 
    //echo $_SESSION['info']['type']; ?>';
    // var wedcode = '';
    // wedcode = '2616';
    // console.log("My Type Is :"+type);
    // if(type == "admin")
    // {
    //     getWeddingList();
    // }

    //invitationlist();
    //customerlist();
    cheforderlist();
    // if(type == "subadmin") 
    // {
    //    getWeddingListBySubadmin(wedcode); 
    // }   
    });
</script>

<script type="text/javascript">

    function cheforderlist(usertype,corporateid)
    {

        var table = $('#tableChefOrderList').DataTable();
        var kitchenid = '<?=$_SESSION['info']['id']?>';
        //clear datatable
        table.clear().draw();
      var invc = '';

      invc = $("#invitecode").val();  

      var cfo = {"invitecode":invc,
                "usertype":usertype,
                "corporateid":corporateid,
                "kitchenid":kitchenid
                };

      $.ajax({
         url: 'service.php?servicename=cheforders',
         type: 'POST',
         datatype: 'JSON',
         data: JSON.stringify(cfo),
         async: false,
         success: function (col)
         {
                     //console.log("Cheforders Response"+JSON.stringify(col));

                     var rscol = JSON.parse(col);
                     var co = new Array();
                     for(var oc=0;oc<rscol.cord.length;oc++)
                     {
                        var IsCorr = "No";
                        var s =  rscol.cord[oc].isCorporateUser;
                        if(s == 1){
                            IsCorr = "Yes";
                        }


                        var ss = rscol.cord[oc].ordertime;
                        if(ss == "00:00:00"){
                            ss = "-";
                        }

                        var ssa =  rscol.cord[oc].startdate;
                        if(ssa == "0000-00-00" || ssa == ""){
                            ssa = "-";
                        }

                        var ssb =  rscol.cord[oc].enddate;
                        if(ssb == "0000-00-00" || ssb == ""){
                            ssb = "-";
                        }

                        co[oc] = new Array();


                        co[oc][0] = oc+1;

                        co[oc][1] = rscol.cord[oc].orderid;

                        co[oc][2] = rscol.cord[oc].chefname;

                        co[oc][3] = rscol.cord[oc].chefphone;

                        if(rscol.cord[oc].chefaddress != '')
                        {
                            co[oc][4] = rscol.cord[oc].chefaddress;
                        }
                        else
                        {
                            co[oc][4] = rscol.cord[oc].cheffaltno + ' - ' + rscol.cord[oc].chefbuilding;
                        }

                        co[oc][5] = rscol.cord[oc].customer;

                        co[oc][6] = rscol.cord[oc].customerphone;

                        if(rscol.cord[oc].customeraddress != '')
                        {
                         co[oc][7] = rscol.cord[oc].delivery_address;
                        }
                        else
                        {
                            co[oc][7] = rscol.cord[oc].customerflatno + ' - ' + rscol.cord[oc].customerbuilding;
                        }

                        co[oc][8] = rscol.cord[oc].cuisinename;

                        co[oc][9] = rscol.cord[oc].selected_addons;

                        co[oc][10] = rscol.cord[oc].special_instructions;

                        co[oc][11] = rscol.cord[oc].cuisinetype;

                        co[oc][12] = rscol.cord[oc].quantity;

                        co[oc][13] =  rscol.cord[oc].price;

                        co[oc][14] = rscol.cord[oc].totalamount; 

                        co[oc][15] = rscol.cord[oc].paymenttype; 

                        co[oc][16] = rscol.cord[oc].orderdate;

                        co[oc][17] = ssa;
                        co[oc][18] = ss;
                        co[oc][19] = IsCorr;
                        co[oc][20] = rscol.cord[oc].corporatename;
                        co[oc][21] = '<a href="javascript:void(0);" onclick="MyWindow=window.open(\'printorder.php?itemid='+rscol.cord[oc].cassitemid+'\',\'MyWindow\',\'width=1000,height=790\'); return false;"" style="float: right;" class="btn btn-block btn-primary"><i class="fa fa-print">&nbsp;&nbsp;</i>Print</a>';
                        var btnenable = "";
                        if(rscol.cord[oc].orderstatus == 1){
                            co[oc][22] = '<p id="orderStatus-'+rscol.cord[oc].orderid+'">Accepted</p>';
                        }else if(rscol.cord[oc].orderstatus == 2){
                            co[oc][22] = '<p id="orderStatus-'+rscol.cord[oc].orderid+'">Rejected</p>';
                        }else if(rscol.cord[oc].orderstatus == 3){
                            co[oc][22] = '<p id="orderStatus-'+rscol.cord[oc].orderid+'">Dispatched</p>';
                        }else if(rscol.cord[oc].orderstatus == 4){
                            co[oc][22] = '<p id="orderStatus-'+rscol.cord[oc].orderid+'">Cancelled</p>';
                        }else{
                            co[oc][22] = '<p id="orderStatus-'+rscol.cord[oc].orderid+'">Pending</p>';
                        }
                                                           
                     }

                    $('#tableChefOrderList').dataTable({
                        "aaData": co,
                        "scrollX": true,
                        "bDestroy": true,
                        "autoWidth": true,
                        "pageLength": 25
                    });   
                }    
            });
    }

</script>

<script type="text/javascript">

    function exporttoexcel()
    {
           // alert("Hii");
        //     $('#tableChefOrderList').tableExport({type:'excel',escape:'false'});

        //     var dt = new Date();
        // var day = dt.getDate();
        // var month = dt.getMonth() + 1;
        // var year = dt.getFullYear();
        // var hour = dt.getHours();
        // var mins = dt.getMinutes();
        // var postfix = day + "." + month + "." + year + "_" + hour;

        //     var a = document.createElement('a');
        // //getting data from our div that contains the HTML table
        // var data_type = 'data:application/vnd.ms-excel';
        // var table_div = document.getElementById('tableChefOrderList');
        // var table_html = table_div.outerHTML.replace(/ /g, '%20');
        // a.href = data_type + ', ' + table_html;
        // //setting the file name
        // a.download = 'Mytree_' + postfix + '.xls';
        // //triggering the function
        // a.click();
        // //just in case, prevent default behaviour
        // e.preventDefault();
    }
</script>

<script type="text/javascript">

    $(document).ready(function() {
  // $("#exptexcel").click(function(e) {
  //   e.preventDefault();

  //   //getting data from our table
  //   var data_type = 'data:application/vnd.ms-excel';
  //   var table_div = document.getElementById('tableChefOrderList');
  //   var table_html = table_div.outerHTML.replace(/ /g, '%20');

  //   var a = document.createElement('a');
  //   a.href = data_type + ', ' + table_html;
  //        var dt = new Date();
  //       var day = dt.getDate();
  //       var month = dt.getMonth() + 1;
  //       var year = dt.getFullYear();
  //       var hour = dt.getHours();
  //       var mins = dt.getMinutes();
  //       var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
  //   a.download = 'MyTree_' + postfix + '.xls';
  //   a.click();
  // });

    function download_csv(csv, filename) {
        var csvFile;
        var downloadLink;
        // CSV FILE
        csvFile = new Blob([csv], {type: "text/csv"});
        // Download link
        downloadLink = document.createElement("a");
        // File name
        downloadLink.download = filename;
        // We have to create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);
        // Make sure that the link is not displayed
        downloadLink.style.display = "none";
        // Add the link to your DOM
        document.body.appendChild(downloadLink);
        // Lanzamos
        downloadLink.click();
    }

    function export_table_to_csv(html, filename) {
        var csv = [];
        var rows = document.querySelectorAll("#tableChefOrderList tr");
        
        for (var i = 0; i < rows.length; i++) {
            var row = [], cols = rows[i].querySelectorAll("td, th");
            
            for (var j = 0; j < cols.length; j++) 
                row.push(cols[j].innerText.replace(/,/g,' '));        
            csv.push(row.join(","));       
        }
        // Download CSV
        download_csv(csv.join("\n"), filename);
    }

    // document.querySelector("#exptexcel").addEventListener("click", function () {
    //     var html = document.querySelector("#tableChefOrderList").outerHTML;
    //     //var table_html = html.outerHTML.replace(/,/g, '%20');
    //     var dt = new Date();
    //     var day = dt.getDate();
    //     var month = dt.getMonth() + 1;
    //     var year = dt.getFullYear();
    //     var hour = dt.getHours();
    //     var mins = dt.getMinutes();
    //     var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    //     filename = 'MyTree_Orders' + postfix + '.csv';
    //     export_table_to_csv(html, filename);
    // });


});

$("#val_usertype").change(function(){
  var usertype = $(this).val();
  var corporateid = '0';
  var kitchenid = '<?=$_SESSION['info']['id']?>';
  if(usertype == '1'){
    $("#corporatediv").show();
  }else{
    $("#corporatediv").hide();
    $("#exportOrderA").attr("href", "service.php?servicename=exportorder&usertype=0&kitchenid="+kitchenid);
    cheforderlist(usertype,corporateid);
  }
});

$("#val_corporate").change(function(){
   var corporateid = $(this).val();
   var usertype = '1';
   var kitchenid = '<?=$_SESSION['info']['id']?>';
   $("#exportOrderA").attr("href", "service.php?servicename=exportorder&usertype=1&corporateid="+corporateid+"&kitchenid="+kitchenid);
   cheforderlist(usertype,corporateid);

}); 


</script>

<script type="text/javascript">

    function searchbypickup()
    {
        $("#loading").show();
        var frmdate = '';
        var todate = '';

        var totm = '';
        var frmtm = '';
        var ftm = '';
        var fttm = '';

        var invc = '';

        invc = $("#invitecode").val();

        frmdate = $("#frmdt").val();
        todate = $("#todt").val();

        // frmtm = $("#frmtm").val();

        // if(frmtm != '')
        // {
        //     frmtm = frmtm.split(":");
        //      ftm = frmtm[0]+':'+frmtm[1];  
        // }
        // else
        // {
        //    ftm = ''; 
        // }  

        // totm = $("#totm").val();

        // if(totm != '')
        // {
        //     totm = totm.split(":");
        //     fttm = totm[0]+':'+totm[1];  
        // }
        // else
        // {
        //   fttm = '';
        // }

         //console.log("From Date"+frmdate+"to date"+todate+"From Time"+ftm+"To Time"+fttm);

         var reqpdt = {"fromdate":frmdate,"todate":todate,"fromtime":ftm,"totime":fttm,"invitecode":invc};

         $.ajax({
          url: 'service.php?servicename=SearchByPickupDate',
          type: 'POST',
          datatype: 'JSON',
          contentType: 'application/json',
          data: JSON.stringify(reqpdt),
          async: false,
          success: function(rspd)
          {
             console.log("search response"+JSON.stringify(rspd));

             $('#tableChefOrderList').dataTable().fnClearTable();
             $('#tableChefOrderList').dataTable().fnDraw();
             $('#tableChefOrderList').dataTable().fnDestroy();

             $("#loading").hide();
                     //console.log("Cheforders Response"+JSON.stringify(col));

                     var rscol = JSON.parse(rspd);

                     var co = new Array();

                     for(var oc=0;oc<rscol.cord.length;oc++)
                     {
                        co[oc] = new Array();

                        co[oc][0] = oc+1;

                        co[oc][1] = rscol.cord[oc].orderid;

                        co[oc][2] = rscol.cord[oc].chefname;

                        co[oc][3] = rscol.cord[oc].chefphone;

                        if(rscol.cord[oc].chefaddress != '')
                        {
                            co[oc][4] = rscol.cord[oc].chefaddress;
                        }
                        else
                        {
                            co[oc][4] = rscol.cord[oc].cheffaltno + ' - ' + rscol.cord[oc].chefbuilding;
                        }

                        co[oc][5] = rscol.cord[oc].customer;

                        co[oc][6] = rscol.cord[oc].customerphone;

                        if(rscol.cord[oc].customeraddress != '')
                        {
                         co[oc][7] = rscol.cord[oc].customeraddress;
                     }
                     else
                     {
                         co[oc][7] = rscol.cord[oc].customerflatno + ' - ' + rscol.cord[oc].customerbuilding;
                     }

                     co[oc][8] = rscol.cord[oc].cuisinename;

                     co[oc][9] = rscol.cord[oc].cuisinetype;

                     co[oc][10] = rscol.cord[oc].quantity;

                     co[oc][11] =  rscol.cord[oc].price;

                     co[oc][12] = rscol.cord[oc].total;

                     co[oc][13] = rscol.cord[oc].paymenttype; 

                     co[oc][14] = rscol.cord[oc].pickupdate;

                     co[oc][15] = rscol.cord[oc].delivery; 

                     if(rscol.cord[oc].status == '')
                     {
                      co[oc][16] = "pending";
                  }
                  else
                  {
                   co[oc][16] = rscol.cord[oc].status;
               } 
               co[oc][17] = rscol.cord[oc].paymentbreakdown;

               co[oc][18] = rscol.cord[oc].aftertime;                                                                  
           }

           $('#tableChefOrderList').dataTable({
            "aaData": co,
            "scrollX": true,
            "bDestroy": true,
            "autoWidth": true


        }); 
       }    
   });
     }

 </script>

 <script type="text/javascript">

    function reset()
    {
        $("#frmdt").val('');

        $("#todt").val('');

        cheforderlist();

    }
</script>

<!-- <script>
    function AcceptOrder(){
        
    }

</script> -->