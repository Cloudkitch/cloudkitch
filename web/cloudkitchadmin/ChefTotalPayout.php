<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Chef Total Payment History</h1>
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">

    <!-- <div class="form-group">

<div class="col-md-6">
<label class="col-md-3 control-label" for="example-daterange1">Pickup Date :</label>
<div class="input-group input-daterange" data-date-format="yyyy/mm/dd">
<input type="text" id="frmdt" name="frmdt" class="form-control" placeholder="From">
<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
<input type="text" id="todt" name="todt" class="form-control" placeholder="To">


</div>

<div style="display: flex;">

<div class="input-group bootstrap-timepicker" style="width: 34%;
    margin-left: 132px;"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a href="#" data-action="incrementHour"><i class="fa fa-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><i class="fa fa-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementSecond"><i class="fa fa-chevron-up"></i></a></td></tr><tr><td><input type="text" class="form-control bootstrap-timepicker-hour" maxlength="2"></td> <td class="separator">:</td><td><input type="text" class="form-control bootstrap-timepicker-minute" maxlength="2"></td> <td class="separator">:</td><td><input type="text" class="form-control bootstrap-timepicker-second" maxlength="2"></td></tr><tr><td><a href="#" data-action="decrementHour"><i class="fa fa-chevron-down"></i></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><i class="fa fa-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="decrementSecond"><i class="fa fa-chevron-down"></i></a></td></tr></tbody></table></div>
<input type="text" id="frmtm" name="frmtm" placeholder="From Time" class="form-control input-timepicker24">
<span class="input-group-btn">
<a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary" style="overflow: hidden; position: relative;"><span class="btn-ripple animate" style="height: 38px; width: 38px; top: 3px; left: 12.625px;"></span><i class="fa fa-clock-o"></i></a>
</span>
</div>

<div class="input-group bootstrap-timepicker" style="width: 34%;
    margin-left: 39px;"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a href="#" data-action="incrementHour"><i class="fa fa-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><i class="fa fa-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementSecond"><i class="fa fa-chevron-up"></i></a></td></tr><tr><td><input type="text" class="form-control bootstrap-timepicker-hour" maxlength="2"></td> <td class="separator">:</td><td><input type="text" class="form-control bootstrap-timepicker-minute" maxlength="2"></td> <td class="separator">:</td><td><input type="text" class="form-control bootstrap-timepicker-second" maxlength="2"></td></tr><tr><td><a href="#" data-action="decrementHour"><i class="fa fa-chevron-down"></i></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><i class="fa fa-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="decrementSecond"><i class="fa fa-chevron-down"></i></a></td></tr></tbody></table></div>
<input type="text" id="totm" name="totm" placeholder="To Time" class="form-control input-timepicker24">
<span class="input-group-btn">
<a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary" style="overflow: hidden; position: relative;"><span class="btn-ripple animate" style="height: 38px; width: 38px; top: 3px; left: 12.625px;"></span><i class="fa fa-clock-o"></i></a>
</span>
</div>
  

</div>

</div>
<div class="col-md-3" >
   <button type="button" onclick="searchbypickup();" class="btn btn-effect-ripple btn-info" style="overflow: hidden; position: relative;">Find</button>
   <button type="button" onclick="reset();" class="btn btn-effect-ripple btn-success" style="overflow: hidden; position: relative;">Reset</button> 
</div>

</div> -->

<!-- <br><br> -->
    <!-- <button type="button" onclick="exporttoexcel()" class="btn btn-rounded btn-success" style="float:right">Export</button> -->
    <button type="button" id="exptexcel" class="btn btn-rounded btn-success" style="float:right">Export</button>
    <br><br>
        <div class="col-sm-10 col-md-12 col-lg-12">
        <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
            <!-- Form Validation Block -->
            <table id ="tablecheftotalpayout" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Chef Name</th>
                                                 <th>Total</th>
                                                 </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->



<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>


<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

        $('#tablecheftotalpayout').dataTable().fnClearTable();
        $('#tablecheftotalpayout').dataTable().fnDraw();
        $('#tablecheftotalpayout').dataTable().fnDestroy();

        // $('#tableOrderItemList').dataTable().fnClearTable();
        // $('#tableOrderItemList').dataTable().fnDraw();
        // $('#tableOrderItemList').dataTable().fnDestroy();

        
    // var type = '<?php 
    //echo $_SESSION['info']['type']; ?>';
    // var wedcode = '';
    // wedcode = '2616';
    // console.log("My Type Is :"+type);
    // if(type == "admin")
    // {
    //     getWeddingList();
    // }

    //invitationlist();
    //customerlist();
    cheftotalpayhistory();
    // if(type == "subadmin") 
    // {
    //    getWeddingListBySubadmin(wedcode); 
    // }   
    });</script>

    <script type="text/javascript">
        
    function cheftotalpayhistory()
    {
       var invitecode = '';

       invitecode = $("#invitecode").val();

       var reqhc = {"invitecode":invitecode};

        $("#loading").show();
        $.ajax({
                 url: 'cheftotalpay-service.php?service_name=cheftotalpayout',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(reqhc),
                 async: false,
                 success: function (col)
                 {
                    $("#loading").hide();
                     console.log("Cheforders Response"+JSON.stringify(col));

                     var rscol = JSON.parse(col);

                     var co = new Array();

                     for(var oc=0;oc<rscol.chefs.length;oc++)
                     {
                        co[oc] = new Array();

                        co[oc][0] = oc+1;

                        co[oc][1] = rscol.chefs[oc].chefname;

                        co[oc][2] = rscol.chefs[oc].total;
                                                                                         
                     }

                    $('#tablecheftotalpayout').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true
                       });   
                 }    
        });
    }

    </script>

     <script type="text/javascript">
        
        function exporttoexcel()
        {
           // alert("Hii");
        //     $('#tableChefOrderList').tableExport({type:'excel',escape:'false'});

        //     var dt = new Date();
        // var day = dt.getDate();
        // var month = dt.getMonth() + 1;
        // var year = dt.getFullYear();
        // var hour = dt.getHours();
        // var mins = dt.getMinutes();
        // var postfix = day + "." + month + "." + year + "_" + hour;

        //     var a = document.createElement('a');
        // //getting data from our div that contains the HTML table
        // var data_type = 'data:application/vnd.ms-excel';
        // var table_div = document.getElementById('tableChefOrderList');
        // var table_html = table_div.outerHTML.replace(/ /g, '%20');
        // a.href = data_type + ', ' + table_html;
        // //setting the file name
        // a.download = 'Mytree_' + postfix + '.xls';
        // //triggering the function
        // a.click();
        // //just in case, prevent default behaviour
        // e.preventDefault();
        }
    </script>

    <script type="text/javascript">
        
        $(document).ready(function() {

          $("#frmtm").val('');
          $("#totm").val('');
  // $("#exptexcel").click(function(e) {
  //   e.preventDefault();

  //   //getting data from our table
  //   var data_type = 'data:application/vnd.ms-excel';
  //   var table_div = document.getElementById('tableChefphList');
  //   var table_html = table_div.outerHTML.replace(/ /g, '%20');

  //   var a = document.createElement('a');
  //   a.href = data_type + ', ' + table_html;
  //        var dt = new Date();
  //       var day = dt.getDate();
  //       var month = dt.getMonth() + 1;
  //       var year = dt.getFullYear();
  //       var hour = dt.getHours();
  //       var mins = dt.getMinutes();
  //       var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
  //   a.download = 'MyTree_' + postfix + '.xls';
  //   a.click();
  // });

  function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;
    // CSV FILE
    csvFile = new Blob([csv], {type: "text/csv"});
    // Download link
    downloadLink = document.createElement("a");
    // File name
    downloadLink.download = filename;
    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);
    // Make sure that the link is not displayed
    downloadLink.style.display = "none";
    // Add the link to your DOM
    document.body.appendChild(downloadLink);
    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("table tr");
    
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText);        
        csv.push(row.join(","));       
    }
    // Download CSV
    download_csv(csv.join("\n"), filename);
}

document.querySelector("#exptexcel").addEventListener("click", function () {
    var html = document.querySelector("table").outerHTML;
    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    filename = 'MyTree_Reported_ChefPayout' + postfix + '.csv';
    export_table_to_csv(html, filename);
});
});


    </script>

    <script type="text/javascript">
        
    function searchbypickup()
    {
        $("#loading").show();
        var frmdate = '';
        var todate = '';
        var totm = '';
        var frmtm = '';
        var ftm = '';
        var fttm = '';

        frmdate = $("#frmdt").val();
        todate = $("#todt").val();
        
        frmtm = $("#frmtm").val();

        if(frmtm != '')
        {
            frmtm = frmtm.split(":");
             ftm = frmtm[0]+':'+frmtm[1];  
        }
        else
        {
           ftm = ''; 
        }  

        totm = $("#totm").val();

        if(totm != '')
        {
            totm = totm.split(":");
            fttm = totm[0]+':'+totm[1];  
        }
        else
        {
          fttm = '';
        }  
        

        // if(typeof ftm === 'undefined')
        // {
        //    ftm = '';
        // }

        // if(typeof fttm === 'undefined')
        // {
        //   fttm = '';
        // }  

        console.log("From Date"+frmdate+"to date"+todate+"From Time"+ftm+"To Time"+fttm);

        var reqpdt = {"fromdate":frmdate,"todate":todate,"fromtime":ftm,"totime":fttm};

        $.ajax({
                  url: 'service.php?servicename=SearchByPickupDate',
                  type: 'POST',
                  datatype: 'JSON',
                  contentType: 'application/json',
                  data: JSON.stringify(reqpdt),
                  async: false,
                  success: function(rspd)
                  {
                     console.log("search response"+JSON.stringify(rspd));

                     $('#tableChefOrderList').dataTable().fnClearTable();
                    $('#tableChefOrderList').dataTable().fnDraw();
                    $('#tableChefOrderList').dataTable().fnDestroy();

                    $("#loading").hide();
                     //console.log("Cheforders Response"+JSON.stringify(col));

                     var rscol = JSON.parse(rspd);

                     var co = new Array();

                     for(var oc=0;oc<rscol.cord.length;oc++)
                     {
                        co[oc] = new Array();

                        co[oc][0] = oc+1;

                        co[oc][1] = rscol.cord[oc].orderid;

                        co[oc][2] = rscol.cord[oc].chefname;

                        co[oc][3] = rscol.cord[oc].chefphone;

                        if(rscol.cord[oc].chefaddress != '')
                        {
                            co[oc][4] = rscol.cord[oc].chefaddress;
                        }
                        else
                        {
                            co[oc][4] = rscol.cord[oc].cheffaltno + ' - ' + rscol.cord[oc].chefbuilding;
                        }

                        co[oc][5] = rscol.cord[oc].customer;

                        co[oc][6] = rscol.cord[oc].customerphone;

                        if(rscol.cord[oc].customeraddress != '')
                        {
                             co[oc][7] = rscol.cord[oc].customeraddress;
                        }
                        else
                        {
                             co[oc][7] = rscol.cord[oc].customerflatno + ' - ' + rscol.cord[oc].customerbuilding;
                        }

                        co[oc][8] = rscol.cord[oc].cuisinename;

                        co[oc][9] = rscol.cord[oc].cuisinetype;

                        co[oc][10] = rscol.cord[oc].quantity;

                        co[oc][11] =  rscol.cord[oc].price;

                        co[oc][12] = rscol.cord[oc].total;

                        co[oc][13] = rscol.cord[oc].paymenttype; 
                        
                        co[oc][14] = rscol.cord[oc].pickupdate;

                        co[oc][15] = 'NA';                                                                                                      
                    }

                    $('#tableChefOrderList').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true
                

                    }); 
                  }    
        });
    }

    </script>

    <script type="text/javascript">
        
    function reset()
    {
        $("#frmdt").val('');

        $("#todt").val('');

        cheforderlist();

   }

    </script>