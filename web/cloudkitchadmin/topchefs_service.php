<?php

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['servicename'] == 'TC-ORDERS')
{
	$reqtc = file_get_contents('php://input');

	$restc = json_decode($reqtc,true);

	$type = '';
	$mt = '';
	$yr = '';
	$iv = '';


	$type = $restc['type'];
	$mt =  $restc['month'];
	$yr = $restc['year'];
	$iv = $restc['invitecode'];

	$tc = array();

	if($type == 'DS')
	{
		$queds = "SELECT (SELECT name FROM user WHERE userid=coi.chefid) as chefname,coi.chefid,
				  sum(coi.quantity) as sold from casseroleorderitem as coi,casseroleorder as co where
				  coi.cassordid=co.cassordid and YEAR(co.orderdate) = '".$yr."' 
				  AND MONTH(co.orderdate) = '".$mt."' AND co.invitecode =  '".$iv."' 
				  group by coi.chefid order by sold DESC LIMIT 5";

		$excds = mysqli_query($conn,$queds) or die(mysqli_error($conn));

		if(mysqli_num_rows($excds) > 0)
		{
			$tc['tc'] = array();

			while($rowtc = mysqli_fetch_assoc($excds))
			{
				$s = array();

				$s['chefname'] = $rowtc['chefname'];

				$s['amt'] = $rowtc['sold'];

				array_push($tc['tc'], $s);
			}

			$tc['status'] = 'success';
			$tc['msg'] = 'data available';	
		}
		else
		{
			$tc['status'] = 'failure';
			$tc['msg'] = 'not available';
		}		  
	}
	else if($type == 'ES')
	{
		$quer = "SELECT cp.chefid,u.name as chefname,SUM(total) as total FROM chefpayout as cp,user as u 
				 WHERE u.userid=cp.chefid AND MONTH(cp.createdate) = '".$mt."' AND 
				 YEAR(cp.createdate) = '".$yr."' AND cp.paidstatus='DONE' AND cp.invitecode='".$iv."' 
				 GROUP BY cp.chefid ORDER BY total DESC LIMIT 5";

		$excr = mysqli_query($conn,$quer) or die(mysqli_error($conn));
		
		if(mysqli_num_rows($excr) > 0)
		{
			$tc['tc'] = array();

			while($rowr = mysqli_fetch_assoc($excr))
			{
				$e = array();

				$e['chefname'] = $rowr['chefname'];

				$e['amt'] = $rowr['total'];

				array_push($tc['tc'], $e);
			}

			$tc['status'] = 'success';

			$tc['msg'] = 'data available';	
		}
		else
		{
			$tc['status'] = 'failure';
			$tc['msg'] = 'not available'; 
		}		 
	}

	print_r(json_encode($tc));
	exit;	
}

?>	