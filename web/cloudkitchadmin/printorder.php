<?php
    include 'inc/databaseConfig.php';
    $id = $_GET['itemid']; 
    // $id = base64_decode($idencoded); 
    $quecord = "SELECT (SELECT name FROM user as u1 WHERE u1.userid=coi.chefid ) as chefname, 
    (SELECT phone FROM user as u1 WHERE u1.userid=coi.chefid ) as chefphone,
    (SELECT tagline FROM user as u1 WHERE u1.userid=coi.chefid ) as tagline,
    (SELECT address FROM user as u1 WHERE u1.userid=coi.chefid ) as chefaddress, 
    (SELECT flatno FROM user as u1 WHERE u1.userid=coi.chefid ) as cheffaltno, 
    (SELECT building FROM user as u1 WHERE u1.userid=coi.chefid ) as chefbuilding, 
    co.cassordid as orderid,u.name as customer,u.phone as customerphone,
    u.address as customeraddress,u.flatno as customerflatno,u.building as customerbuilding,
    c.cuisinename,coi.quantity,coi.cassitemid as orderitemid,coi.price,coi.quantity * coi.price as total,
    co.orderdate,co.delivery_address,co.total as totalamount,co.invitecode,co.paymentType,coi.PaymentType as pt,co.transId,
    co.actualamount,coi.MTP,coi.cash,co.discountamount,co.couponapplied,
    (select couponcode FROM coupon WHERE couponid=co.couponid) as couponcode,
    coi.cuisineid,coi.type,c.cuisinetype,c.time,coi.pickupdate,coi.status,
    coi.code,coi.closedate,coi.usrwntDelivery as delivery,coi.IsAftertime as iat 
    FROM casseroleorder as co,user as u,cuisine as c,casseroleorderitem as coi
    WHERE c.cuisineid=coi.cuisineid and coi.cassordid=co.cassordid and 
    coi.cuisineid=c.cuisineid  and co.userid=u.userid and coi.type!='mohallabazar' AND coi.cassitemid = '$id'  ORDER BY co.cassordid DESC";
    $excaddcui = mysqli_query($conn,$quecord) or die(mysqli_error($conn));
    $rowcord = mysqli_fetch_assoc($excaddcui);

    $orderid = $rowcord['orderid'];
    $chefname = $rowcord['chefname'];
    $tagline = $rowcord['tagline'];
    $chefphone = $rowcord['chefphone'];
    $chefaddress = $rowcord['chefaddress'];
    $cheffaltno = $rowcord['cheffaltno'];
    $delivery_address = $rowcord['delivery_address'];
    $chefbuilding = $rowcord['chefbuilding'];
    $customer = $rowcord['customer'];
    $customerphone = $rowcord['customerphone'];
    $customeraddress = $rowcord['customeraddress'];
    $customerflatno = $rowcord['customerflatno'];
    $customerbuilding = $rowcord['customerbuilding'];
    $cuisinename = $rowcord['cuisinename'];
    $quantity = $rowcord['quantity'];
    $price = $rowcord['price'];
    $total1 = $rowcord['total'];
    $totalamount = $rowcord['totalamount'];
    $orderdate = $rowcord['orderdate'];
    $cuisinetype = $rowcord['cuisinetype'];
    $paymenttype = $rowcord['paymentType'];

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Reciept</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
        <style>
            body{width:70%;margin:0 auto;border:1px solid #5E6A6C;}
            *{padding: 0;margin:0;font-family: 'Open Sans', sans-serif;box-sizing: border-box;}
            p{font-size:14px;line-height:22px;}
            table{width: 100%;padding: 20px;border-collapse: collapse;margin:25px 0;}
            .centerText{text-align: center;}
            tr td,tr th{padding: 5px 20px;}
            .orderTable tr:first-child{background-color: #a38379;text-align: left;}
            .orderTable tr:first-child h3{color: #fff;}
            .orderTable tr td:last-child,table tr th:last-child,.addessTable tr td:last-child{text-align: right;}
        </style>
    </head>
    <body>
        <table>
            <tr class="centerText">
                <td>
                    <img src="http://cloudkitch.co.in/images/logo.png">
                </td>
            </tr>
            <tr class="centerText">
                <td>
                    <h3><?=$chefname?></h3>
                    <p><?=$tagline?></p>
                </td>
            </tr>
        </table>
        <table class="addessTable">
            <tr>
                <td>
                    <h3>Delivery Address</h3>
                    <p style="max-width: 400px;"><?=$delivery_address?><br>Phone: <?=$customerphone?></p>
                </td>
                <td>
                    <h3>Order Details</h3>
                    <p>Order ID: <?=$orderid?><br>Date: <?=$orderdate?></p>
                </td>
            </tr>
        </table>
        <table class="orderTable">
            <tr>
                <th>
                    <h3>Description</h3>
                </th>
                <th>
                    <h3>Quantity</h3>
                </th>
                <th>
                    <h3>Price</h3>
                </th>
                <th>
                    <h3>Amount</h3>
                </th>
            </tr>
            <tr>
                <td>
                    <p><?=$cuisinename?></p>
                </td>
                <td>
                    <p><?=$quantity?></p>
                </td>
                <td>
                    <p><?=$price?></p>
                </td>
                <td>
                    <p>₹ <?=$total1?></p>
                </td>
            </tr>
           <!--  <tr style="border-top: 1px solid #d8d8d8;">
                <td colspan="3">
                    <p>Total</p>
                </td>
                <td>
                    <p><?=$total1?></p>
                </td>
            </tr> -->
            <!-- <tr>
                <td colspan="3">
                    <p>2.5% SGST</p>
                </td>
                <td>
                    <p>10.00</p>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <p>2.5% CGST</p>
                </td>
                <td>
                    <p>10.00</p>
                </td>
            </tr> -->
            <tr style="border-top: 1px solid #d8d8d8;">
                <td colspan="3">
                    <h3>Grant Total</h3>
                </td>
                <td>
                    <h3>₹ <?=$total1?></h3>
                </td>
            </tr>
        </table>
    </body>
</html>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function(){
      window.print();
  });
</script>