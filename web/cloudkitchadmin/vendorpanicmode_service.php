<?php

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['servicename'] == 'Reported-Orders')
{
	$reqro = file_get_contents('php://input');

	$resro =  json_decode($reqro,true);

	$invc = '';

	$invc = $resro['invitecode'];

	$ro = array();

	$quero = "SELECT co.userid,(SELECT name FROM user WHERE userid=co.userid)
	 as customername,coi.chefid,(SELECT name FROM user WHERE userid=coi.chefid)
	  as vendorname,co.cassordid,coi.cassitemid,i.itemname,coi.IsUserPanic,coi.IsChefPanic,
	  coi.userpanicreason,coi.chefpanicreason,coi.IsResolved,coi.uoptional,coi.coptional,coi.panicdatetime 
	  FROM casseroleorderitem as coi,casseroleorder as co,item as i WHERE coi.IsResolved='N' AND co.cassordid=coi.cassordid 
	   AND i.itemid=coi.cuisineid AND co.invitecode='".$invc."' ORDER BY co.cassordid DESC";

	// $quero = "SELECT co.userid,(SELECT name FROM user WHERE userid=co.userid) as customername,coi.chefid,(SELECT name FROM user WHERE userid=coi.chefid) as chefname,co.cassordid,coi.cassitemid,c.cuisinename,coi.IsUserPanic,coi.IsChefPanic,coi.userpanicreason,coi.chefpanicreason,coi.IsResolved FROM casseroleorderitem as coi,casseroleorder as co,cuisine as c WHERE co.cassordid=coi.cassordid  AND c.cuisineid=coi.cuisineid
	// 		  AND co.invitecode='".$invc."'";

	$excro = mysqli_query($conn,$quero) or die(mysqli_error($conn));

	if(mysqli_num_rows($excro) > 0)
	{
		$ro['reportedo'] = array();

		while ($rowro = mysqli_fetch_assoc($excro)) {
			
			$r = array();

			if($rowro['IsUserPanic'] == 'Y')
			{
				$r['user1'] = $rowro['customername']."[Bhukkad]";

				$r['user2'] = $rowro['vendorname']."[Vendor]";

				$r['reason'] = $rowro['userpanicreason'];

				$r['uop'] = $rowro['uoptional'];

				$r['cop'] = '';
			}

			if($rowro['IsChefPanic'] == 'Y')
			{
				$r['user1'] = $rowro['vendorname']."[Vendor]";

				$r['user2'] = $rowro['customername']."[Bhukkad]";

				$r['reason'] = $rowro['chefpanicreason'];

				$r['cop'] = $rowro['coptional'];

				$r['uop'] = '';
			}

			$r['orderid'] = $rowro['cassordid'];

			$r['itemid'] = $rowro['cassitemid'];

			$r['dishname'] = $rowro['itemname'];

			$r['panicdatetime'] = $rowro['panicdatetime'];

			$r['IsResolved'] = $rowro['IsResolved'];

			array_push($ro['reportedo'], $r);	
		}

		$ro['status'] = 'success';
		$ro['msg'] = 'Data available';
	}
	else
	{
		$ro['status'] = 'failure';
		$ro['msg'] = 'Data not available';
	}

	print_r(json_encode($ro));
	exit;		   

}


if($_GET['servicename'] == 'Reported-Orders-Date')
{
	$reqro = file_get_contents('php://input');

	$resro =  json_decode($reqro,true);
    

	$invc = '';

	$fromdate = '';

    $todate = '';

    $ft = '';

    $tt = '';

    $sql = '';
    
	$invc = $resro['invitecode'];

	$fromdate = $respd['fromdate'];

    $todate  = $respd['todate'];

    $ft = $respd['fromtime'];

    $tt =  $respd['totime'];

	$ro = array();


	 if($fromdate != '' && $todate == '' && $ft == '')
    {
        $fromdate = strtotime($fromdate);

        $ffd = '';

        $ffd = date('Y-m-d',$fromdate);

        $sql.= " AND DATE_FORMAT (c.time,'%Y-%m-%d') >= '".$ffd."' AND co.invitecode='".$invc."'";    
    }

    if($todate != '' && $fromdate == '' && $tt == '')
    {
        $todate = strtotime($todate);

        $ttd = '';

        $ttd = date('Y-m-d',$todate);

        $sql.= " AND DATE_FORMAT(c.time,'%Y-%m-%d') <= '".$ttd."' AND co.invitecode='".$invc."'";
    }

    if($fromdate != '' && $ft != '' && $todate == '')
    {
        $fromdate = strtotime($fromdate);

        $ffd = '';

        $ffd = date('Y-m-d',$fromdate);

        $sql.= " AND DATE_FORMAT (c.time,'%Y-%m-%d %H:%i') >= '".$ffd." ".$ft."' AND co.invitecode='".$invc."'";
    }

    if($todate != '' && $tt != '' && $fromdate == '')
    {
        $todate = strtotime($todate);

        $ttd = '';

        $ttd = date('Y-m-d',$todate);

        $sql.= " AND DATE_FORMAT(c.time,'%Y-%m-%d %H:%i') <= '".$ttd." ".$tt." ' AND co.invitecode='".$invc."'";
    }

    if($fromdate != '' && $todate != '' && $ft != '' && $tt != '')
    {
        $fromdate = strtotime($fromdate);
        $todate = strtotime($todate);

        $ffd = '';
        $ttd = '';

        $ffd = date('Y-m-d',$fromdate);
        $ttd = date('Y-m-d',$todate);

        $sql.= " AND DATE_FORMAT(c.time,'%Y-%m-%d %H:%i') >= '".$ffd." ".$ft."' AND DATE_FORMAT(c.time,'%Y-%m-%d %H:%i') <= '".$ttd." ".$tt."' AND co.invitecode='".$invc."'";   
    }    


    if($fromdate != '' && $todate != '' && $ft == '' && $tt == '')
    {
        $fromdate = strtotime($fromdate);
        $todate = strtotime($todate);

        $ffd = '';
        $ttd = '';

        $ffd = date('Y-m-d',$fromdate);
        $ttd = date('Y-m-d',$todate);

        $sql.= " AND DATE_FORMAT(c.time,'%Y-%m-%d') >= '".$ffd."' AND DATE_FORMAT(c.time,'%Y-%m-%d') <= '".$ttd."' AND co.invitecode='".$invc."'";
    } 


	$quero = "SELECT co.userid,(SELECT name FROM user WHERE userid=co.userid) as customername,coi.chefid,
	(SELECT name FROM user WHERE userid=coi.chefid) as chefname,co.cassordid,coi.cassitemid,
	c.cuisinename,coi.IsUserPanic,coi.IsChefPanic,coi.userpanicreason,coi.chefpanicreason,coi.IsResolved,
	coi.uoptional,coi.coptional FROM casseroleorderitem as coi,casseroleorder as co,
	cuisine as c WHERE coi.IsResolved='N' AND co.cassordid=coi.cassordid  AND c.cuisineid=coi.cuisineid ".$sql." ORDER BY co.cassordid DESC";

	echo $sql;
	exit;

	// $quero = "SELECT co.userid,(SELECT name FROM user WHERE userid=co.userid) as customername,coi.chefid,(SELECT name FROM user WHERE userid=coi.chefid) as chefname,co.cassordid,coi.cassitemid,c.cuisinename,coi.IsUserPanic,coi.IsChefPanic,coi.userpanicreason,coi.chefpanicreason,coi.IsResolved FROM casseroleorderitem as coi,casseroleorder as co,cuisine as c WHERE co.cassordid=coi.cassordid  AND c.cuisineid=coi.cuisineid
	// 		  AND co.invitecode='".$invc."'";

	$excro = mysqli_query($conn,$quero) or die(mysqli_error($conn));

	if(mysqli_num_rows($excro) > 0)
	{
		$ro['reportedo'] = array();

		while ($rowro = mysqli_fetch_assoc($excro)) {
			
			$r = array();

			if($rowro['IsUserPanic'] == 'Y')
			{
				$r['user1'] = $rowro['customername']."[Bhukkad]";

				$r['user2'] = $rowro['chefname']."[Chef]";

				$r['reason'] = $rowro['userpanicreason'];

				$r['uop'] = $rowro['uoptional'];

				$r['cop'] = '';
			}

			if($rowro['IsChefPanic'] == 'Y')
			{
				$r['user1'] = $rowro['chefname']."[Chef]";

				$r['user2'] = $rowro['customername']."[Bhukkad]";

				$r['reason'] = $rowro['chefpanicreason'];

				$r['cop'] = $rowro['coptional'];

				$r['uop'] = '';
			}

			$r['orderid'] = $rowro['cassordid'];

			$r['itemid'] = $rowro['cassitemid'];

			$r['dishname'] = $rowro['cuisinename'];

			$r['IsResolved'] = $rowro['IsResolved'];

			array_push($ro['reportedo'], $r);	
		}

		$ro['status'] = 'success';
		$ro['msg'] = 'Data available';
	}
	else
	{
		$ro['status'] = 'failure';
		$ro['msg'] = 'Data not available';
	}

	print_r(json_encode($ro));
	exit;		   

}

// if($_GET['servicename'] == 'Resolved')
// { 
// 	$reqrs = file_get_contents('php://input');

// 	$resrs = json_decode($reqrs,true);

// 	$itemid = '';

// 	$itemid = $resrs['itemid'];

// 	$res = array();

// 	$ures =  "UPDATE casseroleorderitem SET IsResolved='Y' WHERE cassitemid='".$itemid."'";
// 	$excres = mysqli_query($conn,$ures) or die(mysqli_error($conn));

// 	if($excres)
// 	{
// 		$res['status'] = 'success';
// 		$res['msg'] = 'Successfully resolved';
// 	}
// 	else
// 	{
// 		$res['status'] = 'failure';
// 		$res['msg'] = 'Failed to resolved';
// 	}

// 	print_r(json_encode($res));
// 	exit;	
// }

if($_GET['servicename'] == 'ResolvedVendor')
{ 
	$reqrs = file_get_contents('php://input');

	$resrs = json_decode($reqrs,true);

	$itemid = '';

	$narr = '';

	$itemid = $resrs['itemid'];

	$narr =  $resrs['Narration'];

	$narrdt = $resrs['Narrationdt'];

	$res = array();

	$ures =  "UPDATE casseroleorderitem SET IsResolved='Y',ordernarratiion='".$narr."',narrationdate='".$narrdt."' WHERE cassitemid='".$itemid."'";

	$excres = mysqli_query($conn,$ures) or die(mysqli_error($conn));

	if($excres)
	{
		$res['status'] = 'success';
		$res['msg'] = 'Successfully resolved';
	}
	else
	{
		$res['status'] = 'failure';
		$res['msg'] = 'Failed to resolved';
	}

	print_r(json_encode($res));
	exit;	
}

if($_GET['servicename'] == 'Reported-Orders-History')
{
	$reqroh = file_get_contents('php://input');

	$resroh = json_decode($reqroh,true);

	$invitecode = '';

	$invitecode = $resroh['invitecode'];

	$roh = array();

	$queroh = "SELECT co.userid,co.cassordid,coi.cuisineid,coi.cassitemid,coi.chefid,
			   (SELECT name FROM user WHERE userid=co.userid) as customername,
				(SELECT name FROM user WHERE userid=coi.chefid) as vendorname,
				(SELECT itemname FROM item WHERE itemid=coi.cuisineid) as itemname,
				coi.IsUserPanic,coi.IsChefPanic,coi.userpanicreason,coi.ordernarratiion,
				coi.chefpanicreason,coi.IsResolved,coi.uoptional,coi.coptional
				FROM casseroleorderitem as coi,casseroleorder as co,item as i WHERE coi.IsResolved='Y' AND coi.type='mohallabazar' and co.cassordid=coi.cassordid and i.itemid=coi.cuisineid  and coi.panicdatetime != '' AND co.invitecode='".$invitecode."' ORDER BY coi.narrationdate DESC";

	// echo "Q".$queroh;
	// exit;			

	$excroh = mysqli_query($conn,$queroh) or die(mysqli_error($conn));
	
	if(mysqli_num_rows($excroh) > 0)
	{
		$roh['reportedoh'] = array();

		while($rowroh = mysqli_fetch_assoc($excroh))
		{
			$oh = array();

			if($rowroh['IsUserPanic'] == 'Y')
			{
				$oh['user1'] = $rowroh['customername']."[Bhukkad]";

				$oh['user2'] = $rowroh['vendorname']."[Vendor]";

				$oh['reason'] = $rowroh['userpanicreason'];

				$oh['ordernarratiion'] = $rowroh['ordernarratiion'];

				$oh['orderid'] = $rowroh['cassordid'];

				$oh['itemid'] = $rowroh['cassitemid'];

				$oh['dishname'] = $rowroh['itemname'];

				$oh['IsResolved'] = $rowroh['IsResolved'];

				$oh['uop'] = $rowroh['uoptional'];

				$oh['cop'] = '';

				array_push($roh['reportedoh'], $oh);
			}

			if($rowroh['IsChefPanic'] == 'Y')
			{
				$oh['user1'] = $rowroh['vendorname']."[Vendor]";

				$oh['user2'] = $rowroh['customername']."[Bhukkad]";

				$oh['reason'] = $rowroh['chefpanicreason'];

				$oh['ordernarratiion'] = $rowroh['ordernarratiion'];

				$oh['orderid'] = $rowroh['cassordid'];

				$oh['itemid'] = $rowroh['cassitemid'];

				$oh['dishname'] = $rowroh['itemname'];

				$oh['IsResolved'] = $rowroh['IsResolved'];

				$oh['uop'] = '';

				$oh['cop'] = $rowroh['coptional'];

				array_push($roh['reportedoh'], $oh);
			}

			

		}

		$roh['status'] = 'success';
		$roh['msg'] = 'Data available';

	}
	else
	{
		$roh['status'] =  'failure';
		$roh['msg'] = 'Not available';
	}

	print_r(json_encode($roh));
	exit;	

}	

?>	