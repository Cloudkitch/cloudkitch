<?php 

session_start();
include 'inc/databaseConfig.php';
//print_r($_SESSION);

if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: https://coludkitch.co.in/cloudkitchadmin/index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
// include 'inc/config.php'; $template['header_link'] = 'WELCOME ';
 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Validation Header -->
    <!-- <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Create Invitation Code</h1>
                </div>
            </div>
           
        </div>
    </div> -->

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- END Validation Header --> 

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <div class="block">
                <!-- Form Validation Title -->
                <div class="block-title">
                    <h2>Create Cuisine Type Form</h2>
                </div>
                <!-- END Form Validation Title -->

                <!-- Form Validation Form -->
                
                <!--event_start_time,event_end_time,address,info-->
                <form id="createcuisinetyp" action="addcuisinetype.php"  method="post" class="form-horizontal form-bordered">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-pagetype">Page Type<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <select name="val-pagetype" id="val-pagetype" class="form-control">
                                <option value="1">Home Page Type</option>
                                <option value="2" selected>Kitchen Page Type</option>
                                <option value="3">Team Page Type</option>
                                <option value="4">Event Page Type</option>
                                <option value="5">Party Page Type</option>
                                <option value="6">Cafeteria Page Type</option>
                                <option value="7">Society Type</option>
                                <option value="8">Kitty Type</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-cuitype">Cuisine Type<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-cuitype" name="val-cuitype" class="form-control" placeholder="Please Enter Cuisine Type">
                        </div>
                        <span id="user-result-cuitype" style="width:90%;vertical-align: -webkit-baseline-middle;"></span>
                    </div>
                           
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="category_type">Corporate<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                        
                        <select class="form-control selectpicker" id="corporate_type" data-live-search="true" multiple="multiple" name="val_corporate_type[]">                                
                            <?php
                                $query = "SELECT * FROM corporate WHERE status = '1' ORDER BY id ASC";
                                $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                                while($row = mysqli_fetch_assoc($result))
                                {
                                    echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
                                }   
                            ?> 
                            </select>
                        
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="kitchens">Kitchen<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                        
                        <select class="form-control selectpicker" id="kitchens" data-live-search="true" multiple="multiple" name="kitchens[]">                                
                            <?php
                                $query = "SELECT * FROM user WHERE ischef ='1' AND status='1' ORDER BY name ASC";
                                $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                                while($row = mysqli_fetch_assoc($result))
                                {
                                    echo '<option value="'.$row['userid'].'">'.$row['name'].'</option>';
                                }   
                            ?> 
                            </select>
                        
                        </div>
                    </div>

                    <input type="hidden" name="email" id="email" value="<?php echo $_SESSION['info']['email']; ?>" >
                    <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-3">
                            <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button>
                            <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Form Validation Form -->

                
            </div>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
<!-- Load and execute javascript code used only in this page -->
<script src="js/CasseroleService.js"></script>
<script src="js/pages/formsValidationCreateInviteCode.js"></script>
<script>$(function() { FormsValidation.init();
//cuisinecategory();
 });</script>
<script type="text/javascript">
$(document).ready(function() {
    $("#loading").hide();
    var x_timer;    
    $("#val-cuitype").keyup(function (e){
        clearTimeout(x_timer);
        var ctype = $(this).val();
        var pagetype = $("#val-pagetype").val();
        console.log("CUisine Type Is"+ctype);
        x_timer = setTimeout(function(){
            // check_weddingCode_ajax(user_name);
            // check_inviteCode(icode);
            check_cuisinetype(ctype,pagetype);
        }, 1000);
    }); 


});
</script>
<?php include 'inc/template_end.php'; ?>

