<?php

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['servicename'] == 'weeklytotalorders')
{
	$reqwto = file_get_contents('php://input');

	$reswto = json_decode($reqwto,true);

	$fd = '';

	$td = '';

	$ic = '';

	$fdd = '';

	$tdd = '';

	$fd = $reswto['fd'];

	$td = $reswto['td'];

	$ic = $reswto['invitecode'];

	$wto = array();

	$sql = '';

	$totalo =  '';

	$totalco = '';

	$totald = '';

	if($fd != '' && $td == '')
	{
		$fdd = date('Y-m-d',strtotime($fd));

		$sql.= " AND DATE_FORMAT(co.orderdate,'%Y-%m-%d') >= '".$fdd."' ";
	}

	if($fd == '' && $td != '')
	{
		$tdd = date('Y-m-d',strtotime($td));

		$sql.= " AND DATE_FORMAT(co.orderdate,'%Y-%m-%d') <= '".$tdd."' ";
	}

	if($fd != '' && $td != '')
	{
		$fdd = date('Y-m-d',strtotime($fd));

		$tdd = date('Y-m-d',strtotime($td));

		$sql.= " AND DATE_FORMAT(co.orderdate,'%Y-%m-%d') >= '".$fdd."' AND DATE_FORMAT(co.orderdate,'%Y-%m-%d') <= '".$tdd."' ";
	}

	$queto =  "SELECT count(coi.cassordid) as totalorders FROM casseroleorder as co,casseroleorderitem as coi
			   WHERE co.cassordid=coi.cassordid AND coi.status = 'Done' AND co.invitecode = '".$ic."' ".$sql." GROUP BY
			   coi.cassordid";

	$excto = mysqli_query($conn,$queto) or die(mysqli_error($conn));

	if(mysqli_num_rows($excto) > 0)
	{
		$totalo = mysqli_num_rows($excto);
	}
	else
	{
		$totalo = '0';
	}

	$queco =  "SELECT count(coi.cassordid) as totalorders FROM casseroleorder as co,casseroleorderitem as coi
			   WHERE co.cassordid=coi.cassordid AND coi.status = 'cancel' AND co.invitecode = '".$ic."' ".$sql." GROUP BY coi.cassordid";

	$excco = mysqli_query($conn,$queco) or die(mysqli_error($conn));

	if(mysqli_num_rows($excco) > 0)
	{
		$totalco = mysqli_num_rows($excco);
	}
	else
	{
		$totalco = '0';
	}

	$quetd = "SELECT sum(coi.quantity) as totaldishes FROM casseroleorder as co,casseroleorderitem as coi
			  WHERE co.cassordid=coi.cassordid AND coi.status = 'Done' AND co.invitecode = '".$ic."' ".$sql;

	$exctd = mysqli_query($conn,$quetd) or die(mysqli_error($conn));

	$rstd = mysqli_fetch_assoc($exctd);

	if($rstd['totaldishes'] != null)
	{
		$totald = $rstd['totaldishes'];
	}
	else
	{
		$totald = '0';
	}

	$wto['wto'] = array();

	$to = array();

	$to['totalo'] = (string)$totalo;

	$to['totalco'] = (string)$totalco;

	$to['totald'] = (string)$totald;

	array_push($wto['wto'], $to);

	$wto['status'] = 'success';

	$wto['msg'] = 'data available';

	print_r(json_encode($wto));

	exit;		  		   	

}

?>