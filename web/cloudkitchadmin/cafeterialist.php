<?php 

session_start();
include 'inc/config.php'; 
if(!isset($_SESSION['info']['user']))
{
  header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
include 'inc/template_start.php';
include 'inc/page_head.php';

?>


<!-- Page content -->
<div id="page-content">

  <div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <!-- <a href="#myModal" class="btn btn-lg btn-primary" data-toggle="modal">Launch
    Demo Modal</a> -->
    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
            aria-hidden="true">&times;</button>
            <h4 class="modal-title">Cafeteria Detail</h4>
            
          </div>
          <div class="modal-body">

            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <!-- Form Validation Block -->
                <table id ="tablecuisinesmore" class="table table-vcenter table-striped table-hover table-borderless">
                  <thead>
                   <tr>
                     <th>Sr</th>
                     <th>Trending</th>
                     <th>Onrequest</th>
                     <th>Spicymeter</th>
                     <th>Type</th>
                     <th>Serving Detail</th>
                     <th>No of Servings</th>
                     <th>Dish Type</th>
                     <th>Tifin</th>
                     <th>Refregeration</th>
                     <th>Consume Date</th>
                     <th>Delivery</th>                                                                                                                
                   </tr> 
                 </thead>

               </table>
               <!-- END Form Validation Block -->
             </div>
           </div>

           

         </div>
         <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div id="stamplist" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:70%;overflow-y: scroll; max-height:85%;">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Stamp List</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
              <!-- Form Validation Block -->
              <table id ="stamplists" class="table table-vcenter table-striped table-hover table-borderless">
                <thead>
                  <tr>
                    <th>Sr</th>
                    <th>Stamp</th>
                    <th>Preview</th>
                    <th>Assign</th>                                                                                                            
                  </tr> 
                </thead>

              </table>
              <!-- END Form Validation Block -->
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

</div>

<div id="loading" style="position:fixed;left: 50%;
top: 50%;">
<img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<!-- Validation Header -->
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1>Cafeteria</h1>
        <!-- <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>"> -->
      </div>
    </div>
    <?php if($role != 2){ ?>
      <div class="col-sm-6">
      <div class="header-section">
        <a href="add_cafeteria.php" style="width: 150px;float: right;" class="btn btn-block btn-primary">
          <i class="fa fa-plus"></i> Add Cafeteria
        </a>
      </div>
    </div>  
    <?php } ?>
  </div>
</div>
<!-- END Validation Header -->

<!-- Form Validation Content -->
<div class="row">
  <div class="col-sm-12 col-md-12 col-lg-12">
    
    <!-- Form Validation Block -->
    <table id ="tableCuisineList" class="table table-vcenter table-striped table-hover table-borderless">
      <thead>
       <tr>
         <th>Sr</th>
         <th>Cuisine Name</th>
         <th>Cuisine Pic</th>
         <th>Category</th>
         <th>Home Page Category</th>
         <th>Price</th>
         <th>Status</th>
         <?php 
         if($role != 2){
           echo " <th>Action</th>";
         }
        ?>
       </tr> 
     </thead>

   </table>
   <!-- END Form Validation Block -->
 </div>
</div>
<!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
            </div> -->
          </div>
          <!-- END Page Content -->

          <?php include 'inc/page_footer.php'; ?>
          <?php include 'inc/template_scripts.php'; ?>
          <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
          <!-- Load and execute javascript code used only in this page -->
          <!-- <script src="js/MIHService.js"></script> -->
          <script src="js/CasseroleService.js"></script>
          <!-- <script src="js/cuisine.js"></script> -->
          


          <?php include 'inc/template_end.php'; ?>

          <script type="text/javascript">
            $(document).ready(function(){
              
              $("#loading").hide();
              
            });
          </script>
          <script src="js/pages/uiTables.js"></script>
          <script>
            $(function(){ UiTables.init(); 
              var invc = '';
              cafeterialist();
            });
            function deletedata(id)
            {
    
              swal({
                title: "Are You sure want to delete this?",
                //text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                  var request = {"id":id};
                  $.ajax({
                    url: 'service.php?servicename=deletecuisine',
                    type: 'POST',
                    data: JSON.stringify(request),
                    contentType: 'application/json; charset=utf-8',
                    datatype: 'JSON',
                    async: true,
                    success: function(data)
                    {
                      var result = JSON.parse(data);
                      if(result.status == 'success')
                      {
                        cafeterialist();
                        $("#toast-error").html(result.msg);
                        $("#toasterError").fadeIn();
                      }
                      else
                      {
                        $("#toast-error").html(result.msg);
                        $("#toasterError").fadeIn();
                      }
                      setTimeout(function(){
                        $("#toaster").fadeOut();
                        $("#toasterError").fadeOut();
                      }, 3000);
                    }
                  });
                } 
              });

            }
          </script>

<script type="text/javascript">

   /* function exportcuisines() 
    {

        console.log("C");
          
            //getting data from our table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('tableCuisineList');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');

            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
                 var dt = new Date();
                var day = dt.getDate();
                var month = dt.getMonth() + 1;
                var year = dt.getFullYear();
                var hour = dt.getHours();
                var mins = dt.getMinutes();
                var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
            a.download = 'MyTree_Cuisines' + postfix + '.xls';
            a.click();

          }*/

          function download_csv(csv, filename) {
            var csvFile;
            var downloadLink;
            // CSV FILE
            csvFile = new Blob([csv], {type: "text/csv"});
            // Download link
            downloadLink = document.createElement("a");
            // File name
            downloadLink.download = filename;
            // We have to create a link to the file
            downloadLink.href = window.URL.createObjectURL(csvFile);
            // Make sure that the link is not displayed
            downloadLink.style.display = "none";
            // Add the link to your DOM
            document.body.appendChild(downloadLink);
            // Lanzamos
            downloadLink.click();
        }

  function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#tableCuisineList tr");
    
    for (var i = 0; i < rows.length; i++) {
      var row = [], cols = rows[i].querySelectorAll("td, th");

      for (var j = 0; j < cols.length; j++) 
        row.push(cols[j].innerText.replace(/,/g,' '));        
      csv.push(row.join(","));       
    }
    // Download CSV
    download_csv(csv.join("\n"), filename);
  }

  document.querySelector("#exptexcel").addEventListener("click", function () {
    var html = document.querySelector("#tableCuisineList").outerHTML;
    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    filename = 'MyTree_Cuisines' + postfix + '.csv';
    export_table_to_csv(html, filename);
  });


  function uploadExcel()
  {
    if( document.getElementById("excelfile").files.length == 0 ){
     $("#toast-error").html("Please Choose a xlsx file.");
     $("#toasterError").fadeIn();
     setTimeout(function(){
      $("#toaster").fadeOut();
      $("#toasterError").fadeOut();
    }, 3000);
   }
   else
   {
    var data = new FormData($('#excel_form')[0]);
    $.ajax({
      url: 'service.php?servicename=importCuisines',
      type: 'POST',
      mimeType: "multipart/form-data",
      contentType: false,
      cache: false,
      processData: false,
      data: data, 
      success: function(data)
      {
        var html="";
        var result = JSON.parse(data);
        if (result.status=="success") {
          html +='<span style="color:green">Total entry done: "'+result.imported+'"</span><br>'; 
          html +='<span>Total entry failed: "'+result.notimported+'"</span><br>'; 
          $("#response").html(html);
          $('#excel_form')[0].reset();
          cuisinelist();
        } 
        else
        {
          $("#response").html(result.msg);
        }
      }
    });
  }
}
// Aradhan
</script>   