<?php

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['servicename'] == 'CTW-ORDERS')
{
	$reqcwo = file_get_contents('php://input');

	$rescwo = json_decode($reqcwo,true);

	$c = '';
	$ic = '';
	$fd = '';
	$td = '';
	$fdd = '';
	$tdd = '';
	$sql = '';

	$fd = $rescwo['fd'];

	$td = $rescwo['td'];

	$c = $rescwo['cat'];

	$ic = $rescwo['invitecode'];

	if($fd != '' && $td == '')
	{
		$fdd = date('Y-m-d',strtotime($fd));

		$sql.= " AND DATE_FORMAT(co.orderdate,'%Y-%m-%d') >= '".$fdd."' ";
	}

	if($fd == '' && $td != '')
	{
		$tdd = date('Y-m-d',strtotime($td));

		$sql.= " AND DATE_FORMAT(co.orderdate,'%Y-%m-%d') <= '".$tdd."' ";
	}

	if($fd != '' && $td != '')
	{
		$fdd = date('Y-m-d',strtotime($fd));

		$tdd = date('Y-m-d',strtotime($td));

		$sql.= " AND DATE_FORMAT(co.orderdate,'%Y-%m-%d') >= '".$fdd."' AND DATE_FORMAT(co.orderdate,'%Y-%m-%d') <= '".$tdd."' ";
	}

	$cwo = array();

	if($c == 'MT')
	{
		$quemt = "SELECT  c.type,COUNT(coi.cassitemid) as orders 
				  FROM casseroleorderitem as coi,cuisine as c,casseroleorder as co
				  WHERE  c.cuisineid=coi.cuisineid AND co.cassordid=coi.cassordid AND co.invitecode='".$ic."' ".$sql."
				  GROUP BY c.type ORDER BY orders DESC";

		$excmt = mysqli_query($conn,$quemt) or die(mysqli_error($conn));

		if(mysqli_num_rows($excmt) > 0)
		{
			$cwo['ctw'] = array();

			while($rowmt = mysqli_fetch_assoc($excmt))
			{
				$m = array();

				$m['type'] = $rowmt['type'];

				$m['orders'] = $rowmt['orders'];

				array_push($cwo['ctw'], $m);
			}

			$cwo['status'] = 'success';

			$cwo['msg'] = 'data available';	
		}
		else
		{
			$cwo['status'] = 'failure';

			$cwo['msg'] = 'not available';
		}		  
	} 
	else if($c == 'CT')
	{
		$quect = "SELECT c.cuisinetype,COUNT(coi.cassitemid) as count 
				  FROM casseroleorderitem as coi,cuisine as c,casseroleorder as co
				  WHERE c.cuisineid=coi.cuisineid AND co.cassordid=coi.cassordid AND co.invitecode='".$ic."' ".$sql."
				  GROUP BY c.cuisinetype ORDER BY count DESC";

		$excct = mysqli_query($conn,$quect) or die(mysqli_error($conn));

		if(mysqli_num_rows($excct) > 0)
		{
			$cwo['ctw'] = array();

			while($rowct = mysqli_fetch_assoc($excct))
			{
				$t = array();

				$t['type'] = $rowct['cuisinetype'];

				$t['orders'] = $rowct['count'];

				array_push($cwo['ctw'], $t);
			}

			$cwo['status'] = 'success';

			$cwo['msg'] = 'data available';	
		}
		else
		{
			$cwo['status'] = 'failure';

			$cwo['msg'] = 'not available';
		} 		  
	}
	else if($c == 'PHD')
	{
		$queph = "SELECT IF (coi.usrwntDelivery ='Y','Homedelivery','Pickup') as deliverytype ,COUNT(coi.cassitemid) as 		  countph FROM casseroleorderitem as coi,cuisine as c,casseroleorder as co
				  WHERE  c.cuisineid=coi.cuisineid AND co.cassordid=coi.cassordid AND co.invitecode='".$ic."' ".$sql."
				  GROUP BY coi.usrwntDelivery ORDER BY countph DESC LIMIT 2";

		$excph = mysqli_query($conn,$queph) or die(mysqli_error($conn));

		if(mysqli_num_rows($excph) > 0)
		{
			$cwo['ctw'] = array();

			while($rowph = mysqli_fetch_assoc($excph))
			{
				$ph = array();

				$ph['type'] = $rowph['deliverytype'];

				$ph['orders'] = $rowph['countph'];

				array_push($cwo['ctw'], $ph);
			}

			$cwo['status'] =  'success';

			$cwo['msg'] = 'data available';	
		}
		else
		{
			$cwo['status'] = 'failure';

			$cwo['msg'] = 'not available';
		}		  
	}

	print_r(json_encode($cwo));

	exit; 
}

?>	