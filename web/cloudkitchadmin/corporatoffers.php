<?php 
session_start();
include 'inc/config.php'; 

if(!isset($_SESSION['info']['user']))
{
	header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">

  <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
  <img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<!-- Validation Header -->
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1>Corporate Offer List</h1>
        <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
      </div>
    </div>
    <div class="col-sm-6">
     <div class="header-section">
      <a href="add_corporate_offers.php" style="width: 170px;float: right;" class="btn btn-block btn-primary">
        <i class="fa fa-plus"></i> Add Corporate Offers
      </a>
    </div>
  </div>
</div>
</div>
<!-- END Validation Header -->

<!-- Form Validation Content -->
<div class="row">
  <div class="col-sm-12 col-md-12 col-lg-12">
    <!-- Form Validation Block -->
    <table id ="tableChefList" class="table table-vcenter table-striped table-hover table-borderless">
      <thead>
       <tr>
         <th>Sr</th>
         <th>Corprate Name</th>
         <th>Restaurant Name</th>
         <th>Offer (In Percentage)</th>
         <th>Start Date</th>
         <th>End Date</th>
         <th>Status</th>
         <!-- <th>Action</th> -->
       </tr> 
     </thead>

   </table>
   <!-- END Form Validation Block -->
 </div>
</div>

</div>


<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#loading").hide();
    getCorporateOffer();
  });
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 
});
</script>
<script>
  function getCorporateOffer()
  {
    $('#loading').show();
    $.ajax({
      type: "POST",
      url : "service.php?servicename=getCorporateOffer",
      datatype: "JSON",
      success: function(data)
      {
        $('#loading').hide();
        var cusl = JSON.parse(data);
        var customer = new Array();
        for(var c=0;c<cusl.corporateoffer.length;c++)
        {
          customer[c] = new Array();
          customer[c][0] = c+1;
          customer[c][1] = cusl.corporateoffer[c].corporatename;
          customer[c][2] = cusl.corporateoffer[c].restaurant;
          customer[c][3] = cusl.corporateoffer[c].offer;
          customer[c][4] = cusl.corporateoffer[c].startdate;
          customer[c][5] = cusl.corporateoffer[c].enddate;
          if(cusl.corporateoffer[c].isActive == 'Y')
          {
            customer[c][6] = "<button type='button' onclick='inactiveoffer("+cusl.corporateoffer[c].id+");' class='btn btn-rounded btn-primary'>Active</button>";
          }
          else
          {
            customer[c][6] = "<button type='button' class='btn btn-rounded btn-primary'>InActive</button>";
          }
          // customer[c][3] = '<div class=""> <a href="edit_corporate.php?id='+cusl.corporate[c].id+'" class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Edit"> <i class="fa fa-pencil"></i> </a>&nbsp;&nbsp <button type="button" onclick = "deletedata('+cusl.corporate[c].id+');" class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Delete"> <i class="fa fa-fw fa-times"></i> </button> </div>';
        }  
        $('#tableChefList').dataTable({
          "aaData": customer,
          "scrollX": true,
          "bDestroy": true
        });
      }
    });
  }
  function deletedata(id)
  {
    swal({
                title: "Are You sure want to delete this?",
                //text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                      var request = {"id":id};
                      $.ajax({
                        url: 'service.php?servicename=deleteCorporate',
                        type: 'POST',
                        data: JSON.stringify(request),
                        contentType: 'application/json; charset=utf-8',
                        datatype: 'JSON',
                        async: true,
                        success: function(data)
                        {
                          var result = JSON.parse(data);
                          if(result.status == 'success')
                          {
                            getCorporate();
                            $("#toast-error").html(result.msg);
                            $("#toasterError").fadeIn();
                          }
                          else
                          {
                            $("#toast-error").html(result.msg);
                            $("#toasterError").fadeIn();
                          }
                          setTimeout(function(){
                            $("#toaster").fadeOut();
                            $("#toasterError").fadeOut();
                          }, 3000);
                        }
                      });
                    } 
              });

  }

  function inactiveoffer(oid)
{
   console.log("CuisineId"+oid);

   var offerid = {"cid":oid };

   $.ajax({
            url: 'service.php?servicename=inactive_offer',
            type: 'POST',
            datatype: 'JSON',
            async: false,
            data: JSON.stringify(offerid),
            contentType: 'application/json',
            success: function(rsca)
            {
                console.log("Cuisine Active"+JSON.stringify(rsca));

                var jrsc = JSON.parse(rsca);

                if(jrsc.status == 'success')
                {
                   window.location.href = 'corporatoffers.php';
                }  
            }

   });
}
</script>