<?php 
session_start();

include 'inc/databaseConfig.php';
//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/Casseroleadmin/index.php");
	header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Sequence List</h1>
                    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
     <div class="row">
     <div class="form-group">
                    <label class="col-md-3 control-label" for="example-select">Select Cloudkitch Kitchen</label>
                    <div class="col-md-6">
                    <select id="kitchen" name="example-select" class="form-control" size="1" onchange="getKitchenCategory();">
                    <?php
                     $query = "SELECT * FROM user WHERE ischef='1' AND status='1' ORDER BY userid ASC";
                    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                    echo '<option value="">Select kitchen</option>';
                    while($row = mysqli_fetch_assoc($result))
                    {
                        echo '<option value="'.$row['userid'].'">'.$row['name'].'</option>';
                    }   
                    ?>
                    <option value="0">Home</option>
                    </select>
                    </div>
                </div>
                <br/><br/>
                
     </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tablecuisinecate" class="table table-vcenter table-striped table-hover table-borderless">
                <thead>
                        <tr>
                        <th>Sr</th>
                        <th>Category</th>     
                        <th>Seqence</th> 
                    
                    </tr> 
                    </thead>
                    
                </table>
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-3">
                        <button type="button"  class="btn btn-effect-ripple btn-primary" onclick="updatekitchenCategory();">Update</button>
                        <!-- <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button> -->
                    </div>
                </div>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script src="js/CasseroleService.js"></script>



<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();

 
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

     
    });</script>

    <script type="text/javascript">
      
    function getKitchenCategory(ic)
    {
       

        var kitchenid = $("#kitchen").val();
        $('#loading').show();
       $.ajax({
                url: 'service.php?servicename=kitchen_category',  
                type: 'POST',
                datatype: 'JSON',
                data: JSON.stringify({"kitchenid":kitchenid}),
                async: false,
                success: function(data)
                {
                    $('#loading').hide();
                    var categories = JSON.parse(data);
                    var category = new Array();
                    for(var c=0;c<categories.cuisinetypes.length;c++)
                    {
                    category[c] = new Array();
                    category[c][0] = c+1;
                    category[c][1] = categories.cuisinetypes[c].cuisinetype;
                    category[c][2] = '<div class="category" data-id="'+ categories.cuisinetypes[c].cuisinetypeid +'"> <input type="number" id="categorysequence-'+ categories.cuisinetypes[c].cuisinetypeid +'"  value="'+categories.cuisinetypes[c].sequence+'" ></div>';
                    }  
                    $('#tablecuisinecate').dataTable({
                    "aaData": category,
                    "scrollX": true,
                    "bDestroy": true,
                    "pageLength" : 25
                    });
                }

       });
    }

    </script>

    <script type="text/javascript">
    
function updatekitchenCategory()
{
    var section_count = $(".category").length;
    var sectionarray = [];
    var sequence = "";
    $(".category").each(function() {
        var addonarray = [];
        var catid = $(this).attr('data-id');
        var temp = "#categorysequence-"+catid;
        sequence = $(temp).val();
       
        
        sectionarray.push({"catid": catid,"sequence":sequence});
    });
    catseq = JSON.stringify(sectionarray);
  
    var kitchenid = $("#kitchen").val(); 
    var pagedata = {"kitchenid":kitchenid ,"sequence":catseq };
    $.ajax({
            url: 'service.php?servicename=updateKitchenCategorySequence',  
            type: 'POST',
            datatype: 'JSON',
            data: JSON.stringify(pagedata),
            async: false,
            success: function(data)
            {
                location.reload();
            }

    });

    
}


</script>