<?php 

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['servicename'] == 'ChefPayoutList')
{
	$cpl = array();

	// $quecpd = "SELECT (SELECT name FROM user as u1 WHERE u1.userid=c.userid ) as chefname,c.userid as chefid, 
 //                co.cassordid as orderid,c.cuisinename,coi.price,c.cuisineid,(SELECT SUM(quantity) from casseroleorderitem as ci WHERE ci.cuisineid=c.cuisineid) as soldqty,
 //                (SELECT GROUP_CONCAT(cassitemid) from casseroleorderitem as cm WHERE cm.cuisineid=c.cuisineid) as itmid,
	// 			(SELECT SUM(price*quantity) FROM casseroleorderitem as cin WHERE cin.cuisineid=c.cuisineid AND co.PaymentType='OLP') as OLP,
	// 			(SELECT SUM(price*quantity) FROM casseroleorderitem as cic WHERE cic.cuisineid=c.cuisineid AND co.PaymentType='COD') as COD,
	// 			(SELECT SUM(price*quantity) FROM casseroleorderitem as cim WHERE cim.cuisineid=c.cuisineid AND co.PaymentType='MW') as MW,
	// 			coi.cassitemid as itemid,coi.chefpaid 
 //                FROM casseroleorder as co,user as u,cuisine as c,casseroleorderitem as coi
 //                WHERE c.cuisineid=coi.cuisineid and coi.cassordid=co.cassordid and 
 //                coi.cuisineid=c.cuisineid and co.userid=u.userid and coi.chefpaid='N' GROUP BY coi.cuisineid  ORDER BY co.cassordid DESC";

    $quecpd = "SELECT (SELECT name FROM user as u1 WHERE u1.userid=c.userid ) as chefname,c.userid as chefid 
                FROM casseroleorder as co,cuisine as c,casseroleorderitem as coi
                WHERE c.userid=coi.chefid and coi.cassordid=co.cassordid and coi.chefpaid='N' and coi.status != 'cancel' 
                GROUP BY coi.chefid ORDER BY co.cassordid DESC";

     $exccpd = mysqli_query($conn,$quecpd) or die(mysqli_error($conn));

     if(mysqli_num_rows($exccpd) > 0)
     {
     	$cpl['cpd'] = array();

     	while($rowcpd = mysqli_fetch_assoc($exccpd))
     	{
     		$cp = array();

     		$cp['chefname'] = $rowcpd['chefname'];

     		$cp['chefid'] = $rowcpd['chefid'];

     		//$cp['orderid'] = $rowcpd['orderid'];

     		//$cp['cuisinename'] = $rowcpd['cuisinename'];

     		//$cp['price'] = $rowcpd['price'];

     		//$cp['cuisineid'] = $rowcpd['cuisineid'];

            $quesq = "SELECT SUM(quantity) as quantity FROM casseroleorderitem as cin,casseroleorder as co
                      WHERE cin.chefid='".$rowcpd['chefid']."' AND co.cassordid=cin.cassordid AND
                      cin.chefpaid='N'";

             $excsq = mysqli_query($conn,$quesq) or die(mysqli_error($conn));
             
             $rssq = mysqli_fetch_assoc($excsq);

             if($rssq['quantity'] != null)
             {
                $cp['soldqty'] = $rssq['quantity'];
             }
             else
             {
                $cp['soldqty'] = 0;
             }

             $quedco = "SELECT count(usrwntDelivery)*10 as delivchrg FROM casseroleorderitem as
                             cin,casseroleorder as co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.cassordid=cin.cassordid AND cin.chefpaid='N' AND co.PaymentType='OLP' AND usrwntDelivery='Y'";

                         $exdco = mysqli_query($conn,$quedco) or die(mysqli_error($conn));

                         $rsdco = mysqli_fetch_assoc($exdco);

                         if($rsdco['delivchrg'] != null)
                         {
                            $dc_olp = $rsdco['delivchrg'];
                         }
                         else
                         {
                            $dc_olp = 0;
                         }

             $queolp =  "SELECT SUM(price*quantity) as OLP FROM casseroleorderitem as cin,casseroleorder
                         as co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='OLP' AND
                          co.cassordid=cin.cassordid AND cin.chefpaid='N'";

             $excolp = mysqli_query($conn,$queolp) or die(mysqli_error($conn));

             $rsolp = mysqli_fetch_assoc($excolp);

             if($rsolp['OLP'] != null)
             {
                $cp['OLP'] = $rsolp['OLP'] + $dc_olp; 
             } 
             else
             {
                $cp['OLP'] = 0; 
             } 

             $quedcc = "SELECT count(usrwntDelivery)*10 as delivchrg FROM casseroleorderitem as
                             cin,casseroleorder as co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.cassordid=cin.cassordid AND cin.chefpaid='N' AND co.PaymentType='COD' AND usrwntDelivery='Y'";

                         $exdcc = mysqli_query($conn,$quedcc) or die(mysqli_error($conn));

                         $rsdcc = mysqli_fetch_assoc($exdcc);

                         if($rsdcc['delivchrg'] != null)
                         {
                            $dc_cod = $rsdcc['delivchrg'];
                         }
                         else
                         {
                            $dc_cod = 0;
                         }

             $quecod = "SELECT SUM(price*quantity) as COD FROM casseroleorderitem as cin,casseroleorder as
                        co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='COD' AND co.cassordid=cin.cassordid AND cin.chefpaid='N'";

             $exccod = mysqli_query($conn,$quecod) or die(mysqli_error($conn));

             $rscod =  mysqli_fetch_assoc($exccod);

             if($rscod['COD'] != null)
             {
                $cp['COD'] = $rscod['COD'] + $dc_cod; 
             }
             else
             {
                $cp['COD'] = 0; 
             } 

             $quedcm = "SELECT count(usrwntDelivery)*10 as delivchrg FROM casseroleorderitem as
                             cin,casseroleorder as co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.cassordid=cin.cassordid AND cin.chefpaid='N' AND co.PaymentType='COD' AND usrwntDelivery='Y'";

                         $exdcm = mysqli_query($conn,$quedcm) or die(mysqli_error($conn));

                         $rsdcm = mysqli_fetch_assoc($exdcm);

                         if($rsdcm['delivchrg'] != null)
                         {
                            $dc_mw = $rsdcm['delivchrg'];
                         }
                         else
                         {
                            $dc_mw = 0;
                         }

             $quemtp = "SELECT SUM(price*quantity) as MW FROM casseroleorderitem as cin,casseroleorder as
                        co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='MW' AND
                        co.cassordid=cin.cassordid AND cin.chefpaid='N'";

             $excmtp = mysqli_query($conn,$quemtp) or die(mysqli_error($conn));

             $rsmtp = mysqli_fetch_assoc($excmtp);

             if($rsmtp['MW'] != null)
             {
                $cp['MW'] =  $rsmtp['MW'] + $dc_mw;
             }
             else
             {
                $cp['MW'] = 0;  
             }

             $queitm = "SELECT GROUP_CONCAT(cassitemid) as itemid FROM casseroleorderitem as
                        cin,casseroleorder as co WHERE cin.chefid='".$rowcpd['chefid']."' AND
                        co.cassordid=cin.cassordid AND cin.chefpaid='N'";

             $excitm = mysqli_query($conn,$queitm) or die(mysqli_error($conn));
             
             $rsitm = mysqli_fetch_assoc($excitm);

             if($rsitm['itemid'] != null)
             {
                 $cp['itemid'] = $rsitm['itemid'];    
             }
             else
             {
                $cp['itemid'] = 0;
             }

             $quecss = "SELECT count(cuisineid) as cuisines FROM casseroleorderitem as cin,casseroleorder as co
                        WHERE cin.chefid='".$rowcpd['chefid']."' AND co.cassordid=cin.cassordid AND cin.chefpaid='N'";

             $excss = mysqli_query($conn,$quecss) or die(mysqli_error($conn));

             $rscss = mysqli_fetch_assoc($excss);

             if($rscss['cuisines'] != null)
             {
                $cp['cuisines'] = $rscss['cuisines'];
             }
             else
             {
                $cp['cuisines'] = 0;  
             }             

             // if($rowcpd['OLP'] != null)
     		// {
     		// 	$cp['OLP'] =  $rowcpd['OLP'];
     		// }
     		// else
     		// {
     		// 	$cp['OLP'] = 0;	
     		// }

     		// if($rowcpd['COD'] != null)
     		// {
     		// 	$cp['COD'] =  $rowcpd['COD'];
     		// }
     		// else
     		// {
     		// 	$cp['COD'] = 0;	
     		// }

     		// if($rowcpd['MW'] != null)
     		// {
     		// 	$cp['MW'] =  $rowcpd['MW'];
     		// }
     		// else
     		// {
     		// 	$cp['MW'] = 0;	
     		// }

     		//$cp['itemid'] = $rowcpd['itmid'];

     		//$cp['chefpaid'] = $rowcpd['chefpaid'];

     		array_push($cpl['cpd'], $cp);
     	}

     	$cpl['status'] = 'success';
     	$cpl['msg'] = 'Data available';	
     }
     else
     {
     	$cpl['status'] = 'failure';
     	$cpl['msg'] = 'Data not available';
     }

     print_r(json_encode($cpl));
     exit;	
}

if($_GET['servicename'] == 'finalpay')
{
	//{"chefid":fcid,"cuisineid":fcuid,"paymenttype":fpmt,"OLP":folp,"COD","MTP":fmtp,"itemids":fitm,"total":ftot};

	$reqfp = file_get_contents('php://input');

	$resfp = json_decode($reqfp,true);
 
	$cfid = '';

	$cfid = $resfp['chefid'];

	$cuid = '';

	$cuid = $resfp['cuisineid'];

	$pmt = '';

	$pmt =  $resfp['paymenttype'];

	$olp = '';

	$olp = $resfp['OLP'];

	$cod = '';

	$cod = $resfp['COD'];

	$mtp = '';

	$mtp = $resfp['MTP'];

	$itids = '';

	$itids = $resfp['itemids'];

	$tot = '';

	$tot = $resfp['total'];

	$dt = date('Y-m-d');

    $invitecode = '';

    $invitecode = $resfp['invitecode'];

    $fp = array();

    if($pmt == 'CASH')
    {
        $queic = "INSERT INTO chefpayout SET chefid='".$cfid."',cuisineid='".$cuid."',total='".$tot."',paymenttype='".$pmt."',createdate='".$dt."',itemid='".$itids."',invitecode='".$invitecode."'";
        $excic = mysqli_query($conn,$queic) or die(mysqli_error($conn));    
    }
    else
    {
        if($olp != 0)
        {
            $nitmid1 = explode(',', $itids);

            $queitms = "SELECT group_concat(cassitemid) as itids FROM casseroleorderitem WHERE PaymentType='OLP' AND cassitemid IN ('".implode("','",  $nitmid1)."')";
            // echo "Q".$queitms;
            // exit;
            $excitms = mysqli_query($conn,$queitms) or die(mysqli_error($conn));
            $rsitms = mysqli_fetch_assoc($excitms);

            if(mysqli_num_rows($excitms) > 0)
            {
                 $queic = "INSERT INTO chefpayout SET chefid='".$cfid."',cuisineid='".$cuid."',total='".$olp."',paymenttype='CASH',createdate='".$dt."',itemid='".$rsitms['itids']."',invitecode='".$invitecode."'";
                $excic = mysqli_query($conn,$queic) or die(mysqli_error($conn));
            }    
        }

        if($mtp != 0)
        {
            $nitmid2 = explode(',', $itids);

            $queitmsm = "SELECT group_concat(cassitemid) as itids FROM casseroleorderitem WHERE PaymentType='MW' AND cassitemid IN ('".implode("','",  $nitmid2)."')";
            // echo "Q".$queitms;
            // exit;
            $excitmsm = mysqli_query($conn,$queitmsm) or die(mysqli_error($conn));
            $rsitmsm = mysqli_fetch_assoc($excitmsm);

            if(mysqli_num_rows($excitmsm) > 0)
            {
                 $queic = "INSERT INTO chefpayout SET chefid='".$cfid."',cuisineid='".$cuid."',total='".$mtp."',paymenttype='MTP',createdate='".$dt."',itemid='".$rsitmsm['itids']."',invitecode='".$invitecode."'";
                $excic = mysqli_query($conn,$queic) or die(mysqli_error($conn));
            }
        }    
           
    }    

	

    $cpid = '';
    $cpid = mysqli_insert_id($conn);

	if($excic)
	{
	    $nitmid = explode(',', $itids);

         foreach($nitmid as $itemid)
         {
            $updps = "UPDATE casseroleorderitem SET chefpaid='Y' WHERE cassitemid='".$itemid."'";
            $excus = mysqli_query($conn,$updps) or die(mysqli_error($conn));
         }

         if($pmt == 'MTP')
         {
            if($mtp != 0)
            {
                $updwm = "UPDATE walletmaster SET cloudkitchValue = cloudkitchValue + '".$mtp."' WHERE userid='".$cfid."' ";
                $excuwm = mysqli_query($conn,$updwm) or die(mysqli_error($conn));

                // $updtm = "UPDATE chefpayout SET total='".$mtp."' WHERE cfpid='".$cpid."'";
                // $exctm = mysqli_query($conn,$updtm) or die(mysqli_error($conn));
            }    
         }

         $quecd = "SELECT u.name as chefname,u.profilepic as chefpic,c.cuisinename FROM user as u,cuisine as c WHERE u.userid=c.userid and c.userid='".$cfid."' and c.cuisineid='".$cuid."' group by c.userid";
         $exccd = mysqli_query($conn,$quecd) or die(mysqli_error($conn));
         $rscd = mysqli_fetch_assoc($exccd);   

         if($excus)
         {
            $fp['status'] = 'success';
            $fp['msg'] = 'Successfully Paid to Chef';
            $fp['chefname'] = $rscd['chefname'];
            $fp['chefpic'] = $rscd['chefpic'];
            $fp['cuisinename'] = $rscd['cuisinename'];
            $fp['total'] = $tot;
            $fp['date'] = $dt;

         }
         else
         {
            $fp['status'] = 'failure';
            $fp['msg'] = 'Not able to update chef paid status';
         }   

	}
	else
	{
        $fp['status'] = 'failure';
        $fp['msg'] = 'Failed to payout';
	}

    print_r(json_encode($fp));
    exit;	


}

// if($_GET['servicename'] == 'finalpaynew')
// {
//     // Req Paynow{"mtppm":"","olppm":"CASH","itmary":"3014,3035","chefid":"75","cuid":"1","mtp":"","olp":"12.00","invitecode":"1213"}

//     $reqfpn = file_get_contents('php://input');

//     $resfpn = json_decode($reqfpn,true);

//     $mtppm = '';

//     $olppm = '';

//     $itmary = '';

//     $chefid = '';

//     $cuid = '';

//     $mtp = '';

//     $olp = '';

//     $invitecode = '';

//     $mtpstatus = '';

//     $olpstatus = '';

//     $dt = date('Y-m-d');

//     $ndate = date('Y-m-d H:i:s',strtotime("+270 minutes"));

//     $fp = array();

//     $mtppm = $resfpn['mtppm'];

//     $olppm = $resfpn['olppm'];

//     $itmary = $resfpn['itmary'];

//     $chefid = $resfpn['chefid'];

//     $cuid = $resfpn['cuid'];

//     $mtp = $resfpn['mtp'];

//     $olp = $resfpn['olp'];

//     $invitecode = $resfpn['invitecode'];

//     $walletid = '';

//     $quewmd = "SELECT walletid FROM walletmaster WHERE userid='".$chefid."'";
    
//     $excwmd = mysqli_query($conn,$quewmd) or die(mysqli_error($conn));
    
//     $rswmd = mysqli_fetch_assoc($excwmd);

//     $walletid = $rswmd['walletid'];

//     if($mtp != '')
//     {
//         $nitmid2 = explode(',', $itmary);

//         if($mtppm == 'MTP')
//         {
//            $mtpstatus = 'DONE'; 
//         }
//         else if($mtppm == 'CASH')
//         {
//           $mtpstatus = 'PENDING';  
//         }    

//         // $queitmsm = "SELECT group_concat(cassitemid) as itids FROM casseroleorderitem WHERE PaymentType='MW' AND cassitemid IN ('".implode("','",  $nitmid2)."')";
//         // $queitmsm = "SELECT group_concat(cassitemid) as itids FROM casseroleorderitem WHERE cassitemid IN ('".implode("','",  $nitmid2)."') AND PaymentType !='OLP'";
//         $queitmsm = "SELECT group_concat(cassitemid) as itids FROM casseroleorderitem WHERE cassitemid IN ('".implode("','",  $nitmid2)."') AND MTP !=0";
//         // echo "Q".$queitms;
//         // exit;
//         $excitmsm = mysqli_query($conn,$queitmsm) or die(mysqli_error($conn));
//         $rsitmsm = mysqli_fetch_assoc($excitmsm);

//         if(mysqli_num_rows($excitmsm) > 0)
//         {

//             $quec = "SELECT * FROM chefpayout WHERE chefid='".$chefid."' AND itemid='".$rsitmsm['itids']."' AND total='".$mtp."' AND paymenttype='".$mtppm."' AND invitecode='".$invitecode."'";
//             $exc = mysqli_query($conn,$quec) or die(mysqli_error($quec));

//             if(mysqli_num_rows($exc) == 0)
//             {
//                 $queic = "INSERT INTO chefpayout SET chefid='".$chefid."',cuisineid='".$cuid."',total='".$mtp."',paymenttype='".$mtppm."',createdate='".$dt."',itemid='".$rsitmsm['itids']."',invitecode='".$invitecode."',paidstatus='".$mtpstatus."'";
//                 $excic = mysqli_query($conn,$queic) or die(mysqli_error($conn));   
//             }    
//         }

//         if($mtppm == 'MTP')
//         {  
//             $updwm = "UPDATE walletmaster SET cloudkitchValue = cloudkitchValue + '".$mtp."' WHERE userid='".$chefid."' ";
//             $excuwm = mysqli_query($conn,$updwm) or die(mysqli_error($conn));

//             if($excuwm)
//             {
//                 $qwimw = "INSERT INTO walletitem SET walletid='".$walletid."',userid='".$chefid."',amount='".$mtp."',paymentdate='".$ndate."',paymenttype='CHEFPAYOUT',invitecode='".$invitecode."' ";

//                 $excimw = mysqli_query($conn,$qwimw) or die(mysqli_error($conn));
//             }

//         }    
//     }

//     if($olp != '')
//     {
//         $nitmid1 = explode(',', $itmary);

//         $queitms = "SELECT group_concat(cassitemid) as itids FROM casseroleorderitem WHERE PaymentType='OLP' AND cassitemid IN ('".implode("','",  $nitmid1)."')";
//         // echo "Q".$queitms;
//         // exit;
//         $excitms = mysqli_query($conn,$queitms) or die(mysqli_error($conn));
//         $rsitms = mysqli_fetch_assoc($excitms);

//         if($olppm == 'MTP')
//         {
//             $olpstatus = 'DONE';
//         }
//         else if($olppm == 'CASH')
//         {
//             $olpstatus =  'PENDING';
//         }    

//         if(mysqli_num_rows($excitms) > 0)
//         {
//             $quec = "SELECT * FROM chefpayout WHERE chefid='".$chefid."' AND itemid='".$rsitms['itids']."' AND total='".$olp."' AND paymenttype='".$olppm."' AND invitecode='".$invitecode."'";
//             $exc = mysqli_query($conn,$quec) or die(mysqli_error($quec));

//             if(mysqli_num_rows($exc) == 0)
//             {   
//                 $queic = "INSERT INTO chefpayout SET chefid='".$chefid."',cuisineid='".$cuid."',total='".$olp."',paymenttype='".$olppm."',createdate='".$dt."',itemid='".$rsitms['itids']."',invitecode='".$invitecode."',paidstatus='".$olpstatus."'";
//                 $excic = mysqli_query($conn,$queic) or die(mysqli_error($conn));
//             }    
//         }

//         if($olppm == 'MTP')
//         {
//             $updwm = "UPDATE walletmaster SET cloudkitchValue = cloudkitchValue + '".$olp."' WHERE userid='".$chefid."' ";
//             $excuwm = mysqli_query($conn,$updwm) or die(mysqli_error($conn));

//             if($excuwm)
//             {
//                 $quiom = "INSERT INTO walletitem SET walletid='".$walletid."',userid='".$chefid."',amount='".$olp."',paymentdate='".$ndate."',paymenttype='CHEFPAYOUT',invitecode='".$invitecode."'";
                
//                 $exciom = mysqli_query($conn,$quiom) or die(mysqli_error($conn));
//             }    
//         }    
//     } 

//     $cpid = '';
//     $cpid = mysqli_insert_id($conn);

//     if($excic)
//     {
//         $nitmid = explode(',', $itmary);

//          foreach($nitmid as $itemid)
//          {
//             $updps = "UPDATE casseroleorderitem SET chefpaid='Y' WHERE cassitemid='".$itemid."'";
//             $excus = mysqli_query($conn,$updps) or die(mysqli_error($conn));
//          }

//          $quecd = "SELECT u.name as chefname,u.profilepic as chefpic,c.cuisinename FROM user as u,cuisine as c WHERE u.userid=c.userid and c.userid='".$chefid."' group by c.userid";
//          $exccd = mysqli_query($conn,$quecd) or die(mysqli_error($conn));
//          $rscd = mysqli_fetch_assoc($exccd);   

//          if($excic)
//          {
//             $fp['status'] = 'success';
//             $fp['msg'] = 'Successfully Paid to Chef';
//             $fp['chefname'] = $rscd['chefname'];
//             $fp['chefpic'] = $rscd['chefpic'];
//             $fp['cuisinename'] = $rscd['cuisinename'];
//             $fp['total'] = $olp+$mtp;
//             $fp['date'] = $dt;

//          }
//          else
//          {
//             $fp['status'] = 'failure';
//             $fp['msg'] = 'Not able to update chef paid status';
//          }   

//     }
//     else
//     {
//         $fp['status'] = 'failure';
//         $fp['msg'] = 'Failed to payout';
//     }

//     print_r(json_encode($fp));
//     exit;   


// }


if($_GET['servicename'] == 'SearchByPickupDate')
{
    $reqpd = file_get_contents('php://input');

    $respd = json_decode($reqpd,true);

    $fromdate = '';

    $todate = '';

    $ft = '';

    $tt = '';

    $sql = '';

    $sql1 = '';

    $exccordn = '';

    $quecord1 = '';

    $quecordn = '';

    $invc = '';

    $invc = $respd['invitecode'];

    $fromdate = $respd['fromdate'];

    $todate  = $respd['todate'];

    $ft = $respd['fromtime'];

    $tt =  $respd['totime'];

    $cpl = array();

   $cpl['cpd'] = array();
   $cp['cpd1'] = array();

    $sbd = array();

    $quepd = "SELECT DISTINCT type FROM casseroleorderitem GROUP BY type";

    $excpd = mysqli_query($conn,$quepd) or die(mysqli_error($conn));

    if(mysqli_num_rows($excpd) > 0)
    {
        while($rowpd = mysqli_fetch_assoc($excpd))
        {
            if($rowpd['type'] == 'trending')
            {
                $exccordn = '';
                $quecordn = '';

                if($fromdate != '' && $todate == '' && $ft == '')
                {
                    $fromdate = strtotime($fromdate);

                    $ffd = '';

                    $ffd = date('Y-m-d',$fromdate);

                    $sql.= " AND DATE_FORMAT (c.time,'%Y-%m-%d') >= '".$ffd."' AND co.invitecode='".$invc."'";    
                }

                if($todate != '' && $fromdate == '' && $tt == '')
                {
                    $todate = strtotime($todate);

                    $ttd = '';

                    $ttd = date('Y-m-d',$todate);

                    $sql.= " AND DATE_FORMAT(c.time,'%Y-%m-%d') <= '".$ttd."' AND co.invitecode='".$invc."'";
                }

                if($fromdate != '' && $ft != '' && $todate == '')
                {
                    $fromdate = strtotime($fromdate);

                    $ffd = '';

                    $ffd = date('Y-m-d',$fromdate);

                    $sql.= " AND DATE_FORMAT (c.time,'%Y-%m-%d %H:%i') >= '".$ffd." ".$ft."' AND co.invitecode='".$invc."'";
                }

                if($todate != '' && $tt != '' && $fromdate == '')
                {
                    $todate = strtotime($todate);

                    $ttd = '';

                    $ttd = date('Y-m-d',$todate);
 
                    $sql.= " AND DATE_FORMAT(c.time,'%Y-%m-%d %H:%i') <= '".$ttd." ".$tt." ' AND co.invitecode='".$invc."'";
                }

                if($fromdate != '' && $todate != '' && $ft != '' && $tt != '')
                {
                    $fromdate = strtotime($fromdate);
                    $todate = strtotime($todate);

                    $ffd = '';
                    $ttd = '';

                    $ffd = date('Y-m-d',$fromdate);
                    $ttd = date('Y-m-d',$todate);

                    $sql.= " AND DATE_FORMAT(c.time,'%Y-%m-%d %H:%i') >= '".$ffd." ".$ft."' AND DATE_FORMAT(c.time,'%Y-%m-%d %H:%i') <= '".$ttd." ".$tt."' AND co.invitecode='".$invc."'";   
                }    


                if($fromdate != '' && $todate != '' && $ft == '' && $tt == '')
                {
                    $fromdate = strtotime($fromdate);
                    $todate = strtotime($todate);

                    $ffd = '';
                    $ttd = '';

                    $ffd = date('Y-m-d',$fromdate);
                    $ttd = date('Y-m-d',$todate);

                    $sql.= " AND DATE_FORMAT(c.time,'%Y-%m-%d') >= '".$ffd."' AND DATE_FORMAT(c.time,'%Y-%m-%d') <= '".$ttd."' AND co.invitecode='".$invc."'";
                } 

    

                $quecordn =  "SELECT (SELECT name FROM user as u1 WHERE u1.userid=c.userid ) as chefname,c.userid as chefid ,coi.type as type
                FROM casseroleorder as co,cuisine as c,casseroleorderitem as coi
                WHERE c.userid=coi.chefid and coi.cassordid=co.cassordid  and coi.status != 'cancel' AND coi.IsResolved='Y' ".$sql."  
                GROUP BY coi.chefid ORDER BY co.cassordid DESC";//and coi.chefpaid='N'

               //echo $quecordn;
                //exit;

               //exit;
               
                $exccordn = mysqli_query($conn,$quecordn) or die(mysqli_error($conn));

            // print_r($exccordn);
                if(mysqli_num_rows($exccordn) > 0)
                {
                 
                        $cps = array();

                       

                    while($rowcpd = mysqli_fetch_assoc($exccordn))
                    {
                         $cps['test'] = 0;
                        $cps['chefname'] = $rowcpd['chefname'];

                        $cps['chefid'] = $rowcpd['chefid'];

               

                        $quesq = "SELECT SUM(cin.quantity) as quantity FROM casseroleorderitem as cin,casseroleorder as co,cuisine as c
                      WHERE cin.chefid='".$rowcpd['chefid']."' AND co.cassordid=cin.cassordid  AND cin.chefpaid='N' and
                       c.cuisineid=cin.cuisineid AND cin.status != 'cancel' AND cin.IsResolved='Y' and cin.status != 'cancel' ".$sql;

                      // echo "Trq".$quesq;

                         $excsq = mysqli_query($conn,$quesq) or die(mysqli_error($conn));
             
                         $rssq = mysqli_fetch_assoc($excsq);

                         if($rssq['quantity'] != null)
                         {
                            $cps['soldqty'] = $rssq['quantity'];
                         }
                         else
                         {
                            $cps['soldqty'] = 0;
                         }

                          $quedco = "SELECT count(cin.usrwntDelivery)*10 as delivchrg FROM casseroleorderitem as
                             cin,casseroleorder as co,cuisine as c WHERE cin.chefid='".$rowcpd['chefid']."' AND co.cassordid=cin.cassordid AND cin.chefpaid='N' AND co.PaymentType='OLP' AND cin.usrwntDelivery='Y' AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' AND cin.IsResolved='Y' ".$sql;

                         $exdco = mysqli_query($conn,$quedco) or die(mysqli_error($conn));

                         $rsdco = mysqli_fetch_assoc($exdco);

                         if($rsdco['delivchrg'] != null)
                         {
                            $dc_olp = $rsdco['delivchrg'];
                         }
                         else
                         {
                            $dc_olp = 0;
                         }

                         // $queolp =  "SELECT SUM(cin.price*cin.quantity) as OLP FROM casseroleorderitem as cin,casseroleorder
                         //             as co,cuisine as c WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='OLP' AND
                         //              co.cassordid=cin.cassordid AND cin.chefpaid='N' AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' ".$sql;

                         $queolp =  "SELECT SUM(cin.cash) as OLP FROM casseroleorderitem as cin,casseroleorder
                                     as co,cuisine as c WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='OLP' AND
                                      co.cassordid=cin.cassordid AND cin.chefpaid='N' AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' AND cin.IsResolved='Y' ".$sql;

                                 

                         $excolp = mysqli_query($conn,$queolp) or die(mysqli_error($conn));

                         $rsolp = mysqli_fetch_assoc($excolp);

                         if($rsolp['OLP'] != null)
                         {
                            // $cps['OLP'] = $rsolp['OLP'] + $dc_olp; 
                            $cps['OLP'] = $rsolp['OLP']; 
                         } 
                         else
                         {
                            $cps['OLP'] = 0; 
                         } 

                         $quedcc = "SELECT count(cin.usrwntDelivery)*10 as delivchrg FROM casseroleorderitem as
                             cin,casseroleorder as co,cuisine as c WHERE cin.chefid='".$rowcpd['chefid']."' AND co.cassordid=cin.cassordid  
                            AND cin.chefpaid='N'  AND co.PaymentType='COD' AND cin.usrwntDelivery='Y' AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' AND cin.IsResolved='Y' ".$sql;

                         $exdcc = mysqli_query($conn,$quedcc) or die(mysqli_error($conn));

                         $rsdcc = mysqli_fetch_assoc($exdcc);

                         if($rsdcc['delivchrg'] != null)
                         {
                            $dc_cod = $rsdcc['delivchrg'];
                         }
                         else
                         {
                            $dc_cod = 0;
                         }

                         // $quecod = "SELECT SUM(cin.price*cin.quantity) as COD FROM casseroleorderitem as cin,casseroleorder as
                         //            co,cuisine as c WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='COD' AND co.cassordid=cin.cassordid AND cin.chefpaid='N' AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' ".$sql;

                         // $quecod = "SELECT SUM(cin.price*cin.quantity) as COD FROM casseroleorderitem as cin,casseroleorder as
                         //            co,cuisine as c WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='COD' AND co.cassordid=cin.cassordid AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' ".$sql;

                         $quecod = "SELECT SUM(cin.cash) as COD FROM casseroleorderitem as cin,casseroleorder as
                                    co,cuisine as c WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='COD' AND cin.chefpaid='N'
                                     AND co.cassordid=cin.cassordid AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' AND cin.IsResolved='Y' ".$sql;

                         // echo "Trcod".$quecod;           

                         $exccod = mysqli_query($conn,$quecod) or die(mysqli_error($conn));

                         $rscod =  mysqli_fetch_assoc($exccod);

                         if($rscod['COD'] != null)
                         {
                            // $cps['COD'] = $rscod['COD'] + $dc_cod; 
                            $cps['COD'] = $rscod['COD'];
                         }
                         else
                         {
                            $cps['COD'] = 0; 
                         }

                         $quedcm = "SELECT count(cin.usrwntDelivery)*10 as delivchrg FROM casseroleorderitem as
                             cin,casseroleorder as co,cuisine as c WHERE cin.chefid='".$rowcpd['chefid']."' AND 
                             co.cassordid=cin.cassordid AND cin.chefpaid='N' AND co.PaymentType='COD' AND cin.usrwntDelivery='Y' 
                             AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' AND cin.IsResolved='Y' ".$sql;

                         $exdcm = mysqli_query($conn,$quedcm) or die(mysqli_error($conn));

                         $rsdcm = mysqli_fetch_assoc($exdcm);

                         if($rsdcm['delivchrg'] != null)
                         {
                            $dc_mw = $rsdcm['delivchrg'];
                         }
                         else
                         {
                            $dc_mw = 0;
                         } 

                         // $quemtp = "SELECT SUM(cin.price*cin.quantity) as MW FROM casseroleorderitem as cin,casseroleorder as
                         //            co,cuisine as c WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='MW' AND
                         //            co.cassordid=cin.cassordid AND cin.chefpaid='N' AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' ".$sql;

                         $quemtp = "SELECT SUM(cin.MTP) as MW FROM casseroleorderitem as cin,casseroleorder as
                                    co,cuisine as c WHERE cin.chefid='".$rowcpd['chefid']."' AND
                                    co.cassordid=cin.cassordid AND cin.chefpaid='N' AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' AND cin.IsResolved='Y' ".$sql;
                         // echo "Trm".$quemtp;           

                         $excmtp = mysqli_query($conn,$quemtp) or die(mysqli_error($conn));

                         $rsmtp = mysqli_fetch_assoc($excmtp);

                         if($rsmtp['MW'] != null)
                         {
                            // $cps['MW'] =  $rsmtp['MW'] + $dc_mw;
                             $cps['MW'] =  $rsmtp['MW'];
                         }
                         else
                         {
                            $cps['MW'] = 0;  
                         }

                         $queptm = "SELECT SUM(cin.cash) as PTM FROM casseroleorderitem as cin,casseroleorder as
                                    co,cuisine as c WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='PTM' AND cin.chefpaid='N' 
                                     AND co.cassordid=cin.cassordid AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' AND cin.IsResolved='Y' ".$sql;

                     
                         $excpstm = mysqli_query($conn,$queptm) or die(mysqli_error($conn));

                         $rsptm =  mysqli_fetch_assoc($excpstm);

                         if($rsptm['PTM'] != null)
                         {
                            // $cps['COD'] = $rscod['COD'] + $dc_cod; 
                            $cps['PTM'] = $rsptm['PTM'];
                         }
                         else if($rsptm['PTM'] == NULL || $rsptm['PTM'] == '' || $rsptm['PTM'] == 'undefined')
                         {
                            // $cps['COD'] = $rscod['COD'] + $dc_cod; 
                            $cps['PTM'] = 0;
                         }
                         else
                         {
                            $cps['PTM'] = 0; 
                         }
                         $queitm = "SELECT GROUP_CONCAT(cin.cassitemid) as itemid FROM casseroleorderitem as
                                    cin,casseroleorder as co,cuisine as c WHERE cin.chefid='".$rowcpd['chefid']."' AND cin.chefpaid='N' AND
                                    co.cassordid=cin.cassordid  AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' AND cin.IsResolved='Y' ".$sql;

                         $excitm = mysqli_query($conn,$queitm) or die(mysqli_error($conn));
                         
                         $rsitm = mysqli_fetch_assoc($excitm);

                         if($rsitm['itemid'] != null)
                         {
                             $cps['itemid'] = $rsitm['itemid'];    
                         }
                         else
                         {
                            $cps['itemid'] = 0;
                         }

                        //  $quecss = "SELECT count(cin.cuisineid) as cuisines FROM casseroleorderitem as cin,casseroleorder as co,cuisine as c
                        // WHERE cin.chefid='".$rowcpd['chefid']."' AND co.cassordid=cin.cassordid AND cin.chefpaid='N' AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' ".$sql;

                         $quecss = "SELECT count(cin.cuisineid) as cuisines FROM casseroleorderitem as cin,casseroleorder as co,cuisine as c
                        WHERE cin.chefid='".$rowcpd['chefid']."' AND co.cassordid=cin.cassordid AND c.cuisineid=cin.cuisineid  AND cin.chefpaid='N' AND cin.status != 'cancel' AND cin.IsResolved='Y' ".$sql;

                         $excss = mysqli_query($conn,$quecss) or die(mysqli_error($conn));

                         $rscss = mysqli_fetch_assoc($excss);

                         if($rscss['cuisines'] != null)
                         {
                            $cps['cuisines'] = $rscss['cuisines'];
                         }
                         else
                         {
                            $cps['cuisines'] = 0;  
                         }

                        array_push($cpl['cpd'], $cps);
                    }

                    $cpl['status'] = 'success';
                    $cpl['msg'] = 'Orders Available';
                }
                else
                {
                    $cpl['status'] = 'failure';
                    $cpl['msg'] = 'Not Available';
                }  
            }

            else if($rowpd['type'] == 'onrequest')
            {
              
                $exccordn = '';
                $quecordn = '';


                if($fromdate != '' && $todate == '' && $ft == '')
                {
                    $fromdate1 = strtotime($fromdate);

                    $ffd1 = '';

                    $ffd1 = date('Y-m-d',$fromdate1);

                    $sql1.= " AND DATE_FORMAT(STR_TO_DATE(cin.pickupdate, '%d-%m-%Y'), '%Y-%m-%d') >= '".$ffd1."' AND co.invitecode='".$invc."' AND cin.IsResolved='Y'";

                        
                }

                if($fromdate != '' && $ft != '' && $todate == '')
                {
                    $fromdate1 = strtotime($fromdate);

                    $ffd1 = '';

                    $ffd1 = date('Y-m-d',$fromdate1);

                    $sql1.= " AND DATE_FORMAT(STR_TO_DATE(cin.pickupdate, '%d-%m-%Y %H:%i'), '%Y-%m-%d %H:%i') >= '".$ffd1." ".$ft."' AND co.invitecode='".$invc."' AND cin.IsResolved='Y'";                       
                }

                if($fromdate == '' && $todate != '' && $tt != '')
                {
                    $todate1 = strtotime($todate);

                    $ttd1 = '';

                    $ttd1 = date('Y-m-d',$todate1);

                    $sql1.= " AND DATE_FORMAT(STR_TO_DATE(cin.pickupdate, '%d-%m-%Y %H:%i'), '%Y-%m-%d %H:%i') <= '".$ttd1." ".$tt."' AND co.invitecode='".$invc."' AND cin.IsResolved='Y'";   
                }

                if($fromdate != '' && $todate != '' && $tt != '' && $ft != '')
                {
                   $fromdate1 = strtotime($fromdate);
                    $todate1 = strtotime($todate);

                    $ffd1 = '';
                    $ttd1 = '';

                    $ffd1 = date('Y-m-d',$fromdate1);
                    $ttd1 = date('Y-m-d',$todate1);

                   $sql1.= " AND DATE_FORMAT(STR_TO_DATE(cin.pickupdate, '%d-%m-%Y %H:%i'), '%Y-%m-%d %H:%i') >= '".$ffd1." ".$ft."'  AND DATE_FORMAT(STR_TO_DATE(cin.pickupdate, '%d-%m-%Y %H:%i'), '%Y-%m-%d %H:%i') <= '".$ttd1." ".$tt."' AND co.invitecode='".$invc."' AND cin.IsResolved='Y'"; 
                }    

                if($todate != '' && $fromdate == '' && $tt == '')
                {
                    $todate1 = strtotime($todate);

                    $ttd1 = '';

                    $ttd1 = date('Y-m-d',$todate1);

                    $sql1.= " AND DATE_FORMAT(STR_TO_DATE(cin.pickupdate, '%d-%m-%Y'), '%Y-%m-%d') <= '".$ttd1."' AND co.invitecode='".$invc."' AND cin.IsResolved='Y'";
                }

                if($fromdate != '' && $todate != '' && $ft == '' && $tt == '')
                {
                    $fromdate1 = strtotime($fromdate);
                    $todate1 = strtotime($todate);

                    $ffd1 = '';
                    $ttd1 = '';

                    $ffd1 = date('Y-m-d',$fromdate1);
                    $ttd1 = date('Y-m-d',$todate1);

                   $sql1.= " AND DATE_FORMAT(STR_TO_DATE(cin.pickupdate, '%d-%m-%Y'), '%Y-%m-%d') >= '".$ffd1."'  AND DATE_FORMAT(STR_TO_DATE(cin.pickupdate, '%d-%m-%Y'), '%Y-%m-%d') <= '".$ttd1."' AND co.invitecode='".$invc."' AND cin.IsResolved='Y'";
                }    
              

                $quecordn =  "SELECT (SELECT name FROM user as u1 WHERE u1.userid=c.userid ) as chefname,c.userid as chefid ,cin.type as type
                FROM casseroleorder as co,cuisine as c,casseroleorderitem as cin
                WHERE c.userid=cin.chefid and cin.cassordid=co.cassordid  AND cin.status != 'cancel' ".$sql1."  
                GROUP BY cin.chefid ORDER BY co.cassordid DESC";//and cin.chefpaid='N'

               
                $exccordn = mysqli_query($conn,$quecordn) or die(mysqli_error($conn));

                if(mysqli_num_rows($exccordn) > 0)
                {
        
                    while($rowcpd = mysqli_fetch_assoc($exccordn))
                    {
                        $cp = array();

                       // $cp['chefname'] = $rowcpd['chefname'];

                         $cp['chefname'] = $rowcpd['chefname'];

                        $cp['chefid'] = $rowcpd['chefid'];                     

                        $quesq = "SELECT SUM(cin.quantity) as quantity FROM casseroleorderitem as cin,casseroleorder as co
                      WHERE cin.chefid='".$rowcpd['chefid']."' AND co.cassordid=cin.cassordid AND cin.chefpaid='N' AND cin.status != 'cancel' ".$sql1;

                      // echo "onrq".$quesq;

                         $excsq = mysqli_query($conn,$quesq) or die(mysqli_error($conn));
             
                         $rssq = mysqli_fetch_assoc($excsq);

                         if($rssq['quantity'] != null)
                         {
                            $cp['soldqty'] = $rssq['quantity'];
                         }
                         else
                         {
                            $cp['soldqty'] = 0;
                         }

                         $quedco = "SELECT count(cin.usrwntDelivery)*10 as delivchrg FROM casseroleorderitem as
                             cin,casseroleorder as co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.cassordid=cin.cassordid 
                             AND cin.chefpaid='N' AND co.PaymentType='OLP' AND usrwntDelivery='Y' AND cin.status != 'cancel' ".$sql1;

                         $exdco = mysqli_query($conn,$quedco) or die(mysqli_error($conn));

                         $rsdco = mysqli_fetch_assoc($exdco);

                         if($rsdco['delivchrg'] != null)
                         {
                            $dc_olp = $rsdco['delivchrg'];
                         }
                         else
                         {
                            $dc_olp = 0;
                         }   


                         $queolp =  "SELECT SUM(cin.cash) as OLP FROM casseroleorderitem as cin,casseroleorder
                                     as co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='OLP' AND
                                      co.cassordid=cin.cassordid AND cin.chefpaid='N' AND cin.status != 'cancel' ".$sql1;

                         $excolp = mysqli_query($conn,$queolp) or die(mysqli_error($conn));

                         $rsolp = mysqli_fetch_assoc($excolp);

                         if($rsolp['OLP'] != null)
                         {
                            // $cp['OLP'] = $rsolp['OLP'] + $dc_olp; 
                            $cp['OLP'] = $rsolp['OLP']; 
                         } 
                         else
                         {
                            $cp['OLP'] = 0; 
                         } 

                         $quedcc = "SELECT count(cin.usrwntDelivery)*10 as delivchrg FROM casseroleorderitem as
                             cin,casseroleorder as co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.cassordid=cin.cassordid 
                             AND cin.chefpaid='N' AND co.PaymentType='COD' AND usrwntDelivery='Y' AND cin.status != 'cancel' ".$sql1;

                         $exdcc = mysqli_query($conn,$quedcc) or die(mysqli_error($conn));

                         $rsdcc = mysqli_fetch_assoc($exdcc);

                         if($rsdcc['delivchrg'] != null)
                         {
                            $dc_cod = $rsdcc['delivchrg'];
                         }
                         else
                         {
                            $dc_cod = 0;
                         }   

                         // $quecod = "SELECT SUM(cin.price*cin.quantity) as COD FROM casseroleorderitem as cin,casseroleorder as
                         //            co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='COD' AND co.cassordid=cin.cassordid AND cin.chefpaid='N' AND cin.status != 'cancel' ".$sql1;

                         // $quecod = "SELECT SUM(cin.price*cin.quantity) as COD FROM casseroleorderitem as cin,casseroleorder as
                         //            co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='COD' AND co.cassordid=cin.cassordid AND cin.status != 'cancel' ".$sql1;

                         $quecod = "SELECT SUM(cin.cash) as COD FROM casseroleorderitem as cin,casseroleorder as
                                    co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='COD' AND cin.chefpaid='N' AND co.cassordid=cin.cassordid AND cin.status != 'cancel' ".$sql1;

                         // echo "Onrolp".$quecod;

                         $exccod = mysqli_query($conn,$quecod) or die(mysqli_error($conn));

                         $rscod =  mysqli_fetch_assoc($exccod);

                         if($rscod['COD'] != null)
                         {
                            // $cp['COD'] = $rscod['COD'] + $dc_cod;
                            // $cp['COD'] = $rscod['COD'] + $dc_cod; 
                            $cp['COD'] = $rscod['COD']; 
                         }
                         else
                         {
                            $cp['COD'] = 0; 
                         } 

                         $quedcm = "SELECT count(cin.usrwntDelivery)*10 as delivchrg FROM casseroleorderitem as
                             cin,casseroleorder as co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.cassordid=cin.cassordid
                              AND cin.chefpaid='N' AND co.PaymentType='COD' AND usrwntDelivery='Y' AND cin.status != 'cancel' ".$sql1;

                         $exdcm = mysqli_query($conn,$quedcm) or die(mysqli_error($conn));

                         $rsdcm = mysqli_fetch_assoc($exdcm);

                         if($rsdcm['delivchrg'] != null)
                         {
                            $dc_mw = $rsdcm['delivchrg'];
                         }
                         else
                         {
                            $dc_mw = 0;
                         }

                         // $quemtp = "SELECT SUM(cin.price*cin.quantity) as MW FROM casseroleorderitem as cin,casseroleorder as
                         //            co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='MW' AND
                         //            co.cassordid=cin.cassordid AND cin.chefpaid='N' AND cin.status != 'cancel' ".$sql1;

                         // $quemtp = "SELECT SUM(cin.MTP) as MW FROM casseroleorderitem as cin,casseroleorder as
                         //            co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='MW' AND
                         //            co.cassordid=cin.cassordid AND cin.chefpaid='N' AND cin.status != 'cancel' ".$sql1;



                         $queptm = "SELECT SUM(cin.cash) as PTM FROM casseroleorderitem as cin,casseroleorder as
                                    co,cuisine as c WHERE cin.chefid='".$rowcpd['chefid']."' AND co.PaymentType='PTM' and cin.type='onrequest'
                                     AND co.cassordid=cin.cassordid AND cin.chefpaid='N' AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' AND cin.IsResolved='Y' ".$sql;
                        
                         $excpstm = mysqli_query($conn,$queptm) or die(mysqli_error($conn));

                         $rsptm =  mysqli_fetch_assoc($excpstm);

                         if($rsptm['PTM'] != null)
                         {
                            // $cps['COD'] = $rscod['COD'] + $dc_cod; 
                            $cp['PTM'] = $rsptm['PTM'];
                         }
                         else if($rsptm['PTM'] == NULL || $rsptm['PTM'] == '' || $rsptm['PTM'] == 'undefined')
                         {
                            // $cps['COD'] = $rscod['COD'] + $dc_cod; 
                            $cp['PTM'] = 0;
                         }
                         else
                         {
                            $cp['PTM'] = 0; 
                        }

                         $quemtp = "SELECT SUM(cin.MTP) as MW FROM casseroleorderitem as cin,casseroleorder as
                                    co WHERE cin.chefid='".$rowcpd['chefid']."' AND
                                    co.cassordid=cin.cassordid AND cin.chefpaid='N' AND cin.status != 'cancel' ".$sql1;

                         $excmtp = mysqli_query($conn,$quemtp) or die(mysqli_error($conn));

                         $rsmtp = mysqli_fetch_assoc($excmtp);

                         if($rsmtp['MW'] != null)
                         {
                            // $cp['MW'] =  $rsmtp['MW'] + $dc_mw;
                            $cp['MW'] =  $rsmtp['MW'];
                         }
                         else
                         {
                            $cp['MW'] = 0;  
                         }

                         $queitm = "SELECT GROUP_CONCAT(cin.cassitemid) as itemid FROM casseroleorderitem as
                                    cin,casseroleorder as co WHERE cin.chefid='".$rowcpd['chefid']."' AND
                                    co.cassordid=cin.cassordid AND cin.chefpaid='N' AND cin.status != 'cancel' ".$sql1;

                         $excitm = mysqli_query($conn,$queitm) or die(mysqli_error($conn));
                         
                         $rsitm = mysqli_fetch_assoc($excitm);

                         if($rsitm['itemid'] != null)
                         {
                             $cp['itemid'] = $rsitm['itemid'];    
                         }
                         else
                         {
                            $cp['itemid'] = 0;
                         }

                        // $quecss = "SELECT count(cin.cuisineid) as cuisines FROM casseroleorderitem as cin,casseroleorder as co WHERE cin.chefid='".$rowcpd['chefid']."' AND co.cassordid=cin.cassordid AND cin.chefpaid='N' AND cin.status != 'cancel' ".$sql1;

                         $quecss = "SELECT count(cin.cuisineid) as cuisines FROM 
                         casseroleorderitem as cin,casseroleorder as co WHERE cin.chefid='".$rowcpd['chefid']."' AND cin.chefpaid='N' AND co.cassordid=cin.cassordid AND cin.status != 'cancel' ".$sql1;

                         $excss = mysqli_query($conn,$quecss) or die(mysqli_error($conn));

                         $rscss = mysqli_fetch_assoc($excss);

                         if($rscss['cuisines'] != null)
                         {
                            $cp['cuisines'] = $rscss['cuisines'];
                         }
                         else
                         {
                            $cp['cuisines'] = 0;  
                         }
                         

                        array_push($cpl['cpd'], $cp);
                    }

                        $cpl['status'] = 'success';
                        $cpl['msg'] = 'Orders Available';
                }
                else
                {
                    $cpl['status'] = 'failure';
                    $cpl['msg'] = 'Not Available';
                } 

               
            } 

        }  
    }   
    else
    {
       $cpl['status'] = 'failure';
        $cpl['msg'] = 'Data not available';
    }  

    print_r(json_encode($cpl,JSON_UNESCAPED_SLASHES));
    exit;        

}
if($_GET['servicename'] == 'ShowCuisineName')
{
    $reqpd = file_get_contents('php://input');

    $respd = json_decode($reqpd,true);

     $sql = '';

    $chefid = $respd['chefid'];

    $invc = $respd['invitecode'];

    $fromdate = $respd['fromdate'];

    $todate  = $respd['todate'];

    $ft = $respd['fromtime'];

    $tt =  $respd['totime'];

    $cpl = array();    

    $cpl['cuisinelist'] = array();

    $sbd = array();

    $quepd = "SELECT DISTINCT type FROM casseroleorderitem GROUP BY type";

    $excpd = mysqli_query($conn,$quepd) or die(mysqli_error($conn));

    if(mysqli_num_rows($excpd) > 0)
    {
        while($rowpd = mysqli_fetch_assoc($excpd))
        {
            if($rowpd['type'] == 'trending')
            {
                $exccordn = '';
                $quecordn = '';

                if($fromdate != '' && $todate != '' && $ft == '' && $tt == '')
                {
                    $fromdate = strtotime($fromdate);
                    $todate = strtotime($todate);

                    $ffd = '';
                    $ttd = '';

                    $ffd = date('Y-m-d',$fromdate);
                    $ttd = date('Y-m-d',$todate);

                    $sql.= " AND DATE_FORMAT(c.time,'%Y-%m-%d') >= '".$ffd."' AND DATE_FORMAT(c.time,'%Y-%m-%d') <= '".$ttd."' AND co.invitecode='".$invc."'";
                } 

                $quecordn = "SELECT  c.cuisineid,c.cuisinename FROM casseroleorderitem as cin,casseroleorder as co,cuisine as c
                        WHERE cin.chefid= ".$chefid." AND co.cassordid=cin.cassordid AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' AND cin.IsResolved='Y' 
                        ".$sql;

                $exccordn = mysqli_query($conn,$quecordn) or die(mysqli_error($conn));

                 if(mysqli_num_rows($exccordn) > 0)
                {
        
                    while($rowcpd = mysqli_fetch_assoc($exccordn))
                    {
                        $cp = array();

                        $cp['cuisineid'] = $rowcpd['cuisineid'];

                        $cp['cuisinename'] = $rowcpd['cuisinename'];

                        array_push($cpl['cuisinelist'], $cp);
                    } 

                    $cpl['status'] = 'success';
                    $cpl['msg'] = 'Cuisine list Available';

                }
                else
                {
                    $cpl['status'] = 'failure';
                    $cpl['msg'] = 'Not Available';
                }  

            }
            elseif($rowpd['type'] == 'onrequest')
            {
                $exccordn = '';
                $quecordn = '';

                if($fromdate != '' && $todate != '' && $ft == '' && $tt == '')
                {
                    $fromdate = strtotime($fromdate);
                    $todate = strtotime($todate);

                    $ffd = '';
                    $ttd = '';

                    $ffd = date('Y-m-d',$fromdate);
                    $ttd = date('Y-m-d',$todate);

                    $sql.= " AND DATE_FORMAT(c.time,'%Y-%m-%d') >= '".$ffd."' AND DATE_FORMAT(c.time,'%Y-%m-%d') <= '".$ttd."' AND co.invitecode='".$invc."'";
                } 

                $quecordn = "SELECT  c.cuisineid,c.cuisinename,cin.quantity FROM casseroleorderitem as cin,casseroleorder as co,cuisine as c
                        WHERE cin.chefid= ".$chefid." AND co.cassordid=cin.cassordid AND c.cuisineid=cin.cuisineid AND cin.status != 'cancel' AND cin.IsResolved='Y' 
                        ".$sql;
              
                 $exccordn = mysqli_query($conn,$quecordn) or die(mysqli_error($conn));

                 if(mysqli_num_rows($exccordn) > 0)
                {
        
                    while($rowcpd = mysqli_fetch_assoc($exccordn))
                    {
                        $cp = array();

                        $cp['cuisineid'] = $rowcpd['cuisineid'];

                        $cp['cuisinename'] = $rowcpd['cuisinename'];
                        
                        $cp['quantity'] = $rowcpd['quantity'];

                        array_push($cpl['cuisinelist'], $cp);
                    } 

                    $cpl['status'] = 'success';
                    $cpl['msg'] = 'Cuisine list Available';

                }
                else
                {
                    $cpl['status'] = 'failure';
                    $cpl['msg'] = 'Not Available';
                }  

           }
        } 
         $cpl['status'] = 'success';
         $cpl['msg'] = 'Cuisine list Available'; 
    }   
    else
    {
       $cpl['status'] = 'failure';
        $cpl['msg'] = 'Data not available';
    }  

    print_r(json_encode($cpl,JSON_UNESCAPED_SLASHES));
    exit;





}
if($_GET['servicename'] == 'finalpaynew')
{
    // Req Paynow{"mtppm":"","olppm":"CASH","itmary":"3014,3035","chefid":"75","cuid":"1","mtp":"","olp":"12.00","invitecode":"1213"}

    $reqfpn = file_get_contents('php://input');

    $resfpn = json_decode($reqfpn,true);

    $mtppm = '';

    $olppm = '';

    $itmary = '';

    $chefid = '';

    $cuid = '';

    $mtp = '';

    $olp = '';

    $invitecode = '';

    $mtpstatus = '';

    $olpstatus = '';

    $dt = date('Y-m-d');

    $ndate = date('Y-m-d H:i:s',strtotime("+270 minutes"));

    $fp = array();

    $mtppm = $resfpn['mtppm'];

    $olppm = $resfpn['olppm'];

    $ptmpm = $resfpn['ptmpm'];

    $itmary = $resfpn['itmary'];

    $chefid = $resfpn['chefid'];

    $cuid = $resfpn['cuid'];

    $mtp = $resfpn['mtp'];

    $olp = $resfpn['olp'];

    $ptm = $resfpn['ptm'];

    $cod = $resfpn['cod'];

    $invitecode = $resfpn['invitecode'];

    $walletid = '';

    $quewmd = "SELECT walletid FROM walletmaster WHERE userid='".$chefid."'";
    
    $excwmd = mysqli_query($conn,$quewmd) or die(mysqli_error($conn));
    
    $rswmd = mysqli_fetch_assoc($excwmd);

    $walletid = $rswmd['walletid'];

    if($mtp != '')
    {
        $nitmid2 = explode(',', $itmary);

        if($mtppm == 'MTP')
        {
           $mtpstatus = 'DONE'; 
        }
        else if($mtppm == 'CASH')
        {
          $mtpstatus = 'PENDING';  
        }    

        // $queitmsm = "SELECT group_concat(cassitemid) as itids FROM casseroleorderitem WHERE PaymentType='MW' AND cassitemid IN ('".implode("','",  $nitmid2)."')";
        // $queitmsm = "SELECT group_concat(cassitemid) as itids FROM casseroleorderitem WHERE cassitemid IN ('".implode("','",  $nitmid2)."') AND PaymentType !='OLP'";
        $queitmsm = "SELECT group_concat(cassitemid) as itids FROM casseroleorderitem WHERE cassitemid IN ('".implode("','",  $nitmid2)."') AND MTP !=0";
        // echo "Q".$queitms;
        // exit;
        $excitmsm = mysqli_query($conn,$queitmsm) or die(mysqli_error($conn));
        $rsitmsm = mysqli_fetch_assoc($excitmsm);

        if(mysqli_num_rows($excitmsm) > 0)
        {

            $quec = "SELECT * FROM chefpayout WHERE chefid='".$chefid."' AND itemid='".$rsitmsm['itids']."' AND total='".$mtp."' AND paymenttype='".$mtppm."' AND invitecode='".$invitecode."'";
            $exc = mysqli_query($conn,$quec) or die(mysqli_error($quec));

            if(mysqli_num_rows($exc) == 0)
            {
                $queic = "INSERT INTO chefpayout SET chefid='".$chefid."',cuisineid='".$cuid."',total='".$mtp."',paymentmode='MTP',paymenttype='".$mtppm."',createdate='".$dt."',itemid='".$rsitmsm['itids']."',invitecode='".$invitecode."',paidstatus='".$mtpstatus."'";
                $excic = mysqli_query($conn,$queic) or die(mysqli_error($conn));   
            }    
        }

        if($mtppm == 'MTP')
        {  
            $updwm = "UPDATE walletmaster SET cloudkitchValue = cloudkitchValue + '".$mtp."' WHERE userid='".$chefid."' ";
            $excuwm = mysqli_query($conn,$updwm) or die(mysqli_error($conn));

            if($excuwm)
            {
                $qwimw = "INSERT INTO walletitem SET walletid='".$walletid."',userid='".$chefid."',amount='".$mtp."',paymentdate='".$ndate."',paymenttype='CHEFPAYOUT',invitecode='".$invitecode."' ";

                $excimw = mysqli_query($conn,$qwimw) or die(mysqli_error($conn));
            }

        }    
    }

    if($olp != '')
    {
        $nitmid1 = explode(',', $itmary);

        $queitms = "SELECT group_concat(cassitemid) as itids FROM casseroleorderitem WHERE PaymentType='OLP' AND cassitemid IN ('".implode("','",  $nitmid1)."')";
        // echo "Q".$queitms;
        // exit;
        $excitms = mysqli_query($conn,$queitms) or die(mysqli_error($conn));
        $rsitms = mysqli_fetch_assoc($excitms);

        if($olppm == 'MTP')
        {
            $olpstatus = 'DONE';
        }
        else if($olppm == 'CASH')
        {
            $olpstatus =  'PENDING';
        }    

        if(mysqli_num_rows($excitms) > 0)
        {
            $quec = "SELECT * FROM chefpayout WHERE chefid='".$chefid."' AND itemid='".$rsitms['itids']."' AND total='".$olp."' AND paymenttype='".$olppm."' AND invitecode='".$invitecode."'";
            $exc = mysqli_query($conn,$quec) or die(mysqli_error($quec));

            if(mysqli_num_rows($exc) == 0)
            {   
                $queic = "INSERT INTO chefpayout SET chefid='".$chefid."',cuisineid='".$cuid."',paymentmode='OLP',total='".$olp."',paymenttype='".$olppm."',createdate='".$dt."',itemid='".$rsitms['itids']."',invitecode='".$invitecode."',paidstatus='".$olpstatus."'";
                $excic = mysqli_query($conn,$queic) or die(mysqli_error($conn));
            }    
        }

        if($olppm == 'MTP')
        {
            $updwm = "UPDATE walletmaster SET cloudkitchValue = cloudkitchValue + '".$olp."' WHERE userid='".$chefid."' ";
            $excuwm = mysqli_query($conn,$updwm) or die(mysqli_error($conn));

            if($excuwm)
            {
                $quiom = "INSERT INTO walletitem SET walletid='".$walletid."',userid='".$chefid."',amount='".$olp."',paymentdate='".$ndate."',paymenttype='CHEFPAYOUT',invitecode='".$invitecode."'";
                
                $exciom = mysqli_query($conn,$quiom) or die(mysqli_error($conn));
            }    
        }    
    } 

     if($ptm != '')
    {
        $nitmid3 = explode(',', $itmary);

        $queitms = "SELECT group_concat(cassitemid) as itids FROM casseroleorderitem WHERE PaymentType='PTM' AND cassitemid IN ('".implode("','",  $nitmid3)."')";
        // echo "Q".$queitms;
        // exit;
        $excitms = mysqli_query($conn,$queitms) or die(mysqli_error($conn));
        $rsitms = mysqli_fetch_assoc($excitms);

       
        if($ptmpm == 'CASH')
        {
            $ptmstatus =  'PENDING';
        }
      /*   if($olppm == 'MTP')
        {
            $olpstatus = 'DONE';
        }
        else if($olppm == 'CASH')
        {
            $olpstatus =  'PENDING';
        }      
*/
        if(mysqli_num_rows($excitms) > 0)
        {
            $quec = "SELECT * FROM chefpayout WHERE chefid='".$chefid."' AND itemid='".$rsitms['itids']."' AND total='".$ptm."' AND paymenttype='".$ptmpm."' AND invitecode='".$invitecode."'";
            $exc = mysqli_query($conn,$quec) or die(mysqli_error($quec));

            if(mysqli_num_rows($exc) == 0)
            {   
                $queic = "INSERT INTO chefpayout SET chefid='".$chefid."',cuisineid='".$cuid."',paymentmode='PTM',total='".$ptm."',paymenttype='".$ptmpm."',createdate='".$dt."',itemid='".$rsitms['itids']."',invitecode='".$invitecode."',paidstatus='".$ptmstatus."'";
                $excic = mysqli_query($conn,$queic) or die(mysqli_error($conn));
            }    
        }
          
    } 
    $excic = "";
     if($cod != '')
    {
        $nitmid3 = explode(',', $itmary);

        $queitms = "SELECT group_concat(cassitemid) as itids FROM casseroleorderitem WHERE PaymentType='COD' AND cassitemid IN ('".implode("','",  $nitmid3)."')";
        // echo "Q".$queitms;
        // exit;
        $excitms = mysqli_query($conn,$queitms) or die(mysqli_error($conn));
        $rsitms = mysqli_fetch_assoc($excitms);

        if(mysqli_num_rows($excitms) > 0)
        {
            $quec = "SELECT * FROM chefpayout WHERE chefid='".$chefid."' AND itemid='".$rsitms['itids']."' AND total='".$cod."' AND paymenttype='CASH' AND invitecode='".$invitecode."'";
            $exc = mysqli_query($conn,$quec) or die(mysqli_error($quec));

            if(mysqli_num_rows($exc) == 0)
            {   
                $queic = "INSERT INTO chefpayout SET chefid='".$chefid."',cuisineid='".$cuid."',paymentmode='COD',total='".$cod."',paymenttype='CASH',createdate='".$dt."',itemid='".$rsitms['itids']."',invitecode='".$invitecode."',paidstatus='DONE'";
                $excic = mysqli_query($conn,$queic) or die(mysqli_error($conn));
            }    
        }
          
    } 

    $cpid = '';
    $cpid = mysqli_insert_id($conn);

    if($cpid)
    {
        $nitmid = explode(',', $itmary);

         foreach($nitmid as $itemid)
         {
            $updps = "UPDATE casseroleorderitem SET chefpaid='Y' WHERE cassitemid='".$itemid."'";
            $excus = mysqli_query($conn,$updps) or die(mysqli_error($conn));
         }

         $quecd = "SELECT u.name as chefname,u.profilepic as chefpic,c.cuisinename FROM user as u,cuisine as c WHERE u.userid=c.userid and c.userid='".$chefid."' group by c.userid";
         $exccd = mysqli_query($conn,$quecd) or die(mysqli_error($conn));
         $rscd = mysqli_fetch_assoc($exccd);   

         if($excic)
         {
            $fp['status'] = 'success';
            $fp['msg'] = 'Successfully Paid to Chef';
            $fp['chefname'] = $rscd['chefname'];
            $fp['chefpic'] = $rscd['chefpic'];
            $fp['cuisinename'] = $rscd['cuisinename'];
            $fp['total'] = $olp+$mtp+$ptm;
            $fp['date'] = $dt;

         }
         else
         {
            $fp['status'] = 'failure';
            $fp['msg'] = 'Not able to update chef paid status';
         }   

    }
    else
    {
        $fp['status'] = 'failure';
        $fp['msg'] = 'Failed to payout';
    }

    print_r(json_encode($fp));
    exit;   


}


?>