<?php

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['servicename'] == 'getordersnew')
{

    $reqno = file_get_contents('php://input');

    $resno = json_decode($reqno,true);

    $invc = '';

    $invc = $resno['invitecode'];

    $quiol = "SELECT co.cassordid as orderid,co.total,u.name as customer,u.phone,u.address,co.orderdate,co.invitecode,co.paymentType,co.transId,co.actualamount,co.discountamount,co.couponapplied,(select couponcode FROM coupon WHERE couponid=co.couponid) as couponcode FROM casseroleorder as co,user as u WHERE co.userid=u.userid and co.invitecode='".$invc."' ORDER BY co.cassordid DESC";

    $excol = mysqli_query($conn,$quiol) or die(mysqli_error($conn));

    $ol = array();

    if(mysqli_num_rows($excol) > 0)
    {
        $ol['ords'] = array();

        while($rowol = mysqli_fetch_assoc($excol))
        {
            $lo = array();

            $lo['orderid'] = $rowol['orderid'];
            $lo['total'] = $rowol['total'];
            $lo['customer'] = $rowol['customer'];
            $lo['orderdate'] = $rowol['orderdate'];
            $lo['invitecode'] = $rowol['invitecode'];
            $lo['paymenttype'] = $rowol['paymentType'];
            $lo['transactionid'] = $rowol['transId'];
            $lo['actualamount'] = $rowol['actualamount'];
            $lo['discountamount'] = $rowol['discountamount'];
            $lo['couponapplied'] = $rowol['couponapplied'];
			
			$quect = "select DISTINCT cuisinetype from casseroleorder as co,casseroleorderitem as coi,cuisine as c,user as u WHERE co.cassordid=coi.cassordid and coi.cuisineid=c.cuisineid and c.userid=u.userid and coi.cassordid='".$rowol['orderid']."'";
			$excct = mysqli_query($conn,$quect) or die(mysqli_error($conn));
			
			$ct = array();
			
			while($rowct = mysqli_fetch_assoc($excct))
			{
				array_push($ct,$rowct['cuisinetype']);
			}

			$quegoi = "select coi.cassordid,c.cuisinename,coi.price,coi.quantity,coi.type,coi.status,coi.code,coi.closedate,u.name from casseroleorder as co,casseroleorderitem as coi,cuisine as c,user as u WHERE co.cassordid=coi.cassordid and coi.cuisineid=c.cuisineid and c.userid=u.userid and coi.cassordid='".$rowol['orderid']."'";

    		$excgoi = mysqli_query($conn,$quegoi) or die(mysqli_error($conn));	

    		$cui = array();

    		while($rowgoi = mysqli_fetch_assoc($excgoi))
    		{
    			$c = "<b>Cuisine Name : </b>".$rowgoi['cuisinename']."<b> Chef Name : </b>".$rowgoi['name']."<b> Price : </b>".$rowgoi['price']."<b> Quantity : </b>".$rowgoi['quantity']."<b> Type : </b>".$rowgoi['type']."<b> Status : </b>".$rowgoi['status']."<b> Code : <br>".$rowgoi['code']."<b> Close Date : </b>".$rowgoi['closedate']."<br>";

    			array_push($cui, $c);
    		}


    		$lo['orderd'] = implode(',', $cui);

			$lo['ctypes'] = implode(',',$ct);	
			
            if($rowol['couponcode'] == null)
            {
                $lo['couponcode'] = 'NA'; 
            }
            else
            {
                 $lo['couponcode'] = $rowol['couponcode'];
            }
			$lo['cph'] = $rowol['phone'];	
			$lo['add'] = $rowol['address'];

            array_push($ol['ords'], $lo);  
           
        } 

        $ol['status'] = 'success';
        $ol['msg'] = 'Orders Available';   
    }
    else
    {
        $ol['status'] = 'failure';
        $ol['msg'] = 'Orders Not Available';

    }

    print_r(json_encode($ol));
    exit;    
}

?>