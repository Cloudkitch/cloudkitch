<?php 

session_start();
include 'inc/databaseConfig.php';
include 'inc/config.php'; 
if(!isset($_SESSION['info']['user']))
{
	header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
include 'inc/template_start.php';
include 'inc/page_head.php';
$userid = $_SESSION['info']['id'];
?>
<link href="css/summernote.css" rel="stylesheet">
<style type="text/css">
    .img-close{
    position: absolute;
    cursor: pointer;
}
</style>
<div id="page-content">
  <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
  <img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1>Add Cafeteria</h1>
    </div>
</div>
</div>
</div>
<div class="row">
    <div class="col-sm-10 col-md-12 col-lg-12">
        <div class="block">
            <form id="createcuisines" name="createcuisines" class="form-horizontal form-bordered">
             <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinesname">Cuisines name<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="text" id="val_cuisinesname" name="val_cuisinesname" class="form-control" placeholder="">
                </div>
            </div>
             <div class="form-group">
                <label class="col-md-3 control-label" for="image">Cuisines Images<span class="text-danger"></span></label>
                <div class="col-md-6">
                    <input type="hidden" id="base64" name="base64">
                    <input type="file" id="image" name="image" onchange="loadImageFile(this)" multiple>
                    <div id="cuisinesImg"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinescategory">Menu Category<span class="text-danger">*</span></label>
                <div class="col-md-6">
                <input type="hidden" name="cuisinetypename" id="cuisinetypename">
                    <select class="form-control" id="val_cuisinescategory" name="val_cuisinescategory">
                    <?php
                    $query = "SELECT * FROM cuisinetypemaster WHERE page_type='2' and FIND_IN_SET($userid, kitchensid) ORDER BY cuisinetype ASC";
                    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                    echo '<option value="">Select Cuisines Type</option>';
                    while($row = mysqli_fetch_assoc($result))
                    {
                        echo '<option value="'.$row['cuisinetypeid'].'">'.$row['cuisinetype'].'</option>';
                    }   
                    ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_homepagecat">Home Page Category<span class="text-danger">*</span></label>
                <div class="col-md-6">
                
                    <select class="form-control" id="val_homepagecat" name="val_homepagecat">
                    <?php
                    $query = "SELECT * FROM cuisinetypemaster WHERE page_type='6' ORDER BY cuisinetype ASC";
                    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                    echo '<option value="">Select Home Page Category</option>';
                    while($row = mysqli_fetch_assoc($result))
                    {
                        echo '<option value="'.$row['cuisinetypeid'].'">'.$row['cuisinetype'].'</option>';
                    }   
                    ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinesveg">Category Type<span class="text-danger">*</span></label>
                <div class="col-md-6">
                
                  <select class="form-control selectpicker" id="category_type" data-live-search="true" multiple="multiple" name="val_category_type[]">
                        <!-- <option value="" selected>Choose Category Type</option> -->
                        
                    <?php
                        $query = "SELECT * FROM categorytypemaster ORDER BY categorytype ASC";
                        $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                        while($row = mysqli_fetch_assoc($result))
                        {
                            echo '<option value="'.$row['categorytype'].'">'.$row['categorytype'].'</option>';
                        }   
                    ?> 
                    </select>
                  
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="category_type">Corporate<span class="text-danger">*</span></label>
                <div class="col-md-6">
                
                  <select class="form-control selectpicker" id="corporate_type" data-live-search="true" multiple="multiple" name="val_corporate_type[]">
                        <!-- <option value="" selected>Choose Category Type</option> -->
                        
                    <?php
                        $query = "SELECT * FROM corporate WHERE status = '1' ORDER BY id ASC";
                        $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                        while($row = mysqli_fetch_assoc($result))
                        {
                            echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
                        }   
                    ?> 
                    </select>
                   
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinesserves">Serves<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="number" id="val_cuisinesserves" name="val_cuisinesserves" class="form-control" min="1" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinesprice">Price<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="number" id="val_cuisinesprice" name="val_cuisinesprice" class="form-control" min="1" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinesdescription">Description<span class="text-danger"></span></label>
                <div class="col-md-6">
                    <input type="hidden" id="description" name="description">
                    <textarea id="val_cuisinesdescription" name="val_cuisinesdescription" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinesstatus">Status<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <select class="form-control" id="val_cuisinesstatus" name="val_cuisinesstatus">
                        <option value="1">In Stock</option>
                        <option value="0">Out of Stock</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinestag">Tag</label>
                <div class="col-md-6">
                    <input type="text" id="val_cuisinestag" name="val_cuisinestag" class="form-control" placeholder="" rows="3" data-role="tagsinput">
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-8 col-md-offset-3">
                    <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button>
                    <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
<!-- END Page Content -->
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Load and execute javascript code used only in this page -->
<script src="js/CasseroleService.js"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/summernote.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
<!-- <script>
    $(function() { FormsValidation.init();
        invitecodedetail();
    });
</script> -->

    <script type="text/javascript">
        $(document).ready(function() {
            $("#loading").hide();
            // $('#val_cuisinesdescription').summernote({
            //     height: 200 ,
            //     minHeight: null, 
            //     maxHeight: null,
            //     toolbar: [
            //     ['style', ['style']],
            //     ['font', ['bold', 'underline', 'clear']],
            //     ['fontname', ['fontname']],
            //     ['color', ['color']],
            //     ['para', ['ul', 'ol', 'paragraph']],
            //     ['table', ['table']],
            //     ['view', ['fullscreen', 'codeview', 'help']],
            //     ],
            // });
            // $('.selectpicker').selectpicker();
        });
        function previewImage(input) {
            if (input.files) {
                var filesAmount = input.files.length;
                for (var i = 0; i < filesAmount; i++)   {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        var src = event.target.result;
                        $("#cuisinesImg").append('<div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;padding-left:5px;" onclick = "imageClose(this)">X</div><img  src="'+src+'" style="height:100px;margin-right:5px;margin-top:5px;" class="img-thumbnail cuisine-img"></div>');
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

        var loadImageFile = function (input) {
            var filterType = /^(?:image\/jpeg|image\/jpeg|image\/jpeg|image\/png|image\/x\-icon)$/i;
        
            if (input.files) {
                if (input.files.length === 0) { 
                    return; 
                }
                var filesAmount = input.files.length;
                for (var i = 0; i < filesAmount; i++)   {
                    
                    if (!filterType.test(input.files[i].type)) {
                        alert("Please select a valid image."); 
                        return;
                    }
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL(input.files[i]);

                    fileReader.onload = function (event) {
                    var image = new Image();
                    var type = 'image/jpeg';
                    var  quality = 0.30;
                    image.onload=function(){
                        var canvas=document.createElement("canvas");
                        var context=canvas.getContext("2d");
                        canvas.width=image.width/1;
                        canvas.height=image.height/1;
                        context.drawImage(image,0,0);
                        var imgsrc =  canvas.toDataURL( type, quality );
                        $("#cuisinesImg").append('<div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;padding-left:5px;" onclick = "imageClose(this)">X</div><img  src="'+imgsrc +'" style="height:100px;margin-right:5px;margin-top:5px;" class="img-thumbnail cuisine-img"></div>');
                        
                    }
                    image.src=event.target.result;
                    };

                }
            }
        }

        function imageClose(a)
        {
            $(a).parent().remove();
        }
    $("form[name='createcuisines']").validate({
        rules: {
                val_cuisinesname:"required",
                val_cuisinescategory:"required",
                val_cuisinesserves:"required",
                val_cuisinesprice:"required",
                val_cuisinesstatus:"required",
                val_category_type:"required",
                val_corporate_type:"required"
            },
        submitHandler: function(form) {
            $(this).find('button[type=submit]').prop('disabled', true);
            $(".preloader").show();
            var images = "";
            $( ".cuisine-img" ).each(function() {
                var image = $(this).attr("src");
                images += "###"+image;
            });
            $("#base64").val(images);
            var desc = $('#val_cuisinesdescription').val();                    // $('#val_cuisinesdescription').summernote('code');
            $("#description").val(desc);
            var cuisinename = $( "#val_cuisinescategory option:selected" ).text();
            $("#cuisinetypename").val(cuisinename);               
            var data = new FormData($('#createcuisines')[0]);
            $.ajax({
                url: 'service.php?servicename=addcafeteria',
                type: 'POST',
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                data: data, 
                success: function(data)
                {
                     $(".preloader").hide();
                     var result = JSON.parse(data);
                     if(result.status == 'success')
                     {
                        window.location.href = 'cafeterialist.php';
                     }
                     else
                     {
                        $("#toast-error").html(result.msg);
                        $("#toasterError").fadeIn();
                     }
                      setTimeout(function(){
                        $("#toasterError").fadeOut();
                  }, 3000);
                }
            });
        }
    });
    </script>
    <?php include 'inc/template_end.php'; ?>
