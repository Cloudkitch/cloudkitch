<?php 

session_start();
include 'inc/databaseConfig.php';
include 'inc/config.php'; 
if(!isset($_SESSION['info']['user']))
{
	header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
include 'inc/template_start.php';
include 'inc/page_head.php';
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<style type="text/css">
    .working-days .custom-checkbox
    {
        display: inline-block;
        margin-left: 10px;
    }
</style>
<div id="page-content">
  <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
  <img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1>Edit Corporate</h1>
    </div>
</div>
</div>
</div>
<div class="row">
    <div class="col-sm-10 col-md-12 col-lg-12">
        <div class="block">
            <form id="createcorporate" name="createcorporate" class="form-horizontal form-bordered">
                <div class="form-group">
                    <input type="hidden" id="id" name="id" value="">
                    <label class="col-md-3 control-label" for="val_corporatename">Corporate name<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <input type="text" id="val_corporatename" name="val_corporatename" class="form-control" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="val_corporatecode">Corporate Code<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <input type="text" id="val_corporatecode" name="val_corporatecode" class="form-control" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                <label class="col-md-3 control-label" for="image">Corporate Logo<span class="text-danger"></span></label>
                <div class="col-md-6">
                        <input type="hidden" id="base64" name="base64">
                        <input type="file" id="image" name="image" onchange="previewImage(this)" multiple>
                        <div id="cuisinesImg"></div>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-3">
                        <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<!-- END Page Content -->
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Load and execute javascript code used only in this page -->
<script src="js/CasseroleService.js"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<!-- <script>
    $(function() { FormsValidation.init();
        invitecodedetail();
    });
</script> -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#loading").hide();
        var id = "<?=$_GET['id']?>";
        var request = {"id":id};
        $.ajax({
            url: 'service.php?servicename=EditCorporate',
            type: 'POST',
            data: JSON.stringify(request),
            contentType: 'application/json; charset=utf-8',
            datatype: 'JSON',
            async: true,
            success: function(data1)
            {
               var result = JSON.parse(data1);
               $("#id").val(result.id);
               $("#val_corporatename").val(result.corporatename);
               $("#val_corporatecode").val(result.corporatecode);
               if(result['corporatelogo'] != ""){
                    $("#cuisinesImg").append('<div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;padding-left:5px;" onclick = "imageClose(this)">X</div><img  src="'+result['corporatelogo']+'" style="height:100px;margin-right:5px;margin-top:5px;" class="img-thumbnail cuisine-img"></div>');
               }
           }
       });
    });

    function previewImage(input) {
        if (input.files) {
            var filesAmount = input.files.length;
            for (var i = 0; i < filesAmount; i++)   {
                var reader = new FileReader();
                reader.onload = function(event) {
                    var src = event.target.result;
                    $("#cuisinesImg").append('<div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;padding-left:5px;" onclick = "imageClose(this)">X</div><img  src="'+src+'" style="height:100px;margin-right:5px;margin-top:5px;" class="img-thumbnail cuisine-img"></div>');
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }
    function imageClose(a)
    {
        $(a).parent().remove();
    }
    
    $("form[name='createcorporate']").validate({
        rules: {
            val_corporatename : "required",
            val_corporatecode : "required"
        },
        submitHandler: function(form) {
            $(this).find('button[type=submit]').prop('disabled', true);
            $(".preloader").show();
            var images = "";
            $( ".cuisine-img" ).each(function() {
                var image = $(this).attr("src");
                images += "###"+image;
            });
            $("#base64").val(images);
            var data = new FormData($('#createcorporate')[0]);
            $.ajax({
                url: 'service.php?servicename=updatecorporate',
                type: 'POST',
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                data: data, 
                success: function(data)
                {
                   $(".preloader").hide();
                     var result = JSON.parse(data);
                     if(result.status == 'success')
                     {
                        $("#toast-success").html(result.msg);
                        $("#toaster").fadeIn();
                        window.location.href = 'corporate.php';
                     }
                     else
                     {
                        $("#toast-error").html(result.msg);
                        $("#toasterError").fadeIn();
                     }
                     setTimeout(function(){
                        $("#toaster").fadeOut();
                        $("#toasterError").fadeOut();
                  }, 3000);
            }
        });
        }
    });
</script>
<?php include 'inc/template_end.php'; ?>

