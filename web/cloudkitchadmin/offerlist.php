<?php 
session_start();
include 'inc/config.php'; 

if(!isset($_SESSION['info']['user']))
{
	header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">

  <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
  <img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<!-- Validation Header -->
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1>Offer List</h1>
        <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
      </div>
    </div>
    <?php if($role != 2){ ?>
    <div class="col-sm-6">
     <div class="header-section">
      <a href="add_offer.php" style="width: 170px;float: right;" class="btn btn-block btn-primary">
        <i class="fa fa-plus"></i> Add Offer
      </a>
    </div>
  </div>
    <?php } ?>
</div>
</div>
<!-- END Validation Header -->

<!-- Form Validation Content -->
<div class="row">
  <div class="col-sm-12 col-md-12 col-lg-12">
    <!-- Form Validation Block -->
    <table id ="tableChefList" class="table table-vcenter table-striped table-hover table-borderless">
      <thead>
       <tr>
         <th>Sr</th>
         <th>Offer (In Percentage)</th>
         <th>Created on</th>
         <th>Status</th>
         <!-- <th>Action</th> -->
       </tr> 
     </thead>

   </table>
   <!-- END Form Validation Block -->
 </div>
</div>

</div>


<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#loading").hide();
    getCorporateOffer();
  });
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 
});
</script>
<script>
  function getCorporateOffer()
  {
    $('#loading').show();
    $.ajax({
      type: "POST",
      url : "service.php?servicename=getOffer",
      datatype: "JSON",
      success: function(data)
      {
        $('#loading').hide();
        var cusl = JSON.parse(data);
        var customer = new Array();
        for(var c=0;c<cusl.corporateoffer.length;c++)
        {
          customer[c] = new Array();
          customer[c][0] = c+1;
          customer[c][1] = cusl.corporateoffer[c].offer;
          customer[c][2] = cusl.corporateoffer[c].odate;
          if(cusl.corporateoffer[c].isActive == 'Y')
          {
            customer[c][3] = "<button type='button' onclick='inactiveoffer("+cusl.corporateoffer[c].id+");' class='btn btn-rounded btn-primary'>Active</button>";
          }
          else
          {
            customer[c][3] = "<button type='button' onclick='makeactiveoffer("+cusl.corporateoffer[c].id+");' class='btn btn-rounded btn-primary'>Inactive</button><span id='error-"+cusl.corporateoffer[c].id+"' style='color:red;display:none;'><br>Only one offer can active at a time.</span>";
            // customer[c][3] = "<button type='button' class='btn btn-rounded btn-primary'>InActive</button>";
          }
          
        }  
        $('#tableChefList').dataTable({
          "aaData": customer,
          "scrollX": true,
          "bDestroy": true
        });
      }
    });
  }
 

  function inactiveoffer(oid)
{
   console.log("CuisineId"+oid);

   var offerid = {"cid":oid };

   $.ajax({
            url: 'service.php?servicename=inactivenormaloffer',
            type: 'POST',
            datatype: 'JSON',
            async: false,
            data: JSON.stringify(offerid),
            contentType: 'application/json',
            success: function(rsca)
            {
                console.log("Cuisine Active"+JSON.stringify(rsca));

                var jrsc = JSON.parse(rsca);

                if(jrsc.status == 'success')
                {
                  //  window.location.href = 'offerlist.php';
                  getCorporateOffer();
                }  
            }

   });
}

function makeactiveoffer(oid)
{
   console.log("CuisineId"+oid);

   var offerid = {"cid":oid };

   $.ajax({
            url: 'service.php?servicename=makeactivenormaloffer',
            type: 'POST',
            datatype: 'JSON',
            async: false,
            data: JSON.stringify(offerid),
            contentType: 'application/json',
            success: function(rsca)
            {
                console.log("Cuisine Active"+JSON.stringify(rsca));

                var jrsc = JSON.parse(rsca);

                if(jrsc.status == 'success')
                {
                  //  window.location.href = 'offerlist.php';
                  
                  if(jrsc.Offerexist == '1'){
                    $("#error-"+oid).show();
                    setTimeout(function(){
                      $("#error-"+oid).hide();
                      }, 3000);

                  }else{
                    getCorporateOffer();
                  }
                }  
            }

   });
}
</script>