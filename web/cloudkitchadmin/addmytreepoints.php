<?php 

session_start();

print_r($_SESSION);

if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/Casseroleadmin/index.php");
	header("Location: https://coludkitch.co.in/cloudkitchadmin/index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
// include 'inc/config.php'; $template['header_link'] = 'WELCOME ';
 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="t">
    <!-- Validation Header -->
    <!-- <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Create Invitation Code</h1>
                </div>
            </div>
           
        </div>
    </div> -->

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <div class="block">
                <!-- Form Validation Title -->
                <div class="block-title">
                    <h2>ADD MYTREE POINTS</h2>
                </div>
                <!-- END Form Validation Title -->

                <!-- Form Validation Form -->
                
                <!--event_start_time,event_end_time,address,info-->
                <form id="admtp" action="addmytreepoints.php"  method="post" class="form-horizontal form-bordered">

                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-select">Select Mytree Points</label>
                    <div class="col-md-6">
                    <select id="mtp" name="example-select" class="form-control" size="1">
                    <option value="0">Please select</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                    <option value="150">150</option>
                    <option value="200">200</option>
					<option value="201">201</option>
					<option value="5">5</option>
                    </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Customers</label>
                    <div class="col-md-9" id="customers">


                        
                        <div class="checkbox">

                        <label for="example-checkbox1">
                        <input type="checkbox"  id="checkAll" name="checkAll" value="option1"> Check All
                        </label>
                        </div>

                        <br><br>

                       <!--  <div class="checkbox">
                        <label for="example-checkbox2">
                        <input type="checkbox" id="example-checkbox2" name="cus" value="option2"> CSS
                        </label>
                        </div> -->

                        <!-- <div class="checkbox">
                        <label for="example-checkbox3">
                        <input type="checkbox" id="example-checkbox3" name="example-checkbox3" value="option3"> Javascript
                        </label>
                        </div> -->

                    </div>
                </div>

                <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-3">
                            <button type="submit"  class="btn btn-effect-ripple btn-primary" onclick="addmytreepoint();">Submit</button>
                            <!-- <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button> -->
                        </div>
                    </div>

                </form>
                <!-- END Form Validation Form -->

                
            </div>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/CasseroleService.js"></script>


<script src="js/pages/formsValidationCreateInviteCode.js"></script>
<script>$(function() { FormsValidation.init(); });</script>
<script type="text/javascript">
$(document).ready(function() {
    $("#loading").hide();

    
    $("#checkAll").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 });

    $.ajax({
              url: 'service.php?servicename=customerlist',
              type: 'GET',
              success: function(cres)
              {
                 console.log("JSOn"+JSON.stringify(cres));

                 var crs = JSON.parse(cres);

                 var dtu =  '';

                 if(crs.status == 'success')
                 {
                        for(var u=0;u<crs.customerlist.length;u++)
                        {
                            dtu += '<div class="checkbox"><label for="example-checkbox1"><input type="checkbox" id="example-checkbox1" name="cus" value="'+crs.customerlist[u].walletid+'">'+crs.customerlist[u].name+'['+crs.customerlist[u].cloudkitchValue+']</label></div>';
                        }

                        $("#customers").append(dtu); 


                 }   

              }   
    })


});
</script>

<script type="text/javascript">
    
function addmytreepoint()
{
    var cusary = [];

    $.each($("input[name='cus']:checked"),function(){

            cusary.push($(this).val());

    });

    var mtp = '';

    mtp = $("#mtp").val();

    var reqamp = {"mytreepoint":mtp,"users":cusary};

    $.ajax({
             url: 'service.php?servicename=add_mytree_points',
             type: 'POST',
             datatype: 'JSON',
             contentType: 'application/json',
             data: JSON.stringify(reqamp),
             success:function(resmtp)
             {
                    console.log("Res"+JSON.stringify(resmtp));

                    var rmp = JSON.parse(resmtp);

                    if(rmp.status == 'success')
                    {
                        window.Location.href = 'addmytreepoints.php';
                    }    
             }    

    })
}


</script>

<?php include 'inc/template_end.php'; ?>

