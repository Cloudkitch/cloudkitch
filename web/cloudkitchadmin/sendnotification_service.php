<?php

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';



if($_GET['servicename'] == 'Send-Notification')
{

	$reqsn = file_get_contents('php://input');

	$ressn = json_decode($reqsn,true);

	$users = '';

	$msg = '';

	$excud='';

	$users = $ressn['users'];

	$msg = $ressn['not'];

	$sm = array();

	for($u=0;$u<count($users);$u++)
	{
		$devicetype = '';

		$devicetoken = '';

		$queud = "SELECT devicetype,andriodtoken as atoken,iostoken as itoken FROM user WHERE userid='".$users[$u]."'";

		$excud = mysqli_query($conn,$queud) or die(mysqli_error($conn));

		$rsud =  mysqli_fetch_assoc($excud);

		$devicetype = $rsud['devicetype'];

		if($devicetype == 'android')
		{
			$devicetoken = $rsud['atoken'];

			androidpushnotify($devicetoken,$msg);
		}

		if($devicetype == 'ios')
		{
			$devicetoken = $rsud['itoken'];

			iospushnotify($devicetoken,$msg);
		}
	}

	if($excud)
	{
		$sm['status'] = 'success';
		$sm['msg'] = 'Notification sent successfully';
	}
	else
	{
		$sm['status'] = 'failure';
		$sm['msg'] = 'Failed to sent notification';
	}

	print_r(json_encode($sm));
	exit;
}

function androidpushnotify($devicetoken,$msg)
{
	if (!defined('SERVER_KEY')) define('SERVER_KEY', 'AIzaSyAOHHr4uAXB9qQ-wcXAblPifmu-FePy8Rc');
	
	$path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
			
		$fields = array(
            'to' => $devicetoken,
            'notification' => array('title' => 'My Tree', "sound"=>"notificationsound.wav","vibrate"=>1,'body' => $msg),
            // 'data' => array('title' => 'My Tree','sound'=>'notificationsound.wav','vibrate'=>1,'body' => $msg)
		   //'data' => array('message' => '')
        );
 
        $headers = array(
            'Authorization:key=' .SERVER_KEY,
            'Content-Type:application/json'
        );

		$ch = curl_init();
 
        curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    
        $result = curl_exec($ch);
       
        curl_close($ch);
}

function iospushnotify($devicetoken,$msg)
{

// Provide the Host Information.

//$tHost = 'gateway.sandbox.push.apple.com';

$tHost = 'gateway.push.apple.com';


$tPort = 2195;

// Provide the Certificate and Key Data.
	
//$tCert = 'MyTreeDev.pem';

$tCert = 'pushcert.pem';

// Provide the Private Key Passphrase (alternatively you can keep this secrete

// and enter the key manually on the terminal -> remove relevant line from code).

// Replace XXXXX with your Passphrase

$tPassphrase = '123456';

// Provide the Device Identifier (Ensure that the Identifier does not have spaces in it).

// Replace this token with the token of the iOS device that is to receive the notification.

//$tToken = 'b3d7a96d5bfc73f96d5bfc73f96d5bfc73f7a06c3b0101296d5bfc73f38311b4';

//By Dhruvpalsinh on 17aug2016
//$tToken = '0a32cbcc8464ec05ac3389429813119b6febca1cd567939b2f54892cd1dcb134';

$tToken = $devicetoken;

//0a32cbcc8464ec05ac3389429813119b6febca1cd567939b2f54892cd1dcb134

// The message that is to appear on the dialog.

$tAlert = $msg;

// The Badge Number for the Application Icon (integer >=0).

$tBadge = 8;

// Audible Notification Option.

$tSound = 'default';

// The content that is returned by the LiveCode "pushNotificationReceived" message.

//By Dhruvpalsinh on 17aug2016
//$tPayload = 'APNS Message Handled by LiveCode';

$tPayload = $msg;

// Create the message content that is to be sent to the device.

$tBody['aps'] = array (

'alert' => $tAlert,

'badge' => $tBadge,

'sound' => $tSound,

);

$tBody ['payload'] = $tPayload;

//$tBody['click_action'] = 'chatActivity';

//$tBody['type'] = $stype;

//$tBody['name'] = $uname;

//$tBody['userid'] = $uid;

//$tBody['profile'] = $pp;

// Encode the body to JSON.

$tBody = json_encode ($tBody);

// Create the Socket Stream.

$tContext = stream_context_create ();

stream_context_set_option ($tContext, 'ssl', 'local_cert', $tCert);

// Remove this line if you would like to enter the Private Key Passphrase manually.

stream_context_set_option ($tContext, 'ssl', 'passphrase', $tPassphrase);

// Open the Connection to the APNS Server.

$tSocket = stream_socket_client ('ssl://'.$tHost.':'.$tPort, $error, $errstr, 30, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $tContext);

// Check if we were able to open a socket.

if (!$tSocket)

exit ("APNS Connection Failed: $error $errstr" . PHP_EOL);

// Build the Binary Notification.

$tMsg = chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;

// Send the Notification to the Server.

$tResult = fwrite ($tSocket, $tMsg, strlen ($tMsg));

//if ($tResult)

//echo 'Delivered Message to APNS' . PHP_EOL;

//else

//echo 'Could not Deliver Message to APNS' . PHP_EOL;

// Close the Connection to the Server.

fclose ($tSocket);

}