<?php

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['service_name'] == 'paidvendors')
{

	$reqpc = file_get_contents('php://input');

	$respc = json_decode($reqpc,true);

	$invc = '';

	$invc = $respc['invitecode'];

	// $quepc = "SELECT cp.chefid,u.name as chefname,cp.chefid,SUM(total) as total, 
	// 		(SELECT SUM(total) FROM chefpayout WHERE paymenttype='MTP' and cfpid=cp.cfpid) as MTP, 
	// 		(SELECT SUM(total) FROM chefpayout WHERE paymenttype='CASH' and cfpid=cp.cfpid) as CASH, 
	// 		GROUP_CONCAT(cp.itemid) as itemid FROM chefpayout as cp,user as u WHERE u.userid=cp.chefid and
	// 		cp.invitecode='".$invc."' GROUP BY cp.chefid";

	$quepc = "SELECT vp.vnpid,vp.chefid,u.name as vendorname,vp.chefid,vp.total,vp.paymenttype,vp.itemid,vp.createdate,vp.paidstatus FROM vendorpayout as vp,user as u WHERE u.userid=vp.chefid and vp.invitecode='".$invc."' ORDER BY vnpid DESC";

	$excpc = mysqli_query($conn,$quepc) or die(mysqli_error($conn));

	$pc = array();
	
	if(mysqli_num_rows($excpc) > 0)
	{
		$pc['vendors'] = array();

		while($rowpc = mysqli_fetch_assoc($excpc))
		{
			$p = array();

			$p['vnpid'] = $rowpc['vnpid'];

			$p['chefid'] = $rowpc['chefid'];

			$p['vendorname'] = $rowpc['vendorname'];

			$p['total'] = $rowpc['total'];

			$p['pmtmode'] = $rowpc['paymenttype'];

			$p['pmtdate'] = $rowpc['createdate'];

			$p['paidstatus'] = $rowpc['paidstatus'];

			// if($rowpc['MTP'] != null)
			// {
			// 	$p['MTP'] = $rowpc['MTP'];
			// }
			// else
			// {
			// 	$p['MTP'] = 0;
			// }

			// if($rowpc['CASH'] != null)
			// {
			// 	$p['CASH'] = $rowpc['CASH'];
			// }
			// else
			// {
			// 	$p['CASH'] = 0;
			// }

			$p['itemid'] =  $rowpc['itemid'];

			$itemids = explode(',', $p['itemid']);

			$quecfc = "SELECT count(i.itemid) as cus FROM casseroleorderitem as
					   coi,item as i WHERE i.itemid=coi.cuisineid and coi.chefid='".$rowpc['chefid']."' and coi.cassitemid IN  ('".implode("','", $itemids)."')";

			//echo $quecfc;		   
			$excfc = mysqli_query($conn,$quecfc) or die(mysqli_error($conn));
			
			$rscfc = mysqli_fetch_assoc($excfc);		   

			$p['cuisines'] = $rscfc['cus'];

			array_push($pc['vendors'], $p);
		}

		$pc['status'] = 'success';
		$pc['msg'] = 'Data available';	

	}
	else
	{
		$pc['status'] = 'failure';
		$pc['msg'] = 'Not available';
	}

	print_r(json_encode($pc));
	exit;		
}

if($_GET['service_name'] == 'cuisinedetail')
{
	$reqcd = file_get_contents('php://input');

	$rescd = json_decode($reqcd,true);

	$itemid = '';
	$cfid = '';

	$itemid = $rescd['itemid'];

	$cfid = $rescd['chefid'];

	$quecd =  "SELECT coi.cassitemid,c.cuisineid,c.cuisinename,SUM(quantity) as soldqty FROM casseroleorderitem as coi,cuisine as c WHERE cassitemid in (".$itemid.") and
		c.cuisineid=coi.cuisineid and coi.chefid='".$cfid."' group by c.cuisineid";

	$exccd = mysqli_query($conn,$quecd) or die(mysqli_error($conn));

	$cd = array();

	if(mysqli_num_rows($exccd) > 0)
	{
		$cd['cu'] = array();

		while($rowc = mysqli_fetch_assoc($exccd))
		{
			$c = array();

			$c['iid'] = $rowc['cassitemid'];

			$c['cuisinename'] = $rowc['cuisinename'];

			$c['soldqty'] = $rowc['soldqty'];

			$quedt = "SELECT createdate FROM chefpayout WHERE find_in_set('".$rowc['cassitemid']."',itemid)";
			$excdt = mysqli_query($conn,$quedt) or die(mysqli_error($conn));

			$rsdt = mysqli_fetch_assoc($excdt);

			$c['date'] = $rsdt['createdate'];

			array_push($cd['cu'], $c);
		}

		$cd['status'] = 'success';
		$cd['msg'] = 'Data available';
	}
	else
	{
		$cd['status'] = 'failure';
		$cd['msg'] = 'not available';
	}

	print_r(json_encode($cd));
	exit;	

	//echo "Cuisinedetails".$quecd;	
}

if($_GET['service_name'] == 'vclosepayout')
{
	$reqcp = file_get_contents('php://input');

	$rescp = json_decode($reqcp,true);

	$vendorpid = '';

	$vendorpid = $rescp['Vendorpid'];

	$cp = array();

	$updcp = "UPDATE vendorpayout SET paidstatus='DONE' WHERE vnpid='".$vendorpid."' ";
	$exccp = mysqli_query($conn,$updcp) or die(mysqli_error($conn));

	if($exccp)
	{
		$cp['status'] = 'success';
		$cp['msg'] = 'Closed payout successfully';
	}
	else
	{
		$cp['status'] = 'failure';
		$cp['msg'] = 'Failed to update';
	}

	print_r(json_encode($cp));
	exit;	
}

?>