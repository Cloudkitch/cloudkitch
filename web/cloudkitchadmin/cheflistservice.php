<?php
session_start();
header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';
$userid = $_SESSION['info']['id'];
if($_GET['servicename'] == 'ChefList')
{

	$reqcl = file_get_contents('php://input');

	$rescl = json_decode($reqcl,true);

	$invc = '';

	$invc = $rescl['invitecode'];

	$quecl = "SELECT userid,name,email,phone,address FROM user WHERE ischef='1' AND status='1' AND addedby = '$userid' AND invitecode='1111' ORDER BY userid DESC";

	// $quecl = "SELECT * FROM user as u,invitationdetail as ivd,invitationusermap as ium WHERE ivd.id=ium.invitationid and ium.userid=u.userid and ivd.invitationcode='".$invc."' and u.ischef='1'";
	$exccl = mysqli_query($conn,$quecl) or die(mysqli_error($conn));

	$cl = array();

	if(mysqli_num_rows($exccl) > 0)
	{
		$cl['chefs'] = array();

		while($rowcl = mysqli_fetch_assoc($exccl))
		{
			$c = array();
			$c['chefid'] = $rowcl['userid'];
			$c['chefname'] = $rowcl['name'];
			$c['email'] = $rowcl['email'];
			$c['phone'] = $rowcl['phone'];
			$c['address'] = $rowcl['address'];
			$id = $rowcl['userid'];
			$query = "SELECT * FROM restro_meta_details WHERE kitchen_id='$id'";
			$result = mysqli_query($conn,$query) or die(mysqli_error($conn));
			$row = mysqli_fetch_assoc($result);
			$c['keywords'] = isset($row['keywords']) ? $row['keywords'] : ""; 
			$c['title'] = isset($row['title']) ? $row['title'] : ""; 
			$c['description'] = isset($row['description']) ? $row['description'] : ""; 

			array_push($cl['chefs'], $c);	
		}

		$cl['status'] = 'success';
		$cl['msg'] = 'chef list available';

	}
	else
	{
		$cl['status'] = 'failure';
		$cl['msg'] = 'Chef list not found';
	}

	print_r(json_encode($cl));
	exit;	
}

/*if($_GET['servicename'] == 'Approve-chef')
{
	$reqac = file_get_contents('php://input');

	$resac = json_decode($reqac,true);

	$chefid = '';

	$chefid = $resac['chefid'];

	$ac = array();

	$updac = "UPDATE user SET chefverified = 'Y' WHERE userid='".$chefid."' ";
	$excac = mysqli_query($conn,$updac) or die(mysqli_error($conn));

	if($excac)
	{
		$ac['status'] = 'success';
		$ac['msg'] = 'Successfully verified chef';
	}
	else
	{
	   $ac['status'] = 'failure';
	   $ac['msg'] = 'Failed to approve chef';	
	}

	print_r(json_encode($ac));
	exit; 
}
*/
if($_GET['servicename'] == 'ChefListNew')
{

	$reqcl = file_get_contents('php://input');

	$rescl = json_decode($reqcl,true);

	$invc = '';

	$invc = $rescl['invitecode'];

	// $quecl = "SELECT userid,name,email,phone,address,flatno,building FROM user WHERE ischef='1' ORDER BY userid DESC";

	$quecl = "SELECT * FROM user as u,invitationdetail as ivd,invitationusermap as ium WHERE ivd.id=ium.invitationid and ium.userid=u.userid and ivd.invitationcode='".$invc."' and u.ischef='1' ";
	$exccl = mysqli_query($conn,$quecl) or die(mysqli_error($conn));

	$cl = array();

	if(mysqli_num_rows($exccl) > 0)
	{
		$cl['chefs'] = array();

		while($rowcl = mysqli_fetch_assoc($exccl))
		{
			$c = array();

			$c['chefid'] = $rowcl['userid'];

			$c['chefname'] = $rowcl['name'];

			$c['email'] = $rowcl['email'];

			$c['phone'] = $rowcl['phone'];

			if($rowcl['address'] != '')
			{
				$c['address'] = $rowcl['address'];
			}
			else
			{
				$c['address'] = $rowcl['flatno'].'-'.$rowcl['building'];
			}

			$c['chefverified'] = $rowcl['chefverified'];

			array_push($cl['chefs'], $c);	
		}

		$cl['status'] = 'success';
		$cl['msg'] = 'chef list available';

	}
	else
	{
		$cl['status'] = 'failure';
		$cl['msg'] = 'Chef list not found';
	}

	print_r(json_encode($cl));
	exit;	
}

?>