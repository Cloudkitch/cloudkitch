<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<!-- Due History Start -->


  <div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <!-- <a href="#myModal" class="btn btn-lg btn-primary" data-toggle="modal">Launch
      Demo Modal</a> -->
    <!-- Modal HTML -->
    <div id="myModaldues" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Dues Detail</h4>
          </div>
          <div class="modal-body">

          <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tabledueList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Orderid</th>
                                                 <th>Chef</th>
                                                 <th>Cuisine</th>
                                                 <th>Amount</th>
                                                 </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>

           

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>
        </div>
      </div>
    </div>

    
  </div>


  <!-- Due History End -->

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section" id="cp">
                    <h1>Cancel Orders</h1>
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">

    <button type="button" id="exptexcel" class="btn btn-rounded btn-success" style="float:right">Export</button>
    <br><br>
        <div class="col-sm-10 col-md-12 col-lg-12">
        <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
            <!-- Form Validation Block -->
            <table id ="tableco" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>User</th>
                                                 <th>Dues</th>
                                                 <th>Amount</th>
                                                 <th>History</th>
                                                 </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->



<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script src="js/CasseroleService.js"></script>

<!-- <script src="js/order.js"></script> -->


<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

        $('#tableco').dataTable().fnClearTable();
        $('#tableco').dataTable().fnDraw();
        $('#tableco').dataTable().fnDestroy();

        cancelorders();
       
    });</script>

    <script type="text/javascript">
        
    function cancelorders()
    {
       var invitecode = '';

       invitecode = $("#invitecode").val();

       var reqhc = {"invitecode":invitecode};

        $("#loading").show();
        $.ajax({
                 url: 'cod-cancelorders-service.php?servicename=Cancelorders',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(reqhc),
                 async: false,
                 success: function (col)
                 {
                    $("#loading").hide();
                     console.log("Cheforders Response"+JSON.stringify(col));

                     var rscol = JSON.parse(col);

                     var co = new Array();

                     for(var oc=0;oc<rscol.canco.length;oc++)
                     {
                        co[oc] = new Array();

                        co[oc][0] = oc+1;

                        co[oc][1] = rscol.canco[oc].username;

                        co[oc][2] = rscol.canco[oc].duecount;

                        co[oc][3] = rscol.canco[oc].totalamount;

                        co[oc][4] = '<a href="#myModaldues" onclick="viewdues('+rscol.canco[oc].userid+')" data-toggle="modal"><button type="button" class="btn btn-rounded btn-primary">info</button></a>';     
                        

                                                                                                                         
                    }

                    $('#tableco').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true
                    });   
                 }    
        });
    }

    </script>

      <script type="text/javascript">
        
        $(document).ready(function() {

   function download_csv(csv, filename) {
        var csvFile;
        var downloadLink;
        // CSV FILE
        csvFile = new Blob([csv], {type: "text/csv"});
        // Download link
        downloadLink = document.createElement("a");
        // File name
        downloadLink.download = filename;
        // We have to create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);
        // Make sure that the link is not displayed
        downloadLink.style.display = "none";
        // Add the link to your DOM
        document.body.appendChild(downloadLink);
        // Lanzamos
        downloadLink.click();
    }

    function export_table_to_csv(html, filename) {
        var csv = [];
        var rows = document.querySelectorAll("table tr");

        for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++) 
        row.push(cols[j].innerText);        
        csv.push(row.join(","));       
        }
        // Download CSV
        download_csv(csv.join("\n"), filename);
    }

    document.querySelector("#exptexcel").addEventListener("click", function () {
    var html = document.querySelector("table").outerHTML;
    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    filename = 'MyTree_Cancel_Orders_Dues' + postfix + '.csv';
    export_table_to_csv(html, filename);
    });

});


    </script>

    <script type="text/javascript">
      
    function viewdues(uid)
    {
        //console.log("Chefid"+cfid);

        var reqdue = {"userid":uid};

        $.ajax({

                 url:'cod-cancelorders-service.php?servicename=userdues',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(reqdue),
                 async: false,
                 success: function(rsd)
                 {
                    console.log("Json"+JSON.stringify(rsd));

                    var rsdd = JSON.parse(rsd);

                    $('#tabledueList').dataTable().fnClearTable();
                    $('#tabledueList').dataTable().fnDraw();
                    $('#tabledueList').dataTable().fnDestroy();

                    if(rsdd.status == 'success')
                    {
                        var du = new Array();

                        for(var dc=0;dc<rsdd.dues.length;dc++)
                        {
                           du[dc] = new Array();

                           du[dc][0] = dc+1;

                           du[dc][1] = rsdd.dues[dc].orderid;

                           du[dc][2] = rsdd.dues[dc].chef;

                           du[dc][3] = rsdd.dues[dc].cuisine; 

                           du[dc][4] = rsdd.dues[dc].amount; 
                        } 

                        $('#tabledueList').dataTable({
                        "aaData": du,
                        "bDestroy": true,
                        "autoWidth": true
                    }); 


                    }
                    else
                    {

                    }  
                 }

        }); 
    }

    </script>

    

    
    