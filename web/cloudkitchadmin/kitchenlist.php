<?php 
session_start();
include 'inc/config.php'; 

if(!isset($_SESSION['info']['user']))
{
	header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">

  <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
  <img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<!-- Validation Header -->
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1>Restaurant List</h1>
        <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
      </div>
    </div>
    <div class="col-sm-6">
     <div class="header-section">
      <a href="add_kitchen.php" style="width: 150px;float: right;" class="btn btn-block btn-primary">
        <i class="fa fa-plus"></i> Add Restaurant
      </a>
    </div>
  </div>
</div>
</div>
<!-- END Validation Header -->

<!-- Form Validation Content -->
<div class="row">
  <div class="col-sm-12 col-md-12 col-lg-12">
    <!-- Form Validation Block -->
    <table id ="tableChefList" class="table table-vcenter table-striped table-hover table-borderless">
      <thead>
       <tr>
         <th>Sr</th>
         <th>Restaurant Name</th>
         <th>Email</th>
         <th>Phone</th>
         <th>Address</th>
         <th>Title</th>
         <th>Keywords</th>
         <th>Description</th>
         <th>Action</th>
       </tr> 
     </thead>

   </table>
   <!-- END Form Validation Block -->
 </div>
</div>

</div>


<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#loading").hide();

    cheflist();
  });
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

});
</script>
<script>
  function cheflist()
  {
    var invitecode = '';
    invitecode = $("#invitecode").val();
    var reqcl = {"invitecode":invitecode};
    $('#loading').show();
    $.ajax({
      type: "POST",
      url : "cheflistservice.php?servicename=ChefList",
      datatype: "JSON",
      data: JSON.stringify(reqcl),
      success: function(data)
      {
        $('#loading').hide();

        console.log("Customer Data :"+JSON.stringify(data));

        var cusl = JSON.parse(data);

        var customer = new Array();

        for(var c=0;c<cusl.chefs.length;c++)
        {
          customer[c] = new Array();

          customer[c][0] = c+1;
          customer[c][1] = cusl.chefs[c].chefname;
          customer[c][2] = cusl.chefs[c].email;
          customer[c][3] = cusl.chefs[c].phone;
          customer[c][4] = cusl.chefs[c].address;
          customer[c][5] = cusl.chefs[c].title;
          customer[c][6] = cusl.chefs[c].keywords;
          customer[c][7] = cusl.chefs[c].description;
          customer[c][8] = '<div class=""> <a href="edit_kitchen.php?id='+cusl.chefs[c].chefid+'" class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Edit"> <i class="fa fa-pencil"></i> </a>&nbsp;&nbsp <button type="button" onclick = "deletedata('+cusl.chefs[c].chefid+');" class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Delete"> <i class="fa fa-fw fa-times"></i> </button> </div>';
        }  
        $('#tableChefList').dataTable({
          "aaData": customer,
          "scrollX": true,
          "bDestroy": true
        });
      }
    });
  }
  function deletedata(id)
  {
    swal({
                title: "Are You sure want to delete this?",
                //text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                    var request = {"id":id};
                    $.ajax({
                      url: 'service.php?servicename=deletekitchen',
                      type: 'POST',
                      data: JSON.stringify(request),
                      contentType: 'application/json; charset=utf-8',
                      datatype: 'JSON',
                      async: true,
                      success: function(data)
                      {
                        var result = JSON.parse(data);
                        if(result.status == 'success')
                        {
                          cheflist();
                          $("#toast-error").html(result.msg);
                          $("#toasterError").fadeIn();
                        }
                        else
                        {
                          $("#toast-error").html(result.msg);
                          $("#toasterError").fadeIn();
                        }
                        setTimeout(function(){
                          $("#toaster").fadeOut();
                          $("#toasterError").fadeOut();
                        }, 3000);
                      }
                    });
                }
              });
  }
  
</script>