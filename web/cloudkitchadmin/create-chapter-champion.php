<?php 

session_start();

//print_r($_SESSION);

if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/Casseroleadmin/index.php");
	header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
// include 'inc/config.php'; $template['header_link'] = 'WELCOME ';
 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Validation Header -->
    <!-- <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Create Invitation Code</h1>
                </div>
            </div>
           
        </div>
    </div> -->

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <div class="block">
                <!-- Form Validation Title -->
                <div class="block-title">
                    <h2>Create Chapter Champion</h2>
                </div>
                <!-- END Form Validation Title -->

                <!-- Form Validation Form -->
                
                <!--event_start_time,event_end_time,address,info-->
                <form id="chapchamp" action="create-chapter-champion.php"  method="post" class="form-horizontal form-bordered">

                <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">

                    

                    <div class="form-group" id="selectInvite">
                        <label class="col-md-3 control-label" for="val-cc">Select Chapter Champion<span class="text-danger">*</span></label>
                        <div class="col-md-6" >
                           <select id="val-cc" name="val-cc" class="form-control">
                               <option value="">Select Chapter Champion</option>
                               <!-- <option value="selectall">Select All</option> -->
                            </select>
                        </div>
                    </div>

                    <div class="form-group" id="selectInvite">
                        <label class="col-md-3 control-label" for="val-c">Select Chapter<span class="text-danger">*</span></label>
                        <div class="col-md-6" >
                           <select id="val-c" name="val-c" class="form-control">
                               <option value="">Select Chapter</option>
                               <!-- <option value="selectall">Select All</option> -->
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-email">Email<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-email" name="val-email" class="form-control" placeholder="Please Enter Email Address">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-pwd">Password<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="password" id="val-pwd" name="val-pwd" class="form-control" placeholder="Please Enter Password">
                        </div>
                    </div>

                      

                    
                    <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-3">
                            <button type="button" onclick="assignchapterchampion();" class="btn btn-effect-ripple btn-primary">Submit</button>
                            <button type="button" onclick="reset();"  class="btn btn-danger">Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Form Validation Form -->

                
            </div>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->

    

    <div class="row" id="col">

            <div class="col-sm-10 col-md-12 col-lg-12">
            
                <div class="block">

    
                <div class="block-title">
                    <h2>Chapter Champions</h2>
                    <button type="button" id="exptexcel" class="btn btn-rounded btn-success" style="float:right">Export</button>
                </div>
           

      
                <table id ="tablccList" class="table table-vcenter table-striped table-hover table-borderless">

                                                <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Name</th>
                                                 <th>Email</th>
                                                 <th>Password</th>
                                                 <th>Chapter</th>
                                                 <th>Invitecode</th>
                                                 </tr> 
                                                </thead>  
                  
                </table>
            

                </div>

            </div>
    </div>
</div>


 <?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function() {
    $("#loading").hide();
    $("#cht").hide();
   // $("#col").hide();
    
    users();

    chapters();

      $("#exptexcel").click(function(e) {
    e.preventDefault();

    //getting data from our table
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('tablccList');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');

    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
         var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    a.download = 'MyTree_Chapter_Champion' + postfix + '.xls';
    a.click();
  }); 
    


});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    
    $('#tablccList').dataTable().fnClearTable();
        $('#tablccList').dataTable().fnDraw();
        $('#tablccList').dataTable().fnDestroy();

        championchapterlist();
       
      });</script>
<script type="text/javascript">

   function users()
   {
        var ic = $("#invitecode").val();

        var reqic = {"invitecode":ic};

        $.ajax({
                 url: 'chapterchampion_service.php?servicename=UserList',
                 type: 'GET',
                 async: false,
                 success: function(rcl)
                 {
                     console.log("Cl"+JSON.stringify(rcl));

                     var cl = JSON.parse(rcl);

                     var cus = '';

                     if(cl.status == 'success')
                     {
                          for(var c=0;c<cl.users.length;c++)
                          {
                            
                             cus += "<option value='"+cl.users[c].userid+"'> " + cl.users[c].name + "[" + cl.users[c].groupname + "-" + cl.users[c].invitecode + "]</option>";
                          } 

                        $("#val-cc").append(cus);
                     }  
                 }

        }); 
   }     

</script>

<script type="text/javascript">
    
  function chapters()
  {
     $.ajax({
               url:'chapterchampion_service.php?servicename=Chapters',
               type: 'GET',
               async: false,
               success: function(cps)
               {
                    console.log("Json"+JSON.stringify(cps));

                    var rcps = JSON.parse(cps);

                    var chp = '';

                    if(rcps.status == 'success')
                    {
                        for(var cp=0;cp<rcps.chapters.length;cp++)
                        {
                            chp += "<option value='"+rcps.chapters[cp].invitecode+"'> " + rcps.chapters[cp].groupname+ "-" +rcps.chapters[cp].invitecode+"</option>";
                        }

                        $("#val-c").append(chp);


                    }    
               }  
     });
  }  


</script>


<script type="text/javascript">

   function assignchapterchampion()
    {

         console.log("Hii");

         var uid = '';

         var chpid = '';

         var email = '';

         var pwd = '';

         var unm = '';

         uid = $("#val-cc").val();

         chpid = $("#val-c").val();

         email = $("#val-email").val();

         pwd = $("#val-pwd").val();

         console.log("Uid"+uid+"chpid"+chpid+"email"+email+"Pwd"+pwd);

         var reqccchp = {"userid":uid,"invitecode":chpid,"email":email,"pwd":pwd};


        $.ajax({
                 url: 'chapterchampion_service.php?servicename=AssignChapterChampion',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(reqccchp),
                 async: false,
                 success: function(rsachp)
                 {
                     console.log("Json"+JSON.stringify(rsachp));

                     var rsuch = JSON.parse(rsachp);

                     if(rsuch.status == 'success')
                     {
                        location.reload();

                     }
                     else
                     {
                        alert('Something went wrong..!!');
                        return false;
                     }   
                 }    
        });
    }

    function reset()
    {
       location.reload();
    }

</script>

<script type="text/javascript">
  
function championchapterlist()
{
    $.ajax({
             url:'chapterchampion_service.php?servicename=Chapter-Champions', 
             type: 'GET',
             async: false,
             success: function(rscc)
             {
               console.log("CC"+JSON.stringify(rscc));

               var rescc = JSON.parse(rscc);

               var co = new Array();

               for(var ch=0;ch<rescc.chapchm.length;ch++)
               {

                  co[ch] = new Array();

                  co[ch][0] = ch+1;

                  co[ch][1] = rescc.chapchm[ch].username;

                  co[ch][2] = rescc.chapchm[ch].email;

                  co[ch][3] = rescc.chapchm[ch].password;

                  co[ch][4] = rescc.chapchm[ch].chaptername;

                  co[ch][5] = rescc.chapchm[ch].invitecode;                  

           
               }

                $('#tablccList').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true,
                        "aoColumns": [
                            { "orderSequence": [ "desc", "asc"] },
                            { "orderSequence": [ "desc", "asc"] },
                            { "orderSequence": [ "desc", "asc"] },
                            { "orderSequence": [ "desc", "asc"] },
                            { "orderSequence": [ "desc", "asc"] },
                            { "orderSequence": [ "desc", "asc"] }
                        ]
                       });




             }

    });
}

</script>





