<?php 

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['servicename'] == 'cancelorder')
{

    $reqco = file_get_contents('php://input');

    $resco = json_decode($reqco,true);

    $inc = '';

    $inc = $resco['invitecode'];  

    $quecord = "SELECT (SELECT name FROM user as u1 WHERE u1.userid=c.userid ) as chefname, 
                (SELECT phone FROM user as u1 WHERE u1.userid=c.userid ) as chefphone,
                (SELECT address FROM user as u1 WHERE u1.userid=c.userid ) as chefaddress, 
                (SELECT flatno FROM user as u1 WHERE u1.userid=c.userid ) as cheffaltno, 
                (SELECT building FROM user as u1 WHERE u1.userid=c.userid ) as chefbuilding, 
                co.cassordid as orderid,u.name as customer,u.phone as customerphone,
                u.address as customeraddress,u.flatno as customerflatno,u.building as customerbuilding,
                c.cuisinename,coi.quantity,coi.price,coi.quantity * coi.price as total,
                co.orderdate,co.invitecode,co.paymentType,coi.PaymentType as pt,co.transId,
                co.actualamount,coi.MTP,coi.cash,co.discountamount,co.couponapplied,
                (select couponcode FROM coupon WHERE couponid=co.couponid) as couponcode,
                coi.cuisineid,coi.type,c.cuisinetype,c.time,coi.pickupdate,coi.status,
                coi.code,coi.closedate,coi.usrwntDelivery as delivery,coi.IsAftertime as iat 
                FROM casseroleorder as co,user as u,cuisine as c,casseroleorderitem as coi
                WHERE c.cuisineid=coi.cuisineid and coi.cassordid=co.cassordid and 
                coi.cuisineid=c.cuisineid  and co.userid=u.userid and co.invitecode='".$inc."' and coi.status='cancel' and coi.type !='mohallabazar'  ORDER BY co.cassordid DESC";

    $exccord = mysqli_query($conn,$quecord) or die(mysqli_error($conn));
    
    $cfo = array();

    if(mysqli_num_rows($exccord) > 0)
    {
        $cfo['cord'] = array();

        while($rowcord = mysqli_fetch_assoc( $exccord))
        {
            $cor = array();
            $data = $rowcord['cash'];

            if($rowcord['iat'] == 'Y')
            {
                $cor['aftertime'] = 'Yes';
            }
            else if($rowcord['iat'] == 'N')
            {
                $cor['aftertime'] = 'No';
            }    

            $cor['chefname'] = $rowcord['chefname'];

            $cor['chefphone'] = $rowcord['chefphone'];

           // $cor['chefaddress'] = $rowcord['chefaddress'];

            $cor['cheffaltno'] = $rowcord['cheffaltno'];

            $cor['chefbuilding'] = $rowcord['chefbuilding'];

            $cor['orderid'] = $rowcord['orderid'];

            $cor['customer'] = $rowcord['customer'];

            $cor['customerphone'] = $rowcord['customerphone'];

           // $cor['customeraddress'] = $rowcord['customeraddress'];

            $cor['customerflatno'] = $rowcord['customerflatno'];

            $cor['customerbuilding'] = $rowcord['customerbuilding'];

            $cor['cuisinename'] = $rowcord['cuisinename'];

            $cor['quantity'] = $rowcord['quantity'];

            $cor['price'] = $rowcord['price'];

            $cor['total'] = $rowcord['total'];

            $cor['orderdate'] = $rowcord['orderdate'];

            $cor['invitecode'] = $rowcord['invitecode'];

            if($rowcord['pt'] == 'COD' && $rowcord['MTP'] > 0)
            {
                $cor['paymenttype'] =' COD + MTP';
            }
            else if($rowcord['pt'] == 'MW')
             {
                 $cor['paymenttype'] ='MTP';
             }
            else if($rowcord['pt'] == 'OLP' && $rowcord['MTP'] > 0)
             {
                 $cor['paymenttype'] =' OLP + MTP';
             }
            else
            {
                $cor['paymenttype'] = $rowcord['paymentType'];
            }

            

            $cor['transid'] = $rowcord['transId'];

            $cor['actualamount'] = $rowcord['actualamount'];

            $cor['discountamount'] = $rowcord['discountamount'];

            $cor['couponapplied'] = $rowcord['couponapplied'];

            $cor['couponcode'] = $rowcord['couponcode'];

            $cor['cuisinetype'] = $rowcord['cuisinetype'];

            $cor['type'] = $rowcord['type'];

            if($rowcord['type'] == 'trending')
            {
                $cor['pickupdate'] = $rowcord['time'];
            }
            else
            {
                $cor['pickupdate'] = $rowcord['pickupdate'];
            } 

            $cor['status'] = $rowcord['status'];

            $cor['code'] = $rowcord['code'];

            $cor['closedate'] = $rowcord['closedate'];

            if($rowcord['pt'] == 'COD')
            {
               // echo "COD AaAdhana";
                $cor['paymentbreakdown'] = "COD : ".$data." | MTP : ".$rowcord['MTP']." | OLP : 0 | PTM : 0" ;
            }
            else if($rowcord['pt'] == 'OLP')
            {
                            //    echo "OLP";

                $cor['paymentbreakdown'] = "COD : 0 | MTP :".$rowcord['MTP']." | OLP : ".$rowcord['cash']." | PTM : 0" ;   
            }
            else if($rowcord['pt'] == 'PTM')
            {
                $cor['paymentbreakdown'] = "COD : 0 | MTP :".$rowcord['MTP']." | OLP : 0 | PTM : ".$rowcord['cash'] ;   
            }
            else 
            {
                         //       echo "MTP";

                $cor['paymentbreakdown'] = "COD : 0 | MTP :".$rowcord['MTP']." | OLP : 0 | PTM : 0";
            }

            if($rowcord['delivery'] == 'Y')
            {
                $cor['delivery'] = 'Homedelivery';
            }
            else
            {
                $cor['delivery'] = 'Pickup';
            }    

            array_push($cfo['cord'], $cor);
        }

        $cfo['status'] = 'success';
        $cfo['msg'] = 'Orders Available';
    }
    else
    {
        $cfo['status'] = 'failure';
        $cfo['msg'] = 'Not Available';
    }
    //print_r($cor['paymentbreakdown'] );
    //exit();
    print_r(json_encode($cfo,JSON_UNESCAPED_SLASHES));
    exit;            
}

if($_GET['servicename'] == 'SearchByPickupDate')
{
    $reqpd = file_get_contents('php://input');

    $respd = json_decode($reqpd,true);

    $fromdate = '';

    $inc = '';

    $inc = $respd['invitecode'];

    $todate = '';

    $ft = '';

    $tt = '';

    $sql = '';

    $sql1 = '';

    $exccordn = '';

    $quecord1 = '';

    $quecordn = '';

    $fromdate = $respd['fromdate'];

    $todate  = $respd['todate'];

    $ft = $respd['fromtime'];

    $tt =  $respd['totime'];

    $cfos = array();

    $cfos['cord'] = array();

    $sbd = array();

    if($fromdate != '' && $todate == '' && $ft == '')
    {
        $fromdate = strtotime($fromdate);

        $ffd = '';

        $ffd = date('Y-m-d',$fromdate);

        $sql.= " AND DATE_FORMAT (c.time,'%Y-%m-%d') >= '".$ffd."'";    
    }

    if($todate != '' && $fromdate == '' && $tt == '')
    {
        $todate = strtotime($todate);

        $ttd = '';

        $ttd = date('Y-m-d',$todate);

        $sql.= " AND DATE_FORMAT(c.time,'%Y-%m-%d') <= '".$ttd."'";
    }

    if($fromdate != '' && $ft != '' && $todate == '')
    {
        $fromdate = strtotime($fromdate);

        $ffd = '';

        $ffd = date('Y-m-d',$fromdate);

        $sql.= " AND DATE_FORMAT (c.time,'%Y-%m-%d %H:%i') >= '".$ffd." ".$ft."'";
    }

    if($todate != '' && $tt != '' && $fromdate == '')
    {
        $todate = strtotime($todate);

        $ttd = '';

        $ttd = date('Y-m-d',$todate);

        $sql.= " AND DATE_FORMAT(c.time,'%Y-%m-%d %H:%i') <= '".$ttd." ".$tt." '";
    }

    if($fromdate != '' && $todate != '' && $ft != '' && $tt != '')
    {
        $fromdate = strtotime($fromdate);
        $todate = strtotime($todate);

        $ffd = '';
        $ttd = '';

        $ffd = date('Y-m-d',$fromdate);
        $ttd = date('Y-m-d',$todate);

        $sql.= " AND DATE_FORMAT(c.time,'%Y-%m-%d %H:%i') >= '".$ffd." ".$ft."' AND DATE_FORMAT(c.time,'%Y-%m-%d %H:%i') <= '".$ttd." ".$tt."'";   
    }    


    if($fromdate != '' && $todate != '' && $ft == '' && $tt == '')
    {
        $fromdate = strtotime($fromdate);
        $todate = strtotime($todate);

        $ffd = '';
        $ttd = '';

        $ffd = date('Y-m-d',$fromdate);
        $ttd = date('Y-m-d',$todate);

        $sql.= " AND DATE_FORMAT(c.time,'%Y-%m-%d') >= '".$ffd."' AND DATE_FORMAT(c.time,'%Y-%m-%d') <= '".$ttd."'";
    }  

    $quecord = "SELECT (SELECT name FROM user as u1 WHERE u1.userid=c.userid ) as chefname, 
                (SELECT phone FROM user as u1 WHERE u1.userid=c.userid ) as chefphone,
                (SELECT address FROM user as u1 WHERE u1.userid=c.userid ) as chefaddress, 
                (SELECT flatno FROM user as u1 WHERE u1.userid=c.userid ) as cheffaltno, 
                (SELECT building FROM user as u1 WHERE u1.userid=c.userid ) as chefbuilding, 
                co.cassordid as orderid,u.name as customer,u.phone as customerphone,
                u.address as customeraddress,u.flatno as customerflatno,u.building as customerbuilding,
                c.cuisinename,coi.quantity,coi.price,coi.quantity * coi.price as total,
                co.orderdate,co.invitecode,co.paymentType,coi.PaymentType as pt,co.transId,
                co.actualamount,coi.MTP,coi.cash,co.discountamount,co.couponapplied,
                (select couponcode FROM coupon WHERE couponid=co.couponid) as couponcode,
                coi.cuisineid,coi.type,c.cuisinetype,c.time,coi.pickupdate,coi.status,
                coi.code,coi.closedate,coi.usrwntDelivery as delivery,coi.IsAftertime as iat 
                FROM casseroleorder as co,user as u,cuisine as c,casseroleorderitem as coi
                WHERE c.cuisineid=coi.cuisineid and coi.cassordid=co.cassordid and 
                coi.cuisineid=c.cuisineid  and co.userid=u.userid and co.invitecode='".$inc."' and coi.status='cancel' and coi.type !='mohallabazar' ".$sql." ORDER BY co.cassordid DESC";

    $exccord = mysqli_query($conn,$quecord) or die(mysqli_error($conn));
    
    $cfo = array();

    if(mysqli_num_rows($exccord) > 0)
    {
        $cfo['cord'] = array();

        while($rowcord = mysqli_fetch_assoc( $exccord))
        {
            $cor = array();
            $data = $rowcord['cash'];

            if($rowcord['iat'] == 'Y')
            {
                $cor['aftertime'] = 'Yes';
            }
            else if($rowcord['iat'] == 'N')
            {
                $cor['aftertime'] = 'No';
            }    

            $cor['chefname'] = $rowcord['chefname'];

            $cor['chefphone'] = $rowcord['chefphone'];

          //  $cor['chefaddress'] = $rowcord['chefaddress'];

            $cor['cheffaltno'] = $rowcord['cheffaltno'];

            $cor['chefbuilding'] = $rowcord['chefbuilding'];

            $cor['orderid'] = $rowcord['orderid'];

            $cor['customer'] = $rowcord['customer'];

            $cor['customerphone'] = $rowcord['customerphone'];

        //    $cor['customeraddress'] = $rowcord['customeraddress'];

            $cor['customerflatno'] = $rowcord['customerflatno'];

            $cor['customerbuilding'] = $rowcord['customerbuilding'];

            $cor['cuisinename'] = $rowcord['cuisinename'];

            $cor['quantity'] = $rowcord['quantity'];

            $cor['price'] = $rowcord['price'];

            $cor['total'] = $rowcord['total'];

            $cor['orderdate'] = $rowcord['orderdate'];

            $cor['invitecode'] = $rowcord['invitecode'];

            if($rowcord['pt'] == 'COD' && $rowcord['MTP'] > 0)
            {
                $cor['paymenttype'] =' COD + MTP';
            }
            else if($rowcord['pt'] == 'MW')
             {
                 $cor['paymenttype'] ='MTP';
             }
            else if($rowcord['pt'] == 'OLP' && $rowcord['MTP'] > 0)
             {
                 $cor['paymenttype'] =' OLP + MTP';
             }
            else
            {
                $cor['paymenttype'] = $rowcord['paymentType'];
            }

            

            $cor['transid'] = $rowcord['transId'];

            $cor['actualamount'] = $rowcord['actualamount'];

            $cor['discountamount'] = $rowcord['discountamount'];

            $cor['couponapplied'] = $rowcord['couponapplied'];

            $cor['couponcode'] = $rowcord['couponcode'];

            $cor['cuisinetype'] = $rowcord['cuisinetype'];

            $cor['type'] = $rowcord['type'];

            if($rowcord['type'] == 'trending')
            {
                $cor['pickupdate'] = $rowcord['time'];
            }
            else
            {
                $cor['pickupdate'] = $rowcord['pickupdate'];
            } 

            $cor['status'] = $rowcord['status'];

            $cor['code'] = $rowcord['code'];

            $cor['closedate'] = $rowcord['closedate'];

            if($rowcord['pt'] == 'COD')
            {
               // echo "COD AaAdhana";
                $cor['paymentbreakdown'] = "COD : ".$data." | MTP : ".$rowcord['MTP']." | OLP : 0 | PTM : 0" ;
            }
            else if($rowcord['pt'] == 'OLP')
            {
                            //    echo "OLP";

                $cor['paymentbreakdown'] = "COD : 0 | MTP :".$rowcord['MTP']." | OLP : ".$rowcord['cash']." | PTM : 0" ;   
            }
            else if($rowcord['pt'] == 'PTM')
            {
                $cor['paymentbreakdown'] = "COD : 0 | MTP :".$rowcord['MTP']." | OLP : 0 | PTM : ".$rowcord['cash'] ;   
            }
            else 
            {
                         //       echo "MTP";

                $cor['paymentbreakdown'] = "COD : 0 | MTP :".$rowcord['MTP']." | OLP : 0 | PTM : 0";
            }

            if($rowcord['delivery'] == 'Y')
            {
                $cor['delivery'] = 'Homedelivery';
            }
            else
            {
                $cor['delivery'] = 'Pickup';
            }    

            array_push($cfo['cord'], $cor);
        }

        $cfo['status'] = 'success';
        $cfo['msg'] = 'Orders Available';
    }
    else
    {
        $cfo['status'] = 'failure';
        $cfo['msg'] = 'Not Available';
    }
    //print_r($cor['paymentbreakdown'] );
    //exit();
    print_r(json_encode($cfo,JSON_UNESCAPED_SLASHES));
    exit;            
} 
?>