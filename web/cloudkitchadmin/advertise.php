<?php //include 'inc/config.php'; $template['header_link'] = 'FORMS'; 

session_start();
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: index.php");
}
 include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user']; 
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">

<div class="bs-example">
    <div id="myModalcuisines" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Cuisine Detail</h4>
            
          </div>
          <div class="modal-body">

          <div class="row">
            <div class="col-sm-10 col-md-12 col-lg-12">
                
                <table id ="tableadcuisine" class="table table-vcenter table-striped table-hover table-borderless">
                                                <thead>
                                                     <tr>
                                                     <th>Sr</th>
                                                     <th>Cuisine Name</th>
                                                     <th>Chef Name</th>
                                                     <th>Expire Date</th>
                                                     </tr> 
                                                    </thead>
                                                  
                                                </table>
                
            </div>
          </div>

           

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           </div>
        </div>
      </div>
    </div>

    
  </div>

  <!-- Modal Chef details start -->

  <div class="bs-example">
    <div id="myModalchefs" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Chef Detail</h4>
            
          </div>
          <div class="modal-body">

          <div class="row">
            <div class="col-sm-10 col-md-12 col-lg-12">
                
                <table id ="tableadchefs" class="table table-vcenter table-striped table-hover table-borderless">
                                                <thead>
                                                     <tr>
                                                     <th>Sr</th>
                                                     <th>Chef Name</th>
                                                     </tr> 
                                                    </thead>
                                                  
                                                </table>
                
            </div>
          </div>

           

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           </div>
        </div>
      </div>
    </div>

    
  </div>

  <!-- Modal Chef details end  -->



    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Advertise</h1>
                </div>
            </div>
           
        </div>
    </div>

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <div class="block">
                <!-- Form Validation Title -->
                <div class="block-title">
                    <h2>Create Advertise</h2>
                </div>
                <!-- END Form Validation Title -->

                <!-- Form Validation Form -->
                <form id="createadv" action="" class="form-horizontal form-bordered">
                    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
                    <div class="form-group" id="selectInvite">
                        <label class="col-md-3 control-label" for="val-selmedt">Select Media Type<span class="text-danger">*</span></label>
                        <div class="col-md-6" >
                           <select id="val-selmedt" name="val-selmedt" class="form-control" onchange="selmedia();">
                               <option value="">Select Media Type</option>
                               <option value="Image">Image</option>
                               <option value="Video">Video</option>
                               <option value="Cuisine">Cuisine</option>
                               <option value="Chef">Chef</option>
                               <option value="Clickad">ClickAd</option>
                            </select>
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-desc">Description<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-desc" name="val-desc" class="form-control" placeholder="Please Enter Description">
                        </div>
                    </div>

                    <div class="form-group" id="divurl">
                        <label class="col-md-3 control-label" for="val-curl">Url<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-curl" name="val-curl" class="form-control" placeholder="Please Enter Url">
                        </div>
                    </div>

                    <div class="form-group" id="vid">
                        <label class="col-md-3 control-label" for="val-vid">Video<span class="text-danger">*</span></label>
                        <div id="vid0">
                        <div class="col-md-6" >
                            <input type="text" id="val-vid0" name="val-vid" class="form-control" placeholder="Please Enter Video Url">
                        </div>
                        </div>

                        <div class="col-md-12" id="showmv">
                               <input type="button" id="add_more_video" class="btn btn-primary" value="Add More Videos" style="float:right"/>
                        </div>
                    </div>

                    
                    
                    <div class="form-group" id="img">
                        <label class="col-md-3 control-label" for="val-images">Image</label>
                        <div class="col-md-6" id="filediv" > 
                           <div id="filediv0">
                               
                               <div class="row">
                                 
                                   <div class="col-md-9">
                                   <input name="file[]" type="file" id="file" onclick="remove()" />
                                   </div>
                                   <div class="col-md-3">
                                    <!-- <button type="button" id="rm" class="btn btn-primary" style="color: #ffffff;border-radius: 26px;display:none" onclick="btnimgaeremove(0)">Remove Image</button> -->
                                    <button type="button" id="rm" class="btn btn-primary" style="color: #ffffff;border-radius: 26px;display:none" onclick="btnimgaeremovefirst(0)">Remove Image</button> 
                                       
                                   </div>
                                   
                               </div>
                               
                                
                          
                          </div> 
                        </div>
                            <div class="col-md-12" id="showmi">
                               <input type="button" id="add_more" class="btn btn-primary" value="Add More Files" style="float:right"/>
                            </div>
                    </div>

                    <!-- Select cuisine starts -->

                    <div class="row" id="col">

                      <div class="col-sm-10 col-md-12 col-lg-12">
            
                          <div class="block">

    
                          <div class="block-title">
                              <h2>Select Cuisines</h2>
                              
                          </div>
           

      
                          <table id ="tableclList" class="table table-vcenter table-striped table-hover table-borderless">

                                                          <thead>
                                                           <tr>
                                                           <th>Sr</th>
                                                           <th>Choose</th>
                                                           <th>Chef</th>
                                                           <th>Cuisine Name</th>
                                                           <th>Expire Date</th>
                                                           </tr> 
                                                          </thead>  
                            
                          </table>
            

                       </div>

                      </div>
                    </div>

                    <!-- Select cuisine ends -->

                    <!-- Select chef starts -->

                    <div class="row" id="cfd">

                      <div class="col-sm-10 col-md-12 col-lg-12">
            
                          <div class="block">

    
                          <div class="block-title">
                              <h2>Select Chef</h2>
                              
                          </div>
           

      
                          <table id ="tablecfList" class="table table-vcenter table-striped table-hover table-borderless">

                                                          <thead>
                                                           <tr>
                                                           <th>Sr</th>
                                                           <th>Choose</th>
                                                           <th>Chef</th>
                                                           </tr> 
                                                          </thead>  
                            
                          </table>
            

                       </div>

                      </div>
                    </div>

                    <!-- Select chef ends -->
                    
                    
                    <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-3">
                            <button type="button" onclick="createadvertise();" class="btn btn-effect-ripple btn-primary">Submit</button>
                            <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Form Validation Form -->

               
            </div>
            <!-- END Form Validation Block -->
        </div>
    </div>

    <div class="row">

            <div class="col-sm-10 col-md-12 col-lg-12">
            
                <div class="block">

    
                <div class="block-title">
                    <h2>Advertises</h2>
                    <!-- <button type="button" id="exptexcel" class="btn btn-rounded btn-success" style="float:right">Export</button> -->
                </div>
           

      
                <table id ="tableadList" class="table table-vcenter table-striped table-hover table-borderless">

                                                <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Caption</th>
                                                 <th>Mediatype</th>
                                                 <th>Media</th>
                                                 <th>Status</th>
                                                 <th>Action</th>
                                                 </tr> 
                                                </thead>  
                  
                </table>
            

                </div>

            </div>
    </div>

    <!-- Cuisine Details -->

    


    <!-- END Form Validation Content -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script type="text/javascript" src="js/CasseroleService.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $("#loading").hide();
    $("#img").hide();
    $("#vid").hide();
    $("#col").hide();
    $("#cfd").hide();
    $("#divurl").hide();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    $('#tableclList').dataTable().fnClearTable();
        $('#tableclList').dataTable().fnDraw();
        $('#tableclList').dataTable().fnDestroy();

        $('#tableadList').dataTable().fnClearTable();
        $('#tableadList').dataTable().fnDraw();
        $('#tableadList').dataTable().fnDestroy();

       advertiselist();

      });</script>

<script type="text/javascript">
    
    function remove()
    {
        $("#rm").css("display","block");
    }

</script>

<script type="text/javascript">
    
    function btnimgaeremovefirst(p1)
    {
        console.log("Par"+p1);
        
        var divd = '';

        divd += '<div id="filediv'+p1+'">'+
                               
                               '<div class="row">'+
                                 
                                   '<div class="col-md-9">'+
                                   '<input name="file[]" type="file" id="file" onclick="remove()" />'+
                                   '</div>'+
                                   '<div class="col-md-3">'+
                                   '<button type="button" id="rm" class="btn btn-primary" style="color: #ffffff;border-radius: 26px;display:none" onclick="btnimgaeremovefirst(0)">Remove Image</button>'+ 
                                   '</div>'+
                                   
                               '</div>'+
                               
                                
                          
                '</div>';

        $("#filediv").html(divd);        


    }

</script>

<script type="text/javascript">
    
    function selmedia()
    {
        var mtyp = $("#val-selmedt").val();

        console.log("Type"+mtyp);

        if(mtyp == 'Image')
        {
            $("#img").show();
        }
        else
        {
            $("#img").hide();
        }    

        if(mtyp == 'Video')
        {
            $("#vid").show();
        }
        else
        {
            $("#vid").hide();
        } 

        if(mtyp == 'Cuisine')
        {
            $("#img").show();

            $("#showmi").hide();

            cuisinelist();
        }
        else
        {
          $("#col").hide();
        }

        if(mtyp == 'Chef')
        {
            $("#img").show();

            $("#showmi").hide();

            cheflist();
        }
        else
        {
            $("#cfd").hide();
        }

        if(mtyp == 'Clickad')
        {
            $("#img").show();

            $("#divurl").show();
        }
        else
        {
            //$("#img").hide();

            $("#divurl").hide();
        }  




    }

</script>


<script type="text/javascript">
    
    function createadvertise()
    {
        var mediatype = '';

        mediatype = $("#val-selmedt").val();

        var desc = '';

        desc = $("#val-desc").val();

        var valid = 0;

        if(mediatype == 'Image')
        {
            var m = $("#file").val();

            if(m == '')
            {
                valid = valid  +  1;

                alert('Please choose image');

                return false;
            }
        }

        if(mediatype == 'Video') 
        {
            var v = $("#val-vid0").val();

            if(v == '')
            {
                valid = valid + 1;

                alert('Please choose video');

                return false;
            }    
        }

        if(mediatype == 'Cuisine')
        {
           if(!$("input[name='cuisine']:checked").val())
            {
               //jeans="null";

               valid = valid + 1;

               alert('Please choose cuisine');

               return false;
            }

            var m = $("#file").val();

            if(m == '')
            {
                valid = valid  +  1;

                alert('Please choose image');

                return false;
            }
        } 

        if(mediatype == 'Chef')
        {
           if(!$("input[name='chefs']:checked").val())
           {
              valid++;

              alert('Please choose chef');

              return false;
           }

           var cm = $("#file").val();

           if(cm == '')
           {
              valid++;

              alert('Please choose image');

              return false;
           }   
        }

        if(mediatype == 'Clickad')
        {
            var m = $("#file").val();

            if(m == '')
            {
                valid = valid  +  1;

                alert('Please choose image');

                return false;
            }

            var curl = $("#val-curl").val();

            if(curl == '')
            {
               valid = valid + 1;

               alert('Please enter url');

               return false;
            }  
        } 


        if(valid == 0)
        {
            //console.log("Hii");

            advertise();
        }    




    }

</script>

<script type="text/javascript">
  
  function cheflist()
  {
     var ivc = $("#invitecode").val();

     $.ajax({
               url:'cheflistservice.php?servicename=ChefList',
               type: 'POST',
               datatype: 'JSON',
               data: JSON.stringify({"invitecode":ivc}),
               async: false,
               success: function(rescf)
               {
                  console.log("Chefs"+JSON.stringify(rescf));

                  rscf = JSON.parse(rescf);

                  if(rscf.status == 'success')
                  {
                       $("#cfd").show();

                       var cf = new Array();

                       for(var f=0;f<rscf.chefs.length;f++)
                       {
                          cf[f] = new Array();

                          cf[f][0] = f+1;

                          cf[f][1] = '<input type="checkbox" id="chefs" name="chefs" value="'+rscf.chefs[f].chefid+'">';

                          cf[f][2] = rscf.chefs[f].chefname;
                       }

                       $('#tablecfList').dataTable({
                        "aaData": cf,
                        "bDestroy": true,
                        "autoWidth": true,
                        "aoColumns": [
                            null,
                            null,
                            { "orderSequence": [ "desc", "asc", "asc" ] }
                            
                        ]
                       }); 
                  }
                  else
                  {

                  }  
               }

     })
  }

</script>

<script type="text/javascript">
    
    function cuisinelist()
    {
       
        var ivc = $("#invitecode").val();

         console.log("Hiid"+ivc);

         $.ajax({
                  url:'advertise_service.php?servicename=CuisineList',
                  type: 'POST',
                  datatype: 'JSON',
                  data:JSON.stringify({"invitecode":ivc}),
                  async: false,
                  success: function(rescl)
                  {
                     console.log("Josn"+JSON.stringify(rescl));

                     var resc = JSON.parse(rescl);

                     if(resc.status == 'success')
                     {
                        $("#col").show();

                        var cu = new Array();

                        for(var c=0;c<resc.cuisines.length;c++)
                        {
                           cu[c] = new Array();

                           cu[c][0] = c+1;

                           cu[c][1] =  '<input type="checkbox" id="cuisine" name="cuisine" value="'+resc.cuisines[c].cuisineid+'">';

                           cu[c][2] = resc.cuisines[c].chefname;

                           cu[c][3] = resc.cuisines[c].cuisinename;

                           cu[c][4] = resc.cuisines[c].endtime;
                        }

                        $('#tableclList').dataTable({
                        "aaData": cu,
                        "bDestroy": true,
                        "autoWidth": true,
                        "aoColumns": [
                            null,
                            null,
                            { "orderSequence": [ "desc", "asc", "asc" ] },
                            { "orderSequence": [ "desc", "asc", "asc" ] },
                            { "orderSequence": [ "desc", "asc", "asc" ] }
                        ]
                       });  
                     } 
                  }  
         });
    }

</script>

<!-- Advertise List available here -->

<script type="text/javascript">
  
  function advertiselist()
  {
     console.log("Hii D");

     var ivc = $("#invitecode").val();

     $.ajax({
               url:'advertise_service.php?servicename=ADVERTISE-LIST',
               type: 'POST',
               datatype: 'JSON',
               data: JSON.stringify({"invitecode":ivc}),
               async: false,
               success: function(radl)
               {
                 console.log("Json ADL"+JSON.stringify(radl));

                 $('#tableadList').dataTable().fnClearTable();
                 $('#tableadList').dataTable().fnDraw();
                 $('#tableadList').dataTable().fnDestroy();

                 var rsad = JSON.parse(radl);



                 if(rsad.status == 'success')
                 {
                     var ad = new Array();

                     for(var a=0;a<rsad.ads.length;a++)
                     {
                        ad[a] = new Array();

                        ad[a][0] = a+1;

                        ad[a][1] = rsad.ads[a].description;

                       //<button type="button" class="btn btn-rounded btn-info">Info</button>

                        if(rsad.ads[a].mediatype == 'Cuisine')
                        {
                           ad[a][2] = '<a href="#myModalcuisines" onclick="opencuisinedetail(\''+rsad.ads[a].advid+'\')" data-toggle="modal">'+rsad.ads[a].mediatype+'</a>';
                        }
                        else if(rsad.ads[a].mediatype == 'Chef')
                        {
                            ad[a][2] = '<a href="#myModalchefs" onclick="openchefdetail(\''+rsad.ads[a].advid+'\')" data-toggle="modal">'+rsad.ads[a].mediatype+'</a>';
                        }  
                        else
                        {
                            ad[a][2] = rsad.ads[a].mediatype; 
                        }  

                        if(rsad.ads[a].mediatype == 'Video')
                        {
                           var vidurl = rsad.ads[a].url;

                            vidurl = vidurl.split("=");

                            var vdurl = '';

                           vdurl = "https://www.youtube.com/embed/"+vidurl[1];

                             ad[a][3] = '<iframe width="200" height="200" src="'+vdurl+'" frameborder="0" allowfullscreen></iframe>';
                        }
                        else
                        {
                            var vdurl = '';

                            vdurl = rsad.ads[a].url;  

                             ad[a][3] = '<img class="img-rounded" src="'+vdurl+'" style="width:200px;height:200px;">';
                        }

                       // ad[a][4] = rsad.ads[a].description;

                        //<button type="button" onclick="inactivecuisine(1824);" class="btn btn-rounded btn-primary">Active</button>

                        if(rsad.ads[a].isActive == 'Y')
                        {
                          ad[a][4] = '<button type="button" onclick="activation(\''+rsad.ads[a].advid+'\',\''+rsad.ads[a].isActive+'\')" class="btn btn-rounded btn-primary">Active</button>';
                        }else{
                          ad[a][4] = '<button type="button"  onclick="activation(\''+rsad.ads[a].advid+'\',\''+rsad.ads[a].isActive+'\')" class="btn btn-rounded btn-primary">InActive</button>';
                        } 

                        if(rsad.ads[a].mediatype == 'Cuisine')
                        {
                            ad[a][5] = '<button onclick="editcuisine(\''+rsad.ads[a].advid+'\',\''+rsad.ads[a].mediatype+'\',\''+rsad.ads[a].url+'\',\''+rsad.ads[a].description+'\');" type="button" class="btn btn-rounded btn-info">Edit</button><button onclick="deletead(\''+rsad.ads[a].advid+'\',\''+rsad.ads[a].mediatype+'\')" type="button" class="btn btn-rounded btn-danger">Delete</button>';
                        }
                        else if(rsad.ads[a].mediatype == 'Chef')
                        {
                         
                          ad[a][5] = '<button onclick="editchef(\''+rsad.ads[a].advid+'\',\''+rsad.ads[a].mediatype+'\',\''+rsad.ads[a].url+'\',\''+rsad.ads[a].description+'\');" type="button" class="btn btn-rounded btn-info">Edit</button><button onclick="deletead(\''+rsad.ads[a].advid+'\',\''+rsad.ads[a].mediatype+'\')" type="button" class="btn btn-rounded btn-danger">Delete</button>';
                        }  
                        else
                        {
                           ad[a][5] = '<button type="button" onclick="editadvertise(\''+rsad.ads[a].advid+'\',\''+rsad.ads[a].mediatype+'\',\''+rsad.ads[a].url+'\',\''+rsad.ads[a].description+'\',\''+rsad.ads[a].curl+'\')" class="btn btn-rounded btn-info">Edit</button><button onclick="deletead(\''+rsad.ads[a].advid+'\',\''+rsad.ads[a].mediatype+'\')" type="button" class="btn btn-rounded btn-danger">Delete</button>'; 
                        }  



                       
                     }

                     $('#tableadList').dataTable({
                        "aaData": ad,
                        "bDestroy": true,
                        "autoWidth": true,
                        "aoColumns": [
                            null,
                            { "orderSequence": [ "desc", "asc", "asc" ] },
                            { "orderSequence": [ "desc", "asc", "asc" ] },
                            { "orderSequence": [ "desc", "asc", "asc" ] },
                            { "orderSequence": [ "desc", "asc", "asc" ] },
                            null
                           
                        ]
                       });


                 } 
               } 

     });


  }

</script>

<script type="text/javascript">
  
function openchefdetail(aid)
{
   console.log("Adid"+aid);

   var reqad = {"advid":aid};

   $.ajax({

           url:'../Casserole/Services/service_1.6.php?service_name=ADVERTISECHEF',
           type: 'POST',
           datatype: 'JSON',
           data: JSON.stringify(reqad),
           async: false,
           success: function(rsadf)
           {
              console.log("ADF"+JSON.stringify(rsadf));

              var rsf = JSON.parse(rsadf);

             $('#tableadchefs').dataTable().fnClearTable();
             $('#tableadchefs').dataTable().fnDraw();
             $('#tableadchefs').dataTable().fnDestroy();

             if(rsf.status == 'success')
             {
                var fc = new Array();

                for(var f=0;f<rsf.chefs.length;f++)
                {
                   fc[f] = new Array();

                   fc[f][0] = f+1;

                   fc[f][1] = rsf.chefs[f].name;

                }

                $('#tableadchefs').dataTable({
                        "aaData": fc,
                        "bDestroy": true,
                        "autoWidth": true,
                        "aoColumns": [
                            { "orderSequence": [ "desc", "asc", "asc" ] },
                            { "orderSequence": [ "desc", "asc", "asc" ] }
                            
                        ]
                       });


             } 


           }

   });
}

</script>

<script type="text/javascript">
  
  function opencuisinedetail(aid)
  {
     console.log("Adid"+aid);

     var reqad = {"advid":aid};

     $.ajax({
               url:'../Casserole/Services/service_1.6.php?service_name=ADVERTISECUISINE',
               type: 'POST',
               datatype: 'JSON',
               data: JSON.stringify(reqad),
               async: false,
               success: function(rsadc)
               {
                  console.log("Json Ad"+JSON.stringify(rsadc));

                  var adc = JSON.parse(rsadc);

                   $('#tableadcuisine').dataTable().fnClearTable();
                   $('#tableadcuisine').dataTable().fnDraw();
                   $('#tableadcuisine').dataTable().fnDestroy();

                  if(adc.status == 'success')
                  {
                     var ca = new Array();

                     for(var d=0;d<adc.Cuisines.length;d++)
                     {
                        ca[d] = new Array();

                        ca[d][0] = d+1;

                        ca[d][1] = adc.Cuisines[d].cuiname;

                        ca[d][2] = adc.Cuisines[d].chefname;

                        ca[d][3] = adc.Cuisines[d].closedate;                        
                     }

                     $('#tableadcuisine').dataTable({
                        "aaData": ca,
                        "bDestroy": true,
                        "autoWidth": true,
                        "aoColumns": [
                            { "orderSequence": [ "desc", "asc", "asc" ] },
                            { "orderSequence": [ "desc", "asc", "asc" ] },
                            { "orderSequence": [ "desc", "asc", "asc" ] },
                            { "orderSequence": [ "desc", "asc", "asc" ] }
                        ]
                       });


                  }  
               }   
     })
  }

</script>

<script type="text/javascript">
  
  function activation(adid,st)
  {
     console.log("Adid"+adid+"St"+st);

     var reqact = {"adid":adid,"status":st};

     $.ajax({
               url:'advertise_service.php?servicename=activation',
               type:'POST',
               datatype: 'JSON',
               data: JSON.stringify(reqact),
               async: false,
               success: function(rsact)
               {
                  console.log("Act"+JSON.stringify(rsact));

                  var rsac = JSON.parse(rsact);

                  if(rsac.status == 'success')
                  {
                     location.reload();
                  } 
               }

     })
  }

</script>

<script type="text/javascript">
  
  function editadvertise(adid,mtype,url,desc,curl)
  {
     console.log("Adid"+adid+"Mtype"+mtype);

     $('#selectInvite').append('<form action="edit-advertise.php" id="adf" name="adf" method="post" style="display:none;"><input type="text" name="adid" value="' + adid + '" /><input type="text" name="mtype" value="' + mtype + '" /><input type="text" name="url" value="'+url+'" /><input type="text" name="desc" value="'+desc+'" ><input type="text" name="curl" value="'+curl+'" ></form>');

      $('#adf').submit();
  }

  </script>

<script type="text/javascript">
  
  function editchef(adid,mtype,url,desc)
  {
      console.log("Aid"+adid+"Mtp"+mtype+"Url"+url+"Desc"+desc);

      $('#selectInvite').append('<form action="edit-chef-advertise.php" id="adcf" name="adf" method="post" style="display:none;"><input type="text" name="adid" value="' + adid + '" /><input type="text" name="mtype" value="' + mtype + '" /><input type="text" name="url" value="'+url+'" /><input type="text" name="desc" value="'+desc+'" ></form>');

      $('#adcf').submit();
  }

</script>

<script type="text/javascript">
  
  function editcuisine(adid,mtype,url,desc)
  {
     console.log(" Adid :"+adid+" Mtype :"+mtype+" Url :"+url+" Desc :"+desc);

     $('#selectInvite').append('<form action="edit-cuisine-advertise.php" id="adcu" name="adcu" method="post" style="display:none;"><input type="text" name="adid" value="' + adid + '" /><input type="text" name="mtype" value="' + mtype + '" /><input type="text" name="url" value="'+url+'" /><input type="text" name="desc" value="'+desc+'" ></form>');

      $('#adcu').submit();
  }

</script>

<script type="text/javascript">

function redirect()
        {
           location.reload();
        }
  
  function deletead(adid,type)
  {
     console.log("Adid"+adid+"mtyp"+type);

     var reqda = {"adid":adid,"mtype":type};

     $.ajax({
               url:'advertise_service.php?servicename=delete-ad',
               type: 'POST',
               datatype: 'JSON',
               data: JSON.stringify(reqda),
               async: false,
               success: function(rsd)
               {
                  var rd = JSON.parse(rsd);

                  if(rd.status == 'success')
                  {
                      var n = noty({
                     layout: 'bottomRight',
                     text: 'Removed Advertisement Successfully',
                     theme:'relax',type: 'success',
                     timeout : '3000',
                     animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // document.getElementById("divcreateexp").style.display="none" ;
                                //document.getElementById("confirmView").style.display="block" ;
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });

                    setTimeout(redirect,1000);
                  }
                  else
                  {
                      var n = noty({
                         layout: 'bottomRight',
                         text: 'Failed to delete advertise',
                         theme:'relax',type: 'error',
                         timeout : '3000',
                        animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });
                  }  
               }
     })
  }

</script>

<?php include 'inc/template_end.php'; ?>