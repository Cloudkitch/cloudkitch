<?php

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['servicename'] == 'Customer-Order-Report')
{
	$reqcol = file_get_contents('php://input');

	$rescol = json_decode($reqcol,true);

	$userid = '';

	$userid = $rescol['userid'];

	$cor = array();

	$quecol = "SELECT MONTH(orderdate) as mt,MONTHNAME(orderdate) as month,count(cassordid) as orders FROM casseroleorder WHERE userid='".$userid."' group by mt";
	$exccol = mysqli_query($conn,$quecol) or die(mysqli_error($conn));

	if(mysqli_num_rows($exccol) > 0)
	{
		$cor['customerorders'] = array();

		while($rowcol = mysqli_fetch_assoc($exccol))
		{
			$co = array();

			$co['mt'] = $rowcol['mt'];

			$co['month'] = $rowcol['month'];

			$co['orders'] = $rowcol['orders'];

			array_push($cor['customerorders'], $co);
		}

		$cor['status'] = 'success';
		$cor['msg'] = 'Data available';	
	}
	else
	{
		$cor['status'] = 'failure';
		$cor['msg'] = 'Data not available';
	}

	print_r(json_encode($cor));
	exit;	
}

if($_GET['servicename'] == 'Chef-Cuisines-Report')
{
	$reqcfc = file_get_contents('php://input');

	$rescfc = json_decode($reqcfc,true);

	$chefid = '';

	$chefid = $rescfc['chefid'];

	$cfcr = array();

	$quecfcr = "SELECT MONTH(enddate) as mt,MONTHNAME(enddate) as month,count(cuisineid) as cuisines 
				FROM cuisine WHERE userid='".$chefid."' group by mt";

	$exccfcr = mysqli_query($conn,$quecfcr) or die(mysqli_error($conn));

	if(mysqli_num_rows($exccfcr) > 0)
	{
		$cfcr['chefdishes'] = array();

		while($rowcfcr = mysqli_fetch_assoc($exccfcr))
		{
			$cr = array();

			$cr['mt'] = $rowcfcr['mt'];

			$cr['month'] = $rowcfcr['month'];

			$cr['cuisines'] = $rowcfcr['cuisines'];

			array_push($cfcr['chefdishes'], $cr);
		}

		$cfcr['status'] = 'success';
		$cfcr['msg'] = 'Data available';

	}
	else
	{
		$cfcr['status'] = 'failure';
		$cfcr['msg'] = 'Data not available';
	}

	print_r(json_encode($cfcr));
	exit;	
}

if($_GET['servicename'] == 'Chef-Cuisines-Report-Grid')
{
	$reqgr = file_get_contents('php://input');

	$resgr = json_decode($reqgr,true);

	$users = $resgr['users'];

	$fromdate = '';

	$todate = '';

	$invitecode = '';

	$fromdate = $resgr['fromdate'];

	$todate = $resgr['todate'];

	$invitecode = $resgr['invitecode'];

	
	$cfcr = array();

	$cfcr['chefdishes'] = array();

	
	$cnt = 0;

	$sql = '';
		
		

		//echo $users[$u];

		// exit;

		if($fromdate !='' && $todate == '')
		{
			$fromdate = strtotime($fromdate);

            $ffd = '';

            $ffd = date('Y-m-d',$fromdate);

            $sql.= " AND DATE_FORMAT (enddate,'%Y-%m-%d') >= '".$ffd."'";
		}

		if($todate != '' && $fromdate == '')
		{
			$todate = strtotime($todate);

            $ttd = '';

            $ttd = date('Y-m-d',$todate);

            $sql.= " AND DATE_FORMAT(enddate,'%Y-%m-%d') <= '".$ttd."' ";
		}

		if($fromdate != ''  && $todate != '')
		{
			$fromdate = strtotime($fromdate);
            $todate = strtotime($todate);

            $ffd = '';
            $ttd = '';

            $ffd = date('Y-m-d',$fromdate);
            $ttd = date('Y-m-d',$todate);

            $sql.= " AND DATE_FORMAT(enddate,'%Y-%m-%d') >= '".$ffd."'  AND DATE_FORMAT(enddate,'%Y-%m-%d') <= '".$ttd."'";
		}



	//echo "COunt".count($users);

	
	for($u=0;$u<count($users);$u++)
	{


		//echo $sql."<br>";




		$queuser = "SELECT name FROM user WHERE userid='".$users[$u]."'";
		$excuser = mysqli_query($conn,$queuser) or die(mysqli_error($conn));
		$rsuser = mysqli_fetch_assoc($excuser);

		$us = array();	

		$us['chef'] = $rsuser['name'];

		$us['chefdata'] = array();

		$quecfcr = "SELECT MONTH(enddate) as mt,MONTHNAME(enddate) as month,count(cuisineid) as cuisines 
				FROM cuisine WHERE userid='".$users[$u]."' ".$sql."  group by mt";

		// echo $quecfcr."<br>";
		
		// exit;		

		$exccfcr = mysqli_query($conn,$quecfcr) or die(mysqli_error($conn));

		if(mysqli_num_rows($exccfcr) > 0)
		{
			//echo "Name<br>".$rsuser['name'];

			

			

			
			
			//echo "Entered here<br>";
			$cnt = $cnt + 1;
			
			while($rowcfcr = mysqli_fetch_assoc($exccfcr))
			{
				$cr = array();

				$cr['users'] = $rowcfcr['mt'];

				$cr['month'] = $rowcfcr['month'];

				$cr['cuisines'] = $rowcfcr['cuisines'];

				array_push($us['chefdata'], $cr);
			}


			$cfcr['status'] = 'success';
			$cfcr['msg'] = 'Data available';
			array_push($cfcr['chefdishes'],$us);
		}

		if($cnt == 0)
		{
			$cfcr['status'] = 'failure';
			$cfcr['msg'] = 'Data not available';
		}	


	}

	print_r(json_encode($cfcr));
	exit;	
}

if($_GET['servicename'] == 'Chef-Cuisines-Report-Deatil')
{
	$reqgr = file_get_contents('php://input');

	$resgr = json_decode($reqgr,true);

	$users = $resgr['users'];

	$fromdate = '';

	$todate = '';

	$invitecode = '';

	$fromdate = $resgr['fromdate'];

	$todate = $resgr['todate'];

	$cfcr = array();

	$sql = '';

	$cfcr['chefdishes'] = array();

		if($fromdate !='' && $todate == '')
		{
			$fromdate = strtotime($fromdate);

            $ffd = '';

            $ffd = date('Y-m-d',$fromdate);

            $sql.= " AND DATE_FORMAT (enddate,'%Y-%m-%d') >= '".$ffd."'";
		}

		if($todate != '' && $fromdate == '')
		{
			$todate = strtotime($todate);

            $ttd = '';

            $ttd = date('Y-m-d',$todate);

            $sql.= " AND DATE_FORMAT(enddate,'%Y-%m-%d') <= '".$ttd."' ";
		}

		if($fromdate != ''  && $todate != '')
		{
			$fromdate = strtotime($fromdate);
            $todate = strtotime($todate);

            $ffd = '';
            $ttd = '';

            $ffd = date('Y-m-d',$fromdate);
            $ttd = date('Y-m-d',$todate);

            $sql.= " AND DATE_FORMAT(enddate,'%Y-%m-%d') >= '".$ffd."'  AND DATE_FORMAT(enddate,'%Y-%m-%d') <= '".$ttd."'";
		}

	for($u=0;$u<count($users);$u++)
	{
		$queuser = "SELECT name FROM user WHERE userid='".$users[$u]."'";
		$excuser = mysqli_query($conn,$queuser) or die(mysqli_error($conn));
		$rsuser = mysqli_fetch_assoc($excuser);

		$us = array();	

		//$us['chefdata'] = array();

		$us['chef'] = $rsuser['name'];

		$quecfcr = "SELECT count(cuisineid) as cuisines FROM cuisine WHERE userid='".$users[$u]."' ".$sql." ";
		$exccfcr = mysqli_query($conn,$quecfcr) or die(mysqli_error($conn));

		if(mysqli_num_rows($exccfcr) > 0)
		{
			$rscs = mysqli_fetch_assoc($exccfcr);

			$us['cuisines'] = $rscs['cuisines'];

			array_push($cfcr['chefdishes'], $us);

			$cfcr['status'] = 'success';

			$cfcr['msg'] = 'Data available';


		}
		else
		{
			//$us['cuisines'] = "0";
			$cfcr['status'] = 'failure';
			$cfcr['msg'] = 'Data not available';
		}
	
	}

	print_r(json_encode($cfcr));
	exit;
}	

if($_GET['servicename'] == 'Customer-Order-Report-Deatil')
{
	$reqgr = file_get_contents('php://input');

	$resgr = json_decode($reqgr,true);

	$users = $resgr['users'];

	$fromdate = '';

	$todate = '';

	$invitecode = '';

	$fromdate = $resgr['fromdate'];

	$todate = $resgr['todate'];

	$cfcr = array();

	$sql = '';

	$cfcr['corders'] = array();

		if($fromdate !='' && $todate == '')
		{
			$fromdate = strtotime($fromdate);

            $ffd = '';

            $ffd = date('Y-m-d',$fromdate);

            $sql.= " AND DATE_FORMAT (orderdate,'%Y-%m-%d') >= '".$ffd."'";
		}

		if($todate != '' && $fromdate == '')
		{
			$todate = strtotime($todate);

            $ttd = '';

            $ttd = date('Y-m-d',$todate);

            $sql.= " AND DATE_FORMAT(orderdate,'%Y-%m-%d') <= '".$ttd."' ";
		}

		if($fromdate != ''  && $todate != '')
		{
			$fromdate = strtotime($fromdate);
            $todate = strtotime($todate);

            $ffd = '';
            $ttd = '';

            $ffd = date('Y-m-d',$fromdate);
            $ttd = date('Y-m-d',$todate);

            $sql.= " AND DATE_FORMAT(orderdate,'%Y-%m-%d') >= '".$ffd."'  AND DATE_FORMAT(orderdate,'%Y-%m-%d') <= '".$ttd."'";
		}

	for($u=0;$u<count($users);$u++)
	{
		$queuser = "SELECT name FROM user WHERE userid='".$users[$u]."'";
		$excuser = mysqli_query($conn,$queuser) or die(mysqli_error($conn));
		$rsuser = mysqli_fetch_assoc($excuser);

		$us = array();	

		//$us['chefdata'] = array();

		$us['user'] = $rsuser['name'];

		$quecfcr = "SELECT count(cassordid) as orders FROM casseroleorder WHERE userid='".$users[$u]."' ".$sql." ";
		$exccfcr = mysqli_query($conn,$quecfcr) or die(mysqli_error($conn));

		if(mysqli_num_rows($exccfcr) > 0)
		{
			$rscs = mysqli_fetch_assoc($exccfcr);

			$us['orders'] = $rscs['orders'];

			array_push($cfcr['corders'], $us);

			$cfcr['status'] = 'success';

			$cfcr['msg'] = 'Data available';


		}
		else
		{
			//$us['cuisines'] = "0";
			$cfcr['status'] = 'failure';
			$cfcr['msg'] = 'Data not available';
		}
	
	}

	print_r(json_encode($cfcr));
	exit;
}


?>