<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <!-- <a href="#myModal" class="btn btn-lg btn-primary" data-toggle="modal">Launch
      Demo Modal</a> -->
    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Order Item List</h4>
          </div>
          <div class="modal-body">

          <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tableOrderItemList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Cuisine Name</th>
                                                 <th>Chef Name</th>
                                                 <th>Price</th>
                                                 <th>Quantity</th>
                                                 <th>Type</th>
                                                 <th>Status</th>
                                                 <th>Code</th>
                                                 <th>Closed Date</th>
                                               
                                                                                              
                                                </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>

           

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>
        </div>
      </div>
    </div>

    
  </div>

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Item Order List</h1>
                    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
    <!-- <button type="button" onclick="exporttoexcel()" class="btn btn-rounded btn-success" style="float:right">Export</button> -->
	
	<div class="form-group">

<div class="col-md-6">
<label class="col-md-3 control-label" for="example-daterange1">Pickup Date :</label>
<div class="input-group input-daterange" data-date-format="yyyy/mm/dd">
<input type="text" id="frmdt" name="frmdt" class="form-control" placeholder="From">
<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
<input type="text" id="todt" name="todt" class="form-control" placeholder="To">


</div>



</div>
<div class="col-md-3" >
   <button type="button" onclick="searchbypickupvendor();" class="btn btn-effect-ripple btn-info" style="overflow: hidden; position: relative;">Find</button>
   <button type="button" onclick="reset();" class="btn btn-effect-ripple btn-success" style="overflow: hidden; position: relative;">Reset</button> 
</div>

</div>

<br><br>
    <button type="button" id="exptexcel" class="btn btn-rounded btn-success" style="float:right">Export</button>
    <br><br>
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tableVendorOrderList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Order Id</th>
                                                 <th>Vendor Name</th>
                                                 <th>Vendor No.</th>
                                                 <th>Vendor Addr</th>
                                                 <th>Cust Name</th>
                                                 <th>Cust No.</th>
                                                 <th>Cust Addr</th>
                                                 <th>Item Name</th>
                                                 <th>Item Type</th>
                                                 <th>Quantity</th>
                                                 <th>Price</th>
                                                 <th>Total</th>
                                                 <th>Commission</th>
                                                 <th>Payment Mode</th>
                                                 <th>Pickup Date</th>
                                                 <th>Delivery Mode</th>
                                                 <th>Status</th>
                                                 <th>Payment Breakdown</th>
                                                 <th>Cancelled Aftertime</th>
                                                 <!-- <th></th> -->
                                                
                                                </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->



<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script src="js/CasseroleService.js"></script>

<!-- <script src="js/order.js"></script> -->


<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    $('#tableChefOrderList').dataTable().fnClearTable();
        $('#tableChefOrderList').dataTable().fnDraw();
        $('#tableChefOrderList').dataTable().fnDestroy();

        // $('#tableOrderItemList').dataTable().fnClearTable();
        // $('#tableOrderItemList').dataTable().fnDraw();
        // $('#tableOrderItemList').dataTable().fnDestroy();

        
    // var type = '<?php 
    //echo $_SESSION['info']['type']; ?>';
    // var wedcode = '';
    // wedcode = '2616';
    // console.log("My Type Is :"+type);
    // if(type == "admin")
    // {
    //     getWeddingList();
    // }

    //invitationlist();
    //customerlist();
    cheforderlist();
    // if(type == "subadmin") 
    // {
    //    getWeddingListBySubadmin(wedcode); 
    // }   
    });</script>

    <script type="text/javascript">
        
    function cheforderlist()
    {

      var invc = '';

      invc = $("#invitecode").val();  

      var cfo = {"invitecode":invc};

        $.ajax({
                 url: 'service.php?servicename=vendororders',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(cfo),
                 async: false,
                 success: function (col)
                 {
                     //console.log("Cheforders Response"+JSON.stringify(col));

                     var rscol = JSON.parse(col);

                     var co = new Array();

                     for(var oc=0;oc<rscol.cord.length;oc++)
                     {
                        co[oc] = new Array();

                        co[oc][0] = oc+1;

                        co[oc][1] = rscol.cord[oc].orderid;

                        co[oc][2] = rscol.cord[oc].vendorname;

                        co[oc][3] = rscol.cord[oc].vendorphone;

                        if(rscol.cord[oc].vendoraddress != '')
                        {
                            co[oc][4] = rscol.cord[oc].vendoraddress;
                        }
                        else
                        {
                            co[oc][4] = rscol.cord[oc].vendorfaltno + ' - ' + rscol.cord[oc].vendorbuilding;
                        }

                        co[oc][5] = rscol.cord[oc].customer;

                        co[oc][6] = rscol.cord[oc].customerphone;

                        if(rscol.cord[oc].customeraddress != '')
                        {
                             co[oc][7] = rscol.cord[oc].customeraddress;
                        }
                        else
                        {
                             co[oc][7] = rscol.cord[oc].customerflatno + ' - ' + rscol.cord[oc].customerbuilding;
                        }

                        co[oc][8] = rscol.cord[oc].itemname;

                        co[oc][9] = rscol.cord[oc].itemtype;

                        co[oc][10] = rscol.cord[oc].quantity;

                        co[oc][11] =  rscol.cord[oc].price;

                        co[oc][12] = rscol.cord[oc].total;

                        co[oc][13] = rscol.cord[oc].commission;

                        co[oc][14] = rscol.cord[oc].paymenttype; 
                        
                        co[oc][15] = rscol.cord[oc].pickupdate;

                        co[oc][16] = rscol.cord[oc].delivery; 

                        if(rscol.cord[oc].status == '')
                        {
                          co[oc][17] = "pending";
                        }
                        else
                        {
                           co[oc][17] = rscol.cord[oc].status;
                        }
                        co[oc][18] = rscol.cord[oc].paymentbreakdown;

                        co[oc][19] = rscol.cord[oc].aftertime;                                                                                                    
                    }

                    $('#tableVendorOrderList').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true
                    });   
                 }    
        });
    }

    </script>

    <script type="text/javascript">
        
        function exporttoexcel()
        {
           // alert("Hii");
        //     $('#tableChefOrderList').tableExport({type:'excel',escape:'false'});

        //     var dt = new Date();
        // var day = dt.getDate();
        // var month = dt.getMonth() + 1;
        // var year = dt.getFullYear();
        // var hour = dt.getHours();
        // var mins = dt.getMinutes();
        // var postfix = day + "." + month + "." + year + "_" + hour;

        //     var a = document.createElement('a');
        // //getting data from our div that contains the HTML table
        // var data_type = 'data:application/vnd.ms-excel';
        // var table_div = document.getElementById('tableChefOrderList');
        // var table_html = table_div.outerHTML.replace(/ /g, '%20');
        // a.href = data_type + ', ' + table_html;
        // //setting the file name
        // a.download = 'Mytree_' + postfix + '.xls';
        // //triggering the function
        // a.click();
        // //just in case, prevent default behaviour
        // e.preventDefault();
        }
    </script>

    <script type="text/javascript">
        
        $(document).ready(function() {
  // $("#exptexcel").click(function(e) {
  //   e.preventDefault();

  //   //getting data from our table
  //   var data_type = 'data:application/vnd.ms-excel';
  //   var table_div = document.getElementById('tableChefOrderList');
  //   var table_html = table_div.outerHTML.replace(/ /g, '%20');

  //   var a = document.createElement('a');
  //   a.href = data_type + ', ' + table_html;
  //        var dt = new Date();
  //       var day = dt.getDate();
  //       var month = dt.getMonth() + 1;
  //       var year = dt.getFullYear();
  //       var hour = dt.getHours();
  //       var mins = dt.getMinutes();
  //       var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
  //   a.download = 'MyTree_' + postfix + '.xls';
  //   a.click();
  // });

  function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;
    // CSV FILE
    csvFile = new Blob([csv], {type: "text/csv"});
    // Download link
    downloadLink = document.createElement("a");
    // File name
    downloadLink.download = filename;
    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);
    // Make sure that the link is not displayed
    downloadLink.style.display = "none";
    // Add the link to your DOM
    document.body.appendChild(downloadLink);
    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#tableChefOrderList tr");
    
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText.replace(/,/g,' '));        
        csv.push(row.join(","));       
    }
    // Download CSV
    download_csv(csv.join("\n"), filename);
}

document.querySelector("#exptexcel").addEventListener("click", function () {
    var html = document.querySelector("#tableChefOrderList").outerHTML;
    //var table_html = html.outerHTML.replace(/,/g, '%20');
    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    filename = 'MyTree_Orders' + postfix + '.csv';
    export_table_to_csv(html, filename);
});


});


    </script>
	
	<script type="text/javascript">
        
    function searchbypickupvendor()
    {
       // $("#loading").show();
        var frmdate = '';
        var todate = '';

        var totm = '';
        var frmtm = '';
        var ftm = '';
        var fttm = '';

        var invc = '';

        invc = $("#invitecode").val();

        frmdate = $("#frmdt").val();
        todate = $("#todt").val();

        // frmtm = $("#frmtm").val();

        // if(frmtm != '')
        // {
        //     frmtm = frmtm.split(":");
        //      ftm = frmtm[0]+':'+frmtm[1];  
        // }
        // else
        // {
        //    ftm = ''; 
        // }  

        // totm = $("#totm").val();

        // if(totm != '')
        // {
        //     totm = totm.split(":");
        //     fttm = totm[0]+':'+totm[1];  
        // }
        // else
        // {
        //   fttm = '';
        // }

         //console.log("From Date"+frmdate+"to date"+todate+"From Time"+ftm+"To Time"+fttm);

        var reqpdt = {"fromdate":frmdate,"todate":todate,"fromtime":ftm,"totime":fttm,"invitecode":invc};

        $.ajax({
                  url: 'service.php?servicename=SearchByPickupDateVendor',
                  type: 'POST',
                  datatype: 'JSON',
                  contentType: 'application/json',
                  data: JSON.stringify(reqpdt),
                  async: false,
                  success: function (rspd)
                 {
                     //console.log("Cheforders Response"+JSON.stringify(col));

                      console.log("search response"+JSON.stringify(rspd));
                    $('#tableChefOrderList').dataTable().fnClearTable();
                    $('#tableChefOrderList').dataTable().fnDraw();
                    $('#tableChefOrderList').dataTable().fnDestroy();

                     var rscol = JSON.parse(rspd);

                     var co = new Array();

                     for(var oc=0;oc<rscol.cord.length;oc++)
                     {
                        co[oc] = new Array();

                        co[oc][0] = oc+1;

                        co[oc][1] = rscol.cord[oc].orderid;

                        co[oc][2] = rscol.cord[oc].vendorname;

                        co[oc][3] = rscol.cord[oc].vendorphone;

                        if(rscol.cord[oc].vendoraddress != '')
                        {
                            co[oc][4] = rscol.cord[oc].vendoraddress;
                        }
                        else
                        {
                            co[oc][4] = rscol.cord[oc].vendorfaltno + ' - ' + rscol.cord[oc].vendorbuilding;
                        }

                        co[oc][5] = rscol.cord[oc].customer;

                        co[oc][6] = rscol.cord[oc].customerphone;

                        if(rscol.cord[oc].customeraddress != '')
                        {
                             co[oc][7] = rscol.cord[oc].customeraddress;
                        }
                        else
                        {
                             co[oc][7] = rscol.cord[oc].customerflatno + ' - ' + rscol.cord[oc].customerbuilding;
                        }

                        co[oc][8] = rscol.cord[oc].itemname;

                        co[oc][9] = rscol.cord[oc].itemtype;

                        co[oc][10] = rscol.cord[oc].quantity;

                        co[oc][11] =  rscol.cord[oc].price;

                        co[oc][12] = rscol.cord[oc].total;

                        co[oc][13] = rscol.cord[oc].commission;

                        co[oc][14] = rscol.cord[oc].paymenttype; 
                        
                        co[oc][15] = rscol.cord[oc].pickupdate;

                        co[oc][16] = rscol.cord[oc].delivery; 

                        if(rscol.cord[oc].status == '')
                        {
                          co[oc][17] = "pending";
                        }
                        else
                        {
                           co[oc][17] = rscol.cord[oc].status;
                        }
                        co[oc][18] = rscol.cord[oc].paymentbreakdown;

                        co[oc][19] = rscol.cord[oc].aftertime;                                                                                                    
                    }

                    $('#tableVendorOrderList').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true
                    });   
                 }       
        });
    }

    </script>

    <script type="text/javascript">
        
    function reset()
    {
        $("#frmdt").val('');

        $("#todt").val('');

      //  cheforderlist();

   }

    </script>