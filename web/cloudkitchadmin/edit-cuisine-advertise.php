<?php //include 'inc/config.php'; $template['header_link'] = 'FORMS'; 

session_start();
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: index.php");
}
 include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user']; 
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">

  <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Advertise</h1>
                </div>
            </div>
           
        </div>
    </div>

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <div class="block">
                <!-- Form Validation Title -->
                <div class="block-title">
                    <h2>Edit Advertise</h2>
                </div>
                <!-- END Form Validation Title -->

                <!-- Form Validation Form -->
                <form id="createadv" action="" class="form-horizontal form-bordered">
                    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">

                    <input type="hidden" name="adid" id="adid" value="<?php echo $_POST['adid'];?>">

                    <input type="hidden" name="mtype" id="mtype" value="<?php echo $_POST['mtype']; ?>" >

                    <input type="hidden" name="url" id="url" value="<?php echo $_POST['url'];?>">

                    <input type="hidden" name="desc" id="desc" value="<?php echo $_POST['desc'];?>">

                    <div class="form-group" id="selectInvite">
                        <label class="col-md-3 control-label" for="val-selmedt">Media Type<span class="text-danger">*</span></label>
                        <div class="col-md-6" >
                            <input type="text" id="val-selmedt" name="val-selmedt" class="form-control" placeholder="" disabled>
                            </select>
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-desc">Description<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-desc" name="val-desc" class="form-control" placeholder="Please Enter Description">
                        </div>
                    </div>

                    <!-- <div class="form-group" id="vid">
                        <label class="col-md-3 control-label" for="val-vid">Video<span class="text-danger">*</span></label>
                        <div id="vid0">
                        <div class="col-md-6" >
                            <input type="text" id="val-vid0" name="val-vid" class="form-control" placeholder="">
                        </div>
                        </div>
                    </div> -->

                                
                    <div class="form-group" id="img">
                        <label class="col-md-3 control-label" for="val-images">Image</label>
                        <div class="col-md-6" id="filediv" > 
                            <div id="filediv0">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div id="abcd0" class="abcd" style="width:300px;height:300px;">
                                            <div>
                                            <br>
                                            <img style="width:300px;height:300px;" class="img-rounded" id="previewimg0" src="">
                                            </div>
                                        </div>
                                        <input name="file[]" type="file" id="file" onclick="remove()" style="display: none;">
                                    </div>
                                   <div class="col-md-3">
                                        <button type="button" id="rm" class="btn btn-primary" style="color: rgb(255, 255, 255); border-radius: 26px; display: block;" onclick="btnimgaeremovefirst(0)">Remove Image</button>
                                   </div>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <!-- Cuisine List -->

                    <div class="row">

                        <div class="col-sm-10 col-md-12 col-lg-12">
            
                            <div class="block">
    
                                <div class="block-title">
                                    <h2>Cuisines</h2>
                                </div>
                 
                                <table id ="cuisinelist" class="table table-vcenter table-striped table-hover table-borderless">

                                                                <thead>
                                                                 <tr>
                                                                 <th>Sr</th>
                                                                 <th>Choose</th>
                                                                 <th>Chef</th>
                                                                 <th>Cuisine Name</th>
                                                                 <th>Expire Date</th>
                                                                 </tr> 
                                                                </thead>  
                                </table>
                            </div>
                        </div>
                    </div>

                    
                    
                    
                    <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-3">
                            <button type="button" onclick="editadvertisecu();" class="btn btn-effect-ripple btn-primary">Submit</button>
                            <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Form Validation Form -->

               
            </div>
            <!-- END Form Validation Block -->
        </div>
    </div>

    

    <!-- Cuisine Details -->

    


    <!-- END Form Validation Content -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script type="text/javascript" src="js/CasseroleService.js"></script>

<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 
//tableadList
        $('#cuisinelist').dataTable().fnClearTable();
        $('#cuisinelist').dataTable().fnDraw();
        $('#cuisinelist').dataTable().fnDestroy();

        // $('#tableadList').dataTable().fnClearTable();
        // $('#tableadList').dataTable().fnDraw();
        // $('#tableadList').dataTable().fnDestroy();

       });</script>

<script type="text/javascript">

var imgc = 0; 

 var table = '';

  var cid = [];

$(document).ready(function() {

    

    $("#loading").hide();
    //$("#img").hide();
   // $("#vid").hide();
    $("#col").hide();

    var mtype = '';

   

    var caption = '';

    var url = '';

    var ad_id = '';

    var ic = '';

   

    ad_id = $("#adid").val();

    mtype = $("#mtype").val();

    caption = $("#desc").val();

    url = $("#url").val();

    $("#val-selmedt").val(mtype);

    $("#val-desc").val(caption);

    $("#previewimg0").attr('src',url);

    ic =  $("#invitecode").val();

    console.log("Adid"+ad_id);

    $.ajax({
              url:'../Casserole/Services/service_1.6.php?service_name=ADVERTISECUISINE',
              type:'POST',
              datatype: 'JSON',
              data: JSON.stringify({"advid":ad_id}),
              async:false,
              success: function(acu)
              {
                 console.log("ADCU"+JSON.stringify(acu));

                 var fcu = JSON.parse(acu);

                 var c = new Array();

                 

                 if(fcu.status == 'success')
                 {
                    for(var cu=0;cu<fcu.Cuisines.length;cu++)
                    {
                        c[cu] = new Array();

                        c[cu][0] = cu+1;

                        c[cu][1] = '<input type="checkbox" id="example-checkbox1" name="cus" value="'+fcu.Cuisines[cu].cuid+'">';

                        c[cu][2] = fcu.Cuisines[cu].chefname;

                        c[cu][3] = fcu.Cuisines[cu].cuiname;

                        c[cu][4] = fcu.Cuisines[cu].closedate;

                        cid.push(fcu.Cuisines[cu].cuid);

                       // $("input[name=cus][value=" + fcu.Cuisines[cu].cuid + "]").prop('checked', true);
                    }

                    table = $('#cuisinelist').dataTable({
                        "aaData": c,
                        "bDestroy": true,
                        "autoWidth": true,
                        "aoColumns": [
                            null,
                            null,
                            { "orderSequence": [ "desc", "asc", "asc" ] },
                            { "orderSequence": [ "desc", "asc", "asc" ] },
                            { "orderSequence": [ "desc", "asc", "asc" ] }
                        ],

                       });  

                    //  var rows = table.rows({ 'search': 'applied' }).nodes();

                    // $('input[type="checkbox"]', rows).prop('checked', this.checked);

                       check();  
                 }
                 else
                 {

                 }   

                 

                



              }   

    });

     
    
    // var url = $("#url").val();

    // $("#val-selmedt").val(mt);

    // $("#val-desc").val(caption);

    // if(mt == 'Video')
    // {
    //     $("#vid").show();

    //     $("#val-vid0").val(url);
    // }

    // if(mt == 'Image')
    // {   
    //     $("#img").show(); 

    //     $("#previewimg0").attr('src',url);
    // }    






});
</script>

<script type="text/javascript">
    
function check()
{
    for( var u=0;u<cid.length;u++)
    {
         $("input[name=cus][value=" + cid[u] + "]").prop('checked', true); 
    }    

    
}

</script>

<script type="text/javascript">
    
    function showchefs(aid)
    {
        $.ajax({
              url:'../Casserole/Services/service_1.5.php?service_name=ADVERTISECHEF',
              type:'POST',
              datatype: 'JSON',
              data:JSON.stringify({"advid":aid}),
              async:false,
              success: function(cfs)
              {
                 console.log("Cf"+JSON.stringify(cfs));

                 var cfl = JSON.parse(cfs);

                 for(var cf=0;cf<cfl.cheflist.length;cf++)
                 {
                     $("input[name=chef][value=" + cfl.cheflist[cf] + "]").prop('checked', true); 
                 }   
                      

              }    
    });  
    }

</script>



<script type="text/javascript">
    
    function remove()
    {
        $("#rm").css("display","block");

        imgc = 0;
    }

</script>

<script type="text/javascript">
    
    function btnimgaeremovefirst(p1)
    {
        console.log("Par"+p1);

        imgc ++;
        
        var divd = '';

        divd += '<div id="filediv'+p1+'">'+
                               
                               '<div class="row">'+
                                 
                                   '<div class="col-md-9">'+
                                   '<input name="file[]" type="file" id="file" onclick="remove()" />'+
                                   '</div>'+
                                   '<div class="col-md-3">'+
                                   '<button type="button" id="rm" class="btn btn-primary" style="color: #ffffff;border-radius: 26px;display:none" onclick="btnimgaeremovefirst(0)">Remove Image</button>'+ 
                                   '</div>'+
                                   
                               '</div>'+
                               
                                
                          
                '</div>';

        $("#filediv").html(divd);        


    }

</script>

<script type="text/javascript">
    
    function editadvertisecu()
    {
        console.log("Edit Advertise");

        var vl = 0;

        var adid = '';

        var mtype = '';

        var caption = '';

        var url = '';

        var vld = 0;

        adid = $("#adid").val();

        mtype = $("#mtype").val();

        caption = $("#val-desc").val();

        if(caption == '')
        {
            alert('Please provide description');

            vl++;

            return false;
        }

        if(imgc > 0)
        {
            alert('Please provide image');

            vl++;

            return false;
        }

        if(!$("input[name='cus']:checked").val())
        {
            alert('Please select cuisine');

            vl++;

            return false;
        }



        

        if(vl == 0)
        {
            //alert('Everything is fine..!!');

            var cuary = [];

          $.each($("input[name='cus']:checked"),function(){

                cuary.push($(this).val());

          });

          var u = '';

          u = document.getElementById("previewimg0").src;

          console.log("Data"+JSON.stringify({"adid":adid,"mtype":mtype,"caption":caption,"cuisines":cuary,"url":u}));

          //return false;

          $.ajax({
                    url:'advertise_service.php?servicename=edit-cuisine-advertise',
                    type: 'POST',
                    datatype: 'JSON',
                    data: JSON.stringify({"adid":adid,"mtype":mtype,"caption":caption,"cuisines":cuary,"url":u}),
                    async:false,
                    success: function(rscuf)
                    {
                        console.log("Edit cuisine"+JSON.stringify(rscuf));

                        var rscu = JSON.parse(rscuf);

                        if(rscu.status == 'success')
                        {
                            var n = noty({
                           layout: 'bottomRight',
                           text: 'Cuisine Details Updated Successfully',
                           theme:'relax',type: 'success',
                           timeout : '3000',
                           animation: {
                                  open: 'animated bounceInLeft', // Animate.css class names
                                  close: 'animated bounceOutLeft', // Animate.css class names
                                  easing: 'swing', // unavailable - no need
                                  speed: 500 // unavailable - no need
                              },
                              callback: { 
                                  onClose: function() {
                                                           
                                  } 
                              },
                              });

                            setTimeout(redirect,1000);
                        }
                        else
                        {
                            var n = noty({
                             layout: 'bottomRight',
                             text: 'Not able to update cuisine details',
                             theme:'relax',type: 'error',
                             timeout : '3000',
                            animation: {
                                open: 'animated bounceInLeft', // Animate.css class names
                                close: 'animated bounceOutLeft', // Animate.css class names
                                easing: 'swing', // unavailable - no need
                                speed: 500 // unavailable - no need
                            },
                            callback: { 
                                onClose: function() {
                                    
                                   // wizardshowhide("clickable-second");
                                } 
                            },
                            });
                        } 
                    } 


          });


        }
        else
        {
            alert('Error');

        //     var reqea = {"adid":adid,"mtype":mtype,"caption":caption,"url":url};

        //     $.ajax({
        //              url:'advertise_service.php?servicename=Edit-Advertise',
        //              type: 'POST',
        //              datatype: 'JSON',
        //              data: JSON.stringify(reqea),
        //              success: function(rsea)
        //              {
        //                 console.log("EDit"+JSON.stringify(rsea));

        //                 var ed = JSON.parse(rsea);

        //                 if(ed.status == 'success')
        //                 {
        //                     window.location.href = 'advertise.php';
        //                 }    
        //              }   

        //     });


        }    

    }

    function redirect()
        {
           window.location.href = "advertise.php";
        }

</script>

<?php include 'inc/template_end.php'; ?>