<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
   // header("Location: http://localhost:7755/Casseroleadmin/index.php");
   header("Location: https://coludkitch.co.in/cloudkitchadmin/index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <!-- <a href="#myModal" class="btn btn-lg btn-primary" data-toggle="modal">Launch
      Demo Modal</a> -->
    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Cravers</h4>
          </div>
          <div class="modal-body">

          <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tableCraverUsers" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>User</th>
                                                 <th>Profile</th>
                                                 <!-- <th>Price</th>
                                                 <th>Quantity</th>
                                                 <th>Type</th>
                                                 <th>Status</th>
                                                 <th>Code</th>
                                                 <th>Closed Date</th> -->
                                               
                                                                                              
                                                </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>

           

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>
        </div>
      </div>
    </div>

    <!-- Modal For Craving Images -->

    <div id="myModalcraveimg" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Craving Images</h4>
          </div>

          <div class="modal-body">

          <div class="row">
                  <div class="col-sm-10 col-md-12 col-lg-12">
           
                            <table id ="tableCraveimg" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Craving Image</th>
                                                 </tr> 
                                                </thead>
                                              
                            </table>
            
                  </div>
          </div>

           

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
      
    </div>

    
  </div>

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Craver Details</h1>
                    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tableCraverDetail" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Dish</th>
                                                 <th>Type</th>
                                                 <th>Image</th>
                                                 <th>Cuisine Type</th>
                                                 <th>Likes</th>
                                                 <th>Invite Code</th>
                                                 <!-- <th>Transaction Id</th>
                                                 <th>Actual Amount</th>
                                                 <th>Discount Amount</th>
                                                 <th>Coupon Applied</th>
                                                 <th>Coupon</th> -->
                                                 <!-- <th></th> -->
                                                
                                                </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->



<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script src="js/CasseroleService.js"></script>

<!-- <script src="js/order.js"></script> -->


<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    $('#tableOrderList').dataTable().fnClearTable();
        $('#tableOrderList').dataTable().fnDraw();
        $('#tableOrderList').dataTable().fnDestroy();

        $('#tableOrderItemList').dataTable().fnClearTable();
        $('#tableOrderItemList').dataTable().fnDraw();
        $('#tableOrderItemList').dataTable().fnDestroy();

        
    // var type = '<?php 
    //echo $_SESSION['info']['type']; ?>';
    // var wedcode = '';
    // wedcode = '2616';
    // console.log("My Type Is :"+type);
    // if(type == "admin")
    // {
    //     getWeddingList();
    // }

    var invitecode = '';

    invitecode = $("#invitecode").val();

    //invitationlist();
    //customerlist();
    //orderlist();
    //craverdetail();
    craverslist(invitecode);
    // if(type == "subadmin") 
    // {
    //    getWeddingListBySubadmin(wedcode); 
    // }   
    });</script>