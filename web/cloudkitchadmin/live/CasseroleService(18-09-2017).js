//24 june


//Login function
function login(){
    var email = $("#login-email").val();
    var password = $("#login-password").val();
    var logindetail = {"email":email,"pwd":password};

    $.ajax({
            type: 'POST',
            url: 'service.php?servicename=login',
             datatype: 'JSON',
             contentType: 'application/json',
             data: JSON.stringify(logindetail),
             success : function(respo)
             {
                console.log("Login Response Is :"+JSON.stringify(respo));
                var reslg = JSON.parse(respo);
                var status = '';
                status = reslg.status;
                if(status == 'true')
                {
                     window.location="Deshboard.php";
                }
                else
                {
                    alert("Login Failed..!!");
                    return false;
                }    
             }  

    });
    // console.log("Email :"+email+"Password :"+password);
    // if(email == 'admin@gmail.com' && password == '123456'){
    //     window.location="Deshboard.php";
    // }

}
//view the image fro create wedding

function customerlist()
{
  $('#loading').show();
    $.ajax({

            type: "GET",
            url : "service.php?servicename=getcustomers",
            datatype : "JSON",
            contentType: "application/json",
            success: function(data)
            {
              $('#loading').hide();
                    console.log("Customer Data :"+JSON.stringify(data));

                    var cusl = JSON.parse(data);

                   var customer = new Array();

                   for(var c=0;c<cusl.Customers.length;c++)
                   {
                      customer[c] = new Array();

                      customer[c][0] = c+1;
                      customer[c][1] = cusl.Customers[c].name;
                      customer[c][2] = cusl.Customers[c].email;
                      customer[c][3] = cusl.Customers[c].phone;
                      customer[c][4] = cusl.Customers[c].accounttype;
                      customer[c][5] = cusl.Customers[c].address;
                      customer[c][6] = cusl.Customers[c].ivdts;

                   }  

                    $('#tableCustomerList').dataTable({
                        "aaData": customer,
                        "bDestroy": true
                    });
            }
    })
}

function cuisinecatlist()
{
   $('#loading').show();
    $.ajax({
              type: "GET",
              url: "service.php?servicename=getcuisinecat",
              datatype: "JSON",
              contentType: "application/json",
              success: function (cuic)
              {
                  $('#loading').hide();
                  console.log("Cuisine Category List"+JSON.stringify(cuic));

                  var cucl = JSON.parse(cuic);

                  var cul = new Array();

                  for(var cl=0;cl<cucl.ccat.length;cl++)
                  {
                     cul[cl] = new Array();

                     cul[cl][0] = cl+1;

                     cul[cl][1] = cucl.ccat[cl].cuisinecatname;
                  }

                  $('#tablecuicatList').dataTable({
                        "aaData":  cul,
                        "bDestroy": true
                    });


              }

    });
}

function dishlist()
{
   $('#loading').show();

    $.ajax({
              type: "GET",
              url: "service.php?servicename=dishlist",
              datatype: "JSON",
              contentType: "application/json",
              success: function(dls)
              {
                 $('#loading').hide();

                  console.log("Dishlist "+JSON.stringify(dl));

                  var dishl = JSON.parse(dls);

                  var dl = new Array();

                  for(var d=0;d<dishl.dishlist.length;d++)
                  {
                     dl[d] = new Array();

                     dl[d][0] = d+1;

                     dl[d][1] = dishl.dishlist[d].dishname;

                     dl[d][2] = dishl.dishlist[d].type;

                     dl[d][3] = "<img class='img-rounded' src='"+dishl.dishlist[d].ctimage+"' style='width:200px;height:200px;'/>";

                     dl[d][4] = dishl.dishlist[d].cuisinecatname;

                     dl[d][5] = dishl.dishlist[d].createdate;
                  } 

                  $('#tabledishList').dataTable({
                        "aaData":  dl,
                        "bDestroy": true
                    });


              }
    });
}


  function createcuisinecati()
  {
      $("#loading").show();

        var cuicat = $("#val-cuisinecat").val();

        console.log("Cuisine Category Name:"+cuicat);

        var cuiscat = {"cuicat":cuicat};

        $.ajax({
                    type: "POST",
                    url: "service.php?servicename=createcuicat",
                    datatype: "JSON",
                    contentType : "application/json",
                    data: JSON.stringify(cuiscat),
                    success: function(datac)
                    {
                        $("#loading").hide();
                        console.log("My Response Is:"+datac);
                    }

        });
  }

  function createcuisinetype()
  {
      $("#loading").show();

      var ctyp = $("#val-cuitype").val();

      console.log("Cuisine Type:"+ctyp);

      var cuityp = {"cuisinetype":ctyp};

      $.ajax({
                type: "POST",
                url: "service.php?servicename=addcuisinetype",
                datatype: "JSON",
                contentType: "application/json",
                data: JSON.stringify(cuityp),
                success: function(cutype)
                {
                    $("#loading").hide();
                    console.log("AddED Cuisine type:"+JSON.stringify(cutype));

                    var rct = JSON.parse(cutype);

                    if(rct.status == 'success')
                    {
                        var n = noty({
                       layout: 'bottomRight',
                       text: 'Cuisine Type Added Successfully',
                       theme:'relax',type: 'success',
                       timeout : '3000',
                       animation: {
                              open: 'animated bounceInLeft', // Animate.css class names
                              close: 'animated bounceOutLeft', // Animate.css class names
                              easing: 'swing', // unavailable - no need
                              speed: 500 // unavailable - no need
                          },
                          callback: { 
                              onClose: function() {
                                  
                                 // document.getElementById("divcreateexp").style.display="none" ;
                                  //document.getElementById("confirmView").style.display="block" ;
                                 // wizardshowhide("clickable-second");
                              } 
                          },
                          });
                    }
                    else
                    {
                        var n = noty({
                         layout: 'bottomRight',
                         text: 'Not able to add Cuisine Type',
                         theme:'relax',type: 'error',
                         timeout : '3000',
                        animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });
                    }  
                }
      });
  }

  var loadDishImage = function(event)
{
    var readevi = new FileReader();
    readevi.onload = function(){
        $("#dishImageView").css("height","200px");
        $("#dishImage").css("margin-bottom","10px");
        var op = document.getElementById('dishImageView');
        op.src = readevi.result;
    };
    readevi.readAsDataURL(event.target.files[0]); 
};

  function cuisinecategory(){
 $.ajax({ url: 'service.php?servicename=getcuicat', 
            type: 'POST',
            datatype:'JSON',
            contentType: 'application/json',
            success: function(data){
                
                var mydata = JSON.parse(data);
                
                console.log("My Response :"+JSON.stringify(mydata));
                var weddingdata = "";
                //pending exp data
                // weddingdata = " <option value='0'>select wedding</option>";
                for(var i = 0 ; i < mydata.length ; i++)
                {
                   // weddingdata += "<option value='"+mydata[i].id+"'>"+mydata[i].hashtag+"</option>"
                   //weddingCode
                   weddingdata += "<option value='"+mydata[i].id+"'>"+mydata[i].category+"</option>";
                    
                }
                
                $("#val-selectcuicat").append(weddingdata);
                
            }
         
        });
}

function invitecodedetail()
{
    $.ajax({
              type: 'POST',
              url: 'service.php?servicename=invitecode',
              datatype: 'JSON',
              contentType: 'application/json',
              success: function(invd)
              {
                  console.log("Invitation Details"+JSON.stringify(invd));

                  var inv = JSON.parse(invd);

                  var invd = '';

                  for(var iv=0;iv<inv.invitedetail.length;iv++)
                  {
                     invd += "<option value='"+inv.invitedetail[iv].invid+"'>"+inv.invitedetail[iv].invitationcode+"</option>";
                  } 

                  $("#val-selectInvitecode").append(invd); 
              }
    });
}

function invitecodes()
{
    $.ajax({
              type: 'POST',
              url: 'service.php?servicename=invitecode',
              datatype: 'JSON',
              contentType: 'application/json',
              success: function(invd)
              {
                  console.log("Invitation Details"+JSON.stringify(invd));

                  var inv = JSON.parse(invd);

                  var invd = '';

                  for(var iv=0;iv<inv.invitedetail.length;iv++)
                  {
                     invd += "<option value='"+inv.invitedetail[iv].invitationcode+"'>"+inv.invitedetail[iv].invitationcode+"</option>";
                  } 

                  $("#val-invc").append(invd); 
              }
    });
}

function adddish()
{
    $("#loading").show();
    var dishname = $("#val-dishName").val();
    var cuicatid = $("#val-selectcuicat").val();
    var dishImage = $("#dishImageView").attr('src');
    var type = $("#val-type").val();

    console.log("DIshname"+dishname+"Cat"+cuicatid+"img"+dishImage+"type"+type);

    var dish = {"dishname":dishname,"cuisinecat":cuicatid,"dishimg":dishImage,"type":type};

    $.ajax({
                type: "POST",
                url: "service.php?servicename=adddish",
                datatype: "JSON",
                contentType: "application/json",
                 data: JSON.stringify(dish),
                 success : function(data)
                 {
                        $("#loading").hide();
                        console.log("Response:"+JSON.stringify(data));
                 }


    });








}

//By Dhruvpalsinh On 6-7-2017

function createCouponCode()
{
   $("#loading").show();

   var cpc = '';

   var disc = '';

   cpc = $("#val-cpcode").val();

   disc = $("#val-disc").val();

   console.log("Coupon Code"+cpc+"Discount"+disc);

   var reqAddcoupon =  {"couponcode":cpc,"discount":disc};

   $.ajax({
            url: "service.php?servicename=addCouponCode",
            type: "POST",
            datatype: "JSON",
            contentType: "application/json",
            async: false,
            data: JSON.stringify(reqAddcoupon),
            success: function(rescc)
            {
               console.log("CpCode Response"+JSON.stringify(rescc));

               $("#loading").hide();

               var rscp = JSON.parse(rescc);

               if(rscp.status == 'success')
               {
                    var n = noty({
                     layout: 'bottomRight',
                     text: 'Coupon Code Created Successfully',
                     theme:'relax',type: 'success',
                     timeout : '3000',
                     animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // document.getElementById("divcreateexp").style.display="none" ;
                                //document.getElementById("confirmView").style.display="block" ;
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });
               }
               else
               {
                    var n = noty({
                         layout: 'bottomRight',
                         text: 'Failed to create coupon code',
                         theme:'relax',type: 'error',
                         timeout : '3000',
                        animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });
               } 
            }

   });


}

function createInviteCode(){

    $("#loading").show();
   // alert("hello");
   // return false;
    var grpnm = $("#val-grpname").val();
    var invCode = $("#val-inviteCode").val();

    var email = $("#email").val();

    //alert("EMail"+email);
    
    console.log("groupname : "+grpnm+ "InviteCode : "+invCode);
    

    var  createinvitation = {
		"groupname":grpnm,
		"inviteCode":invCode
		};
    
//console.log(JSON.stringify(createWedding));
    $.ajax({
				 type: "POST",
				 url: "service.php?servicename=create_invitecode",
				 datatype: "JSON",
                 contentType: "application/json",
        	     data: JSON.stringify(createinvitation), // make JSON string
				 success: function(da)
				 {
                    console.log("Responese is:"+JSON.stringify(da));
                    //return false;

                    $("#loading").hide();

                    var dt = JSON.parse(da);

                    

                    if(dt.status == 'success')
                    {
                        var group = '';
                        var invitecode = '';
                        
                        group = dt.groupname;
                        invitecode = dt.invitationcode;
                        

                        sendmail(group,invitecode,email);


                        var n = noty({
                     layout: 'bottomRight',
                     text: 'Invitation Created',
                     theme:'relax',type: 'success',
                     timeout : '3000',
                     animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // document.getElementById("divcreateexp").style.display="none" ;
                                //document.getElementById("confirmView").style.display="block" ;
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });
                    }
                    else
                    {
                            var n = noty({
                         layout: 'bottomRight',
                         text: 'Invitation Not Created',
                         theme:'relax',type: 'error',
                         timeout : '3000',
                        animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });

                    }    

                     console.log("Invitation created");
                 }
        
    });



}

//By Dhruvpalsinh on 19-07-2016

function sendmail(grp,invitecode,mail)
{
   var body = {"groupname":grp,"invitationcode":invitecode,"mail":mail}
    $.ajax({
             type: "POST",
             url: "service.php?servicename=SendMail",
             datatype: "JSON",
             contentType : "application/json",
             data: JSON.stringify(body),
             success : function(data)
             {
                var rs = JSON.parse(data);

                if(rs.status == 'true')
                {
                    console.log('Mail Sent');
                }
                else
                {
                    console.log('Mail Sending Failed');
                }    


             } 


    })

}

//Uadate wedding



// check wedding code



function check_inviteCode(ivcode)
{
    // alert(ivcode);
    // return false;
    $("#user-result").html('<img src="image/ajax-loader.gif">');
    
    $.post('service.php?servicename=inviteCodeCheck',{'invitecode':ivcode}, function(rs){

        if(rs == '<img src="image/not-available.png" /> <span style="color:  #8B0000"> choice another invitation code</span>')
        {
            $("#val-inviteCode").val("");
            $("#user-result").html(rs);
        }
        else
        {
            $("#user-result").html(rs);
        }    



    });

}

function check_cuisinetype(ctype)
{
    $("#user-result-cuitype").html('<img src="image/ajax-loader.gif">');
    
    $.post('service.php?servicename=cuisinetypeCheck',{'cuitype':ctype}, function(rs){

        if(rs == '<img src="image/not-available.png" /> <span style="color:  #8B0000">Choice Another Cuisine Type</span>')
        {
            $("#val-cuitype").val("");
            $("#user-result-cuitype").html(rs);
        }
        else
        {
            $("#user-result-cuitype").html(rs);
        }    



    });
}

function check_cuisinetag(ctag)
{
   $("#user-result-cuitag").html('<img src="image/ajax-loader.gif">');

   $.post('service.php?servicename=cuisinetagCheck',{'cuitag':ctag},function(ctrs){

    if(ctrs == '<img src="image/not-available.png" /> <span style="color:  #8B0000">Choice Another Cuisine Tag</span>')
    {
       $("#val-tagname").val("");
       $("#user-result-cuitag").html(ctrs);
    }
    else
    {
       $("#user-result-cuitag").html(ctrs);
    }  


   });
}


function check_couponCode(cpcode)
{
   $("#user-result-coupon").html('<img src="image/ajax-loader.gif">');

   $.post('service.php?servicename=couponcodeCheck',{'cp_code':cpcode},function(cprs){

    if(cprs == '<img src="image/not-available.png" /> <span style="color:  #8B0000">Choice Another Coupon Code</span>')
    {
      $("#val-cpcode").val("");
      $("#user-result-coupon").html(cprs);
    }
    else
    {
       $("#user-result-coupon").html(cprs);
    }

   });
}

//By Dhruvpalsinh On 10 July 2017

function couponcodelist()
{
    $("#loading").show();

    $.ajax({
              type: 'GET',
              url: 'service.php?servicename=couponcodeList',
              success: function(rcl)
              {
                 $("#loading").hide();
                 console.log("CC Response"+JSON.stringify(rcl));

                 var rccl = JSON.parse(rcl);

                 var cpl = new Array();

                 for(var lc=0;lc<rccl.coupons.length;lc++)
                 {
                    cpl[lc] = new Array();

                    cpl[lc][0] = lc+1;

                    cpl[lc][1] = rccl.coupons[lc].couponcode;

                    cpl[lc][2] = rccl.coupons[lc].discount; 
                 } 

                 $('#tablecouponcode').dataTable({
                        "aaData": cpl,
                        "bDestroy": true
                    });


              }
    })
}

//By Dhruvpalsinh On 11 August 2016

function invitationlist()
{
    $("#loading").show();

    $.ajax({
             type: 'POST',
             url: 'service.php?servicename=invitationlist',
             datatype: 'JSON',
             contentType: 'application/json',
             success: function(res)
             {
                $("#loading").hide();
                var ilist = JSON.parse(res);

                var ildt = new Array();

                for(var l = 0; l < ilist.length ;l++)
                {
                    ildt[l] = new Array();

                    ildt[l][0] = l+1;
                    ildt[l][1] = ilist[l].groupname;
                    ildt[l][2] = ilist[l].invitationcode;
                }

                $('#tableInviteList').dataTable({
                        "aaData": ildt,
                        "bDestroy": true
                    });    

             }

    });
}

//Get the wedding List 

function getWeddingList(){

    $('#loading').show();

    $.ajax({ url: 'service.php?servicename=WeddingList', 
			type: 'POST',
			datatype:'JSON',
			contentType: 'application/json',
            success: function(data){
                
                $('#loading').hide();
                var mydata = JSON.parse(data);
                
                //pending exp data
			    var weddingdata = new Array();
                for(var i = 0 ; i < mydata.length ; i++)
                {
                       weddingdata[i] = new Array();
                       weddingdata[i][0] = i+1;
                       weddingdata[i][1] = mydata[i].hashtag;
                       weddingdata[i][2] = mydata[i].weddingCode;
                       weddingdata[i][3] = "<a href = \"EditWedding.php?weddingCode=" + mydata[i].id + "\" target = \"blank\" >Edit</a>"; 
                        
                }
                
               
				
                $('#tableWeddingList').dataTable({
                        "aaData": weddingdata,
                        "bDestroy": true
                    });
                
                
              
			}
			
		
		});

}


//end

//Wedding detail for edit page
function weddingDetail(weddingCode){
    
      var  weddingCode = {
		"weddingCode":weddingCode
		};   
    
 $.ajax({ url: 'service.php?servicename=weddingDetail', 
			type: 'POST',
			datatype:'JSON',
			contentType: 'application/json',
            data: JSON.stringify(weddingCode), // make JSON string
            success: function(datas){
               var data =JSON.parse(datas); 
                
                $("#val-hashtag").val(data.wedding_name);
                $("#val-weddingCode").val(data.wedding_code);
                $("#val-groomName").val(data.groomName);
                $("#val-bridalName").val(data.bridalName);
                  $('#weddingImageView').css("height","200px");
       $('#weddingImage').css("margin-bottom","10px");
                $("#weddingImageView").attr('src',data.weddingImage);
                
               // $('#val-message').value(data.weddingMessage);
                document.getElementById("val-message").value = data.weddingMessage;
                
            }
         
        });
}

//Edit Event Detail Page By Dhruvpalsinh D on 9-07-2016

function eventDetail(eventId)
{
    var event = {"eventId":eventId};

    $.ajax({
                url: 'service.php?servicename=eventDetail',
                type: 'POST',
                datatype: 'JSON',
                contentType : 'application/json',
                data: JSON.stringify(event),
                success : function(resp)
                {
                    //weddingListForEventPage()
                    console.log("My Event Response Is :"+JSON.stringify(resp));
                    //$("div.id_100 select").val("val2");

                    var edata = JSON.parse(resp);

                    console.log("id is : "+edata.id);

                    //$("#val-selectWedding select").val(edata.id);
                    //$('#val-selectWedding option[value="'+edata.id+'"]');
                    $('#val-selectWedding').val(edata.wedding_code);
                    $('#val-eventName').val(edata.event_tittle);
                    $('#example-datepickerForEvent').val(edata.event_date);
                    $('#example-timepicker-start').val(edata.event_start_time);
                    $('#example-timepicker-end').val(edata.event_end_time);
                    $('#val-address').val(edata.address);
                    $('#val-info').val(edata.info);
                    $('#eventImageView').css("height","200px");
                    $('#eventImage').css("margin-bottom","10px");
                    $('#eventImageView').attr('src',edata.eventimage);


                    console.log("Assigned Vale Successfully");
                }

    });
} 

//Get the Event List By Dhruvpalsinh D on 9-07-2016
function getEventList(wedid)
{
    $("#loading").show();
    console.log("Hii I'm Enter In EventList And Weeding Id Is :"+wedid);
    //return false;

    var wid = {"wid":wedid};

    $.ajax({
             url: 'service.php?servicename=EventList',
             type: 'POST',
             datatype: 'JSON',
             contentType: 'application/json',
             data: JSON.stringify(wid),
             success : function(res)
             {
                $("#loading").hide();
                // var evls = '';
                // evls = "<h1>Event List</h1>";
                // console.log("Head :"+evls);
                $("#eventheader").css("display","block");

                //var dte = '';

                $("#cntev").css("display","block");

                // var hdr = '';
                // hdr = '<thead><tr><th>Sr</th><th>Event Name</th><th>Event Date</th><th>Operation</th></tr></thead>';

                // console.log("Table Headre :"+hdr);
                // $('#tableEventList').append(hdr);

                //evls = '<div class="content-header"><div class="row"><div class="col-sm-6"><div class="header-section"><h1>Event List</h1></div></div></div></div>';
                // evls += '<div class="row"><div class="col-sm-10 col-md-12 col-lg-12"><table id ="tableEventList" class="table table-vcenter table-striped table-hover table-borderless"><thead><tr><th>Sr</th><th>Event Name</th><th>Event Date</th><th></th></tr> </thead></table> </div></div>';
                // console.log("Event"+evls);
                // $('#eventlist').append(evls);
                //$(evls).appendTo('#eventlist');

                //console.log("Res Is:"+res);
               //console.log("My Response Is :"+JSON.stringify(res));

                var eventdata = JSON.parse(res);

                var data = new Array();

                var table = '';
                 var textOfEventList = "";
                for(var d=0;d<eventdata.length;d++)
                {
                    textOfEventList += "<option value="+eventdata[d].id+" selected>"+eventdata[d].event+"</option>";
                    data[d] = new Array();
                    data[d][0] = d+1;
                    data[d][1] = eventdata[d].event;
                    data[d][2] = eventdata[d].date;
                    data[d][3] = "<a href = \"EditEvent.php?event=" + eventdata[d].id + "\" target = \"blank\" >Edit</a>";

                }

               console.log($textOfEventList);           
                 $('#list').append(textOfEventList);
                $('#tableEventList').dataTable({
                        "aaData": data,
                        "bDestroy": true
                    });

             } 

    });
}

//6-jul

//Create Event By Dhruvpalsinh on 8-7-2016
// function createevent()
// {
//     $("#loading").show();
  
//    // console.log("Hii I'm In");
//     //return false;
//     var wedding = $("#val-selectWedding").val();
//     var eventname = $("#val-eventName").val();
//     var eventImage = $("#eventImageView").attr('src');
//     //var wedcode = $("#val-weddingCode").val();
//     var eventdate = $("#example-datepickerForEvent").val();
//     var starttime = $("#example-timepicker-start").val();
//     //var starttime = formatDate() ;
//     var endtime = $("#example-timepicker-end").val();
//     var address = $("#val-address").val();
//     var info = $("#val-info").val();

//     console.log("Wedding Name :"+wedding+"Event :"+eventname+"Event Date :"+eventdate+"Start Time :"+starttime+"End Time :"+endtime+"Address :"+address+"Info :"+info);
//     //var bridalnm = $("#val-bridalName").val();
//    // return false;
    
//    var createevnt = {
//        "wedding":wedding,
//        "event":eventname,
//        "eventImage":eventImage,
//        "eventdate":eventdate,
//        "starttime":starttime,
//        "endtime":endtime,
//        "address":address,
//        "info":info
//    };

//   // console.log("My JSon Is :"+JSON.stringify(createevnt));
    
//    $.ajax({
//             type: "POST",
//             url: "service.php?servicename=create_event",
//             datatype: "JSON",
//             contentType: "application/json",
//             data: JSON.stringify(createevnt),
//             success: function(data)
//             {
//                 $("#loading").hide();
                
//                console.log("Event Created"+JSON.parse(data));

//                var respe = JSON.parse(data);

//                if(respe.status == 'true')
//                {
//                     var n = noty({
//                      layout: 'bottomRight',
//                      text: 'Event Created',
//                      theme:'relax',type: 'success',
//                      timeout : '3000',
//                      animation: {
//                             open: 'animated bounceInLeft', // Animate.css class names
//                             close: 'animated bounceOutLeft', // Animate.css class names
//                             easing: 'swing', // unavailable - no need
//                             speed: 500 // unavailable - no need
//                         },
//                     callback: { 
//                         onClose: function() {
                            
//                            // document.getElementById("divcreateexp").style.display="none" ;
//                             //document.getElementById("confirmView").style.display="block" ;
//                            // wizardshowhide("clickable-second");
//                         } 
//                     },
//                 });

//                }
//                else
//                {
//                     var n = noty({
//                      layout: 'bottomRight',
//                      text: 'Event Not Created',
//                      theme:'relax',type: 'error',
//                      timeout : '3000',
//                     animation: {
//                         open: 'animated bounceInLeft', // Animate.css class names
//                         close: 'animated bounceOutLeft', // Animate.css class names
//                         easing: 'swing', // unavailable - no need
//                         speed: 500 // unavailable - no need
//                     },
//                     callback: { 
//                         onClose: function() {
                            
//                            // wizardshowhide("clickable-second");
//                         } 
//                     },
//                     });

//                } 
//             }
       
//    });
    
// }
















  

 





//end
var abc = 0;   
var totalimage = 0;
$('#add_more').click(function() {

    if($("#filediv > div").length === 5){
    alert("You have already added five images");
    //break;
    }
    else{
        abc++;
    totalimage ++
    var text ='<div id="filediv'+abc+'"><div class="row"><div class="col-md-9"><input name="file[]" type="file" id="file"/></div><div class="col-md-3"><button type="button" class="btn btn-primary" style="color: #ffffff;border-radius: 26px;" onclick="btnimgaeremove('+abc+')">Remove Image</button></div></div></div>' ;
  
    $("#filediv").append(text);
    }
    
});

function btnimgaeremove(par)
{
    $("#filediv"+par).remove();
}

$('body').on('change', '#file', function() {
if (this.files && this.files[0]) {
//totalimage = abc;
//abc += 1; // Incrementing global variable by 1.
var z = abc - 1;
var x = $(this).parent().find('#previewimg' + z).remove();
$(this).before("<div id='abcd" + abc + "' class='abcd' style='width:300px;height:300px;' ><div><br><img class='img-rounded' id='previewimg" + abc + "' src='' style='float:left;'/></div></div> ");
var reader = new FileReader();
reader.onload = imageIsLoaded;
reader.readAsDataURL(this.files[0]);
$(this).hide();

}
});

console.log("Total Images Are:"+totalimage);

// To Preview Image
function imageIsLoaded(e) {
$('#previewimg' + abc).attr('src', e.target.result);
};
$('#upload').click(function(e) {
var name = $(":file").val();
if (!name) {
alert("First Image Must Be Selected");
e.preventDefault();
}
});

function uploadImage(){
       $('#loading').show();
//var productId = $("#productId").val();
//var gridImage = $("#previewimgGrid").src();
//var gridImage = document.getElementById("previewimgGrid").src;
var cusinename = $("#val-cuisine").val();
var inv = $("#val-invc").val();
var imgsrc =[]; 
    //alert(totalimage);
    //var text = document.getElementById("previewimg1").src;
    //alert(text.length);
    console.log("Total Images Are:"+totalimage);
    for (i=0 ; i < (totalimage + 1) ; i++) {
        try{
            //imgsrc[j] = 
            //alert(imgsrc[i]);
            imgsrc.push({
                "imagepath": document.getElementById("previewimg"+i).src
            
         });
         
        }catch (e){ }
        
    }
    //console.log("asdfgfdda"+gridImage);
    var partfour = {
       // "gridImage":gridImage,
        //"productId" : productId,
    "cusinename":cusinename,    
    "image": imgsrc,
    "invitecode":inv
    
    }
    
    console.log("Image response is"+JSON.stringify(partfour)); 

    $.ajax({
            type: 'POST',
            url :  'service.php?servicename=uploadimages',
            data : JSON.stringify(partfour),
            datatype: 'JSON',
            contentType: 'application/json',
            success : function(dt)
            {
                $("#loading").hide();
                console.log("Response Is:"+JSON.stringify(dt));

                var rac = JSON.parse(dt);

                if(rac.status == 'success')
                {


                    var n = noty({
                     layout: 'bottomRight',
                     text: 'Popular Cuisine Added Successfully',
                     theme:'relax',type: 'success',
                     timeout : '3000',
                     animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // document.getElementById("divcreateexp").style.display="none" ;
                                //document.getElementById("confirmView").style.display="block" ;
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });

                    window.location.href = 'popularcuisinelist.php';
                }
                else
                {
                    var n = noty({
                         layout: 'bottomRight',
                         text: 'Not able to add cuisine',
                         theme:'relax',type: 'error',
                         timeout : '3000',
                        animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });
                }  
            }



    })

    }

   

 function  walletlist()
 {
    $('#loading').show();

    $.ajax({
              type: 'GET',
              url: 'service.php?servicename=walletdisplay',
              success : function(wdet)
              {
                $('#loading').hide();
                  console.log("Wallet Response Is:"+JSON.stringify(wdet));

                  var wdt = JSON.parse(wdet);

                  var wdtl = new Array();

                  for(var wd=0;wd<wdt.wallet.length;wd++)
                  {
                      wdtl[wd] = new Array();

                      wdtl[wd][0] = wd+1;

                      // wdtl[wd][1] = '<a href="#myModal" onclick="openwalletitem('+wdt.wallet[wd].walletid+')" data-toggle="modal">'+wdt.wallet[wd].walletid+'</a>';
                      wdtl[wd][1] = wdt.wallet[wd].walletid;

                      wdtl[wd][2] = wdt.wallet[wd].name;

                      wdtl[wd][3] = wdt.wallet[wd].total;
                  }

                  $('#tableWalletList').dataTable({
                        "aaData": wdtl,
                        "bDestroy": true
                    });   


              }

    });

 }

 function openwalletitem(wid)
 {
    console.log("Wallet Item Is"+wid);

    var wltitm = {"walletid":wid};

    console.log("Wallet item:"+JSON.stringify(wltitm));

    $.ajax({
              type: 'POST',
              url : 'service.php?servicename=walletitem',
              datatype: 'JSON',
              contentType: 'application/json',
              data: JSON.stringify(wltitm),
              success: function(wlt)
              {
                  console.log("walletitem response:"+JSON.stringify(wlt));

                  var wl = JSON.parse(wlt);

                  var wli = new Array();

                  for(var w=0;w<wl.walletitems.length;w++)
                   {
                      wli[w] = new Array();

                      wli[w][0] = w+1;

                      wli[w][1] = wl.walletitems[w].amount;

                      wli[w][2] = wl.walletitems[w].paymentid;

                      wli[w][3] = wl.walletitems[w].paymentdate;

                      wli[w][4] = wl.walletitems[w].paymenttype;
                   }

                   $('#tableWalletItem').dataTable({
                        "aaData": wli,
                        "bDestroy": true
                    }); 


              }

    });
 }

 function popularcuisinelist()
 {
     $('#loading').show();

      $.ajax({
               type: 'GET',
               url: 'service.php?servicename=popularlist',
               success : function(popl)
               {
                   $('#loading').hide();
                  console.log("Popular Cuisine"+JSON.stringify(popl));

                  var poplc = JSON.parse(popl);

                  var po = new Array();

                  for(var p=0;p<poplc.popular.length;p++)
                  {
                     po[p] = new Array();

                     po[p][0] = p+1;

                     po[p][1] = poplc.popular[p].cuisinename;

                     po[p][2] = '<a href="#myModal" onclick="opencuisineimages('+poplc.popular[p].cuisinemasterid+')" data-toggle="modal">View Images</a>';
                  }

                  $('#tablepopcimgList').dataTable({
                        "aaData": po,
                        "bDestroy": true
                    });


               }
      })
 }

 function opencuisineimages(cmid)
 {
    console.log("Cuisine Master Id Is:"+JSON.stringify(cmid));

    var popularimg = {"cuisinemasterId":cmid};

    $.ajax({
              type: 'POST',
              url: 'service.php?servicename=popularimages',
              datatype: 'JSON',
              contentType: 'application/json',
              data: JSON.stringify(popularimg),
              success: function(popl)
              {
                  console.log("poularimage"+JSON.stringify(popl));

                  var pimg = JSON.parse(popl);

                  var pm = new Array();

                  for(var po=0;po<pimg.popimgs.length;po++)
                  {
                     pm[po] = new Array();

                     pm[po][0] = po+1;

                     pm[po][1] = "<img class='img-rounded' src='"+pimg.popimgs[po].pcimages+"' style='width:50%;height:200px;'/>";
                  }

                   $('#tablecuisinepopimg').dataTable({
                        "aaData": pm,
                        "bDestroy": true
                    });


              }

    });
 }


 function cuisinetypelist()
 {
    $('#loading').show();

    $.ajax({
              type: 'GET',
              url: 'service.php?servicename=getcuisinetype',
              success: function(cutls)
              {
                  $('#loading').hide();

                  console.log("Cuisine Type List"+JSON.stringify(cutls));

                  var ctl = JSON.parse(cutls);

                  var ct = new Array();

                  for(var t=0;t<ctl.ctypes.length;t++)
                  {
                      ct[t] = new Array();

                      ct[t][0] = t+1;

                      ct[t][1] = ctl.ctypes[t].cuisinetype;
                  }

                  $('#tablecuisinetypeList').dataTable({
                        "aaData": ct,
                        "bDestroy": true
                    });


              }

    });
 }

 function addrecharge()
 {
     $("#loading").show();

    var rechargeamt = $("#val-rechargeamt").val();

    var actualamt = $("#val-actualamt").val();

    console.log("Recharge Amt"+rechargeamt+"Actual Amt"+actualamt);

    var recharged = {"rechargeamount":rechargeamt,"actualamount":actualamt};

    console.log("Recharge Details"+JSON.stringify(recharged));

    $.ajax({
              type: 'POST',
              url: 'service.php?servicename=addrecharge',
              datatype: 'JSON',
              contentType: 'application/json',
              data: JSON.stringify(recharged),
              success: function(rch)
              {
                  $("#loading").hide();

                  console.log("Recharge Details Are:"+JSON.stringify(rch));
              }

    });
 }

 function rechargelist()
 {
    $('#loading').show();
    $.ajax({
              type: 'GET',
              url: 'service.php?servicename=rechargedetail',
              success: function(dtrch)
              {
                $('#loading').hide();
                console.log("Recharge Detail Are:"+JSON.stringify(dtrch));

                var rchg = JSON.parse(dtrch);

                var g = new Array();

                 for(var ch=0;ch<rchg.recharges.length;ch++)
                 {
                    g[ch] = new Array();

                    g[ch][0] = ch+1;

                    g[ch][1] = rchg.recharges[ch].rechargeamount;

                    g[ch][2] = rchg.recharges[ch].actualamount;

                    g[ch][3] = rchg.recharges[ch].createdate;
                 }

                  $('#tablerechargeList').dataTable({
                        "aaData": g,
                        "bDestroy": true
                    });


              }


    });
 }

 function builinglist()
 {
    $('#loading').show();

    $.ajax({
              type: 'GET',
              url: 'service.php?servicename=buildingdetail',
              success:function(bl)
              {
                  $('#loading').hide();

                  console.log("Building List Are"+JSON.stringify(bl));

                  var bld = JSON.parse(bl);

                  var b = new Array();

                  for(var bl=0;bl<bld.bldgs.length;bl++)
                  {
                     b[bl] = new Array();

                     b[bl][0] = bl+1;

                     b[bl][1] = bld.bldgs[bl].building;

                     b[bl][2] = bld.bldgs[bl].invitationcode;
                  }  

                   $('#tablebldgList').dataTable({
                        "aaData": b,
                        "bDestroy": true
                    });


              }

    })
 }

 function addbuilding()
 {

   $("#loading").show();

  var bldg = $("#val-bldgname").val();

  var invc = $("#val-selectInvitecode").val();

   console.log("Add Building"+bldg+"Invitation Id"+invc);

  var buildg = {"bldg":bldg,"invid":invc};

  console.log("Building Add Is:"+JSON.stringify(buildg));

  $.ajax({
           type: 'POST',
           url: 'service.php?servicename=addbldg',
           datatype: 'JSON',
           contentType: 'application/json',
           data: JSON.stringify(buildg),
           success: function(bld)
           {
               $("#loading").hide();
              console.log("My Buiding Response Is"+JSON.stringify(bld));

              var resb =  JSON.parse(bld);

              if(resb.status == 'success')
              {
                  var n = noty({
                     layout: 'bottomRight',
                     text: 'Added Building Successfully',
                     theme:'relax',type: 'success',
                     timeout : '3000',
                     animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // document.getElementById("divcreateexp").style.display="none" ;
                                //document.getElementById("confirmView").style.display="block" ;
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });
              }
              else
              {
                  var n = noty({
                         layout: 'bottomRight',
                         text: 'Not able to add building',
                         theme:'relax',type: 'error',
                         timeout : '3000',
                        animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });
              }  
           }

  });

 }

 function addtag()
 {

    $("#loading").show();

    var tg = '';

    tg = $("#val-tagname").val();

    var tags = {"tagname":tg};

    console.log("Tag Request:"+JSON.stringify(tags));

    $.ajax({

              type: 'POST',
              url: 'service.php?servicename=addtags',
              datatype: 'JSON',
              contentType: 'application/json',
              data: JSON.stringify(tags),
              success: function(tr)
              {
                  $("#loading").hide();
                  console.log("Add Tag Response Is:"+JSON.stringify(tr));

                  var rtg = JSON.parse(tr);

                  if(rtg.status == 'success')
                  {
                      var n = noty({
                     layout: 'bottomRight',
                     text: 'Cuisine Suggested Tag Added',
                     theme:'relax',type: 'success',
                     timeout : '3000',
                     animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // document.getElementById("divcreateexp").style.display="none" ;
                                //document.getElementById("confirmView").style.display="block" ;
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });
                  }
                  else
                  {
                      var n = noty({
                         layout: 'bottomRight',
                         text: 'Not able to add tag',
                         theme:'relax',type: 'error',
                         timeout : '3000',
                        animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });
                  }  
              }

    });
 }

 function taglist()
 {
     $('#loading').show();

    $.ajax({
            type: 'GET',
            url: 'service.php?servicename=tagdetail',
            success: function(tlg)
            {
               $('#loading').hide();
                console.log("Tl"+JSON.stringify(tlg));

                var tgl = JSON.parse(tlg);

                var tagl = new Array();

                for(var g=0;g<tgl.taglist.length;g++)
                {
                   tagl[g] = new Array();

                   tagl[g][0] = g+1;

                   tagl[g][1] = tgl.taglist[g].tagname;
                }

                $('#tabletagList').dataTable({
                        "aaData": tagl,
                        "bDestroy": true
                    });


            }


    });  
 }

 function craverslist()
 {
    $('#loading').show();

    $.ajax({
              type: 'GET',
              url: 'service.php?servicename=craversdetail',
              success: function(crvs)
              {
                $('#loading').hide();
                 console.log("Cravers Response:"+JSON.stringify(crvs));

                 var rsc = JSON.parse(crvs);

                 var cv = new Array();

                 for(var v=0;v<rsc.cravers.length;v++)
                 {
                    cv[v] = new Array();

                    cv[v][0] = v+1;

                    cv[v][1] = rsc.cravers[v].dish;

                    cv[v][2] = rsc.cravers[v].type;

                    cv[v][3] = '<a href="#myModalcraveimg" onclick="opencraveimg('+rsc.cravers[v].onreqcuid+')" data-toggle="modal">View Image</a>';

                    cv[v][4] = rsc.cravers[v].dishtype;

                    cv[v][5] = '<img src="img/like.png" style="width:25px;height:25px;"/>&nbsp;<a href="#myModal" onclick="opencravers('+rsc.cravers[v].onreqcuid+')" data-toggle="modal">'+rsc.cravers[v].likes+'</a>';

                    cv[v][6] = rsc.cravers[v].invitecode;
                 }

                 $('#tableCraverDetail').dataTable({
                        "aaData": cv,
                        "bDestroy": true
                    });
              }


    });
 }

 function opencraveimg(cvid)
 {
   console.log("Craving Id"+cvid);

   $('#tableCraveimg').dataTable().fnClearTable();
   $('#tableCraveimg').dataTable().fnDraw();
   $('#tableCraveimg').dataTable().fnDestroy();

   var craveid = {"cvid":cvid};

   $.ajax({
            type: 'POST',
            url: 'service.php?servicename=getCraveImage',
            datatype: 'JSON',
            contentType: 'application/json',
            async: false,
            data: JSON.stringify(craveid),
            success: function(rcvi)
            {
               console.log("Craved Images"+JSON.stringify(rcvi));

               var jci = JSON.parse(rcvi);

               var ci = new Array();

               for(var im=0;im<jci.cimgs.length;im++)
               {
                  ci[im] = new Array();

                  ci[im][0] = im+1;

                  ci[im][1] = "<img class='img-rounded' src='"+jci.cimgs[im].cvimages+"' style='width:200px;height:200px;'/>";
               } 

               $('#tableCraveimg').dataTable({
                        "aaData": ci,
                        "bDestroy": true
                    });


            }

   });

 }

 function opencravers(oreqid)
 {

        $('#tableCraverUsers').dataTable().fnClearTable();
        $('#tableCraverUsers').dataTable().fnDraw();
        $('#tableCraverUsers').dataTable().fnDestroy();


   console.log("Cravers Id is"+oreqid);

   var onreq = {"onreqid":oreqid};

   $.ajax({
            type: 'POST',
            url: 'service.php?servicename=getcravers',
            datatype: 'JSON',
            contentType: 'application/json',
            data: JSON.stringify(onreq),
            success: function(gcv)
            {
               console.log("cravers users details"+JSON.stringify(gcv));

               var ucv = JSON.parse(gcv);

               var uc = new Array();

               for(var u=0;u<ucv.craversdetail.length;u++)
               {
                  uc[u] = new Array();

                  uc[u][0] = u+1;

                  uc[u][1] = ucv.craversdetail[u].name;

                  uc[u][2] = "<img class='img-rounded' src='"+ucv.craversdetail[u].profilepic+"' style='width:100px;height:100px;'/>";
               }

                $('#tableCraverUsers').dataTable({
                        "aaData": uc,
                        "bDestroy": true
                    });

            }

   });
 }

 function orderlist()
{
    $('#loading').show();
    $.ajax({
                type:"GET",
                //cache: false,
                url:"service.php?servicename=getorders",
                datatype :"JSON",
               // dataType:"jsonp"
                //async : false,
               contentType : "application/json",
                success : function(dtod)
                {
                     $('#loading').hide();
                    console.log("My Orders ARe:"+JSON.stringify(dtod));

                    var ordl = JSON.parse(dtod);

                    var orders = new Array();

                    for(var o=0;o<ordl.ords.length;o++)
                     {
                        orders[o] = new Array();

                        orders[o][0] = o+1;
                        orders[o][1] = '<a href="#myModal" onclick="openiemlist('+ordl.ords[o].orderid+')" data-toggle="modal">'+ordl.ords[o].orderid+'</a>';
                        orders[o][3] = ordl.ords[o].total;
                        orders[o][2] = ordl.ords[o].customer;
                        orders[o][4] = ordl.ords[o].orderdate;
                        orders[o][5] = ordl.ords[o].invitecode;
                        orders[o][6] = ordl.ords[o].paymenttype;
						orders[o][7] = ordl.ords[o].cph;
						orders[o][8] = ordl.ords[o].add;
						orders[o][9] = ordl.ords[o].ctypes;
                        /*orders[o][7] = ordl.ords[o].transactionid;
                        orders[o][8] = ordl.ords[o].actualamount;
                        orders[o][9] = ordl.ords[o].discountamount;
                        orders[o][10] = ordl.ords[o].couponapplied;
                        orders[o][11] = ordl.ords[o].couponcode;*/
                     } 

                      $('#tableOrderList').dataTable({
                        "aaData": orders,
                        "bDestroy": true
                    });  
                } 

    });
}

function openiemlist(oid)
{
    console.log("Orderid is:"+oid);

    var orderid = {"orderid":oid};

    $.ajax({
                type: 'POST',
                cache: false,
                async: false,
                url: 'service.php?servicename=getorderitem',
             datatype: 'JSON',
             contentType: 'application/json',
             data: JSON.stringify(orderid),
             success : function(doid)
             {
                //console.log("Order items"+JSON.stringify(doi));

                var oitm = JSON.parse(doid);

                var orditem = new Array();

                console.log("Length:"+oitm.orderitems.length);

                for(var oi=0;oi<oitm.orderitems.length;oi++)
                {
                    orditem[oi] = new Array();

                    orditem[oi][0] = oi+1;
                    orditem[oi][1] = oitm.orderitems[oi].cuisinename;
                    orditem[oi][2] = oitm.orderitems[oi].chef;
                    orditem[oi][3] = oitm.orderitems[oi].price;
                    orditem[oi][4] = oitm.orderitems[oi].quantity;
                    orditem[oi][5] = oitm.orderitems[oi].type;
                    orditem[oi][6] = oitm.orderitems[oi].status;
                    orditem[oi][7] = oitm.orderitems[oi].code;
                    orditem[oi][8] = oitm.orderitems[oi].closedate;
                } 

                $('#tableOrderItemList').dataTable({
                        "aaData": orditem,
                        "bDestroy": true
                    }); 
             }

    });
}

function cuisinelist()
{
  $('#loading').show();

  $.ajax({
            type: "GET",
            //cache: false,
            url: "service.php?servicename=cuisines",
           datatype : "JSON",
           //contentType : 'application/json',
            success: function(rclist)
            {
               $('#loading').hide();
               
                console.log("Cuisines:"+JSON.stringify(rclist));

                var cl = '';

                cl = JSON.parse(rclist);

                var culi = new Array();

                for(var d=0;d<cl.cuisinesd.length;d++)
                {
                    culi[d] = new Array();

                    culi[d][0] = d+1;
                    culi[d][1] = cl.cuisinesd[d].cuisinename;
                    culi[d][2] = cl.cuisinesd[d].chef;
                    culi[d][3] = "<img class='img-rounded' src='"+cl.cuisinesd[d].cuisinepic+"' style='width:200px;height:200px;'/>";
                    culi[d][4] = cl.cuisinesd[d].cuisinedescription;
                    culi[d][5] = cl.cuisinesd[d].price;

                    if(cl.cuisinesd[d].isActive == 'Y')
                    {
                         culi[d][6] = "<button type='button' onclick='inactivecuisine("+cl.cuisinesd[d].cuisineid+");' class='btn btn-rounded btn-primary'>Active</button>";
                    }
                    else
                    {
                         culi[d][6] = "<button type='button' class='btn btn-rounded btn-primary'>Inctive</button>";
                    }  
                   
                    culi[d][7] = cl.cuisinesd[d].enddate;
                    culi[d][8] = cl.cuisinesd[d].pickuptime;
                    culi[d][9] = '<a href="#myModal" onclick="viewmore('+cl.cuisinesd[d].cuisineid+')" data-toggle="modal">View More</a>';
                }  

                $('#tableCuisineList').dataTable({
                        "aaData": culi,
                        "bDestroy": true
                    });
            }


  });
}

//By Dhruvpalsinh On 1-7-2017

function inactivecuisine(cid)
{
   console.log("CuisineId"+cid);

   var cuisineid = {"cid":cid};

   $.ajax({
            url: 'service.php?servicename=inactive_cuisine',
            type: 'POST',
            datatype: 'JSON',
            async: false,
            data: JSON.stringify(cuisineid),
            contentType: 'application/json',
            success: function(rsca)
            {
                console.log("Cuisine Active"+JSON.stringify(rsca));

                var jrsc = JSON.parse(rsca);

                if(jrsc.status == 'success')
                {
                   window.location.href = 'cuisinelist.php';
                }  
            }

   });
}

function viewmore(cid)
{
  console.log("My Cuisine Id Is:"+cid);

  var cuisineids = {"cuisineid":cid};

  console.log("My Detail Is"+JSON.stringify(cuisineids));

  $.ajax({
           type: 'POST',
           cache: false,
           url: 'service.php?servicename=getcuidetail',
           datatype: 'JSON',
           contentType: 'application/json',
           data: JSON.stringify(cuisineids),
           success : function(viewcu)
           {
              //console.log("My Response For Cuisine Detail Is"+JSON.stringify(viewc));

              var viewc = JSON.parse(viewcu);

              var cud = new Array();

              for(var cd=0;cd<viewc.cuisined.length;cd++)
              {
                  cud[cd] = new Array();

                  cud[cd][0] = cd+1;
                  cud[cd][1] = viewc.cuisined[cd].trending;
                  cud[cd][2] = viewc.cuisined[cd].onrequest;
                  cud[cd][3] = viewc.cuisined[cd].spicymeter;
                  cud[cd][4] = viewc.cuisined[cd].type;
                  cud[cd][5] = viewc.cuisined[cd].servingdetail;
                  cud[cd][6] = viewc.cuisined[cd].noofservings;

              }  

              $('#tablecuisinesmore').dataTable({
                        "aaData": cud,
                        "bDestroy": true
                    });
           }

  });
}


function sharedwallet()
{
    $('#loading').show();

    $.ajax({
               type: 'GET',
               url: 'service.php?servicename=sharedwalletdetail',
               success: function(shw)
               {
                  $('#loading').hide();

                  console.log("My Response for sharedwallet :"+JSON.stringify(shw));

                  var swd = JSON.parse(shw);

                  var shwd = new Array();

                  for(var sd=0;sd<swd.sharedwallet.length;sd++)
                  {
                     shwd[sd] = new Array();

                     shwd[sd][0] = sd+1;

                     shwd[sd][1] = swd.sharedwallet[sd].sharedwalletid;

                     shwd[sd][2] = swd.sharedwallet[sd].admin;

                     shwd[sd][3] = '<a href="#myModal1" onclick="viewmembers('+swd.sharedwallet[sd].sharedwalletid+')" data-toggle="modal">'+swd.sharedwallet[sd].members+'</a>';

                     shwd[sd][4] = swd.sharedwallet[sd].activityamount;

                     shwd[sd][5] = swd.sharedwallet[sd].regularamount;

                     shwd[sd][6] = swd.sharedwallet[sd].totalamount;

                     shwd[sd][7] = swd.sharedwallet[sd].createdate;

                     shwd[sd][8] = '<a href="#myModal" onclick="viewhistory('+swd.sharedwallet[sd].sharedwalletid+')" data-toggle="modal">View History</a>'; 

                  }

                   $('#tablesharedwalletList').dataTable({
                        "aaData": shwd,
                        "bDestroy": true
                    });


               }

    });
}

function viewhistory(swid)
{
  console.log("Shared Wallet Id Is:"+swid);

  var sharedhd = {"sharedwalletid":swid};

  $.ajax({
           type: 'POST',
           url: 'service.php?servicename=sharedwallettransaction',
           datatype: 'JSON',
           contentType: 'application/json',
           cache: false,
           async: false,
           data: JSON.stringify(sharedhd),
           success: function(swtd)
           {
              console.log("Shared Wallet Transaction"+JSON.stringify(swtd));

              var swth = JSON.parse(swtd);

              var swtc = new Array();

              if(swth.status == 'success')
              {
                  for(var st=0;st<swth.swtrans.length;st++)
                  {
                     swtc[st] = new Array();

                     swtc[st][0] = st+1;

                     swtc[st][1] = swth.swtrans[st].name;

                     swtc[st][2] = swth.swtrans[st].amount;

                     swtc[st][3] = swth.swtrans[st].type;

                     swtc[st][4] = swth.swtrans[st].transactiondate;
                  }

                  $('#tableswtransList').dataTable({
                            "aaData": swtc,
                            "bDestroy": true
                        }); 
              }
              else
              {
                $('#tableswtransList').dataTable({
                            "aaData": swtc,
                            "bDestroy": true
                        });
              }  

               

           }

  });
}

function viewmembers(shwid)
{
  console.log("SHared Wallet Id"+shwid);

  var shwm = {"sharedwalletid":shwid};

  $.ajax({
          type: 'POST',
          url: 'service.php?servicename=sharedwmembers',
          datatype: 'JSON',
          contentType: 'application/json',
          data: JSON.stringify(shwm),
          success: function(swmd)
          {
              console.log("Response Wallet members :"+swmd);

              var swmh = JSON.parse(swmd);

              var swmt = new Array();

              if(swmh.status == 'success')
              { 
                  for(var swt=0;swt<swmh.swmembers.length;swt++)
                  {
                      swmt[swt] = new Array();

                      swmt[swt][0] = swt+1;

                      swmt[swt][1] = swmh.swmembers[swt].name;

                      swmt[swt][2] = "<img class='img-rounded' src='"+swmh.swmembers[swt].profile+"' style='width:100px;height:100px;'/>";
                  }

                  $('#tableswmemberList').dataTable({
                            "aaData": swmt,
                            "bDestroy": true
                        }); 
              }
              else
              {
                  $('#tableswmemberList').dataTable({
                            "aaData": swmt,
                            "bDestroy": true
                        });
              }  

        }

  });

}

