<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: http://52.39.93.52/Casseroleadmin/index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
 <style type="text/css">
   
   @media screen {
  #printSection {
      display: none;
  }
}

@media print {
  body * {
    visibility:hidden;
  }
  #printSection, #printSection * {
    visibility:visible;
  }
  #printSection {
    position:absolute;
    left:0;
    top:0;
  }
}

 </style>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<!-- My Tree Option Available Modal -->

<div class="bs-example">
    
    <div id="pcmyModal" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Pay To Chef</h4>
          </div>
          <div class="modal-body">

          <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
        <p>My Tree Points Are : <span id="mtv"></span></p>
        <input type="hidden" name="codv" id="codv" value="">
        <input type="hidden" name="olpv" id="olpv" value="">
        <input type="hidden" name="itmids" id="itmids" value="">
        <input type="hidden" name="chefid" id="chefid" value="">
        <input type="hidden" name="cuid" id="cuid" value="">
        <div class="form-group">

<div class="col-md-9">
<div class="radio">
<label for="example-radio1">
<input type="radio" id="cash" name="example-radios" value="CASH"> Would you like to use cash
</label>
</div>
<div class="radio">
<label for="example-radio2">
<input type="radio" id="mtp" name="example-radios" value="MTP"> Would you like to use MTP
</label>
</div>

</div>
</div>
            
        </div>

        <button type="button" class="btn btn-rounded btn-info" onclick="next();">Next</button>
    </div>

           

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           
          </div>
        </div>
      </div>
    </div>

    
  </div> 

  <!-- Payment Calculation Modal-->

  <div class="bs-example">
    
    <div id="fpcmyModal" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Pay To Chef</h4>
          </div>
          <div class="modal-body">

          <div class="row">

<div class="col-md-6 col-lg-6">
<div class="block full">
<div class="block-title">
<h2>Net payout</h2>
</div>
<ol>
<li>OLP : <span id="olp"></span><input type="hidden" name="n_olp" id="n_olp" value="" ></li>
<li>COD : <span id="cod"></span><input type="hidden" name="n_cod" id="n_cod" value="" ></li>
<li>MTP : <span id="mtpn"></span><input type="hidden" name="n_mtp" id="n_mtp" value="" ></li>
<input type="hidden" name="n_itm" id="n_itm" value="" >
<hr>
</ol>
<span style="margin-left: 30px;">Total :</span><span id="total"></span><br><br>
<span id="nt" style="margin-left: 30px;color:red;display:none"><b>Note : By clicking Paynow button my tree points will be credited to the chef's account </b></span>
<input type="hidden" name="n_tot" id="n_tot" value="" >
<input type="hidden" name="n_cf" id="n_cf" value="">
<input type="hidden" name="n_cu" id="n_cu" value="">
<input type="hidden" name="pt" id="pt" value="">
</div>
</div>


</div>

          <button type="button" style="margin-left: 182px;" onclick="paynow()"  class="btn btn-rounded btn-primary">Paynow</button> 


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           
          </div>
        </div>
      </div>
    </div>

    
  </div>

  <!-- Payment Confirmation Modal -->

  <div class="bs-example">
    
    <div id="pnmyModal" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Pay To Chef</h4>
          </div>
          <div class="modal-body">

          <p>Do you want to Pay Now..?</p>
           
          </div>
          <div class="modal-footer">
          <input type="hidden" id="adsid" value=""><input type="hidden" id="cwatid" value="">
          <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-top: 5px;border: 1px solid #ce0d0d;background-color: transparent;border-radius: 2px;">Cancel</button>
          <button type="button" class="btn btn-default" id="approve" onclick="finalpay()"  data-dismiss="modal" style="margin-top: 5px;border: 1px solid #00ad2d;background-color: transparent;border-radius: 2px;">Approve</button>
            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
           
          </div>
        </div>
      </div>
    </div>

    
  </div>

  <!-- Receipt Modal -->

  <div class="bs-example" id="printThis">
    
    <div id="rcptModal" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Receipt</h4>
          </div>
          <div class="modal-body">

          <div class="row">
            
            <div class="col-sm-9">
<a href="javascript:void(0)" class="widget">
<div class="widget-content text-center">
<img src="" id="cfimg" alt="avatar" class="img-circle img-thumbnail img-thumbnail-avatar-2x">
<h2 class="widget-heading h3 text-muted" id="cfname"></h2>
</div>
<!-- <div class="widget-content themed-background-muted text-dark text-center">
<strong>Cuisine : <span id="cuname"></span></strong> 
</div> -->
<div class="widget-content">
<div class="row text-center">
<div class="col-xs-6">
<!-- <h3 class="widget-heading"><i class="gi gi-briefcase text-info"></i> <br><small>35 Projects</small></h3> -->
<h3 class="widget-heading"> <br><small><span>Net Payout : </span><span id="netp"></span></small></h3>
</div>
<div class="col-xs-6">
<!-- <h3 class="widget-heading"><i class="gi gi-heart_empty text-danger"></i> <br><small>5.3k Likes</small></h3> -->
<h3 class="widget-heading"> <br><small><span>Date : </span><span id="dt"></span></small></h3>
</div>
</div>
</div>
</a>
</div>

          </div>

      
           
          </div>
          <div class="modal-footer">
          <input type="hidden" id="adsid" value=""><input type="hidden" id="cwatid" value="">
         <!--  <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-top: 5px;border: 1px solid #ce0d0d;background-color: transparent;border-radius: 2px;">Cancel</button> -->
          <button type="button" class="btn btn-default" id="approve" onclick="print1()"  data-dismiss="modal" style="margin-top: 5px;border: 1px solid #00ad2d;background-color: transparent;border-radius: 2px;">Print</button>
            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
           
          </div>
        </div>
      </div>
    </div>

    
  </div>

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Order List</h1>
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">

    <div class="form-group">

<div class="col-md-6">
<label class="col-md-3 control-label" for="example-daterange1">Pickup Date :</label>
<div class="input-group input-daterange" data-date-format="yyyy/mm/dd">
<input type="text" id="frmdt" name="frmdt" class="form-control" placeholder="From">
<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
<input type="text" id="todt" name="todt" class="form-control" placeholder="To">


</div>

<div style="display: flex;">

<div class="input-group bootstrap-timepicker" style="width: 34%;
    margin-left: 132px;"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a href="#" data-action="incrementHour"><i class="fa fa-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><i class="fa fa-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementSecond"><i class="fa fa-chevron-up"></i></a></td></tr><tr><td><input type="text" class="form-control bootstrap-timepicker-hour" maxlength="2"></td> <td class="separator">:</td><td><input type="text" class="form-control bootstrap-timepicker-minute" maxlength="2"></td> <td class="separator">:</td><td><input type="text" class="form-control bootstrap-timepicker-second" maxlength="2"></td></tr><tr><td><a href="#" data-action="decrementHour"><i class="fa fa-chevron-down"></i></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><i class="fa fa-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="decrementSecond"><i class="fa fa-chevron-down"></i></a></td></tr></tbody></table></div>
<input type="text" id="frmtm" name="frmtm" placeholder="From Time" class="form-control input-timepicker24">
<span class="input-group-btn">
<a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary" style="overflow: hidden; position: relative;"><span class="btn-ripple animate" style="height: 38px; width: 38px; top: 3px; left: 12.625px;"></span><i class="fa fa-clock-o"></i></a>
</span>
</div>

<div class="input-group bootstrap-timepicker" style="width: 34%;
    margin-left: 39px;"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a href="#" data-action="incrementHour"><i class="fa fa-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><i class="fa fa-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementSecond"><i class="fa fa-chevron-up"></i></a></td></tr><tr><td><input type="text" class="form-control bootstrap-timepicker-hour" maxlength="2"></td> <td class="separator">:</td><td><input type="text" class="form-control bootstrap-timepicker-minute" maxlength="2"></td> <td class="separator">:</td><td><input type="text" class="form-control bootstrap-timepicker-second" maxlength="2"></td></tr><tr><td><a href="#" data-action="decrementHour"><i class="fa fa-chevron-down"></i></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><i class="fa fa-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="decrementSecond"><i class="fa fa-chevron-down"></i></a></td></tr></tbody></table></div>
<input type="text" id="totm" name="totm" placeholder="To Time" class="form-control input-timepicker24">
<span class="input-group-btn">
<a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary" style="overflow: hidden; position: relative;"><span class="btn-ripple animate" style="height: 38px; width: 38px; top: 3px; left: 12.625px;"></span><i class="fa fa-clock-o"></i></a>
</span>
</div>
  

</div>

</div>
<div class="col-md-3" >
   <button type="button" onclick="searchbypickup();" class="btn btn-effect-ripple btn-info" style="overflow: hidden; position: relative;">Find</button>
   <button type="button" onclick="reset();" class="btn btn-effect-ripple btn-success" style="overflow: hidden; position: relative;">Reset</button> 
</div>

</div>

<br><br>
    <!-- <button type="button" onclick="exporttoexcel()" class="btn btn-rounded btn-success" style="float:right">Export</button> -->
    <button type="button" id="exptexcel" class="btn btn-rounded btn-success" style="float:right">Export</button>
    <br><br>
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tableChefpOrderList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Chef Name</th>
                                                 <!-- <th>Cuisine Name</th> -->
                                                 <!-- <th>Price</th> -->
                                                 <th>Cuisines</th>
                                                 <th>Sold Qty</th>
                                                 <th>Sales Breakdown</th>
                                                 <th>Pay</th>
                                                 <!-- <th>History</th> -->
                                                 </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->



<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script src="js/CasseroleService.js"></script>

<!-- <script src="js/order.js"></script> -->


<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    $('#tableChefpOrderList').dataTable().fnClearTable();
        $('#tableChefpOrderList').dataTable().fnDraw();
        $('#tableChefpOrderList').dataTable().fnDestroy();

        // $('#tableOrderItemList').dataTable().fnClearTable();
        // $('#tableOrderItemList').dataTable().fnDraw();
        // $('#tableOrderItemList').dataTable().fnDestroy();

        
    // var type = '<?php 
    //echo $_SESSION['info']['type']; ?>';
    // var wedcode = '';
    // wedcode = '2616';
    // console.log("My Type Is :"+type);
    // if(type == "admin")
    // {
    //     getWeddingList();
    // }

    //invitationlist();
    //customerlist();
    chefporderlist();
    // if(type == "subadmin") 
    // {
    //    getWeddingListBySubadmin(wedcode); 
    // }   
    });</script>

    <script type="text/javascript">
        
    function chefporderlist()
    {
        $("#loading").show();
        $.ajax({
                 url: 'chefpayoutservice.php?servicename=ChefPayoutList',
                 type: 'GET',
                 success: function (col)
                 {
                    $("#loading").hide();
                     console.log("Cheforders Response"+JSON.stringify(col));

                     var rscol = JSON.parse(col);

                     var co = new Array();

                     for(var oc=0;oc<rscol.cpd.length;oc++)
                     {
                        co[oc] = new Array();

                        co[oc][0] = oc+1;

                        co[oc][1] = rscol.cpd[oc].chefname;

                        co[oc][2] = rscol.cpd[oc].cuisines;

                        co[oc][3] = rscol.cpd[oc].soldqty;



                        co[oc][4] =  ' OLP : '+ rscol.cpd[oc].OLP + ' | COD : '+ rscol.cpd[oc].COD + ' | MTP : '+  rscol.cpd[oc].MW;

                        // co[oc][2] = rscol.cpd[oc].cuisinename;

                        // co[oc][3] = rscol.cpd[oc].price;
                       if(rscol.cpd[oc].MW != 0)
                        {
                            // co[oc][4] = '<a href="#pcmyModal" onclick="mtparea(\''+rscol.cpd[oc].MW+'\',\''+rscol.cpd[oc].COD+'\',\''+rscol.cpd[oc].OLP+'\',\''+rscol.cpd[oc].itemid+'\',\''+rscol.cpd[oc].chefid+'\',\''+rscol.cpd[oc].cuisineid+'\')" data-toggle="modal">Pay</a>';

                            co[oc][5] = '<a href="#pcmyModal" onclick="mtparea(\''+rscol.cpd[oc].MW+'\',\''+rscol.cpd[oc].COD+'\',\''+rscol.cpd[oc].OLP+'\',\''+rscol.cpd[oc].itemid+'\',\''+rscol.cpd[oc].chefid+'\',1)" data-toggle="modal">Pay</a>';
                        }
                        else
                        {
                            // co[oc][4] = '<a href="#fpcmyModal" onclick="mtpareaf(\''+rscol.cpd[oc].MW+'\',\''+rscol.cpd[oc].COD+'\',\''+rscol.cpd[oc].OLP+'\',\''+rscol.cpd[oc].itemid+'\',\''+rscol.cpd[oc].chefid+'\',\''+rscol.cpd[oc].cuisineid+'\')" data-toggle="modal">Pay</a>';

                            co[oc][5] = '<a href="#fpcmyModal" onclick="mtpareaf(\''+rscol.cpd[oc].MW+'\',\''+rscol.cpd[oc].COD+'\',\''+rscol.cpd[oc].OLP+'\',\''+rscol.cpd[oc].itemid+'\',\''+rscol.cpd[oc].chefid+'\',1)" data-toggle="modal">Pay</a>';  
                        }  

                        

                        //co[oc][7] =  '<a href="#pcmyModal" onclick="" data-toggle="modal">History</a>';
                      
                                                                                                                 
                    }

                    $('#tableChefpOrderList').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true
                //         dom: '<"html5buttons"B>lTfgitp',
                // buttons: [
                //     { extend: 'copy'},
                //     {extend: 'csv'},
                //     {extend: 'excel', title: 'ExampleFile'},
                //     {extend: 'pdf', title: 'ExampleFile'},

                //     {extend: 'print',
                //      customize: function (win){
                //             $(win.document.body).addClass('white-bg');
                //             $(win.document.body).css('font-size', '10px');

                //             $(win.document.body).find('table')
                //                     .addClass('compact')
                //                     .css('font-size', 'inherit');
                //         }
                //     }
                // ]

                    });   
                 }    
        });
    }

    </script>

    <script type="text/javascript">
        
        function exporttoexcel()
        {
           // alert("Hii");
        //     $('#tableChefOrderList').tableExport({type:'excel',escape:'false'});

        //     var dt = new Date();
        // var day = dt.getDate();
        // var month = dt.getMonth() + 1;
        // var year = dt.getFullYear();
        // var hour = dt.getHours();
        // var mins = dt.getMinutes();
        // var postfix = day + "." + month + "." + year + "_" + hour;

        //     var a = document.createElement('a');
        // //getting data from our div that contains the HTML table
        // var data_type = 'data:application/vnd.ms-excel';
        // var table_div = document.getElementById('tableChefOrderList');
        // var table_html = table_div.outerHTML.replace(/ /g, '%20');
        // a.href = data_type + ', ' + table_html;
        // //setting the file name
        // a.download = 'Mytree_' + postfix + '.xls';
        // //triggering the function
        // a.click();
        // //just in case, prevent default behaviour
        // e.preventDefault();
        }
    </script>

    <script type="text/javascript">
        
        $(document).ready(function() {

          $("#frmtm").val('');
          $("#totm").val('');
  $("#exptexcel").click(function(e) {
    e.preventDefault();

    //getting data from our table
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('tableChefpOrderList');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');

    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
         var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    a.download = 'MyTree_' + postfix + '.xls';
    a.click();
  });
});


    </script>

    <script type="text/javascript">
        
    function searchbypickup()
    {
        $("#loading").show();
        var frmdate = '';
        var todate = '';
        var totm = '';
        var frmtm = '';
        var ftm = '';
        var fttm = '';

        frmdate = $("#frmdt").val();
        todate = $("#todt").val();
        
        frmtm = $("#frmtm").val();

        if(frmtm != '')
        {
            frmtm = frmtm.split(":");
             ftm = frmtm[0]+':'+frmtm[1];  
        }
        else
        {
           ftm = ''; 
        }  

        totm = $("#totm").val();

        if(totm != '')
        {
            totm = totm.split(":");
            fttm = totm[0]+':'+totm[1];  
        }
        else
        {
          fttm = '';
        }  
        

        // if(typeof ftm === 'undefined')
        // {
        //    ftm = '';
        // }

        // if(typeof fttm === 'undefined')
        // {
        //   fttm = '';
        // }  

        console.log("From Date"+frmdate+"to date"+todate+"From Time"+ftm+"To Time"+fttm);

        var reqpdt = {"fromdate":frmdate,"todate":todate,"fromtime":ftm,"totime":fttm};

        $.ajax({
                  url: 'chefpayoutservice.php?servicename=SearchByPickupDate',
                  type: 'POST',
                  datatype: 'JSON',
                  contentType: 'application/json',
                  data: JSON.stringify(reqpdt),
                  async: false,
                  success: function(rspd)
                  {
                     console.log("search response"+JSON.stringify(rspd));

                     $('#tableChefpOrderList').dataTable().fnClearTable();
                    $('#tableChefpOrderList').dataTable().fnDraw();
                    $('#tableChefpOrderList').dataTable().fnDestroy();

                    $("#loading").hide();
                     //console.log("Cheforders Response"+JSON.stringify(col));

                     var rscol = JSON.parse(rspd);

                     var co = new Array();

                     for(var oc=0;oc<rscol.cpd.length;oc++)
                     {
                        co[oc] = new Array();

                        co[oc][0] = oc+1;

                        co[oc][1] = rscol.cpd[oc].chefname;

                        co[oc][2] = rscol.cpd[oc].cuisines;

                        co[oc][3] = rscol.cpd[oc].soldqty;

                        co[oc][4] =  ' OLP : '+ rscol.cpd[oc].OLP + ' | COD : '+ rscol.cpd[oc].COD + ' | MTP : '+  rscol.cpd[oc].MW;

                        //co[oc][2] = rscol.cpd[oc].cuisinename;

                        //co[oc][3] = rscol.cpd[oc].price;
                        if(rscol.cpd[oc].MW != 0)
                        {
                            co[oc][5] = '<a href="#pcmyModal" onclick="mtparea(\''+rscol.cpd[oc].MW+'\',\''+rscol.cpd[oc].COD+'\',\''+rscol.cpd[oc].OLP+'\',\''+rscol.cpd[oc].itemid+'\',\''+rscol.cpd[oc].chefid+'\',1)" data-toggle="modal">Pay</a>';
                        }
                        else
                        {
                            co[oc][5] = '<a href="#fpcmyModal" onclick="mtpareaf(\''+rscol.cpd[oc].MW+'\',\''+rscol.cpd[oc].COD+'\',\''+rscol.cpd[oc].OLP+'\',\''+rscol.cpd[oc].itemid+'\',\''+rscol.cpd[oc].chefid+'\',1)" data-toggle="modal">Pay</a>';  
                        }  

                        

                       // co[oc][7] =  '<a href="#pcmyModal" onclick="" data-toggle="modal">History</a>';
                      
                                                                                                                 
                    }

                    $('#tableChefpOrderList').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true
                          }); 
                  }    
        });
    }

    </script>

    <script type="text/javascript">
        
    function reset()
    {
        $("#frmdt").val('');

        $("#todt").val('');

        //cheforderlist();
        location.reload();

   }

   function mtparea(mtpv,codv,olpv,itmid,cfid,cuid)
   {
      console.log("Itemid"+itmid);

      $("#itmids").val(itmid);

      $("#mtv").text(mtpv);

      $("#codv").val(codv);

      $("#olpv").val(olpv);

      $("#chefid").val(cfid);

      $("#cuid").val(cuid);
   }

   function next()
   {
      if(!$("input[name='example-radios']:checked").val())
      {
         alert("Please choose any one option");
         return false;
      }
      else
      {
         var cop =  $("input[name='example-radios']:checked").val();
      }

      console.log("checked value"+cop);

      var vmtp = '';

      vmtp = $("#mtv").text();

      var cod = 0;

      var nmtp =  0;

      var nolp = 0;

      var volp = '';

      var itid = '';

      var ncf = '';

      var ncu = '';

      ncf = $("#chefid").val();

      ncu = $("#cuid").val();

      $("#n_cf").val(ncf);

      $("#n_cu").val(ncu);

      itid = $("#itmids").val();

      volp = $("#olpv").val();

      var totv = 0;

      var f_mtp = 0;

      if(cop == 'CASH')
      {
          nmtp =(70 * vmtp) / 100;

          f_mtp = nmtp;

          console.log("New MTP"+nmtp);

          nolp = parseInt(volp);

          totv = parseInt(nmtp) + parseInt(volp);
      }
      else
      {
        // nmtp = 0;

        nmtp = parseInt(vmtp);

        // f_mtp = vmtp;

          //nolp = 0;

           nolp = parseInt(volp);

         // totv = parseInt(volp);

         totv = parseInt(volp) + parseInt(vmtp);

          f_mtp = parseInt(vmtp) + parseInt(volp);

          $("#nt").css("display","block");
      }

      $("#pt").val(cop);

      console.log("Total Is"+totv);

      $('#pcmyModal').modal('hide');

      $('#fpcmyModal').modal('show');

      $("#olp").text(nolp);

      $("#n_olp").val(nolp);

      $("#cod").text(cod);

      $("#n_cod").val(cod);

      $("#mtpn").text(nmtp);

      $("#n_mtp").val(f_mtp);

      $("#n_itm").val(itid);

      $("#total").text(totv);

      $("#n_tot").val(totv);




   }

   function paynow()
   {

      $('#fpcmyModal').modal('hide');

      $('#pnmyModal').modal('show');
   }

   function finalpay()
   {
       var fcid = $("#n_cf").val();

       var fcuid = $("#n_cu").val();

       var fpmt = $("#pt").val();

        var folp = $("#n_olp").val();

        var fcod = $("#n_cod").val();

        var fmtp = $("#n_mtp").val();

        var fitm = $("#n_itm").val();

        var ftot = $("#n_tot").val();

      console.log(" Cfid "+fcid+" Cuid "+fcuid+" Payment Type "+fpmt+ " OLP "+folp+  " COD "+fcod+ " MTP "+fmtp+" Itemids "+fitm+ "Total "+ftot);

      var reqpcf = {"chefid":fcid,"cuisineid":fcuid,"paymenttype":fpmt,"OLP":folp,"COD":fcod,"MTP":fmtp,"itemids":fitm,"total":ftot};

      $.ajax({
                url: 'chefpayoutservice.php?servicename=finalpay',
                type: 'POST',
                datatype: 'JSON',
                data: JSON.stringify(reqpcf),
                async: false,
                success: function(rsfp)
                {
                   console.log("Response"+JSON.stringify(rsfp));

                   var rsp = JSON.parse(rsfp);

                   if(rsp.status == 'success')
                   {
                      var cfn = '';
                      var cun =  '';
                      var cfp = '';
                      var t = '';
                      var d = '';

                      cfn = rsp.chefname;
                      cun = rsp.cuisinename;
                      cfp = rsp.chefpic;
                      t = rsp.total;
                      d = rsp.date;

                      $("#cfimg").attr('src',cfp);

                      $("#cfname").text(cfn);

                      $("#cuname").text(cun);

                      $("#netp").text(t);

                      $("#dt").text(d);



                       $('#rcptModal').modal('show');
                   } 
                }

      });




   }

   //mtpareaf(\''+rscol.cpd[oc].MW+'\',\''+rscol.cpd[oc].COD+'\',\''+rscol.cpd[oc].OLP+'\',\''+rscol.cpd[oc].itemid+'\',\''+rscol.cpd[oc].chefid+'\',\''+rscol.cpd[oc].cuisineid+'\')

   function mtpareaf(mtpf,codf,olpf,itif,cfidf,cuidf)
   {
      $("#olp").text(olpf);

      $("#n_olp").val(olpf);

      $("#cod").text(codf);

      $("#n_cod").val(codf);

      $("#mtpn").text(mtpf);

      $("#n_mtp").val(mtpf);

      $("#n_itm").val(itif);

      var fft = parseInt(mtpf) + parseInt(olpf);

      $("#total").text(fft);

      $("#n_tot").val(fft);

      $("#n_cf").val(cfidf);

      $("#n_cu").val(cuidf);

      var ptf = 'CASH';

      $("#pt").val(ptf);

   }

   function print1()
   {
      // $("#rcptModal").printThis({ 
      //       debug: false,              
      //       importCSS: true,             
      //       importStyle: true,         
      //       printContainer: true,       
      //       //loadCSS: "../css/style.css", 
      //       pageTitle: "Receipt",             
      //       removeInline: false,        
      //       printDelay: 333,            
      //       header: null,             
      //       formValues: true          
      //   }); 

      printElement(document.getElementById("printThis"));
   }

   function printElement(elem) {
    var domClone = elem.cloneNode(true);
    
    var $printSection = document.getElementById("printSection");
    
    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }
    
    $printSection.innerHTML = "";
    $printSection.appendChild(domClone);
    window.print();
}


    </script>