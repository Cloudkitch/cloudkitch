<?php

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['servicename'] == 'Chefs')
{
	$reqcf = file_get_contents('php://input');

	$rescf = json_decode($reqcf,true);

	$month = '';

	$yr = '';

	$invitecode = '';

	$month = $rescf['month'];

	$yr = $rescf['year'];

	$invitecode = $rescf['invitecode'];

	$cf = array();

	// $quecf = "SELECT userid,count(cuisineid) as cuisines FROM cuisine WHERE 
	// 		  MONTH(enddate)='".$month."' AND invitecode='".$invitecode."' group by userid HAVING COUNT(cuisines) 
	// 		  BETWEEN 3 AND 10 union
	// 		  SELECT userid,count(cuisineid) as cuisines FROM cuisine WHERE 
	// 		  MONTH(enddate)='".$month."' AND invitecode='".$invitecode."' group by userid HAVING COUNT(cuisines) <=3";

	$quecf = "SELECT userid,count(cuisineid) as cuisines FROM cuisine WHERE 
			  MONTH(enddate)='".$month."' AND YEAR(enddate) = '".$yr."' AND invitecode='".$invitecode."' group by userid HAVING COUNT(cuisines) <=3";

	$exccf = mysqli_query($conn,$quecf) or die(mysqli_error($conn));
	
	if(mysqli_num_rows($exccf) > 0)
	{
		$cf['chefs'] = array();

		while($rowcf = mysqli_fetch_assoc($exccf))
		{
			$f = array();

			$f['chefid'] = $rowcf['userid'];

			$quecnm =  "SELECT name FROM user WHERE userid='".$rowcf['userid']."'";
			$excnm =  mysqli_query($conn,$quecnm) or die(mysqli_error($conn));
			$rsnm = mysqli_fetch_assoc($excnm);

			$f['chefname'] = $rsnm['name'];

			$f['cuisines'] = $rowcf['cuisines'];

			$queactivedt = "SELECT enddate FROM cuisine where userid='".$rowcf['userid']."' AND MONTH(enddate)='".$month."' ORDER BY cuisineid DESC LIMIT 1";
			$excactdt = mysqli_query($conn,$queactivedt) or die(mysqli_error($conn));
			$rsactdt = mysqli_fetch_assoc($excactdt);

			$f['lastactivedate'] = $rsactdt['enddate'];

			array_push($cf['chefs'], $f);
		}

		$cf['status'] = 'success';
		$cf['msg'] = 'Data available';	
	}
	else
	{
		$cf['status'] = 'failure';
		$cf['msg'] = 'Data not available';	
	}

	print_r(json_encode($cf));
	exit;		  
}

if($_GET['servicename'] == 'Customers')
{
	$reqc = file_get_contents('php://input');

	$resc = json_decode($reqc,true);

	$c = array();

	$month = '';

	$yr = '';

	$invitecode =  '';

	$month = $resc['month'];

	$yr = $resc['year'];
		
	$invitecode = $resc['invitecode'];

	$quec = "SELECT userid,count(cassordid) as orders FROM casseroleorder WHERE 
			MONTH(orderdate)='".$month."' AND YEAR(orderdate) = '".$yr."' AND invitecode='".$invitecode."'  group by userid HAVING COUNT(orders) <=3";

	$excc = mysqli_query($conn,$quec) or die(mysqli_error($conn));
	
	if(mysqli_num_rows($excc) > 0)
	{
		$c['users'] = array();

		while($rowu = mysqli_fetch_assoc($excc))
		{
			$u = array();

			$u['userid'] = $rowu['userid'];

			$u['orders'] = $rowu['orders'];

			$queu = "SELECT name FROM user WHERE userid='".$rowu['userid']."'";
			$excu = mysqli_query($conn,$queu) or die(mysqli_error($conn));
			$rsu = mysqli_fetch_assoc($excu);

			$u['user'] = $rsu['name'];

			$queactivedt = "SELECT orderdate FROM casseroleorder where userid='".$rowu['userid']."' AND MONTH(orderdate)='".$month."' ORDER BY cassordid DESC LIMIT 1";
			$excactdt = mysqli_query($conn,$queactivedt) or die(mysqli_error($conn));
			$rsactdt = mysqli_fetch_assoc($excactdt);

			$u['lastorderdate'] = $rsactdt['orderdate'];

			array_push($c['users'], $u);
		}

		$c['status'] = 'success';
		$c['msg'] = 'Data available';
	}
	else
	{
		$c['status'] = 'failure';
		$c['msg'] = 'Data not available';
	}

	print_r(json_encode($c));
	exit;		  
}

?>