<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
     header("Location: http://localhost:7755/Casseroleadmin/index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <!-- <a href="#myModal" class="btn btn-lg btn-primary" data-toggle="modal">Launch
      Demo Modal</a> -->
    <!-- Modal HTML -->
    <div id="myModalpay" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Pay To Chef</h4>
          </div>
          <div class="modal-body">

          <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
        <form id="createcoupon" action="chefpayment.php"  method="post" class="form-horizontal form-bordered">

                <div class="form-group">
                        <label class="col-md-3 control-label" for="val-ccode">Amount<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-amount" name="val-amount" class="form-control" placeholder="Please Enter Amount ">
                        </div>
                    </div>

                                     

                          <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-3">
                        <input type="hidden" name="chefid" id="chefid" value="" >
                            <button type="submit"   class="btn btn-effect-ripple btn-primary" onclick="paytochef();">Submit</button>
                            
                        </div>
                    </div>
                </form>
            <!-- Form Validation Block -->
            <!-- <table id ="tablecuisinepopimg" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                    <th>Sr</th>
                                                    <th>Chef Name</th>
                                                    <th>Invitation Code</th>
                                                    <th>Total</th>
                                                    <th>Paid</th>
                                                    <th>Balance</th>
                                                    <th>Pay</th>
                                                 </tr> 
                                            </thead>
                                              
                                            </table> -->
            <!-- END Form Validation Block -->
        </div>
    </div>

           

          </div>
         <!--  <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div> -->
        </div>
      </div>
    </div>

    <div id="myModalhistory" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Chef Order History</h4>
          </div>
          <div class="modal-body">

          <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
        <!-- <form id="createcoupon" action="chefpayment.php"  method="post" class="form-horizontal form-bordered">

                <div class="form-group">
                        <label class="col-md-3 control-label" for="val-ccode">Amount<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-amount" name="val-amount" class="form-control" placeholder="Please Enter Amount ">
                        </div>
                    </div>

                                     

                          <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-3">
                        <input type="hidden" name="chefid" id="chefid" value="" >
                            <button type="submit"   class="btn btn-effect-ripple btn-primary" onclick="paytochef();">Submit</button>
                            
                        </div>
                    </div>
                </form> -->
            <!-- Form Validation Block -->
            <table id ="tablechefoh" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                    <th>Sr</th>
                                                    <th>Cuisine Name</th>
                                                    <th>Price</th>
                                                    <th>Quantity</th>
                                                 </tr> 
                                            </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>

           

          </div>
         <!--  <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div> -->
        </div>
      </div>
    </div>

    
  </div>

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Customer List</h1>
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tablecf" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                  <th>Chef Name</th>
                                                  <th>Total</th>
                                                  <th>Paid</th>
                                                  <th>Balance</th>
                                                  <th>Pay</th>
                                                  <th>History</th>
                                                
                                                </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<!-- <script src="js/CasseroleService.js"></script> -->
<!-- <script src="js/cuisine.js"></script>
 -->


<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
});
</script>
<script src="js/payment.js"></script>

<script type="text/javascript">
  
  chefpayment();
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    // var type = '<?php 
    //echo $_SESSION['info']['type']; ?>';
    // var wedcode = '';
    // wedcode = '2616';
    // console.log("My Type Is :"+type);
    // if(type == "admin")
    // {
    //     getWeddingList();
    // }

    //invitationlist();
   // customerlist();
    //cuisinelist();
    
    // if(type == "subadmin") 
    // {
    //    getWeddingListBySubadmin(wedcode); 
    // }   
    });</script>

    <script type="text/javascript">
      
      function paytochef()
      {
         var chef_id = '';

         chef_id = $("#chefid").val();

         console.log("Chef"+chef_id);

         var amt = '';

         amt = $("#val-amount").val();

         var cef = {"chefid":chef_id,"amount":amt};

         $.ajax({
                  type: 'POST',
                  url: 'service.php?servicename=paytoChef',
                  datatype: 'JSON',
                  contentType: 'application/json',
                  data: JSON.stringify(cef),
                  success: function(cf)
                  {
                     console.log("Res is"+JSON.stringify(cf));
                     
                  }

         });
      }
    </script>