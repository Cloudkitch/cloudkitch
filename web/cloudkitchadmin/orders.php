<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: https://coludkitch.co.in/cloudkitchadmin/index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <!-- <a href="#myModal" class="btn btn-lg btn-primary" data-toggle="modal">Launch
      Demo Modal</a> -->
    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Order Item List</h4>
          </div>
          <div class="modal-body">

          <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tableOrderItemList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Cuisine Name</th>
                                                 <th>Chef Name</th>
                                                 <th>Price</th>
                                                 <th>Quantity</th>
                                                 <th>Type</th>
                                                 <th>Status</th>
                                                 <th>Code</th>
                                                 <th>Closed Date</th>
                                               
                                                                                              
                                                </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>

           

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>
        </div>
      </div>
    </div>

    
  </div>

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Order List</h1>
                    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tableOrderListn" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Order Id</th>
                                                 <th>Customer</th>
                                                 <th>Total</th>
                                                 <th>Order Date</th>
                                                 <th>Invitecode</th>
                                                 <th>Payment Type</th>
                        												 <th>Customer No.</th>
                        												<th>Customer Addrs</th>
                        												<th>Cuisine Type</th>	
                                                <th>Order History</th>		
                                                 <!-- <th></th> -->
                                                
                                                </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->



<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<!-- <script src="js/CasseroleService.js"></script> -->

<!-- <script src="js/order.js"></script> -->


<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    $('#tableOrderListn').dataTable().fnClearTable();
        $('#tableOrderListn').dataTable().fnDraw();
        $('#tableOrderListn').dataTable().fnDestroy();

        // $('#tableOrderItemList').dataTable().fnClearTable();
        // $('#tableOrderItemList').dataTable().fnDraw();
        // $('#tableOrderItemList').dataTable().fnDestroy();

        
    // var type = '<?php 
    //echo $_SESSION['info']['type']; ?>';
    // var wedcode = '';
    // wedcode = '2616';
    // console.log("My Type Is :"+type);
    // if(type == "admin")
    // {
    //     getWeddingList();
    // }

    //invitationlist();
    //customerlist();
    orderlistnew();
    // if(type == "subadmin") 
    // {
    //    getWeddingListBySubadmin(wedcode); 
    // }   
    });</script>

    <script type="text/javascript">
      
      function orderlistnew()
{
    var invc = $("#invitecode").val();

    var reqo2 = {"invitecode":invc};

    $('#loading').show();
    $.ajax({
                type:"POST",
                //cache: false,
                url:"servicenew.php?servicename=getordersnew",
                datatype :"JSON",
                data: JSON.stringify(reqo2),
               // dataType:"jsonp"
                async : false,
               contentType : "application/json",
                success : function(dtod)
                {
                     $('#loading').hide();
                    console.log("My Orders ARe:"+JSON.stringify(dtod));

                    var ordl = JSON.parse(dtod);

                    var orders = new Array();

                    for(var o=0;o<ordl.ords.length;o++)
                     {
                        orders[o] = new Array();

                        orders[o][0] = o+1;
                        // orders[o][1] = '<a href="#myModal" onclick="openiemlist('+ordl.ords[o].orderid+')" data-toggle="modal">'+ordl.ords[o].orderid+'</a>';
                        orders[o][1] = ordl.ords[o].orderid;
                        orders[o][3] = ordl.ords[o].total;
                        orders[o][2] = ordl.ords[o].customer;
                        orders[o][4] = ordl.ords[o].orderdate;
                        orders[o][5] = ordl.ords[o].invitecode;
                        orders[o][6] = ordl.ords[o].paymenttype;
            orders[o][7] = ordl.ords[o].cph;
            orders[o][8] = ordl.ords[o].add;
            orders[o][9] = ordl.ords[o].ctypes;
            orders[o][10] = ordl.ords[o].orderd;
                        /*orders[o][7] = ordl.ords[o].transactionid;
                        orders[o][8] = ordl.ords[o].actualamount;
                        orders[o][9] = ordl.ords[o].discountamount;
                        orders[o][10] = ordl.ords[o].couponapplied;
                        orders[o][11] = ordl.ords[o].couponcode;*/
                     } 

                      $('#tableOrderListn').dataTable({
                        "aaData": orders,
                        "scrollX": true,
                        "bDestroy": true
                    });  
                } 

    });
}

    </script>