<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <!-- <a href="#myModal" class="btn btn-lg btn-primary" data-toggle="modal">Launch
      Demo Modal</a> -->
    <!-- Modal HTML -->
    <div id="myModalcuisines" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Cuisine Detail</h4>
          </div>
          <div class="modal-body">

          <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tablecuList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Cuisine Name</th>
                                                 <th>Sold Qty</th>
                                                 <th>Paid Date</th>
                                                                                                
                                                                                              
                                                </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>

           

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>
        </div>
      </div>
    </div>

    
  </div>

  <!-- Due History Start -->


  <div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <!-- <a href="#myModal" class="btn btn-lg btn-primary" data-toggle="modal">Launch
      Demo Modal</a> -->
    <!-- Modal HTML -->
    <div id="myModaldues" class="modal fade">
      <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
              aria-hidden="true">&times;</button>
            <h4 class="modal-title">Dues Detail</h4>
          </div>
          <div class="modal-body">

          <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tabledueList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Orderid</th>
                                                 <th>Cuisine</th>
                                                 <th>Chefname</th>
                                                 <th>Payment Type</th>
                                                 <th>Amount</th>
                                                 </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>

           

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>
        </div>
      </div>
    </div>

    
  </div>


  <!-- Due History End -->

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section" id="cp">
                    <h1>Chef Payout Cancel Order</h1>
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">

    <button type="button" id="exptexcel" class="btn btn-rounded btn-success" style="float:right">Export</button>
    <br><br>
        <div class="col-sm-10 col-md-12 col-lg-12">
        <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
            <!-- Form Validation Block -->
            <table id ="tableChefphListcancelo" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Chef Name</th>
                                                 <th>Payment Mode</th>
                                                 <th>Pay</th>
                                                 <th>History</th>
                                                 </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->



<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script src="js/CasseroleService.js"></script>

<!-- <script src="js/order.js"></script> -->


<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

        $('#tableChefphListcancelo').dataTable().fnClearTable();
        $('#tableChefphListcancelo').dataTable().fnDraw();
        $('#tableChefphListcancelo').dataTable().fnDestroy();

        $('#tabledueList').dataTable().fnClearTable();
        $('#tabledueList').dataTable().fnDraw();
        $('#tabledueList').dataTable().fnDestroy();

        chefpaycancel();
       
    });</script>

    <script type="text/javascript">
        
    function chefpaycancel()
    {
       var invitecode = '';

       invitecode = $("#invitecode").val();

       var reqhc = {"invitecode":invitecode};

        $("#loading").show();
        $.ajax({
                 url: 'chefpayout-cancelorders-service.php?servicename=ChefPayoutList-Cancelorders',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(reqhc),
                 async: false,
                 success: function (col)
                 {
                    $("#loading").hide();
                     console.log("Cheforders Response"+JSON.stringify(col));

                     var rscol = JSON.parse(col);

                     var co = new Array();

                     for(var oc=0;oc<rscol.chefpcod.length;oc++)
                     {
                        co[oc] = new Array();

                        co[oc][0] = oc+1;

                        co[oc][1] = rscol.chefpcod[oc].chefname;

                        co[oc][2] =  ' OLP : '+ rscol.chefpcod[oc].OLP + ' | COD : '+ rscol.chefpcod[oc].COD + ' | MTP : '+  rscol.chefpcod[oc].MW;

                        co[oc][3] = '<a onclick="payco(\''+rscol.chefpcod[oc].chefid+'\',\''+rscol.chefpcod[oc].OLP+'\',\''+rscol.chefpcod[oc].COD+'\',\''+rscol.chefpcod[oc].MW+'\',\''+rscol.chefpcod[oc].itemid+'\')">Pay</a>'; 

                        // if(rscol.chefpcod[oc].COD != 0)
                        // {
                            //co[oc][4] = '<button type="button" class="btn btn-rounded btn-primary">info</button>';

                            co[oc][4] = '<a href="#myModaldues" onclick="viewdues('+rscol.chefpcod[oc].chefid+')" data-toggle="modal"><button type="button" class="btn btn-rounded btn-primary">info</button></a>';     
                        // }
                        // else
                        // {
                        //     co[oc][4] = '';  
                        // }  

                                                                                                                         
                    }

                    $('#tableChefphListcancelo').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true
                    });   
                 }    
        });
    }

    </script>

    <script type="text/javascript">
      
    function payco(chefid,olp,cod,mw,itemid)
    {
        console.log("Cfid :"+chefid+"OLP  :"+olp+"COD :"+cod+"MTP :"+mw+"Items :"+itemid);

        $('#cp').append('<form action="chefpayoutcancelorderdetail.php" id="cpd" name="cpd" method="post" style="display:none;"><input type="text" name="itmids" value="' + itemid + '" /><input type="text" name="mtpv" value="' + mw + '" /><input type="text" name="olpv" value="'+olp+'" /><input type="text" name="cfid" value="'+chefid+'" ></form>');

        $('#cpd').submit();

        
    }

    </script>

    <script type="text/javascript">
        
        $(document).ready(function() {


        function download_csv(csv, filename) {
            var csvFile;
            var downloadLink;
            // CSV FILE
            csvFile = new Blob([csv], {type: "text/csv"});
            // Download link
            downloadLink = document.createElement("a");
            // File name
            downloadLink.download = filename;
            // We have to create a link to the file
            downloadLink.href = window.URL.createObjectURL(csvFile);
            // Make sure that the link is not displayed
            downloadLink.style.display = "none";
            // Add the link to your DOM
            document.body.appendChild(downloadLink);
            // Lanzamos
            downloadLink.click();
        }

        function export_table_to_csv(html, filename) {
            var csv = [];
            var rows = document.querySelectorAll("table tr");

            for (var i = 0; i < rows.length; i++) {
            var row = [], cols = rows[i].querySelectorAll("td, th");

            for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText);        
            csv.push(row.join(","));       
            }
            // Download CSV
            download_csv(csv.join("\n"), filename);
        }

        document.querySelector("#exptexcel").addEventListener("click", function () {
        var html = document.querySelector("table").outerHTML;
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
        filename = 'MyTree_Cancel_Orders' + postfix + '.csv';
        export_table_to_csv(html, filename);
        });

});


    </script>

    <script type="text/javascript">
      
    function viewdues(cfid)
    {
        //console.log("Chefid"+cfid);

        var reqdue = {"chefid":cfid};

        $.ajax({

                 url:'chefpayout-cancelorders-service.php?servicename=Due-History',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(reqdue),
                 async: false,
                 success: function(rsd)
                 {
                    console.log("Json"+JSON.stringify(rsd));

                    var rsdd = JSON.parse(rsd);

                    if(rsdd.status == 'success')
                    {
                        var du = new Array();

                        for(var dc=0;dc<rsdd.dues.length;dc++)
                        {
                           du[dc] = new Array();

                           du[dc][0] = dc+1;

                           du[dc][1] = rsdd.dues[dc].orderid;

                           du[dc][2] = rsdd.dues[dc].dish;

                           du[dc][3] = rsdd.dues[dc].chefname; 

                           du[dc][4] = rsdd.dues[dc].paymenttype; 

                           du[dc][5] = rsdd.dues[dc].amount; 
                        } 

                        $('#tabledueList').dataTable({
                        "aaData": du,
                        "bDestroy": true,
                        "autoWidth": true
                    }); 


                    }
                    else
                    {

                    }  
                 }

        }); 
    }

    </script>

    

    
    