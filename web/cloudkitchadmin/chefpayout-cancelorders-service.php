<?php 

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['servicename'] == 'ChefPayoutList-Cancelorders')
{
	$cpco =  array();

	$reqcpco = file_get_contents('php://input');

	$rescpco = json_decode($reqcpco,true);

	$invitecode = '';

	$invitecode = $rescpco['invitecode'];

	$quecpco = "SELECT (SELECT name FROM user as c WHERE c.userid=co.chefid) as chefname,co.chefid FROM codcancelorders as co WHERE co.chefpaid='N' AND co.isCleared='Y' AND co.invitecode='".$invitecode."' GROUP BY co.chefid";

	$exccpco = mysqli_query($conn,$quecpco) or die(mysqli_error($conn));

	if(mysqli_num_rows($exccpco) > 0)
	{
		$cpco['chefpcod'] = array();

		while($rowco =  mysqli_fetch_assoc($exccpco))
		{
			$co = array();

			$co['chefid'] = $rowco['chefid'];

			$co['chefname'] = $rowco['chefname'];

			//COD Calculations
			$quecod =  "SELECT SUM(co.chefamount)as COD FROM codcancelorders as co WHERE co.chefpaid='N' AND co.isCleared='Y' AND co.paymenttype='COD' AND co.chefid= '".$rowco['chefid']."' AND co.invitecode='".$invitecode."'";

			$exccod = mysqli_query($conn,$quecod) or die(mysqli_error($conn));

			$rscod = mysqli_fetch_assoc($exccod);

			if($rscod['COD'] != null || $rscod['COD'] != '')
			{
			   $co['COD'] = $rscod['COD'];	
			}
			else
			{
				$co['COD'] = 0;
			}	

			//OLP Calculations
			$queolp = "SELECT SUM(co.chefamount)as OLP FROM codcancelorders as co WHERE co.chefpaid='N' AND co.isCleared='Y' AND co.paymenttype='OLP' AND co.chefid= '".$rowco['chefid']."' AND co.invitecode='".$invitecode."'";

			$excolp = mysqli_query($conn,$queolp) or die(mysqli_error($conn));

			$rsolp = mysqli_fetch_assoc($excolp);

			if($rsolp['OLP'] != null || $rsolp['OLP'] != '')
			{
				$co['OLP'] = $rsolp['OLP'];
			}
			else
			{
				$co['OLP'] = 0;
			}

			//MTP(MW) Calculations
			$quemw =  "SELECT SUM(co.chefamount+co.MTP)as MW FROM codcancelorders as co WHERE co.chefpaid='N' AND co.isCleared='Y' AND co.paymenttype='MW' AND co.chefid= '".$rowco['chefid']."' AND co.invitecode='".$invitecode."'";

			$excmw = mysqli_query($conn,$quemw) or die(mysqli_error($conn));

			$rsmw = mysqli_fetch_assoc($excmw);

			if($rsmw['MW'] != null || $rsmw['MW'] != '')
			{
				$co['MW'] = $rsmw['MW']; 
			}		
			else
			{
				$co['MW'] = 0;
			}

			//Itmids
			$queitms = "SELECT GROUP_CONCAT(co.cassitemid) as itemid FROM codcancelorders as co 
						WHERE co.chefpaid='N' AND co.isCleared='Y' AND co.invitecode='".$invitecode."' AND
						co.chefid='".$rowco['chefid']."'";
			$excitms = mysqli_query($conn,$queitms) or die(mysqli_error($conn));
			
			$rsitms = mysqli_fetch_assoc($excitms);

			if($rsitms['itemid'] != null || $rsitms['itemid'] != '')
			{
				$co['itemid'] = $rsitms['itemid'];
			}
			else
			{
				$co['itemid'] = 0;
			}

			array_push($cpco['chefpcod'], $co);				
		}

		$cpco['status'] =  'success';
		$cpco['msg'] = 'Data found..!!';	
	}	
	else
	{
		$cpco['status'] = 'failure';
		$cpco['msg'] = 'Data not found..!!';
	}

	print_r(json_encode($cpco));
	exit;	
}

if($_GET['servicename'] == 'Cancel-Order-Payout')
{
	//{"mtppm":"CASH","olppm":"","itmary":"3246","chefid":"947","mtp":"1.40","olp":"","invitecode":"9999"}:

	$reqcop = file_get_contents('php://input');

	$rescop = json_decode($reqcop,true);

	$mtppm = '';

	$olppm = '';

	$itmary = '';

	$chefid = '';

	$mtp = '';

	$olp = '';

	$invitecode = '';

	$mtpstatus = '';

	$olpstatus = '';

	$dt = date('Y-m-d');

	$cop = array();

	$mtppm = $rescop['mtppm'];

	$olppm = $rescop['olppm'];

	$itmary = $rescop['itmary'];

	$chefid = $rescop['chefid'];

	$mtp = $rescop['mtp'];

	$olp = $rescop['olp'];

	$invitecode = $rescop['invitecode'];

	$nitmids = '';

	$nitmids1 = '';

	$walletid = '';

	$ndate = date('Y-m-d H:i:s',strtotime("+210 minutes"));

	$quewmd = "SELECT walletid FROM walletmaster WHERE userid='".$chefid."'";
	
	$excwmd = mysqli_query($conn,$quewmd) or die(mysqli_error($conn));
	
	$rswmd = mysqli_fetch_assoc($excwmd);

	$walletid = $rswmd['walletid'];

	if($mtp != '')
	{
		$nitmids = explode(',', $itmary);

		if($mtppm == 'MTP')
		{
			$mtpstatus = 'DONE';
		}
		else if($mtppm == 'CASH')
		{
			$mtpstatus = 'PENDING';
		}

		$quemitm = "SELECT group_concat(cassitemid) as itids FROM codcancelorders WHERE cassitemid IN ('".implode("','",  $nitmids)."') AND paymenttype !='OLP'";

		$excmitm = mysqli_query($conn,$quemitm) or die(mysqli_error($conn));

		$rsmitm = mysqli_fetch_assoc($excmitm);

		if(mysqli_num_rows($excmitm) > 0)
		{
			$queimw =  "INSERT INTO chefcancelorderpayout SET chefid='".$chefid."',itemid='".$rsmitm['itids']."',total='".$mtp."',paymenttype='".$mtppm."',	paidstatus='".$mtpstatus."',invitecode='".$invitecode."',createdate='".$ndate."'";

			$excimw = mysqli_query($conn,$queimw) or die(mysqli_error($conn));

		}

		if($mtppm == 'MTP')
		{
			$updmwm =  "UPDATE walletmaster SET cloudkitchValue = cloudkitchValue  + '".$mtp."' WHERE userid = '".$chefid."' ";

			$excmwm = mysqli_query($conn,$updmwm) or die(mysqli_error($conn));

			if($excmwm)
			{
				$qwimw = "INSERT INTO walletitem SET walletid='".$walletid."',userid='".$chefid."',amount='".$mtp."',paymentdate='".$ndate."',paymenttype='CHEFPAYOUT',invitecode='".$invitecode."' ";

				$excimw = mysqli_query($conn,$qwimw) or die(mysqli_error($conn));
			}	
		}	
	}

	if($olp != '')
	{
		$nitmids1 = explode(',', $itmary);

		if($olppm == 'MTP')
		{
			$olpstatus = 'DONE';
		}
		else if($olppm == 'CASH')
		{
			$olpstatus = 'PENDING';
		}

		$queoitm = "SELECT group_concat(cassitemid) as itids FROM codcancelorders WHERE cassitemid IN ('".implode("','",  $nitmids1)."') AND paymenttype ='OLP'";

		$excoitm = mysqli_query($conn,$queoitm) or die(mysqli_error($conn));

		$rsoitm = mysqli_fetch_assoc($excoitm);

		if(mysqli_num_rows($excoitm) > 0)
		{
			$queiolp = "INSERT INTO chefcancelorderpayout SET chefid='".$chefid."',itemid='".$rsoitm['itids']."',total='".$olp."',paymenttype='".$olppm."',	paidstatus='".$olpstatus."',invitecode='".$invitecode."',createdate='".$ndate."'";

			$exciolp =  mysqli_query($conn,$queiolp) or die(mysqli_error($conn));
		}

		if($olppm == 'MTP')
		{
			$updowm = "UPDATE walletmaster SET cloudkitchValue = cloudkitchValue  + '".$olp."' WHERE userid='".$chefid."' ";

			$excowm = mysqli_query($conn,$updowm) or die(mysqli_error($conn));

			if($excowm)
			{
				$quiom = "INSERT INTO walletitem SET walletid='".$walletid."',userid='".$chefid."',amount='".$olp."',paymentdate='".$ndate."',paymenttype='CHEFPAYOUT',invitecode='".$invitecode."'";
				
				$exciom = mysqli_query($conn,$quiom) or die(mysqli_error($conn));	
			}	
		}	
	}

	if($excimw || $exciolp)
	{
		//All items for payout   
		$nitmids = explode(',', $itmary);

		 foreach($nitmids as $itemid)
	     {
	        $updps = "UPDATE codcancelorders SET chefpaid='Y' WHERE cassitemid='".$itemid."'";
	        $excus = mysqli_query($conn,$updps) or die(mysqli_error($conn));
	     }

	     if($excus)
	     {
	     	$cop['status'] = 'success';
	     	$cop['msg'] = 'Successfully paid to chef';
	     }
	     else
	     {
	     	$cop['status'] = 'failure';
	     	$cop['msg'] = 'Not able to update paid status';
	     }	
	}
	else
	{
		$cop['status'] = 'failure';
		$cop['msg'] = 'Failed to payout..!!';
	}

	print_r(json_encode($cop));
	exit;	
}

if($_GET['servicename'] == 'Due-History')
{
	$reqdh = file_get_contents('php://input');

	$resdh = json_decode($reqdh,true);

	$chefid = '';

	$chefid = $resdh['chefid'];

	$dh = array();

	// $quedue = "SELECT canceloid,nextorderid,nextitemid,nchefid,chefamount,(SELECT name FROM user WHERE userid=nchefid) as chefname FROM codcancelorders WHERE paymenttype='COD' AND chefid='".$chefid."'";

	$quedue = "SELECT canceloid,cassordid,cassitemid,chefid,(SELECT cuisinename FROM cuisine WHERE cuisineid=co.cuisineid)
			 as cuisine,nextorderid,nextitemid,nchefid,chefamount,paymenttype,
			(SELECT name FROM user WHERE userid=nchefid) as chefname,
			(SELECT name FROM user WHERE userid=chefid) as mchefname
			FROM codcancelorders as co WHERE chefid='".$chefid."' AND chefpaid='N' AND isCleared='Y'";

	$excdue = mysqli_query($conn,$quedue) or die(mysqli_error($conn));

	if(mysqli_num_rows($excdue) > 0)
	{
		$dh['dues'] = array();

		while($rowdu = mysqli_fetch_assoc($excdue))
		{
			$d = array();

			$d['canceloid'] = $rowdu['canceloid'];

			if($rowdu['paymenttype'] == 'COD')
			{
				$d['orderid'] = $rowdu['nextorderid'];

				$d['nchefid'] = $rowdu['nchefid'];

				$d['chefname'] = $rowdu['chefname'];
			}
			else
			{
				$d['orderid'] = $rowdu['cassordid'];

				$d['nchefid'] = $rowdu['chefid'];

				$d['chefname'] = $rowdu['mchefname'];
			}

			$d['dish'] = $rowdu['cuisine'];	
	
			$d['amount'] = $rowdu['chefamount'];

			

			$d['paymenttype'] = $rowdu['paymenttype'];

			array_push($dh['dues'], $d);
		}

		$dh['status'] = 'success';
		$dh['msg'] = 'Data available';	
	}
	else
	{
		$dh['status'] = 'failure';
		$dh['msg'] = 'Data not available';
	}

	print_r(json_encode($dh));
	exit;	

}

?>