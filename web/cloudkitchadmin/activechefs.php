<?php 

session_start();

//print_r($_SESSION);

if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/Casseroleadmin/index.php");
	header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
// include 'inc/config.php'; $template['header_link'] = 'WELCOME ';
 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Validation Header -->
    <!-- <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Create Invitation Code</h1>
                </div>
            </div>
           
        </div>
    </div> -->

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <div class="block">
                <!-- Form Validation Title -->
                <div class="block-title">
                    <h2>Active Chef</h2>
                </div>
                <!-- END Form Validation Title -->

                <!-- Form Validation Form -->
                
                <!--event_start_time,event_end_time,address,info-->
                <form id="cusordreport" action="activechefs.php"  method="post" class="form-horizontal form-bordered">

                <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">

                    <div class="form-group" id="year">
                        <label class="col-md-3 control-label" for="val-mt">Select From Date<span class="text-danger">*</span></label>
                        <div class="col-md-6" >
                            <div class="input-group input-daterange" data-date-format="yyyy/mm/dd">
                              <input type="text" id="frmdt" name="frmdt" class="form-control" placeholder="From">
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="selectInvite">
                        <label class="col-md-3 control-label" for="val-mt">Select End Date<span class="text-danger">*</span></label>
                        <div class="col-md-6" >
                           <div class="input-group input-daterange" data-date-format="yyyy/mm/dd">
                                <input type="text" id="todt" name="todt" class="form-control" placeholder="To">
                           </div>
                        </div>
                    </div>

                                        
                    <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-3">
                            <!-- <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button> -->
                            <button type="button" onclick="activechefs();" class="btn btn-effect-ripple btn-primary">Submit</button>
                            <button type="button" onclick="reset();"  class="btn btn-danger">Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Form Validation Form -->

                
            </div>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->

    

    <div class="row" id="col">

            <div class="col-sm-10 col-md-12 col-lg-12">
            
                <div class="block">

    
                <div class="block-title">
                    <h2>Active Chef Detail</h2>
                    <button type="button" id="exptexcel" class="btn btn-rounded btn-success" style="float:right">Export</button>
                </div>
           

      
                <table id ="activechefList" class="table table-vcenter table-striped table-hover table-borderless">

                                                <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Chef</th>
                                                 </tr> 
                                                </thead>  
                  
                </table>
            

                </div>

            </div>
    </div>
</div>


 <?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function() {
    $("#loading").hide();
    //$("#cht").hide();
    $("#col").hide();
   // chefdetail();

     function download_csv(csv, filename) {
        var csvFile;
        var downloadLink;
        // CSV FILE
        csvFile = new Blob([csv], {type: "text/csv"});
        // Download link
        downloadLink = document.createElement("a");
        // File name
        downloadLink.download = filename;
        // We have to create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);
        // Make sure that the link is not displayed
        downloadLink.style.display = "none";
        // Add the link to your DOM
        document.body.appendChild(downloadLink);
        // Lanzamos
        downloadLink.click();
    }

    function export_table_to_csv(html, filename) {
        var csv = [];
        var rows = document.querySelectorAll("table tr");
        
        for (var i = 0; i < rows.length; i++) {
            var row = [], cols = rows[i].querySelectorAll("td, th");
            
            for (var j = 0; j < cols.length; j++) 
                row.push(cols[j].innerText);        
            csv.push(row.join(","));       
        }
        // Download CSV
        download_csv(csv.join("\n"), filename);
    }

    document.querySelector("#exptexcel").addEventListener("click", function () {
        var html = document.querySelector("table").outerHTML;
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
        filename = 'MyTree_Active_Chefs' + postfix + '.csv';
        export_table_to_csv(html, filename);
    });    
 


});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

        $('#activechefList').dataTable().fnClearTable();
        $('#activechefList').dataTable().fnDraw();
        $('#activechefList').dataTable().fnDestroy();

       
      });</script>



<script type="text/javascript">

    function activechefs()
    {

        console.log("Hii");

        var frmdate = '';
        var todate = '';

        frmdate = $("#frmdt").val();
        todate = $("#todt").val();

        if(frmdate == '' || todate == '')
        {
            alert('Please select date');

            return false;
        }

        var ic =  $("#invitecode").val();

        
        var reqsu = {"fd":frmdate,"invitecode":ic,"td":todate};        
   

        $.ajax({
                 url: 'activechef_service.php?servicename=ActiveChefs',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(reqsu),
                 async: false,
                 success: function(rsch)
                 {
                     console.log("Json"+JSON.stringify(rsch));

                     var rsuch = JSON.parse(rsch);

                     if(rsuch.status == 'success')
                     {
                        $("#col").show();

                        var co = new Array();
                        

                        for(var ch=0;ch<rsuch.chefs.length;ch++)
                        {

                            co[ch] = new Array();

                            co[ch][0] = ch+1;

                            co[ch][1] = rsuch.chefs[ch].chef;

                            // co[ch][2] = rsuch.chefs[ch].cuisines;

                            // co[ch][3] = rsuch.chefs[ch].lastactivedate;
                     
                        } 

                        $('#activechefList').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true,
                        //"order": [[ 2, "asc" ]]
                        "aoColumns": [{ "orderSequence": [ "desc", "asc" ] },
                            { "orderSequence": [ "desc", "asc" ] }
                        ]
                       });

                        
                            
                     }
                     else
                     {
                       // $("#cht").hide();

                         $("#col").hide();
                     }   
                 }    
        });
    }

    function reset()
    {
       location.reload();
    }

</script>





