<?php

 session_start();

 if(!isset($_SESSION['info']['user']))
 {
 	 // header("Location: http://52.39.93.52/Casseroleadmin/index.php");
 	header("Location: index.php");
 }
 
 
?>



<?php include 'inc/config.php'; 


if ($_SESSION['info']['type'] !='admin' || $_SESSION['info']['type']!='cluster'){
	$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
	$template['neworder'] = '<span id="neworder" class="blink">New Order!</span>';
}
else{
	$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user']; 
	$template['neworder'] = '';
}
?>



<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
			

<?php  if (@$_SESSION['info']['type']=='admin' || $_SESSION['info']['type']=='cluster'){  ?>
	<div class="col-sm-6 col-lg-3">
	
	<input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
	<a href="javascript:void(0)" class="widget">
	<div class="widget-content widget-content-mini text-right clearfix">
	<div class="widget-icon pull-left themed-background">
	<i class="gi gi-user text-light-op"></i>
	</div>
	<h2 class="widget-heading h3">
	<strong>+<span id="cuss" data-to=""></span></strong>
	</h2>
	<span class="text-muted">CUSTOMERS</span>
	</div>
	</a>
	</div>

	<div class="col-sm-6 col-lg-3">
	<a href="javascript:void(0)" class="widget">
	<div class="widget-content widget-content-mini text-right clearfix">
	<div class="widget-icon pull-left themed-background-success">
	<i class="gi gi-kiosk_food text-light-op"></i>
	</div>
	<h2 class="widget-heading h3 text-success">
	<strong>+ <span id="cfss"  data-to=""></span></strong>
	</h2>
	<span class="text-muted">RESTAURANT</span>
	</div>
	</a>
	</div>

	<div class="col-sm-6 col-lg-3">
	<a href="javascript:void(0)" class="widget">
	<div class="widget-content widget-content-mini text-right clearfix">
	<div class="widget-icon pull-left themed-background-warning">
	<i class="gi gi-fast_food text-light-op"></i>
	</div>
	<h2 class="widget-heading h3 text-warning">
	<strong>+ <span  id="dish" data-to=""></span></strong>
	</h2>
	<span class="text-muted">DISHES</span>
	</div>
	</a>
	</div>

	<div class="col-sm-6 col-lg-3">
	<a href="javascript:void(0)" class="widget">
	<div class="widget-content widget-content-mini text-right clearfix">
	<div class="widget-icon pull-left themed-background-danger">
	<i class="gi gi-shopping_cart text-light-op"></i>
	</div>
	<h2 class="widget-heading h3 text-danger">
	<strong>+ <span id="ords" data-to=""></span></strong>
	</h2>
	<span class="text-muted">ORDERS</span>
	</div>
	</a>
	</div>
<?php  }else{  ?>
	<div class="col-sm-6 col-lg-3">
	<input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
	<a href="javascript:void(0)" class="widget">
	<div class="widget-content widget-content-mini text-right clearfix">
	<div class="widget-icon pull-left themed-background">
	<i class="gi gi-user text-light-op"></i>
	</div>
	<h2 class="widget-heading h3">
	<strong>+<span id="cuss" data-to=""></span></strong>
	</h2>
	<span class="text-muted">CUSTOMERS</span>
	</div>
	</a>
	</div>

	<div class="col-sm-6 col-lg-3">
	<a href="javascript:void(0)" class="widget">
	<div class="widget-content widget-content-mini text-right clearfix">
	<div class="widget-icon pull-left themed-background-warning">
	<i class="gi gi-fast_food text-light-op"></i>
	</div>
	<h2 class="widget-heading h3 text-warning">
	<strong>+ <span  id="dish" data-to=""></span></strong>
	</h2>
	<span class="text-muted">DISHES</span>
	</div>
	</a>
	</div>

	<div class="col-sm-6 col-lg-3">
	<a href="javascript:void(0)" class="widget">
	<div class="widget-content widget-content-mini text-right clearfix">
	<div class="widget-icon pull-left themed-background-danger">
	<i class="gi gi-shopping_cart text-light-op"></i>
	</div>
	<h2 class="widget-heading h3 text-danger">
	<strong>+ <span id="ords" data-to=""></span></strong>
	</h2>
	<span class="text-muted">ORDERS</span>
	</div>
	</a>
	</div>
<?php  }  ?>

        
       
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->

  
</div>
<!-- END Page Content -->



<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
	
$(document).ready(function(){
	$("#neworder").hide();
	var invitecode = '';

	invitecode = $("#invitecode").val();

	var reqivc = {"invitecode":invitecode};


	$.ajax({
			 url: 'service.php?servicename=dashboard',
			 type: 'POST',
			 datatype: 'JSON',
			 data: JSON.stringify(reqivc),
			 async: false,
			 success: function(ds)
			 {
			 	console.log("REsp"+JSON.stringify(ds));
			 	var rds = JSON.parse(ds);

			 	if(rds.status == 'success')
			 	{
			 		var u  = rds.users;
			 		console.log("USers"+u);
			 		$("#cuss").attr('data-to',u);
			 		$("#cuss").text(u);

			 		$("#cfss").attr('data-to',rds.chefs);
			 		$("#cfss").text(rds.chefs);

			 		$("#dish").attr('data-to',rds.cuisines);
			 		$("#dish").text(rds.cuisines);

			 		$("#ords").attr('data-to',rds.orders);
			 		$("#ords").text(rds.orders);




			 	}	
			 }	

	})


});

</script>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/readyDashboard.js"></script>
<script>$(function(){ ReadyDashboard.init(); });</script>

<?php include 'inc/template_end.php'; ?>