<?php 

session_start();

//print_r($_SESSION);

if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/Casseroleadmin/index.php");
	header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
// include 'inc/config.php'; $template['header_link'] = 'WELCOME ';
 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Validation Header -->
    <!-- <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Create Invitation Code</h1>
                </div>
            </div>
           
        </div>
    </div> -->

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <div class="block">
                <!-- Form Validation Title -->
                <div class="block-title">
                    <h2>Dormant User</h2>
                </div>
                <!-- END Form Validation Title -->

                <!-- Form Validation Form -->
                
                <!--event_start_time,event_end_time,address,info-->
                <form id="cusordreport" action="customer-order-detail.php"  method="post" class="form-horizontal form-bordered">

                <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">


                    <div class="form-group" id="year">
                        <label class="col-md-3 control-label" for="val-mt">Select Year<span class="text-danger">*</span></label>
                        <div class="col-md-6" >
                           <select id="val-yr" name="val-yr" class="form-control">
                               <option value="">Select Year</option>
                               <option value="2017">2017</option>
                               <option value="2018">2018</option>
                               <option value="2019">2019</option>
                               <option value="2020">2020</option>
                           </select>
                        </div>
                    </div>
                    

                    <div class="form-group" id="selectInvite">
                        <label class="col-md-3 control-label" for="val-mt">Select Month<span class="text-danger">*</span></label>
                        <div class="col-md-6" >
                           <select id="val-mt" name="val-mt" class="form-control">
                               <option value="">Select Month</option>
                               <option value="1">January</option>
                               <option value="2">February</option>
                               <option value="3">March</option>
                               <option value="4">April</option>
                               <option value="5">May</option>
                               <option value="6">June</option>
                               <option value="7">July</option>
                               <option value="8">August</option>
                               <option value="9">September</option>
                               <option value="10">October</option>
                               <option value="11">November</option>
                               <option value="12">December</option>
                            </select>
                        </div>
                    </div>

                                        
                    <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-3">
                            <!-- <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button> -->
                            <button type="button" onclick="searchuser();" class="btn btn-effect-ripple btn-primary">Submit</button>
                            <button type="button" onclick="reset();"  class="btn btn-danger">Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Form Validation Form -->

                
            </div>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->

    

    <div class="row" id="col">

            <div class="col-sm-10 col-md-12 col-lg-12">
            
                <div class="block">

    
                <div class="block-title">
                    <h2>Dormant User Detail</h2>
                    <button type="button" id="exptexcel" class="btn btn-rounded btn-success" style="float:right">Export</button>
                </div>
           

      
                <table id ="tablecList" class="table table-vcenter table-striped table-hover table-borderless">

                                                <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Customer</th>
                                                 <th>No of Orders</th>
                                                 <th>Last Active Date</th>
                                                 </tr> 
                                                </thead>  
                  
                </table>
            

                </div>

            </div>
    </div>
</div>


 <?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function() {
    $("#loading").hide();
    $("#cht").hide();
    $("#col").hide();

    function download_csv(csv, filename) {
        var csvFile;
        var downloadLink;
        // CSV FILE
        csvFile = new Blob([csv], {type: "text/csv"});
        // Download link
        downloadLink = document.createElement("a");
        // File name
        downloadLink.download = filename;
        // We have to create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);
        // Make sure that the link is not displayed
        downloadLink.style.display = "none";
        // Add the link to your DOM
        document.body.appendChild(downloadLink);
        // Lanzamos
        downloadLink.click();
    }

    function export_table_to_csv(html, filename) {
        var csv = [];
        var rows = document.querySelectorAll("table tr");
        
        for (var i = 0; i < rows.length; i++) {
            var row = [], cols = rows[i].querySelectorAll("td, th");
            
            for (var j = 0; j < cols.length; j++) 
                row.push(cols[j].innerText);        
            csv.push(row.join(","));       
        }
        // Download CSV
        download_csv(csv.join("\n"), filename);
    }

    document.querySelector("#exptexcel").addEventListener("click", function () {
        var html = document.querySelector("table").outerHTML;
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
        filename = 'MyTree_Dormant_User' + postfix + '.csv';
        export_table_to_csv(html, filename);
    });

   // chefdetail();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

        $('#tablecList').dataTable().fnClearTable();
        $('#tablecList').dataTable().fnDraw();
        $('#tablecList').dataTable().fnDestroy();

       
      });</script>



<script type="text/javascript">

    function searchuser()
    {

        console.log("Hii");

        var mt = '';

        var yr = '';

        mt = $("#val-mt").val();

        yr = $("#val-yr").val();

        if(mt == '' && yr == '')
        {
            alert('Please select year and month');

            return false;
        }    

        var ic =  $("#invitecode").val();

        
        var reqsu = {"month":mt,"invitecode":ic,"year":yr};        
   

        $.ajax({
                 url: 'dormantaccount_service.php?servicename=Customers',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(reqsu),
                 async: false,
                 success: function(rsch)
                 {
                     console.log("Json"+JSON.stringify(rsch));

                     var rsuch = JSON.parse(rsch);

                     if(rsuch.status == 'success')
                     {
                        $("#col").show();

                        var co = new Array();
                        

                        for(var ch=0;ch<rsuch.users.length;ch++)
                        {

                            co[ch] = new Array();

                            co[ch][0] = ch+1;

                            co[ch][1] = rsuch.users[ch].user;

                            co[ch][2] = rsuch.users[ch].orders;

                            co[ch][3] = rsuch.users[ch].lastorderdate;
                     
                        } 

                        $('#tablecList').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true,
                        //"order": [[ 2, "asc" ]]
                        "aoColumns": [{ "orderSequence": [ "desc", "asc" ] },
                            { "orderSequence": [ "desc", "asc" ] },
                            { "orderSequence": [ "desc", "asc" ] },
                            { "orderSequence": [ "desc", "asc" ] }
                        ]
                       });

                        
                            
                     }
                     else
                     {
                        $("#cht").hide();

                         $("#col").hide();
                     }   
                 }    
        });
    }

    function reset()
    {
       location.reload();
    }

</script>





