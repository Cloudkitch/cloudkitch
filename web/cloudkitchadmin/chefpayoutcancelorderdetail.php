
<?php //include 'inc/config.php'; $template['header_link'] = 'FORMS'; 

if(!isset($_POST['itmids'])){

	header('Location: CancelOrderPayout.php');
}

session_start();
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: index.php");
}



 include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user']; 
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">

  <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Chef Payout Detail</h1>
                </div>
            </div>
           
        </div>
    </div>

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />

              
        </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <div class="block">
<div class="block-title">
<h2>Chef Payout Detail</h2>
</div>
<form id="validation-wizard" action="" method="post" class="form-horizontal form-bordered ui-formwizard" novalidate="novalidate">

			  <input type="hidden" name="itmids" id="itmids" value="<?php echo $_POST['itmids'];?>">

              <input type="hidden" name="mtpv" id="mtpv" value="<?php echo $_POST['mtpv'];?>">

              <input type="hidden" name="olpv" id="olpv" value="<?php echo $_POST['olpv'];?>">

              <input type="hidden" name="cfid" id="cfid" value="<?php echo $_POST['cfid'];?>">

              <!-- <input type="hidden" name="cuid" id="cuid" value="<?php echo $_POST['cuid'];?>"> -->


              <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">

<div id="validation-first" class="step ui-formwizard-content" style="display: block;">
<div class="form-group">
<div class="col-xs-12">
<ul class="nav nav-pills nav-justified">
<li class="active disabled"><a href="javascript:void(0)" class="text-muted"> <i class="fa fa-user"></i> <strong>Step1</strong></a></li>
<li class="disabled"><a href="javascript:void(0)"><i class="fa fa-info-circle"></i> <strong>Step2</strong></a></li>
</ul>
</div>
</div>

<div class="row">
	<div class="col-sm-6" id="mtpbox">
		<div class="widget">
			<div class="widget-content themed-background-dark text-light-op text-center">
				<i class="fa fa-money"></i> <strong>MTP</strong>
			</div>
			<div class="widget-content themed-background-muted text-center">
				<i class="fa fa-rupee fa-3x text-success"></i><h2 class="widget-heading text-dark" id="mtprs"></h2>
			</div>
			<div class="widget-content">
				<div class="row text-center">
					<div class="col-xs-6">
						<div class="form-group">
							<div class="radio">
								<label for="example-radio1">
								<input type="radio" id="cash" name="radmtp" value="CASH"> Would you like to take cash
								</label>
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="form-group">
							<div class="radio">
								<label for="example-radio1">
								<input type="radio" id="cash" name="radmtp" value="MTP"> Would you like to take mtp
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6" id="olpbox">
		<div class="widget">
			<div class="widget-content themed-background-dark text-light-op text-center">
				<i class="fa fa-money"></i> <strong>OLP</strong>
			</div>
			<div class="widget-content themed-background-muted text-center">
				<i class="fa fa-rupee fa-3x text-success"></i><h2 class="widget-heading text-dark" id="olprs"></h2>
			</div>
			<div class="widget-content">
				<div class="row text-center">
					<div class="col-xs-6">
						<div class="form-group">
							<div class="radio">
								<label for="example-radio1">
								<input type="radio" id="cash" name="radolp" value="CASH"> Would you like to take cash
								</label>
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="form-group">
							<div class="radio">
								<label for="example-radio1">
								<input type="radio" id="cash" name="radolp" value="MTP"> Would you like to take mtp
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <div class="form-group">
<label class="col-md-4 control-label">MTP</label>
<div class="col-md-6">
<p class="form-control-static">This is static text</p>
</div>
</div>

<div class="form-group">
<div class="col-md-4">
</div>
<div class="col-md-6">
<div class="radio">
<label for="example-radio1">
<input type="radio" id="cash" name="example-radios" value="CASH"> Would you like to use cash
</label>
</div>
<div class="radio">
<label for="example-radio2">
<input type="radio" id="mtp" name="example-radios" value="MTP"> Would you like to use MTP
</label>
</div>
</div>
</div>

<div class="form-group">
<label class="col-md-4 control-label">OLP</label>
<div class="col-md-6">
<p class="form-control-static">This is static text</p>
</div>
</div>

<div class="form-group">
<div class="col-md-4">
</div>
<div class="col-md-6">
<div class="radio">
<label for="example-radio1">
<input type="radio" id="cash" name="example-radios" value="CASH"> Would you like to use cash
</label>
</div>
<div class="radio">
<label for="example-radio2">
<input type="radio" id="mtp" name="example-radios" value="MTP"> Would you like to use MTP
</label>
</div>
</div>
</div> -->





</div>
<div id="validation-second" class="step ui-formwizard-content" style="display: none;">
<div class="form-group">
<div class="col-xs-12">
<ul class="nav nav-pills nav-justified">
<li class="disabled"><a href="javascript:void(0)" class="text-muted"><del><i class="fa fa-user"></i> <strong>Step1</strong></del></a></li>
<li class="active disabled"><a href="javascript:void(0)"><i class="fa fa-info-circle"></i> <strong>Step2</strong></a></li>
</ul>
</div>
</div>
<!-- <div class="form-group">
<label class="col-md-4 control-label" for="example-validation-suggestions">Suggestions</label>
<div class="col-md-8">
<textarea id="example-validation-suggestions" name="example-validation-suggestions" rows="5" class="form-control ui-wizard-content" placeholder="Share your ideas with us.." disabled="disabled"></textarea>
</div>
</div> -->
<!-- <div class="form-group">
<label class="col-md-4 control-label"><a href="#modal-terms" data-toggle="modal">Terms</a> <span class="text-danger">*</span></label>
<div class="col-md-6">
<label class="switch switch-primary" for="example-validation-terms">
<input type="checkbox" id="example-validation-terms" name="example-validation-terms" value="1" disabled="disabled" class="ui-wizard-content">
<span data-toggle="tooltip" title="" data-original-title="I agree to the terms!"></span>
</label>
</div>
</div> -->
<div class="col-sm-12">
<div class="widget">
<div class="widget-image widget-image-xs">
<div class="widget-image-content">
<h2 class="widget-heading text-light"><strong>CHEF PAYOUT</strong></h2>
<!-- <h3 class="widget-heading text-light-op h4">Favorite games you have to play</h3> -->
</div>
<i class="fa fa-money"></i>
</div>
<div class="widget-content widget-content-full">
<table class="table table-striped table-borderless remove-margin">
<tbody>
<tr id="olptr">
<td><a href="javascript:void(0)" class="text-black">OLP</a></td>
<td><a href="javascript:void(0)" class="text-black" id="olppt"></a></td>
<td class="text-center" style="width: 80px;"><span class="text-muted" id="olppv"></span></td>
</tr>
<tr id="mtptr">
<td><a href="javascript:void(0)" class="text-black">MTP</a></td>
<td><a href="javascript:void(0)" class="text-black" id="mtppt"></a></td>
<td class="text-center" style="width: 80px;"><span class="text-muted" id="mtppv"></span></td>
</tr>
<tr>
<td><a href="javascript:void(0)" class="text-black"><b>Total</b></a></td>
<td><a href="javascript:void(0)" class="text-black"></a></td>
<td class="text-center" style="width: 80px;"><b><span class="text-muted" id="tot"></span></b></td>
</tr>


</tbody>
</table>

<button  type="button" onclick="paynow();" class="btn btn-effect-ripple btn-primary" style="overflow: hidden; position: relative;margin-left: 530px;">Paynow</button>


</div>
</div>
</div>
</div>
<div class="form-group form-actions">
<div class="col-md-8 col-md-offset-4">
<input type="reset" class="btn btn-sm btn-warning ui-wizard-content ui-formwizard-button" id="back3" value="Back" disabled="disabled">
<input type="submit" onclick="validate(this.value);" class="btn btn-sm btn-primary ui-wizard-content ui-formwizard-button" id="next3" value="Next">
</div>
</div>
</form>
</div>
            <!-- END Form Validation Block -->
        </div>
    </div>

    

    <!-- Cuisine Details -->

    


    <!-- END Form Validation Content -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script type="text/javascript" src="js/CasseroleService.js"></script>
<script src="js/pages/formsWizard.js"></script>
<script>$(function(){ FormsWizard.init(); });</script>

<script type="text/javascript">
$(document).ready(function() {
    $("#loading").hide();
    $("#olpbox").hide();
    $("#mtpbox").hide();
    $("#olptr").hide();
    $("#mtptr").hide();

    var itmid = '';

    itmid = $("#itmids").val();

    var mtp = '';

    mtp = $("#mtpv").val();

    var olp = '';

    olp = $("#olpv").val();

    var cefid = '';

    cefid = $("#cfid").val();

    // var cid = '';

    // cid = $("#cuid").val();  

    var olptyp = '';

    var mtptyp = '';

    // console.log("Itmid :"+itmid+"Mtp :"+mtp+"Olp :"+olp+"Chefid :"+cefid+" Cuid  :"+cid);

    console.log("Itmid :"+itmid+"Mtp :"+mtp+"Olp :"+olp+"Chefid :"+cefid);

    if(mtp != 0  && olp != 0)
    {
    	$("#olpbox").show();

    	$("#mtpbox").show();

    	$("#mtprs").text(mtp);

    	$("#olprs").text(olp);

    	olptyp = $("input[name='radolp']:checked").val();

    	mtptyp = $("input[name='radmtp']:checked").val();

    	//console.log("MTP Payment Type"+mtptyp);

    	//console.log("Olp Payment Type"+olptyp);
    }
    else if(mtp != 0)
    {
    	$("#olpbox").hide();

    	$("#mtpbox").show();

    	$("#mtpbox").removeClass("col-sm-6");

    	$("#mtpbox").addClass("col-sm-12");

    	$("#mtprs").text(mtp);

    	//mtptyp = $("input[name='radmtp']:checked").val();

    	//console.log("MTP Payment Type"+mtptyp);

    }
    else if(olp != 0)
    {
    	$("#olpbox").show();

	   	$("#mtpbox").hide();	

	   	$("#olpbox").removeClass("col-sm-6");

    	$("#olpbox").addClass("col-sm-12");

    	$("#olprs").text(olp);

    	//olptyp = $("input[name='radolp']:checked").val();

    	//console.log("Olp Payment Type"+olptyp);
    }	







});
</script>

<script type="text/javascript">
	
	function validate(btn)
	{

		if(btn == 'Next')
		{
			var sbtbtntxt = $('#next3').val();
			if(sbtbtntxt=='Submit')
			{
				$('#next3').hide();
			} else {
				$('#next3').show();
			}

			console.log("Entered Over Here"+btn);

			var itmid = '';	

			itmid = $("#itmids").val();

		    var mtp = '';

		    mtp = $("#mtpv").val();

		    var olp = '';

		    olp = $("#olpv").val();

		    var cefid = '';

		    cefid = $("#cfid").val();

		    // var cid = '';

		    // cid = $("#cuid").val();  

		    var olptyp = '';

		    var mtptyp = '';

		    var fmtp = '';

		    var folp = '';

		    var tot = '';

		    // console.log("Itmid :"+itmid+"Mtp :"+mtp+"Olp :"+olp+"Chefid :"+cefid+" Cuid  :"+cid);

		    console.log("Itmid :"+itmid+"Mtp :"+mtp+"Olp :"+olp+"Chefid :"+cefid);

		    if(mtp != 0  && olp != 0)
		    {
		    	

		    	 // if(!$("input[name='radolp']:checked").val() && !$("input[name='radmtp']:checked").val())
			     // {
			     //     alert("Please choose any one option");
			     //     return false;
			     // }
			     // else
			     // {
			     	olptyp = $("input[name='radolp']:checked").val();

		    		mtptyp = $("input[name='radmtp']:checked").val();

		    		console.log("MTP Payment Type"+mtptyp);

			    	console.log("Olp Payment Type"+olptyp);

			    	if(mtptyp == 'CASH')
			    	{
			    		// fmtp = (70 * mtp) / 100;
			    		fmtp =  mtp;
			    	}
			    	else if(mtptyp == 'MTP')
			    	{
			    		fmtp = mtp;
			    	}

			    	tot = parseFloat(fmtp) + parseFloat(olp);

			    	$("#olptr").show();

			    	$("#mtptr").show();

			    	$("#olppv").text(olp);

			    	$("#mtppv").text(fmtp);

			    	$("#olppt").text(olptyp);

			    	$("#mtppt").text(mtptyp);

			    	$("#tot").text(tot);
			    	
			     //}	

		    	
		   	}
		   	else if(mtp != 0)
		    {
		    	// if(!$("input[name='radmtp']:checked").val())
			    // {
			    //  	alert("Please choose any one option");
			    //      return false;
			    // }
			    // else
			    // {
			    	mtptyp = $("input[name='radmtp']:checked").val();

		    		console.log("MTP Payment Type"+mtptyp);

		    		$("#mtptr").show();	

		    		$("#mtppt").text(mtptyp);

		    		if(mtptyp == 'CASH')
			    	{
			    		// fmtp = (70 * mtp) / 100;
			    		fmtp = mtp;
			    	}
			    	else if(mtptyp == 'MTP')
			    	{
			    		fmtp = mtp;
			    	}

			    	$("#tot").text(fmtp);

			    	$("#mtppv").text(fmtp);
			    //}	
			}
		    else if(olp != 0)
		    {
		    	// if(!$("input[name='radolp']:checked").val())
			    // {
			    // 	alert("Please choose any one option");
			    //      return false;
			    // }
			    // else
			    // {
			    	olptyp = $("input[name='radolp']:checked").val();

		    		console.log("Olp Payment Type"+olptyp);

		    		$("#olptr").show();	

		    		$("#olppv").text(olp);

		    		$("#olppt").text(olptyp);

		    		$("#tot").text(olp);


			    //}	

		    	
		    }

		   // $("#next3").hide();
		    	
		}

		if(btn == 'Submit')
		{
			$("#next3").hide();
		}	

		

    


	}

</script>

<script type="text/javascript">

function redirect()
{
	window.location.href='CancelOrderPayout.php';
}
	
	function paynow()
	{
		console.log("hii");

		var mtppm = '';

		var olppm = '';

		var itmary = '';

		var mtpvl = '';

		var olpvl = '';

		itmary = $("#itmids").val();

		var chefid = '';

		chefid = $("#cfid").val();

		var cuid = '';

		cuid = $("#cuid").val();

		var ic = '';

		ic = $("#invitecode").val();

		if($("#mtppt").text() != '')
		{
			mtppm = $("#mtppt").text();	

			mtpvl = $("#mtppv").text();
		}

		if($("#olppt").text() != '')
		{
			olppm = $("#olppt").text();	

			olpvl = $("#olppv").text();
		}	



		console.log("Mtp paymode  :"+mtppm+  "Olp paymode :"+olppm+"Items :"+itmary+"Chefid :"+chefid+"Mtp Rs :"+mtpvl+ "Olp Rs :"+olpvl);

		var reqchefp = {"mtppm":mtppm,"olppm":olppm,"itmary":itmary,"chefid":chefid,"mtp":mtpvl,"olp":olpvl,"invitecode":ic};

		console.log("Req Paynow"+JSON.stringify(reqchefp));

		$.ajax({
                url: 'chefpayout-cancelorders-service.php?servicename=Cancel-Order-Payout',
                type: 'POST',
                datatype: 'JSON',
                data: JSON.stringify(reqchefp),
                async: false,
                success: function(rsfp)
                {
                   console.log("Response"+JSON.stringify(rsfp));

                   var rsp = JSON.parse(rsfp);

                   if(rsp.status == 'success')
                   {
                   		var n = noty({
                     layout: 'bottomRight',
                     text: 'Paid to Chef Successfully',
                     theme:'relax',type: 'success',
                     timeout : '3000',
                     animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // document.getElementById("divcreateexp").style.display="none" ;
                                //document.getElementById("confirmView").style.display="block" ;
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });

                    setTimeout(redirect,3000);
                   



                   	 //window.location.href = 'ChefPayout.php';

                      // var cfn = '';
                      // var cun =  '';
                      // var cfp = '';
                      // var t = '';
                      // var d = '';

                      // cfn = rsp.chefname;
                      // cun = rsp.cuisinename;
                      // cfp = rsp.chefpic;
                      // t = rsp.total;
                      // d = rsp.date;

                      // $("#cfimg").attr('src',cfp);

                      // $("#cfname").text(cfn);

                      // $("#cuname").text(cun);

                      // $("#netp").text(t);

                      // $("#dt").text(d);





                      //  $('#rcptModal').modal('show');
                   }
                   else
                   {
                   	  var n = noty({
                         layout: 'bottomRight',
                         text: 'Failed to paid',
                         theme:'relax',type: 'error',
                         timeout : '3000',
                        animation: {
                            open: 'animated bounceInLeft', // Animate.css class names
                            close: 'animated bounceOutLeft', // Animate.css class names
                            easing: 'swing', // unavailable - no need
                            speed: 500 // unavailable - no need
                        },
                        callback: { 
                            onClose: function() {
                                
                               // wizardshowhide("clickable-second");
                            } 
                        },
                        });
                   } 
                }

      });
	}

</script>



<?php include 'inc/template_end.php'; ?>