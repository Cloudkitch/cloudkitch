<?php
/**
 * page_header.php
 *
 * Author: pixelcave
 *
 * The header of each page
 *
 */
// if(!isset($_SESSION)) 
//     { 
//         session_start(); 
//     }
?>
<!-- Header -->
<!-- In the PHP version you can set the following options from inc/config file -->
<!--
    Available header.navbar classes:

    'navbar-default'            for the default light header
    'navbar-inverse'            for an alternative dark header

    'navbar-fixed-top'          for a top fixed header (fixed main sidebar with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
        'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added

    'navbar-fixed-bottom'       for a bottom fixed header (fixed main sidebar with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
        'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
-->
<header class="navbar<?php if ($template['header_navbar']) { echo ' ' . $template['header_navbar']; } ?><?php if ($template['header']) { echo ' '. $template['header']; } ?>">
    <!-- Left Header Navigation -->
    <ul class="nav navbar-nav-custom">
        <!-- Main Sidebar Toggle Button -->
        <li>
            <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');">
                <i class="fa fa-ellipsis-v fa-fw animation-fadeInRight" id="sidebar-toggle-mini"></i>
                <i class="fa fa-bars fa-fw animation-fadeInRight" id="sidebar-toggle-full"></i>
            </a>
        </li>
        <!-- END Main Sidebar Toggle Button -->

        <?php if ($template['header_link']) { ?>
        <!-- Header Link -->
        <li class="hidden-xs animation-fadeInQuick">
            <a href=""><strong><?php echo $template['header_link']; ?></strong></a>
        </li>
        
        <!-- END Header Link -->
        <?php } ?>
    </ul>
    <!-- END Left Header Navigation -->

    <ul class="nav navbar-nav-custom pull-center">
        <li style="font-weight: bold;
       color: white;
    margin-left: 205px;
    font-size: 19px;padding-top: 10px;">
           
                        
                        <?php echo $_SESSION['info']['chaptername'];?>
                  
        </li>
    
    </ul>

    <!-- Right Header Navigation -->
    <ul class="nav navbar-nav-custom pull-right">
        <!-- Search Form -->
        <!-- By DHruvpalsinh on 19-07-2016 -->
        <!-- <li>
            <form action="page_ready_search_results.php" method="post" class="navbar-form-custom" role="search">
                <input type="text" id="top-search" name="top-search" class="form-control" placeholder="Search..">
            </form>
        </li> -->
        <!-- END Search Form -->

        <!-- Alternative Sidebar Toggle Button -->
        <!-- By Dhruvpalsinh on 19-07-2016 -->
        <!-- <li>
            <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar-alt');">
                <i class="gi gi-settings"></i>
            </a>
        </li> -->
        <!-- END Alternative Sidebar Toggle Button -->

        <!-- User Dropdown -->
        <li onclick="myFunction();">
            <a  href="javascript:void(0)" >
                <i class="fa fa-fw pull-right"></i> 
                Log out
             </a> 
        </li>
        <!-- END User Dropdown -->
    </ul>
    <!-- END Right Header Navigation -->
</header>
<script type="text/javascript">

function myFunction() {
    swal({
                title: "Are You sure want to Logout from Cloudkitch?",
                //text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                    window.location.href = 'logout.php';

                }
            });
    // var txt;
    // var r = confirm("Are You sure want to Logout from Cloudkitch?");
    // if (r == true) {
        
    // } else {
    //     txt = "You pressed Cancel!";
    // }
    //document.getElementById("demo").innerHTML = txt;
}

</script>
