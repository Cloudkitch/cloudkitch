<?php
/**
 * template_scripts.php
 *
 * Author: pixelcave
 *
 * All vital JS scripts are included here
 *
 */
?>

<!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-2.1.1.min.js"%3E%3C/script%3E'));</script>

<!-- Bootstrap.js, Jquery plugins and Custom JS code -->
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/app.js"></script>

<link href="js/animate.css" rel="stylesheet"/>
<script type="text/javascript" src="js/jquery.noty.packaged.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="js/bootstrap-tagsinput.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>

<script>
    // if("<?php //echo $_SESSION['info']['type'] ?>" == 'kitchenmember' ){
    //  //   alert("hiu");
    //     if("<?php //echo $_SESSION['info']['role'] ?>" == 2){

    //     }

    // }

        $("body").append('<input type="hidden" id="role_data" value="<?= $role ?>" name="role" class="form-control" placeholder="">');
        var role = $("#role_data").val();
    
    // Code to flash new order if any new order is placed
	setInterval(function(){
        // if("?php echo $_SESSION['info']['type'] ?>" == 'kitchen' || "?php echo $_SESSION['info']['type'] ?>" == 'kitchenmember'){
        if("<?php echo $_SESSION['info']['type'] ?>" == 'cluster' || "<?php echo $_SESSION['info']['type'] ?>" == 'kitchenmember'){
            
                $.ajax({
                url: 'service.php?servicename=isneworder',
                type: 'POST',
                success: function(data)
                {
                    var result = JSON.parse(data);
                    var status = result.status;
                    if(status == "success"){
                        //$("#neworder").show();
						$("#toast-success").html("New Order!");
                        $("#toaster").fadeIn();
                    }
                    else{
                        $("#neworder").hide();
                        return;
                    }

					setTimeout(function(){
                         $("#toaster").fadeOut();
                        $("#toasterError").fadeOut();
                  }, 2000);


                }
            
                });
            }

        }, 1000); 

</script>
