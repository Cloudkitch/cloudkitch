<?php
// error_reporting(0);
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if(isset($_SESSION['info']['type'])){
    if($_SESSION['info']['type'] == 'kitchenmember'){
        $userid = $_SESSION['info']['kitchenId'];
        $role  = $_SESSION['info']['role'];
    }
    else{
        $userid = $_SESSION['info']['id'];   
        $role  = 0;
    }
}

/**
 * config.php
 *
 * Author: pixelcave
 *
 * Configuration file. It contains variables used in the template as well as the primary navigation array from which the navigation is created
 *
 */
    if(!isset($_SESSION)) 
    { 
        session_start(); 
    }
//session_start();
/* Template variables */
$template = array(
    'name'              => 'Cloudkitch',
    'baseurl'           => 'http://cloudkitch.co.in/cloudkitchadmin/', //live
    // 'baseurl'           => 'https://localhost/cloudkitch/web/cloudkitchadmin/', //local
    // 'baseurl'           => 'http://52.66.213.167/cloudkitch/web/cloudkitchadmin/', // UAT
    'version'           => '1.0',
    'author'            => 'pixelcave',
    'robots'            => 'noindex, nofollow',
    'title'             => 'Cloudkitch',
    'description'       => '',
    // true                         enable page preloader
    // false                        disable page preloader
    'page_preloader'    => true,
    // 'navbar-default'             for a light header
    // 'navbar-inverse'             for a dark header
    'header_navbar'     => 'navbar-inverse',
    // ''                           empty for a static header/main sidebar
    // 'navbar-fixed-top'           for a top fixed header/sidebars
    // 'navbar-fixed-bottom'        for a bottom fixed header/sidebars
    'header'            => 'navbar-fixed-top',
    // ''                           empty for the default full width layout
    // 'fixed-width'                for a fixed width layout (can only be used with a static header/main sidebar)
    'layout'            => '',
    // 'sidebar-visible-lg-mini'    main sidebar condensed - Mini Navigation (> 991px)
    // 'sidebar-visible-lg-full'    main sidebar full - Full Navigation (> 991px)
    // 'sidebar-alt-visible-lg'     alternative sidebar visible by default (> 991px) (You can add it along with another class - leaving a space between)
    // 'sidebar-light'              for a light main sidebar (You can add it along with another class - leaving a space between)
    'sidebar'           => 'sidebar-visible-lg-full',
    // ''                           Disable cookies (best for setting an active color theme from the next variable)
    // 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
    'cookies'           => '',
    // '', 'classy', 'social', 'flat', 'amethyst', 'creme', 'passion'
    'theme'             => 'amethyst',
    // Used as the text for the header link - You can set a value in each page if you like to enable it in the header
    'header_link'       => '',
    // The name of the files in the inc/ folder to be included in page_head.php - Can be changed per page if you
    // would like to have a different file included (eg a different alternative sidebar)
    'inc_sidebar'       => 'page_sidebar',
    'inc_sidebar_alt'   => 'page_sidebar_alt',
    'inc_header'        => 'page_header',
    // The following variable is used for setting the active link in the sidebar menu
    'active_page'       => basename($_SERVER['PHP_SELF'])
);

if (@$_SESSION['info']['type']=='admin') {
    $primary_nav = array(
        array(
            'name'  => 'Dashboard',
            'url'   => 'Dashboard.php',
            'icon'  => 'gi gi-compass'
        ),
        array(
           'name'  => 'Admin',
           'icon'  => 'fa fa-user',
           'url'   => 'adminlist.php',
       ),
        array(
           'name'  => 'Cuisines Types',
           'icon'  => 'fa fa-cutlery',
           'url'   => 'cuisinetypelist.php',
       ),
       array(
        'name'  => 'Category Types',
        'icon'  => 'fa fa-cutlery',
        'url'   => 'categorytypelist.php',
        ),
        array(
           'name'  => 'Corporate',
           'icon'  => 'fa fa-users',
           'sub'   => array(
             array(
                 'name'  => 'Corporate',
                 'url'   => 'corporate.php',
             ),
            //  array(
            //     'name'  => 'Corporate User',
            //     'url'   => 'corporateuser.php',
            // ),
            array(
                'name'  => 'Corporate Offers',
                'url'   => 'corporatoffers.php',
            ),
            array(
                'name'  => 'Corporate Meal Type',
                'url'   => 'corporatMealType.php',
            ),
           
         )
       ),
       array(
        'name'  => 'Add cloudkitch Points',  
        'icon'  => 'fa fa-usd',                             
        'url'   => 'assignpoint.php',                              
    ),
    array(
        'name'  => 'Enquiry List',  
        'icon'  => 'fa fa-usd',                             
        'url'   => 'enquirylist.php',                              
    ),
    array(
        'name'  => 'Get In Touch',  
        'icon'  => 'fa fa-usd',                             
        'url'   => 'getintouchlist.php',                              
    ),
    array(
        'name'  => 'Banners',  
        'icon'  => 'fa fa-image',                             
        'url'   => 'banners.php',                              
    ),
    array(
        'name'  => 'Promote',  
        'icon'  => 'fa fa-image',                             
        'url'   => 'promotelist.php',                              
    ),
    array(
        'name'  => 'Coupon',
        'url'   => 'couponlist.php',
        'icon'  => 'gi gi-user'
    ),
    array(
        'name'  => 'Course Sequence',  
        'icon'  => 'fa fa-image',                             
        'url'   => 'restro_category_sequence.php',                              
    ),
    array(
        'name'  => 'Subscription Offer',  
        'icon'  => 'fa fa-image',                             
        'url'   => 'subscriptionofferlist.php',                              
    ),
   
    );
}
else if (@$_SESSION['info']['type']=='cluster') {
    $primary_nav = array(
        array(
            'name'  => 'Dashboard',
            'url'   => 'Dashboard.php',
            'icon'  => 'gi gi-compass'
        ),
        array(
           'name'  => 'Restaurant',
           'url'   => 'kitchenlist.php',
           'icon'  => 'gi gi-dining-set'
       ),
       array(
        'name'  => 'Orders',
        'url'   => 'clusterwiseorders.php',
        'icon'  => 'gi gi-dining-set'
    ),
    );
}
else
{
    $primary_nav = array(
        array(
            'name'  => 'Dashboard',
            'url'   => 'Dashboard.php',
            'icon'  => 'gi gi-compass'
        ),
        array(
            'url'   => 'separator',
        ),     
        array(
           'name'  => 'Cuisines',
           'icon'  => 'fa fa-cutlery',
           'sub'   => array(
            array(
                'name'  => 'Cuisines',
                'url'   => 'cuisinelist.php',                                
            ),
        )
       ),
    // array(
    //      'name'  => 'Coupons',
    //      'url'   => 'couponcodelist.php',
    //      'icon'  => 'gi gi-credit_card'
    // ),
    array(
        'name'  => 'Member',
        'url'   => 'memberlist.php',
        'icon'  => 'gi gi-user'
    ),
        array(
         'name'  => 'Orders',
           'icon'  => 'gi gi-shopping_bag',
           'sub'   => array(
               array(
                   'name'  => 'Orders',
                   'url'   => 'chefwiseorders.php',
               ),

           )
       ),
       array(
        'name'  => 'Team Meals',
        'icon'  => 'fa fa-cutlery',
        'sub'   => array(
         array(
             'name'  => 'Team Meals',
             'url'   => 'corporatemeallist.php',                                
         ),
     )
    ),
    array(
        'name'  => 'Event Meals',
        'icon'  => 'fa fa-cutlery',
        'sub'   => array(
         array(
             'name'  => 'Event Meals',
             'url'   => 'eventmeallist.php',                                
         ),
     )
    ),
    array(
        'name'  => 'Party Meals',
        'icon'  => 'fa fa-cutlery',
        'sub'   => array(
         array(
             'name'  => 'Party Meals',
             'url'   => 'partymeallist.php',                                
         ),
     )
    ),
    array(
        'name'  => 'Cafeteria',
        'icon'  => 'fa fa-cutlery',
        'sub'   => array(
         array(
             'name'  => 'Cafeteria',
             'url'   => 'cafeterialist.php',                                
         ),
     )
    ),
    array(
        'name'  => 'Offer',
        'icon'  => 'fa fa-cog',
        'sub'   => array(
         array(
             'name'  => 'Offer',
             'url'   => 'offerlist.php',                              
         ),
     )
    ),
    
    array(
        'name'  => 'Corporate',
        'icon'  => 'fa fa-users',
        'sub'   => array(
          array(
              'name'  => 'Corporate',
              'url'   => 'corporatelist.php',
          ),
      )
    ),
    array(
        'name'  => 'Recommended',
        'icon'  => 'fa fa-cutlery',
        'sub'   => array(
         array(
             'name'  => 'Recommended',
             'url'   => 'recommendedlist.php',                              
         ),
     )
    ),
    array(
        'name'  => 'Feedback',
        'icon'  => 'fa fa-users',
        'sub'   => array(
          array(
              'name'  => 'Feedback',
              'url'   => 'feedbacklist.php',
          ),
      )
    ),
    array(
        'name'  => 'Setting',
        'icon'  => 'fa fa-cog',
        'sub'   => array(
         array(
             'name'  => 'Meal Type',
             'url'   => 'mealtypepermission.php',                              
         ),
     )
    ),

    // array(
    //      'name'  => 'Vendor',
    //      'icon'  => 'gi gi-shopping_bag',
    //      'sub'   => array(
    //                          array(
    //                              'name'  => 'Vendor List',
    //                              'url'   => 'vendorlist.php',
    //                          ),
    //                          array(
    //                              'name'  => 'Item List',
    //                              'url'   => 'itemlist.php',
    //                          ),
    //                          array(
    //                              'name'  => 'Vendor Order',
    //                              'url'   => 'vendorwiseorder.php',
    //                          ),
    //                          array(
    //                              'name'  => 'Vendor Approval',
    //                              'url'   => 'vendoronboard.php',
    //                          ),
    //                          array(
    //                              'name'  => 'Vendor Payout',
    //                              'url'   => 'vendorpayout.php',
    //                          ),
    //                           array(
    //                              'name'  => 'Vendor Payout History',
    //                              'url'   => 'vendorpayouthistory.php',
    //                          ),
    //                         array(
    //                              'name'  => 'Cancel Order Report',
    //                              'url'   => 'itemcancelorder.php',
    //                          ),
    //                           array(
    //                              'name'  => 'Vendor Payment',
    //                              'url'   => 'VendorTotalPayout.php',
    //                          ),
    //                           array(
    //                              'name'  => 'Vendor Reported Order',
    //                              'url'   => 'vendorreportedorder.php',
    //                          ),
    //                           array(
    //                              'name'  => 'Vendor Reported Order History',
    //                              'url'   => 'vendorreportedorders-history.php',
    //                          ),

    //                     )
    // ),
    // array(
    //     'name'  => 'Chefincome',
    //     'url'   => 'chefpayment.php',
    //     'icon'  => 'gi gi-money'
    // ), 
    // array(
    //      'name'  => 'Finance',
    //      'icon'  => 'gi gi-money',
    //      'sub'   => array(
    //                          array(
    //                              'name'  => 'Chef Payout',
    //                              'url'   => 'ChefPayout.php',
    //                          ),
    //                          array(
    //                              'name'  => 'Chef Payout History',
    //                              'url'   => 'ChefPayoutHistory.php',
    //                          ),
    //                          array(
    //                              'name'  => 'Chef Payment',
    //                              'url'   => 'ChefTotalPayout.php',
    //                          ),
    //                           array(
    //                              'name'  => 'Cancel Order Report',
    //                              'url'   => 'cuisinecancelorder.php',
    //                          ),                        
    //                           array(
    //                             'name'  => 'Cancel Order Payout',
    //                             'url'   => 'CancelOrderPayout.php'                              
    //                                 ),
    //                           array(
    //                             'name'  => 'Cancel Orders Due',
    //                             'url'   => 'CancelOrders.php'
    //                                 ),
    //                          array(
    //                             'name'  => 'Reported Orders',
    //                             'url'   => 'reportedorders.php'
    //                                 ),
    //                          array(
    //                             'name'  => 'Reported Order History',
    //                             'url'   => 'reportedorders-history.php'
    //                                 ),    
    //                          array(
    //                             'name'  => 'Cancel Order Pay History',
    //                             'url'   => 'chefcancelopayouthistory.php'
    //                                 ),

    //                     )
    // ),
    // array(
    //      'name'  => 'Report',
    //      'icon'  => 'hi hi-tasks',
    //      'sub'   => array(
    //                         array(
    //                             'name'  => 'Chef Report',
    //                             'url'   => 'chef_report.php'
    //                         ),
    //                         array(
    //                             'name'  => 'Customer Report',
    //                             'url'   => 'customer_report.php'
    //                             ),
    //                         array(
    //                             'name'  => 'Chef Activity Report',
    //                             'url'   => 'chef-report-detail.php'
    //                             ),
    //                         array(
    //                             'name'  => 'Customer Activity Report',
    //                             'url'   => 'customer-order-detail.php'
    //                             ),   
    //                         array(
    //                             'name'  => 'Dormant Chef',
    //                             'url'   => 'dormantchef.php',
    //                                 ),
    //                         array(
    //                             'name'  => 'Dormant User',
    //                             'url'   => 'dormantuser.php'
    //                                 ),
    //                         array(
    //                             'name'  => 'Weekly Total Orders',
    //                             'url'   => 'weeklytotalorders.php'
    //                                 ),
    //                         array(
    //                             'name'  => 'Active Chefs',
    //                             'url'   => 'activechefs.php'
    //                                 ),
    //                         array(
    //                             'name'  => 'Active Users',
    //                             'url'   => 'activeuser.php'
    //                                 ),
    //                         array(
    //                             'name'  => 'Paymentwise Order Report',
    //                             'url'   => 'paymenttypewiseorders.php'
    //                                 ),
    //                         array(
    //                             'name'  => 'Categorywise Orders',
    //                             'url'   => 'categorywiseorders.php'
    //                                 ),
    //                         array(
    //                             'name'  => 'Top Chefs',
    //                             'url'   => 'topcheforders.php'
    //                                 ),
    //                         array(
    //                             'name'  => 'Averagewise Orders',
    //                             'url'   => 'weekdayvsweekendorders.php'
    //                                 ),
    //                         array(
    //                             'name'  => 'Customerwise Orders',
    //                             'url'   => 'weekly.php'
    //                                 ),
    //                     )
    //  ),
    // array(
    //     'name'  => 'Send Notification',
    //     'url'   => 'sendnotification.php',
    //     'icon'  => 'gi gi-message_new'
    //     ),
    // array(
    //     'name'  => 'Advertisement',
    //     'url'   => 'advertise.php',
    //     'icon'  => 'gi gi-imac'
    //         ), 

    );
}

?>