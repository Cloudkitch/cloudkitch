<?php

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['servicename'] == 'weeklyorders')
{
	$reqwo = file_get_contents('php://input');

	$reswo = json_decode($reqwo,true);

	$fd = '';

	$td = '';

	$ic = '';

	$cu = '';

	$fdd = '';

	$tdd = '';

	$sql = '';

	$wo = array();

	$fd = $reswo['fd'];

	$td = $reswo['td'];

	$ic = $reswo['invitecode'];

	$cu = $reswo['cust'];

	if($fd != ''  && $td == '')
	{
		$fdd = date('Y-m-d',strtotime($fd));

		$sql.= " AND DATE_FORMAT(co.orderdate,'%Y-%m-%d') >= '".$fdd."' ";
	}

	if($fd == '' && $td != '')
	{
		$tdd = date('Y-m-d',strtotime($td));

		$sql.= " AND DATE_FORMAT(co.orderdate,'%Y-%m-%d') <= '".$tdd."' ";
	}

	if($fd != '' && $td != '')
	{
		$fdd = date('Y-m-d',strtotime($fd));

		$tdd = date('Y-m-d',strtotime($td));

		$sql.= " AND DATE_FORMAT(co.orderdate,'%Y-%m-%d') >= '".$fdd."' AND DATE_FORMAT(co.orderdate,'%Y-%m-%d') <= '".$tdd."' ";
	}

	if($cu == 'ALL')
	{
		$quewu = "SELECT u.userid,u.name FROM user as u,invitationdetail as ivd,invitationusermap as ium WHERE
				  ivd.id=ium.invitationid and ium.userid=u.userid and ivd.invitationcode='".$ic."'";

		$excwu = mysqli_query($conn,$quewu) or die(mysqli_error($conn));

		if(mysqli_num_rows($excwu) > 0)
		{
			$wo['wko'] = array();

			while($rowu = mysqli_fetch_assoc($excwu))
			{
				$u = array();

				$u['userid'] = $rowu['userid'];

				$u['user'] = $rowu['name'];

				$queuo = "SELECT SUM(coi.quantity) as qty FROM casseroleorder as co,casseroleorderitem as coi 
						  WHERE co.cassordid=coi.cassordid AND co.userid='".$rowu['userid']."' ".$sql." ORDER BY coi.cassitemid DESC";

				$excuo = mysqli_query($conn,$queuo) or die(mysqli_error($conn));

				if(mysqli_num_rows($excuo) > 0)
				{
					$rsuo = mysqli_fetch_assoc($excuo);

					if($rsuo['qty'] != null)
					{
						$u['qty'] = $rsuo['qty'];
					}
					else
					{
						$u['qty'] = '0';
					}	
				}
				else
				{
					$u['qty'] = '0';
				}

				$queov = "SELECT SUM(
						  CASE 
							WHEN coi.usrwntDelivery = '' THEN coi.total 
							WHEN coi.usrwntDelivery = 'N' THEN coi.total
							WHEN coi.usrwntDelivery = 'Y' THEN coi.total - 10
							ELSE 1 END
 							) as total  FROM casseroleorder as co,casseroleorderitem as coi WHERE 
 							co.cassordid=coi.cassordid AND co.userid='".$rowu['userid']."' ".$sql;

 				$excov = mysqli_query($conn,$queov) or die(mysqli_error($conn));

 				if(mysqli_num_rows($excov) > 0)
 				{
 					$rsov = mysqli_fetch_assoc($excov);

 					if($rsov['total'] != null)
 					{
 						$u['total'] = $rsov['total'];
 					}
 					else
 					{
 						$u['total'] = '0';
 					}	
 				}
 				else
 				{
 					$u['total'] = '0';
 				}	

 				array_push($wo['wko'], $u);				  	


			}

			$wo['status'] = 'success';

			$wo['msg'] = 'data available';	
		}
		else 
		{
			$wo['status'] = 'failure';

			$wo['msg'] = 'data not available';
		}		  
	}
	else 
	{
		$queu = "SELECT name FROM user WHERE userid='".$cu."'";
		$excu = mysqli_query($conn,$queu) or die(mysqli_error($conn));
		$rsu = mysqli_fetch_assoc($excu);

		if(mysqli_num_rows($excu) > 0)
		{
			$wo['wko'] = array();

			$u = array();

			$u['userid'] = $cu;

			$u['user'] = $rsu['name'];

			$queuo = "SELECT SUM(coi.quantity) as qty FROM casseroleorder as co,casseroleorderitem as coi 
						  WHERE co.cassordid=coi.cassordid AND co.userid='".$cu."' ".$sql." ORDER BY coi.cassitemid DESC";

			$excuo = mysqli_query($conn,$queuo) or die(mysqli_error($conn));

			if(mysqli_num_rows($excuo) > 0)
			{
				$rsuo = mysqli_fetch_assoc($excuo);

				if($rsuo['qty'] != null)
				{
					$u['qty'] = $rsuo['qty'];
				}
				else
				{
					$u['qty'] = '0';
				}	
			}
			else
			{
				$u['qty'] = '0';
			}

			$queov = "SELECT SUM(
						  CASE 
							WHEN coi.usrwntDelivery = '' THEN coi.total 
							WHEN coi.usrwntDelivery = 'N' THEN coi.total
							WHEN coi.usrwntDelivery = 'Y' THEN coi.total - 10
							ELSE 1 END
 							) as total  FROM casseroleorder as co,casseroleorderitem as coi WHERE 
 							co.cassordid=coi.cassordid AND co.userid='".$cu."' ".$sql;

			$excov = mysqli_query($conn,$queov) or die(mysqli_error($conn));

			if(mysqli_num_rows($excov) > 0)
			{
				$rsov = mysqli_fetch_assoc($excov);

				if($rsov['total'] != null)
				{
					$u['total'] = $rsov['total'];
				}
				else
				{
					$u['total'] = '0';
				}	
			}
			else
			{
				$u['total'] = '0';
			}

			array_push($wo['wko'],$u);

			 
			$wo['status'] = 'success';

			$wo['msg'] = 'data available';
		}
		else
		{
			$wo['status'] = 'failure';

			$wo['msg'] = 'data not available';
		}	
	}

	print_r(json_encode($wo));

	exit;	
}

?>