<?php 

session_start();
include 'inc/databaseConfig.php';
include 'inc/config.php'; 
if(!isset($_SESSION['info']['user']))
{
	header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
include 'inc/template_start.php';
include 'inc/page_head.php';
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<style type="text/css">
    .working-days .custom-checkbox
    {
        display: inline-block;
        margin-left: 10px;
    }
</style>
<div id="page-content">
  <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
  <img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1>Edit Restaurant</h1>
    </div>
</div>
</div>
</div>
<div class="row">
    <div class="col-sm-10 col-md-12 col-lg-12">
        <div class="block">
            <form id="createkitchen" name="createkitchen" class="form-horizontal form-bordered">
             <div class="form-group">
                <label class="col-md-3 control-label" for="val-bldgname">Image<span class="text-danger"></span></label>
                <div class="col-md-6">
                    <input type="hidden" id="base64" name="base64">
                    <input type="hidden" id="id" name="id">
                    <input type="file" id="image" name="image" onchange="previewImage(this)">
                    <div id="kitchenPic"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_kitchenname">Restaurant name<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="text" id="val_kitchenname" name="val_kitchenname" class="form-control" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_tagline">Tagline<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="text" id="val_tagline" name="val_tagline" class="form-control" placeholder="">
                </div>
            </div>
             <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinescategory">Restaurant Categories<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <select class="form-control" id="val_cuisinescategory" name="val_cuisinescategory[]" multiple>
                    <?php
                    $query = "SELECT * FROM cuisinetypemaster ORDER BY cuisinetype ASC";
                    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                    while($row = mysqli_fetch_assoc($result))
                    {
                        echo '<option value="'.$row['cuisinetypeid'].'">'.$row['cuisinetype'].'</option>';
                    }   
                    ?>
                    </select>
                </div>
            </div>
             <div class="form-group">
                <label class="col-md-3 control-label" for="forms">Restaurant forms<span class="text-danger"></span></label>
                <div class="col-md-6">
                    <input type="hidden" id="base64forms" name="base64forms">
                    <input type="file" id="forms" name="forms" onchange="previewForms(this)" multiple>
                    <div id="formsImg"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_kitchenveg">Restaurant Type<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                        <input type="checkbox" class="custom-control-input" id="val_kitchenveg" name="val_kitchenveg">
                        <label class="custom-control-label" for="val_kitchenveg">Veg</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                        <input type="checkbox" class="custom-control-input" id="val_kitchennonveg" name="val_kitchennonveg">
                        <label class="custom-control-label" for="val_kitchennonveg">Non Veg</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_kitchenname">Delivery time (in minutes)<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="number" id="val_time" name="val_time" class="form-control" placeholder="">
                </div>
            </div>
             <div class="form-group">
                <label class="col-md-3 control-label" for="val_kitchenemail">Working hours<span class="text-danger">*</span></label>
                <div class="col-md-3">
                   <input type="text" id="val_kitchenstart" name="val_kitchenstart" class="form-control" placeholder="">
                </div>
                 <div class="col-md-3">
                   <input type="text" id="val_kitchenend" name="val_kitchenend" class="form-control" placeholder="">
                </div>
            </div>
            <div class="form-group working-days">
                <label class="col-md-3 control-label" for="val_kitchenemail">Working days<span class="text-danger">*</span></label>
                <div class="col-md-6 workingdays">
                     <div class="custom-control custom-checkbox custom-control-lg mb-1">
                        <input type="checkbox" class="custom-control-input" id="val_sun" name="val_sun" onclick="workingdayscheck()">
                        <label class="custom-control-label" for="val_sun">Sun</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                        <input type="checkbox" class="custom-control-input" id="val_mon" name="val_mon" onclick="workingdayscheck()">
                        <label class="custom-control-label" for="val_mon">Mon</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                        <input type="checkbox" class="custom-control-input" id="val_tue" name="val_tue" onclick="workingdayscheck()">
                        <label class="custom-control-label" for="val_tue">Tue</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                        <input type="checkbox" class="custom-control-input" id="val_wed" name="val_wed" onclick="workingdayscheck()">
                        <label class="custom-control-label" for="val_wed">Wed</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                        <input type="checkbox" class="custom-control-input" id="val_thu" name="val_thu" onclick="workingdayscheck()">
                        <label class="custom-control-label" for="val_thu">Thu</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                        <input type="checkbox" class="custom-control-input" id="val_fri" name="val_fri" onclick="workingdayscheck()">
                        <label class="custom-control-label" for="val_fri">Fri</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                        <input type="checkbox" class="custom-control-input" id="val_sat" name="val_sat" onclick="workingdayscheck()">
                        <label class="custom-control-label" for="val_sat">Sat</label>
                    </div>
                    <br><span style="color:red;display:none" id="errworkingday">Please select at least one day</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_kitchenemail">Email address<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="text" id="val_kitchenemail" name="val_kitchenemail" class="form-control" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_kitchenpassword">Password<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="password" id="val_kitchenpassword" name="val_kitchenpassword" class="form-control" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_kitchenphone">Phone number<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="text" id="val_kitchenphone" name="val_kitchenphone" class="form-control" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_kitchenaddress">Address<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <textarea class="form-control" id="val_kitchenaddress" name="val_kitchenaddress" rows="4" placeholder="" spellcheck="false"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_pin">Add pin code<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <select class="form-control" id="val_pin" name="val_pin[]" multiple>
                    <?php
                    $query = "SELECT * FROM pincode WHERE `region` LIKE 'Mumbai'";
                    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                    while($row = mysqli_fetch_assoc($result))
                    {
                        echo '<option value="'.$row['pincode'].'">'.$row['pincode'].'</option>';
                    }   
                    ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_metatitle">Meta Title<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="val_metatitle" name="val_metatitle">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_keywords">Keywords<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <textarea class="form-control" id="val_keywords" name="val_keywords" rows="4" placeholder="" spellcheck="false"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_metadescription">Meta Description<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <textarea class="form-control" id="val_metadescription" name="val_metadescription" rows="4" placeholder="" spellcheck="false"></textarea>
                </div>
            </div>

            <div class="form-group form-actions">
                <div class="col-md-8 col-md-offset-3">
                    <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button>
                    <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
<!-- END Page Content -->
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Load and execute javascript code used only in this page -->
<script src="js/CasseroleService.js"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<!-- <script>
    $(function() { FormsValidation.init();
        invitecodedetail();
    });
</script> -->
    <script type="text/javascript">
        $(document).ready(function() {
          $('#val_cuisinescategory').select2({
                placeholder: "Select Restaurant Categories"
            });

            $('#val_pin').select2({
                placeholder: "Select Pin Code"
            });


          $('#val_kitchenstart').timepicker({
            timeFormat: 'h:mm p',
            interval: 30,
            minTime: '1',
            maxTime: '11:30pm',
            startTime: '10:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
          });
          $('#val_kitchenend').timepicker({
            timeFormat: 'h:mm p',
            interval: 30,
            maxTime: '11:30pm',
            startTime: '10:00',
            startTime: '10:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
          });
            $("#loading").hide();
            var id = "<?=$_GET['id']?>";
            var request = {"id":id};
            $.ajax({
                url: 'service.php?servicename=editkitchen',
                type: 'POST',
                data: JSON.stringify(request),
                contentType: 'application/json; charset=utf-8',
                datatype: 'JSON',
                async: true,
                success: function(data)
                {
                     var result = JSON.parse(data);
                     if (result['image']!="") {
                       $("#base64").val(result['image']); 
                       $("#kitchenPic").html('<img style="height: 150px;margin-top:10px;" src="'+result['image']+'" class="img-thumbnail">');
                     }
                     $("#val_kitchenname").val(result['kitchenname']);
                     var type = result['type'];
                     if (type==0) {
                        $('#val_kitchenveg').prop('checked', true);
                        $('#val_kitchennonveg').prop('checked', true);
                     }
                     else if(type==1)
                     {
                        $('#val_kitchenveg').prop('checked', true);
                     }
                     else
                     {
                        $('#val_kitchennonveg').prop('checked', true);
                     }
                     $("#id").val(result['id']);
                     $("#val_time").val(result['time']);
                     $("#val_kitchenemail").val(result['kitchenemail']);
                     $("#val_kitchenpassword").val(result['kitchenpassword']);
                     $("#val_kitchenphone").val(result['kitchenphone']);
                     $("#val_kitchenaddress").val(result['kitchenaddress']);
                     $("#val_tagline").val(result['tagline']);
                    
                     var daysarr = result['workingdays'].split(',');
                     for(var i=0;i<daysarr.length;i++){
                      $("#val_"+daysarr[i]).prop('checked', true)
                    }


                     var extarr = result['category'].split(',');
                     $('#val_cuisinescategory').val(extarr).trigger('change');

                     var extarr_pin = result['pin_code'].split(',');
                     $('#val_pin').val(extarr_pin).trigger('change');

                     for(var i=0;i<result['images'].length;i++){
                      var ext = result['images'][i].split('.').pop();
                      if (ext=='pdf') {   
                        $("#formsImg").append('<style type="text/css"> .img-close{ position: absolute; cursor: pointer; } </style><div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;padding-left:5px;" onclick = "imageClose(this)">X</div><img  src="img/pdf.png" data-type="'+ext+'" data-base64 = "'+result['images'][i]+'" style="height:100px;margin-right:5px;margin-top:5px;" class="img-thumbnail form-img"></div>');
                      }
                      else
                      {
                        $("#formsImg").append('<style type="text/css"> .img-close{ position: absolute; cursor: pointer; } </style><div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;padding-left:5px;" onclick = "imageClose(this)">X</div><img  src="'+result['images'][i]+'" data-type="'+ext+'" style="height:100px;margin-right:5px;margin-top:5px;" class="img-thumbnail form-img"></div>');
                      }
                    }
                     $("#val_kitchenstart").val(result['workingstart']);
                     $("#val_kitchenend").val(result['workingend']);
                     $("#val_metatitle").val(result['title']);
                     $("#val_keywords").val(result['keywords']);
                     $("#val_metadescription").val(result['description']);
                }
            });            
        });
        function previewImage(input)
        {
            if (input.files) {
                var filesAmount = input.files.length;
                var reader = new FileReader();
                reader.onload = function(event) {
                    $("#kitchenPic").html('<img style="height: 150px;margin-top:10px;" src="'+event.target.result+'" class="img-thumbnail">');
                    $("#base64").val(event.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function previewForms(input) {
            if (input.files) {
                var filesAmount = input.files.length;
                for (var i = 0; i < filesAmount; i++)   {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        var src = event.target.result;
                        var extarr = src.split(';base64');
                        var extstr = extarr[0];
                        var ext = extstr.split('/').pop();
                        if (ext=='pdf') {   
                            $("#formsImg").append('<style type="text/css"> .img-close{ position: absolute; cursor: pointer; } </style><div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;padding-left:5px;" onclick = "imageClose(this)">X</div><img  src="img/pdf.png" data-type="'+ext+'" data-base64 = "'+src+'" style="height:100px;margin-right:5px;margin-top:5px;" class="img-thumbnail form-img"></div>');
                        }
                        else
                        {
                            $("#formsImg").append('<style type="text/css"> .img-close{ position: absolute; cursor: pointer; } </style><div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;padding-left:5px;" onclick = "imageClose(this)">X</div><img  src="'+src+'" data-type="'+ext+'" style="height:100px;margin-right:5px;margin-top:5px;" class="img-thumbnail form-img"></div>');
                        }
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }
        $("form[name='createkitchen']").validate({
        rules: {
            val_kitchenname : "required",
            val_time : "required",
            val_kitchenemail : "required",
            val_kitchenpassword : "required",
            val_kitchenphone : "required",
            val_kitchenaddress : "required",
            val_tagline : "required",
            val_pin : "required"

        },
        submitHandler: function(form) {
            var checked = $(".workingdays input:checked").length;
            if (checked < 1){
                $("#errworkingday").show();
                return false;
            }
            $(this).find('button[type=submit]').prop('disabled', true);
            $(".preloader").show();
            var images = "";
            $( ".form-img" ).each(function() {
                var type = $(this).data('type');
                if (type=='pdf') {
                    var image = $(this).data('base64');
                }
                else
                {
                    var image = $(this).attr("src");
                }
                images += "###"+image;
            });
            $("#base64forms").val(images);
            var data = new FormData($('#createkitchen')[0]);
            $.ajax({
                url: 'service.php?servicename=updatekitchen',
                type: 'POST',
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                data: data, 
                success: function(data)
                {
                     $(".preloader").hide();
                     var result = JSON.parse(data);
                     if(result.status == 'success')
                     {
                        $("#toast-success").html(result.msg);
                        $("#toaster").fadeIn();
                        window.location.href = 'kitchenlist.php';
                     }
                     else
                     {
                        $("#toast-error").html(result.msg);
                        $("#toasterError").fadeIn();
                     }
                     setTimeout(function(){
                        $("#toaster").fadeOut();
                        $("#toasterError").fadeOut();
                  }, 3000);
                }
            });
        }
    });

    function workingdayscheck(){
        var checked = $('.workingdays input:checkbox').filter(':checked').length ;
        if (checked < 1){
            // alert("Please check at least one checkbox");
            $("#errworkingday").show();
        }else{
            $("#errworkingday").hide();
        }
    }
    </script>
    <?php include 'inc/template_end.php'; ?>

