<?php 
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
session_start();
if(!isset($_SESSION['info']['user']))
{
     header("Location: http://localhost:7755/MIHAdmin/index.php");
}
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>
<?php
$weddingCode =  $_SESSION['info']['weddingcode'];
$query = "select * from wedding_event where wedding_code =$weddingCode"; 
$result = mysqli_query($conn, $query);

?>
<!-- Page content -->
<div id="page-content">
    <!-- Validation Header -->
    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>

    <div class="block" id="eventlist">
                <!-- General Elements Title -->

                <div class="block-title">
                    <div class="block-options pull-right">
                        <!-- <a href="javascript:void(0)" class="btn btn-effect-ripple btn-default toggle-bordered enable-tooltip" data-toggle="button" title="Toggles .form-bordered class">Borderless</a> -->

                    </div>
                    <h2>Create Invitation</h2>
                </div>



<?php 
if($_SESSION['info']['type'] == 'admin'){

?>
                <form action="EventList.php" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" onsubmit="return false;">

                <div class="form-group">
                        <label class="col-md-3 control-label" for="example-select">Select Wedding :</label>
                        <div class="col-md-6">
                            <select id="val-selectWedding" onchange="wedding();" name="val-selectWedding" class="form-control" size="1">
                                <option value="">Please select</option>
                                
                            </select>
                        </div>
                    </div>

                    <!-- <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="button" onclick="wedding();" class="btn btn-effect-ripple btn-primary">Submit</button>
                            <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                        </div>
                    </div> -->

                 </form>
                <?php
            }
                 ?>

<!-- By Dhruvpal On 9-6-2016  -->
                 <!-- <div class="content-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="header-section" style="display:none" id="eventheader" >
                                <h1>Event List</h1>
                            </div>
                        </div>
                       
                    </div>
                </div> -->

    <!-- END Validation Header -->

    <!-- Form Validation Content -->

    <!-- By Dhrurvpal on 9-7-2016 -->

    <div class="row" style="display:none" id="cntev" >
        <div class="col-sm-10 col-md-12 col-lg-12">
    
            <div class="form-group remove-margin-bottom">
                    <div class="col-sm-6 push-bit">
                        <select id="estore-categories" name="estore-categories" class="select-chosen" data-placeholder="Choose Event.." style="width: 250px;" multiple>
                            <?php
                           while($row = mysqli_fetch_assoc($result)) {
       echo "<option>"+$row['event_title']+"</option>";
    }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-effect-ripple btn-effect-ripple btn-primary">Create Invitation</button>
                    </div>
                </div>
            
    <div class="row" style="display:" id="cntev" >
        <div class="col-sm-10 col-md-12 col-lg-12">
                 <table id ="tableEventList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Event Name</th>
                                                 <th>Event Date</th>
                                                 <th>Operation</th>
                                                
                                                </tr> 
                                                </thead>
                                              
                                            </table>
            </div>
            </div>
                      </div>
    </div>

    

                <!-- END Checkboxes Content -->
            </div>
            <!-- END Checkboxes Block -->

    
    <!-- END Form Validation Content -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/MIHService.js"></script>



<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){

    var type = '<?php echo $_SESSION['info']['type']; ?>';

    if(type == 'subadmin' )
    {
        $("#loading").show();
    } 
    else
    {
        $("#loading").hide();
    }

 
weddingListForEventPage();

});
</script>

<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 
                    // getWeddingList();



                    });</script>

<script type="text/javascript">

var type = '<?php echo $_SESSION['info']['type']; ?>';

var wcod = '<?php echo $_SESSION['info']['weddingcode']; ?>';
    
    function wedding()
    {
        var wed = $("#val-selectWedding").val();
        console.log("My wed is : "+wed);
        getEventList(wed);
                    
    }

    if(type == 'subadmin')
    {
        //var wed = 2616;
         getEventList(wcod);
    }
</script>
