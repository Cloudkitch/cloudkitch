<?php 
session_start();
include 'inc/config.php'; 

if(!isset($_SESSION['info']['user']))
{
	header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">

  <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
  <img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<!-- Validation Header -->
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1>Update Meal Type Status</h1>
        <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
      </div>
    </div>
   
</div>
</div>
<!-- END Validation Header -->

<!-- Form Validation Content -->
<div class="row">
  <div class="col-sm-12 col-md-12 col-lg-12">
    <!-- Form Validation Block -->
    <table id ="tableChefList" class="table table-vcenter table-striped table-hover table-borderless">
      <thead>
       <tr>
         <th>Sr</th>
         <th>Meal Type</th>
         <th>Status</th>
         <th>Action</th>
       </tr> 
     </thead>

   </table>
   <!-- END Form Validation Block -->
 </div>
</div>

</div>


<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#loading").hide();
    getMealTypeStatus();
  });
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 
});
</script>
<script>
  function getMealTypeStatus()
  {
    $('#loading').show();
    $.ajax({
      type: "POST",
      url : "service.php?servicename=getMealTypeStatus",
      datatype: "JSON",
      success: function(data)
      {
        $('#loading').hide();
        var cusl = JSON.parse(data);
        var customer = new Array();
        for(var c=0;c<cusl.mealtype.length;c++)
        {
          customer[c] = new Array();
          customer[c][0] = c+1;
          customer[c][1] = cusl.mealtype[c].meal_type;
          if(cusl.mealtype[c].status == '1'){
            customer[c][2] = "Active";
          }else{
            customer[c][2] = "Inactive";
          }
          if(cusl.mealtype[c].status == '1'){
            customer[c][3] = "<button type='button' onclick='changemealtypestatus("+cusl.mealtype[c].id+",\"0\");' class='btn btn-rounded btn-primary'>Inactive</button>";
          }else{
            customer[c][3] = "<button type='button' onclick='changemealtypestatus("+cusl.mealtype[c].id+",\"1\");' class='btn btn-rounded btn-primary'>Active</button>";
          }
          
        }  
        $('#tableChefList').dataTable({
          "aaData": customer,
          "scrollX": true,
          "bDestroy": true
        });
      }
    });
  }

  function changemealtypestatus(id,st){
    var request = {"id":id,"status":st};
        $.ajax({
        url: 'service.php?servicename=changemealtypestatus',
        type: 'POST',
        data: JSON.stringify(request),
        contentType: 'application/json; charset=utf-8',
        datatype: 'JSON',
        async: true,
        success: function(data)
        {
            getMealTypeStatus();
        }
        });
  }

 
</script>