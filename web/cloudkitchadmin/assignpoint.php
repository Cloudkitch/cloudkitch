<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/Casseroleadmin/index.php");
	header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php';
      include 'inc/databaseConfig.php';
 ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Customer List</h1>
                    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
     <div class="row">
     <div class="form-group">
                    <label class="col-md-3 control-label" for="example-select">Select Cloudkitch Points</label>
                    <div class="col-md-6">
                    <select id="corporate" name="example-select" class="form-control" onchange="customerlistnew()">
                        <option value="">Select Corporate</option>
                    <?php
                        $query = "SELECT * FROM corporate WHERE status = '1' ORDER BY id ASC";
                        $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                        while($row = mysqli_fetch_assoc($result))
                        {
                            echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
                        }   
                    ?> 
                    </select>
                    </div>
                </div>
                <br/><br/>
     </div>
    <div class="row" style = "display:none;" id="corporateuserlist">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tablecul" class="table table-vcenter table-striped table-hover table-borderless">
            <thead>
                    <tr>
                    <th>Sr</th>
                    <th><label for="example-checkbox1">
                    <input type="checkbox"  id="checkAll" name="checkAll" value="option1"> Check All
                    </label></th>
                    <th>User Name</th>     
                    <th>Phone Number</th>                                            
                    <th>Points</th>
                    <th>Assign</th>
                </tr> 
                </thead>
                
            </table>
            <!-- END Form Validation Block -->
        </div>
        <div class="form-group form-actions">
            <div class="col-md-8 col-md-offset-3" style="margin-left: 45%;">
                <button type="submit"  class="btn btn-effect-ripple btn-primary" onclick="addmytreepoint();">Submit</button>
                <!-- <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button> -->
            </div>
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script src="js/CasseroleService.js"></script>



<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
 
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>
    $(function(){ UiTables.init(); 
        var invitecode = '';

        invitecode = $("#invitecode").val();

    });
</script>

    <script type="text/javascript">
      
    function customerlistnew()
    {
    
        var corporate = $("#corporate").val();
        console.log("corporate"+corporate);

       $.ajax({
                url: 'assignmtp_service.php?servicename=customer_list',  
                type: 'POST',
                datatype: 'JSON',
                data: JSON.stringify({"corporate":corporate}),
                async: false,
                success: function(cul)
                {
                   

                   $("#corporateuserlist").show();
                    var cr = JSON.parse(cul);

                    var ca = new Array();
                    if(cr.status == "failure"){
                        var table = $('#tablecul').DataTable({
                        "aaData": ca,
                        "bDestroy": true,
                        "scrollX": true,
                        "autoWidth": true
                        }); 
                        return;
                    }

                    for(var c=0;c<cr.customerlist.length;c++)
                    {
                        ca[c] = new Array();

                        ca[c][0] = c+1;

                        ca[c][1] = '<input type="checkbox" id="example-checkbox1" name="cus" value="'+cr.customerlist[c].walletid+'">';

                        ca[c][2] = cr.customerlist[c].name;

                        ca[c][3] = cr.customerlist[c].phone;

                        ca[c][4] = cr.customerlist[c].cloudkitchValue;

                        ca[c][5] = '<input type="number" id="txt'+cr.customerlist[c].walletid+'" value="" >';


                    }

                    var table = $('#tablecul').DataTable({
                        "aaData": ca,
                        "bDestroy": true,
                        "scrollX": true,
                        "autoWidth": true
                    }); 

                    $('#checkAll').on('click', function(){

                    //alert("Heelo");
                        // Check/uncheck all checkboxes in the table
                        var rows = table.rows({ 'search': 'applied' }).nodes();
                        $('input[type="checkbox"]', rows).prop('checked', this.checked);
                     }); 
                }

       });
    }

    </script>

    <script type="text/javascript">
    
function addmytreepoint()
{
   
        var umtp = [];

        var wltid =  '';

        var mtpv = '';
        
        $.each($("input[name='cus']:checked"),function(){

               wltid = $(this).val();

               mtpv = $("#txt"+wltid).val();   

               console.log("Wltid"+wltid+"Mtpv"+mtpv);

              umtp.push({
                          "walletid":wltid,
                          "mtp":mtpv 

              });

        });

        console.log("MTP"+JSON.stringify(umtp));

        $.ajax({
                 url: 'assignmtp_service.php?servicename=Assign-MTP',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(umtp),
                 async:false,
                 success: function(rmtp)
                 {
                     console.log("MTP res"+JSON.stringify(rmtp));

                     var r = JSON.parse(rmtp);

                     if(r.status == 'success')
                     {
                        // location.reload();
                        customerlistnew();
                     } 
                 }


        })


 

    
}


</script>