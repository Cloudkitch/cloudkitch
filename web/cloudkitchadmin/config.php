<?php
/**
 * config.php
 *
 * Author: pixelcave
 *
 * Configuration file. It contains variables used in the template as well as the primary navigation array from which the navigation is created
 *
 */
    // if(!isset($_SESSION)) 
    // { 
    //     session_start(); 
    // }

//session_start();
/* Template variables */
$template = array(
    'name'              => 'My Tree',
    'version'           => '1.0',
    'author'            => 'pixelcave',
    'robots'            => 'noindex, nofollow',
    'title'             => 'My Tree',
    'description'       => 'Khao Khilao Kamao',
    // true                         enable page preloader
    // false                        disable page preloader
    'page_preloader'    => true,
    // 'navbar-default'             for a light header
    // 'navbar-inverse'             for a dark header
    'header_navbar'     => 'navbar-inverse',
    // ''                           empty for a static header/main sidebar
    // 'navbar-fixed-top'           for a top fixed header/sidebars
    // 'navbar-fixed-bottom'        for a bottom fixed header/sidebars
    'header'            => 'navbar-fixed-top',
    // ''                           empty for the default full width layout
    // 'fixed-width'                for a fixed width layout (can only be used with a static header/main sidebar)
    'layout'            => '',
    // 'sidebar-visible-lg-mini'    main sidebar condensed - Mini Navigation (> 991px)
    // 'sidebar-visible-lg-full'    main sidebar full - Full Navigation (> 991px)
    // 'sidebar-alt-visible-lg'     alternative sidebar visible by default (> 991px) (You can add it along with another class - leaving a space between)
    // 'sidebar-light'              for a light main sidebar (You can add it along with another class - leaving a space between)
    'sidebar'           => 'sidebar-visible-lg-full',
    // ''                           Disable cookies (best for setting an active color theme from the next variable)
    // 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
    'cookies'           => '',
    // '', 'classy', 'social', 'flat', 'amethyst', 'creme', 'passion'
    'theme'             => 'amethyst',
    // Used as the text for the header link - You can set a value in each page if you like to enable it in the header
    'header_link'       => '',
    // The name of the files in the inc/ folder to be included in page_head.php - Can be changed per page if you
    // would like to have a different file included (eg a different alternative sidebar)
    'inc_sidebar'       => 'page_sidebar',
    'inc_sidebar_alt'   => 'page_sidebar_alt',
    'inc_header'        => 'page_header',
    // The following variable is used for setting the active link in the sidebar menu
    'active_page'       => basename($_SERVER['PHP_SELF'])
);

// if($_SESSION['info']['type'] == 'admin')
// {
//     // array_push($template, array('theme'=>'classy'));
//     $template['theme'] = '';
// }

// if($_SESSION['info']['type'] == 'subadmin')
// {
//    // array_push($template, array('theme'=>'passion'));
//     $template['theme'] = 'passion';
// }

/* Primary navigation array (the primary navigation will be created automatically based on this array, up to 3 levels deep) */

//Menu List


// if($_SESSION['info']['type'] == 'admin')
// {
    $primary_nav = array(
    array(
        'name'  => 'Dashboard',
        'url'   => 'Dashboard.php',
        'icon'  => 'gi gi-compass'
    ),
    array(
        'url'   => 'separator',
    ),
    // array(
    //     'name'  => 'Add Invitation Code',
    //     'url'   => 'AddInvCode.php',
    //     'icon'  => 'gi gi-compass'
    // ),
    array(
        'name'  => 'Invitation Code',
        'url'   => 'InviteList.php',
        'icon'  => 'gi gi-qrcode'
    ),
    // array(
    //     'name'  => 'Add Popular Cuisine',
    //     'url'   => 'addCuisine.php',
    //     'icon'  => 'gi gi-compass'
    // ),
    array(
        'name'  => 'Customers',
        'url'   => 'customerlist.php',
        'icon'  => 'gi gi-group'
    ),
    array(
        'name'  => 'Chefs',
        'url'   => 'cheflist.php',
        'icon'  => 'gi gi-group'
    ),
    // array(
    //     'name'  => 'Add Dish',
    //     'url'   => 'adddish.php',
    //     'icon'  => 'gi gi-compass'
    // ),
    // array(
    //     'name'  => 'Add Cuisine Category',
    //     'url'   => 'addcuisinecategory.php',
    //     'icon'  => 'gi gi-compass'
    // ),
    // array(
    //     'name'  => 'Add Building',
    //     'url'   => 'addbldg.php',
    //     'icon'  => 'gi gi-compass'
    //     ),
    // array(
    //     'name'  => 'Add Cuisine Type',
    //     'url'   => 'addcuisinetype.php',
    //     'icon'  => 'gi gi-compass'
    //     ),
    // array(
    //     'name'  => 'Add Recharge',
    //     'url'   => 'addrecharge.php',
    //     'icon'  => 'gi gi-compass'
    //     ),
    // array(
    //     'name'  => 'Add Suggested Tags',
    //     'url'   => 'addtags.php',
    //     'icon'  => 'gi gi-compass'
    //     ),
    array(
        'name'  => 'Buildings',
        'url'   => 'bldglist.php',
        'icon'  => 'gi gi-building'
        ),
    array(
        'name'  => 'Cravers',
        'url'   => 'craverslist.php',
        'icon'  => 'gi gi-parents'
        ),
    // array(
    //     'name'  => 'Cuisine Categories',
    //     'url'   => 'cuisinecatlist.php',
    //     'icon'  => 'gi gi-compass'
    //     ),
    array(
        'name'  => 'Cuisines',
        'url'   => 'cuisinelist.php',
        'icon'  => 'gi gi-pizza'
        ),
    array(
        'name'  => 'Cuisine Types',
        'url'   => 'cuisinetypelist.php',
        'icon'  => 'gi gi-certificate'
        ),
    // array(
    //     'name'  => 'Customers',
    //     'url'   => 'customerlist.php',
    //     'icon'  => 'gi gi-compass'
    //     ),
    // array(
    //     'name'  => 'Dishes',
    //     'url'   => 'dishlist.php',
    //     'icon'  => 'gi gi-compass'
    //     ),
    array(
        'name'  => 'Orders',
        'url'   => 'orderlist.php',
        'icon'  => 'gi gi-shopping_bag'
        ),
    array(
        'name'  => 'Popular Cuisines',
        'url'   => 'popularcuisinelist.php',
        'icon'  => 'gi gi-fast_food'
        ),
    // array(
    //     'name'  => 'Recharges',
    //     'url'   => 'rechargelist.php',
    //     'icon'  => 'gi gi-compass'
    //     ),
    array(
        'name'  => 'Cuisine Suggested Tags',
        'url'   => 'taglist.php',
        'icon'  => 'gi gi-tag'
        ),
    array(
        'name'  => 'Wallet',
        'url'   => 'walletdisplay.php',
        'icon'  => 'gi gi-wallet'
        ),
    array(
        'name'  => 'Coupons',
        'url'   => 'couponcodelist.php',
        'icon'  => 'gi gi-credit_card'
        ),
	// array(
 //        'name'  => 'Sharedwallet',
 //        'url'   => 'sharedwallet.php',
 //        'icon'  => 'gi gi-compass'
 //        ),
	array(
        'name'  => 'Chefincome',
        'url'   => 'chefpayment.php',
        'icon'  => 'gi gi-money'
        ),
	array(
        'name'  => 'Add Mytree Points',
        //'url'   => 'addmytreepoints.php',
        'url'   => 'assignmtp.php',
        'icon'  => 'gi gi-money'
        ),
	array(
        'name'  => 'Orders2',
        'url'   => 'orders.php',
        'icon'  => 'gi gi-shopping_bag'
        ),
	array(
        'name'  => 'Orders3',
        'url'   => 'chefwiseorders.php',
        'icon'  => 'gi gi-shopping_bag'
        ),
        array(
        'name'  => 'Chef Payout',
        'url'   => 'ChefPayout.php',
        'icon'  => 'gi gi-money'
        ),
        array(
        'name'  => 'Chef Payout History',
        'url'   => 'ChefPayoutHistory.php',
        'icon'  => 'gi gi-money'
        ),
        array(
        'name'  => 'Chef Payment',
        'url'   => 'ChefTotalPayout.php',
        'icon'  => 'gi gi-money'
        ),
        array(
        'name'  => 'Chef Report',
        'url'   => 'chef_report.php',
        'icon'  => 'gi gi-file'
        ),
        array(
        'name'  => 'Customer Report',
        'url'   => 'customer_report.php',
        'icon'  => 'gi gi-file'
        ),
        array(
        'name'  => 'Chef Activity Report',
        'url'   => 'chef-report-detail.php',
        'icon'  => 'gi gi-file'
        ),
        array(
        'name'  => 'Customer Activity Report',
        'url'   => 'customer-order-detail.php',
        'icon'  => 'gi gi-file'
        ),
        array(
        'name'  => 'Send Notification',
        'url'   => 'sendnotification.php',
        'icon'  => 'gi gi-message_new'
        ),
        array(
        'name'  => 'Advertisement',
        'url'   => 'advertise.php',
        'icon'  => 'gi gi-imac'
            ),
        array(
        'name'  => 'Cancel Order Payout',
        'url'   => 'CancelOrderPayout.php',
        'icon'  => 'gi gi-cart_out'
            ),
        array(
        'name'  => 'Cancel Orders Due',
        'url'   => 'CancelOrders.php',
        'icon'  => 'gi gi-circle_arrow_down'
            ),
        array(
        'name'  => 'Reported Orders',
        'url'   => 'reportedorders.php',
        'icon'  => 'gi gi-flag'
            ),
        array(
        'name'  => 'Reported Order History',
        'url'   => 'reportedorders-history.php',
        'icon'  => 'gi gi-history'
            ),
         array(
        'name'  => 'Chef Approval',
        'url'   => 'chefonboard.php',
        'icon'  => 'gi gi-list'
            ),
         array(
        'name'  => 'Cancel Order Pay History',
        'url'   => 'chefcancelopayouthistory.php',
        'icon'  => 'gi gi-money'
            ),
         array(
        'name'  => 'Dormant Chef',
        'url'   => 'dormantchef.php',
        'icon'  => 'gi gi-group'
            ),
         array(
        'name'  => 'Dormant User',
        'url'   => 'dormantuser.php',
        'icon'  => 'gi gi-group'
            ),
         array(
        'name'  => 'Weekly Total Orders',
        'url'   => 'weeklytotalorders.php',
        'icon'  => 'gi gi-more_items'
            ),
         array(
        'name'  => 'Active Chefs',
        'url'   => 'activechefs.php',
        'icon'  => 'gi gi-old_man'
            ),
          array(
        'name'  => 'Active Users',
        'url'   => 'activeuser.php',
        'icon'  => 'gi gi-group'
            ),
          array(
        'name'  => 'Paymentwise Order Report',
        'url'   => 'paymenttypewiseorders.php',
        'icon'  => 'gi gi-money'
            ),
          array(
        'name'  => 'Categorywise Orders',
        'url'   => 'categorywiseorders.php',
        'icon'  => 'hi hi-tasks'
            ),
          array(
        'name'  => 'Top Chefs',
        'url'   => 'topcheforders.php',
        'icon'  => 'hi hi-tasks'
            ),
          array(
        'name'  => 'Averagewise Orders',
        'url'   => 'weekdayvsweekendorders.php',
        'icon'  => 'gi gi-calendar'
            ),
          array(
        'name'  => 'Customerwise Orders',
        'url'   => 'weekly.php',
        'icon'  => 'gi gi-calendar'
            ),
        //  array(
        // 'name'  => 'Create Chapter Champion',
        // 'url'   => 'create-chapter-champion.php',
        // 'icon'  => 'gi gi-git_create'
        //     ),

    // array(
    //     'name'  => 'Create Event',
    //     'url'   => 'CreateEvent.php',
    //     'icon'  => 'gi gi-compass'
	//chefwiseorders.php
    // ),
    // array(
    //      'name' => 'Event List',
    //      'url'  => 'EventList.php',
    //      'icon' => 'gi gi-compass'
    //     ),
    // array(
    //        'name' => 'Configure Subadmin',
    //        'url' => 'ConfSubadmin.php',
    //        'icon' => 'gi gi-compass'  
    //     ),
   
  
);

// }  

// if($_SESSION['info']['type'] == 'subadmin')
// {
//     $primary_nav = array(
//     array(
//         'name'  => 'Dashboard',
//         'url'   => 'Deshboard.php',
//         'icon'  => 'gi gi-compass'
//     ),
//     array(
//         'url'   => 'separator',
//     ),
//     array(
//         'name'  => 'Error Report',
//         'url'   => 'ErrorReport.php',
//         'icon'  => 'gi gi-compass'
//     ),
//     array(
//          'name' => 'Event List',
//          'url'  => 'EventList.php',
//          'icon' => 'gi gi-compass'
//         ),
//     // array(
//     //        'name' => 'User List',
//     //        'url' => 'UserList.php',
//     //        'icon' => 'gi gi-compass'  
//     //     ),
//     array(
//             'name' => 'Blocked Post List',
//             'url' => 'BlockPostList.php',
//             'icon' => 'gi gi-compass'
//         ),
//     array(
//             'name' => 'Help Details Report',
//             'url' => 'HelpDetailsList.php',
//             'icon' => 'gi gi-compass'
//         ),
//     array(
//            'name' => 'Configure Subadmin',
//            'url' => 'ConfSubadmin.php',
//            'icon' => 'gi gi-compass'  
//         ),array(
//            'name' => 'Invitation',
//            'url' => 'invitaiont.php',
//            'icon' => 'gi gi-compass'  
//         ),
   
  
// );

// }  



//
/*$primary_nav = array(
    array(
        'name'  => 'Dashboard',
        'url'   => 'index.php',
        'icon'  => 'gi gi-compass'
    ),
    array(
        'url'   => 'separator',
    ),
    array(
        'name'  => 'User Interface',
        'icon'  => 'fa fa-rocket',
        'sub'   => array(
            array(
                'name'  => 'Widgets',
                'url'   => 'page_ui_widgets.php',
            ),
            array(
                'name'  => 'Elements',
                'sub'   => array(
                    array(
                        'name'  => 'Blocks &amp; Grid',
                        'url'   => 'page_ui_blocks_grid.php'
                    ),
                    array(
                        'name'  => 'Typography',
                        'url'   => 'page_ui_typography.php'
                    ),
                    array(
                        'name'  => 'Buttons &amp; Dropdowns',
                        'url'   => 'page_ui_buttons_dropdowns.php'
                    ),
                    array(
                        'name'  => 'Navigation &amp; More',
                        'url'   => 'page_ui_navigation_more.php'
                    ),
                    array(
                        'name'  => 'Progress &amp; Loading',
                        'url'   => 'page_ui_progress_loading.php'
                    ),
                    array(
                        'name'  => 'Tables',
                        'url'   => 'page_ui_tables.php'
                    )
                )
            ),
            array(
                'name'  => 'Forms',
                'sub'   => array(
                    array(
                        'name'  => 'Components',
                        'url'   => 'page_forms_components.php'
                    ),
                    array(
                        'name'  => 'Wizard',
                        'url'   => 'page_forms_wizard.php'
                    ),
                    array(
                        'name'  => 'Validation',
                        'url'   => 'page_forms_validation.php'
                    )
                )
            ),
            array(
                'name'  => 'Icon Packs',
                'sub'   => array(
                    array(
                        'name'  => 'Font Awesome',
                        'url'   => 'page_ui_icons_fontawesome.php'
                    ),
                    array(
                        'name'  => 'Glyphicons Pro',
                        'url'   => 'page_ui_icons_glyphicons_pro.php'
                    )
                )
            )
        )
    ),
    array(
        'name'  => 'Components',
        'icon'  => 'gi gi-airplane',
        'sub'   => array(
            array(
                'name'  => 'To-do List',
                'url'   => 'page_comp_todo.php',
            ),
            array(
                'name'  => 'Gallery',
                'url'   => 'page_comp_gallery.php',
            ),
            array(
                'name'  => 'Google Maps',
                'url'   => 'page_comp_maps.php',
            ),
            array(
                'name'  => 'Calendar',
                'url'   => 'page_comp_calendar.php',
            ),
            array(
                'name'  => 'Charts',
                'url'   => 'page_comp_charts.php',
            ),
            array(
                'name'  => 'CSS3 Animations',
                'url'   => 'page_comp_animations.php',
            ),
            array(
                'name'  => 'Tree View Lists',
                'url'   => 'page_comp_tree.php',
            ),
            array(
                'name'  => 'Nestable &amp; Sortable Lists',
                'url'   => 'page_comp_nestable.php',
            )
        )
    ),
    array(
        'name'  => 'UI Layouts',
        'icon'  => 'gi gi-more_items',
        'sub'   => array(
            array(
                'name'  => 'Static',
                'url'   => 'page_layout_static.php'
            ),
            array(
                'name'  => 'Static Fixed Width',
                'url'   => 'page_layout_static_fixed_width.php'
            ),
            array(
                'name'  => 'Top Header (Fixed)',
                'url'   => 'page_layout_fixed_top.php'
            ),
            array(
                'name'  => 'Bottom Header (Fixed)',
                'url'   => 'page_layout_fixed_bottom.php'
            ),
            array(
                'name'  => 'Sidebar Mini (Static)',
                'url'   => 'page_layout_static_sidebar_mini.php'
            ),
            array(
                'name'  => 'Sidebar Mini (Fixed)',
                'url'   => 'page_layout_fixed_sidebar_mini.php'
            ),
            array(
                'name'  => 'Visible Alternative Sidebar',
                'url'   => 'page_layout_alternative_sidebar_visible.php'
            )
        )
    ),
    array(
        'name'  => 'Extra Pages',
        'icon'  => 'fa fa-gift',
        'sub'   => array(
            array(
                'name'  => 'Error Page',
                'url'   => 'page_ready_error.php'
            ),
            array(
                'name'  => 'Blank',
                'url'   => 'page_ready_blank.php'
            ),
            array(
                'name'  => 'Article',
                'url'   => 'page_ready_article.php'
            ),
            array(
                'name'  => 'Timeline',
                'url'   => 'page_ready_timeline.php'
            ),
            array(
                'name'  => 'Invoice',
                'url'   => 'page_ready_invoice.php'
            ),
            array(
                'name'  => 'Search Results',
                'url'   => 'page_ready_search_results.php'
            ),
            array(
                'name'  => 'Pricing Tables',
                'url'   => 'page_ready_pricing_tables.php'
            ),
            array(
                'name'  => 'FAQ',
                'url'   => 'page_ready_faq.php'
            ),
            array(
                'name'  => 'User Profile',
                'url'   => 'page_ready_profile.php'
            ),
            array(
                'name'  => 'Login, Register &amp; Lock',
                'sub'   => array(
                    array(
                        'name'  => 'Login',
                        'url'   => 'index.php'
                    ),
                    array(
                        'name'  => 'Password Reminder',
                        //'url'   => 'page_ready_reminder.php'
                        'url'   => 'Reminderpassword.php'
                    ),
                    array(
                        'name'  => 'Register',
                        'url'   => 'register.php'
                       // 'url'   => 'page_ready_register.php'
                    ),
                    array(
                        'name'  => 'Lock Screen',
                        'url'   => 'lockScreen.php'
                        //'url'   => 'page_ready_lock_screen.php'
                    )
                )
            )
        )
    ),
    array(
        'url'   => 'separator',
    ),
    array(
        'name'  => 'Email Center',
        'icon'  => 'gi gi-inbox',
        'url'   => 'page_app_email.php'
    ),
    array(
        'name'  => 'Social Net',
        'icon'  => 'fa fa-share-alt',
        'url'   => 'page_app_social.php'
    ),
    array(
        'name'  => 'Media Box',
        'icon'  => 'gi gi-picture',
        'url'   => 'page_app_media.php'
    ),
    array(
        'name'  => 'eStore',
        'icon'  => 'gi gi-shopping_cart',
        'url'   => 'page_app_estore.php'
    )
);*/