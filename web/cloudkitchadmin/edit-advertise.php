<?php //include 'inc/config.php'; $template['header_link'] = 'FORMS'; 

session_start();
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: index.php");
}
 include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user']; 
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">

  <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Advertise</h1>
                </div>
            </div>
           
        </div>
    </div>

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <div class="block">
                <!-- Form Validation Title -->
                <div class="block-title">
                    <h2>Create Advertise</h2>
                </div>
                <!-- END Form Validation Title -->

                <!-- Form Validation Form -->
                <form id="createadv" action="" class="form-horizontal form-bordered">
                    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">

                    <input type="hidden" name="adid" id="adid" value="<?php echo $_POST['adid'];?>">

                    <input type="hidden" name="mtype" id="mtype" value="<?php echo $_POST['mtype']; ?>" >

                    <input type="hidden" name="url" id="url" value="<?php echo $_POST['url'];?>">

                    <input type="hidden" name="desc" id="desc" value="<?php echo $_POST['desc'];?>">

                    <input type="hidden" name="curl" id="curl" value="<?php echo $_POST['curl'];?>">

                    <div class="form-group" id="selectInvite">
                        <label class="col-md-3 control-label" for="val-selmedt">Media Type<span class="text-danger">*</span></label>
                        <div class="col-md-6" >
                            <input type="text" id="val-selmedt" name="val-selmedt" class="form-control" placeholder="" disabled>
                            </select>
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-desc">Description<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-desc" name="val-desc" class="form-control" placeholder="Please Enter Description">
                        </div>
                    </div>

                    <div class="form-group" id="divurl">
                        <label class="col-md-3 control-label" for="val-curl">Url<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-curl" name="val-curl" class="form-control" placeholder="Please Enter Url">
                        </div>
                    </div>

                    <div class="form-group" id="vid">
                        <label class="col-md-3 control-label" for="val-vid">Video<span class="text-danger">*</span></label>
                        <div id="vid0">
                        <div class="col-md-6" >
                            <input type="text" id="val-vid0" name="val-vid" class="form-control" placeholder="">
                        </div>
                        </div>
                    </div>

                                
                    <div class="form-group" id="img">
                        <label class="col-md-3 control-label" for="val-images">Image</label>
                        <div class="col-md-6" id="filediv" > 
                            <div id="filediv0">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div id="abcd0" class="abcd" style="width:300px;height:300px;">
                                            <div>
                                            <br>
                                            <img style="width:300px;height:300px;" class="img-rounded" id="previewimg0" src="">
                                            </div>
                                        </div>
                                        <input name="file" type="file" id="file" onclick="remove()" style="display: none;">
                                    </div>
                                   <div class="col-md-3">
                                        <button type="button" id="rm" class="btn btn-primary" style="color: rgb(255, 255, 255); border-radius: 26px; display: block;" onclick="btnimgaeremovefirst(0)">Remove Image</button>
                                   </div>
                                </div>
                            </div> 
                        </div>
                    </div>

                    
                    
                    
                    <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-3">
                            <button type="button" onclick="editadvertise();" class="btn btn-effect-ripple btn-primary">Submit</button>
                            <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Form Validation Form -->

               
            </div>
            <!-- END Form Validation Block -->
        </div>
    </div>

    

    <!-- Cuisine Details -->

    


    <!-- END Form Validation Content -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script type="text/javascript" src="js/CasseroleService.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $("#loading").hide();
    $("#img").hide();
    $("#vid").hide();
    $("#col").hide();
    $("#divurl").hide();

    var mt = $("#mtype").val();

    var ad_id = $("#adid").val();

    var caption = $("#desc").val();

    var url = $("#url").val();

    var curl = $("#curl").val();

    $("#val-curl").val(curl);

    $("#val-selmedt").val(mt);

    $("#val-desc").val(caption);

    if(mt == 'Video')
    {
        $("#vid").show();

        $("#val-vid0").val(url);
    }

    if(mt == 'Image')
    {   
        $("#img").show(); 

        $("#previewimg0").attr('src',url);
    }

    if(mt == 'Clickad')
    {
        $("#img").show(); 

        $("#previewimg0").attr('src',url);

        $("#divurl").show();
    }    






});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 
//tableadList
    $('#tableclList').dataTable().fnClearTable();
        $('#tableclList').dataTable().fnDraw();
        $('#tableclList').dataTable().fnDestroy();

        $('#tableadList').dataTable().fnClearTable();
        $('#tableadList').dataTable().fnDraw();
        $('#tableadList').dataTable().fnDestroy();

       });</script>

<script type="text/javascript">
    
    function remove()
    {
        $("#rm").css("display","block");
    }

</script>

<script type="text/javascript">
    
    function btnimgaeremovefirst(p1)
    {
        console.log("Par"+p1);
        
        var divd = '';

        divd += '<div id="filediv'+p1+'">'+
                               
                               '<div class="row">'+
                                 
                                   '<div class="col-md-9">'+
                                   '<input name="file[]" type="file" id="file" onclick="remove()" />'+
                                   '</div>'+
                                   '<div class="col-md-3">'+
                                   '<button type="button" id="rm" class="btn btn-primary" style="color: #ffffff;border-radius: 26px;display:none" onclick="btnimgaeremovefirst(0)">Remove Image</button>'+ 
                                   '</div>'+
                                   
                               '</div>'+
                               
                                
                          
                '</div>';

        $("#filediv").html(divd);        


    }

</script>

<script type="text/javascript">
    
    function editadvertise()
    {
        console.log("Edit Advertise");

        var adid = '';

        var mtype = '';

        var caption = '';

        var url = '';

        var vld = 0;

        var curl = '';

        curl = $("#val-curl").val();

        adid = $("#adid").val();

        mtype = $("#mtype").val();

        caption = $("#val-desc").val();

        if(mtype == 'Image')
        {
            if($("#previewimg0").length != 0)
            {
                url = document.getElementById("previewimg0").src;
            }        
        }

        if(mtype == 'Video')
        {
            url = $("#val-vid0").val();
        }

        if(mtype == 'Clickad')
        {
            if($("#previewimg0").length != 0)
            {
                url = document.getElementById("previewimg0").src;
            }

            if(curl == '')
            {
                alert('Please Enter Url');

                return false;
            }    
        }    

        console.log("Adid :"+adid+"Mtype :"+mtype+"Caption :"+caption +"Url :"+url + "Curl"+curl);

       // return false;

        if(url == '' || caption == '' || mtype == '')
        {
            alert('Please fill all required fields..!!');

            return false;
        }
        else
        {
            var reqea = {"adid":adid,"mtype":mtype,"caption":caption,"url":url,"curl":curl};

            $.ajax({
                     url:'advertise_service.php?servicename=Edit-Advertise',
                     type: 'POST',
                     datatype: 'JSON',
                     data: JSON.stringify(reqea),
                     success: function(rsea)
                     {
                        console.log("EDit"+JSON.stringify(rsea));

                        var ed = JSON.parse(rsea);

                        if(ed.status == 'success')
                        {
                            window.location.href = 'advertise.php';
                        }    
                     }   

            });


        }    

    }

</script>

<?php include 'inc/template_end.php'; ?>