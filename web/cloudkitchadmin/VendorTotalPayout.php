<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Vendor Total Payment History</h1>
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">


<!-- <br><br> -->
    <!-- <button type="button" onclick="exporttoexcel()" class="btn btn-rounded btn-success" style="float:right">Export</button> -->
    <button type="button" id="exptexcel" class="btn btn-rounded btn-success" style="float:right">Export</button>
    <br><br>
        <div class="col-sm-10 col-md-12 col-lg-12">
        <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
            <!-- Form Validation Block -->
            <table id ="tablevendortotalpayout" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Vendor Name</th>
                                                 <th>Total</th>
                                                 </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->



<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>


<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

        $('#tablevendortotalpayout').dataTable().fnClearTable();
        $('#tablevendortotalpayout').dataTable().fnDraw();
        $('#tablevendortotalpayout').dataTable().fnDestroy();

        // $('#tableOrderItemList').dataTable().fnClearTable();
        // $('#tableOrderItemList').dataTable().fnDraw();
        // $('#tableOrderItemList').dataTable().fnDestroy();

        
    // var type = '<?php 
    //echo $_SESSION['info']['type']; ?>';
    // var wedcode = '';
    // wedcode = '2616';
    // console.log("My Type Is :"+type);
    // if(type == "admin")
    // {
    //     getWeddingList();
    // }

    //invitationlist();
    //customerlist();
    vendortotalpayhistory();
    // if(type == "subadmin") 
    // {
    //    getWeddingListBySubadmin(wedcode); 
    // }   
    });</script>

    <script type="text/javascript">
      
        
    function vendortotalpayhistory()
    {
       var invitecode = '';

       invitecode = $("#invitecode").val();

       var reqhc = {"invitecode":invitecode};

        $("#loading").show();
        $.ajax({
                 url: 'vendortotalpay-service.php?service_name=vendortotalpayout',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(reqhc),
                 async: false,
                 success: function (col)
                 {
                    $("#loading").hide();
                     console.log("Cheforders Response"+JSON.stringify(col));

                     var rscol = JSON.parse(col);

                     var co = new Array();

                     for(var oc=0;oc<rscol.vendors.length;oc++)
                     {
                        co[oc] = new Array();

                        co[oc][0] = oc+1;

                        co[oc][1] = rscol.vendors[oc].vendorname;

                        co[oc][2] = rscol.vendors[oc].total;
                                                                                         
                     }

                    $('#tablevendortotalpayout').dataTable({
                        "aaData": co,
                        "bDestroy": true,
                        "autoWidth": true
                       });   
                 }    
        });
    }

    </script>



    <script type="text/javascript">
        
        $(document).ready(function() {

          $("#frmtm").val('');
          $("#totm").val('');
  // $("#exptexcel").click(function(e) {
  //   e.preventDefault();

  //   //getting data from our table
  //   var data_type = 'data:application/vnd.ms-excel';
  //   var table_div = document.getElementById('tableChefphList');
  //   var table_html = table_div.outerHTML.replace(/ /g, '%20');

  //   var a = document.createElement('a');
  //   a.href = data_type + ', ' + table_html;
  //        var dt = new Date();
  //       var day = dt.getDate();
  //       var month = dt.getMonth() + 1;
  //       var year = dt.getFullYear();
  //       var hour = dt.getHours();
  //       var mins = dt.getMinutes();
  //       var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
  //   a.download = 'MyTree_' + postfix + '.xls';
  //   a.click();
  // });

  function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;
    // CSV FILE
    csvFile = new Blob([csv], {type: "text/csv"});
    // Download link
    downloadLink = document.createElement("a");
    // File name
    downloadLink.download = filename;
    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);
    // Make sure that the link is not displayed
    downloadLink.style.display = "none";
    // Add the link to your DOM
    document.body.appendChild(downloadLink);
    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("table tr");
    
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText);        
        csv.push(row.join(","));       
    }
    // Download CSV
    download_csv(csv.join("\n"), filename);
}

document.querySelector("#exptexcel").addEventListener("click", function () {
    var html = document.querySelector("table").outerHTML;
    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    filename = 'MyTree_Reported_ChefPayout' + postfix + '.csv';
    export_table_to_csv(html, filename);
});
});


    </script>

    