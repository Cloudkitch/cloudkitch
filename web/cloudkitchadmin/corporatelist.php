<?php 
session_start();
include 'inc/config.php'; 

if(!isset($_SESSION['info']['user']))
{
	header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">

  <div id="loading" style="position:fixed;left: 50%;top: 50%;">
    <img id="loading-image" src="image/loading.gif" alt="Loading..." />
  </div>
<!-- Validation Header -->
  <div class="content-header">
    <div class="row">
      <div class="col-sm-6">
        <div class="header-section">
          <h1>Corporate List</h1>
          <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
        </div>
      </div>
      <!-- <div class="col-sm-6">
      <div class="header-section">
        <a href="add_corporate.php" style="width: 150px;float: right;" class="btn btn-block btn-primary">
          <i class="fa fa-plus"></i> Add Corporate
        </a>
      </div>
    </div> -->
  </div>
  </div>
  <!-- END Validation Header -->

  

  <div class="row" id="corporatelistdiv">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <!-- Form Validation Block -->
      <table id ="tableChefList" class="table table-vcenter table-striped table-hover table-borderless">
        <thead>
        <tr>
          <th>Sr</th>
          <th>Corprate Name</th>
          <th>Corprate Code</th>
          <th>Corporate Logo</th>
          <!-- <th>Action</th> -->
        </tr> 
        </thead>

      </table>
    <!-- END Form Validation Block -->
    </div>
  </div>

  <div class="row" id="cuisinelistdiv" style="display: none">
    <div class="col-sm-12 col-md-12 col-lg-12">
    <!-- Form Validation Block -->
    <table id ="tableCuisineList1" class="table table-vcenter table-striped table-hover table-borderless">
      <thead>
       <tr>
         <th>Sr</th>
         <th>Cuisine Name</th>
         <th>Cuisine Pic</th>
         <th>Category</th>
         <th>Page category</th>
         <th>Price</th>
         <th>Status</th>
         <!-- <th>Action</th> -->
       </tr> 
      </thead>
    </table>
    </div>
  </div>

  <div class="row" id="corporatemeallistdiv" style="display: none">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div class="header-section">
        <h3>Team meals</h3>
      </div>
      <table id ="tableCuisineList" class="table table-vcenter table-striped table-hover table-borderless">
        <thead>
        <tr>
          <th>Sr</th>
          <th>Cuisine Name</th>
          <th>Cuisine Pic</th>
          <th>Category</th>
          <th>Home page category</th>
          <th>Price</th>
          <th>Minimum Order Limit</th>
          <th>Lead Time</th>
          <!-- <th>Action</th> -->
        </tr> 
        </thead>
      </table>
    <!-- END Form Validation Block -->
    </div>
  </div>
  <br>
  <br>

  <div class="row" id="eventmeallistdiv" style="display: none">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div class="header-section">
        <h3>Event meals</h3>
      </div>
      <!-- Form Validation Block -->
      <table id ="tableeventCuisineList" class="table table-vcenter table-striped table-hover table-borderless">
        <thead>
        <tr>
          <th>Sr</th>
          <th>Cuisine Name</th>
          <th>Cuisine Pic</th>
          <th>Category</th>
          <th>Home page category</th>
          <th>Price</th>
          <th>Minimum Order Limit</th>
          <th>Lead Time</th>
          <!-- <th>Action</th> -->
        </tr> 
      </thead>
      </table>
    <!-- END Form Validation Block -->
    </div>
  </div>
  <br>
  <br>
  <div class="row" id="cafeteriameallistdiv" style="display: none">
    <div class="col-sm-12 col-md-12 col-lg-12">
    <div class="header-section">
        <h3>Cafeteria meals</h3>
      </div>
      <!-- Form Validation Block -->
      <table id ="tablecafeteriaCuisineList" class="table table-vcenter table-striped table-hover table-borderless">
        <thead>
        <tr>
          <th>Sr</th>
          <th>Cuisine Name</th>
          <th>Cuisine Pic</th>
          <th>Category</th>
          <th>Home Page Category</th>
          <th>Price</th>
          <th>Status</th>
          <?php 
         if($role != 2){
           echo " <th>Action</th>";
         }
        ?>        </tr> 
      </thead>

    </table>
    <!-- END Form Validation Block -->
  </div>
  </div>

</div>


<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#tableChefList tr[role='row']").on("click",function() {
        alert($(this).find("td:first-child").text());
    });

    $("#loading").hide();
    getCorporate();
   
  });
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 
});
</script>
<script>

    $('#tableChefList').on('click','tr',function() {
          $('#loading').show();
          var table  = $(document.getElementById('tableChefList')).DataTable();
          var selectedItem = table.rows( this ).data()[0][0];
          var ids = "#corpo-"+selectedItem;
          var corporate_id = $(ids).html();
          // alert(corporate_id);
          cuisinelist(corporate_id)
          corporatemeal(corporate_id);
          eventmeal(corporate_id);
          cafeteria(corporate_id);
      });

  function getCorporate()
  {
    $('#loading').show();
    $.ajax({
      type: "POST",
      url : "service.php?servicename=getCorporate",
      datatype: "JSON",
      success: function(data)
      {
        $('#loading').hide();
        var cusl = JSON.parse(data);
        var customer = new Array();
        for(var c=0;c<cusl.corporate.length;c++)
        {
          customer[c] = new Array();
          customer[c][0] = c+1;
          customer[c][1] = cusl.corporate[c].corporatename;
          customer[c][2] = cusl.corporate[c].corporatecode;
          var logo = "<span style='display:none;' id='corpo-"+ parseInt(c+1) +"'>" + cusl.corporate[c].id +"</span>";
          if(cusl.corporate[c].corporatelogo != null){
            logo += "<img class='img-thumbnail' src='"+cusl.corporate[c].corporatelogo+"' style='width:100px;'/>"; 
          }
          customer[c][3] = logo ;
        //   customer[c][4] = '<div class=""> <a href="edit_corporate.php?id='+cusl.corporate[c].id+'" class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Edit"> <i class="fa fa-pencil"></i> </a>&nbsp;&nbsp <button type="button" onclick = "deletedata('+cusl.corporate[c].id+');" class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Delete"> <i class="fa fa-fw fa-times"></i> </button> </div>';
          
        }  
        $('#tableChefList').dataTable({
          "aaData": customer,
          "scrollX": true,
          "bDestroy": true
        });
      }
    });
  }
  function deletedata(id)
  {
    swal({
                title: "Are You sure want to delete this?",
                //text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                      var request = {"id":id};
                      $.ajax({
                        url: 'service.php?servicename=deleteCorporate',
                        type: 'POST',
                        data: JSON.stringify(request),
                        contentType: 'application/json; charset=utf-8',
                        datatype: 'JSON',
                        async: true,
                        success: function(data)
                        {
                          var result = JSON.parse(data);
                          if(result.status == 'success')
                          {
                            getCorporate();
                            $("#toast-error").html(result.msg);
                            $("#toasterError").fadeIn();
                          }
                          else
                          {
                            $("#toast-error").html(result.msg);
                            $("#toasterError").fadeIn();
                          }
                          setTimeout(function(){
                            $("#toaster").fadeOut();
                            $("#toasterError").fadeOut();
                          }, 3000);
                        }
                      });
                    } 
              });

  }

  function corporatemeal(corporateid)
  {
    var reqc = {"id":"1","corporateid":corporateid};
    $('#loading').show();
    $.ajax({
      type: "POST",
              //cache: false,
              url: "service.php?servicename=corporatemeals",
              datatype : "JSON",
              data: JSON.stringify(reqc),
              async: false,
            success: function(rclist)
            {
              $('#loading').hide();
              var cl = '';

              cl = JSON.parse(rclist);
              if(cl.cuisinesd.length<=0 || cl.status == "failure"){
                return false;
              }
              $("#corporatelistdiv").hide();
              $("#corporatemeallistdiv").show();
              var culi = new Array();

              for(var d=0;d<cl.cuisinesd.length;d++)
              {
                culi[d] = new Array();

                culi[d][0] = d+1;
                culi[d][1] = cl.cuisinesd[d].cuisinename;
                culi[d][2] = "<img class='img-thumbnail' src='"+cl.cuisinesd[d].cuisinepic+"' style='width:100px;'/>";
                culi[d][3] = cl.cuisinesd[d].cuisinetype;
                culi[d][4] = cl.cuisinesd[d].homepagetype;
                culi[d][5] = cl.cuisinesd[d].price;
                culi[d][6] = cl.cuisinesd[d].minorderquantity;
                culi[d][7] = cl.cuisinesd[d].leadtime;
                // culi[d][8] = '<div class=""> <a href="edit_corporate_meal.php?id='+cl.cuisinesd[d].cuisineid+'" class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Edit"> <i class="fa fa-pencil"></i> </a>&nbsp;&nbsp <button type="button" onclick = "deletedata('+cl.cuisinesd[d].cuisineid+');" class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Delete"> <i class="fa fa-fw fa-times"></i> </button> </div>'; 
            }  

            $('#tableCuisineList').dataTable({
              "aaData": culi,
              "scrollX": true,
              "bDestroy": true
            });
          }

        });
  }

  function eventmeal(corporateid)
  {
      var reqc = {"id":"1","corporateid":corporateid};
      $('#loading').show();
      $.ajax({
          type: "POST",
                  //cache: false,
                  url: "service.php?servicename=eventmeals",
                  datatype : "JSON",
                  data: JSON.stringify(reqc),
                  async: false,
              success: function(rclist)
              {
                  $('#loading').hide();                  
                  var cl = '';
                  cl = JSON.parse(rclist);
                  if(cl.cuisinesd.length<=0 || cl.status == "failure"){
                    return false;
                  }
                  $("#corporatelistdiv").hide();
                  $("#eventmeallistdiv").show();
                  var culi = new Array();

                  for(var d=0;d<cl.cuisinesd.length;d++)
                  {
                  culi[d] = new Array();

                  culi[d][0] = d+1;
                  culi[d][1] = cl.cuisinesd[d].cuisinename;
                  culi[d][2] = "<img class='img-thumbnail' src='"+cl.cuisinesd[d].cuisinepic+"' style='width:100px;'/>";
                  culi[d][3] = cl.cuisinesd[d].cuisinetype;
                  culi[d][4] = cl.cuisinesd[d].homepagetype;
                  culi[d][5] = cl.cuisinesd[d].price;
                  culi[d][6] = cl.cuisinesd[d].minorderquantity;
                  culi[d][7] = cl.cuisinesd[d].leadtime;
                  // culi[d][8] = '<div class=""> <a href="edit_event_meal.php?id='+cl.cuisinesd[d].cuisineid+'" class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Edit"> <i class="fa fa-pencil"></i> </a>&nbsp;&nbsp <button type="button" onclick = "deletedata('+cl.cuisinesd[d].cuisineid+');" class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Delete"> <i class="fa fa-fw fa-times"></i> </button> </div>'; 
              }  

              $('#tableeventCuisineList').dataTable({
                  "aaData": culi,
                  "scrollX": true,
                  "bDestroy": true
              });
              }

          });
  }

  function cafeteria(corporateid)  
{
  var reqc = {"id":"1","corporateid":corporateid};
  $('#loading').show();

  $.ajax({
    type: "POST",
            //cache: false,
            url: "service.php?servicename=cafeteria",
            datatype : "JSON",
            data: JSON.stringify(reqc),
            async: false,
           //contentType : 'application/json',
           success: function(rclist)
           {
             
             $('#loading').hide();             
             var cl = '';

             cl = JSON.parse(rclist);
             if(cl.cuisinesd.length<=0 || cl.status == "failure"){
                return false;
              }
             $("#corporatelistdiv").hide();
             $("#cafeteriameallistdiv").show();
             var culi = new Array();

             for(var d=0;d<cl.cuisinesd.length;d++)
             {
              culi[d] = new Array();

              culi[d][0] = d+1;
              culi[d][1] = cl.cuisinesd[d].cuisinename;
              culi[d][2] = "<img class='img-thumbnail' src='"+cl.cuisinesd[d].cuisinepic+"' style='width:100px;'/>";
              culi[d][3] = cl.cuisinesd[d].cuisinetype;
              culi[d][4] = cl.cuisinesd[d].homepagetype;
              culi[d][5] = cl.cuisinesd[d].price;
              if(cl.cuisinesd[d].isActive == 'Y')
              {
               culi[d][6] = "<button type='button' onclick='inactivecuisine("+cl.cuisinesd[d].cuisineid+");' class='btn btn-rounded btn-primary'>Active</button>";
             }
             else
             {
               culi[d][6] = "<button type='button' class='btn btn-rounded btn-primary'>InActive</button>";
             } 
            //  culi[d][7] = '<div class=""> <a href="edit_cafeteria.php?id='+cl.cuisinesd[d].cuisineid+'" class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Edit"> <i class="fa fa-pencil"></i> </a>&nbsp;&nbsp <button type="button" onclick = "deletedata('+cl.cuisinesd[d].cuisineid+');" class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Delete"> <i class="fa fa-fw fa-times"></i> </button> </div>'; 
           }  

           $('#tablecafeteriaCuisineList').dataTable({
            "aaData": culi,
            "scrollX": true,
            "bDestroy": true
          });
         }

       });
}

function cuisinelist(corporateid)
{
  var reqc = {"id":"1","corporateid":corporateid};
  $('#loading').show();
  $.ajax({
    type: "POST",
            //cache: false,
            url: "service.php?servicename=cuisines",
            datatype : "JSON",
            data: JSON.stringify(reqc),
            async: false,
           //contentType : 'application/json',
           success: function(rclist)
           {
             $('#loading').hide();
             
             
             var cl = '';

             cl = JSON.parse(rclist);
             if(cl.cuisinesd.length<=0 || cl.status == "failure"){
                return false;
            }
             $("#corporatelistdiv").hide();
             $("#cuisinelistdiv").show();

             var culi = new Array();

             for(var d=0;d<cl.cuisinesd.length;d++)
             {
              culi[d] = new Array();

              culi[d][0] = d+1;
              culi[d][1] = cl.cuisinesd[d].cuisinename;
              culi[d][2] = "<img class='img-thumbnail' src='"+cl.cuisinesd[d].cuisinepic+"' style='width:100px;'/>";
              culi[d][3] = cl.cuisinesd[d].cuisinetype;
              culi[d][4] = cl.cuisinesd[d].homepagetype;
              culi[d][5] = cl.cuisinesd[d].price;
              if(cl.cuisinesd[d].isActive == 'Y')
              {
               culi[d][6] = "<button type='button' onclick='inactivecuisine("+cl.cuisinesd[d].cuisineid+");' class='btn btn-rounded btn-primary'>Active</button>";
             }
             else
             {
               culi[d][6] = "<button type='button' class='btn btn-rounded btn-primary'>InActive</button>";
             } 
            //  culi[d][7] = '<div class=""> <a href="edit_cuisines.php?id='+cl.cuisinesd[d].cuisineid+'" class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Edit"> <i class="fa fa-pencil"></i> </a>&nbsp;&nbsp <button type="button" onclick = "deletedata('+cl.cuisinesd[d].cuisineid+');" class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Delete"> <i class="fa fa-fw fa-times"></i> </button> </div>'; 
           }  

           $('#tableCuisineList1').dataTable({
            "aaData": culi,
            "bDestroy": true,
            "scrollX": true
          });
         }

       });
}

</script>