<?php 

session_start();

if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/MIHAdmin/index.php");
	header("Location: https://coludkitch.co.in/cloudkitchadmin/index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Create Dish</h1>
                </div>
            </div>
           
        </div>
    </div>

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <div class="block">
                <!-- Form Validation Title -->
                <div class="block-title">
                    <h2>Create Dish Form</h2>
                </div>
                <!-- END Form Validation Title -->

                <!-- Form Validation Form -->
                
                <!--event_start_time,event_end_time,address,info-->
                <form id="createdish" action="adddish.php"  method="post" class="form-horizontal form-bordered">
                    
                   <div class="form-group" id="selectWedding">
                        <label class="col-md-3 control-label" for="val-selectWedding">Select Cuisine Category<span class="text-danger">*</span></label>
                        <div class="col-md-6" >
                           <select id="val-selectcuicat" name="val-selectcuicat" class="form-control">
                               <option value="">select category</option>
                            </select>
                        </div>
                    </div> 
                    
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-username">Dish Name<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-dishName" name="val-eventName" class="form-control" placeholder="Please enter Dish name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-username">Type<span class="text-danger">*</span></label>
                         <div class="col-md-6" >
                           <select id="val-type" name="val-type" class="form-control">
                               <option value="">select type</option>
                               <option value="veg">Veg</option>
                               <option value="nonveg">Non-Veg</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-username">Dish Image<span class="text-danger">*</span></label>

                        <div class="col-md-6">
                            <div class="col-md-6">
                            <img src="" id="dishImageView" name="dishImageView" style=""/>
                            </div>

                            <input type="file" onchange="loadDishImage(event);" id="val-eventImage" name="val-eventImage" class="form-control" placeholder="Please enter eventImage">
                        </div>
                    </div>
                    
                    
<!--
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-username">Wedding Code<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="number" id="val-weddingCode" name="val-weddingCode" class="form-control" placeholder="Pleade enter wedding code">
                        </div>
                        <span id="user-result" style="width:90%;vertical-align: -webkit-baseline-middle;"></span>
                    </div>
-->
                    
                    <!-- <div class="form-group">
                        <label class="col-md-3 control-label" for="val-username">Event Date<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="example-datepickerForEvent" name="example-datepickerForEvent" class="form-control input-datepicker" data-date-format="yyyy-dd-mm" placeholder="yyyy-dd-mm">
                        </div>
                    </div> -->
                    
                    <!-- <div class="form-group">
                        <label class="col-md-3 control-label" for="val-eventstarttime">Event Start Time<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group bootstrap-timepicker">
                                <input type="text" id="example-timepicker-start" name="example-timepicker-start" class="form-control input-timepicker">
                                <span class="input-group-btn">
                                <a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary"><i class="fa fa-clock-o"></i></a>
                                </span>
                            </div>

                        </div>
                    </div> -->
                    
                   <!--  <div class="form-group">
                        <label class="col-md-3 control-label" for="val-eventendtime">Event End Time<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group bootstrap-timepicker">
                                <input type="text" id="example-timepicker-end" name="example-timepicker-end" class="form-control input-timepicker">
                                <span class="input-group-btn">
                                <a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary"><i class="fa fa-clock-o"></i></a>
                                </span>
                            </div>

                        </div>
                    </div> -->
                    
<!--
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-username">Bridal  Name<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-bridalName" name="val-bridalName" class="form-control" placeholder="Please enter bridal">
                        </div>
                    </div>
<textarea id="example-textarea-input" name="example-textarea-input" rows="7" class="form-control" placeholder="Description.."></textarea>
-->
                        <!-- <div class="form-group">
                        <label class="col-md-3 control-label" for="val-address">Address<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <textarea   id="val-address" name="val-address" class="form-control" placeholder="Please enter address"> </textarea>
                        </div>
                        </div> -->
                    
                        <!-- <div class="form-group">
                        <label class="col-md-3 control-label" for="val-info">Dress Code</label>
                        <div class="col-md-6">
                            <input type="text" id="val-info" name="val-info" class="form-control" placeholder="Please enter information">
                        </div>
                        </div> -->
                    
                    
                     
                    
                    
                    
                    
                    
                    <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-3">
                            <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button>
                            <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Form Validation Form -->

                <!-- Terms Modal -->
                <div id="modal-terms" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title text-center"><strong>Terms and Conditions</strong></h3>
                            </div>
                            <div class="modal-body">
                                <h4 class="page-header">1. <strong>General</strong></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor.</p>
                                <h4 class="page-header">2. <strong>Account</strong></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor.</p>
                                <h4 class="page-header">3. <strong>Service</strong></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor.</p>
                                <h4 class="page-header">4. <strong>Payments</strong></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor.</p>
                            </div>
                            <div class="modal-footer">
                                <div class="text-center">
                                    <button type="button" class="btn btn-effect-ripple btn-primary" data-dismiss="modal">I've read them!</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Terms Modal -->
            </div>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/CasseroleService.js"></script>


<script src="js/pages/formsValidationCreateEvent.js"></script>
<script>$(function() { FormsValidation.init(); 
                     cuisinecategory();
                     });</script>
<script type="text/javascript">
$(document).ready(function() {

    $("#loading").hide();
    var x_timer;    
    $("#val-weddingCode").keyup(function (e){
        clearTimeout(x_timer);
        var user_name = $(this).val();
        x_timer = setTimeout(function(){
            check_weddingCode_ajax(user_name);
        }, 1000);
    }); 


});
</script>
<?php include 'inc/template_end.php'; ?>