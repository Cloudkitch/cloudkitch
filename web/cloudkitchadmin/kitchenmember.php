<?php 

session_start();
include 'inc/databaseConfig.php';
include 'inc/config.php'; 
if(!isset($_SESSION['info']['user']))
{
	header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
include 'inc/template_start.php';
include 'inc/page_head.php';
?>
<style type="text/css">
    .working-days .custom-checkbox
    {
        display: inline-block;
        margin-left: 10px;
    }
</style>
<div id="page-content">
  <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
  <img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1>Add Member</h1>
    </div>
</div>
</div>
</div>
<div class="row">
    <div class="col-sm-10 col-md-12 col-lg-12">
        <div class="block">
            <form id="createadmin" name="createadmin" class="form-horizontal form-bordered">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="val_nname">Name<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <input type="text" id="val_name" name="val_name" class="form-control" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="val_email">Email address<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <input type="text" id="val_email" name="val_email" class="form-control" placeholder="">
                    </div>
                </div> 
                <div class="form-group">
                    <label class="col-md-3 control-label" for="val_password">Password<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <input type="password" id="val_password" name="val_password" class="form-control" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="val_password">Role<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <select id="role" name="role" class="form-control">
                            <option value="1">Edit</option>
                            <option value="2">View</option>
                        </select>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-3">
                        <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button>
                        <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<!-- END Page Content -->
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<!-- Load and execute javascript code used only in this page -->
<script src="js/CasseroleService.js"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- <script>
    $(function() { FormsValidation.init();
        invitecodedetail();
    });
</script> -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#loading").hide();
    });
    $("form[name='createadmin']").validate({
        rules: {
            val_name : "required",
            val_email : "required",
            val_password : "required",
            role : "required",
        },
        submitHandler: function(form) {
            $(this).find('button[type=submit]').prop('disabled', true);
            $(".preloader").show();
            var data = new FormData($('#createadmin')[0]);
            $.ajax({
                url: 'service.php?servicename=addmember',
                type: 'POST',
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                data: data, 
                success: function(data)
                {
                 $(".preloader").hide();
                 var result = JSON.parse(data);
                 if(result.status == 'success')
                 {
                    window.location.href = 'memberlist.php';
                }
                else
                {
                    $("#toast-error").html(result.msg);
                    $("#toasterError").fadeIn();
                }
                setTimeout(function(){
                    $("#toasterError").fadeOut();
                }, 3000);
            }
        });
        }
    });
</script>
<?php include 'inc/template_end.php'; ?>

