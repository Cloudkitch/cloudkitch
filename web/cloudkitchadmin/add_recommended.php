<?php 

session_start();
include 'inc/databaseConfig.php';
include 'inc/config.php'; 
if(!isset($_SESSION['info']['user']))
{
	header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
include 'inc/template_start.php';
include 'inc/page_head.php';
?>
<link href="css/summernote.css" rel="stylesheet">
<style type="text/css">
    .img-close{
    position: absolute;
    cursor: pointer;
}
</style>
<div id="page-content">
  <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
  <img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1>Add Recommended Cuisine</h1>
    </div>
</div>
</div>
</div>
<div class="row">
    <div class="col-sm-10 col-md-12 col-lg-12">
        <div class="block">
            <form id="createcuisines" name="createcuisines" class="form-horizontal form-bordered">
                
            <div class="form-group">
                <label class="col-md-3 control-label" for="recommandedto">Select Cuisine<span class="text-danger">*</span></label>
                <div class="col-md-6">
                
                  <select class="form-control selectpicker" id="recommandedto" data-live-search="true" name="val_recommandedto">
                    <?php
                    $userid = $_SESSION['info']['id'];
                        $query = "SELECT * FROM cuisine WHERE status='1' AND isCorporateMeal='0' AND isEventMeal='0' AND userid='$userid' ORDER BY cuisinename ASC";
                        $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                        while($row = mysqli_fetch_assoc($result))
                        {
                            echo '<option value="'.$row['cuisineid'].'">'.$row['cuisinename'].' - '.$row['type'].' - '.$row['price'].'</option>';
                        }   
                    ?> 
                    </select>
                   
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="recommandeddish">Select Recommands<span class="text-danger">*</span></label>
                <div class="col-md-6">
                
                  <select class="form-control selectpicker" id="recommandeddish" data-live-search="true" data-max-options="2" multiple="multiple" name="val_recommandeddish[]">
                    <?php
                    $userid = $_SESSION['info']['id'];
                        $query = "SELECT * FROM cuisine WHERE status='1' AND isCorporateMeal='0' AND isEventMeal='0' AND userid='$userid' ORDER BY cuisinename ASC";
                        $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                        while($row = mysqli_fetch_assoc($result))
                        {
                            echo '<option value="'.$row['cuisineid'].'">'.$row['cuisinename'].' - '.$row['type'].' - '.$row['price'].'</option>';
                        }   
                    ?> 
                    </select>
                   
                </div>
            </div>
             <!-- <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinesname">Cuisine name<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="text" id="val_cuisinesname" name="val_cuisinesname" class="form-control" placeholder="">
                </div>
            </div>
             <div class="form-group">
                <label class="col-md-3 control-label" for="image">Cuisine Images<span class="text-danger"></span></label>
                <div class="col-md-6">
                    <input type="hidden" id="base64" name="base64">
                    <input type="file" id="image" name="image" onchange="previewImage(this)" multiple>
                    <div id="cuisinesImg"></div>
                </div>
            </div>
           
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinesprice">Price<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="number" id="val_cuisinesprice" name="val_cuisinesprice" class="form-control" min="1" placeholder="">
                </div>
            </div> -->
           
           
            <div class="form-group form-actions">
                <div class="col-md-8 col-md-offset-3">
                    <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button>
                    <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
<!-- END Page Content -->
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Load and execute javascript code used only in this page -->
<script src="js/CasseroleService.js"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/summernote.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
<!-- <script>
    $(function() { FormsValidation.init();
        invitecodedetail();
    });
</script> -->

    <script type="text/javascript">
        $(document).ready(function() {
            $("#loading").hide();
           
        });
        function previewImage(input) {
            if (input.files) {
                var filesAmount = input.files.length;
                for (var i = 0; i < filesAmount; i++)   {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        var src = event.target.result;
                        $("#cuisinesImg").append('<div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;padding-left:5px;" onclick = "imageClose(this)">X</div><img  src="'+src+'" style="height:100px;margin-right:5px;margin-top:5px;" class="img-thumbnail cuisine-img"></div>');
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }
        function imageClose(a)
        {
            $(a).parent().remove();
        }
    $("form[name='createcuisines']").validate({
        rules: {
            val_recommandedto:"required",
            val_recommandeddish:"required",
        },
        submitHandler: function(form) {
            $(this).find('button[type=submit]').prop('disabled', true);
            $(".preloader").show();
            var images = "";
            $( ".cuisine-img" ).each(function() {
                var image = $(this).attr("src");
                images += "###"+image;
            });
            $("#base64").val(images);
           
            var data = new FormData($('#createcuisines')[0]);
            $.ajax({
                url: 'service.php?servicename=addrecommendedcuisine',
                type: 'POST',
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                data: data, 
                success: function(data)
                {
                     $(".preloader").hide();
                     var result = JSON.parse(data);
                     if(result.status == 'success')
                     {
                        window.location.href = 'recommendedlist.php';
                     }
                     else
                     {
                        $("#toast-error").html(result.msg);
                        $("#toasterError").fadeIn();
                     }
                      setTimeout(function(){
                        $("#toasterError").fadeOut();
                  }, 3000);
                }
            });
        }
    });

   
   
    </script>
    <?php include 'inc/template_end.php'; ?>
   

