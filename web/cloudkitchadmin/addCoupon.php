<?php 

session_start();


if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/Casseroleadmin/index.php");
	header("Location: https://coludkitch.co.in/cloudkitchadmin/index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
// include 'inc/config.php'; $template['header_link'] = 'WELCOME ';
 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Validation Header -->
    <!-- <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Create Invitation Code</h1>
                </div>
            </div>
           
        </div>
    </div> -->

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <div class="block">
                <!-- Form Validation Title -->
                <div class="block-title">
                    <h2>Create Coupon Code</h2>
                </div>
                <!-- END Form Validation Title -->

                <!-- Form Validation Form -->
                
                <!--event_start_time,event_end_time,address,info-->
                <form id="createCouponCode" action="addCoupon.php"  method="post" class="form-horizontal form-bordered">

                <div class="form-group">
                        <label class="col-md-3 control-label" for="val-cpcode">Coupon Code<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-cpcode" name="val-cpcode" class="form-control" placeholder="Please Enter Coupon Code">
                        </div>
                        <span id="user-result-coupon" style="width:90%;vertical-align: -webkit-baseline-middle;"></span>
                    </div>
                    
                                     
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-disc">Discount(In percentage)<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="number" id="val-disc" name="val-disc" class="form-control" placeholder="Please Enter Discount Amount">
                        </div>
                        
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-disc">Valid<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="date" id="val-date" name="val-date" class="form-control" placeholder="" min="<?php echo date("Y-m-d");?>">
                        </div>
                        
                    </div>

                    <input type="hidden" name="email" id="email" value="<?php echo $_SESSION['info']['email']; ?>" >
                    <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-3">
                            <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button>
                            <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Form Validation Form -->

                
            </div>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/CasseroleService.js"></script>


<script src="js/pages/formsValidationCreateInviteCode.js"></script>
<script>$(function() { FormsValidation.init(); });</script>
<script type="text/javascript">
$(document).ready(function() {
    $("#loading").hide();
    var x_timer;    
    $("#val-cpcode").keyup(function (e){
        clearTimeout(x_timer);
        var ccode = $(this).val();
        console.log("CouponCOde"+ccode);
        x_timer = setTimeout(function(){
            // check_weddingCode_ajax(user_name);
            check_couponCode(ccode);
        }, 1000);
    }); 


});
</script>
<?php include 'inc/template_end.php'; ?>

