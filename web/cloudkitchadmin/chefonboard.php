<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/Casseroleadmin/index.php");
	header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Chef List</h1>
                    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tableChefList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                   <th>Sr</th>
                                                   <th>Chef Name</th>
                                                   <th>Email</th>
                                                   <th>Phone</th>
                                                   <th>Address</th>
                                                   <th>Action</th>
                                                 </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<!-- <script src="js/CasseroleService.js"></script> -->



<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();

    cheflist();
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    // var type = '<?php 
    //echo $_SESSION['info']['type']; ?>';
    // var wedcode = '';
    // wedcode = '2616';
    // console.log("My Type Is :"+type);
    // if(type == "admin")
    // {
    //     getWeddingList();
    // }

    //invitationlist();
    //customerlist();
    // if(type == "subadmin") 
    // {
    //    getWeddingListBySubadmin(wedcode); 
    // }   
    });</script>

    <script>
      
      function cheflist()
      {

        var invitecode = '';

        invitecode = $("#invitecode").val();

        var reqcl = {"invitecode":invitecode};
 
        $('#loading').show();
          $.ajax({

                  type: "POST",
                  url : "cheflistservice.php?servicename=ChefListNew",
                  datatype: 'JSON',
                  data: JSON.stringify(reqcl),
                 // async: false,
                  success: function(data)
                  {
                    $('#loading').hide();

                          console.log("Customer Data :"+JSON.stringify(data));

                          var cusl = JSON.parse(data);

                         var customer = new Array();

                         for(var c=0;c<cusl.chefs.length;c++)
                         {
                            customer[c] = new Array();

                            customer[c][0] = c+1;
                            customer[c][1] = cusl.chefs[c].chefname;
                            customer[c][2] = cusl.chefs[c].email;
                            customer[c][3] = cusl.chefs[c].phone;
                            customer[c][4] = cusl.chefs[c].address;

                            if(cusl.chefs[c].chefverified == 'Y')
                            {
                               customer[c][5] = '<button style="pointer-events: none;" type="button" class="btn btn-rounded btn-success">Approved</button>'; 
                            }
                            else
                            {
                               customer[c][5] = '<button onclick="approvechef(\''+cusl.chefs[c].chefid+'\')"  type="button" class="btn btn-rounded btn-danger">Requested</button>';  
                            }  
                          }  

                          $('#tableChefList').dataTable({
                              "aaData": customer,
                              "bDestroy": true
                          });
                  }
          });

      }

    </script>

    <script type="text/javascript">
    
    function approvechef(cfid)
    {
       console.log("chefid"+cfid);

       $.ajax({
                  type: "POST",
                  url : "service.php?servicename=Approve-chef",
                  datatype: 'JSON',
                  data: JSON.stringify({"chefid":cfid}),
                  async: false,
                  success: function(rsac)
                  {
                      var rsc = JSON.parse(rsac);

                      if(rsc.status == 'success')
                      {
                         location.reload();
                      }
                      else
                      {
                          alert('Something went wrong..!!');
                      }  
                  }    
       })
    }    


    </script>