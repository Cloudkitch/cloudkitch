<?php //include 'inc/config.php'; $template['header_link'] = 'FORMS'; 

session_start();
if(!isset($_SESSION['info']['user']))
{
     //header("Location: http://localhost:7755/Casseroleadmin/index.php");
	 header("Location: https://coludkitch.co.in/cloudkitchadmin/index.php");
}
 include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user']; 
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>ADD CUISINE</h1>
                </div>
            </div>
           
        </div>
    </div>

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <div class="block">
                <!-- Form Validation Title -->
                <div class="block-title">
                    <h2>ADD POPULAR CUISINE FORM</h2>
                </div>
                <!-- END Form Validation Title -->

                <!-- Form Validation Form -->
                <form id="createpopularcuisine" action="" class="form-horizontal form-bordered">
                    
                    
                    
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-username">Cuisine Name<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-cuisine" name="val-cuisine" class="form-control" placeholder="Please Cuisine Name">
                        </div>
                    </div>

                    <!-- <div class="form-group">
                        <label class="col-md-3 control-label" for="val-username">Invite Code<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-invc" name="val-cuisine" class="form-control" placeholder="Please Enter Invite Code">
                        </div>
                    </div> -->

                    <div class="form-group" id="selectInvite">
                        <label class="col-md-3 control-label" for="val-selectInvitecode">Select Invite Code<span class="text-danger">*</span></label>
                        <div class="col-md-6" >
                           <select id="val-invc" name="val-invc" class="form-control">
                               <option value="">Select Invitecode</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="val-images">Cuisine Images</label>
                        <div class="col-md-6" id="filediv" > 
                           <div id="filediv0">
                               
                               <div class="row">
                                 
                                   <div class="col-md-9">
                                   <input name="file[]" type="file" id="file" onclick="remove()" />
                                   </div>
                                   <div class="col-md-3">
                                    <button type="button" id="rm" class="btn btn-primary" style="color: #ffffff;border-radius: 26px;display:none" onclick="btnimgaeremove(0)">Remove Image</button> 
                                       
                                   </div>
                                   
                               </div>
                               
                                
                          
                          </div> 
                        </div>
                            <div class="col-md-12">
                               <input type="button" id="add_more" class="btn btn-primary" value="Add More Files" style="float:right"/>
                            </div>
                    </div>
                    
                    
                   <!--  <div class="form-group">
                        <label class="col-md-3 control-label" for="val-username">Wedding Code<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="number" id="val-weddingCode" name="val-weddingCode" class="form-control" placeholder="Pleade enter wedding code">
                        </div>
                        <span id="user-result" style="width:90%;vertical-align: -webkit-baseline-middle;"></span>
                    </div> -->

                    <!-- <div class="form-group">
                        <label class="col-md-3 control-label" for="val-email">Email<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="email" id="val-email" name="val-email" class="form-control" placeholder="Pleade enter wedding code">
                        </div>
                        <span id="user-result" style="width:90%;vertical-align: -webkit-baseline-middle;"></span>
                    </div> -->

                   <!--  <div class="form-group">
                        <label class="col-md-3 control-label" for="val-pwd">Password<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="password" id="val-pwd" name="val-pwd" class="form-control" placeholder="Please Enter Password">
                        </div>
                        <span id="user-result" style="width:90%;vertical-align: -webkit-baseline-middle;"></span>
                    </div> -->
                    
                   <!--  <div class="form-group">
                        <label class="col-md-3 control-label" for="val-username">Groom Name<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-groomName" name="val-groomName" class="form-control" placeholder="Please enter groom">
                        </div>
                    </div> -->
                    
                    <!-- <div class="form-group">
                        <label class="col-md-3 control-label" for="val-username">Bridal  Name<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-bridalName" name="val-bridalName" class="form-control" placeholder="Please enter bridal">
                        </div>
                    </div> -->
                    
                    <!-- <div class="form-group">
                        <label class="col-md-3 control-label" for="val-username">Wedding Message<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <textarea id="val-message" name="val-message" class="form-control" placeholder="Please enter bridal"> </textarea>
                        </div>
                    </div> -->
                    
                    
                     
                    
                    
                    
                    
                    
                    <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-3">
                            <button type="button" onclick="uploadImage();" class="btn btn-effect-ripple btn-primary">Submit</button>
                            <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Form Validation Form -->

                <!-- Terms Modal -->
                <div id="modal-terms" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title text-center"><strong>Terms and Conditions</strong></h3>
                            </div>
                            <div class="modal-body">
                                <h4 class="page-header">1. <strong>General</strong></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor.</p>
                                <h4 class="page-header">2. <strong>Account</strong></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor.</p>
                                <h4 class="page-header">3. <strong>Service</strong></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor.</p>
                                <h4 class="page-header">4. <strong>Payments</strong></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor.</p>
                            </div>
                            <div class="modal-footer">
                                <div class="text-center">
                                    <button type="button" class="btn btn-effect-ripple btn-primary" data-dismiss="modal">I've read them!</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Terms Modal -->
            </div>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script type="text/javascript" src="js/CasseroleService.js"></script>


<script src="js/pages/formsValidation.js"></script>

<script>$(function() { 
    FormsValidation.init(); 
    });</script>
<script type="text/javascript">
$(document).ready(function() {
    $("#loading").hide();
    invitecodes();
    var x_timer;    
    $("#val-weddingCode").keyup(function (e){
        clearTimeout(x_timer);
        var user_name = $(this).val();
        x_timer = setTimeout(function(){
           // check_weddingCode_ajax(user_name);
        }, 1000);
    }); 


});
</script>

<script type="text/javascript">
    
    function remove()
    {
        $("#rm").css("display","block");
    }

</script>

<?php include 'inc/template_end.php'; ?>