<?php 

session_start();
include 'inc/databaseConfig.php';
include 'inc/config.php'; 
if(!isset($_SESSION['info']['user']))
{
	header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
include 'inc/template_start.php';
include 'inc/page_head.php';
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
    .img-close{
        position: absolute;
        cursor: pointer;
    }
    #sortable { list-style-type: none; margin: 0; padding: 0; }
    #sortable li { margin: 3px 3px 3px 0; padding: 1px; float: left; text-align: center; }
</style>
<div id="page-content">
  <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
  <img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1>Banners</h1>
    </div>
</div>
</div>
</div>
<div class="row">
    <div class="col-sm-10 col-md-12 col-lg-12">
        <div class="block">
            <form id="banner_form" name="banner_form" class="form-horizontal form-bordered">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="val_nname">Name<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <input type="file" id="banners" name="banners" class="form-control" onchange="loadImageFile(this)" multiple>
                        <span style="color: red;">(Banner should be <b>1200x400</b>)</span>
                        <ul id="sortable">
                         <?php
                         $query = "SELECT * FROM banners";
                         $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                         while($row = mysqli_fetch_assoc($result))
                         {
                            ?>
                            <li class="ui-state-default" style="border:unset;cursor:move;"> <div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;" onclick = "imageClose(this)">X</div><img  src="<?=$row['image']?>" style="height:70px;margin-right:5px;margin-top:5px;" class="img-thumbnail banner-img"></div></li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-8 col-md-offset-3">
                    <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
<!-- END Page Content -->
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Load and execute javascript code used only in this page -->
<script src="js/CasseroleService.js"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#loading").hide();
    });
    $( function() {
        // $( "#bannerImg" ).sortable({ axis: 'y' });
        // $( "#bannerImg" ).disableSelection();

        $( "#sortable" ).sortable();
        $( "#sortable" ).disableSelection();
    } );
    function previewImage(input) {
        if (input.files) {
            var filesAmount = input.files.length;
            for (var i = 0; i < filesAmount; i++)   {
                var reader = new FileReader();
                reader.onload = function(event) {
                    var src = event.target.result;
                    $("#sortable").append('<li class="ui-state-default" style="border:unset;cursor:move;"> <div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;" onclick = "imageClose(this)">X</div><img  src="'+src+'" style="height:70px;margin-right:5px;margin-top:5px;" class="img-thumbnail banner-img"></div></li>');
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }

    var loadImageFile = function (input) {
            var filterType = /^(?:image\/jpeg|image\/jpeg|image\/jpeg|image\/png|image\/x\-icon)$/i;
        
            if (input.files) {
                if (input.files.length === 0) { 
                    return; 
                }
                var filesAmount = input.files.length;
                for (var i = 0; i < filesAmount; i++)   {
                    
                    if (!filterType.test(input.files[i].type)) {
                        alert("Please select a valid image."); 
                        return;
                    }
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL(input.files[i]);

                    fileReader.onload = function (event) {
                    var image = new Image();
                    var type = 'image/jpeg';
                    var  quality = 0.30;
                    image.onload=function(){
                        var canvas=document.createElement("canvas");
                        var context=canvas.getContext("2d");
                        canvas.width=image.width/1;
                        canvas.height=image.height/1;
                        context.drawImage(image,0,0);
                        var imgsrc =  canvas.toDataURL( type, quality );
                        $("#sortable").append('<li class="ui-state-default" style="border:unset;cursor:move;"> <div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;" onclick = "imageClose(this)">X</div><img  src="'+imgsrc+'" style="height:70px;margin-right:5px;margin-top:5px;" class="img-thumbnail banner-img"></div></li>');
                    }
                    image.src=event.target.result;
                    };

                }
            }
        }

    function imageClose(a)
    {
        $(a).parent().parent().remove();
    }
    $("form[name='banner_form']").validate({
        submitHandler: function(form) {
            var images="";
            $( ".banner-img" ).each(function() {
              var src = $( this ).attr('src');
              images = images+"###"+src;
          });
            $(".preloader").show();
            var subCatid = {"images":images};
            $.ajax({
                url: 'service.php?servicename=addbanner',
                type: 'POST',
                data: JSON.stringify(subCatid),
                contentType: 'application/json; charset=utf-8',
                datatype: 'JSON',
                async: true,
                success: function(data)
                {
                 $(".preloader").hide();
                 var result = JSON.parse(data);
                 if(result.status == 'success')
                 {
                    $("#toast-success").html(result.msg);
                    $("#toaster").fadeIn();
                }
                else
                {
                    $("#toast-error").html(result.msg);
                    $("#toasterError").fadeIn();
                }
                setTimeout(function(){
                    $("#toasterError").fadeOut();
                    $("#toaster").fadeOut();
                }, 3000);
            }
        });
        }
    });
</script>
<?php include 'inc/template_end.php'; ?>

