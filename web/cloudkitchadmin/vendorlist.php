<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/Casseroleadmin/index.php");
	header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Vendor List</h1>
                    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    <div class="row">
        <div class="col-sm-10 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tableVendorList" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                   <th>Sr</th>
                                                   <th>Vendor Name</th>
                                                   <th>Email</th>
                                                   <th>Phone</th>
                                                   <th>Address</th>
                                                 </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<!-- <script src="js/CasseroleService.js"></script> -->



<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();

    vendorlist();
});
</script>
<script src="js/pages/uiTables.js"></script>


    <script>
      
    function vendorlist()
  {

  var invitecode = '';

  invitecode = $("#invitecode").val();

  var reqcl = {"invitecode":invitecode};

  $('#loading').show();
    $.ajax({

        type: "POST",
        url : "vendorlistservice.php?servicename=VendorList",
        datatype: 'JSON',
        data: JSON.stringify(reqcl),
       // async: false,
        success: function(data)
        {
          console.log(data);
        $('#loading').hide();

            console.log("vendor Data :"+JSON.stringify(data));

            var cusl = JSON.parse(data);

           var customer = new Array();

           for(var v=0;v<cusl.vendors.length;v++)
           {
            customer[v] = new Array();

            customer[v][0] = v+1;
            customer[v][1] = cusl.vendors[v].vendorname;
            customer[v][2] = cusl.vendors[v].email;
            customer[v][3] = cusl.vendors[v].phone;
            customer[v][4] = cusl.vendors[v].address;
            }  

            $('#tableVendorList').dataTable({
              "aaData": customer,
              "bDestroy": true
            });
        }
    });

  }

    </script>