function cuisinelist()
{
  $('#loading').show();

  $.ajax({
            type: "GET",
            //cache: false,
            url: "service.php?servicename=cuisines",
           datatype : "JSON",
           //contentType : 'application/json',
            success: function(rclist)
            {
               $('#loading').hide();
               
                console.log("Cuisines:"+JSON.stringify(rclist));

                var cl = '';

                cl = JSON.parse(rclist);

                var culi = new Array();

                for(var d=0;d<cl.cuisinesd.length;d++)
                {
                    culi[d] = new Array();

                    culi[d][0] = d+1;
                    culi[d][1] = cl.cuisinesd[d].cuisinename;
                    culi[d][2] = cl.cuisinesd[d].chef;
                    culi[d][3] = cl.cuisinesd[d].cuisinepic;
                    culi[d][4] = cl.cuisinesd[d].cuisinedescription;
                    culi[d][5] = cl.cuisinesd[d].price;
                    culi[d][6] = cl.cuisinesd[d].createdate;
                    culi[d][7] = cl.cuisinesd[d].enddate;
                    culi[d][8] = cl.cuisinesd[d].pickuptime;
                    culi[d][9] = '<a href="#myModal" onclick="viewmore('+cl.cuisinesd[d].cuisineid+')" data-toggle="modal">View More</a>';
                }  

                $('#tableCuisineList').dataTable({
                        "aaData": culi,
                        "bDestroy": true
                    });
            }


  });
}

function viewmore(cid)
{
  console.log("My Cuisine Id Is:"+cid);

  var cuisineids = {"cuisineid":cid};

  console.log("My Detail Is"+JSON.stringify(cuisineids));

  $.ajax({
           type: 'POST',
           cache: false,
           url: 'service.php?servicename=getcuidetail',
           datatype: 'JSON',
           contentType: 'application/json',
           data: JSON.stringify(cuisineids),
           success : function(viewcu)
           {
              //console.log("My Response For Cuisine Detail Is"+JSON.stringify(viewc));

              var viewc = JSON.parse(viewcu);

              var cud = new Array();

              for(var cd=0;cd<viewc.cuisined.length;cd++)
              {
                  cud[cd] = new Array();

                  cud[cd][0] = cd+1;
                  cud[cd][1] = viewc.cuisined[cd].trending;
                  cud[cd][2] = viewc.cuisined[cd].onrequest;
                  cud[cd][3] = viewc.cuisined[cd].spicymeter;
                  cud[cd][4] = viewc.cuisined[cd].type;
                  cud[cd][5] = viewc.cuisined[cd].servingdetail;
                  cud[cd][6] = viewc.cuisined[cd].noofservings;

              }  

              $('#tablecuisinesmore').dataTable({
                        "aaData": cud,
                        "bDestroy": true
                    });
           }

  });
}
