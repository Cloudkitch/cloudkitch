/*
 *  Document   : formsValidation.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Forms Validation page
 */

var FormsValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#createInviteCode').validate({
                errorClass: 'help-block animation-pullUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    'val-grpname': {
                        required: true,
                        minlength: 3
                    },
                    'val-inviteCode': {
                        required: true,
                        minlength:4,
                        maxlength:6
                    }
                },
                messages: {
                    'val-grpname': {
                        required: 'Please enter a group name',
                        minlength: 'Your group name must consist of at least 5 characters'
                    },
                    'val-inviteCode': 'Please enter a valid invitation code'
                          },
                submitHandler: function() {
                    
					//createWeddings();
                    //alert("HII");
                    createInviteCode();
					}
            }); 

            $('#createcuisinecat').validate({
                errorClass: 'help-block animation-pullUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    'val-cuisinecat': {
                        required: true,
                        minlength: 3
                    }
                },
                messages: {
                    'val-cuisinecat': {
                        required: 'Please enter a cuisine category',
                        //minlength: 'Your hash tag must consist of at least 5 characters'
                    },
                                     
                },
                submitHandler: function() {
                    //createWedding();
                    createcuisinecati();
                    }
            });

            $('#createcuisinetyp').validate({
                errorClass: 'help-block animation-pullUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    'val-cuitype': {
                        required: true,
                        minlength: 3
                    }
                },
                messages: {
                    'val-cuitype': {
                        required: 'Please enter a cuisine type',
                        //minlength: 'Your hash tag must consist of at least 5 characters'
                    },
                                     
                },
                submitHandler: function() {
                    //createWedding();
                    //createcuisinecati();
                    createcuisinetype();
                    }
            });

            $('#createcategorytyp').validate({
                errorClass: 'help-block animation-pullUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    'val-cattype': {
                        required: true,
                        minlength: 3
                    }
                },
                messages: {
                    'val-cattype': {
                        required: 'Please enter a Category type',
                        //minlength: 'Your hash tag must consist of at least 5 characters'
                    },
                                     
                },
                submitHandler: function() {
                    
                    createcategorytype();
                    }
            });

            $('#createwalletrecharge').validate({
                errorClass: 'help-block animation-pullUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    'val-rechargeamt': {
                        required: true,
                       
                    },
                    'val-actualamt': {
                        required: true,
                       
                    }

                },
                messages: {
                    'val-rechargeamt': {
                        required: 'Please enter a recharge amount',
                        //minlength: 'Your hash tag must consist of at least 5 characters'
                    },
                    'val-actualamt': {
                        required: 'Please enter a actual amount',
                        //minlength: 'Your hash tag must consist of at least 5 characters'
                    },
                                     
                },
                submitHandler: function() {
                    //createWedding();
                    //createcuisinecati();
                    // createcuisinetype();

                    addrecharge();
                    }
            });

            // Validation for Coupon Code

            $('#createCouponCode').validate({
                errorClass: 'help-block animation-pullUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    'val-cpcode': {
                        required: true,
                        minlength: 6
                    },
                    'val-disc': {
                        required: true
                    }
                },
                messages: {
                    'val-grpname': {
                        required: 'Please Enter Coupon Code',
                        minlength: 'Your coupon code must consist of at least 6 characters'
                    },
                    'val-disc': 'Please Enter Discount Amount'
                          },
                submitHandler: function() {
                    
                    //createWeddings();
                    //alert("HII");
                    createCouponCode();
                    }
            }); 

             $('#createbldg').validate({
                errorClass: 'help-block animation-pullUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    'val-bldgname': {
                        required: true,
                       
                    }
                  },
                messages: {
                    'val-bldgname': {
                        required: 'Please enter a building name',
                        //minlength: 'Your hash tag must consist of at least 5 characters'
                    }
                },
                submitHandler: function() {
                    //createWedding();
                    //createcuisinecati();
                    // createcuisinetype();

                   // addrecharge();
                   addbuilding();
                    }
            });

             $('#createtag').validate({
                errorClass: 'help-block animation-pullUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    'val-tagname': {
                        required: true,
                       
                    }
                  },
                messages: {
                    'val-tagname': {
                        required: 'Please enter a tag name',
                        //minlength: 'Your hash tag must consist of at least 5 characters'
                    }
                },
                submitHandler: function() {
                    //createWedding();
                    //createcuisinecati();
                    // createcuisinetype();

                   // addrecharge();
                   //addbuilding();
                   addtag();
                    }
            });
            
                   
        }
    };
}();