 function orderlist()
{
    $('#loading').show();
    $.ajax({
                type:"GET",
                //cache: false,
                url:"service.php?servicename=getorders",
                datatype :"JSON",
               // dataType:"jsonp"
                //async : false,
               contentType : "application/json",
                success : function(dtod)
                {
                     $('#loading').hide();
                   // console.log("My Orders ARe:"+JSON.stringify(dto));

                    var ordl = JSON.parse(dtod);

                    var orders = new Array();

                    for(var o=0;o<ordl.ords.length;o++)
                     {
                        orders[o] = new Array();

                        orders[o][0] = o+1;
                        orders[o][1] = '<a href="#myModal" onclick="openiemlist('+ordl.ords[o].orderid+')" data-toggle="modal">'+ordl.ords[o].orderid+'</a>';
                        orders[o][3] = ordl.ords[o].total;
                        orders[o][2] = ordl.ords[o].customer;
                        orders[o][4] = ordl.ords[o].orderdate;
                        orders[o][5] = ordl.ords[o].invitecode;
                        orders[o][6] = ordl.ords[o].paymenttype;
                        orders[o][7] = ordl.ords[o].transactionid;
                        orders[o][8] = ordl.ords[o].actualamount;
                        orders[o][9] = ordl.ords[o].discountamount;
                        orders[o][10] = ordl.ords[o].couponapplied;
                        orders[o][11] = ordl.ords[o].couponcode;
                     } 

                      $('#tableOrderList').dataTable({
                        "aaData": orders,
                        "bDestroy": true
                    });  
                } 

    });
}

function openiemlist(oid)
{
    console.log("Orderid is:"+oid);

    var orderid = {"orderid":oid};

    $.ajax({
                type: 'POST',
                cache: false,
                async: false,
                url: 'service.php?servicename=getorderitem',
             datatype: 'JSON',
             contentType: 'application/json',
             data: JSON.stringify(orderid),
             success : function(doid)
             {
                //console.log("Order items"+JSON.stringify(doi));

                var oitm = JSON.parse(doid);

                var orditem = new Array();

                console.log("Length:"+oitm.orderitems.length);

                for(var oi=0;oi<oitm.orderitems.length;oi++)
                {
                    orditem[oi] = new Array();

                    orditem[oi][0] = oi+1;
                    orditem[oi][1] = oitm.orderitems[oi].cuisinename;
                    orditem[oi][2] = oitm.orderitems[oi].chef;
                    orditem[oi][3] = oitm.orderitems[oi].price;
                    orditem[oi][4] = oitm.orderitems[oi].quantity;
                    orditem[oi][5] = oitm.orderitems[oi].type;
                    orditem[oi][6] = oitm.orderitems[oi].status;
                    orditem[oi][7] = oitm.orderitems[oi].code;
                    orditem[oi][8] = oitm.orderitems[oi].closedate;
                } 

                $('#tableOrderItemList').dataTable({
                        "aaData": orditem,
                        "bDestroy": true
                    }); 
             }

    });
}
