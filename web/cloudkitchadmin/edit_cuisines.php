<?php 

session_start();
include 'inc/databaseConfig.php';
include 'inc/config.php'; 
$ids = $_REQUEST['id'];
if(!isset($_SESSION['info']['user']))
{
	header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
include 'inc/template_start.php';
include 'inc/page_head.php';
$userid = $_SESSION['info']['id'];
?>
<link href="css/summernote.css" rel="stylesheet">
<style type="text/css">
    .img-close{
    position: absolute;
    cursor: pointer;
}
</style>
<div id="page-content">
  <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
  <img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1>Edit Cuisines</h1>
    </div>
</div>
</div>
</div>
<div class="row">
    <div class="col-sm-10 col-md-12 col-lg-12">
        <div class="block">
            <form id="createcuisines" name="createcuisines" class="form-horizontal form-bordered">
             <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinesname">Cuisines name<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="hidden" id="id" name="id">
                    <input type="text" id="val_cuisinesname" name="val_cuisinesname" class="form-control" placeholder="">
                </div>
            </div>
             <div class="form-group">
                <label class="col-md-3 control-label" for="image">Cuisines Images<span class="text-danger"></span></label>
                <div class="col-md-6">
                    <input type="hidden" id="base64" name="base64">
                    <input type="file" id="image" name="image" onchange="loadImageFile(this)" multiple>
                    <div id="cuisinesImg"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinescategory">Menu Category<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="hidden" name="cuisinetypename" id="cuisinetypename">
                    <select class="form-control" id="val_cuisinescategory" name="val_cuisinescategory">
                    <?php
                    $query = "SELECT * FROM cuisinetypemaster WHERE page_type='2' and FIND_IN_SET($userid, kitchensid) ORDER BY cuisinetype ASC";
                    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                    echo '<option value="">Select Cuisines Type</option>';
                    while($row = mysqli_fetch_assoc($result))
                    {
                        echo '<option value="'.$row['cuisinetypeid'].'">'.$row['cuisinetype'].'</option>';
                    }   
                    ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_homepagecat">Home Page Category<span class="text-danger">*</span></label>
                <div class="col-md-6">
                
                    <select class="form-control" id="val_homepagecat" name="val_homepagecat">
                    <?php
                    $query = "SELECT * FROM cuisinetypemaster WHERE page_type='1' ORDER BY cuisinetype ASC";
                    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                    echo '<option value="">Select Home Page Category</option>';
                    while($row = mysqli_fetch_assoc($result))
                    {
                        echo '<option value="'.$row['cuisinetypeid'].'">'.$row['cuisinetype'].'</option>';
                    }   
                    ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinesveg">Category Type<span class="text-danger">*</span></label>
                <div class="col-md-6">

                    <select class="form-control selectpicker" id="category_type" data-live-search="true" multiple="multiple" name="val_category_type[]">
                        <!-- <option value="" selected>Choose Category Type</option> -->
                        
                    <?php
                        $query1 = "SELECT type FROM cuisine WHERE cuisineid='$ids'";
                        $result1 = mysqli_query($conn,$query1) or die(mysqli_error($conn));
                        $row = mysqli_fetch_assoc($result1);

                        $types = $row['type']; 

                     

                        $typearray  = explode(",",$types);

                        $query = "SELECT * FROM categorytypemaster ORDER BY categorytype ASC";
                        $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                        while($row = mysqli_fetch_assoc($result))
                        {
                            $t = "";
                            if (in_array($row['categorytype'], $typearray))
                            {
                                $t = "selected";
                            }
                            
                            
                            echo '<option value="'.$row['categorytype'].'" '.$t.'>'.$row['categorytype'].'</option>';
                        }   
                    ?> 
                    </select>
                    <!-- <div class="custom-control custom-checkbox custom-control-lg mb-1">
                        <input type="radio" class="custom-control-input" id="val_cuisinesveg" value="1" name="cuisinetype" checked="">
                        <label class="custom-control-label" for="val_cuisinesveg">Veg</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                        <input type="radio" class="custom-control-input" id="val_cuisinesnonveg" value="2" name="cuisinetype">
                        <label class="custom-control-label" for="val_cuisinesnonveg">Non Veg</label>
                    </div> -->
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinesveg">Corporate<span class="text-danger">*</span></label>
                <div class="col-md-6">

                    <select class="form-control selectpicker" id="corporate_type" data-live-search="true" multiple="multiple" name="val_corporate_type[]">
                    <?php
                        $query1 = "SELECT invitecode FROM cuisine WHERE cuisineid='$ids'";
                        $result1 = mysqli_query($conn,$query1) or die(mysqli_error($conn));
                        $row = mysqli_fetch_assoc($result1);

                        $types = $row['invitecode']; 

                        $typearray  = explode(",",$types);

                        $query = "SELECT * FROM corporate WHERE status = '1' ORDER BY id ASC";
                        $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                        while($row = mysqli_fetch_assoc($result))
                        {
                            $t = "";
                            if (in_array($row['id'], $typearray))
                            {
                                $t = "selected";
                            }
                            
                            
                            echo '<option value="'.$row['id'].'" '.$t.'>'.$row['name'].'</option>';
                        }    
                    ?> 
                    </select>
                    
                </div>
            </div>

           
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinesserves">Serves<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="number" id="val_cuisinesserves" name="val_cuisinesserves" class="form-control" min="1" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinesprice">Price<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <input type="number" id="val_cuisinesprice" name="val_cuisinesprice" class="form-control" min="1" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinesdescription">Description<span class="text-danger"></span></label>
                <div class="col-md-6">
                    <input type="hidden" id="description" name="description">
                    <textarea id="val_cuisinesdescription" name="val_cuisinesdescription" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinesstatus">Status<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <select class="form-control" id="val_cuisinesstatus" name="val_cuisinesstatus">
                        <option value="1">In Stock</option>
                        <option value="0">Out of Stock</option>
                    </select>
                </div>
            </div>

            <?php
                $tags = "";
                $query1 = "SELECT tags FROM cuisine WHERE cuisineid='$ids'";
                $result1 = mysqli_query($conn,$query1) or die(mysqli_error($conn));
                $row = mysqli_fetch_assoc($result1);
                $tags = $row['tags'];
            ?>
            <div class="form-group">
                <label class="col-md-3 control-label" for="val_cuisinestag">Tag</label>
                <div class="col-md-6">
                    <input type="text" id="val_cuisinestag" name="val_cuisinestag" class="form-control" placeholder="" rows="3" data-role="tagsinput" data-caseInsensitive="true" value="<?=$tags?>">
                </div>
            </div>


            <input type="hidden" id="sections" name="sections">
            <div class="form-group" id="addSeectionsParentDiv">
            </div>      
                

            <div class="form-group">
               
                <div class="col-md-6">
                    <input type="hidden" id="section_count" value="0">
                    <button type="button" id="addSection">Add Section</button>
                    <!-- <button type="button" id="checkdata">Check data</button> -->
                </div>
            </div>

            <div class="form-group form-actions">
                <div class="col-md-8 col-md-offset-3">
                    <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button>
                    <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
<!-- END Page Content -->
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Load and execute javascript code used only in this page -->
<script src="js/CasseroleService.js"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/summernote.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function() {
            $("#loading").hide();
            // $('#val_cuisinesdescription').summernote({
            //     height: 200 ,
            //     minHeight: null, 
            //     maxHeight: null,
            //     toolbar: [
            //     ['style', ['style']],
            //     ['font', ['bold', 'underline', 'clear']],
            //     ['fontname', ['fontname']],
            //     ['color', ['color']],
            //     ['para', ['ul', 'ol', 'paragraph']],
            //     ['table', ['table']],
            //     ['view', ['fullscreen', 'codeview', 'help']],
            //     ],
            // });
            var id = "<?=$_GET['id']?>";
            var request = {"id":id};
            $.ajax({
                url: 'service.php?servicename=editcuisine',
                type: 'POST',
                data: JSON.stringify(request),
                contentType: 'application/json; charset=utf-8',
                datatype: 'JSON',
                async: true,
                success: function(data)
                {
                   var result = JSON.parse(data);
                   for(var i=0;i<result['images'].length;i++){
                    $("#cuisinesImg").append('<div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;padding-left:5px;" onclick = "imageClose(this)">X</div><img  src="'+result['images'][i]+'" style="height:100px;margin-right:5px;margin-top:5px;" class="img-thumbnail cuisine-img"></div>');
                   }
                   //console.log(result['cuisinetype']);
                   $("#id").val(result['id']); 
                   $("#val_cuisinesname").val(result['val_cuisinesname']); 
                   $("#val_cuisinescategory").val(result['val_cuisinescategory']); 
                   $("#val_homepagecat").val(result['val_pagecategory']); 
                   $("#val_cuisinesserves").val(result['val_cuisinesserves']); 
                   $("#val_cuisinesprice").val(result['val_cuisinesprice']); 
                   $("#description").val(result['description']); 
                   $("#val_cuisinesdescription").val(result['description']);
                   var status =(result['val_cuisinesstatus']); 
                   if (status=='Y') {
                       $("#val_cuisinesstatus").val(1);
                   }
                   else
                   {
                    $("#val_cuisinesstatus").val(0);
                    }

                    for(var i=0;i<result['sections'].length;i++){
                      
                      var addon = result['sections'][i].addons;
                      var cnt = i + 1;
                      var singleselected = "";
                      var multiselected = "";
                      if(result['sections'][i].choicetype == 1){
                        multiselected = "selected";
                      }else{
                        singleselected = "selected";
                      }
                      var addsection = '<div class="form-group AddSectionDiv" data-id="'+cnt+'" id="extraSectionParentdDiv-'+cnt+'"><label class="col-md-3 control-label" for="val_cuisinesdescription">Section '+cnt+'<span class="text-danger"></span></label><button type="button" id="r-'+cnt+'" onclick="removeSection(this)">X</button><div class="col-md-6"><input type="text" id="section-'+cnt+'" name="val_section_'+cnt+'[]" class="form-control" placeholder="Add Heading" value="'+result['sections'][i].title+'"><select id="choice-'+cnt+'" class="form-control"><option value="1" '+multiselected+'>Multiple</option><option value="0" '+singleselected+'>Single</option></select><input type="number" id="minchoice-'+cnt+'" placeholder="No. of choice" value="'+result['sections'][i].noofchoice+'"><input type="hidden" id="section_addon_count_'+cnt+'" value="1"><div id="extraAddOn-'+cnt+'"></div><br><button type="button" id="b-'+cnt+'" onclick="addAddOn(this)">Add AddOn</button></div></div></div>';
                      $("#addSeectionsParentDiv").append(addsection);
                      secid = cnt;
                      for(var j=0;j<addon.length;j++){
                                                
                        var acnt = j + 1;
                        var addsection = '<div id="addonDiv-'+secid+'-'+acnt+'"><br><input type="text" data-id="'+acnt+'" id="addon-'+secid+'-'+acnt+'" name="val_addon_'+secid+'[]" class="form-control AddAddOnInput-'+secid+'" placeholder="Add Add On" value="'+addon[j].detail+'"><input type="number" id="addonprice-'+secid+'-'+acnt+'" name="price[]" placeholder="Add Price" value="'+addon[j].price+'"><br>';
                        if(addon[j].ctype == '1'){
                            addsection += '<input type="radio" name="addontype-'+secid+'-'+acnt+'"  value="1" checked>Veg <input type="radio" name="addontype-'+secid+'-'+acnt+'" value="0">Non-veg';
                        }else{
                            addsection += '<input type="radio" name="addontype-'+secid+'-'+acnt+'"  value="1">Veg <input type="radio" name="addontype-'+secid+'-'+acnt+'" value="0" checked>Non-veg';
                        }
                        addsection += '<button type="button" id="ra-'+secid+'-'+acnt+'" onclick="removeAddon(\''+secid+'-'+acnt+'\')">X</button></div>';
                        $("#extraAddOn-"+secid).append(addsection);
                        $("#section_addon_count_"+secid).val(addon.length);
                   }
                   }
                   $("#section_count").val(result['sections'].length);
                    //var cuisinetype = result['cuisinetype'].split(',');
                    // $.each(result['cuisinetype'].split(","), function(i,e){
                    //     var d = $("#category_type option[value='" + e + "']").val();
                    //     alert(d);
                    //     console.log(e);



                    //     //$("#category_type option[value='veg']").prop("selected", true);
                    // });

                    // $('.dropdown-menu').each(function(){
                    //     $(this).find('li').each(function(){
                    //         var t = $(this).find('li').attr("data-original-index");
                    //         alert (t);
                    //         var dat = $(this).html();
                    //         $.each(result['cuisinetype'].split(","), function(i,e){
                    //             var d = $("#category_type option[value='" + e + "']").val();
                    //             if(d == dat){
                    //                 //alert(dat);
                    //                 $(this).find('li').addClass("selected");
                    //             }
                                
                    //         });

                    //     });

                    // });
               }
            });     
        });
        function previewImage(input) {
            if (input.files) {
                var filesAmount = input.files.length;
                for (var i = 0; i < filesAmount; i++)   {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        var src = event.target.result;
                        $("#cuisinesImg").append('<div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;padding-left:5px;" onclick = "imageClose(this)">X</div><img  src="'+src+'" style="height:100px;margin-right:5px;margin-top:5px;" class="img-thumbnail cuisine-img"></div>');
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

        var loadImageFile = function (input) {
            var filterType = /^(?:image\/jpeg|image\/jpeg|image\/jpeg|image\/png|image\/x\-icon)$/i;
        
            if (input.files) {
                if (input.files.length === 0) { 
                    return; 
                }
                var filesAmount = input.files.length;
                for (var i = 0; i < filesAmount; i++)   {
                    
                    if (!filterType.test(input.files[i].type)) {
                        alert("Please select a valid image."); 
                        return;
                    }
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL(input.files[i]);

                    fileReader.onload = function (event) {
                    var image = new Image();
                    var type = 'image/jpeg';
                    var  quality = 0.30;
                    image.onload=function(){
                        var canvas=document.createElement("canvas");
                        var context=canvas.getContext("2d");
                        canvas.width=image.width/1;
                        canvas.height=image.height/1;
                        context.drawImage(image,0,0);
                        var imgsrc =  canvas.toDataURL( type, quality );
                        $("#cuisinesImg").append('<div style="position:relative;display:inline-block;"><div class="img-close" style="background-color:#c69c54;width:20px;height:20px;border-radius:10px;color:#fff;padding-left:5px;" onclick = "imageClose(this)">X</div><img  src="'+imgsrc +'" style="height:100px;margin-right:5px;margin-top:5px;" class="img-thumbnail cuisine-img"></div>');
                        
                    }
                    image.src=event.target.result;
                    };

                }
            }
        }

        function imageClose(a)
        {
            $(a).parent().remove();
        }
        $("form[name='createcuisines']").validate({
        rules: {
            val_cuisinesname:"required",
            val_cuisinescategory:"required",
            val_cuisinesserves:"required",
            val_cuisinesprice:"required",
            val_cuisinesstatus:"required",
            val_category_type:"required",
            val_corporate_type:"required"
        },
        submitHandler: function(form) {
            $(this).find('button[type=submit]').prop('disabled', true);
            $(".preloader").show();
            var images = "";
            $( ".cuisine-img" ).each(function() {
                var image = $(this).attr("src");
                images += "###"+image;
            });
            $("#base64").val(images);
            var desc =  $('#val_cuisinesdescription').val(); // $('#val_cuisinesdescription').summernote('code');
            $("#description").val(desc);

            var section_count = $(".AddSectionDiv").length;
            var sectionarray = [];
            $(".AddSectionDiv").each(function() {
                var addonarray = [];
                var secid = $(this).attr('data-id');
                var secheading = $("#section-"+secid).val();
                var choicetype = $("#choice-"+secid).val();
                var noofchoice = $("#minchoice-"+secid).val();
                var addon_count = $("#section_addon_count_"+secid).val();
                //alert(addon_count); 
                for(i = 1; i <= addon_count; i++) {
                    if($("#addon-"+secid+"-"+i).length > 0){
                        var addon = $("#addon-"+secid+"-"+i).val();
                        var price = $("#addonprice-"+secid+"-"+i).val();
                        var cuisinetype = $("input[name='addontype-"+secid+"-"+i +"']:checked").val();
                        if(addon!="undefined" && addon!=""){
                            var addondata = {"addon": addon,"price":price,"cuisinetype":cuisinetype};
                            addonarray.push(addondata);
                        }
                    }
                    
                }
                sectionarray.push({"title": secheading,"addons":addonarray,"Choicetype": choicetype ,"noofchoice":noofchoice});
            });
            $("#sections").val(JSON.stringify(sectionarray));


            var cuisinename = $( "#val_cuisinescategory option:selected" ).text();
            $("#cuisinetypename").val(cuisinename);
            
            var data = new FormData($('#createcuisines')[0]);
            $.ajax({
                url: 'service.php?servicename=updatecuisine',
                type: 'POST',
                mimeType: "multipart/form-data",
                contentType: false, 
                cache: false,
                processData: false,
                data: data, 
                success: function(data)
                {
                     $(".preloader").hide();
                     var result = JSON.parse(data);
                     if(result.status == 'success')
                     {
                        $("#toast-success").html(result.msg);
                        $("#toaster").fadeIn();
                        window.location.href = 'cuisinelist.php';
                     }
                     else
                     {
                        $("#toast-error").html(result.msg);
                        $("#toasterError").fadeIn();
                     }
                      setTimeout(function(){
                         $("#toaster").fadeOut();
                        $("#toasterError").fadeOut();
                  }, 3000);
                }
            });
        }
    });

    $("#addSection").click(function(){
        var section_count = $("#section_count").val();
        var cnt = parseInt(section_count) + 1;
        var addsection = '<div class="form-group AddSectionDiv" data-id="'+cnt+'" id="extraSectionParentdDiv-'+cnt+'"><label class="col-md-3 control-label" for="val_cuisinesdescription">Section '+cnt+'<span class="text-danger"></span></label><button type="button" id="r-'+cnt+'" onclick="removeSection(this)">X</button><div class="col-md-6"><input type="text" id="section-'+cnt+'" name="val_section_'+cnt+'[]" class="form-control" placeholder="Add Heading"><select id="choice-'+cnt+'" class="form-control"><option value="1" selected>Multiple</option><option value="0">Single</option></select><input type="number" id="minchoice-'+cnt+'" placeholder="No. of choice"><input type="hidden" id="section_addon_count_'+cnt+'" value="1"><br><div id="extraAddOn-'+cnt+'"><div id="addonDiv-'+cnt+'-1"><input type="text" id="addon-'+cnt+'-1" data-id="'+cnt+'" name="val_addon_'+cnt+'[]" class="form-control AddAddOnInput-'+cnt+'" placeholder="Add AddOn"><input type="number" id="addonprice-'+cnt+'-1" name="price[]" placeholder="Add Price"><br>  <input type="radio" name="addontype-'+cnt+'-1">Veg <input type="radio" name="addontype-'+cnt+'-1">Non-veg        <button type="button" id="ra-'+cnt+'-1" onclick="removeAddon(\''+cnt+'-1\')">X</button></div></div><br><button type="button" id="b-'+cnt+'" onclick="addAddOn(this)">Add AddOn</button></div></div></div>';
        $("#addSeectionsParentDiv").append(addsection);
        $("#section_count").val(cnt);
    });

    function removeSection(obj){
        
        var secid = obj.id; 
        //var sec =$(secid).parent('div');
        var sec = $("#"+secid).parent('div').attr("id");
        $("#"+sec).remove();
        // alert(sec);
    }

    function removeAddon(aid){
        $("#addonDiv-"+aid).remove();
        //$("#addonprice-"+aid).remove();
        //$("#ra-"+aid).remove();
    }
    function addAddOn(obj){
        
        var section = obj.id; 
        section_split = section.split("-");
        var secid = section_split[1];        
        var addoncount = $("#section_addon_count_"+secid).val();
        var acnt = parseInt(addoncount) + 1;
        var addsection = '<br><br><input type="text" data-id="'+acnt+'" id="addon-'+secid+'-'+acnt+'" name="val_addon_'+secid+'[]" class="form-control AddAddOnInput-'+secid+'" placeholder="Add Add On"><input type="number" id="addonprice-'+secid+'-'+acnt+'" name="price[]" placeholder="Add Price"><br>  <input type="radio" name="addontype-'+secid+'-'+acnt+'" value="1">Veg <input type="radio" name="addontype-'+secid+'-'+acnt+'" value="0">Non-veg        <button type="button" id="ra-'+secid+'-'+acnt+'" onclick="removeAddon(\''+secid+'-'+acnt+'\')">X</button>';
        $("#extraAddOn-"+secid).append(addsection);
        $("#section_addon_count_"+secid).val(acnt);
    }

    $("#checkdata").click(function(){
        var section_count = $(".AddSectionDiv").length;
            var sectionarray = [];
            $(".AddSectionDiv").each(function() {
                var addonarray = [];
                var secid = $(this).attr('data-id');
                var secheading = $("#section-"+secid).val();
                //alert(secheading);
                //var addon_count = $(".AddAddOnInput-"+secid).length;
                var addon_count = $("#section_addon_count_"+secid).val();
                alert(addon_count); 
                for(i = 1; i <= addon_count; i++) {
                    if($("#addon-"+secid+"-"+i).length > 0){
                        var addon = $("#addon-"+secid+"-"+i).val();
                        var price = $("#addonprice-"+secid+"-"+i).val();
                        var addondata = {"addon": addon,"price":price};
                        addonarray.push(addondata);
                    }
                    
                }
                sectionarray.push({"title": secheading,"addons":addonarray});
            });
            $("#sections").val(JSON.stringify(sectionarray));
    });
    </script>
    <?php include 'inc/template_end.php'; ?>

