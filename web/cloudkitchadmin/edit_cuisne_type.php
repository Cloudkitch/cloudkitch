<?php 
$ids = $_REQUEST['id'];
session_start();
include 'inc/databaseConfig.php';
include 'inc/config.php'; 
if(!isset($_SESSION['info']['user']))
{
	header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
include 'inc/template_start.php';
include 'inc/page_head.php';
?>
<style type="text/css">
    .working-days .custom-checkbox
    {
        display: inline-block;
        margin-left: 10px;
    }
</style>
<div id="page-content">
  <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
  <img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1>Edit Cuisine Type</h1>
    </div>
</div>
</div>
</div>
<div class="row">
    <div class="col-sm-10 col-md-12 col-lg-12">
        <div class="block">
            <form id="createcuisinetyp" name="createcuisinetyp" class="form-horizontal form-bordered">
                <div class="form-group">
                <input type="hidden" id="id" name="id" class="form-control" placeholder="">
                        <label class="col-md-3 control-label" for="val-cuitype">Cuisine Type<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        
                        
                            <input type="text" id="val-cuitype" name="valcuitype" class="form-control" placeholder="Please Enter Cuisine Type">
                        
                        <span id="user-result-cuitype" style="width:90%;vertical-align: -webkit-baseline-middle;"></span>
                    </div>
                    
                </div>

                <div class="form-group">
                <label class="col-md-3 control-label" for="val-pagetype">Page Type<span class="text-danger">*</span></label>
                    <div class="col-md-6">                       
                            <select name="valpagetype" id="val-pagetype" class="form-control">
                            <option value="1">Home Page Type</option>
                                <option value="2">Kitchen Page Type</option>
                                <option value="3">Team Page Type</option>
                                <option value="4">Event Page Type</option>
                                <option value="5">Party Page Type</option>
                                <option value="6">Cafeteria Page Type</option>
                                <option value="7">Society Type</option>
                                <option value="8">Kitty Type</option>
                            </select>
                       
                        
                    </div>
                </div>
                <div class="form-group">
                        <label class="col-md-3 control-label" for="category_type">Corporate<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                        
                        <select class="form-control selectpicker" id="corporate_type" data-live-search="true" multiple="multiple" name="val_corporate_type[]">                                
                            <?php
                             $query1 = "SELECT corporateid FROM cuisinetypemaster WHERE cuisinetypeid='$ids'";
                             $result1 = mysqli_query($conn,$query1) or die(mysqli_error($conn));
                             $row = mysqli_fetch_assoc($result1);
                             $types = $row['corporateid']; 
                             $typearray  = explode(",",$types);
                             
                                $query = "SELECT * FROM corporate WHERE status = '1' ORDER BY id ASC";
                                $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                                while($row = mysqli_fetch_assoc($result))
                                {
                                    
                                    $t = "";
                                    if (in_array($row['id'], $typearray))
                                    {
                                        $t = "selected";
                                    }

                                    echo '<option value="'.$row['id'].'" '.$t.'>'.$row['name'].'</option>';
                                }   
                            ?> 
                            </select>
                        
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="kitchens">Kitchen<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                        
                        <select class="form-control selectpicker" id="kitchens" data-live-search="true" multiple="multiple" name="kitchens[]">                                
                            <?php
                             $query1 = "SELECT kitchensid FROM cuisinetypemaster WHERE cuisinetypeid='$ids'";
                             $result1 = mysqli_query($conn,$query1) or die(mysqli_error($conn));
                             $row = mysqli_fetch_assoc($result1);
                             $types = $row['kitchensid']; 
                             $typearray  = explode(",",$types);

                                $query = "SELECT * FROM user WHERE status='1' AND ischef ='1' ORDER BY name ASC";
                                $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                                while($row = mysqli_fetch_assoc($result))
                                {
                                    $t = "";
                                    if (in_array($row['userid'], $typearray))
                                    {
                                        $t = "selected";
                                    }
                                    echo '<option value="'.$row['userid'].'" '.$t.'>'.$row['name'].'</option>';
                                }   
                            ?> 
                            </select>
                        
                        </div>
                    </div>

                
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-3">
                        <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button>
                        <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<!-- END Page Content -->
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
<!-- Load and execute javascript code used only in this page -->
<script src="js/CasseroleService.js"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<!-- <script>
    $(function() { FormsValidation.init();
        invitecodedetail();
    });
</script> -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#loading").hide();
            var id = "<?=$_GET['id']?>";
            var request = {"id":id};
            $.ajax({
                url: 'service.php?servicename=editcuisinetype',
                type: 'POST',
                data: JSON.stringify(request),
                contentType: 'application/json; charset=utf-8',
                datatype: 'JSON',
                async: true,
                success: function(data)
                {
                   var result = JSON.parse(data);
                   $("#id").val(result[0]['cuisinetypeid']); 
                   $("#val-cuitype").val(result[0]['cuisinetype']); 
                   $("#val-pagetype").val(result[0]['page_type']); 
               }
           });

        //    var x_timer;    
        //     $("#val-cuitype").keyup(function (e){
        //         clearTimeout(x_timer);
        //         var ctype = $(this).val();
        //         console.log("CUisine Type Is"+ctype);
        //         x_timer = setTimeout(function(){
        //             check_cuisinetype(ctype);
        //         }, 1000);
        //     }); 
    });
    $("form[name='createcuisinetyp']").validate({
        rules: {
            valcuitype : "required"
        },
        submitHandler: function(form) {
            $(this).find('button[type=submit]').prop('disabled', true);
            $(".preloader").show();
            var data = new FormData($('#createcuisinetyp')[0]);
            $.ajax({
                url: 'service.php?servicename=updatecuisinetype',
                type: 'POST',
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                data: data, 
                success: function(data)
                {
                 $(".preloader").hide();
                 var result = JSON.parse(data);
                 if(result.status == 'success')
                 {
                    $("#toast-success").html(result.msg);
                    $("#toaster").fadeIn();
                    window.location.href = 'cuisinetypelist.php';
                 }
                else
                {
                    $("#toast-error").html(result.msg);
                    $("#toasterError").fadeIn();
                }
                setTimeout(function(){
                    $("#toasterError").fadeOut();
                }, 3000);
            }
        });
        }
    });
</script>
<?php include 'inc/template_end.php'; ?>

