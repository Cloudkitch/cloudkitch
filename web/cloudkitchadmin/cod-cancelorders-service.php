<?php 

header("Access-Control-Allow-Origin:*");

include 'inc/databaseConfig.php';

if($_GET['servicename'] == 'Cancelorders')
{
    $reqco = file_get_contents('php://input');

    $resco = json_decode($reqco,true);

    $invitecode = '';

    $invitecode = $resco['invitecode'];

    $queco =  "SELECT (SELECT name FROM user as c WHERE c.userid=co.userid) as username,co.userid FROM
    		   codcancelorders as co WHERE co.chefpaid='N' AND co.isCleared='N' AND co.invitecode='".$invitecode."'
    		   GROUP BY co.userid";

    $excco = mysqli_query($conn,$queco) or die(mysqli_error($conn));

    $co = array();

    if(mysqli_num_rows($excco) > 0)
    {
    	$co['canco'] = array();

    	while($rowco = mysqli_fetch_assoc($excco))
    	{
    		$o = array();

    		$o['userid'] = $rowco['userid'];

    		$o['username'] = $rowco['username'];

    		$quedc = "SELECT count(*) as dues FROM codcancelorders WHERE userid='".$rowco['userid']."' AND isCleared='N' AND chefpaid='N'";

    		$excdc = mysqli_query($conn,$quedc) or die(mysqli_error($conn));

    		$rsdc = mysqli_fetch_assoc($excdc);

    		$o['duecount'] = $rsdc['dues'];

    		$quetc = "SELECT SUM(chefamount) as amount FROM codcancelorders WHERE userid='".$rowco['userid']."' AND isCleared='N' AND chefpaid='N'";

    		$exctc = mysqli_query($conn,$quetc) or die(mysqli_error($conn));

    		$rstc = mysqli_fetch_assoc($exctc);

    		$o['totalamount'] = $rstc['amount'];

    		array_push($co['canco'], $o);

    	}

    	$co['status'] = 'success';
    	$co['msg'] = 'Data available';	
    }
    else
    {
    	$co['status'] = 'failure';
    	$co['msg'] = 'No Data found..!!';
    }

    print_r(json_encode($co));
    exit;		   

}

if($_GET['servicename'] == 'userdues')
{
	$requd = file_get_contents('php://input');

	$resud = json_decode($requd,true);

	$userid = '';

	$userid = $resud['userid'];

	$ud = array();

	$queud =  "SELECT cassordid,cassitemid,cuisineid,(SELECT cuisinename FROM cuisine WHERE cuisineid=
				co.cuisineid) as cuisine,(SELECT name FROM user WHERE userid=co.chefid) as chefname,
				chefid,chefamount FROM codcancelorders as co WHERE userid='".$userid."' AND isCleared='N'
				AND chefpaid='N'";

	$excud = mysqli_query($conn,$queud) or die(mysqli_error($conn));

	if(mysqli_num_rows($excud) > 0)
	{
		$ud['dues'] = array();

		while($rowud = mysqli_fetch_assoc($excud)) 
		{
			$d = array();

			$d['orderid'] = $rowud['cassordid'];

			$d['cuisineid'] = $rowud['cuisineid'];

			$d['cuisine'] = $rowud['cuisine'];

			$d['chef'] = $rowud['chefname'];

			$d['chefid'] = $rowud['chefid'];

			$d['amount'] = $rowud['chefamount'];

			array_push($ud['dues'], $d);
		}

		$ud['status'] = 'success';
		$ud['msg'] = 'data available';
	}
	else
	{
		$ud['status'] = 'failure';
		$ud['msg'] = 'Data not found..!!';
	}

	print_r(json_encode($ud));
	exit;			
}

?>
