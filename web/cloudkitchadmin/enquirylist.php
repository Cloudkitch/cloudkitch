<?php 
session_start();

//print_r($_SESSION);
if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/Casseroleadmin/index.php");
	header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];

//include 'inc/config.php'; $template['header_link'] = 'WELCOME';

 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>


<!-- Page content -->
<div id="page-content">

<div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- Validation Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Enquiry List</h1>
                    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">
                </div>
            </div>
           
        </div>
    </div>
    <!-- END Validation Header -->

    <!-- Form Validation Content -->
    
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <!-- Form Validation Block -->
            <table id ="tablecul" class="table table-vcenter table-striped table-hover table-borderless">
                                            <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Name</th>     
                                                 <th>Email</th>                                            
                                                 <th>Mobile</th>
                                                 <th>Company Name</th>
                                                 <th>Event Desc</th>
                                                 <th>No Guest</th>
                                                 <th>Budget</th>
                                                 <th>Event Date</th>
                                                 <th>Event Time</th>
                                                 <th>Catered</th>
                                                 <th>Restrictions</th>
                                                 <th>Remarks</th>



                                                 <!-- <th></th> -->
                                                
                                                </tr> 
                                                </thead>
                                              
                                            </table>
            <!-- END Form Validation Block -->
        </div>
    </div>
    <!-- END Form Validation Content -->
    <!-- <div id="loading">
              <img id="loading-image" src="img/loading.gif" alt="Loading..." />
        </div> -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/MIHService.js"></script> -->
<script src="js/CasseroleService.js"></script>



<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#loading").hide();

 //    $("#checkAll").click(function () {
 //     $('input:checkbox').not(this).prop('checked', this.checked);
 // });

 // var table = $('#tablecul').DataTable({
 //                        "bDestroy": true,
 //                        "autoWidth": true
 //                    });

 
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    // var type = '?php 
    //echo $_SESSION['info']['type']; ?>';
    // var wedcode = '';
    // wedcode = '2616';
    // console.log("My Type Is :"+type);
    // if(type == "admin")
    // {
    //     getWeddingList();
    // }

    //invitationlist();

    var invitecode = '';

      invitecode = $("#invitecode").val();

    customerlistnew(invitecode);
    // if(type == "subadmin") 
    // {
    //    getWeddingListBySubadmin(wedcode); 
    // }   
    });</script>

    <script type="text/javascript">
      
    function customerlistnew(ic)
    {
       console.log("invitecode"+ic);



       $.ajax({
                url: 'assignmtp_service.php?servicename=enquiry_list',  
                type: 'POST',
                datatype: 'JSON',
                data: JSON.stringify({"ic":ic}),
                async: false,
                success: function(cul)
                {
                    console.log("Cl"+JSON.stringify(cul));

                    var cr = JSON.parse(cul);

                    var ca = new Array();

                    for(var c=0;c<cr.customerlist.length;c++)
                    {
                        ca[c] = new Array();

                        ca[c][0] = c+1;

                        ca[c][1] = cr.customerlist[c].corpFullName	;

                        ca[c][2] = cr.customerlist[c].corpFullEmail;

                        ca[c][3] = cr.customerlist[c].corpFullPhone;

                        ca[c][4] = cr.customerlist[c].corpFullCompany;
                        ca[c][5] = cr.customerlist[c].corpEventDesc;
                        ca[c][6] = cr.customerlist[c].corpNoGuest;
                        ca[c][7] = cr.customerlist[c].corpFullBudget;
                        ca[c][8] = cr.customerlist[c].corpdatepicker;
                        ca[c][9] = cr.customerlist[c].corptimepicker;
                        ca[c][10] = cr.customerlist[c].corpCatered;
                        ca[c][11] = cr.customerlist[c].corpRestrictions;
                        ca[c][12] = cr.customerlist[c].corpRemarks;



                    }

                    var table = $('#tablecul').DataTable({
                        "aaData": ca,
                        "bDestroy": true,
                        "scrollX": true,
                        "autoWidth": true
                    }); 

                    $('#checkAll').on('click', function(){

                    //alert("Heelo");
                        // Check/uncheck all checkboxes in the table
                        var rows = table.rows({ 'search': 'applied' }).nodes();
                        $('input[type="checkbox"]', rows).prop('checked', this.checked);
                     }); 
                }

       });
    }

    </script>

    <script type="text/javascript">
    
function addmytreepoint()
{
    

    var mtp = '';

    mtp = $("#mtp").val();

    if(mtp != 0)
    {

      var cusary = [];

      $.each($("input[name='cus']:checked"),function(){

            cusary.push($(this).val());

      });
        var reqamp = {"mytreepoint":mtp,"users":cusary};

      $.ajax({
               url: 'service.php?servicename=add_mytree_points',
               type: 'POST',
               datatype: 'JSON',
               contentType: 'application/json',
               data: JSON.stringify(reqamp),
               success:function(resmtp)
               {
                      console.log("Res"+JSON.stringify(resmtp));

                      var rmp = JSON.parse(resmtp);

                      if(rmp.status == 'success')
                      {
                          //window.Location.href = 'addmytreepoints.php';
                          location.reload();
                      }    
               }    

      })
    }
    else
    {
        var umtp = [];

        var wltid =  '';

        var mtpv = '';

        $.each($("input[name='cus']:checked"),function(){

               wltid = $(this).val();

               mtpv = $("#txt"+wltid).val();   

               console.log("Wltid"+wltid+"Mtpv"+mtpv);

              umtp.push({
                          "walletid":wltid,
                          "mtp":mtpv 

              });

        });

        console.log("MTP"+JSON.stringify(umtp));

        $.ajax({
                 url: 'assignmtp_service.php?servicename=Assign-MTP',
                 type: 'POST',
                 datatype: 'JSON',
                 data: JSON.stringify(umtp),
                 async:false,
                 success: function(rmtp)
                 {
                     console.log("MTP res"+JSON.stringify(rmtp));

                     var r = JSON.parse(rmtp);

                     if(r.status == 'success')
                     {
                        location.reload();
                     } 
                 }


        })


    }  

    
}


</script>