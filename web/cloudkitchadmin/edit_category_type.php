<?php 

session_start();
include 'inc/databaseConfig.php';
include 'inc/config.php'; 
if(!isset($_SESSION['info']['user']))
{
	header("Location: ".$template['baseurl']);
}
$template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
include 'inc/template_start.php';
include 'inc/page_head.php';
?>
<style type="text/css">
    .working-days .custom-checkbox
    {
        display: inline-block;
        margin-left: 10px;
    }
</style>
<div id="page-content">
  <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
  <img id="loading-image" src="image/loading.gif" alt="Loading..." />
</div>
<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1>Edit Category Type</h1>
    </div>
</div>
</div>
</div>
<div class="row">
    <div class="col-sm-10 col-md-12 col-lg-12">
        <div class="block">
            <form id="createcategorytype" name="createcategorytype" class="form-horizontal form-bordered">
                <div class="form-group">
                    
                    <div class="col-md-6">
                        <input type="hidden" id="id" name="id" class="form-control" placeholder="">
                        <label class="col-md-3 control-label" for="val-cuitype">Category Type<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input type="text" id="val-cuitype" name="valcattype" class="form-control" placeholder="Please Enter Cuisine Type">
                        </div>
                        <span id="user-result-cuitype" style="width:90%;vertical-align: -webkit-baseline-middle;"></span>
                    </div>
                </div>
                
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-3">
                        <button type="submit"  class="btn btn-effect-ripple btn-primary">Submit</button>
                        <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<!-- END Page Content -->
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Load and execute javascript code used only in this page -->
<script src="js/CasseroleService.js"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<!-- <script>
    $(function() { FormsValidation.init();
        invitecodedetail();
    });
</script> -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#loading").hide();
            var id = "<?=$_GET['id']?>";
            var request = {"id":id};
            $.ajax({
                url: 'service.php?servicename=editcategorytype',
                type: 'POST',
                data: JSON.stringify(request),
                contentType: 'application/json; charset=utf-8',
                datatype: 'JSON',
                async: true,
                success: function(data)
                {
                   var result = JSON.parse(data);
                   $("#id").val(result[0]['categorytypeid']); 
                   $("#val-cuitype").val(result[0]['categorytype']); 
               }
           });

        //    var x_timer;    
        //     $("#val-cuitype").keyup(function (e){
        //         clearTimeout(x_timer);
        //         var ctype = $(this).val();
        //         console.log("CUisine Type Is"+ctype);
        //         x_timer = setTimeout(function(){
        //             check_cuisinetype(ctype);
        //         }, 1000);
        //     }); 
    });
    $("form[name='createcategorytype']").validate({
        rules: {
            valcattype : "required"
        },
        submitHandler: function(form) {
            $(this).find('button[type=submit]').prop('disabled', true);
            $(".preloader").show();
            var data = new FormData($('#createcategorytype')[0]);
            $.ajax({
                url: 'service.php?servicename=updatecategorytype',
                type: 'POST',
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                data: data, 
                success: function(data)
                {
                 $(".preloader").hide();
                 var result = JSON.parse(data);
                 if(result.status == 'success')
                 {
                    $("#toast-success").html(result.msg);
                    $("#toaster").fadeIn();
                    window.location.href = 'categorytypelist.php';
                 }
                else
                {
                    $("#toast-error").html(result.msg);
                    $("#toasterError").fadeIn();
                }
                setTimeout(function(){
                    $("#toasterError").fadeOut();
                }, 3000);
            }
        });
        }
    });
</script>
<?php include 'inc/template_end.php'; ?>

