<?php 

session_start();

//print_r($_SESSION);

if(!isset($_SESSION['info']['user']))
{
    // header("Location: http://localhost:7755/Casseroleadmin/index.php");
	header("Location: index.php");
}
//include 'inc/config.php'; $template['header_link'] = 'FORMS';
include 'inc/config.php'; $template['header_link'] = 'WELCOME '.''.$_SESSION['info']['user'];
// include 'inc/config.php'; $template['header_link'] = 'WELCOME ';
 ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Validation Header -->
    <!-- <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Create Invitation Code</h1>
                </div>
            </div>
           
        </div>
    </div> -->

    <div id="loading" style="position:fixed;left: 50%;
  top: 50%;">
              <img id="loading-image" src="image/loading.gif" alt="Loading..." />
        </div>
    <!-- END Validation Header -->

    <div class="row" id="col">

    <input type="hidden" name="invitecode" id="invitecode" value="<?php echo $_SESSION['info']['invitecode']; ?>">

            <div class="col-sm-12 col-md-12 col-lg-12">
            
                <div class="block">

    
                <div class="block-title">
                    <h2>Reported Orders</h2>
                    <button type="button" id="exptexcel" class="btn btn-rounded btn-success" style="float:right">Export</button>
                </div>
           

      
                <table id ="tablccList" class="table table-vcenter table-striped table-hover table-borderless">

                                                <thead>
                                                 <tr>
                                                 <th>Sr</th>
                                                 <th>Reported By</th>
                                                 <th>Reported Against</th>
                                                 <th>Orderid</th>
                                                 <th>Dishname</th>
                                                 <th>Reason</th>
                                                 <th>Narration</th>
                                                 <th>User Comments</th>
                                                 <th>Chef Comments</th>
                                                 <!-- <th>Action</th> -->
                                                 </tr> 
                                                </thead>  
                  
                </table>
            

                </div>

            </div>
    </div>
</div>


 <?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>

<script type="text/javascript">
$(document).ready(function() {
    $("#loading").hide();
    $("#cht").hide();
   // $("#col").hide();
    
    // users();

    // chapters();

  function download_csv(csv, filename) {
        var csvFile;
        var downloadLink;
        // CSV FILE
        csvFile = new Blob([csv], {type: "text/csv"});
        // Download link
        downloadLink = document.createElement("a");
        // File name
        downloadLink.download = filename;
        // We have to create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);
        // Make sure that the link is not displayed
        downloadLink.style.display = "none";
        // Add the link to your DOM
        document.body.appendChild(downloadLink);
        // Lanzamos
        downloadLink.click();
    }

    function export_table_to_csv(html, filename) {
        var csv = [];
        var rows = document.querySelectorAll("table tr");

        for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++) 
        row.push(cols[j].innerText);        
        csv.push(row.join(","));       
        }
        // Download CSV
        download_csv(csv.join("\n"), filename);
    }

    document.querySelector("#exptexcel").addEventListener("click", function () {
    var html = document.querySelector("table").outerHTML;
    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    filename = 'MyTree_Reported_Orders' + postfix + '.csv';
    export_table_to_csv(html, filename);
    });
});
</script>
<script src="js/pages/uiTables.js"></script>
<script>$(function(){ UiTables.init(); 

    
    $('#tablccList').dataTable().fnClearTable();
        $('#tablccList').dataTable().fnDraw();
        $('#tablccList').dataTable().fnDestroy();

        reportedordershistory();
       
      });</script>


<script type="text/javascript">
  
function reportedordershistory()
{

    var ic = '';

    ic = $("#invitecode").val();

    var reqro = {"invitecode":ic}; 


    $.ajax({
             url: 'panicmode_service.php?servicename=Reported-Orders-History',
             type: 'POST',
             datatype: 'JSON',
             data: JSON.stringify(reqro),
             async: false,
             success: function(rro)
             {
               console.log("CC"+JSON.stringify(rro));

               var rescc = JSON.parse(rro);

                var co = new Array();

               if(rescc.status == 'success')
               {
                   for(var ch=0;ch<rescc.reportedoh.length;ch++)
                   {

                      co[ch] = new Array();

                      co[ch][0] = ch+1;

                      co[ch][1] = rescc.reportedoh[ch].user1;

                      co[ch][2] = rescc.reportedoh[ch].user2;

                      co[ch][3] = rescc.reportedoh[ch].orderid;

                      co[ch][4] = rescc.reportedoh[ch].dishname;

                      co[ch][5] = rescc.reportedoh[ch].reason;

                      co[ch][6] = rescc.reportedoh[ch].ordernarratiion;

                      co[ch][7] = rescc.reportedoh[ch].uop;

                      co[ch][8] = rescc.reportedoh[ch].cop;

                      // co[ch][6] = '<button type="button" onclick="resolve(\''+rescc.reportedo[ch].itemid+'\');" class="btn btn-effect-ripple btn-primary">Reported</button>';                  

               
                   }
               }
               // else
               // {
               //      co[] = '<p>Not Available</p>';
               // } 

               

                $('#tablccList').dataTable({
                        "aaData": co,
                        "scrollX": true,
                        "bDestroy": true,
                        "autoWidth": true                    
                       });




             }

    });
}

</script>







