<!DOCTYPE html> 
<html>
    <head>
        <?php
			include 'head.php';
		?>
        <title>CloudKitch Partners</title>
        <meta name="description" content="We partner with restauranteurs, established brands and aspiring entrepreneurs to bring their diners or customers the unique opportunity to have delicious meals delivered across multiple locations. ">
        <link rel="stylesheet" type="text/css" href="<?=$baseurl;?>css/slick.css">
    </head>
    <body>
        <?php
			include 'header.php';
        ?>
        <section class="banner backgroundImage">
            <div class="table">
                <div class="tableCell">
                    <h2>YOU'RE AN ESSENTIAL INGREDIENT<br>TO OUR DISH</h2>
                    <a href="<?=$baseurl;?>#contactform" class="btn">Click here</a>
                    <p>to set up your own Smart Kitchen</p>
                </div>
            </div>
            <video autoplay="" loop="" muted="" playsinline="" poster="<?=$baseurl;?>images/poster.jpg"></video>
        </section>
        <section class="gradientBack backgroundImage offerSection" id="we-offer">
            <div class="col-40 backgroundImage imgBack">
            </div><div class="col-60">
                <h2 class="sectionTitle">Great food experience is about thoughtful preparation. CloudKitch makes that happen for you.</h2>
                <p>Introducing CloudKitch, a unique and flexible co-working kitchen solution for delivery restaurants, designed to simplify your restaurant-business and scale faster. Our investment, helps you to optimize your resources and minimizes your risk.</p><br>
                <p>Our innovatively designed and efficient Smart Kitchens are perfect for new and existing restaurant brands, chefs who want their own ‘signature’ space or aspiring, successful restauranteurs wanting to open up their delivery-only restaurant.</p><br>
                <p>CloudKitch helps you focus on your core strengths, identify opportunities that help you leverage resources and expertise, and create a ‘wowlicious product’ that over time, builds a growing customer base, and an efficient and result-oriented customer loyalty.</p>
            </div>
            <div class="tomato">
                <img src="<?=$baseurl;?>images/tomato.png" alt="">
            </div>
        </section>
        <section class="gradientBack backgroundImage whyChooseSection" id="smart-kitchens">
            <div class="plate">
                <img src="<?=$baseurl;?>images/plate.png" alt="">
            </div>
            <div class="col-40">
            </div><div class="col-60 backgroundImage imgBack">
                <div class="whyChooseBlock gradientBack">
                    <h2 class="sectionTitle">CloudKitch, beyond kitchen and technology. It’s an intuitive Smart Platform.</h2>
                    <p>Setting up a restaurant business has its challenges: from identifying the right location and signing the contract, to licensing, designing and building it from the ground up.</p><br>
                    <p>All of which are extremely time-consuming and resource intense.</p><br>
                    <p>There’s also the immediate need for attracting new customers, managing and sustaining day-to-day operations, putting delivery systems into place, setting targeted marketing with maximum customer satisfaction and ensuring repeat business.</p><br>
                    <p>Over and above, it stretches management bandwidth, demands deep investments, lock-ins, higher commitments and risks.</p>
                </div>
            </div>
        </section>
        <section class="gradientBack expertiseSection">
            <div class="col-40 backgroundImage imgBack">
            </div><div class="col-60">
                <h2 class="sectionTitle">Why leveraging the CloudKitch Expertise is key:</h2>
                <p>CloudKitch is designed to help you focus on your core business: Creativity & Food. It’s a single stop solution, offering best researched and strategic locations, providing you with fully-fitted and licensed kitchens managing operations and deliveries. </p><br>
                <p>As a communications expert, CloudKitch has the tools to grow your brand presence, provide insights into digital marketing, and identify opportunities in real time by gathering market intelligence that enhance your sales.  </p><br>
                <p>Think of it as a 'plug & play' solution for brands to kick-start your sales in just a matter of few hours where it would otherwise take months.</p>
            </div>
        </section>
        <section class="gradientBack backgroundImage kitchenSection" id="ecosystem">
            <div class="textCenter">
                <h2 class="sectionTitle">CloudKitch: The Restaurateur’s most reliable Ecosystem</h2>
            </div>
            <div class="kitchenBlock">
                <div class="col-25">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/ecosystem/1.svg" alt=""></div>
                    <p>Generate smooth cashflow</p>
                    <p>Low capex, Low fixed costs, Maximum profits.</p>
                </div><div class="col-25">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/ecosystem/2.svg" alt=""></div>
                    <p>Leverage operations & management</p>
                    <p>Kitchen supervision, call centre to dispatch and customer delight.</p>
                </div><div class="col-25">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/ecosystem/3.svg" alt=""></div>
                    <p>Maintain Quality standards</p>
                    <p>Gain insights, enhance your products and offerings, while ensuring high standards of Food Safety and Hygiene.</p>
                </div><div class="col-25">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/ecosystem/4.svg" alt=""></div>
                    <p>Ready-to-use = Ready-to-sell</p>
                    <p>Partner with all aggregators and own delivery team for distribution.</p>
                </div><div class="col-25">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/ecosystem/5.svg" alt=""></div>
                    <p>Digital to Local Area Marketing </p>
                    <p>Build your digital presence in local markets, with full stack Customer Relationship Management. We will help you create an effective story for your brand.</p>
                </div><div class="col-25">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/ecosystem/6.svg" alt=""></div>
                    <p>Consultancy</p>
                    <p>We will help you identify a gap, and leverage our expertise to maximize effective sales opportunities.</p>
                </div><div class="col-25">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/ecosystem/7.svg" alt=""></div>
                    <p>The Right Location</p>
                    <p>We help you with the right location for your brand based on effective research.</p>
                </div><div class="col-25">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/ecosystem/8.svg" alt=""></div>
                    <p>Technology </p>
                    <p>We use key tools to help you gain visibility and traction for your brand across key consumer touchpoints: POS, Website, Customer Relationship Management and Analytics. </p>
                </div>
            </div>
            <div class="space"></div>
            <!-- <h2 class="sectionTitle">Kitchen Specifications</h2>
            <div class="specificationWrap">
                <div class="col-30">
                    <img src="<?=$baseurl;?>images/smart-kitchen/kitchen-3.jpg" alt="">
                    <div class="overlay redOverlay">
                        <p>Fitted Kitchen Licences</p>
                    </div>
                </div><div class="col-30">
                    <img src="<?=$baseurl;?>images/smart-kitchen/kitchen-1.jpg" alt="">
                    <div class="overlay yellowOverlay">
                        <p>Technology</p>
                    </div>
                </div><div class="col-30">
                    <img src="<?=$baseurl;?>images/smart-kitchen/kitchen-2.jpg" alt="">
                    <div class="overlay redOverlay">
                        <p>Operations, Marketing & Sales</p>
                    </div>
                </div>
            </div> -->
            <div class="leftTomato">
                <img src="<?=$baseurl;?>images/plate-icon-5.png" alt="">
            </div>
        </section>
        <!-- <section class="woodGradient backgroundImage" id="get-started">
            <div class="plate2">
                <img src="<?=$baseurl;?>images/plate2.png" alt="">
            </div>
            <div class="leftTomato">
                <img src="<?=$baseurl;?>images/plate-icon-5.png" alt="">
            </div>
            <h2 class="sectionTitle capTitle">What Do You Need To Get Started?</h2>
            <div class="getStartedWrap">
                <div class="col-20">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/get-started/chefs.png" alt=""></div>
                    <p>Chefs</p>
                </div><div class="col-20">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/get-started/menu.png" alt=""></div>
                    <p>Menu</p>
                </div><div class="col-20">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/get-started/ingredients.png" alt=""></div>
                    <p>Ingredients</p>
                </div><div class="col-20">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/get-started/packaging.png" alt=""></div>
                    <p>Packaging</p>
                </div>
            </div>
            <div class="space"></div>
            <h2 class="sectionTitle capTitle">What can we bring to the table</h2>
            <div class="getStartedWrap bringTableWrap">
                <div class="col-20">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/bring-table/table-1.png" alt=""></div>
                    <p>Minimal risk</p>
                </div><div class="col-20">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/bring-table/table-2.png" alt=""></div>
                    <p>Low set up cost</p>
                </div><div class="col-20">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/bring-table/table-3.png" alt=""></div>
                    <p>Jump start business overnight</p>
                </div><div class="col-20">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/bring-table/table-4.png" alt=""></div>
                    <p>Area mapping</p>
                </div>
                <div class="col-20">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/bring-table/table-5.png" alt=""></div>
                    <p>Focus on creation</p>
                </div><div class="col-20">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/bring-table/table-6.png" alt=""></div>
                    <p>Management of time & bandwidth</p>
                </div><div class="col-20">
                    <div class="iconWrap"><img src="<?=$baseurl;?>images/bring-table/table-7.png" alt=""></div>
                    <p>Access to international standards</p>
                </div>
            </div>
        </section> -->
        <!--<section class="woodySection backgroundImage">
            <div class="testimonialWrap">
                <h2 class="sectionTitle capTitle">Our customers' view</h2>
                <div class="testimonialBlock">
                    <div class="testimonial">
                        <div class="testTop">
                            <p><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper.</span></p>
                        </div>
                        <div class="testBottom">
                            <div class="clientImg backgroundImage" style="background-image:url('<?=$baseurl;?>images/client.jpg');"></diV>
                            <div class="rating">
                                <img src="<?=$baseurl;?>images/star.png" alt=""><img src="<?=$baseurl;?>images/star.png" alt=""><img src="<?=$baseurl;?>images/star.png" alt=""><img src="<?=$baseurl;?>images/star.png" alt=""><img src="<?=$baseurl;?>images/star.png" alt="">
                            </div>
                            <h3>Riya Sharma</h3>
                            <p>Customer</p>
                        </div>
                    </div>
                    <div class="testimonial">
                        <div class="testTop">
                            <p><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper.</span></p>
                        </div>
                        <div class="testBottom">
                            <div class="clientImg backgroundImage" style="background-image:url('<?=$baseurl;?>images/client.jpg');"></diV>
                            <div class="rating">
                                <img src="<?=$baseurl;?>images/star.png" alt=""><img src="<?=$baseurl;?>images/star.png" alt=""><img src="<?=$baseurl;?>images/star.png" alt=""><img src="<?=$baseurl;?>images/star.png" alt=""><img src="<?=$baseurl;?>images/star.png" alt="">
                            </div>
                            <h3>Riya Sharma</h3>
                            <p>Customer</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->
        <section class="gradientBack backgroundImage scaleUpSection">
            <div class="col-30">
                <h2 class="sectionTitle">Established Brands</h2>
                <p class="sectionText">Scale your investments, while leveraging ours. Your time and management bandwidth is best utilized in your core markets, while you leave the growth for your ancillary markets to us.</p>
                <p class="sectionText">What you have is a strategic advantage that lets you enhance your micro-markets reach and build your brand equity.</p>
            </div><div class="col-30">
                <h2 class="sectionTitle">Aspiring Entrepreneurs</h2>
                <p class="sectionText">As a potential entrepreneur you may have a path-breaking product and concept, but with limited resources and risk-taking ability. </p>
                <p class="sectionText">As an aspiring entrepreneur, you need to: Try, Test, Improve… in order to create the perfect launchpad for your brand. That’s where we can help you.</p>
            </div><div class="col-30">
                <h2 class="sectionTitle">For International Brands: A Dynamic Market</h2>
                <p class="sectionText">India is a complex market, but can you afford to ignore the opportunity? </p>
                <p class="sectionText">The landscape: A population swelling to1.3 billion, with fragmented tastes and constantly changing demographics, in a 50% millennial & baby boomer split, and of course, a growing disposable income.</p>
            </div>
            <div class="space"></div>
            <!-- <div class="textCenter">
                <h2 class="sectionTitle ">Our contact Info for all aspiring restaurateurs:</h2>
                <div class="specificationWrap">
                    <form class="restaurateursForm" id="restaurateursForm">
                        <div class="formElement col-50">
                            <input type="text" name="idBrand"  id="idBrand" placeholder="Brand Name">
                        </div><div class="formElement col-50">
                            <input type="text" name="idContactPerson"  id="idContactPerson" placeholder="Contact Person">
                        </div>
                        <div class="formElement col-50">
                            <input type="text" name="idTitle"  id="idTitle" placeholder="Title">
                        </div><div class="formElement col-50">
                            <input type="text" name="idNumber"  id="idNumber" placeholder="Contact number / email id">
                        </div>
                        <div class="formElement text-right">
                            <button type="button">Submit</button>
                        </div>
                    </form>
                </div>
            </div> -->
            <div class="textCenter">
                <h2 class="sectionTitle">Interested in Joining Our Team?</h2>
                <p>Send in your resume to <a href="mailto:hr@cloudkitch.co.in" style="color:#a38379;">hr@cloudkitch.co.in</a></h3>
            </div>
            <div class="interestedTitle">
                <div class="col-30">
                    <h2 class="sectionIntr">Data Scientist</h2>
                    <h2 class="sectionIntr">Delivery Expert</h2>
                </div><div class="col-30">
                    <h2 class="sectionIntr">Research Analyst</h2>
                    <h2 class="sectionIntr">Business Development</h2>
                    <h2 class="sectionIntr">Finance Manager</h2>
                </div><div class="col-30">
                    <h2 class="sectionIntr">Digital Marketer</h2>
                    <h2 class="sectionIntr">Corporate Sales</h2>
                </div>
            </div>
            <div class="footerTomato">
                <img src="<?=$baseurl;?>images/tomato-footer.png" alt="">
            </div>
            <!-- <div class="space" id="our-clients"></div>
            <h2 class="sectionTitle textCenter">Our Clients</h2>
            <div class="clientSlider">
                <div>
                    <img src="<?=$baseurl;?>images/clients/1.png" alt="">
                </div>
                <div>
                    <img src="<?=$baseurl;?>images/clients/2.png" alt="">
                </div>
                <div>
                    <img src="<?=$baseurl;?>images/clients/3.png" alt="">
                </div>
                <div>
                    <img src="<?=$baseurl;?>images/clients/4.png" alt="">
                </div>
                <div>
                    <img src="<?=$baseurl;?>images/clients/5.png" alt="">
                </div>
                <div>
                    <img src="<?=$baseurl;?>images/clients/6.png" alt="">
                </div>
                <div>
                    <img src="<?=$baseurl;?>images/clients/1.png" alt="">
                </div>
                <div>
                    <img src="<?=$baseurl;?>images/clients/2.png" alt="">
                </div>
                <div>
                    <img src="<?=$baseurl;?>images/clients/3.png" alt="">
                </div>
                <div>
                    <img src="<?=$baseurl;?>images/clients/4.png" alt="">
                </div>
                <div>
                    <img src="<?=$baseurl;?>images/clients/5.png" alt="">
                </div>
                <div>
                    <img src="<?=$baseurl;?>images/clients/6.png" alt="">
                </div>
            </div>
            <div class="space"></div>
            <div class="stats">
                <div class="col-50">
                    <p><span>99%</span>accurate delivery</p>
                </div><div class="col-50">
                    <p><span>100%</span>On time delivery</p>
                </div>
            </div> -->
        </section>
        <section class="contactSection backgroundImage">
            <div class="table">
                <div class="tableCell">
                    <h2>Have more questions?</h2>
                    <h2>Write to us at <a href="mailto:naresh@cloudkitch.co.in">naresh@cloudkitch.co.in</a> or <br> WhatsApp us at <a href="https://api.whatsapp.com/send?phone=919663057733" target="_blank">+91 966 305 7733</a></h2>
                </div>
            </div>
        </section>
        <?php
			include 'footer.php';
		?>
        <script src="<?=$baseurl;?>js/slick.js"></script>
        <script>
            $('.clientSlider').slick({
                arrows: true,
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 6,
                slidesToScroll: 6,
                responsive: [
                    {
                        breakpoint: 500,
                        settings: {
                            dots: false,
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 1000,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3
                        }
                    },
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 4
                        }
                    }
                ]
            });

            $('.testimonialBlock').slick({
                arrows: true,
                dots: true,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1
            });

            
            function saveContacts() {

                var flag = 0;
                var Name = $("#idName").val();
                var PhoneNo = $("#idPhoneNo").val();
                var EmailId = $("#idEmailId").val();
                var Messages = $("#idMessages").val();

                function isValidEmailAddress(Email) {
                    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
                    return pattern.test(Email);
                }

                $("#idEmailId").keyup(function () {
                        $('#errormsgforemail').hide();
                })
                if (!isValidEmailAddress(EmailId)) {
                    eflag = 3;
                    $('#errormsgforemail').show();
                }else {
                        eflag = 0;
                }
                if (Name == "") {
                    flag++;
                    $("#idName").css("border-color", "#CF6F6F");
                }
                if (PhoneNo == "") {
                        flag++;
                        $("#idPhoneNo").css("border-color", "#CF6F6F");
                }
                if (EmailId == "") {
                        flag++;
                        $("#idEmailId").css("border-color", "#CF6F6F");
                }
                if (Messages == "") {
                        flag++;
                        $("#idMessages").css("border-color", "#CF6F6F");
                }

                if (flag == 0 && eflag == 0) {
                    $.ajax({
                        "async": true,
                        "crossDomain": true,
                        "url": "http://cloudkitch.co.in/partners/addcontact.php",
                        "method": "POST",

                        "data": {
                            "name": Name, "phone": PhoneNo, "email": EmailId, "message": Messages
                    }
                }).done(function (result) {
                if (result == "Success") {
                    $("#idName").val('');
                    $("#idPhoneNo").val('');
                    $("#idEmailId").val('');
                    $("#idMessages").val('');
                    swal({
                        title: "Success!",
                        text: "Data submitted sucessfully ",
                        type: "success",
                        timer: 5000
                    });
//                    sendmail(result.Name,result.EmailId)
                }
                else {
                    alert("Please try again later");
                }

            });
            }    
        }
                
            function removeError(id) {
            $('#errormsg').hide();
            $('#' + id).css("border-color", "#707070");
        }

        </script>
    </body>
</html>