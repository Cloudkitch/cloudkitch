<footer class="backgroundImage" id="footer">
    <div class="leftPlate">
        <img src="<?=$baseurl;?>images/leftPlate.png" alt="">
    </div>
    <div class="rightPlate">
        <img src="<?=$baseurl;?>images/plate-icon-4.png" alt="">
    </div>
    <div class="col-60">
        <!-- <div class="appIcon">
            <img src="<?=$baseurl;?>images/googlePlay.png" alt=""><img src="<?=$baseurl;?>images/appStore.png" alt="">
        </div> -->
        <div class="footerLinks">
            <div class="col-30">
                <ul>
                    <li>
                        <p><a href=""><span>About Us</span></a></p>
                    </li><li>
                        <p><a href=""><span>Join The Team</span></a></p>
                    </li><li>
                        <p><a href=""><span>Contact Us</span></a></p>
                    </li>
                </ul>
            </div><div class="col-30">
                <ul>
                    <li>
                        <p><a href=""><span>Privacy Policy</span></a></p>
                    </li><li>
                        <p><a href=""><span>Terms & Conditions</span></a></p>
                    </li><li>
                        <p><a href=""><span>Contact Us</span></a></p>
                    </li>
                </ul>
            </div><div class="col-30">            
            <p class="mobadd"><a href="#">Unit No. G4-A, Trade Centre,<br> Bandra Kurla Complex,<br>Bandra East, Mumbai-400051</a></p><br>
                <p>Phone : <a href="tel:+91 966 305 7733">+91 966 305 7733</a></p>
                <p>Email :  <a href="mailto:naresh@cloudkitch.co.in">naresh@cloudkitch.co.in</a></p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="mailto:kumar@cloudkitch.co.in">kumar@cloudkitch.co.in</a></p>
            </div>
        </div>
        <div class="social">
            <ul>
                <li>
                    <a target="_blank" href="https://www.facebook.com/CloudKitch-106998757387085"><img src="<?=$baseurl;?>images/facebook.png" alt=""></a>
                </li><li>
                    <a target="_blank" href="https://twitter.com/CloudKitch"><img src="<?=$baseurl;?>images/twitter.png" alt=""></a>
                </li><li>
                    <a target="_blank" href="https://www.linkedin.com/company/cloudkitch-private-limited"><img src="<?=$baseurl;?>images/linkedin.png" alt=""></a>
                </li><li>
                    <a target="_blank" href="https://www.instagram.com/cloudkitch/"><img src="<?=$baseurl;?>images/instagram.png" alt=""></a>
                </li><li>
                    <a target="_blank" href="https://api.whatsapp.com/send?phone=919663057733"><img src="<?=$baseurl;?>images/whatsapp.png" alt=""></a>
                </li>
            </ul>
        </div>
    </div><div class="col-40">
        <form class="contactForm" id="contactform">
            <div class="formElement">
                <input type="text" name="idName"  id="idName" placeholder="Name" onkeyup="removeError('idName')">
            </div>
            <div class="formElement">
                <input type="text" name="idEmailId" id="idEmailId" placeholder="Email" onkeyup="removeError('idEmailId')">
                <p id="errormsgforemail" class="error-label" style="">*Email is Invalid</p>
            </div>
            <div class="formElement">
                <input type="text" name="idPhoneNo" id="idPhoneNo" placeholder="Phone" onkeyup="removeError('idPhoneNo')">
            </div>
            <div class="formElement">
                <input type="text" name="idMessages" id="idMessages" placeholder="Message" onkeyup="removeError('idMessages')">
            </div>
            <div class="formElement formElementCenter">
                <button type="button" onclick="saveContacts()">Submit</button>
            </div>
        </form>
    </div>
</footer>
<div class="copyright">
    <p>Copyright &copy; <?php echo date('Y'); ?></p>
</div>
<div class="backtotop">
    <p>&#8593;</p>
</div>
<script src="<?=$baseurl;?>js/jquery.js"></script>
<script src="<?=$baseurl;?>js/custom.js" defer></script>
<script src="<?=$baseurl;?>js/sweetalert.min.js"></script>  