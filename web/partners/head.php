<?php
	$baseurl = '/partners/'; //live
	// $baseurl = '/cloudkitch/web/partners/'; //local
	$mainbaseurl = '/'; //main baseurl
?>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="theme-color" content="#a38379">
<link href="<?=$baseurl;?>images/favicon.png" rel="icon">
<link href="<?=$baseurl;?>css/sweetalert.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?=$baseurl;?>css/style.css">