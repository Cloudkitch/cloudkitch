<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    error_reporting(E_ALL);
        include 'head.php';
    ?>

    <title>Corporate | Cloudkitch</title>
    <meta name="description" content="">
    <meta name="google-signin-client_id" content="287145767352-qi8asdmobui20671musqaq0jga2ru2re.apps.googleusercontent.com">
</head>

<body class="corporatePage">
    <h1 class="seoTag">Cloud Kitch</h1>
    <?php
    include 'header.php';
    
    if (isset($_SESSION['userid'])){
        if($_SESSION['isCorporateUser'] == 1){
            // echo "Aradhana";
        }
        else{
            // echo "Deowanshi";
            // header_remove();
            // header("Location: $baseurl");
            // echo("<meta http-equiv='refresh' content='1;url=.'>");
            echo '<script>window.location = "'.$baseurl.'";</script>';
        }
    }
    else{
        echo "Deowanshi11";
        // header_remove();
        // header("Location: $baseurl");
        // echo("<meta http-equiv='refresh' content='1;url=.'>");
        echo '<script>window.location = "'.$baseurl.'";</script>';
    }
    ?>
<div class="overlay"></div>
<div class="popup popup-offerings">
    <h2>Choose from our Corporate Offerings</h2>
    <div class="flexBlock">
    <?php
        $corporateid = $_SESSION['corporateid'];
        $query = "SELECT meal_type FROM corporate_mealtype WHERE corporate_id = '$corporateid'";
        $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
        $solutions = array();
        while($row = mysqli_fetch_assoc($result)) {
            $solutions[] = $row['meal_type'];
        }
        if(in_array("personal_meals",$solutions)){
    ?>
        <div class="meals-select meal-select" data-value="personalmeal">
            <!-- <img src="?= $baseurl; ?>images/icons/personal-meals.svg" alt="Personal Meals"> -->
            <svg xmlns="http://www.w3.org/2000/svg" width="50.004" height="50" viewBox="0 0 50.004 50">
                <path d="M47.508,50H2.495A2.5,2.5,0,0,1,.269,46.375L5,36.9A12.435,12.435,0,0,1,16.175,30H33.83a12.435,12.435,0,0,1,11.178,6.9l4.725,9.475a2.476,2.476,0,0,1-.1,2.432A2.511,2.511,0,0,1,47.508,50ZM25,25A12.5,12.5,0,1,1,37.5,12.5,12.515,12.515,0,0,1,25,25Z"/>
            </svg>
            <p class="meal-title">Personal Meals</p>
        </div>
    <?php
        }    ?>
          
<?php   if(in_array("team_meals",$solutions)){
    ?>
        <div class="meals-select" data-value="teammeal">
            <!-- <img src="?= $baseurl; ?>images/icons/team-meals.svg" alt="Team Meals"> -->
            <svg xmlns="http://www.w3.org/2000/svg" width="80.006" height="56.002" viewBox="0 0 80.006 56.002">
                <path d="M28.718,56H3.991A4,4,0,0,1,.43,50.2l5.76-11.56A11.939,11.939,0,0,1,16.926,32H39.04a19.745,19.745,0,0,0-5.962,7.038l-3.8,7.56a12.076,12.076,0,0,0-.56,9.4h0Zm47.3,0H40a4,4,0,0,1-3.561-5.8l4-7.6A11.97,11.97,0,0,1,50.965,36H65.049a12,12,0,0,1,10.723,6.6l3.8,7.6A4,4,0,0,1,76.013,56Zm-18-28h-.04a10.021,10.021,0,1,1,.04,0ZM28,24A12,12,0,1,1,40,12,12.018,12.018,0,0,1,28,24Z" transform="translate(0 0)"/>
            </svg>
            <p class="meal-title">Team Meals</p>
        </div>
    <?php
        }    ?>
       
  <?php   if(in_array("event_meals",$solutions)){
    ?>
        <div class="meals-select" data-value="corporatemeal">
            <!-- <img src="<?= $baseurl; ?>images/icons/corporate-events.svg" alt="Corporate Events"> -->
            <svg xmlns="http://www.w3.org/2000/svg" width="78.333" height="54.835" viewBox="0 0 78.333 54.835">
                <path d="M76.376,54.835H1.96A1.962,1.962,0,0,1,0,52.875V48.959A1.962,1.962,0,0,1,1.96,47H3.916V43.083A35.225,35.225,0,0,1,35.25,8.069V1.96A1.962,1.962,0,0,1,37.21,0h3.916a1.96,1.96,0,0,1,1.956,1.96V8.069A35.222,35.222,0,0,1,74.416,43.083V47h1.96a1.96,1.96,0,0,1,1.956,1.96v3.916A1.96,1.96,0,0,1,76.376,54.835Zm-37.21-39.17A27.449,27.449,0,0,0,11.749,43.083V47H66.584V43.083A27.449,27.449,0,0,0,39.166,15.665Z"/>
            </svg>
            <p class="meal-title">Corporate Events</p>
        </div>
    <?php
        } ?>
        
        <?php 
        if(in_array("cafeteria",$solutions)){
    ?>
        <div class="meals-select" data-value="cafeteria"> 
            <!-- <img src="<?= $baseurl; ?>images/icons/cafeteria-menu.svg" alt="Cafeteria Menu"> -->
            <svg xmlns="http://www.w3.org/2000/svg" width="79.999" height="64.001" viewBox="0 0 79.999 64.001">
                <path d="M46.361,64H33.638a3.973,3.973,0,0,1-2.8-1.159L29.16,61.16A3.977,3.977,0,0,0,26.359,60H4a4,4,0,0,1-4-4V4A4,4,0,0,1,4,0H28A15.991,15.991,0,0,1,40,5.479,16.006,16.006,0,0,1,52,0H76a4,4,0,0,1,4,4V56a4,4,0,0,1-4,4H53.64a4.032,4.032,0,0,0-2.8,1.159l-1.678,1.681A4.032,4.032,0,0,1,46.361,64ZM52,8a8.008,8.008,0,0,0-8,8v34.16A16.068,16.068,0,0,1,52,48H72V8ZM8,8V48H28a16.044,16.044,0,0,1,8,2.16V16a8.008,8.008,0,0,0-8-8Z"/>
            </svg>
            <p class="meal-title">Cafeteria Menu</p>
        </div>
    <?php   ?>
       
  <?php  }
        ?>
    </div>
    <?php
    ?>
    <button type="button" id="chooseProcedBtn" class="proceed-btn" onclick="chooseOffering()">Proceed</button>
</div>

    <div class="popup popup-order addtocartpopup">
        <div class="popup-wrapper">
            <div class="popup-close">
                <img src="<?= $baseurl; ?>images/icons/close.svg" alt="close">
            </div>
            <div class="popup-item">
                <h2 class="cuisineTitle" id="cuisineTitle1">Veg Christmas Buffet B</h2>
                <input type="hidden" id="cuisine_id">
                <input type="hidden" id="kitchen_open">
                <input type="hidden" id="kitchen_close">
                <!-- <p>Nine course mini buffet with 3 salad, 4 main dishes and 2 desserts.</p> -->
                <div class="cuisine-details">
                    <h4>What’s included:</h4>
                    <div id="cuisine-details">
                    </div>
                    <!-- <ul class="menu-list">
                            <li><p>Watermelon & Feta Salad drizzled with black tea vinaigrette (Vegetarian) </p></li>
                            <li><p>Smoked Duck Salad with roasted cashew and citrus vinaigrette </p></li>
                            <li><p>Tamarind Vinaigrette Capellini tossed with edamame and herbed cherry tomatoes (Vegetarian) </p></li>
                            <li><p>Christmas Spiced Rice with thyme roasted pumpkin and pomegranate (Vegetarian) </p></li>
                            <li><p>Roasted Winter Vegetables with charred pineapple and sweet prune sprinkles (Vegetarian) </p></li>
                            <li><p>Cinnamon Sugar Glazed Chicken with ginger flower and almond flakes </p></li>
                            <li><p>Eggless Eggnog Dory with caramelised onions and fresh cherry tomatoes </p></li>
                            <li><p>Holiday Walnut Cake with rhubarb cherry compote (Vegetarian) </p></li>
                            <li><p>Christmas Cookies with a Grain surprise (Vegetarian) </p></li>
                        </ul> -->
                    
                    <h4 id="addonheading">Add on Packages</h4>
                    <!-- Add on Packages (Multipe select) -->
                    <div class="checkboxWrap" id="addonlist">

                    </div>


                    <h4>Special Instructions</h4>
                    <textarea name="special-instrustion" class="custom-textarea" id="special-instrustion" rows="8"></textarea>
                </div>
            </div>
            <div class="popup-item">
                <img src="<?= $baseurl; ?>images/cuisine/corporate/1.jpg" alt="Cuisine" class="order-img" id="order-img_cuisin">
                <div class="cuisineType">
                    <p id="veg_val"><img src="<?= $baseurl; ?>images/icons/veg.svg" alt="Veg">4 course meal</p>
                </div>
                <div class="priceBlock">
                    <p class="price">&#8377; <span id="pop_price">1,550</span> <span class="price-gst">GST</span></p>
                    <p class="label-gray" id="mim_order">Min Order 25pax</p>
                </div>
                <div class="quantityBlock">
                    <div class="timeBlock">
                        <p class="timeStat label-gray"><span id="leadtime"></span> hours lead time</p>
                    </div>
                </div>
                <div class="order-date-time">
                    <div class="order-date">
                        <div class="form-group form-legend">
                            <input type="text" id="datepicker" class="form-input-legend" placeholder="Select Date">
                        </div>
                    </div>
                    <div class="order-time">
                        <div class="form-group form-legend">
                            <input type="text" class="form-input-legend timepicker" id="order_time" placeholder="Select Time" />
                        </div>
                    </div>
                </div>
                <input id="teamMealMinQty1" type="hidden" class="quantity">
                <div class="order-quantity">
                    <a href="#" class="quantity-icon sub">
                        <p>-</p>
                    </a>
                    <input id="teamMealMinQty" type="text" class="quantity-input quantity" readonly>
                    <a href="#" class="quantity-icon add active">
                        <p>+</p>
                    </a>
                </div>
                
                <input type="hidden" id="validateflag" value="1">
                <div class="addBtn">
                    <p class="addToCart" id="addtocartBtn" onclick="corporatemealorder()"><img src="<?= $baseurl; ?>images/icons/addCart.svg" alt="download">ADD to cart</p>
                    <span id="quterror" style="color:red"></span>
                    
                </div>
               
            </div>
        </div>
        <div class="bg-grey">
            <!-- <p>Terms and Conditions para will follow here if any, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p> -->
        </div>
    </div>
<!-- Cafeteria popup -->
    <div class="popup popup-order cafeteria-popup" style="display:none">
        <div class="popup-wrapper">
            <div class="popup-close">
                <img src="<?= $baseurl; ?>images/icons/close.svg" alt="">
            </div>
            <div class="popup-item">
                <h2 class="cuisineTitle" id="cuisineTitleCaf">Veg Christmas Buffet B</h2>
                <p style="color:#F68F30" id="kitchen_name_caf">Restro Name</p>
                <p id="cuisine-details_caf"></p>
            </div>
            <div class="popup-item">
                <img id="cuisine_img_caf" src="<?= $baseurl; ?>images/cuisine/corporate/1.jpg" alt="Cuisine" class="order-img">
                <div class="cuisineType">
                    <p id="veg_val_caf"><img src="<?= $baseurl; ?>images/icons/veg.svg" alt="Veg">Veg</p>
                </div>
                <div class="priceBlock">
                    <p class="price">&#8377;<span id="price_caf"> 1,550 </span> <span class="price-gst">+ GST</span></p>
                    <p class="label-gray" id="serve_caf">Serves 2</p>
                </div>
            </div>
        </div>
    </div>

    <section class="banner topSection" >
        <h2 style="display:none;">Cloudkitch</h2>
        <div class="homeSlider" id="homeSlider" style="display: none">
            <?php
                $query = "SELECT * FROM banners";
                $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                while($row = mysqli_fetch_assoc($result))
                {
                    ?>
                    <div class="homeSlide">
                        <img src="<?=$row['image']?>" alt="Banner">
                    </div>
                    <?php
                }
            ?>
        </div>
    </section>
    
    <section class="section" id="ScrollUpToDiv" >
        <div class="filterWrap" >
            <div class="titleWrap">
                <h2 class="meal-order">Personal Meals</h2>
                <p class="meal-text">Courses to Choose From</p>
            </div>
            <div class="filterBtn corpfilterBtn"></div>
            <div class="filterContainer">
                <div class="searchWrap menuSearch">
                    <form>
                        <input type="text" id="search_inner" onkeyup="getCuisinesFilter()" placeholder="Search Hot Favourites">
                        <input type="hidden" id="kitchen_id">
                    </form>
                </div>
                <div class="buttonWrap">
                    <p class="btn borderBtn offersBtn" onclick="getOffer()"><img src="<?= $baseurl; ?>images/icons/discount.svg" alt="offers">All Offers</p>
                    <div class="filter-check">
                        <input class="custom-radio" type="checkbox" onchange="getCuisinesFilter()" id="checkbox_veg" name="pure_veg" value="PURE VEG">
                        <label for="checkbox_veg" class="btn radio-label">
                            <img src="<?= $baseurl; ?>images/icons/leaf.svg" alt="Veg">VEG
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="menuWrap autoHeight">
            <div class="cuisineWrap card restrocuisineWrap">
                <h3 class="cusineTitle">
                    <span class="cusine-first">
                        <img src="<?= $baseurl; ?>images/icons/cuisine.svg" alt="Course">Course
                    </span>
                    <span class="cusine-second">
                        <span class="closeFilter">Close X</span>
                    </span>
                </h3>
                <div class="checkWrap" >
                    <ul id="categories">

                    </ul>
                </div>
            </div>
            <div class="cuisineWrapper cuisineWrappernotflex">
                <div class="restroWrap homerestroSlider" id="kitchens"></div>
                <div class="cuisineContainer heightContainer">  
                    <div id="cuisines"></div>
                </div>
                <span data-value="1" id="loadmoredata" class="loadmore" style="border: none;"></span>
            </div>
        </div>
    </section>
    <?php
    include 'footer.php';
    ?>

    <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?= $baseurl; ?>js/timepicki.js"></script> -->
    <script>
        // $(".cuisineWrap ul li").on("click", function() {
        //     $(".cuisineWrap ul li").removeClass("activeTab");
        //     $(this).addClass("activeTab");
        // });

       
        $(document).ready(function() {
            $("#homeSlider").css("display","block");
            $(".overlay").css("display","none");
            $(".popup-offerings").css("display","none");
            getCategories('1');
            getKitchens();
            getCuisines();
            
            $('#search_inner').keypress(function(event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                }
             });

            var name = "mealstype=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            

            var t = '<?php echo isset($_COOKIE["mealstype"]) ?>';
            if(t == 1){
                    $(".overlay").css("display","none");
                    $(".popup-offerings").css("display","none");
                    changeChooseOffering('<?php echo $_COOKIE["mealstype"]; ?>');
            }else{
                        $(".overlay").css("display","block");
                        $(".popup-offerings").css("display","block");
            }

        });

        function adjustHeight() {
            $(".cuisineTitle").matchHeight();
            $(".cuisineImgWrap").matchHeight();  
            $('.restroWrapContent').matchHeight();
            $('.cuisineDesc').matchHeight();
            $(".cuisineRestroTitle").matchHeight();
            $(".cuisineData").matchHeight();
        }

        function slideCat(){
            $(".cuisineWrap ul li").on("click", function() {
                $(".cuisineWrap ul li").removeClass("activeTab");
                $(this).addClass("activeTab");
            });
        }

        jQuery(
            function($)
            {
                $('.cuisineContainer').bind('scroll', function()
                    {
                        if($(this).scrollTop() + $(this).innerHeight()>=$(this)[0].scrollHeight)
                        {
                            var loadlimit = Number($("#loadmoredata").attr("data-value"));
                                    if ($("#checkbox_veg").prop("checked") == true) {
                                        var type = "veg";
                                    } else {
                                        var type = "";
                                    }
                                    var keyword = $("#search_inner").val();
                                    var category_id = $('.checkWrap  li.activeTab').attr("data-id");
                                    $("#loadmoredata").attr("data-value",loadlimit+1);
                                    var mealtype = $("#meals_check").val();
                                    var pagedata = {
                                        "keyword": keyword,
                                        "categories": category_id,
                                        "sortbycost": "",
                                        "type": type,
                                        "limit": loadlimit
                                    };
                                    if (mealtype == 'corporate_meals') {
                                        $.ajax({
                                            url: serviceurl + 'cuisines',
                                            type: 'POST',
                                            data: JSON.stringify(pagedata),
                                            datatype: 'JSON',
                                            async: false,
                                            success: function(data) {
                                                var value = JSON.parse(data);
                                                var html = "";
                                                if (value.cuisines.length > 0) {
                                                    for (var i = 0; i < value.cuisines.length; i++) {
                                                            html += '<div class="cuisine card"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '</p><p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="hurryBlockContainer"><p>Customisable</p></div><div class="hurryBlockContainer">';
                                                            var cuisintype = value.cuisines[i].type.split(",");
                                                            if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                                                html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                                            } else {
                                                                html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                                            }
                                                            
                                                            //html += '</div></div><div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div></div></div></div>';
                                                            var start = value.cuisines[i].kitchen_workingstart;
                                                                var end = value.cuisines[i].kitchen_workingend;

                                                                var d = new Date();
                                                                        var datesss = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                                                                        var min = new Date(datesss + ' ' + start);
                                                                        var max = new Date(datesss + ' ' + end);

                                                                        if(min.getTime() > d.getTime() || d.getTime() >  max.getTime()){
                                                                            html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '">Kitchen closed</p></div>';
                                                                        }
                                                                        else{
                                                                            html += '</div></div><div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div></div></div></div>';
                                                                        }

                                                        }
                                                        if(value.cuisines.length < 30){
                                                            $("#loadmoredata").hide();
                                                        }
                                                        $("#cuisines").html(html);
                                                        addedIncart();
                                                        hoverCuisine();
                                                        adjustHeight();
                                                } else {
                                                
                                                    $("#loadmoredata").hide();
                                                }

                                            }
                                        });
                                    }else if(mealtype == 'team_meals'){

                                    }else{

                                    }
                              
                                }
                            })
                    }
        );

        function getKitchens() {

            $.ajax({
                url: serviceurl + 'kitchens',
                type: 'POST',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                    var vl = value.kitchens.length;
                    for (var i = 0; i < vl; i++) {
                        // html += '<div class="restro card" onclick="getKitchesData(' + value.kitchens[i].id + ')" id="kitchenDiv-' + value.kitchens[i].id + '"><div class="restroWrapContent"><img src="' + value.kitchens[i].image + '" alt="' + value.kitchens[i].name + '" /><div class="context"><h3>' + value.kitchens[i].name + '</h3><a href="corporate-kitchen/' + value.kitchens[i].id + '/' + value.kitchens[i].name1 + '"><h6 class="visitRestro">Visit Restaurant ></h6></a></div></div></div>';
                        html += '<div class="restro card" onclick="getKitchesData(' + value.kitchens[i].id + ')" id="kitchenDiv-' + value.kitchens[i].id + '"><div class="restroWrapContent"><img src="' + value.kitchens[i].image + '" alt="' + value.kitchens[i].name + '" /><div class="context"><h3>' + value.kitchens[i].name + '</h3><a href="javascript:goToKitchenCorporate(' + value.kitchens[i].id + ',' + '\'' + value.kitchens[i].name1.replace("&#39;","\\'") + '\''+')"><h6 class="visitRestro">Visit Restaurant ></h6></a></div></div></div>';
                        $("#kitchens").html(html);
                    }
                }
            });
        }
        

        function getCuisines() {
            var kid = $("#kitchen_id").val();
            var category_id = $('.checkWrap  li.activeTab').attr("data-id");
            if(kid != ""){
                getKitchesData(kid);
                return;
            }
           
            var pagedata = {
                "keyword": "",
                "categories": category_id,
                "sortbycost": "",
                "type": "",
                "kitchen_id":""
            };
            $.ajax({
                url: serviceurl + 'cuisines',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                    for (var i = 0; i < value.cuisines.length; i++) {
                            //  html += '<div class="cuisine card"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '</p><p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="hurryBlockContainer"><p>Customisable</p></div><div class="hurryBlockContainer">';
                            // var cuisintype = value.cuisines[i].type.split(",");
                            // if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                            //     html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                            // } else {
                            //     html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                            // }
                            // html += '</div></div><div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div></div></div></div>';
                            if(value.cuisines[i].isCustomisable == '1'){
                                html += '<div class="cuisine card customize_cusine" onclick="customizecuisine('+ value.cuisines[i].id +')">';
                                }else{
                                    html += '<div class="cuisine card">';
                                }
                            
                                html += '<div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description + '</p><div class="priceBlock">';
                                if(value.cuisines[i].discountprice != '' && value.cuisines[i].offer != ""){
                                    html += '<p class="price"><strike>₹ ' + value.cuisines[i].price + '</strike>  ₹ ' + value.cuisines[i].discountprice + '</p>';
                                }else{
                                    html += '<p class="price">₹ ' + value.cuisines[i].price + '</p>';
                                }
                                
                                html += '<p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock" id="cuisineCatDiv-' + value.cuisines[i].id + '"><div class="hurryBlockContainer">';
                                if(value.cuisines[i].isCustomisable == '1'){
                                    html += '<p>Customizable</p>';
                                }else{
                                    html += '<p> </p>';
                                }
                                html += ' </div><div class="hurryBlockContainer">';
                                var offer = "";
                                if (value.cuisines[i].offer != "") {
                                    offer = '<span>'+ + value.cuisines[i].offer + '% off</span>';
                                }

                                var cuisintype = value.cuisines[i].type.split(",");
                                if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                    html += '<p class="food-cat">'+offer+'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                } else {
                                    html += '<p class="food-cat nonfood-cat">'+offer+'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                }
                                
                                html += '</div></div>';
                                if(value.cuisines[i].isCustomisable != '1'){
//                                    html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="0" max="3" readonly="readonly"><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';
                                                                var start = value.cuisines[i].kitchen_workingstart;
                                                                var end = value.cuisines[i].kitchen_workingend;

                                                                var d = new Date();
                                                                        var datesss = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                                                                        var min = new Date(datesss + ' ' + start);
                                                                        var max = new Date(datesss + ' ' + end);

                                                                        if(min.getTime() > d.getTime() || d.getTime() >  max.getTime()){
                                                                            html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '">Kitchen closed</p></div>';
                                                                        }
                                                                        else{
                                                                            html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="0" max="3" readonly><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';
                                                                        }
                                }
                                html += '</div></div></div>';
                                
                        }
                        if(value.cuisines.length < 30){
                            $("#loadmoredata").hide();
                        }
                        $("#cuisines").html(html);
                        addedIncart();
                        hoverCuisine();
                        addButtonFunction();
                        adjustHeight();

                }
            });

        }

        function getCafeteria() {
            var category_id = $('.checkWrap  li.activeTab').attr("data-id");
            var kid = $("#kitchen_id").val();
            if(kid != ""){
                getKitchesData(kid);
                return;
            }
           
            var pagedata = {
                "keyword": "",
                "categories": category_id,
                "sortbycost": "",
                "type": "",
                "kitchen_id":""
            };
            $.ajax({
                url: serviceurl + 'cafeteria',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";
                    if(value.cuisines.length > 0){
                        for (var i = 0; i < value.cuisines.length; i++) {
                                html += '<div class="cuisine card" onclick="cafeteriaPopup('+value.cuisines[i].id+');"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description1 + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '</p><p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="hurryBlockContainer"></div><div class="hurryBlockContainer">';
                                var cuisintype = value.cuisines[i].type.split(",");
                                if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                    html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                } else {
                                    html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                }
                                html += '</div></div></div></div></div>';
                        
                            }
                            if(value.cuisines.length < 30){
                                $("#loadmoredata").hide();
                            }
                            $("#cuisines").html(html);
                        
                            adjustHeight();

                        
                    }else{
                        $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");  
                    }
                }
            });

        }

        function getTeamMeals() {
            var category_id = "";
            var kid = $("#kitchen_id").val();
            category_id = $('.checkWrap  li.activeTab').attr("data-id");
            if(kid != ""){
                getKitchesData(kid);
                return;
            }
            var pagedata = {
                "keyword": "",
                "categories": category_id,
                "sortbycost": "",
                "type": "",
                "kitchenid":""
            };
            $.ajax({
                url: serviceurl + 'corporatemeals',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    console.log(value);
                    var html = "";
                    if(value.cuisines.length > 0){
                        // for (var i = 0; i < value.cuisines.length; i++) {
                        //     html += '<div class="cuisine card corporate-card" onclick="corporate_order(\'' + value.cuisines[i].id + '\')"><div class="cuisineImgWrap"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><div class="cuisineType"><p>';
                        //     var cuisintype = value.cuisines[i].type.split(",");
                        //     if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                        //         html += '<img src="images/icons/nonveg.svg" alt="Non Veg">';
                        //     } else {
                        //         html += '<img src="images/icons/veg.svg" alt="Veg">';
                        //     }
                        //     html += value.cuisines[i].kitchen_name + '</p></div><div class="priceBlock"><p class="price">&#8377; ' + value.cuisines[i].price + ' <span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[i].minorderquantity + '</p></div><div class="quantityBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[i].leadtime + ' hours lead time</p><p class="state">New</p></div></div></div></div>';

                        // }

                        // $("#cuisines").html(html);
                        // addedIncart();
                        // hoverCuisine();
                        for (var i = 0; i < value.cuisines.length; i++) {
                             html += '<div class="cuisine card" onclick="corporate_order(\'' + value.cuisines[i].id + '\')"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description1 + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '<span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[i].minorderquantity + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[i].leadtime + ' hours lead time</p></div><div class="hurryBlockContainer">';
                            var cuisintype = value.cuisines[i].type.split(",");
                            if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                            } else {
                                html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                            }
                            html += '</div></div></div></div></div>';
                      
                        }
                        // if(value.cuisines.length < 30){
                        //     $("#loadmoredata").hide();
                        // }
                        $("#cuisines").html(html);
                        addedIncart();
                        adjustHeight();
                    }else{
                        $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");  
                    }

                }
            });
        }

        function getEventMeals() {
            var category_id = "";
            category_id = $('.checkWrap  li.activeTab').attr("data-id");
            var kid = $("#kitchen_id").val();
            if(kid != ""){
                getKitchesData(kid);
                return;
            }

            var pagedata = {
                "keyword": "",
                "categories": category_id,
                "sortbycost": "",
                "type": "",
                "kitchenid":""
            };
            $.ajax({
                url: serviceurl + 'eventmeals',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    console.log(value);
                    var html = "";
                    if(value.cuisines.length > 0){
                        for (var i = 0; i < value.cuisines.length; i++) {
                             html += '<div class="cuisine card" onclick="corporate_order(\'' + value.cuisines[i].id + '\')"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description1 + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '<span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[i].minorderquantity + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[i].leadtime + ' hours lead time</p></div><div class="hurryBlockContainer">';
                            var cuisintype = value.cuisines[i].type.split(",");
                            if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                            } else {
                                html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                            }
                            html += '</div></div></div></div></div>';
                      
                        }
                        // if(value.cuisines.length < 30){
                        //     $("#loadmoredata").hide();
                        // }
                        $("#cuisines").html(html);
                        addedIncart();
                        adjustHeight();
                    }else{
                        $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");
                    }

                }
            });
        }

        function getCategories(pagetype) {
            var pagedata = {
                "pagetype": pagetype
            };
            $.ajax({
                url: serviceurl + 'homecategories',
                type: 'POST',
                data: JSON.stringify(pagedata),
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    var html = "";                   
                    for (var i = 0; i < value.categories.length; i++) {
                        var id = value.categories[i].id;
                        var category = value.categories[i].category;
                            // html += '<div class="checkboxWrap"><label class="container">' + category + '<input type="checkbox" id="category" name="category[]" onchange="getCuisinesFilter()" value="' + id + '"><span class="checkmark"></span></label></div>';
                            if(i == 0){
                                html += '<li class="snacks-wrapper activeTab" id="category-' + id + '" onclick="addActivetab(' + id + ')" data-id="' + id + '"><a><p><span>' + category + '</span></span></p></a></li>';
                            }else{
                                html += '<li class="snacks-wrapper" id="category-' + id + '" onclick="addActivetab(' + id + ')" data-id="' + id + '"><a><p><span>' + category + '</span></span></p></a></li>';
                            }

                    }
                    $("#categories").html(html);
                }
            });
        }
        function addActivetab(catid) {
            var categoryid = catid;
            $('.checkWrap  li.activeTab').removeClass('activeTab');
            $("#category-"+catid).addClass("activeTab");
            getCuisinesFilter();
        }

        function getCuisinesFilter() {
            //
            var keyword = $("#search_inner").val();
            var sort_by_cost = $("#sort_by_cost").val();
            var keyLength = keyword.length;
            var type = "";
            if ($("#checkbox_veg").prop("checked") == true) {
                var type = "veg";
            } else {
                var type = "";
            }
            var kid = $("#kitchen_id").val();
            if(kid != ""){
                getKitchesData(kid);
                return;
            }
            // var categories = [];
            // $.each($("input[name='category[]']:checked"), function() {
            //     categories.push("'" + $(this).val() + "'");
            // });
            // var categorydata = categories.join(",");
            var category_id = $('.checkWrap  li.activeTab').attr("data-id");

            var pagedata = {
                "keyword": keyword,
                "categories": category_id,
                "sortbycost": sort_by_cost,
                "type": type,
                "kitchenid": kid
            };
            //$(".loader").show();
            var mealtype = $("#meals_check").val();
            if (mealtype == 'corporate_meals') {
                $.ajax({
                    url: serviceurl + 'cuisines',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: true,
                    success: function(data) {
                        var value = JSON.parse(data);
                        var html = "";
                       // $(".loader").hide();
                        if (value.cuisines.length > 0) {
                            for (var i = 0; i < value.cuisines.length; i++) {
                               
                                if(value.cuisines[i].isCustomisable == '1'){
                                html += '<div class="cuisine card customize_cusine" onclick="customizecuisine('+ value.cuisines[i].id +')">';
                                }else{
                                    html += '<div class="cuisine card">';
                                }
                            
                                html += '<div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description + '</p><div class="priceBlock">';
                                if(value.cuisines[i].discountprice != '' && value.cuisines[i].offer != ""){
                                    html += '<p class="price"><strike>₹ ' + value.cuisines[i].price + '</strike>  ₹ ' + value.cuisines[i].discountprice + '</p>';
                                }else{
                                    html += '<p class="price">₹ ' + value.cuisines[i].price + '</p>';
                                }
                                html += '<p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock" id="cuisineCatDiv-' + value.cuisines[i].id + '"><div class="hurryBlockContainer">';
                                if(value.cuisines[i].isCustomisable == '1'){
                                    html += '<p>Customizable</p>';
                                }else{
                                    html += '<p> </p>';
                                }
                                html += ' </div><div class="hurryBlockContainer">';
                                var offer = "";
                                if (value.cuisines[i].offer != "") {
                                    offer = '<span>'+ + value.cuisines[i].offer + '% off</span>';
                                }
                                var cuisintype = value.cuisines[i].type.split(",");
                                    if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                        html += '<p class="food-cat">'+offer+'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                    } else {
                                        html += '<p class="food-cat nonfood-cat">'+offer+'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                    }
                                
                                html += '</div></div>';
                                if(value.cuisines[i].isCustomisable != '1'){
                                    //html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="0" max="3" readonly="readonly"><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';
                                    var start = value.cuisines[i].kitchen_workingstart;
                                    var end = value.cuisines[i].kitchen_workingend;

                                            var d = new Date();
                                            var datesss = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                                            var min = new Date(datesss + ' ' + start);
                                            var max = new Date(datesss + ' ' + end);

                                            if(min.getTime() > d.getTime() || d.getTime() >  max.getTime()){
                                                html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '">Kitchen closed</p></div>';
                                            }
                                            else{
                                                html += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[i].id + '" onclick="addToCart(' + value.cuisines[i].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[i].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[i].id + '" class="quantity-input" value="1" min="0" max="3" readonly="readonly"><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[i].id + ',\'add\')"><h2>+</h2></a></div>';
                                            }
                                }
                                html += '</div></div></div>';
                                
                                }
                            $("#cuisines").html(html);
                            addedIncart();
                            hoverCuisine();
                            addButtonFunction();
                            adjustHeight();
                        } else {
                            $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");
                        }


                    }
                });
            } else if(mealtype == 'team_meals'){
                $.ajax({
                    url: serviceurl + 'corporatemeals',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        var html = "";
                       // $(".loader").hide();
                        if (value.cuisines.length > 0) {
                            for (var i = 0; i < value.cuisines.length; i++) {
                                html += '<div class="cuisine card" onclick="corporate_order(\'' + value.cuisines[i].id + '\')"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description1 + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '<span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[i].minorderquantity + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[i].leadtime + ' hours lead time</p></div><div class="hurryBlockContainer">';
                                var cuisintype = value.cuisines[i].type.split(",");
                                if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                    html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                } else {
                                    html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                }
                                html += '</div></div></div></div></div>';
                        
                            }
                            // if(value.cuisines.length < 30){
                            //     $("#loadmoredata").hide();
                            // }
                            $("#cuisines").html(html);
                            addedIncart();
                            adjustHeight();
                        } else {
                            $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");
                        }
                    }
                });
            }else if(mealtype == 'event_meals'){
                $.ajax({
                    url: serviceurl + 'eventmeals',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        var html = "";
                       // $(".loader").hide();
                        if (value.cuisines.length > 0) {
                            for (var i = 0; i < value.cuisines.length; i++) {
                                html += '<div class="cuisine card" onclick="corporate_order(\'' + value.cuisines[i].id + '\')"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description1 + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '<span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[i].minorderquantity + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[i].leadtime + ' hours lead time</p></div><div class="hurryBlockContainer">';
                                var cuisintype = value.cuisines[i].type.split(",");
                                if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                    html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                } else {
                                    html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                }
                                html += '</div></div></div></div></div>';
                            }
                            // if(value.cuisines.length < 30){
                            //     $("#loadmoredata").hide();
                            // }
                            $("#cuisines").html(html);
                            addedIncart();
                            adjustHeight();
                        } else {
                            $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");
                        }
                    }
                });
            }else if(mealtype == 'cafeteria'){
                $.ajax({
                    url: serviceurl + 'cafeteria',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        var html = "";
                        if(value.cuisines.length > 0){
                            for (var i = 0; i < value.cuisines.length; i++) {
                                html += '<div class="cuisine card" onclick="cafeteriaPopup('+value.cuisines[i].id+');"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description1 + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '</p><p>Serves ' + value.cuisines[i].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="hurryBlockContainer"></div><div class="hurryBlockContainer">';
                                var cuisintype = value.cuisines[i].type.split(",");
                                if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                    html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                } else {
                                    html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                }
                                html += '</div></div></div></div></div>';
                        
                            }
                          
                            $("#cuisines").html(html);
                        
                            adjustHeight();
                        } else {
                            $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");
                        }
                       

                    }
                });
            }

        }
        /*
        By:Jyoti Vishwakarma
        Description: display meals description in popup
        */
        function corporate_order(id){
            var pagedata = {
                "id": id
            };
            $.ajax({
                url: serviceurl + 'corporatemeals_detail',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    console.log(value.cuisines[0].name);
                    var val = "";
                    $("#cuisine_id").val(value.cuisines[0].id);
                    $("#kitchen_open").val(value.cuisines[0].kitchen_open);
                    $("#kitchen_close").val(value.cuisines[0].kitchen_close);

                    if (value.cuisines[0].type.includes('Non Veg')) {
                        val = '<img src="images/icons/nonveg.svg" alt="Non Veg">' + value.cuisines[0].kitchen_name;
                    } else {
                        val = '<img src="images/icons/veg.svg" alt="Veg">' + value.cuisines[0].kitchen_name;
                    }
                    $("#cuisineTitle1").html(value.cuisines[0].name);
                    $("#order-img_cuisin").attr("src", value.cuisines[0].image);
                    $("#veg_val").html(val);
                    $("#cuisine-details").html(value.cuisines[0].description);
                    $("#pop_price").html(value.cuisines[0].price);
                    $("#mim_order").html("Min Order " + value.cuisines[0].minorderquantity + "pax");
                    $("#leadtime").html(value.cuisines[0].leadtime);
                    $("#teamMealMinQty").val(value.cuisines[0].minorderquantity);
                    $("#teamMealMinQty1").val(value.cuisines[0].minorderquantity);
                    var addon = "";
                    
                        var addonshtml = "";
                    if(value.cuisines[0].sections.length == 0){
                        $("#addonheading").hide();
                    }else{
                        $("#addonheading").show();
                    }    
                    for(var i=0;i<value.cuisines[0].sections.length;i++){
                        console.log(value.cuisines[0].sections[i].title);
                        var section_id = value.cuisines[0].sections[i].section_id;
                        var noofchoice = value.cuisines[0].sections[i].noofchoice;
                        addonshtml += '<h4 >'+value.cuisines[0].sections[i].title+'</h4><input id="da'+section_id+'" type="hidden" value="'+noofchoice+'"><input id="das'+section_id+'" type="hidden" value="'+noofchoice+'"><span id="counterror'+section_id+'" style="color:red;display:none">error</span><div id="prt'+section_id+'">';
                        var addon = value.cuisines[0].sections[i].addons;
                        

                        if(value.cuisines[0].sections[i].choicetype == 1){
                            addonshtml += '<div class="section-div" id="'+section_id+'"></div>';
                            for(var j=0;j<addon.length;j++){

                                addonshtml += '<label class="container">';
                                addonshtml += '<p>' + addon[j].detail + ' &#8377;' + addon[j].price + '</p>';
                                addonshtml += '<input type="checkbox" onclick="checkval('+section_id+','+addon[j].addon_id+')" id="'+addon[j].addon_id+'" class="addons_corporate" name="addon"  data-id="' + addon[j].addon_id + '" value="' + addon[j].addon_id + '">';
                                addonshtml += '<span class="checkmark"></span>';
                                addonshtml += '</label>';
                            }

                        }else{
                            addonshtml += '<div class="section-divs"></div>';
                            for(var j=0;j<addon.length;j++){
                                    var checked = "";
                                    if(j==0){
                                        checked = "checked";
                                    }
                                    addonshtml +=   ' <div class="checkboxWrap">';
                                    addonshtml +=    '<label class="container radioContainer">';
                                    addonshtml +=        '<p>' + addon[j].detail + ' &#8377;' + addon[j].price + '</p>';
                                    addonshtml +=       '<input type="radio" name="addOnPacagesRadio-'+ value.cuisines[0].sections[i].section_id +'" value="' + addon[j].addon_id + '" ' + checked + '>';
                                    addonshtml +=        '<span class="checkmark"></span>';
                                    addonshtml +=    '</label>';
                                    addonshtml +=  '</div>'; 
                            }

                        }
                        addonshtml +=  '</div>'; 
                    }

                    $("#addonlist").html(addonshtml);
                    $("#datepicker").val("");
                    $("#order_time").val("");
                    $("#quterror").hide();
                    $("#quterror").text("");
                    $(".addtocartpopup, .overlay")
                        .delay(400)
                        .fadeIn();

                }
            });

        }
        /*
        By:Jyoti Vishwakarma
        Description: Validate Order requirement to palce order
        */
        function corporatemealorder() {

            var cuisine_id = $("#cuisine_id").val();
            var date = $("#datepicker").val();
            var time = $("#order_time").val();           
            var qty = parseInt($("#teamMealMinQty").val());
            var qty1 = parseInt($("#teamMealMinQty1").val());

            var numItems = $('.section-div').length
            if(numItems > 0){
                $('.section-div').each(function(){
                   var section_id = $(this).prop('id');
                   var minreq = $("#da"+section_id).val(); 
                   var qut1 = $("#das"+section_id).val();
                  
                    if(qut1 != 0){
                        $("#counterror"+section_id).css("display","block");
                        $("#counterror"+section_id).text("Minimum selection should be "+minreq+".");
                        $("#validateflag").val(1);
                    }else{
                        $("#counterror"+section_id).css("display","none");
                        $("#validateflag").val(0);
                    }
                   
                });
            }else{
                $("#validateflag").val(0);
            }
            
            var leadtime = parseInt($("#leadtime").html());
            var today = new Date();
            var minord = new Date(today.setHours(today.getHours()+leadtime));
            var orddate = date + ' ' + time;
            orddate = new Date(orddate);
            console.log($("#kitchen_open").val());
            console.log($("#kitchen_close").val());
            var min = new Date(date + ' ' + $("#kitchen_open").val());
            var max = new Date(date + ' ' + $("#kitchen_close").val());

            if(qty1 > qty || date == "" || time == ""){
                if(date == ""){
                    $("#quterror").show();
                    $("#quterror").text("Please select date.");
                }else if( time == ""){
                    $("#quterror").show();
                    $("#quterror").text("Please select time.");
                }else if(qty1 > qty){
                    $("#quterror").show();
                    $("#quterror").text("Min Order "+qty1+"pax");
                }
                return;
            }else if(minord.getTime() > orddate.getTime()){
                $("#quterror").show();
                $("#quterror").text("Order time should be greater than leadtime");
                return;
            }else  if(min.getTime() > orddate.getTime() || orddate.getTime() >  max.getTime()){
                $("#quterror").show();
                $("#quterror").text("Order time should be between "+ $("#kitchen_open").val() +" and "+ $("#kitchen_close").val());
                return;
            }else{
                $("#quterror").hide();
            }

            var special_instruction = $("#special-instrustion").val();

            var items = $(".addons_corporate");
            var selectedaddons = "";
            for (var i = 0; i < items.length; i++) {
                if (items[i].type == 'checkbox' && items[i].checked == true){
                        selectedaddons += items[i].value + ",";
                }
            }

            $('#addonlist input[type=radio]:checked').each(function() {
                 selectedaddons += this.value + ",";
            });

            addToCartCorporate(cuisine_id, date, time, qty, special_instruction, selectedaddons);
            // alert(special_instruction);

        }
        /*
        By:Jyoti Vishwakarma
        Description: Add to cart meals
        */
        function addToCartCorporate(id, date, time, qty, special_instruction, selectedaddons) {
            var count = 1;
            var data = {
                "id": id,
                "date": date,
                "time": time,
                "qty": qty,
                "special_instruction": special_instruction,
                "selectedaddons": selectedaddons
            };
            if($("#validateflag").val() == 0){
                    $.ajax({
                    url: serviceurl + 'addtocartCorporate',
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        if (value.status == 'success') {
                            var cartCount = $(".notiCount").html();
                            showCartData();
                            $(".popup-order, .overlay")
                            .delay(400)
                            .fadeOut();
                            $(".toast").fadeIn();
                            setTimeout(function(){
                                $(".toast").fadeOut();
                            }, 3000);
                            //$(".notiCount").html(parseInt(cartCount) + 1);
                        } else {

                            $('.overlay,.cartPopup').fadeIn();
                            setTimeout(function() {
                                $('.overlay,.cartPopup').fadeOut();
                            }, 1200);
                        }
                    }
                });
            }
            else{
                // $("#quterror").text("Min Order "+qty1+"pax");
            }
        }
        /*
        By:Jyoti Vishwakarma
        Description: fetch kitchen cuisines
        */
        function getKitchesData(kitchenid) {
            var kid = $("#kitchen_id").val();
            $("#kitchenDiv-"+kid).removeClass("activeRestro");
            $("#kitchenDiv-"+kitchenid).addClass("activeRestro");
            $("#kitchen_id").val(kitchenid);
            var mealtype = $("#meals_check").val();
            if ($("#checkbox_veg").prop("checked") == true) {
                var type = "veg";
            } else {
                var type = "";
            }
            keyword = "";
            var keyword = $("#search_inner").val();
            var pagedata = {
                "kitchenid": kitchenid,
                "keyword": keyword,
                "categories": "",
                "sortbycost": "",
                "type": type,
                "mealtype":mealtype
            };
            $.ajax({
                    url: serviceurl + 'kitchen_page',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        var categories = "";
                        for (var i = 0; i < value.categories.length; i++) {
                            if (i == 0) {
                                categories += '<li class="snacks-wrapper activeTab"><a href="#category' + value.categories[i].categoryid + '"><p><span>' + value.categories[i].categoryname + '</span></span></p></a></li>';
                            } else {
                                categories += '<li class="snacks-wrapper"><a href="#category' + value.categories[i].categoryid + '"><p><span>' + value.categories[i].categoryname + '</span></span></p></a></li>';
                            }
                        }
                        $("#categories").html(categories);
                        if(value.cuisines.length == 0){
                            $("#cuisines").html("<div class='noCuisine'><p>No cuisine found.</p></div>");
                            return;
                        }
                        if(mealtype == 'corporate_meals'){
                            var cuisinesdata = "";
                            for (var j = 0; j < value.cuisines.length; j++) {
                                cuisinesdata += '<div class="cuisine-divider" id="category' + value.cuisines[j].categoryid + '"> <div class="titleBlock"> <h2>' + value.cuisines[j].categoryname + '</h2> <p>' + value.cuisines[j].cuisines.length + ' Items</p> </div><div class="cuisineContainer">';
                            
                                for (var k = 0; k < value.cuisines[j].cuisines.length; k++) {
                                    if(value.cuisines[j].cuisines[k].isCustomisable == '1'){
                                        cuisinesdata += '<div class="cuisine card customize_cusine" onclick="customizecuisine('+ value.cuisines[j].cuisines[k].id +')">';
                                    }else{
                                        cuisinesdata += '<div class="cuisine card">';
                                    }
                                    cuisinesdata += '<div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[j].cuisines[k].image + '" alt="' + value.cuisines[j].cuisines[k].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[j].cuisines[k].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[j].cuisines[k].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[j].cuisines[k].description + '</p><div class="priceBlock">';
                                    if(value.cuisines[j].cuisines[k].discountprice != '' && value.cuisines[j].cuisines[k].offer != ""){
                                        cuisinesdata += '<p class="price"><strike>₹ ' + value.cuisines[j].cuisines[k].price + '</strike>  ₹ ' + value.cuisines[j].cuisines[k].discountprice + '</p>';
                                    }else{
                                        cuisinesdata += '<p class="price">₹ ' + value.cuisines[j].cuisines[k].price + '</p>';
                                    } 
                                    cuisinesdata +='<p>Serves ' + value.cuisines[j].cuisines[k].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="hurryBlockContainer">';
                                    if(value.cuisines[j].cuisines[k].isCustomisable == '1'){
                                        cuisinesdata += '<p>Customizable</p>';
                                    }else{
                                        cuisinesdata += '<p> </p>';
                                    }
                                    cuisinesdata += '</div><div class="hurryBlockContainer">';
                                    var offer = "";
                                    if (value.cuisines[j].cuisines[k].offer != "") {
                                        offer =  '<span>'+value.cuisines[j].cuisines[k].offer + '% off</span>';
                                    }
                                    var cuisintype = value.cuisines[j].cuisines[k].type.split(",");
                                    if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                        cuisinesdata +=  '<p class="food-cat">'+offer +'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                    } else {
                                        cuisinesdata += '<p class="food-cat nonfood-cat">'+offer +'<img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                    }
                                    cuisinesdata += '</div></div>';
                                    if(value.cuisines[j].cuisines[k].isCustomisable != '1'){
                                        //cuisinesdata += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[j].cuisines[k].id + '" onclick="addToCart(' + value.cuisines[j].cuisines[k].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[j].cuisines[k].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[j].cuisines[k].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[j].cuisines[k].id + '" class="quantity-input" value="1" min="1" readonly="readonly"><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[j].cuisines[k].id + ',\'add\')"><h2>+</h2></a></div>';
                                        var start = value.cuisines[j].cuisines[k].kitchen_workingstart;
                                    var end = value.cuisines[j].cuisines[k].kitchen_workingend;

                                    var d = new Date();
                                            var datesss = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                                            var min = new Date(datesss + ' ' + start);
                                            var max = new Date(datesss + ' ' + end);

                                            if(min.getTime() > d.getTime() || d.getTime() >  max.getTime()){
                                                cuisinesdata += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[j].cuisines[k].id + '">Kitchen closed</p></div>';
                                            }
                                            else{
                                                cuisinesdata += '<div class="addBtn" style="display: none;"><p class="addToCart" id="addedTocart-' + value.cuisines[j].cuisines[k].id + '" onclick="addToCart(' + value.cuisines[j].cuisines[k].id + ')"><img src="<?=$baseurl;?>images/icons/addCart.svg" alt="download">ADD to cart</p></div><div class="order-quantity cuisineQty" id="upadteQuantity-' + value.cuisines[j].cuisines[k].id + '"><a class="quantity-icon sub1" onclick="updateCuisineQuantity(' + value.cuisines[j].cuisines[k].id + ',\'less\')"><h2>-</h2></a><input type="number" id="cuisineQty-' + value.cuisines[j].cuisines[k].id + '" class="quantity-input" value="1" min="1" readonly="readonly"><a class="quantity-icon add1 active" onclick="updateCuisineQuantity(' + value.cuisines[j].cuisines[k].id + ',\'add\')"><h2>+</h2></a></div>';
                                            }
                                    }
                                    cuisinesdata += '</div></div></div>';
                                }

                                cuisinesdata += '</div>';
                            }

                            $("#cuisines").html(cuisinesdata);
                            addedIncart();
                            hoverCuisine();
                            addButtonFunction();
                            adjustHeight();
                            slideCat()
                        }else if(mealtype == 'team_meals'){
                            var cuisinesdata ="";
                            for (var i = 0; i < value.cuisines.length; i++) {
                                // html += '<div class="cuisine card" onclick="corporate_order(\'' + value.cuisines[i].id + '\')"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[i].image + '" alt="' + value.cuisines[i].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[i].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[i].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[i].description + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[i].price + '<span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[i].minorderquantity + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[i].leadtime + ' hours lead time</p></div><div class="hurryBlockContainer">';
                                // var cuisintype = value.cuisines[i].type.split(",");
                                // if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                //     html += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                // } else {
                                //     html += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                // }
                                // html += '</div></div></div></div></div>';

                                for (var j = 0; j < value.cuisines.length; j++) {
                                    cuisinesdata += '<div class="cuisine-divider" id="category' + value.cuisines[j].categoryid + '"> <div class="titleBlock"> <h2>' + value.cuisines[j].categoryname + '</h2> <p>' + value.cuisines[j].cuisines.length + ' Items</p> </div><div class="cuisineContainer">';
                                
                                    for (var k = 0; k < value.cuisines[j].cuisines.length; k++) {
                                        
                                        cuisinesdata += '<div class="cuisine card" onclick="corporate_order(\'' + value.cuisines[j].cuisines[k].id + '\')"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[j].cuisines[k].image + '" alt="' + value.cuisines[j].cuisines[k].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[j].cuisines[k].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[j].cuisines[k].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[j].cuisines[k].description + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[j].cuisines[k].price + '<span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[j].cuisines[k].minorderquantity + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[j].cuisines[k].leadtime + ' hours lead time</p></div><div class="hurryBlockContainer">';
                                        
                                        var cuisintype = value.cuisines[j].cuisines[k].type.split(",");
                                        if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                            cuisinesdata += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                        } else {
                                            cuisinesdata += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                        }
                                        cuisinesdata += '</div></div></div></div></div>';
                                       
                                    }

                                    cuisinesdata += '</div>';
                                }
                        
                            }
                            $("#cuisines").html(cuisinesdata);
                            addedIncart();
                            adjustHeight();
                        }else if(mealtype == 'event_meals'){
                            var cuisinesdata ="";
                            for (var j = 0; j < value.cuisines.length; j++) {
                                    cuisinesdata += '<div class="cuisine-divider" id="category' + value.cuisines[j].categoryid + '"> <div class="titleBlock"> <h2>' + value.cuisines[j].categoryname + '</h2> <p>' + value.cuisines[j].cuisines.length + ' Items</p> </div><div class="cuisineContainer">';
                                
                                    for (var k = 0; k < value.cuisines[j].cuisines.length; k++) {
                                        
                                        cuisinesdata += '<div class="cuisine card" onclick="corporate_order(\'' + value.cuisines[j].cuisines[k].id + '\')"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[j].cuisines[k].image + '" alt="' + value.cuisines[j].cuisines[k].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[j].cuisines[k].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[j].cuisines[k].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[j].cuisines[k].description + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[j].cuisines[k].price + '<span class="price-gst">+ GST / pax</span></p><p class="label-gray">Min Qty. ' + value.cuisines[j].cuisines[k].minorderquantity + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"><p class="timeStat label-gray">' + value.cuisines[j].cuisines[k].leadtime + ' hours lead time</p></div><div class="hurryBlockContainer">';
                                        
                                        var cuisintype = value.cuisines[j].cuisines[k].type.split(",");
                                        if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                            cuisinesdata += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                        } else {
                                            cuisinesdata += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                        }
                                        cuisinesdata += '</div></div></div></div></div>';
                                       
                                    }

                                    cuisinesdata += '</div>';
                                }
                        
                            $("#cuisines").html(cuisinesdata);
                            addedIncart();
                            adjustHeight();
                        }else if(mealtype == 'cafeteria'){
                            var cuisinesdata ="";
                            for (var j = 0; j < value.cuisines.length; j++) {
                                    cuisinesdata += '<div class="cuisine-divider" id="category' + value.cuisines[j].categoryid + '"> <div class="titleBlock"> <h2>' + value.cuisines[j].categoryname + '</h2> <p>' + value.cuisines[j].cuisines.length + ' Items</p> </div><div class="cuisineContainer">';
                                
                                    for (var k = 0; k < value.cuisines[j].cuisines.length; k++) {
                                        
                                        cuisinesdata += '<div class="cuisine card" onclick="cafeteriaPopup('+value.cuisines[j].cuisines[k].id+');"><div class="cuisineImgWrap"><div class="table"><div class="tableCell"><img src="' + value.cuisines[j].cuisines[k].image + '" alt="' + value.cuisines[j].cuisines[k].name + '"></div></div></div><div class="cuisineData"><h2 class="cuisineTitle">' + value.cuisines[j].cuisines[k].name + '</h2><h4 class="cuisineRestroTitle">'+ value.cuisines[j].cuisines[k].kitchen_name + '</h4><p class="cuisineDesc">' + value.cuisines[j].cuisines[k].description + '</p><div class="priceBlock"><p class="price">₹ ' + value.cuisines[j].cuisines[k].price + '</p><p>Serves ' + value.cuisines[j].cuisines[k].serves + '</p></div><div class="quantityBlock"><div class="hurryBlock"><div class="timeBlock"></div><div class="hurryBlockContainer">';
                                        
                                        var cuisintype = value.cuisines[j].cuisines[k].type.split(",");
                                        if ((cuisintype.indexOf("veg") !== -1) || (cuisintype.indexOf("Veg") !== -1)) {
                                            cuisinesdata += '<p class="food-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                        } else {
                                            cuisinesdata += '<p class="food-cat nonfood-cat"><img src="<?=$baseurl;?>images/icons/veg-cat.svg" alt=""></p>';
                                        }
                                        cuisinesdata += '</div></div></div></div></div>';
                                       
                                    }

                                    cuisinesdata += '</div>';
                                }
                        
                            $("#cuisines").html(cuisinesdata);
                           
                            adjustHeight();
                        }
                        
                    }
                });
            
        }
        /*
        By:Jyoti Vishwakarma
        Description: choose meal type frpm popup
        */
        function chooseOffering(){
            $("#corporateli").removeClass("activeSub");
            $("#teamli").removeClass("activeSub");
            $("#eventli").removeClass("activeSub");
            $("#cafeteriali").removeClass("activeSub");
            $("#corporateli2").removeClass("activeSub");
            $("#teamli2").removeClass("activeSub");
            $("#eventli2").removeClass("activeSub");
            $("#cafeteriali2").removeClass("activeSub");
            $("#chooseProcedBtn").attr("disabled", true);
            $("#chooseProcedBtn").text("Proccessing...");
            if ($(".meals-select").hasClass("meal-selected")) {
                var selected = $(".meal-selected").attr("data-value");
                var mt = "";
                if(selected == "personalmeal"){
                    $(".meal-order").text("Personal Meals");
                    $("#meals_check").val('corporate_meals');
                    mt = 'corporate_meals';
                    getCategories('1');
                    getCuisines();
                    $("#corporateli").addClass("activeSub");
                    $("#corporateli2").addClass("activeSub");
                }else if(selected == "teammeal"){
                    $(".meal-order").text("Team Meals");
                    $("#meals_check").val('team_meals');
                    mt = 'team_meals';
                    getCategories('3');
                    getTeamMeals();
                    $("#teamli").addClass("activeSub");
                    $("#teamli2").addClass("activeSub");
                }else if(selected == "corporatemeal"){
                    $(".meal-order").text("Event Meals");
                    $("#meals_check").val('event_meals');
                    mt = 'event_meals';
                    getCategories('4');
                    getEventMeals();
                    $("#eventli").addClass("activeSub");
                    $("#eventli2").addClass("activeSub");
                }else if(selected == "cafeteria"){
                    $(".meal-order").text("Cafeteria Meals");
                    $("#meals_check").val('cafeteria');
                    mt = 'cafeteria';
                    getCategories('6');
                    getCafeteria();
                    $("#cafeteriali").addClass("activeSub");
                    $("#cafeteriali2").addClass("activeSub");
                }else{
                    return false;
                }
                var d = new Date();
                d.setTime(d.getTime() + (1*24*60*60*1000));
                var expires = "expires="+ d.toUTCString();
                document.cookie = "mealstype" + "=" + mt + ";" + expires + ";path=/";
            } 

        }

        /*
        By:Jyoti Vishwakarma
        Description: change meal type selection from dropdown
        */
        function changeChooseOffering(selected){

            var d = new Date();
            d.setTime(d.getTime() + (1*24*60*60*1000));
            var expires = "expires="+ d.toUTCString();
            document.cookie = "mealstype" + "=" + selected + ";" + expires + ";path=/";
            $("#corporateli").removeClass("activeSub");
            $("#teamli").removeClass("activeSub");
            $("#eventli").removeClass("activeSub");
            $("#cafeteriali").removeClass("activeSub");
            $("#corporateli2").removeClass("activeSub");
            $("#teamli2").removeClass("activeSub");
            $("#eventli2").removeClass("activeSub");
            $("#cafeteriali2").removeClass("activeSub");
            if(selected == "corporate_meals"){
                $(".meal-order").text("Personal Meals");
                $("#meals_check").val('corporate_meals');
                var elmnt = document.getElementById("ScrollUpToDiv");
                elmnt.scrollIntoView(true); 
                getCategories('1');
                getCuisines();
                $("#corporateli").addClass("activeSub");
                $("#corporateli2").addClass("activeSub");
            }else if(selected == "team_meals"){
                $(".meal-order").text("Team Meals");
                $("#meals_check").val('team_meals');
                var elmnt = document.getElementById("ScrollUpToDiv");
                elmnt.scrollIntoView(true); // Top
                getCategories('3');
                getTeamMeals();
                $("#teamli").addClass("activeSub");
                $("#teamli2").addClass("activeSub");
            }else if(selected == "event_meals"){
                $(".meal-order").text("Event Meals");
                var elmnt = document.getElementById("ScrollUpToDiv");
                elmnt.scrollIntoView(true); // Top
                $("#meals_check").val('event_meals');
                getCategories('4');
                getEventMeals();
                $("#eventli").addClass("activeSub");
                $("#eventli2").addClass("activeSub");
            }else if(selected == "cafeteria"){
                $(".meal-order").text("Cafeteria Meals");
                $("#meals_check").val('cafeteria');
                var elmnt = document.getElementById("ScrollUpToDiv");
                elmnt.scrollIntoView(true); // Top
                getCategories('6');
                getCafeteria();
                $("#cafeteriali").addClass("activeSub");
                $("#cafeteriali2").addClass("activeSub");
            }
        }
        /*
        By:Jyoti Vishwakarma
        Description: Validate addons selections
        */
        function checkval(s,z){
            qut1 = $("#da"+s).val();
            qut = $("#da"+s).val();
            var q = "#das"+s;
            var qt = $(q).val();   
            var prt = "#prt"+s;   
            var ckId =  "#"+z;
            
                // alert($(q).val());
                if($(ckId).is(":checked")){
                    $(q).val(parseInt(qt) - 1);
                }
                else if($(ckId).is(":not(:checked)")){
                    $(q).val(parseInt(qt) + 1);
                    
                }
                
                if($(q).val() == 0 ){
                    $("#validateflag").val(0);
                    $("#counterror"+s).css("display","none");
                }
                else{
                    $("#counterror"+s).css("display","block");
                    $("#counterror"+s).text("Minimum selection should be "+qut1+".");
                    $("#validateflag").val(1);
                }
                        
        }
        /*
        By:Jyoti Vishwakarma
        Description: get Offer
        */
        function getOffer(){
            var kitchenid = $("#kitchen_id").val();
            $("#kitchenDiv-"+kitchenid).removeClass("activeRestro");
            $("#kitchen_id").val('');
            var mealtype = $("#meals_check").val();
            changeChooseOffering(mealtype);
        }
        /*
        By:Jyoti Vishwakarma
        Description: cafeteria cuisine descriptopn in popup 
        */
        function cafeteriaPopup(id) {
            var pagedata = {
                "id": id
            };
            $.ajax({
                url: serviceurl + 'cafeteria_cuisine_desc',
                type: 'POST',
                data: JSON.stringify(pagedata),
                datatype: 'JSON',
                async: false,
                success: function(data) {
                    var value = JSON.parse(data);
                    console.log(value.cuisines[0].name);
                    var val = "";
                    if (value.cuisines[0].type.includes('Non Veg')) {
                        val = '<img src="images/icons/nonveg.svg" alt="Non Veg">' + value.cuisines[0].kitchen_name;
                    } else {
                        val = '<img src="images/icons/veg.svg" alt="Veg">' + value.cuisines[0].kitchen_name;
                    }
                    
                    $("#cuisineTitleCaf").html(value.cuisines[0].name);
                    $("#kitchen_name_caf").html(value.cuisines[0].kitchen_name);
                    $("#cuisine_img_caf").attr("src", value.cuisines[0].image);
                    $("#veg_val_caf").html(val);
                    $("#cuisine-details_caf").html(value.cuisines[0].description);
                    $("#price_caf").html(value.cuisines[0].price);
                    $("#serve_caf").html("Serves " + value.cuisines[0].serve);
                    $(".cafeteria-popup, .overlay")
                        .delay(400)
                        .fadeIn();

                }
            });

            
        }
    </script>

    
</body>

</html>