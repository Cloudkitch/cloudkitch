<?php
	include 'inc/databaseConfig.php';
	error_reporting(0);
	$baseurl = '/'; //live
	// $baseurl = '/cloudkitch/web/'; //UAT
    // $baseurl = '/cloudkitch/web/'; //local


    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
    header("Pragma: no-cache"); // HTTP 1.0.
    header("Expires: 0 "); // Proxies.


?>
<script>
	 var serviceurl = '/../../api_1.0.php?service_name=';  //live
	 var imgurl = '/../../'; //live
	// var serviceurl = 'http://52.66.213.167/cloudkitch/web/api_1.0.php?service_name='; // UAT
	// var imgurl = 'http://52.66.213.167/cloudkitch/web/';  //UAT
	// var serviceurl = 'http://localhost/cloudkitch/web/api_1.0.php?service_name='; // local
	// var imgurl = 'http://localhost/cloudkitch/web/';  //local
</script>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="theme-color" content="#a38379">
<link href="<?= $baseurl; ?>images/favicon.png" rel="icon" >
<link rel="stylesheet" type="text/css" href="<?= $baseurl; ?>css/slick.css" >
<link rel="stylesheet" type="text/css" href="<?= $baseurl; ?>css/style.css" defer>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="<?= $baseurl; ?>css/timepicki.css">
<script src="https://apis.google.com/js/api:client.js"></script>
<style>
    .error {
        color: red;
    }
</style>
<!-- New analytics codes Started-->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-161799933-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-161799933-1');
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '291183102008879');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=291183102008879&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Linked In-->
<script type="text/javascript">
_linkedin_partner_id = "2226420";
window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=2226420&fmt=gif" />
</noscript>

<!-- Analytics code end-->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-155865951-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-155865951-1');
</script>
<!-- Hotjar Tracking Code for https://cloudkitch.co.in/ -->
    <script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1749547,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>


<script>
  var googleUser = {};
  var startApp = function() {
    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
        client_id: '287145767352-qi8asdmobui20671musqaq0jga2ru2re.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      attachSignin(document.getElementById('customBtn'));
    });
  };

  function attachSignin(element) {
    // console.log(element.id);
    auth2.attachClickHandler(element, {},
        function(googleUser) {
        //   document.getElementById('name').innerText = "Signed in: " +
            //   alert(googleUser.getBasicProfile().getName());
            var profile = googleUser.getBasicProfile();
            var email = profile.getEmail();
            console.log(profile.getImageUrl()); 
            var pagedata = {
                "email": email
            };
            var userid = '<?php if (isset($_SESSION['userid'])) {  echo $_SESSION['userid'];    } ?>';
            if (userid == "") {
                $.ajax({
                    url: serviceurl + 'isUserExist',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        if (value.status == 'new') {
                            $('.first-login').fadeOut();
                            $('.corporate-login').delay(400).fadeIn();   //$(".google").on("click",function(){$(".first-login").fadeOut();$(".corporate-login").delay(400).fadeIn();});
                            $("#google_id").val(profile.getId());
                            $("#google_name").val(profile.getName());
                            $("#google_imgurl").val(profile.getImageUrl());
                            $("#google_email").val(profile.getEmail());
                            $("#social_type").val('google');
                        } else {
                            if (value.isCorporateUser == '1') {
                                // window.location.href = "<?=$baseurl?>corporate.php";
                                var currenturl = window.location.href;
                                if(currenturl.includes("cart")){
                                    location.reload();
                                }else{
                                    window.location.href = "<?=$baseurl?>corporate.php";
                                }
                                
                            } else {
                                location.reload();
                            }
                        }
                    }
                });
            }

        }, function(error) {
        //   alert(JSON.stringify(error, undefined, 2));
        });
  }
  function loginClick()
  {
      $("#gSignInWrapper").click();
  }
  </script>