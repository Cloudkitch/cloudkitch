<!DOCTYPE html>
<html>

<head>
    <title>Cloudkitch</title>
    <meta name="description" content="CloudKitch introduces smart kitchens that are connected with innovative technologies which are quintessential to bring success to any restaurant.">
    <style>
    .partymenuBtn {position: absolute; right: 330px;}
    .ordolnBtn {position: absolute;right: 150px;}
    .heading1 {font-family: OpenSans-Bold;color: #000000;font-size: 30px;}
    </style>
</head>

<body>
<?php
include "head.php";
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
;

?>

<style type="text/css">
    .error {
        color: red;
    }
</style>
<div class="preloader"></div>
<div class="loader"></div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="<?= $baseurl; ?>css/timepicki.css">

<header>
    <div class="logo">
       <a href="<?=$baseurl?>"><img src="<?=$baseurl?>images/logo.svg" alt="Cloudkitch"></a>
    </div>
    <div>
        <p class="linkBtn custBtn">HOME</p>
    </div>
    <div class="linkWrap">
        <ul>
            <li class="partymenuBtn"><a>
                <p class="btn borderBtn gradientBtn partyBtn">
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                    <path id="Icon_color" data-name="Icon color" d="M12.5,25A12.5,12.5,0,1,1,25,12.5,12.514,12.514,0,0,1,12.5,25Zm0-22.5a10,10,0,1,0,10,10A10.011,10.011,0,0,0,12.5,2.5Zm0,16.261A6.878,6.878,0,0,1,6.925,15.9a.613.613,0,0,1-.1-.488.643.643,0,0,1,.3-.412l1.087-.638a.638.638,0,0,1,.813.163,4.35,4.35,0,0,0,6.95,0,.638.638,0,0,1,.813-.163L17.875,15a.643.643,0,0,1,.3.412.616.616,0,0,1-.1.488A6.881,6.881,0,0,1,12.5,18.761Zm5.625-7.511H14.376a.626.626,0,0,1-.625-.625V10a2.5,2.5,0,0,1,5,0v.625A.626.626,0,0,1,18.125,11.25Zm-7.5,0H6.875a.626.626,0,0,1-.625-.625V10a2.5,2.5,0,0,1,5,0v.625A.626.626,0,0,1,10.625,11.25Z" transform="translate(0 0)" fill="#F68F30"/>
                    </svg>
                    Party Menu</p></a>
            </li>

            <li class="ordolnBtn"><a>
                <p class="btn borderBtn gradientBtn partyBtn">
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                    <path id="Icon_color" data-name="Icon color" d="M12.5,25A12.5,12.5,0,1,1,25,12.5,12.514,12.514,0,0,1,12.5,25Zm0-22.5a10,10,0,1,0,10,10A10.011,10.011,0,0,0,12.5,2.5Zm0,16.261A6.878,6.878,0,0,1,6.925,15.9a.613.613,0,0,1-.1-.488.643.643,0,0,1,.3-.412l1.087-.638a.638.638,0,0,1,.813.163,4.35,4.35,0,0,0,6.95,0,.638.638,0,0,1,.813-.163L17.875,15a.643.643,0,0,1,.3.412.616.616,0,0,1-.1.488A6.881,6.881,0,0,1,12.5,18.761Zm5.625-7.511H14.376a.626.626,0,0,1-.625-.625V10a2.5,2.5,0,0,1,5,0v.625A.626.626,0,0,1,18.125,11.25Zm-7.5,0H6.875a.626.626,0,0,1-.625-.625V10a2.5,2.5,0,0,1,5,0v.625A.626.626,0,0,1,10.625,11.25Z" transform="translate(0 0)" fill="#F68F30"/>
                    </svg>
                    Order Online</p></a>
            </li>
          
        </ul>
    </div>
</header>

<div class="mobHeader">
    <div class="logo">
        <a href="<?= $baseurl; ?>"><img src="<?= $baseurl; ?>images/logo.svg" alt="Cloudkitch"></a>
    </div>
    <div class="mobCart">
        <a href="<?= $baseurl; ?>cart/">
            <p><img src="<?= $baseurl; ?>images/icons/headCart.svg" alt="Cart"><span class="notiCount">10</span></p>
        </a>
    </div>
<?php 
    if (isset($_SESSION['isCorporateUser']) && $_SESSION['isCorporateUser']== '1') {
?>
    <div class="profile-logo">
        <p><img src="<?= $baseurl; ?>images/restro/Market-Bistro.svg" alt="Corporate"></p>
        <ul class="submenu corporateSubmenu">
            <li>
                <a href="<?= $baseurl; ?>">
                    <p><img src="<?= $baseurl; ?>images/icons/personal-meals.svg" alt="Personal Meals">Personal Meals</p>
                </a>
            </li>
            <li>
                <a href="<?= $baseurl; ?>">
                    <p><img src="<?= $baseurl; ?>images/icons/team-meals.svg" alt="Team Meals">Team Meals</p>
                </a>
            </li>
            <li>
                <a href="<?= $baseurl; ?>">
                    <p><img src="<?= $baseurl; ?>images/icons/corporate-events.svg" alt="Corporate Events">Corporate Events</p>
                </a>
            </li>
            <li>
                <a href="<?= $baseurl; ?>">
                    <p><img src="<?= $baseurl; ?>images/icons/cafeteria-menu.svg" alt="Cafeteria Menu">Cafeteria Menu</p>
                </a>
            </li>
        </ul>
    </div>
<?php }  ?>
    <div class="menuBtn">
        <div class="line l1"></div>
        <div class="line l2"></div>
        <div class="line l3"></div>
    </div>
</div>

<div class="mobNavbar">
    <ul>
        <?php
        if (!isset($_SESSION['userid'])) {
        ?>
            <li><a href="javascript:;" class="linkBtn">
                    <p>Login</p>
                </a></li>
            <li><a class="signUp">
                    <p>Sign Up</p>
                </a></li>
        <?php
        }
        ?>
        <?php
        if (isset($_SESSION['userid'])) {
        ?>
            <li>
                <a href="<?= $baseurl; ?>profile/">
                    <p>My Profile</p>
                </a>
            </li>
            <li>
                <a href="order-history.php">
                    <p>My Orders</p>
                </a>
            </li>
            <li>
                <a onclick="logout()">
                    <p>Logout</p>
                </a>
            </li>
        <?php
        }
        ?>
        <!-- <li><a><p>Get Our App</p></a></li> -->
        <!-- <li><a href="javascript:;" class="corporate-login-link">
                <p>Corporate Login</p>
            </a></li> -->
        <li>
            <div class="searchWrap-mob">
                <form>
                    <input type="text" placeholder="Search Hot Favourites" id="mobSearch">
                    <button onClick="searchCusine();"><img src="<?= $baseurl; ?>images/icons/search.svg" alt="Search"></button>
                </form>
            </div>
        </li>
    </ul>
</div>
<section class="topSection">
        <div class="homeSlide">
               <img src="<?= $baseurl; ?>images/banner/1.jpg" alt="">
        </div>
      
       
</section>
<section>
<section class="section">
        <div class="titleWrap sectionText">
            <h2 class="heading1">Your food requirements got Served</h2>
        </div>
</section>
   
    <!-- <section class="section">
        <div class="titleWrap sectionText">
            <h2>Quick Orders</h2>
            <p>Kitchen's to Choose From</p>
        </div>
        <div class="filterWrap">
            <div class="titleWrap">
                <h2><img src="<?= $baseurl; ?>images/icons/cuisine.svg" alt="Course"> Popular Options</h2>
            </div>
            <div class="filterBtn"></div>
            <div class="filterContainer">
                <div class="searchWrap menuSearch">
                    <form>
                        <input type="text" id="search_inner" onkeyup="getCuisinesFilter()" placeholder="Search Hot Favourites">
                        <input type="hidden" id="kitchen_id">
                    </form>
                </div>
                <div class="buttonWrap">
                    <p class="btn borderBtn offersBtn" onclick="getOffer()"><img src="<?= $baseurl; ?>images/icons/discount.svg" alt="offers">All Offers</p>
                    <div class="filter-check">
                        <input class="custom-radio" onchange="getCuisinesFilter()" type="checkbox" id="checkbox_veg" name="checkbox_veg">
                        <label for="checkbox_veg" class="btn radio-label">
                            <img src="<?= $baseurl; ?>images/icons/leaf.svg" alt="Veg">VEG
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="menuWrap autoHeight">
            <div class="cuisineWrap card restrocuisineWrap">
               
                <span class="cusine-second">
                    <p class="closeFilter">Close X</p>
                </span>
                <div class="checkWrap">
                    <ul id="categories">

                    </ul>
                </div>
            </div>
            <div class="cuisineWrapper cuisineWrappernotflex">
                <div class="restroWrap homerestroSlider" id="kitchens"></div>
                <div class="cuisineContainer heightContainer">
                    <div id="cuisines"></div>
                </div>
                <span data-value="1" id="loadmoredata" class="loadmore" style="border: none;"> </span>
            </div>
        </div>
        <?php
        if (isset($_COOKIE['pincode'])) {
            $display = 'none;';
        } else {
            $display = 'block;';
        }
        ?>
        <div class="overlay" style="display:<?= $display ?>"></div>
        <div class="popup locationPopup" style="display:<?= $display ?>">
            <div class="popup-wrapper">
                <div class="centerText">
                    <img src="<?= $baseurl; ?>images/logo.svg" alt="Cloudkitch" class="popup-logo">
                </div>
                <div class="firstDiv">
                    <form id="pincode_form" name="pincode_form">
                        <h2>Welcome to Cloudkitch!</h2><br>
                        <div class="form-conatiner">
                            <div class="form-group form-float-group">
                                <input type="text" placeholder="" id="pincode" name="pincode" autocomplete="off" class="form-float-control">
                                <label for="sign-in-email" class="form-float-lab">Enter area pincode here</label>
                            </div>
                            <button type="submit" class="btn-gradient">Proceed</button>
                        </div>
                    </form>
                    <p class="skip">Skip</p>
                </div>
                <div class="secDiv centerText" style="display:none;">
                    <h2>Your area is currently not serviceable.</h2>
                </div>
            </div>
        </div>
    </section> -->
    <?php
    include 'footer.php';
    ?>

   
    
</body>

</html>