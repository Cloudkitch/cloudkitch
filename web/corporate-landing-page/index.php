<?php
    $baseurl = 'https://cloudkitch.co.in/corporate-landing-page/';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <noscript>
            <meta http-equiv="refresh" content="">
        </noscript>
        <meta charset = "UTF-8" />
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?= $baseurl; ?>images/favicon.png" rel="icon">
        <link rel="stylesheet" type="text/css" href="<?= $baseurl; ?>css/slick.css">
        <link rel="stylesheet" type="text/css" href="<?= $baseurl; ?>css/style.css">
        <!-- Google Analytics -->        
        <title>Cloudkitch</title>
        <meta name="description" content="">
        <style>
        .formElement{position: relative}
        .error{
            color : red;
        }
        .thankYou{position: absolute;
                 bottom: -35px;
                 display: none;
                 font-size: 20px;
                 color:green}
        label.error{
            position: absolute;
    left: 0;
    bottom: 5px;
    font-size: 12px;}
        </style>
    </head>
    <body>
        <div class="preloader"></div>
        <header>
            <ul>
                <li><a href="<?= $baseurl; ?>"><img src="<?= $baseurl; ?>images/logo.svg" alt="" class="logo"></a></li>
                <!-- <li><a href="https://cloudkitch.co.in/" target="_blank"><p>Home</p></a></li> -->
            </ul>
            <div class="btnWrap">
                <!-- <a href="https://cloudkitch.co.in/" target="_blank">
                    <div class="gradientBtn">
                        <img src="?= $baseurl; ?>images/icons/smiley.svg" alt=""><p>Party<br>Menu</p>
                    </div>
                </a> -->
                <a href="https://cloudkitch.co.in/" target="_blank">
                    <div class="gradientBtn">
                        <img src="<?= $baseurl; ?>images/icons/cart.svg" alt=""><p>Corporate Login</p>
                    </div>
                </a>
            </div>
        </header>
        <div class="mobHeader">
            <div class="logo">
                <a href="<?= $baseurl; ?>"><img src="<?= $baseurl; ?>images/logo.svg" alt="" class="logo"></a>
            </div>
            <div class="menuBtn">
                <div class="line l1"></div>
                <div class="line l2"></div>
                <div class="line l3"></div>
            </div>
        </div>
        <div class="mobNavbar">
            <ul>
                <li><a href="https://cloudkitch.co.in/" target="_blank"><p>Home</p></a></li>
                <!-- <li><a href="https://cloudkitch.co.in/" target="_blank"><p>Party Menu</p></a></li> -->
                <li><a href="https://cloudkitch.co.in/" target="_blank"><p>Corporate Login</p></a></li>
            </ul>
        </div>
        <section class="banner">
            <img src="<?= $baseurl; ?>images/banner.svg" alt="" class="deskBanner">
            <img src="<?= $baseurl; ?>images/mobBanner.jpg" alt="" class="mobBanner">
            <div class="overlay">
                <div class="table">
                    <div class="tableCell">
                        <h3 class="text-uppercase">Ala carte selection, Working curated lunch breakfast high tea, Event menus designed for celebrations and occasion</h3>
                        <h2>Specialists @ your service</h2>
                        <a href="#how-it-works" class="btn blackBtn">Know More</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <!-- <h2 class="sectionTitle">Your food requirements got <span>Served</span></h2> -->
            <h2 class="sectionTitle">Uniquely curated culinary <span> Experiences</span></h2>
            <div class="mealsWrap">
                <div class="mealBlock">
                    <!-- <img src="<?= $baseurl; ?>images/personal-meals.svg" alt="Personal Meals"> -->
                    <img src="<?= $baseurl; ?>images/personal-meal.jpg" alt="Personal Meals">
                    <div class="overlay">
                        <div class="table">
                            <div class="tableCell">
                                <h2>Personal Meals</h2>
                                <p>Order single meal/ meal for two from our different partner restaurants in a single cart.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mealBlock">
                    <img src="<?= $baseurl; ?>images/team-meals.svg" alt="Team Meals">
                    <div class="overlay">
                        <div class="table">
                            <div class="tableCell">
                                <h2>Team Meals</h2>
                                <p>Order single meal / meal for two from our different partner restaurants in a single cart.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mealBlock">
                    <img src="<?= $baseurl; ?>images/parties.svg" alt="Parties/Events">
                    <div class="overlay">
                        <div class="table">
                            <div class="tableCell">
                                <h2>Parties / Events</h2>
                                <p>Order single meal/ meal for two from our different partner restaurants in a single cart.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btnWrap">
                <a href="https://cloudkitch.co.in/" target="_blank" class="btn orangeBtn">CORPORATES REGISTER WITH US</a>
            </div>
        </section>
        <section class="section greySection" id="how-it-works">
            <h2 class="sectionTitle">How it <span>Works?</span></h2>
            <div class="workSlider">
                <div class="slide"  title="PERSONAL MEALS">
                    <div class="worksWrap">
                        <div class="workblock">
                            <img src="<?= $baseurl; ?>images/step1.svg" alt="Login with your ID">
                            <h4>Login with your ID</h4>
                            <p>Login with your personal, with few clicks.</p>
                        </div>
                        <div class="workblock">
                            <img src="<?= $baseurl; ?>images/step2.svg" alt="Select Meal & Customise">
                            <h4>Select Meal & Customise</h4>
                            <!-- <p>Select Your Favourite meal, add from multiple cuisines and restaurant partners</p> -->
                            <p>Select Your Favourite meal, add from multiple restaurant, delivered at once.</p>
                        </div>
                        <div class="workblock">
                            <img src="<?= $baseurl; ?>images/step3.svg" alt="Get Delivery">
                            <h4>Get Delivery</h4>
                            <!-- <p>Schedule weekly delivery or order instantly and get food delivered to your preferred address.</p> -->
                            <p>Order now or schedule for later and get food delivered to your preferred address.</p>
                        </div>
                    </div>
                </div>
                <div class="slide" title="TEAM MEALS">
                    <div class="worksWrap">
                        <div class="workblock">
                            <img src="<?= $baseurl; ?>images/team-1.svg" alt="Login with your ID">
                            <h4>Login with your corporate ID</h4>
                            <p>Login with your corporate ID, with few clicks.</p>
                        </div>
                        <div class="workblock">
                            <img src="<?= $baseurl; ?>images/team-2.svg" alt="Select Meal & Customise">
                            <h4>Select Meal & Customise</h4>
                            <p>Select Your Favourite meal, add from multiple cuisines and restaurant partners</p>
                        </div>
                        <div class="workblock">
                            <img src="<?= $baseurl; ?>images/team-3.svg" alt="Get Delivery">
                            <h4>Get Delivery</h4>
                            <p>Schedule weekly delivery or order instantly and get food delivered to your preferred address.</p>
                        </div>
                    </div>
                </div>   
                <div class="slide" title="EVENT/PARTIES">
                    <div class="worksWrap">
                        <div class="workblock">
                            <img src="<?= $baseurl; ?>images/event-1.svg" alt="Login with your ID">
                            <h4>Login with your corporate ID</h4>
                            <p>Login with your corporate ID, with few clicks.</p>
                        </div>
                        <div class="workblock">
                            <img src="<?= $baseurl; ?>images/event-2.svg" alt="Select Meal & Customise">
                            <h4>Select Vivid Cuisines & Customize</h4>
                            <p>Select Your Favourite meal, add from multiple cuisines and restaurant partners</p>
                        </div>
                        <div class="workblock">
                            <img src="<?= $baseurl; ?>images/event-3.svg" alt="Get Delivery">
                            <h4>Get Delivery on Preferred Location</h4>
                            <p>Schedule weekly delivery or order instantly and get food delivered to your preferred address.</p>
                        </div>
                    </div>
                </div>                
            </div>
        </section>
        <section class="section">
            <!-- <h2 class="sectionTitle">We cater <span>Occasions</span></h2> -->
            <h2 class="sectionTitle">The Art & Science of creating an <span>Event</span></h2>
            <div class="occasionsWrap">
                <div class="occasionsBlock">
                    <img src="<?= $baseurl; ?>images/icon1.svg" alt="Team Meetings">
                    <h4>Team Meetings</h4>
                    <!-- <p>Team dinners, lunch, quick snacks</p> -->
                    <p>Team building, strategy or meeting.</p>
                    <p>Boost your energies with wide choices of personal box or mini buffet. Breakfast, lunch, high tea, dinner or snacks.</p>
                </div>
                <div class="occasionsBlock">
                    <img src="<?= $baseurl; ?>images/icon2.svg" alt="Corporate Events">
                    <h4>Corporate Events</h4>
                    <!-- <p>Corporate Parties, Annual Summits, Outdoor Meetings</p> -->
                    <p>Board meeting, Product launch, Annual day, festival or celebrations</p>
                    <p>Let us curate a menu, specially for you.</p>
                </div>
                <div class="occasionsBlock">
                    <img src="<?= $baseurl; ?>images/icon3.svg" alt="Birthday Party">
                    <h4>Birthday Party</h4>
                    <!-- <p>Themed birthday parties, special deserts needs.</p> -->
                    <p>Celebrations in your team, let us organize a cake & snacks.</p>
                </div>
                <!-- <div class="occasionsBlock">
                    <img src="<?= $baseurl; ?>images/icon4.svg" alt="House Party">
                    <h4>House Party</h4>
                    <p>Live counters, culinary buffet services</p>
                </div>
                <div class="occasionsBlock">
                    <img src="<?= $baseurl; ?>images/icon5.svg" alt="Kitty Party">
                    <h4>Kitty Party</h4>
                    <p>Snacks and Beverages accompanied with gossips</p>
                </div> -->
                <div class="occasionsBlock">
                    <img src="<?= $baseurl; ?>images/icon6.svg" alt="Other Occasions">
                    <!-- <h4>Other Occasions</h4> -->
                    <h4>Gourmet Experiences</h4>
                    <p>Imagine and we do the rest</p>
                </div>
            </div>
        </section>
        <section class="section greySection">
            <h2 class="sectionTitle"><span>Benefits</span> we offer to corporates</h2>
            <div class="benifitsSection">
                <div class="accordianBlock">
                    <div class="accordian">
                        <div class="accTitle" id="acc1">
                            <h4>Easy Registration</h4>
                        </div>
                        <div class="accBody acc1">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <a href="">register now ></a>
                        </div>
                    </div>
                    <div class="accordian">
                        <div class="accTitle" id="acc2">
                            <!-- <h4>Predefined Credit Period</h4> -->
                            <h4>Digitised Experience</h4>
                        </div>
                        <div class="accBody acc2">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <a href="">register now ></a>
                        </div>
                    </div>
                    <div class="accordian">
                        <div class="accTitle" id="acc3">
                            <!-- <h4>Great Corporate Discounts</h4> -->
                            <h4>A World of choices</h4>
                        </div>
                        <div class="accBody acc3">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <a href="">register now ></a>
                        </div>
                    </div>
                    <div class="accordian">
                        <div class="accTitle" id="acc4">
                            <h4>Use Meal Vouchers/Other payment options</h4>
                        </div>
                        <div class="accBody acc4">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <a href="">register now ></a>
                        </div>
                    </div>
                    <div class="accordian">
                        <div class="accTitle" id="acc5">
                            <h4>Curated Catering for Events/Meetings</h4>
                        </div>
                        <div class="accBody acc5">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <a href="">register now ></a>
                        </div>
                    </div>
                </div>
                <div class="accImage">
                    <img src="<?= $baseurl; ?>images/benifits.svg" alt="">
                </div>
            </div>
        </section>
        <section class="section" style="display:none;">
            <!-- <h2 class="sectionTitle">Currently we <span>Cater</span></h2> -->
            <h2 class="sectionTitle">We deliver the <span>Experience</span></h2>
            <div class="caterSlider">
                <div>
                    <img src="<?= $baseurl; ?>images/logo.svg" alt="">
                </div>
                <div>
                    <img src="<?= $baseurl; ?>images/logo.svg" alt="">
                </div>
                <div>
                    <img src="<?= $baseurl; ?>images/logo.svg" alt="">
                </div>
                <div>
                    <img src="<?= $baseurl; ?>images/logo.svg" alt="">
                </div>
                <div>
                    <img src="<?= $baseurl; ?>images/logo.svg" alt="">
                </div>
                <div>
                    <img src="<?= $baseurl; ?>images/logo.svg" alt="">
                </div>
                <div>
                    <img src="<?= $baseurl; ?>images/logo.svg" alt="">
                </div>
                <div>
                    <img src="<?= $baseurl; ?>images/logo.svg" alt="">
                </div>
                <div>
                    <img src="<?= $baseurl; ?>images/logo.svg" alt="">
                </div>
                <div>
                    <img src="<?= $baseurl; ?>images/logo.svg" alt="">
                </div>
                <div>
                    <img src="<?= $baseurl; ?>images/logo.svg" alt="">
                </div>
            </div>
        </section>
        <section class="section greySection">
            <h2 class="sectionTitle">Get in <span>Touch</span></h2>
            <div class="formWrap">
                <form name="getInTouchForm" id="getInTouchForm">
                    <div class="formGroup">
                        <div class="formElement">
                            <label>Name</label>
                            <input type="text" name="name" id="name" required/>
                        </div>
                        <div class="formElement">
                            <label>Email</label>
                            <input type="email" name="email" id="email" required/>
                        </div>
                    </div>
                    <div class="formGroup">
                        <div class="formElement">
                            <label>Phone</label>
                            <input type="text" name="phone" id="phone"  maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" onpast="return false" required>
                        </div>
                        <!-- <div class="formElement">
                            <label>Purpose</label>
                            <select>
                                <option></option>
                                <option>Option 1</option>
                                <option>Option 2</option>
                            </select>
                        </div> -->
                    </div>
                    <div class="formElement">
                        <label>Message</label>
                        <input type="text" name="message" id="message" required>
                    </div>
                    <div class="formElement btnWrap">
                        <button type="submit" id="submitBtn" class="btn orangeBtn">Send Message</button>
                        <p class="thankYou">Thank You...We will get back to you shortly.</p>
                    </div>
                </form>
            </div>
        </section>
        <!-- <footer>
            <div class="footerTop">
                <a href="<?= $baseurl; ?>"><img src="<?= $baseurl; ?>images/logo.svg" alt=""></a>
                <p>Great food experience is about thoughtful preparation. CloudKitch makes that happen for you.</p>
                <div class="social">
                    <a href="" target="_blank"><img src="<?= $baseurl; ?>images/twitter.svg" alt=""></a>
                    <a href="" target="_blank"><img src="<?= $baseurl; ?>images/instagram.svg" alt=""></a>
                    <a href="" target="_blank"><img src="<?= $baseurl; ?>images/linkedin.svg" alt=""></a>
                </div>
                <div class="copyrights">
                    <p>&copy; CloudKitch. All rights reserved.</p>
                </div>
            </div>
        </footer> -->
        <footer class="footer">
        <div class="footerLogoWrap">
            <img src="<?= $baseurl; ?>images/logo.svg" alt="Cloudkitch" class="logo">
            <p>Great food experience is about thoughtful preparation. CloudKitch makes that happen for you.</p>
        </div>
        <div class="footer-wrapper">
            <div class="footerCol">
                <ul class="footer-link">
                    <li>
                        <a href="https://cloudkitch.co.in/partners/" target="_blank">
                            <p>About Us</p>
                        </a>
                    </li>
                    <li>
                        <a href="https://cloudkitch.co.in/partners/" target="_blank">
                            <p>Partner with us</p>
                        </a>
                    </li>
                    <li>
                        <a href=https://cloudkitch.co.in/partners/#footer" target="_blank">
                            <p>Contact Us</p>
                        </a>
                    </li>
                    <li>
                        <a href="https://cloudkitch.co.in/" class="linkCorporate">
                            <p>Special Events Inquiry</p>
                        </a>
                    </li>
                </ul>
                <ul class="social-link">
                    <li>
                        <a href="https://www.facebook.com/officialcloudkitch/" target="_blank">
                            <p><img src="<?= $baseurl; ?>images/icons/fb-icon.svg" alt="facebook"></p>
                        </a>
                    </li>
                    <!-- <li>
                        <a href="https://twitter.com/CloudKitch" target="_blank">
                            <p><img src="<?= $baseurl; ?>images/icons/icon-twitter.svg" alt="twitter"></p>
                        </a>
                    </li> -->
                    <li>
                        <a href="https://www.instagram.com/cloudkitch/" target="_blank">
                            <p><img src="<?= $baseurl; ?>images/icons/icon-instagram.svg" alt="instagram"></p>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/company/cloudkitch-private-limited" target="_blank">
                            <p><img src="<?= $baseurl; ?>images/icons/icon-linkedin.svg" alt="linkedin"></p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="footerCol">
                <ul class="footer-link" id="favouriteResto">
                    <li><p class="subtitle">Your Favourite Restaurants</p></li>
                    <li><a href="https://cloudkitch.co.in/kitchen/3450/Between-Breads"><p>Between Breads</p></a></li>
                    <li><a href="https://cloudkitch.co.in/kitchen/3421/Sun-Moon-&-Potatoes"><p>Sun Moon &amp; Potatoes</p></a></li>
                    <li><a href="https://cloudkitch.co.in/kitchen/3420/India-Bistro-Express"><p>India Bistro Express</p></a></li></ul>
            </div>
            <div class="footerCol">
                <ul class="footer-link">
                    <li>
                        <p class="subtitle">To Place Your Order Call</p>
                    </li>
                    <li>
                        <a href="tel:917710999666">
                            <p>+91 771 099 9666</p>
                        </a>
                    </li>
                    <li>
                        <p class="subtitle">Take away</p>
                    </li>
                    <li>
                        <a href="mailto:enquiry@cloudkitch.co.in">
                            <p>enquiry@cloudkitch.co.in</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="footerCol">
                <ul class="footer-link">
                    <li>
                        <a href="https://cloudkitch.co.in/terms-and-conditions/">
                            <p>Terms &amp; Conditions</p>
                        </a>
                    </li>
                    <li>
                        <a href="https://cloudkitch.co.in/grievance-policy/">
                            <p>Grievance Policy</p>
                        </a>
                    </li>
                    <li>
                        <a href="https://cloudkitch.co.in/cookie-policy/">
                            <p>Cookie Policy</p>
                        </a>
                    </li>
                    <li>
                        <a href="https://cloudkitch.co.in/cancellation-and-refund/">
                            <p>Cancellation &amp; Refund</p>
                        </a>
                    </li>
                    <li>
                        <a href="https://cloudkitch.co.in/sitemap/">
                            <p>Sitemap</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
        <script src="<?= $baseurl; ?>js/jquery.js" defer></script>
        <script src="<?= $baseurl; ?>js/slick.js" defer></script>
        <script src="<?= $baseurl; ?>js/custom.js" defer></script>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://cloudkitch.co.in/js/jquery.validate.min.js"></script>
        <script>
        
        $("form[name='getInTouchForm']").validate({
            rules: {
                name: "required",
                email: "required",
                phone: "required",
                message: "required",
            },
            submitHandler: function(form) {
                $("#submitBtn").prop("disabled", true);
                $("#submitBtn"). html("Sending...");
                var name = $("#name").val();
                var email = $("#email").val();
                var phone = $("#phone").val();
                var message = $("#message").val();

                var pagedata = {
                    "name": name,
                    "email": email,
                    "phone": phone,
                    "message": message
                };
                $.ajax({
                    url:  'https://cloudkitch.co.in/api_1.0.php?service_name=getInTouch',
                    type: 'POST',
                    data: JSON.stringify(pagedata),
                    datatype: 'JSON',
                    async: false,
                    success: function(data) {
                        var value = JSON.parse(data);
                        if (value.status == 'success') {
                            $("#submitBtn").prop("disabled", false);
                            $("#submitBtn"). html("Send Message");   
                            $("#name").val("");
                            $("#email").val("");
                            $("#phone").val("");
                            $("#message").val("");
                            $(".thankYou").css("display","block");   
                            setTimeout(function(){
                                $(".thankYou").css("display","none");   
                            }, 3000);  
                            // alert(value.status);
                        
                        }

                    }
                });
            }
        });
           
        </script>
    </body>
</html>