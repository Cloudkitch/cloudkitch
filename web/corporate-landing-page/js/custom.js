$(document).ready(function(){

    $('.preloader').fadeOut();

    $('.caterSlider').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 0,
        speed: 2500,
        cssEase:'linear',
        responsive: [
            {
              breakpoint: 500,
              settings: {
                slidesToShow: 2
              }
            }
        ]
    });

    $('.accTitle').click(function() {
        var acc_id = this.id;
        if ($(this).hasClass('open')) {} else {
            $('.open').parent().find('.accBody').slideUp();
            // $('.open').parent().find('.faq_btn').removeClass('rotate_arrow');
            $('.accTitle').removeClass('open');
        }
        $('.' + acc_id).slideToggle("slow");
        $(this).toggleClass('open');
        // $(this).find(".faq_btn").toggleClass('rotate_arrow');
    });

    $('.workSlider').slick({
        arrows: false,
        dots: true,
        customPaging: function(slider, i) { 
            return '<button class="tab">' + $(slider.$slides[i]).attr('title') + '</button>';
        },
        autoplay: false,
        autoplaySpeed: 2500,
        infinite: true
    });

    $('.menuBtn').on('click',function(){
        $(this).toggleClass('actMenuBtn');
        $('.mobNavbar').slideToggle();
    })

});